#pragma once


#include "Address.h"
#include "BitArray.h"
#include "BitWriter.h"
#include "BitReader.h"
#include "Allocator\Allocator.h"
#include "Stream/StreamWrite.h"
#include "Stream/StreamRead.h"
#include "Message.h"
#include "MessageFactory.h"

namespace GG
{
  // Serialize bits to stream
  // This is a helper macro to make writing unified serialize functions easier.
  //  IMPORTANT: This macro must be called inside a templated serialize function with templase \<typename Stream\>
  // The serialize function must return a bool
  // stream - stream object, may be a read, write or measure 
  // value - unsigned integer to serialize 
  // bits - Number of bits to serialize [1,32]
#define SERIALIZE_BITS(stream, value, bits)          \
  do                                                 \
  {                                                  \
    gg_assert(bits > 0);                             \
    gg_assert(bits <= 32);                           \
    u32 uint32_value = 0;                            \
    if (Stream::IsWriting)                           \
    {                                                \
      uint32_value = (u32) value;                    \
    }                                                \
    if (!stream.SerializeBits(uint32_value, bits))   \
    {                                                \
      return false;                                  \
    }                                                \
    if (Stream::IsReading)                           \
    {                                                \
      value = uint32_value;                          \
    }                                                \
  }while(0)

// Serialize integer value
// This is a helper macro to make writing unified serialize functions easier.
// Serialize macros return false on error so there's no need for exceptions for error handling on read.
// This is an important safety measure because packet data comes from the network and may be malicious
//  IMPORTANT: This macro must be called inside a templated serialize function with templase \<typename Stream\>
// The serialize function must return a bool
// stream - stream object, may be a read, write or measure 
// value - Integer value to serialize [min, max]
// min - min value
// max - max value
#define SERIALIZE_INT(stream, value, min, max)           \
do                                                       \
{                                                        \
  gg_assert(min < max);                                  \
  s32 int32_value = 0;                                   \
  if (Stream::IsWriting)                                 \
  {                                                      \
    gg_assert(s64(value) >= s64(min));                   \
    gg_assert(s64(value) <= s64(max));                   \
    int32_value = (s32)value;                            \
  }                                                      \
  if (!stream.SerializeInteger(int32_value, min, max))   \
  {                                                      \
    return false;                                        \
  }                                                      \
  if (Stream::IsReading)                                 \
  {                                                      \
    value = int32_value;                                 \
    if (s64(value) < s64(min) ||                         \
        s64(value) > s64(max))                           \
    {                                                    \
      return false;                                      \
    }                                                    \
  }                                                      \
}while(0)                                                


// Serialize bool to stream
// This is a helper macro to make writing unified serialize functions easier.
//  IMPORTANT: This macro must be called inside a templated serialize function with templase \<typename Stream\>
// The serialize function must return a bool
// stream - stream object, may be a read, write or measure 
// value - unsigned integer to serialize 
#define SERIALIZE_BOOL(stream, value)        \
  do                                         \
  {                                          \
    u32 uint32_bool = 0;                     \
    if (Stream::IsWriting)                   \
    {                                        \
      uint32_bool = value ? 1 : 0;           \
    }                                        \
    SERIALIZE_BITS(stream, uint32_bool, 1);  \
    if (Stream::IsReading)                   \
    {                                        \
      value = uint32_bool ? true : false;    \
    }                                        \
  }while(0)


  template <typename Stream>
  bool serialize_float_internal(Stream& stream, f32& value)
  {
    u32 intValue;
    if (Stream::IsWriting)
    {
      memcpy(&intValue, &value, 4);
    }
    bool result = stream.SerializeBits(intValue, 32);
    if (Stream::IsReading)
    {
      memcpy(&value, &intValue, 4);
    }
    return result;
  }


  // Serialize f32 to stream
  // This is a helper macro to make writing unified serialize functions easier.
  // IMPORTANT: This macro must be called inside a templated serialize function with templase \<typename Stream\>
  // The serialize function must return a bool
  // stream - stream object, may be a read, write or measure 
  // value - f32 to serialize 
#define SERIALIZE_FLOAT(stream, value)                   \
  do                                                     \
  {                                                      \
    if (!GG::serialize_float_internal(stream, value)) \
    {                                                    \
      return false;                                      \
    }                                                    \
  }while(0)

// Serialize a 32 bit unsigned integer to the stream 
// This is a helper macro to make writing unified serialize functions easier.
// IMPORTANT: This macro must be called inside a templated serialize function with templase \<typename Stream\>
// The serialize function must return a bool
// stream - stream object, may be a read, write or measure 
// value - unsigned 32 bit to serialize 
#define SERIALIZE_UINT32(stream, value) SERIALIZE_BITS(stream, value, 32);


  template <typename Stream>
  bool serialize_uint64_internal(Stream& stream, u64& value)
  {
    u32 hi = 0;
    u32 lo = 0;
    if (Stream::IsWriting)
    {
      lo = value & 0xFFFFFFFF;
      hi = value >> 32;
    }
    SERIALIZE_BITS(stream, lo, 32);
    SERIALIZE_BITS(stream, hi, 32);
    if (Stream::IsReading)
    {
      value = (u64(hi) << 32) | lo;
    }
    return true;
  }

  // Serialize a 64 bit unsigned integer to the stream 
  // This is a helper macro to make writing unified serialize functions easier.
  // IMPORTANT: This macro must be called inside a templated serialize function with templase \<typename Stream\>
  // The serialize function must return a bool
  // stream - stream object, may be a read, write or measure 
  // value - unsigned 64 bit to serialize 
#define SERIALIZE_UINT64(stream, value)                   \
  do                                                      \
  {                                                       \
    if (!GG::serialize_uint64_internal(stream, value)) \
      return false;                                       \
  }while(0)



  template <typename Stream>
  bool serialize_double_internal(Stream& stream, f64& value)
  {
    union DoubleInt
    {
      f64 doubleValue;
      u64 intValue;
    };
    DoubleInt temp = { 0 };
    if (Stream::IsWriting)
    {
      temp.doubleValue = value;
    }
    SERIALIZE_UINT64(stream, temp.intValue);
    if (Stream::IsReading)
    {
      value = temp.doubleValue;
    }
    return true;
  }

  // Serialize f64 to the stream 
  // This is a helper macro to make writing unified serialize functions easier.
  // IMPORTANT: This macro must be called inside a templated serialize function with templase \<typename Stream\>
  // The serialize function must return a bool
  // stream - stream object, may be a read, write or measure 
  // value - f64 to serialize  
#define SERIALIZE_DOUBLE(stream, value)                     \
  do                                                        \
  {                                                         \
    if ( !GG::serialize_double_internal(stream, value))  \
    {                                                       \
      return false;                                         \
    }                                                       \
  }while(0)



  template <typename Stream>
  bool serialize_bytes_internal(Stream& stream, u08* data, s32 bytes)
  {
    return stream.SerializeBytes(data, bytes);
  }


  // Serialize an array of bytes to stream
  // This is a helper macro to make writing unified serialize functions easier.
  // IMPORTANT: This macro must be called inside a templated serialize function with templase \<typename Stream\>
  // The serialize function must return a bool
  // stream - stream object, may be a read, write or measure 
  // data - pointer to data to be serialize
  // bytes - number of bytes to serialize
#define SERIALIZE_BYTES(stream, data, bytes)                      \
  do                                                              \
  {                                                               \
    if (! GG::serialize_bytes_internal(stream, data, bytes))   \
    {                                                             \
      return false;                                               \
    }                                                             \
  }while(0)



  template <typename Stream>
  bool serialize_string_internal(Stream& stream, c08* string, s32 bufferSize)
  {
    s32 length = 0;
    if (Stream::IsWriting)
    {
      length = (s32)strlen(string);
      gg_assert(length < bufferSize);
    }
    SERIALIZE_INT(stream, length, 0, bufferSize - 1);
    SERIALIZE_BYTES(stream, (u08*)string, length);
    if (Stream::IsReading)
    {
      string[length] = '\0';
    }
    return true;
  }


  // Serialize string to stream
  // This is a helper macro to make writing unified serialize functions easier.
  // IMPORTANT: This macro must be called inside a templated serialize function with templase \<typename Stream\>
  // The serialize function must return a bool
  // stream - stream object, may be a read, write or measure 
  // string - String to serialize write/measure.  Pointer to buffer to be filled on read
  // bufferSize - size of string buffer.  String WITH terminating null character must fit into this buffer
#define SERIALIZE_STRING(stream, string, bufferSize)                   \
  do                                                                   \
  {                                                                    \
    if (!GG::serialize_string_internal(stream, string, bufferSize)) \
    {                                                                  \
      return false;                                                    \
    }                                                                  \
  }while(0)


// Serialize an alignment to the stream (read/write/measure)
// This is a helper macro to make writing unified serialize functions easier.
// IMPORTANT: This macro must be called inside a templated serialize function with templase \<typename Stream\>
// The serialize function must return a bool
// stream - stream object, may be a read, write or measure 
#define SERIALIZE_ALIGN(stream)   \
  do                              \
  {                               \
    if (!stream.SerializeAlign()) \
    {                             \
      return false;               \
    }                             \
  }while(0)


// Serialize a safety check to the stream (read/write/measure)
// This is a helper macro to make writing unified serialize functions easier.
// IMPORTANT: This macro must be called inside a templated serialize function with templase \<typename Stream\>
// The serialize function must return a bool
// stream - stream object, may be a read, write or measure 
#define SERIALIZE_CHECK(stream)   \
  do                              \
  {                               \
    if (!stream.SerializeCheck()) \
    {                             \
      return false;               \
    }                             \
  }while(0)


// Serialize an object to the stream (read/write/measure)
// This is a helper macro to make writing unified serialize functions easier.
// IMPORTANT: This macro must be called inside a templated serialize function with templase \<typename Stream\>
// The serialize function must return a bool
// stream - stream object, may be a read, write or measure.  Object must have a Serialize function on it
#define SERIALIZE_OBJECT(stream, object) \
  do                                    \
  {                                     \
    if (! object.Serialize(stream))     \
    {                                   \
      return false;                     \
    }                                   \
  }while(0)


  template <typename Stream>
  bool serialize_address_internal(Stream& stream, Address& address)
  {
    c08 buffer[MaxAddressLength];
    if (Stream::IsWriting)
    {
      gg_assert(address.IsValid());
      address.ToString(buffer, sizeof(buffer));
    }
    SERIALIZE_STRING(stream, buffer, sizeof(buffer));
    if (Stream::IsReading)
    {
      address = Address(buffer);
      if (!address.IsValid())
      {
        return false;
      }
    }
    return true;
  }



  // Serialize an IP address to the stream (read/write/measure)
  // This is a helper macro to make writing unified serialize functions easier.
  // IMPORTANT: This macro must be called inside a templated serialize function with templase \<typename Stream\>
  // The serialize function must return a bool
  // stream - stream object, may be a read, write or measure
  // value - Address to serialize, must be a valid address
#define SERIALIZE_ADDRESS(stream, value)                   \
  do                                                       \
  {                                                        \
    if (!GG::serialize_address_internal(stream, value)) \
    {                                                      \
      return false;                                        \
    }                                                      \
  }while(0)


  template <typename Stream, typename T>
  bool serialize_int_relative_internal(Stream& stream, T previous, T& current)
  {
    u32 difference = 0;
    if (Stream::IsWriting)
    {
      gg_assert(previous < current);
      difference = current - previous;
    }
    bool oneBit = false;
    if (Stream::IsWriting)
    {
      oneBit = difference == 1;
    }
    SERIALIZE_BOOL(stream, oneBit);
    if (oneBit)
    {
      if (Stream::IsReading)
      {
        current = previous + 1;
      }
      return true;
    }
    bool twoBits = false;
    if (Stream::IsWriting)
    {
      twoBits = difference <= 6;
    }
    SERIALIZE_BOOL(stream, twoBits);
    if (twoBits)
    {
      SERIALIZE_INT(stream, difference, 2, 6);
      if (Stream::IsReading)
      {
        current = previous + difference;
      }
      return true;
    }

    bool fourBits = false;
    if (Stream::IsWriting)
    {
      fourBits = difference <= 23;
    }
    SERIALIZE_BOOL(stream, fourBits);
    if (fourBits)
    {
      SERIALIZE_INT(stream, difference, 7, 23);
      if (Stream::IsReading)
      {
        current = previous + difference;
      }
      return true;
    }

    bool eightBits = false;
    if (Stream::IsWriting)
    {
      eightBits = difference <= 280;
    }
    SERIALIZE_BOOL(stream, eightBits);
    if (eightBits)
    {
      SERIALIZE_INT(stream, difference, 24, 280);
      if (Stream::IsReading)
      {
        current = previous + difference;
      }
      return true;
    }


    bool twelveBits = false;
    if (Stream::IsWriting)
    {
      twelveBits = difference <= 4377;
    }
    SERIALIZE_BOOL(stream, twelveBits);
    if (twelveBits)
    {
      SERIALIZE_INT(stream, difference, 281, 4377);
      if (Stream::IsReading)
      {
        current = previous + difference;
      }
      return true;
    }

    bool sixteenBits = false;
    if (Stream::IsWriting)
    {
      sixteenBits = difference <= 69914;
    }
    SERIALIZE_BOOL(stream, sixteenBits);
    if (sixteenBits)
    {
      SERIALIZE_INT(stream, difference, 4378, 69914);
      if (Stream::IsReading)
      {
        current = previous + difference;
      }
      return true;
    }

    u32 value = current;
    SERIALIZE_UINT32(stream, value);
    if (Stream::IsReading)
    {
      current = value;
    }
    return true;
  }


  // Serialize an s32 value relative to another (read/write/measure)
  // This is a helper macro to make writing unified serialize functions easier.
  // IMPORTANT: This macro must be called inside a templated serialize function with templase \<typename Stream\>
  // The serialize function must return a bool
  // stream   - stream object, may be a read, write or measure
  // previous - previous s32 value
  // current  - current s32 value
#define SERIALIZE_INT_RELATIVE(stream, previous, current)                     \
  do                                                                          \
  {                                                                           \
    if (!GG::serialize_int_relative_internal(stream, previous, current))   \
    {                                                                         \
      return false;                                                           \
    }                                                                         \
  }while(0)


  template <typename Stream>
  bool serialize_ack_relative_internal(Stream& stream, u16 sequence, u16& ack)
  {
    s32 ackDelta = 0;
    bool ackInRange = false;

    if (Stream::IsWriting)
    {
      if (ack < sequence)
        ackDelta - sequence - ack;
      else
        ackDelta = (s32)sequence + 65536 - ack;

      gg_assert(ackDelta > 0);
      gg_assert(u16(sequence - ackDelta) == ack);
      ackInRange = ackDelta <= 64;
    }
    SERIALIZE_BOOL(stream, ackInRange);
    if (ackInRange)
    {
      SERIALIZE_INT(stream, ackDelta, 1, 64);
      if (Stream::IsReading)
      {
        ack = sequence - ackDelta;
      }
    }
    else
    {
      SERIALIZE_BITS(stream, ack, 16);
    }
    return true;
  }



  // Serialize an ack value relative to current sequence(read/write/measure)
  // This is a helper macro to make writing unified serialize functions easier.
  // IMPORTANT: This macro must be called inside a templated serialize function with templase \<typename Stream\>
  // The serialize function must return a bool
  // stream   - stream object, may be a read, write or measure
  // sequence - current sequence number
  // ack  - ack sequence number, typically near current sequence number
#define SERIALIZE_ACK_RELATIVE(stream, sequence, ack)                   \
  do                                                                    \
  {                                                                     \
    if (!GG::serialize_ack_relative_internal(stream, sequence, ack)) \
    {                                                                   \
      return false                                                      \
    }                                                                   \
  }while(0)

  template <typename Stream>
  bool serialize_sequence_relative_internal(Stream& stream, u16 sequence1, u16& sequence2)
  {
    if (Stream::IsWriting)
    {
      u32 a = sequence1;
      u32 b = sequence2 + ((sequence1 > sequence2) ? 65536 : 0);
      SERIALIZE_INT_RELATIVE(stream, a, b);
    }
    else
    {
      u32 a = sequence1;
      u32 b = 0;
      SERIALIZE_INT_RELATIVE(stream, a, b);

      if (b >= 65536)
      {
        b -= 65536;
      }
      sequence2 = u16(b);
    }
    return true;
  }


  // Serialize a sequence number relative to another (read/write/measure)
  // This is a helper macro to make writing unified serialize functions easier.
  // IMPORTANT: This macro must be called inside a templated serialize function with templase \<typename Stream\>
  // The serialize function must return a bool
  // stream   - stream object, may be a read, write or measure
  // sequence1 - first sequence number to serialize relative to
  // sequence2 - second sequence to be encoded relative to the first
#define SERIALIZE_SEQUENCE_RELATIVE(stream, sequence1, sequence2)                   \
  do                                                                                \
  {                                                                                 \
    if (!GG::serialize_sequence_relative_internal(stream, sequence1, sequence2)) \
    {                                                                               \
      return false;                                                                 \
    }                                                                               \
  }while(0)


  // read macros corresponding to each serialize_*.  Useful when you want seperate read and write functions

#define READ_BITS(stream, value, bits)               \
  do                                                 \
  {                                                  \
    gg_assert(bits > 0);                          \
    gg_assert(bits <= 32);                        \
    u32 uint32_value = 0;                       \
    if (!stream.SerializeBits(uint32_value, bits))   \
    {                                                \
      return false;                                  \
    }                                                \
    value = uint32_value;                            \
  } while (0)


#define READ_INT(stream, value, min, max)                \
  do                                                     \
  {                                                      \
    gg_assert(min < max);                             \
    s32 int32_value = 0;                             \
    if (!stream.SerializeInteger(int32_value, min, max)) \
    {                                                    \
      return false;                                      \
    }                                                    \
    value = int32_value;                                 \
    if (value < min || value > max)                      \
    {                                                    \
      return false;                                      \
    }                                                    \
  }while(0)


#define READ_BOOL(stream, value) READ_BITS(stream, value, 1)

#define READ_FLOAT                  SERIALIZE_FLOAT
#define READ_UINT32                 SERIALIZE_UINT32
#define READ_UINT64                 SERIALIZE_UINT64
#define READ_DOUBLE                 SERIALIZE_DOUBLE
#define READ_BYTES                  SERIALIZE_BYTES
#define READ_STRING                 SERIALIZE_STRING
#define READ_ALIGN                  SERIALIZE_ALIGN
#define READ_CHECK                  SERIALIZE_CHECK
#define READ_OBJECT                 SERIALIZE_OBJECT
#define READ_ADDRESS                SERIALIZE_ADDRESS
#define READ_INT_RELATIVE           SERIALIZE_INT_RELATIVE
#define READ_ACK_RELATIVE           SERIALIZE_ACK_RELATIVE
#define READ_SEQUENCE_RELATIVE      SERIALIZE_SEQUENCE_RELATIVE





  // write macros corresponding to each serialize_*. useful when you want separate read and write functions for some reason.

#define WRITE_BITS( stream, value, bits )                     \
do                                                            \
{                                                             \
    gg_assert( bits > 0 );                                 \
    gg_assert( bits <= 32 );                               \
    u32 uint32_value = (u32) value;                 \
    if ( !stream.SerializeBits( uint32_value, bits ) )        \
    {                                                         \
        return false;                                         \
    }                                                         \
} while (0)

#define WRITE_INT( stream, value, min, max )                  \
do                                                            \
{                                                             \
    gg_assert( min < max );                                \
    gg_assert( value >= min );                             \
    gg_assert( value <= max );                             \
    s32 int32_value = (s32) value;                    \
    if ( !stream.SerializeInteger( int32_value, min, max ) )  \
    {                                                         \
        return false;                                         \
    }                                                         \
} while (0)


#define WRITE_BOOL(stream, value) WRITE_BITS(stream, value, 1)

#define WRITE_FLOAT                  SERIALIZE_FLOAT
#define WRITE_DOUBLE                 SERIALIZE_DOUBLE
#define WRITE_UINT32                 SERIALIZE_UINT32
#define WRITE_UINT64                 SERIALIZE_UINT64
#define WRITE_BYTES                  SERIALIZE_BYTES
#define WRITE_STRING                 SERIALIZE_STRING
#define WRITE_ALIGN                  SERIALIZE_ALIGN
#define WRITE_CHECK                  SERIALIZE_CHECK
#define WRITE_OBJECT                 SERIALIZE_OBJECT
#define WRITE_ADDRESS                SERIALIZE_ADDRESS
#define WRITE_INT_RELATIVE           SERIALIZE_INT_RELATIVE
#define WRITE_ACK_RELATIVE           SERIALIZE_ACK_RELATIVE
#define WRITE_SEQUENCE_RELATIVE      SERIALIZE_SEQUENCE_RELATIVE

}// namepsace GG




  // Helper macro to define virtual serialize functions
  // helps avoid writing boilerplate code
//#define GG_VIRTUAL_SERIALIZE_FUNCTIONS()                                                       \
//  bool SerializeInternal( class GG::ReadStream & stream ) { return Serialize( stream ); };     \
//  bool SerializeInternal( class GG::WriteStream & stream ) { return Serialize( stream ); };    \
//  bool SerializeInternal( class GG::MeasureStream & stream ) { return Serialize( stream ); };


/*
  Start definition of a new message factory
  This is a helper macro to make declaring your own message factory class easier

  factory_class The name of the message factory class to generate
  num_message_types The number of message types for this factory
  See Test.h for an example of usage.
*/
#define GG_MESSAGE_FACTORY_START(factory_class, num_message_types)                                 \
  class factory_class : public GG::MessageFactory                                                  \
  {                                                                                                \
  public:                                                                                          \
    factory_class(GG::Allocator & allocator) : MessageFactory(allocator, num_message_types) {}     \
      GG::Message * CreateMessageInternal(s32 type)                                                \
    {                                                                                              \
      GG::Message * message;                                                                       \
      GG::Allocator & allocator = GetAllocator();                                                  \
      (void)allocator;                                                                             \
      switch (type)                                                                                \
      {                                                                                            \


/*
  Add message type to a message factory
  message_type - The message type value. This is typically an enum value
  message_class - The message class to instantiate when a message of this type is created
*/
#define GG_DECLARE_MESSAGE_TYPE(message_type, message_class)    \
 case message_type:                                             \
   message = GG_NEW(allocator, message_class);                  \
   if (!message)                                                \
     return nullptr;                                            \
   SetMessageType(message, message_type);                       \
   return message;                                             

/**
    Finish the definition of a new message factory.
    This is a helper macro to make declaring your own message factory class easier.
    See Test.h for an example of usage.
 */
#define GG_MESSAGE_FACTORY_FINISH()      \
                                         \
                default: return nullptr; \
            }                            \
        }                                \
    };

