#pragma once
#include "IServer.h"
#include "Channel/Channel.h"
#include "Config/ClientServerConfig.h"


namespace GG
{
  struct ClientServerConfig;
  struct reliable_endpoint_t;
  class Allocator;
  class Adapter;
  class MessageFactory;
  class NetworkSimulator;
  class Connection;

  // common functionality between all server implementations
  class BaseServer : public IServer
  {
  public:
    BaseServer(Allocator& allocator, const ClientServerConfig& config, Adapter& adapter, f64 time);
    ~BaseServer();

    void SetContext(void* context);
    void Start(s32 maxClients);
    void Stop();
    void AdvanceTime(f64 time);
    bool IsRunning() const;
    s32 GetMaxClients() const;
    f64 GetTime() const;
    void SetLatency(f32 milliseconds);
    void SetJitter(f32 milliseconds);
    void SetPacketLoss(f32 percent);
    void SetDuplicates(f32 percent);
    Message* CreateMessage(s32 clientIndex, s32 type);
    uint8_t* AllocateBlock(s32 clientIndex, s32 bytes);
    void AttachBlockToMessage(s32 clientIndex, Message* message, u08* block, s32 bytes);
    void FreeBlock(s32 clientIndex, u08* block);
    bool CanSendMessage(s32 clientIndex, s32 channelIndex) const;
    void SendMessage(s32 clientIndex, s32 channelIndex, Message* message);
    Message* ReceiveMessage(s32 clientIndex, s32 channelIndex);
    void ReleaseMessage(s32 clientIndex, Message* message);
    void GetNetworkInfo(s32 clientIndex, NetworkInfo& info) const;

  protected:

    u08* GetPacketBuffer();
    void* GetContext();
    Adapter& GetAdapter();
    Allocator& GetGlobalAllocator();
    MessageFactory& GetClientMessageFactory(s32 clientIndex);
    NetworkSimulator* GetNetworkSimulator();
    reliable_endpoint_t* GetClientEndpoint(s32 clientIndex);
    Connection& GetClientConnection(s32 clientIndex);
    virtual void TransmitPacketFunction(s32 clientIndex, u16 packetSequence, u08* packetData, s32 packetBytes) = 0;
    virtual s32 ProcessPacketFunction(s32 clientIndex, u16 packetSequence, u08* packetData, s32 packetBytes) = 0;
    static void StaticTransmitPacketFunction(void* context, s32 index, u16 packetSequence, u08* packetData, s32 packetBytes);
    static s32 StaticProcessPacketFunction(void* context, s32 index, u16 packetSequence, u08* packetData, s32 packetBytes);
    static void* StaticAllocateFunction(void* context, u64 bytes);
    static void StaticFreeFunction(void* context, void* pointer);

  private:
    ClientServerConfig m_config;                        // base client/server config
    f64 m_time;                                         // current server time in seconds
    s32 m_maxClients;                                   // max number of clients supported
    bool m_running;                                     // true if server is currently running.  ex after "Start" is called, before "Stop"
    Allocator* m_allocator;                             // allocator passed in to constructor
    Adapter* m_adapter;                                 // adapter specifies the allocator to use and the message factory class
    void* m_context;                                    // optional serialization context
    u08* m_globalMemory;                                // the block of memory backing the global allocator.  Allocated with m_allocator
    u08* m_clientMemory[MAX_CLIENTS];                   // the block of memory back the per-client allocators.  Allocated with m_allocator
    Allocator* m_globalAllocator;                       // Used for allocations that don't belong to a specific client
    Allocator* m_clientAllocator[MAX_CLIENTS];          // array of per-client allocator.  These are used for allocations related to connected clients
    MessageFactory* m_clientMessageFactory[MAX_CLIENTS];// array of per-client message factories.  This silos message allocations per-client slot
    Connection* m_clientConnection[MAX_CLIENTS];        // array of per-client connection classes.  This is how messages are exchanged with clients
    reliable_endpoint_t* m_clientEndpoint[MAX_CLIENTS]; // array of per-client reliable endpoints
    NetworkSimulator* m_networkSimulator;               // the network simulator used to simulate packet loss, jitter, latency, ect
    u08* m_packetBuffer;                                // buffer used when writing packets
  };
}// namespace GG
