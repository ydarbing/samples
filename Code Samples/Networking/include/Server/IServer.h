#pragma once

namespace GG
{
  class Message;
  // Network information for a connection
  // contains statistics like round trip time (RTT), packet loss percentage, bandwidth estimates, number of packets sent/received/acked
  struct NetworkInfo
  {
    f32 RTT;               // round trip time estimate (milliseconds)
    f32 packetLoss;        // packet loss percent
    f32 sentBandwidth;     // sent bandwidth (kbps)
    f32 receivedBandwidth; // received bandwidth (kbps)
    f32 ackedBandwidth;    // acked bandwidth (kbps)
    u64 numPacketsSent;    // number of packets sent
    u64 numPacketsReceived;// number of packets received
    u64 numPacketsAcked;   // number of packets acked
  };

  class IServer
  {
  public:
    virtual ~IServer() {}
    // set context for reading and writing packets
    // This is optional, it lets you pass in a pointer to some structure that you want to have available when reading and writing packets via Stream::GetContext
    // Typical use case is to pass in an array of min/max ranges for values determined by some data that is loaded from a toolchain vs. being known at compile time
    // if you use a context, make sure the same context data is set on client and server and includes a checksum of the context data in the protocol ID
    virtual void SetContext(void* context) = 0;
    // start server and allocate client slots
    // each client that connects to this server occupies one of the client slots allocated by this function
    // maxClients - number of client slots to allocate. must be [1, maxClients]
    virtual void Start(s32 maxClients) = 0;
    // stop the server and free client slots
    virtual void Stop() = 0;
    // disconnect a client at a specific client index
    // clientIndex - index of the client to disconnect in range [0, maxClients - 1], where maxClients is the number of client slots allocated in Server::Start
    virtual void DisconnectClient(s32 clientIndex) = 0;
    // Disconnect all clients from the server
    // client slots remain allocated as per last call to Server::Start, they are simply made available for new clients to connect
    virtual void DisconnectAllClients() = 0;
    // send packets to connected clients
    // this function drives the sending of packets that transmit messages to clients
    virtual void SendPackets() = 0;
    // receive packets to connected clients
    // this function drives the processing of messages included in packets received from connected clients
    virtual void ReceivePackets() = 0;
    // advance server time
    // call this at the end of each frame to advance the server time forward
    // IMPORTANT:: Please us a double to maintain sufficient accuracy as time increases
    virtual void AdvanceTime(f64 time) = 0;
    // The server is running after Server::Start is called.  Not running before Server::Start is called and after Server::Stop is called
    virtual bool IsRunning()const = 0;
    // get max number of clients that can connect to the server
    // return - max nunmber of clients that can connect to the server/ the number of client slots
    virtual s32 GetMaxClients()const = 0;
    // Check if a client is connected to a client slot
    // clientIndex - index of client in range [0, maxClients - 1] where maxClients is value passed into the last call of Server::Start
    virtual bool IsClientConnected(s32 clientIndex)const = 0;
    // get number of clients currently connected to the server
    virtual s32 GetNumConnectedClients()const = 0;
    // Get current server time
    virtual f64 GetTime()const = 0;
    // Create a message of the specified type for a specific client
    // clientIndex - index of the client this message belongs to.  determines which client heap is used to allocate the message
    // type - type of message to create.  The message type corresponds to the message factory created by the adapter set on the server
    virtual Message* CreateMessage(s32 clientIndex, s32 type) = 0;
    // Helper function to allocate a data block
    // Used to create blocks of data to attach to block messages.  See BlockMessage for details
    // clientIndex - the index of the client this message belongs to.  Determines which client heap is used to allocate the data
    // bytes - number of bytes to allocate
    // return - pointer to the data block.  This must be attached to a message via Client::AttachBlockMessage or freed via Client::FreeBlock
    virtual u08* AllocateBlock(s32 clientIndex, s32 bytes) = 0;
    // Attach data block to message
    // clientIndex - index of the client this block belongs to
    // message - message to attach the block to. This message must be derived from BlockMessage
    // block - pointer to the block of data to attach.  Must be created via Client::AllocateBlock
    // bytes - size of the block of data in bytes
    virtual void AttachBlockToMessage(s32 clientIndex, Message* message, u08* block, s32 bytes) = 0;
    // Free a block of memory
    // clientIndex - index of the client this block belongs to
    // block - the block of memory created by Client::AllocateBlock
    virtual void FreeBlock(s32 clientIndex, u08* block) = 0;
    // Can we send a message to a particular client on a channel?
    // clientIndex - index of client to send a message to
    // channelIndex - channel index in range [0, NUM_CHANNELS -1]
    // return - true if a message can be sent over the channel, false otherwise
    virtual bool CanSendMessage(s32 clientIndex, s32 channelIndex) const = 0;
    // Send a message to a client of a channel
    // clientIndex - index of the client to send a message to
    // channelIndex - channel index in range [0, NUM_CHANNELS -1]
    // message - the message to send
    virtual void SendMessage(s32 clientIndex, s32 channelIndex, Message* message) = 0;
    // receive message froma client over a channel
    // clientIndex -index of the client to receive messages from
    // channelIndex - channel index in range [0, NUM_CHANNELS - 1]
    // return - message received, nullptr if no message is available.  
    // MAKE sure to release this message by calling Server::ReleaseMessage
    virtual Message* ReceiveMessage(s32 clientIndex, s32 channelIndex) = 0;
    // Release a message
    // Call this for messagess received by Server::ReceiveMessage
    // clientIndex - index of the client that the message belongs to
    // message - the message to release
    virtual void ReleaseMessage(s32 clientIndex, Message* message) = 0;
    // Get client network info
    // Call this to receive info about the client connection. ex RTT, packet loss %, num packets sent, ect
    // clientIndex - index of the client
    // info - struct to be filled with network info [out]
    virtual void GetNetworkInfo(s32 clientIndex, NetworkInfo& info) const = 0;
    // connect a loopback client
    // This allows you to have local clients connected to a server, ex. integrated server or single player
    // clientIndex - index of the client
    // clientID - unique client ID
    // userData - optional user data for this client.  nullptr if not used
    virtual void ConnectLoopbackClient(s32 clientIndex, u64 clientID, const u08* userData) = 0;
    // Disconnect a loopback client
    // loopback clients are not disconnected by regular Disconnect or DisconnectAllClient calls.  You need to call this function instead
    // clientIndex - index of client to disconnect.  Must already be a connected loopback client
    virtual void DisconnectLoopbackClient(s32 clientIndex) = 0;
    // check if this is a loopback client
    // clientIndex - true if client is a connected loopback client.
    virtual bool IsLoopbackClient(s32 clientIndex) const = 0;
    // process loopback packet
    // Use this to pass packets from a client directly to the loopback client slot on the server
    // packetData - packet data to process
    // packetBytes - number of bytes of packet data
    // packetSequence - packet sequence number
    virtual void ProcessLoopbackPacket(s32 clientIndex, const u08* packetData, s32 packetBytes, u64 packetSequence) = 0;
  };
}// namespace GG