#pragma once
#include "Server/BaseServer.h"
#include "Address.h"

namespace GG
{
  struct netcode_server_t;

  // Dedicated Server implementation
  class Server : public BaseServer
  {
  public:
    Server(Allocator& allocator, const u08 privateKey[], const Address& address, const ClientServerConfig& config, Adapter& adapter, f64 time);

    ~Server();
    void Start(s32 maxClients) override;
    void Stop() override;
    void DisconnectClient(s32 clientIndex) override;
    void DisconnectAllClients() override;
    void SendPackets() override;
    void ReceivePackets() override;
    void AdvanceTime(f64 time) override;
    bool IsClientConnected(s32 clientIndex) const override;
    s32 GetNumConnectedClients() const override;
    void ConnectLoopbackClient(s32 clientIndex, u64 clientID, const u08* userData) override;
    void DisconnectLoopbackClient(s32 clientIndex) override;
    bool IsLoopbackClient(s32 clientIndex) const override;
    void ProcessLoopbackPacket(s32 clientIndex, const u08* packetData, s32 packetBytes, u64 packetSequence) override;
    const Address& GetAddress()const;

  private:
    void TransmitPacketFunction(s32 clientIndex, u16 packetSequence, u08* packetData, s32 packetBytes) override;
    s32 ProcessPacketFunction(s32 clientIndex, u16 packetSequence, u08* packetData, s32 packetBytes) override;
    void ConnectDisconnectCallbackFunction(s32 clientIndex, s32 connected);
    void SendLoopbackPacketCallbackFunction(s32 clientIndex, const u08* packetData, s32 packetBytes, u64 packetSequence);

    static void StaticConnectDisconnectCallbackFunction(void* context, s32 clientIndex, s32 connected);
    static void StaticSendLoopbackPacketCallbackFunction(void* context, s32 clientIndex, const u08* packetData, s32 packetBytes, u64 packetSequence);
  private:
    ClientServerConfig m_config;
    netcode_server_t* m_server;
    Address m_address;      // original address passes to constructor
    Address m_boundAddress; // address after socket bind ex valid port
    u08 m_privateKey[KEY_BYTES];
  };
}// namespace GG
