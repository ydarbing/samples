#pragma once
#include "NetworkPrecompiled.h"
#include "Channel/Channel.h"

namespace GG
{
  // Configures connection properties and the set of channels for sending and receiving messages
  // specifies the max packet size to generate, the number of message channels and per-channel configuration data.  see ChannelConfig for details
  // Typically configured as part of a ClientServerConfig which is passed into Client and Server constructors
  struct ConnectionConfig 
  {
    s32 numChannels;                   // number of message channels in [1, MAX_CHANNELS].  Each message channel must have a corresponding configuration below
    s32 maxPacketSize;                 // max size (in bytes) of packets generated to transmit messages between client and server
    ChannelConfig channel[MAX_CHANNELS];// per-channel configuration.  See ChannelConfig for details

    ConnectionConfig()
    {
      numChannels = 1;
      maxPacketSize = 8 * 1024;
    }
  };
}// namespace GG
