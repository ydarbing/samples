#pragma once
#include "NetworkPrecompiled.h"
#include "ConnectionConfig.h"


namespace GG
{
  // configuration shared between client and server
  // Passed to Client and Server constructors to configure their behavior
  // Make sure that the message configuration is identical between client and server
  struct ClientServerConfig : public ConnectionConfig
  {
    u64 protocolID;                 // clients can only connect to servers with the same protocol ID.  Use this for versioning
    s32 timeout;                    // Timeout value in seconds.  Set to negative value to disable timeouts (for debugging only)
    s32 clientMemory;               // memory allocated inside Client for packets, messages and stream allocations (bytes)
    s32 serverPerClientMemory;      // memory allocated inside Server for packets, messages and stream allocations per-client (bytes)
    s32 serverGlobalMemory;         // memory allocated inside Server for global connection request and challenge response packets (bytes)
    s32 maxSimulatorPackets;        // max number of packets that can be stored in the network simulator.  Addional packets are dropped
    s32 fragmentPacketsAbove;       // packets above this size (in bytes) are split apart into fragments and reassembled on the other side
    s32 packetFragmentSize;         // size pf each packet fragment (in bytes)
    s32 maxPacketFragments;         // max number of fragments a packet can be split up into
    s32 packetReassemblyBufferSize; // number of packet entries in the fragmentation reassembly buffer
    s32 ackedPacketsBufferSize;     // number of packet entries in the acked packet buffer.  Consider your packet send rate and aim to have at least a few seconds worth of entries
    s32 receivedPacketsBufferSize;  // number of packet entries in the received packet sequence buffer. Consider your packet send rate and aim to have at least a few seconds worth of entries
    bool networkSimulator;          // true when a network simulator is created for simulating latency, jitter, packet loss and duplicates

    ClientServerConfig()
    {
      protocolID = 0;
      timeout = GG_DEFAULT_TIMEOUT;
      clientMemory = 10 * 1024 * 1024;
      serverPerClientMemory = 10 * 1024 * 1024;
      serverGlobalMemory = 10 * 1024 * 1024;
      maxSimulatorPackets = 4 * 1024;
      fragmentPacketsAbove = 1024;
      packetFragmentSize = 1024;
      maxPacketFragments = (s32)ceil(maxPacketSize / packetFragmentSize);
      packetReassemblyBufferSize = 64;
      ackedPacketsBufferSize = 256;
      receivedPacketsBufferSize = 256;
      networkSimulator = true;
    }
  };
}// namespace GG
