#pragma once

namespace GG
{
  const s32 MAX_CLIENTS = 64;                      // max number of clients
  const s32 MAX_CHANNELS = 64;                     // max number of message channels, if you need less than 64 channels per-packet, reducing this will save memory
  const s32 KEY_BYTES = 32;                        // DO NOT CHANGE. Size of encryption key for dedicated client/server in bytes.  Must be equal to key size for libsodium encryption primitive. 
  const s32 CONNECT_TOKEN_BYTES = 2048;            // Size of the encrypted connect token data return from the matchmaker.  must equal size of NETCODE_CONNECT_TOKEN_BYTE (2048)
  const s32 CONSERVATIVE_MESSAGE_HEADER_BITS = 32; // Conservative number of bits per-message header
  const s32 CONSERVATIVE_FRAGMENT_HEADER_BITS = 64;// Conservative number of bits per-fragment header
  const s32 CONSERVATIVE_CHANNEL_HEADER_BITS = 32; // Conservative number of bits per-channel header
  const s32 CONSERVATIVE_PACKET_HEADER_BITS = 16;  // Conservative number of bits per-packet header
  const u32 SERIALIZE_CHECK_VALUE = 0x12345678;    // Value written to the stream for serialize checks.  See WriteStream::SerailizeCheck and ReadStream::SerializeCheck
                                                   //  const u32 SerializeCheckValue = 0x12345678; // The value written to the stream for serialize checks. See WriteStream::SerializeCheck and ReadStream::SerializeCheck

} // namespace GG