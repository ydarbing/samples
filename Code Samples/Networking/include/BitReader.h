#pragma once

#include <stdint.h>// u32, ...

namespace GG
{

  class BitReader
  {
  public:
    BitReader(const void * data, s32 bytes);
    // if bitreader read given bits would it read past the end of the buffer;
    bool WouldReadPastEnd(s32 bits)const;
    // read bits from buffer
    u32 ReadBits(s32 bits);
    // make sure this call corresponds with BitWriter::writeAlign
    // Skips ahead to next aligned byte index
    bool ReadAlign();
    // read bytes from bitpacked data
    void ReadBytes(u08* data, s32 bytes);
    // How many align bits would be read if we were to read an align right now
    // returns 0 - 7: 0 is zero bits required to align and 7 is worst case
    s32 GetAlignBits()const;
    // How many bits have been read so far
    s32 GetBitsRead()const;
    // number of bits still available to read
    s32 GetBitsRemaining()const;


  private:
    const u32* m_data; // bitpacked data that is read as dword array
    u64 m_scratch;     // new data is read in 32 bits at a top to the left of this buffer, data is read off to the right
    s32 m_numBits;          // number of bits to read in buffer (can't really know this so it's actually m_numBytes * 8)
    s32 m_numBytes;         // number of bytes to read in buffer
    s32 m_numWords;         // number of words in buffer.  Rounded up if necessary
    s32 m_bitsRead;         // number of bits read so far
    s32 m_scratchBits;      // number of bits currently in scratch value, if user wants to read more bits than this, need to get another dword from memory
    s32 m_wordIndex;        // index of next word to read from memory
  };

}// namespace GG
