#pragma once


#if GG_PLATFORM == GG_PLATFORM_WINDOWS
#define NOMINMAX
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <winsock2.h>
#include <ws2tcpip.h>
#include <ws2ipdef.h>
#pragma comment( lib, "WS2_32.lib" )

#ifdef SetPort // get rid of stupid printer port macro from windows.h
#undef SetPort
#endif // #ifdef SetPort
#elif GG_PLATFORM == GG_PLATFORM_MAC || GG_PLATFORM == GG_PLATFORM_UNIX
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <fcntl.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#else
#error unknown platform!
#endif

namespace GG
{
  const s32 MAX_ADDRESS_LENGTH = 256; // Maximum length of an address when converted to a string (includes terminating NULL)

  enum AddressType
  {
    E_ADDRESS_NONE,  // no address yet
    E_ADDRESS_IPV4, // ex: "146.95.129.237"
    E_ADDRESS_IPV6  // ex: "5635:4a08:b543:ae31:89d8:3226:b92c:cbba"
  };


  // An IP address and port number
  // supports both IPv4 and IPv6 addresses
  // identifies where a packet came from and where a packet should be sent
  class Address
  {

  public:
    Address();
    Address(u08 a, u08 b, u08 c, u08 d, u16 port = 0);
    Address(const u08 address[], u16 port = 0);
    Address(u16 a, u16 b, u16 c, u16 d, u16 e, u16 f, u16 g, u16 h, u16 port = 0);
    Address(const u16 address[], u16 port = 0);
    explicit Address(const c08 * address);
    explicit Address(const c08 * address, u16 port);
    void Clear();
    const u08 * GetAddress4() const;
    const u16 * GetAddress6() const;
    void SetPort(u16 port);
    u16 GetPort() const;
    AddressType GetType() const;
    const c08* ToString(c08 buffer[], s32 bufferSize) const;
    bool IsValid() const;
    bool IsLoopback() const;
    bool IsLinkLocal() const;
    bool IsSiteLocal() const;
    bool IsMulticast() const;
    bool IsGlobalUnicast() const;
    bool operator ==(const Address & other) const;
    bool operator !=(const Address & other) const;

  protected:
    void Parse(const c08* address);

  private:
    AddressType m_type;  // type of address.  IPv4 or IPv6
    union
    {
      u08 ipv4[4];
      u16 ipv6[8];
    } m_address; 

    u16 m_port; // IP port, valid for both IPv4 or IPv6
  };
}// namespace GG

