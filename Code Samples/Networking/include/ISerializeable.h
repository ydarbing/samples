#pragma once

namespace GG
{
  // Interface for an object that knows how to read, write and measure how many bits it would take up in a bit stream
// Important: Instead of overriding the serialize virtual methods method directly, use the YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS macro in your derived class to override and redirect them to your templated serialize method
//            This way you can implement read and write for your messages in a single method and the C++ compiler takes care of generating specialized read, write and measure implementations for you.
  class ISerializeable
  {
  public:
    virtual ~ISerializeable() {}

    // Writes the object to a bitstream
    virtual bool SerializeInternal(class StreamWrite& stream) = 0;
    // Reads the object from a bitstream
    virtual bool SerializeInternal(class StreamRead& stream) = 0;
    // Measures how many bits the object would take if it were written to a bitstream
    virtual bool SerializeInternal(class StreamMeasure& stream) = 0;
  };
  

}// namespace GG

  // Helper macro to define virtual serialze functions for read, write and measure that call into the templated serialize function
  // Helps avoid writing boilerplate code, nice when you have lots of hand coded message types
#define GG_VIRTUAL_SERIALIZE_FUNCTIONS()                                                     \
  bool SerializeInternal(class GG::StreamRead & stream) { return Serialize(stream); };       \
  bool SerializeInternal(class GG::StreamWrite & stream) { return Serialize(stream); };      \
  bool SerializeInternal(class GG::StreamMeasure & stream) { return Serialize(stream); };
