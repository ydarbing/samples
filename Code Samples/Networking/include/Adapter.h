#pragma once

namespace GG
{
  class Allocator;
  class MessageFactory;

  // Specifies the message factory and callbacks for clients and servers
  // An instance of this class is passed into the client and server constructors
  // you can share the same adapter across a client/server pair if you have local multiplayer ex. loopback
  class Adapter
  {
  public:
    virtual ~Adapter() {}

    // Override this function to specify your own custom allocator class
    // allocator - the base allocator that must be used to allocate your allocator instance 
    // memory - the block of memory backing your allocator
    // bytes - number of bytes of memory available to your allocator
    // return - pointer to the allocator instance you created
    virtual Allocator* CreateAllocator(Allocator& allocator, void* memory, size_t bytes);
    /*
     *  you must override this function to create a message factory used by the client and server
     *  allocator - the allocator must be used to create your message factory instance via GG_NEW
     *  return - pointer to message factory you created
     */
    virtual MessageFactory* CreateMessageFactory(Allocator& allocator);
    /*
     *  Override this callback to process packets sent from client to server over loopback
     *  clientIndex - client index in range [0, MAX_CLIENTS -1]
     *  packetData - packet data (raw_ to be sent to the server
     *  packetByes - number of packet bytes in the server
     *  packetSequence - the sequence number of the packet
     *  
     *  See Client::ConnectLoopback
     */
    virtual void ClientSendLoopbackPacket(s32 clientIndex, const u08* packetData, s32 packetBytes, u64 packetSequence);
    /*
     *  Override this callback to process packets sent from client to server over loopback
     *  clientIndex - client index in range [0, MAX_CLIENTS -1]
     *  packetData - packet data (raw_ to be sent to the server
     *  packetByes - number of packet bytes in the server
     *  packetSequence - the sequence number of the packet
     *  
     *  See Server::ConnectLoopback
     */
    virtual void ServerSendLoopbackPacket(s32 clientIndex, const u08* packetData, s32 packetBytes, u64 packetSequence);
    /*
     * override this to get a callback when a client connects to the server
     */
    virtual void OnServerClientConnected(s32 clientIndex);
    /*
     * override this to get a callback when a client disconnects from the server
     */
    virtual void OnServerClientDisconnected(s32 clientIndex);
  };
}// namespace GG