#pragma once
#include "Allocator\Allocator.h"

namespace GG
{
  // simple bit array class
  class BitArray
  {
  public:
    // bit array constructor
    // allocator - allocator to use
    // size - number of bits in array.  All bits are initialized to zero
    BitArray(Allocator& allocator, s32 size);
    ~BitArray();
    // set all bits to zero
    void Clear();
    // set a bit to 1
    void SetBit(s32 index);
    // clear a bit to 0
    void ClearBit(s32 index);
    // get value of specific bit
    // return - 1 if bit is set, 0 if not set
    u64 GetBit(s32 index)const;
    // get size of bit array in number of bits
    s32 GetSize() const;

  private:
    Allocator* m_allocator;
    s32 m_size;            // size of bit array in bits
    s32 m_bytes;           // size of bit array in bytes
    u64* m_data;           // data backing the bit array is an array of 64 bit integer values

    BitArray(const BitArray& other);
    BitArray& operator=(const BitArray& other);
  };

}