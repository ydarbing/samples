#pragma once
#include <stdint.h>// u32, ...

namespace GG
{
  class BitWriter
  {
  public:
    // create bit writer to write to specified buffer
    BitWriter(void* data, s32 bytes);
    // writes bits to buffer, biits are written as is with no padding. 
    // assert if it writes past buffer
    // When finished writing to buffer call PushBits otherwise the last dword of data will no get pushed
    // Value: Integer value to write to the buffer.Must be in[0, (1 << bits) - 1]
    // Bits:  Bits The number of bits to encode in[1, 32]
    void WriteBits(u32 value, s32 bits);
    // write alignment to the bit stream, padding zeros so the bit index becomes a multiple of 8
    // usefor when writing data to a packet that should be byte aligned like a string or an array of bytes
    // If the current bit index is already a multiple of 8 nothing happens
    void WriteAlign();
    // Write an array of bytes to bit stream
    // use this when copying a large block of data into your bit stream
    // This is faster than writing each byte using writebits because it aligns to byte index and copies into buffer without bitpacking
    void WriteBytes(const u08* dataToWrite, s32 numBytesToWrite);
    // Flush any remaining bits to memory
    // Call this once after you've finished writing bits to flush the last dword of scratch to memory!
    void FlushBits();
    // How many bits would be writtne if we were to write an align right now?
    // returns result in [0,7] where 0 is zero bits required to align (already aligned) and 7 is worst case 
    s32 GetAlignBits() const;
    // How many bits have we written to the buffer so far?
    s32 GetBitsWritten() const;
    //  How many bits are still avaible to write?
    //  Ex. if buffer size is 4, there are 32 bits available to write, if we have already written 10 bits then 22 are still available to write.
    s32 GetBitsAvailable() const;
    // Get pointer to data written by bit writer
    const u08* GetData() const;
    // Number of bytes flushed to memory
    // This is effectively the size of the packet that should be sent after finished bitpacking values with this class
    // The returned value is not always a multiple of 4, even though we flush dwords to memory. Won't miss any data in this case because the order of bits written is designed to work with little endian memory
    // IMPORTANT: Call BitWriter::FlushBits before calling this function, if you dont you risk missiout that last dword of data
    s32 GetBytesWritten() const;

  private:
    u32* m_data;   // buffer that is writen to, write dwords at a time
    u64 m_scratch; // where bits are written (right to left) 64 bits for overflow.  Once # of bits in scratch is >= 32 the lower 32 bits are pushed to memory
    s32 m_numBits;      // number of bits in buffer  (his is equivalent to the size of the buffer in bytes multiplied by 8) Note: size of buffer must be a multiple of 4
    s32 m_numWords;     // number of words in buffer (This is equivalent to the size of the buffer in bytes divided by 4)
    s32 m_bitsWritten;  // bits written so far
    s32 m_wordIndex;    // current word index.  Next word pushed to memory will be at the index in m_data
    s32 m_scratchbits;  // number of bits in scratch
  };

}// namespace GG
