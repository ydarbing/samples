#pragma once

#include "Channel\Channel.h" // ChannelConfig
#include "Allocator/Allocator.h"
#include "MessageFactory.h"
#include "Stream/StreamWrite.h"
#include "Stream/StreamRead.h"
#include "Stream/StreamMeasure.h"

#include "Config\ConnectionConfig.h" //struct ConnectionConfig

namespace GG
{
  class Message;
  class Allocator;
  class MessageFactory;

  TODO("move this to cpp file")
  struct ConnectionPacket
  {
    s32 numChannelEntries;
    ChannelPacketData* channelEntry;
    MessageFactory* messageFactory;

    ConnectionPacket()
      : numChannelEntries(0), channelEntry(nullptr), messageFactory(nullptr)
    {
    }

    ~ConnectionPacket()
    {
      if (messageFactory)
      {
        for (s32 i = 0; i < numChannelEntries; ++i)
        {
          channelEntry[i].Free(*messageFactory);
        }
        GG_FREE(messageFactory->GetAllocator(), channelEntry);
        messageFactory = nullptr;
      }
    }

    bool AllocateChannelData(MessageFactory& messageFactory_, s32 numEntries)
    {
      gg_assert(numEntries > 0);
      gg_assert(numEntries <= MAX_CHANNELS);
      messageFactory = &messageFactory_;
      Allocator& allocator = messageFactory->GetAllocator();
      channelEntry = (ChannelPacketData*)GG_ALLOCATE(allocator, sizeof(ChannelPacketData) * numEntries);

      if (channelEntry == nullptr)
      {
        return false;
      }

      for (s32 i = 0; i < numEntries; ++i)
      {
        channelEntry[i].Initialize();
      }
      numChannelEntries = numEntries;
      return true;
    }

    template <typename Stream>
    bool Serialize(Stream& stream, MessageFactory& messageFactory_, const ConnectionConfig& connectionConfig)
    {
      const s32 numChannels = connectionConfig.numChannels;
      SERIALIZE_INT(stream, numChannelEntries, 0, connectionConfig.numChannels);
#if GG_DEBUG_MESSAGE_BUDGET
      gg_assert(stream.GetBitsProcessed() <= CONSERVATIVE_PACKET_HEADER_BITS);
#endif

      if (numChannelEntries > 0)
      {
        if (Stream::IsReading)
        {
          if (!AllocateChannelData(messageFactory_, numChannelEntries))
          {
            gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to allocate channel data (ConnectionPacket)\n");
            return false;
          }
          for(s32 i = 0; i < numChannelEntries; ++i)
          {
            gg_assert(channelEntry[i].messageFailedToSerialize == 0);
          }
        }
        for (s32 i = 0; i < numChannelEntries; ++i)
        {
          gg_assert(channelEntry[i].messageFailedToSerialize == 0);
          if (!channelEntry[i].SerializeInternal(stream, messageFactory_, connectionConfig.channel, numChannels))
          {
            gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to serialize channel %d\n", i);
            return false;
          }
        }

      }
      return true;
    }
    bool SerializeInternal(StreamRead& stream, MessageFactory& messageFactory_, const ConnectionConfig& connectionConfig)
    {
      return Serialize(stream, messageFactory_, connectionConfig);
    }

    bool SerializeInternal(StreamWrite& stream, MessageFactory& messageFactory_, const ConnectionConfig& connectionConfig)
    {
      return Serialize(stream, messageFactory_, connectionConfig);
    }

    bool SerializeInternal(StreamMeasure& stream, MessageFactory& messageFactory_, const ConnectionConfig& connectionConfig)
    {
      return Serialize(stream, messageFactory_, connectionConfig);
    }

  private:
    ConnectionPacket(const ConnectionPacket& other);
    const ConnectionPacket& operator=(const ConnectionPacket& other);
  };


  enum ConnectionErrorLevel
  {
    E_CONNECTION_ERROR_NONE = 0,          // no error
    E_CONNECTION_ERROR_CHANNEL,           // a channel is in an error state
    E_CONNECTION_ERROR_ALLOCATOR,         // allocator in an error state
    E_CONNECTION_ERROR_MESSAGE_FACTORY,   // message factory in an error state
    E_CONNECTION_ERROR_READ_PACKET_FAILED // failed to read packet, possibly received invalid packet
  };

  // sends and receives message across a set of user defined channels
  class Connection
  {
  public:
    Connection(Allocator& allocator, MessageFactory& messageFactory, const ConnectionConfig& connectionConfig, f64 time);
    ~Connection();
    void Reset();
    bool CanSendMessage(s32 channelIndex)const;
    void SendMessage(s32 channelIndex, Message* message, void* context = nullptr);
    Message* ReceiveMessage(s32 channelIndex);
    void ReleaseMessage(Message* message);
    bool GeneratePacket(void* context, u16 packetSequence, u08* packetData, s32 maxPacketBytes, s32& packetBytes);
    bool ProcessPacket(void* context, u16 packetSequence, const u08* packetData, s32 packetBytes);
    void ProcessAcks(const u16* acks, s32 numAcks);
    void AdvanceTime(f64 time);

    ConnectionErrorLevel GetErrorLevel() const;

  private:
    Allocator* m_allocator;
    MessageFactory* m_messageFactory;   // for creating and destroying messages
    ConnectionConfig m_connectionConfig;
    Channel* m_channel[MAX_CHANNELS];    // array of connection channels, size comes from m_connectionConfig.numChannels
    ConnectionErrorLevel m_errorLevel;  //
  };

  static s32 WritePacket(void* context, MessageFactory& messageFactory, const ConnectionConfig& connectionConfig, ConnectionPacket& packet, u08* buffer, s32 bufferSize);
  static bool ReadPacket(void* context, MessageFactory& messageFactory, const ConnectionConfig& connectionConfig, ConnectionPacket& packet, const u08* buffer, s32 bufferSize);
}//namespace GG
