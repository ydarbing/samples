#pragma once


#if defined(_MSC_VER) && !defined(_CRT_SECURE_NO_WARNINGS)
#define _CRT_SECURE_NO_WARNINGS
#endif

#ifdef _MSC_VER
#pragma warning( disable : 4127 )
#pragma warning( disable : 4244 )
#endif // #ifdef _MSC_VER


#include <chrono>
#include <thread>
#include <mutex>


#define GG_SERIALIZE_CHECKS 1


#include <stdlib.h>
#include <stddef.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <array>
#include <memory.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

// containers
#include <string>
//#include <vector>
//#include <list>


#include "GG_Networking.h"

#include "Assertion.h"

