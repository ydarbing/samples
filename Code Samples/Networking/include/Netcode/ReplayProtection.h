#pragma once

#define NETCODE_REPLAY_PROTECTION_BUFFER_SIZE 256
//extern const u32 NETCODE_REPLAY_PROTECTION_BUFFER_SIZE;

namespace GG
{
  struct netcode_replay_protection_t
  {
    u64 mostRecentSequence;
    u64 receivedPacket[NETCODE_REPLAY_PROTECTION_BUFFER_SIZE];
  };
  void netcode_replay_protection_reset(netcode_replay_protection_t* replayProtection);
  bool netcode_replay_protection_already_received(netcode_replay_protection_t* replayProtection, u64 sequence);
  void netcode_replay_protection_advance_sequence(netcode_replay_protection_t* replayProtection, u64 sequence);

}// namespace GG
