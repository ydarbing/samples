#pragma once
#include "Netcode\PacketQueue.h"
#include "Netcode\ConnectToken.h"

#define NETCODE_CONNECT_TOKEN_BYTES 2048
#define NETCODE_KEY_BYTES 32
#define NETCODE_MAC_BYTES 16
#define NETCODE_MAX_SERVERS_PER_CONNECT 32

#define NETCODE_CLIENT_STATE_CONNECT_TOKEN_EXPIRED              -6
#define NETCODE_CLIENT_STATE_INVALID_CONNECT_TOKEN              -5
#define NETCODE_CLIENT_STATE_CONNECTION_TIMED_OUT               -4
#define NETCODE_CLIENT_STATE_CONNECTION_RESPONSE_TIMED_OUT      -3
#define NETCODE_CLIENT_STATE_CONNECTION_REQUEST_TIMED_OUT       -2
#define NETCODE_CLIENT_STATE_CONNECTION_DENIED                  -1
#define NETCODE_CLIENT_STATE_DISCONNECTED                       0
#define NETCODE_CLIENT_STATE_SENDING_CONNECTION_REQUEST         1
#define NETCODE_CLIENT_STATE_SENDING_CONNECTION_RESPONSE        2
#define NETCODE_CLIENT_STATE_CONNECTED                          3

#define NETCODE_MAX_CLIENTS         256
#define NETCODE_MAX_PACKET_SIZE     1200

#define NETCODE_MAX_CONNECT_TOKEN_ENTRIES (NETCODE_MAX_CLIENTS * 8)



#define NETCODE_OK                  1
#define NETCODE_ERROR               0

#define NETCODE_NETWORK_NEXT        1

#define NETCODE_ADDRESS_NONE        0
#define NETCODE_ADDRESS_IPV4        1
#define NETCODE_ADDRESS_IPV6        2

#if NETCODE_NETWORK_NEXT
#define NETCODE_ADDRESS_NEXT        3
#endif // #if NETCODE_NETWORK_NEXT








// TODO : move this to inside a cpp file to hide them

#define NETCODE_SOCKET_ERROR_NONE                       0
#define NETCODE_SOCKET_ERROR_CREATE_FAILED              1
#define NETCODE_SOCKET_ERROR_SET_NON_BLOCKING_FAILED    2
#define NETCODE_SOCKET_ERROR_SOCKOPT_IPV6_ONLY_FAILED   3
#define NETCODE_SOCKET_ERROR_SOCKOPT_RCVBUF_FAILED      4
#define NETCODE_SOCKET_ERROR_SOCKOPT_SNDBUF_FAILED      5
#define NETCODE_SOCKET_ERROR_BIND_IPV4_FAILED           6
#define NETCODE_SOCKET_ERROR_BIND_IPV6_FAILED           7
#define NETCODE_SOCKET_ERROR_GET_SOCKNAME_IPV4_FAILED   8
#define NETCODE_SOCKET_ERROR_GET_SOCKNAME_IPV6_FAILED   7

#define NETCODE_CONNECTION_REQUEST_PACKET               0
#define NETCODE_CONNECTION_DENIED_PACKET                1
#define NETCODE_CONNECTION_CHALLENGE_PACKET             2
#define NETCODE_CONNECTION_RESPONSE_PACKET              3
#define NETCODE_CONNECTION_KEEP_ALIVE_PACKET            4
#define NETCODE_CONNECTION_PAYLOAD_PACKET               5
#define NETCODE_CONNECTION_DISCONNECT_PACKET            6
#define NETCODE_CONNECTION_NUM_PACKETS                  7



/*
  Move everything below this to source file
*/

#define NETCODE_SOCKET_IPV6         1
#define NETCODE_SOCKET_IPV4         2

#define NETCODE_CONNECT_TOKEN_NONCE_BYTES 24
#define NETCODE_CONNECT_TOKEN_PRIVATE_BYTES 1024
#define NETCODE_CHALLENGE_TOKEN_BYTES 300
#define NETCODE_USER_DATA_BYTES 256
#define NETCODE_MAX_PACKET_BYTES 1300
#define NETCODE_MAX_PAYLOAD_BYTES 1200
#define NETCODE_MAX_ADDRESS_STRING_LENGTH 256
#define NETCODE_PACKET_QUEUE_SIZE 256
//#define NETCODE_REPLAY_PROTECTION_BUFFER_SIZE 256 
#define NETCODE_CLIENT_MAX_RECEIVE_PACKETS 64
#define NETCODE_SERVER_MAX_RECEIVE_PACKETS ( 64 * NETCODE_MAX_CLIENTS )
#define NETCODE_CLIENT_SOCKET_SNDBUF_SIZE ( 256 * 1024 )
#define NETCODE_CLIENT_SOCKET_RCVBUF_SIZE ( 256 * 1024 )
#define NETCODE_SERVER_SOCKET_SNDBUF_SIZE ( 4 * 1024 * 1024 )
#define NETCODE_SERVER_SOCKET_RCVBUF_SIZE ( 4 * 1024 * 1024 )

#define NETCODE_VERSION_INFO_BYTES 13
#define NETCODE_VERSION_INFO ( (u08*) "NETCODE 1.02" )
#define NETCODE_PACKET_SEND_RATE 10.0
#define NETCODE_NUM_DISCONNECT_PACKETS 10


#ifndef NETCODE_ENABLE_TESTS
#define NETCODE_ENABLE_TESTS 1
#endif // #ifndef NETCODE_ENABLE_TESTS

#ifndef NETCODE_ENABLE_LOGGING
#define NETCODE_ENABLE_LOGGING 1
#endif // #ifndef NETCODE_ENABLE_LOGGING





namespace GG
{
  struct netcode_network_simulator_t;
  struct netcode_client_t;
  struct netcode_server_t;
  void* netcode_default_allocate_function(void* context, u64 bytes);
  void netcode_default_free_function(void* context, void* pointer);


  struct netcode_address_t
  {
    u08 type;
    union
    {
      u08 ipv4[4];
      u16 ipv6[8];
#ifdef NETCODE_NETWORK_NEXT
      u64 flow_id;
#endif
    } data;
    u16 port;
  };
  
  
  /***********************************************************************************************************/
  /*
                  Structs to be moved later
  */

  //netcode_connect_token_t;
  //netcode_socket_holder_t;
  //netcode_context_t;
  //netcode_replay_protection_t;
  //netcode_connect_token_entry_t ;
  //netcode_encryption_manager_t;

  typedef std::array<netcode_address_t, NETCODE_MAX_SERVERS_PER_CONNECT> ServerAddressArray;

  struct netcode_connect_token_t
  {
    u08 versionInfo[NETCODE_VERSION_INFO_BYTES];
    u64 protocolId;
    u64 createTimestamp;
    u64 expireTimestamp;
    u08 nonce[NETCODE_CONNECT_TOKEN_NONCE_BYTES];
    u08 privateData[NETCODE_CONNECT_TOKEN_PRIVATE_BYTES];
    s32 timeoutSeconds;
    s32 numServerAddresses;
    //netcode_address_t serverAddresses[NETCODE_MAX_SERVERS_PER_CONNECT];
    ServerAddressArray serverAddresses;
    u08 clientToServerKey[NETCODE_KEY_BYTES];
    u08 serverToClientKey[NETCODE_KEY_BYTES];
  };

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#if GG_PLATFORM == GG_PLATFORM_WINDOWS
  typedef u64 netcode_socket_handle_t;
#else
  typedef s32 netcode_socket_handle_t;
#endif
  struct netcode_socket_t
  {
    struct netcode_address_t address;
    netcode_socket_handle_t handle;
  };

  struct netcode_socket_holder_t
  {
    struct netcode_socket_t ipv4;
    struct netcode_socket_t ipv6;
  };
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  struct netcode_context_t
  {
    u08 writePacketKey[NETCODE_KEY_BYTES];
    u08 readPacketKey[NETCODE_KEY_BYTES];
  };
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
#define NETCODE_MAX_CONNECT_TOKEN_ENTRIES (NETCODE_MAX_CLIENTS * 8)
  struct netcode_connect_token_entry_t
  {
    f64 time;
    u08 mac[NETCODE_MAC_BYTES];
    netcode_address_t address;
  };
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define NETCODE_MAX_ENCRYPTION_MAPPINGS (NETCODE_MAX_CLIENTS * 4)
  struct netcode_encryption_manager_t
  {
    s32 numEncryptionMappings;
    s32 timeout[NETCODE_MAX_ENCRYPTION_MAPPINGS];
    f64 expireTime[NETCODE_MAX_ENCRYPTION_MAPPINGS];
    f64 lastAccessTime[NETCODE_MAX_ENCRYPTION_MAPPINGS];
    netcode_address_t address[NETCODE_MAX_ENCRYPTION_MAPPINGS];
    u08 sendKey[NETCODE_KEY_BYTES * NETCODE_MAX_ENCRYPTION_MAPPINGS];
    u08 receiveKey[NETCODE_KEY_BYTES * NETCODE_MAX_ENCRYPTION_MAPPINGS];
  };
  /***********************************************************************************************************/
  

  struct netcode_client_config_t
  {
    void* allocatorContext;
    void* (*allocate_function)(void*, u64);
    void(*free_function)(void*, void*);
    netcode_network_simulator_t* networkSimulator;
    void* callbackContext;
    void(*state_change_callback)(void*, s32, s32);
    void(*send_loopback_packet_callback)(void*, s32, const u08*, s32, u64);
    s32 overrideSendAndReceive;
    void(*send_packet_override)(void*, netcode_address_t*, const u08*, s32);
    s32(*receive_packet_override)(void*, netcode_address_t*, u08*, s32);
  };

  struct netcode_server_config_t
  {
    u64 protocolId;
    u08 privateKey[NETCODE_KEY_BYTES];
    void* callbackContext;
    void* allocatorContext;

    void* (*allocate_function)(void*, u64);
    void(*free_function)(void*, void*);
    netcode_network_simulator_t* networkSimulator;
    void(*connect_disconnect_callback)(void*, s32, s32);
    void(*send_loopback_packet_callback)(void*, s32, const u08*, s32, u64);
    s32 overrideSendAndReceive;
    void(*send_packet_override)(void*, netcode_address_t*, const u08*, s32);
    s32(*receive_packet_override)(void*, netcode_address_t*, u08*, s32);
  };


  // private structs
  
  //class Netcode
  //{
  //public:
    s32 netcode_init();
    void netcode_term();
    s32 netcode_parse_address(const c08* address_string_in, netcode_address_t* address);
    c08* netcode_address_to_string(netcode_address_t* address, c08* buffer);
    bool netcode_address_equal(netcode_address_t* a, netcode_address_t* b);
    void netcode_default_client_config(netcode_client_config_t* config);

    netcode_client_t* netcode_client_create_overload(const c08* address1, const c08* address2, const netcode_client_config_t* config, f64 time);
    netcode_client_t* netcode_client_create(const c08* address, const netcode_client_config_t* config, f64 time);
    void netcode_client_destroy(netcode_client_t* client);
    void netcode_client_connect(netcode_client_t* client, u08* connect_token);
    void netcode_client_update(netcode_client_t* client, f64 time);

    u64 netcode_client_next_packet_sequence(netcode_client_t* client);
    void netcode_client_send_packet(netcode_client_t* client, const u08* packet_data, s32 packetBytes);
    u08* netcode_client_receive_packet(netcode_client_t* client, s32* packet_bytes, u64* packetSequence);

    void netcode_client_free_packet(netcode_client_t* client, u08* packet);
    void netcode_client_disconnect(netcode_client_t* client);
    s32 netcode_client_state(netcode_client_t* client);
    s32 netcode_client_index(netcode_client_t* client);
    s32 netcode_client_max_clients(netcode_client_t* client);
    void netcode_client_connect_loopback(netcode_client_t* client, s32 clientIndex, s32 max_clients);
    void netcode_client_disconnect_loopback(netcode_client_t* client);
    void netcode_client_process_packet(netcode_client_t* client, netcode_address_t* from, u08* packet_data, s32 packet_bytes);
    bool netcode_client_loopback(netcode_client_t* client);
    void netcode_client_process_loopback_packet(netcode_client_t* client, const u08* packet_data, s32 packetBytes, u64 packetSequence);
    u16 netcode_client_get_port(netcode_client_t* client);

    s32 netcode_generate_connect_token(s32 num_server_addresses,
                                       const c08** publicServerAddresses,
                                       const c08** internalServerAddresses,
                                       s32 expire_seconds,
                                       s32 timeout_seconds,
                                       u64 client_id,
                                       u64 protocol_id,
                                       const u08* private_key, 
                                       u08* userData,
                                       u08* connect_token);


    netcode_server_t* netcode_server_create_overload(const c08* serverAddress1String, const c08* serverAddress2String, const netcode_server_config_t* config, f64 time);
    netcode_server_t* netcode_server_create(const c08* serverAddressString, const netcode_server_config_t* config, f64 time);
    
    void  netcode_default_server_config(netcode_server_config_t* config);
    void  netcode_server_destroy(netcode_server_t* server);
    void  netcode_server_start(netcode_server_t* server, s32 max_clients);
    void  netcode_server_stop(netcode_server_t* server);
    bool  netcode_server_running(netcode_server_t* server);
    s32   netcode_server_max_clients(netcode_server_t* server);
    void  netcode_server_update(netcode_server_t* server, f64 time);
    s32   netcode_server_client_connected(netcode_server_t* server, s32 client_index);
    u64   netcode_server_client_id(netcode_server_t* server, s32 client_index);
    void  netcode_server_disconnect_client(netcode_server_t* server, s32 client_index);
    void  netcode_server_disconnect_all_clients(netcode_server_t* server);
    u64   netcode_server_next_packet_sequence(netcode_server_t* server, s32 clientIndex);
    void  netcode_server_send_packet(netcode_server_t* server, s32 client_index, const u08* packet_data, s32 packet_bytes);
    u08*  netcode_server_receive_packet(netcode_server_t* server, s32 client_index, s32* packetBytes, u64* packet_sequence);
    void  netcode_server_free_packet(netcode_server_t* server, void* packet);
    s32   netcode_server_num_connected_clients(netcode_server_t* server);
    void* netcode_server_client_user_data(netcode_server_t* server, s32 client_index);
    void  netcode_server_process_packet(netcode_server_t* server, netcode_address_t* from, u08* packet_data, s32 packet_bytes);
    void  netcode_server_connect_loopback_client(netcode_server_t* server, s32 client_index, u64 client_id, const u08* user_data);
    void  netcode_server_disconnect_loopback_client(netcode_server_t* server, s32 client_index);
    s32   netcode_server_client_loopback(netcode_server_t* server, s32 client_index);
    void  netcode_server_process_loopback_packet(netcode_server_t* server, s32 client_index, const u08* packet_data, s32 packet_bytes, u64 packet_sequence);
    u16   netcode_server_get_port(netcode_server_t* server);


    void netcode_random_bytes(u08* data, s32 bytes);
    void netcode_sleep(f64 seconds);
    f64 netcode_time();
  //};



#if NETCODE_ENABLE_TESTS
    void netcode_test();
#endif



}// namespace GG
