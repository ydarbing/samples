#pragma once

namespace GG
{
#define NETCODE_PACKET_QUEUE_SIZE 256


  struct netcode_packet_queue_t
  {
    void* allocatorContext;
    void* (*allocateFunction)(void*, u64);
    void(*freeFunction)(void*, void*);
    s32 numPackets;
    s32 startIndex;
    void* packetData[NETCODE_PACKET_QUEUE_SIZE];
    u64 packetSequence[NETCODE_PACKET_QUEUE_SIZE];
  };

  // public functions
  void netcode_packet_queue_init(netcode_packet_queue_t* queue, void* allocatorContext, void* (*allocateFunction) (void*, u64), void(*freeFunction)(void*, void*));
  void netcode_packet_queue_clear(netcode_packet_queue_t* queue);
  void* netcode_packet_queue_pop(netcode_packet_queue_t* queue, u64* packetSequence);
  bool netcode_packet_queue_push(netcode_packet_queue_t* queue, void* packetData, u64 packetSequence);


}// namespace GG
