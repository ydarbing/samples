#pragma once

namespace GG
{

  /*
  typedef std::array<netcode_address_t, NETCODE_MAX_SERVERS_PER_CONNECT> ServerAddressArray;

  struct netcode_connect_token_t
  {
    u08 versionInfo[NETCODE_VERSION_INFO_BYTES];
    u64 protocolId;
    u64 createTimestamp;
    u64 expireTimestamp;
    u64 sequence;
    u08 privateData[NETCODE_CONNECT_TOKEN_PRIVATE_BYTES];
    s32 timeoutSeconds;
    s32 numServerAddresses;
    //netcode_address_t serverAddresses[NETCODE_MAX_SERVERS_PER_CONNECT];
    ServerAddressArray serverAddresses;
    u08 clientToServerKey[NETCODE_KEY_BYTES];
    u08 serverToClientKey[NETCODE_KEY_BYTES];
  };


  void netcode_write_connect_token(netcode_connect_token_t* connectToken, u08* buffer, s32 bufferLength);

  s32 netcode_read_connect_token(u08* buffer, s32 bufferLength, netcode_connect_token_t* connectToken);
  */
}// namespace GG
