#pragma once

#include "Allocator\Allocator.h"
#include "Message.h"
#include "Channel\Channel.h"
#include "BitArray.h"

#include "SequenceBuffer.h"

namespace GG
{
  /*
      Messages sent across this channel are guaranteed to arrive in the order they were sent
      This channel type is best used for control messages and RPCs
      Messages sent over this channel are included in connection packets until one of those packets is acked
      Messages are acked individually and remain in send queue until acked
      Bloacks attached to messages over this channel are split up into fragments.  Each fragment of the block is included in a connection packet until one of those packets are acked
        Eventually all fragments are received on the other side and the block is reassembled and attached to the message
      Only one message block may be in flight over the network at any time, so blocks stall out message delivery slightly.
      Only use blocks for large data that won't fit inside a single connection packet where you actually need the channel to split it up into fragments
      If your block fits inside a packet, just serialize it inside your message serialize via SERIALIZE_BYTES instead
  */
  class ChannelReliableOrdered : public Channel
  {
  public:
    ChannelReliableOrdered(Allocator& allocator, MessageFactory& messageFactory, const ChannelConfig& config, s32 channelIndex, f64 time);
    // any message still in send or receive queues will be released
    ~ChannelReliableOrdered();
    void Reset();
    bool CanSendMessage() const;
    void SendMessage(Message* message, void* context);
    Message* ReceiveMessage();
    void AdvanceTime(f64 time);
    s32 GetPacketData(void* context, ChannelPacketData& packetData, u16 packetSequence, s32 availableBits);
    void ProcessPacketData(const ChannelPacketData& packetData, u16 packetSequence);
    void ProcessAck(u16 ack);
    // Are there any unacked messages in the end queue?
    // Messages are acked individually and remain in send queue until acked
    // returns true if there is at least one unacked message in the send queue
    bool HasMessagesToSend()const;
    // Get messages to include in a packet
    // Messages are measured to see how many bits they take, and only messages that fit within the channel packet budget will be included.  See ChannelConfig::packetBudget
    // takes care not to send messages to rapidly by respecting ChannelConfig::messageResendTime for each message, and to only include messages that the receiver is able to buffer in their receive queue
    //      won't run ahead of the receiver
    // MessageIds - array of message ids to be filled out.  Fills up to ChannelConfig::maxMessagesPerPacket messages, make sure your array is at least this size
    // numMessageIds - number of message ids written to the array
    // remainingPacketBits - number of bits remaining in the packet. Considers this as a hard limit when determining how many messages can fit into this packet
    // returns - estimate of the number of bits required to serialize the message (upper bound)
    s32 GetMessagesToSend(u16* messageIds, s32& numMessageIds, s32 remainingPacketBits, void* context);
    // Fills channel packet data with messages
    // This is the payload function to fill packet data while sending regular messages (wihtout blocks attached)
    // Messages have references added to them when they are added to the packet.  They also have a reference while they are stored in a send or receive queue.  Messages are cleaned up when they are no longer in a queue, and no longer referenced by any packets
    // packetData - The packet data to fill out
    // messageIds - array of message ids identifying which messages to add to the packet from the message send queue
    // numMessageIds - number of messages ids in the array
    void GetMessagePacketData(ChannelPacketData& packetData, const u16* messageIds, s32 numMessageIds);
    // Add packet entry for the set of messages included in a packet
    // This lets is look up the set of messages that were included in that packet later on when it is acked, so we can ack those messages individually
    // messageIds - set of message ids that were included in the packet
    // numMessageids - number of message ids in the array
    // sequence - the sequence number of the connection packet the messages were included in
    void AddMessagePacketEntry(const u16* messageIds, s32 numMessageIds, u16 sequence);
    // Process messages included in a packet
    // Any messages that have already been received are added to the message receive queue.  Messages that are added to the receive queue have a reference added.  See Message::AddRef
    // numMessages - number of messages to process
    // messages - array of pointers to messages
    void ProcessPacketMessages(s32 numMessages, Message** messages);
    // track oldest unacked message id in the send queue
    // because messages are acked indivudually, the send queue is not a true queue and may have holes
    // because of theses possible holes it is necessary to periodically walk forward from the previous oldest unacked message id, to find the current oldest unacked message id
    // this lets us know our starting point for considering messages to include in the next packet we send
    void UpdateOldestUnackedMessageId();
    // Block messages are treated differently than regular messages
    // regualr messages are small so we try to fit as many into the packet as we can.  See ReliableChannelData::GetMessagesToSend
    // Blocks attached to block messages are usually larger than the maximum packet size or channel budget, they they are split up into fragments
    // while in the mode of sending a block mssage, each channel packet data generated has exactly one fragment from the current block in it.  Fragments keep getting included in packets until all fragments of that block are acked
    // returns - true if currently sending a block message over network
    bool SendingBlockMessage();
    // Get next block fragment to send
    // selected by scanning left to right over the set of fragments in the block, skipping over any fragments that have already been acked or have been sent within ChannelConfig::fragmentResendTime
    // messageId - id of the message that the block is attached to [out]
    // fragmentId - id of the fragment to send out
    // fragmentBytes - size of the fragment in bytes
    // numFragments - total number of fragments in this block
    // messageType - type of message the block is attached to.  See MessageFactory
    // return - pointer to the fragment data
    u08* GetFragmentToSend(u16& messageId, u16& fragmentId, s32& fragmentBytes, s32& numFragments, s32& messageType);
    // Fill packet data with block and fragment data
    // This is the payload function that fills the channel packet data while we are sending a block message
    // packetData - Packet data to be filled out
    // messageId - id of the message that the block is attached to
    // fragmentId - id of the block fragment being sent
    // fragmentData - fragment data
    // fragmentSize - size of the fragment data in bytes
    // numFragments - number of fragments in block
    // messageType - type of message block is attached to
    // return - Estimate of the number of bits required to serialize the block message and fragment data (upper bound)
    s32 GetFragmentPacketData(ChannelPacketData& packetData, u16 messageId, u16 fragmentId, u08* fragmentData, s32 fragmentSize, s32 numFragments, s32 messageType);
    // Adds packet entry for the fragment
    // lets us look up the fragment that was in the packet later on when it is acked, so we can ack that block fragment
    // messageId - message is that the block was attached to
    // fragmentId - fragment id
    // sequence - sequence number of the packet the fragment was included in
    void AddFragmentPacketEntry(u16 messageId, u16 fragmentId, u16 sequence);
    // Fragment is added to the set of received fragments for the block.  When all packet fragments are received, that block is reconstructed, attached to the block message and added to the message receive queue
    // messageType - Type of message this block fragment is attached to.  Used to make sure this message type actually allows blocks attached to it
    // messageId - Id of the message the block fragment belongs to
    // numFragments - number of fragments in the block
    // fragmentId - if of the fragment in [0, numFragments -1]
    // fragmentData - fragment data
    // fragmentBytes - size of fragment data in bytes
    // blockMessage - pointer to the block message. Passed this in only with the first message (0), pass nullptr for all other fragments
    void ProcessPacketFragment(s32 messageType, u16 messageId, s32 numFragments, u16 fragmentId, const u08* fragmentData, s32 fragmentBytes, BlockMessage* blockMessage);

  protected:
    /*
      Entry in the send queue of the reliable-ordered channel
      Messages stay into the send queue until acked.  Each Message is acked individually.
      There can be "holes" in the message send queue
    */
    struct MessageSendQueueEntry
    {
      Message* message;          // When inserted in the send queue the message has one reference.  It is relaesed when the message is acked and removed from the send queue
      f64 timeLastSent;       // time the message was last sent.  Used to implement ChannelConfig::messageResendTime
      u32 measuredBits : 31; // number of bits the message takes up in a bit stream
      u32 block : 1;        // 1 if it is a block message,  Block messages are treated differently to regular messages when sent over a reliable ordered channel
    };

    struct MessageReceiveQueueEntry
    {
      Message* message; // has a reference count of at least 1 while in the receive queue.  Ownership of this message is passed back to the caller when the message is dequeued
    };

    // maps packet leel acks to messages and fragments for the reliable ordered channel
    struct SentPacketEntry
    {
      f64 timeSent;              // time this packet was sent
      u16* messageIds;         // array of message ids, dynamically allocated because user can configure max number of messages in a packet per channel with ChannelConfig::maxMessagesPerPacket
      u32 numMessageIds : 16;  // number of message ids in the array
      u32 acked : 1;           // 1 if packet has been acked
      u64 block : 1;           // 1 if packet contains a fragment of a block message
      u64 blockMessageId : 16; // valid only if "block" is 1
      u64 blockFragmentId : 16;// valid only if "block" is 1
    };

    // internal state of a block being sent across the reliable ordered channel
    // stores block data and tracks which fragments have been acked.  block send completes when all fragments have been acked
    // ONLY one data block can be in flight at a time
    struct SendBlockData
    {
      SendBlockData(Allocator& allocator, s32 maxFragmentsPerBlock)
      {
        m_allocator = &allocator;
        ackedFragment = GG_NEW(allocator, BitArray, allocator, maxFragmentsPerBlock);
        fragmentSendTime = (f64*)GG_ALLOCATE(allocator, sizeof(f64)* maxFragmentsPerBlock);
        gg_assert(ackedFragment);
        gg_assert(fragmentSendTime);
        Reset();
      }

      ~SendBlockData()
      {
        GG_DELETE(*m_allocator, BitArray, ackedFragment);
        GG_FREE(*m_allocator, fragmentSendTime);
      }

      void Reset()
      {
        active = false;
        numFragments = 0;
        numAckedFragments = 0;
        blockMessageId = 0;
        blockSize = 0;
      }

      bool active;              // true if currently sending block
      s32 blockSize;            // suze of the block in bytes
      s32 numFragments;         // number of fragments in the block being sent
      s32 numAckedFragments;    // number of acked fragments in the block being sent
      u16 blockMessageId;  //  message id the block is attached to
      BitArray* ackedFragment;  //  has fragment n been receieved?
      f64* fragmentSendTime; // last time fragment was sent
    private:
      Allocator* m_allocator;
      SendBlockData(const SendBlockData& other);
      SendBlockData& operator=(const SendBlockData& other);
    };

    // internal state of a block being received across the reliable ordered channel
    // Stores fragments received over the network for the block, and completes once all fragments have been received
    // ONLY one data block can be in flight at a time
    struct ReceiveBlockData
    {
      ReceiveBlockData(Allocator& allocator, s32 maxBlockSize, s32 maxFragmentsPerBlock)
      {
        m_allocator = &allocator;
        receivedFragment = GG_NEW(allocator, BitArray, allocator, maxFragmentsPerBlock);
        blockData = (u08*)GG_ALLOCATE(allocator, maxBlockSize);
        gg_assert(receivedFragment && blockData);
        blockMessage = nullptr;
        Reset();
      }

      ~ReceiveBlockData()
      {
        GG_DELETE(*m_allocator, BitArray, receivedFragment);
        GG_FREE(*m_allocator, blockData);
      }

      void Reset()
      {
        active = false;
        numFragments = 0;
        numReceivedFragments = 0;
        messageId = 0;
        messageType = 0;
        blockSize = 0;
      }

      bool active;               // true if currently receiving a block
      s32 numFragments;          // Number of fragments in this block
      s32 numReceivedFragments;  // number of fragments received
      u16 messageId;        // message id corresponding to the block
      s32 messageType;           // message type of block being received
      u32 blockSize;        // block size in bytes
      BitArray* receivedFragment;// has fragment n been received?
      u08* blockData;        // block data for receive
      BlockMessage* blockMessage;// blockmessage (sent with fragment 0)

    private:
      Allocator* m_allocator; // used to free the data on shutdown
      ReceiveBlockData(const ReceiveBlockData& other);
      ReceiveBlockData& operator=(const ReceiveBlockData& other);
    };


  private:
    u16 m_sendMessageId; // id of next message to be added to send queue
    u16 m_receiveMessageId; // id of next message to be added to the receive queue
    u16 m_oldestUnackedMessageId; // id of oldest unacked message in the send queue
    SequenceBuffer<SentPacketEntry>* m_sentPackets; // stores info per sent connection packet about messages and block data included in each packet 
                                                    // Used to walk from connection packet level acks to message and data block fragment level acks
    SequenceBuffer<MessageSendQueueEntry>* m_messageSendQueue;
    SequenceBuffer<MessageReceiveQueueEntry>* m_messageReceiveQueue;
    u16* m_sentPacketMessageIds; // array of n message ids per sent connection packet.  Allows the max number of messages per packet to be allocated dynamically
    SendBlockData* m_sendBlock;  // data about the block currently being sent
    ReceiveBlockData* m_receiveBlock; // data about block currently being received

  private:
    ChannelReliableOrdered(const ChannelReliableOrdered& other);
    ChannelReliableOrdered& operator=(const ChannelReliableOrdered& other);
  };

}// namespace GG
