#pragma once

 
#include "BlockMessage.h"
#include "MessageFactory.h"
#include "Stream/StreamRead.h"
#include "Stream/StreamWrite.h"
#include "Stream/StreamMeasure.h"
#include "Serializer.h" // SERIALIZE_INT

namespace GG
{
  class Message;
  class MessageFactory;
  class Allocator;



  enum ChannelType
  {
    E_CHANNEL_TYPE_RELIABLE_ORDERED,
    E_CHANNEL_TYPE_UNRELIABLE_UNORDERED
  };


  struct ChannelConfig
  {
    ChannelType type;
    bool disableBlocks;
    s32 sentPacketBufferSize;
    s32 messageSendQueueSize;
    s32 messageReceiveQueueSize;
    s32 maxMessagesPerPacket;
    s32 packetBudget;
    s32 maxBlockSize;
    s32 blockFragmentSize;
    f32 messageResendTime;
    f32 blockFragmentResendTime;

    ChannelConfig() : type(E_CHANNEL_TYPE_RELIABLE_ORDERED)
    {
      disableBlocks = false;
      sentPacketBufferSize = 1024;
      messageSendQueueSize = 1024;
      messageReceiveQueueSize = 1024;
      maxMessagesPerPacket = 256;
      packetBudget = -1;
      maxBlockSize = 256 * 1024;
      blockFragmentSize = 1024;
      messageResendTime = 0.1f;
      blockFragmentResendTime = 0.25f;
    }

    s32 GetMaxFragmentsPerBlock() const
    {
      return maxBlockSize / blockFragmentSize;
    }
  };

  struct ChannelPacketData
  {
    u32 channelIndex : 16;
    u32 initialized : 1;
    u32 blockMessage : 1;
    u32 messageFailedToSerialize : 1;

    struct MessageData
    {
      s32 numMessages;
      Message** messages;
    };

    struct BlockData
    {
      BlockMessage* message;
      u08* fragmentData;
      u64 messageId : 16;
      u64 fragmentId : 16;
      u64 fragmentSize : 16;
      u64 numFragments : 16;
      s32 messageType;
    };

    union
    {
      MessageData message;
      BlockData block;
    };

    void Initialize();
    void Free(MessageFactory& messageFactory);
    template<typename Stream>
    bool Serialize(Stream& stream, MessageFactory& messageFactory, const ChannelConfig* channelConfigs, s32 numChannels);

    bool SerializeInternal(StreamRead& stream, MessageFactory& messageFactory, const ChannelConfig* channelConfigs, s32 numChannels);
    bool SerializeInternal(StreamWrite& stream, MessageFactory& messageFactory, const ChannelConfig* channelConfigs, s32 numChannels);
    bool SerializeInternal(StreamMeasure& stream, MessageFactory& messageFactory, const ChannelConfig* channelConfigs, s32 numChannels);
  };


  /*
  Channel counters provide insight into the number of times an action was performed by a channel
  Intended for use in telemetry system, ex. reported to some backend logging system to track behavior in a production environment
  */
  enum ChannelCounters
  {
    E_CHANNEL_COUNTER_MESSAGES_SENT,     // Number of messages sent over this channel
    E_CHANNEL_COUNTER_MESSAGES_RECEIVED, // number of messages received over this channel
    E_CHANNEL_COUNTER_NUM_COUNTERS       // number of channel counters
  };

  // If channel gets into an error state, sets an error state on the corresponding connection.
  // if an channel on a client/server connection gets into a bad state, that client is automatically kicked from the server
  enum ChannelErrorLevel
  {
    E_CHANNEL_ERROR_NONE = 0,            // No error. All is well.
    E_CHANNEL_ERROR_DESYNC,              // Channel has desynced. Connection protocol has desynced and cannot recover. The client should be disconnected
    E_CHANNEL_ERROR_SEND_QUEUE_FULL,     // User tried to send a message but the send queue was full. Will assert out in development, but in production it sets this error on the channel
    E_CHANNEL_ERROR_BLOCKS_DISABLED,     // Channel received a packet containing data for blocks, but this channel is configured to disable blocks. See ChannelConfig::disableBlocks
    E_CHANNEL_ERROR_FAILED_TO_SERIALIZE, // Serialize read failed for a message sent to this channel. Check your message serialize functions, one of them is returning false on serialize read. This can also be caused by a desync in message read and write
    E_CHANNEL_ERROR_OUT_OF_MEMORY,       // Channel tried to allocate some memory but couldn't
  };

  // helper function to convert a channel error to user friendly string
  inline const c08* GetChannelErrorString(ChannelErrorLevel error)
  {
    switch (error)
    {
    case E_CHANNEL_ERROR_NONE:                    return "none";
    case E_CHANNEL_ERROR_DESYNC:                  return "desync";
    case E_CHANNEL_ERROR_SEND_QUEUE_FULL:         return "send queue full";
    case E_CHANNEL_ERROR_OUT_OF_MEMORY:           return "out of memory";
    case E_CHANNEL_ERROR_BLOCKS_DISABLED:         return "blocks disabled";
    case E_CHANNEL_ERROR_FAILED_TO_SERIALIZE:     return "failed to serialize";
    default:
      gg_assert(false);
      return "unknown channel error";
    }
  }


  // common functionallity shared across all cannel types
  class Channel
  {
  public:
    Channel(Allocator& allocator, MessageFactory& messageFactory, const ChannelConfig& config, s32 channelIndex, f64 time);

    virtual ~Channel() {}

    virtual void Reset() = 0;
    // returns true if message can be sent over this channel
    virtual bool CanSendMessage()const = 0;
    // are there messages in the send queue?
    // return true if there is at least one message in the send queue
    virtual bool HasMessagesToSend()const = 0;
    // queue message to be sent across this channel
    virtual void SendMessage(Message* message, void* context) = 0;
    // Pop message off the receive queue if there is one
    // returns pointer to the received message, nullptr if none.  
    // Caller owns the message object returned by this function and is responsible for releasing it via Message::Release
    virtual Message* ReceiveMessage() = 0;
    // Advance Channel time
    // Connection::AdvancedTime for each channel configured on the connection
    virtual void AdvanceTime(f64 time) = 0;
    // packetData - channel packetData to be filled out
    // packetSequence - sequence number of the packet being generated
    // availableBits -  maximum number of bits of packet data the channel is allowed to write
    // returns number of bits of packet data written by channel
    virtual s32 GetPacketData(void* context, ChannelPacketData& packetData, u16 packetSequence, s32 availableBits) = 0;
    // Process packet data in a connection packet
    // packetData - channel packet to process
    // packetSequence - connection packet sequence number that contains the channel packet data
    virtual void ProcessPacketData(const ChannelPacketData& packetData, u16 packetSequence) = 0;
    // Process connection packet ack
    // For reliable-ordered - Acks message and block fragments so they stop being included in outgoing connection packets 
    // For unreliable-unordered - does nothing at all ()
    virtual void ProcessAck(u16 sequence) = 0;

  public:
    ChannelErrorLevel GetErrorLevel() const;
    // returns channel index [0, numChannels-1]
    s32 GetChannelIndex() const;
    // returns value of counter
    u64 GetCounter(s32 index)const;
    // reset all counter values to 0
    void ResetCounters();

  protected:
    void SetErrorLevel(ChannelErrorLevel errorLevel);

  protected:
    const ChannelConfig m_config;
    Allocator* m_allocator; // allocator for allocations matching life of this channel
    s32 m_channelIndex; // [0, numChannels-1]
    f64 m_time; // the current time
    ChannelErrorLevel m_errorLevel; // channel error level
    MessageFactory* m_messageFactory; // for creating and destroying messages
    u64 m_counters[E_CHANNEL_COUNTER_NUM_COUNTERS]; // counter for unit testing
  };

  template <typename Stream>
  bool SerializeOrderedMessages(Stream& stream, MessageFactory& messageFactory, s32& numMessages, Message**& messages, s32 maxMessagesPerPacket)
  {
    const s32 maxMessageType = messageFactory.GetNumTypes() - 1;
    bool hasMessages = Stream::IsWriting && numMessages != 0;

    SERIALIZE_BOOL(stream, hasMessages);

    if (hasMessages)
    {
      SERIALIZE_INT(stream, numMessages, 1, maxMessagesPerPacket);

      s32* messageTypes = (s32*)alloca(sizeof(s32) * numMessages);
      u16* messageIDs = (u16*)alloca(sizeof(u16) * numMessages);

      memset(messageTypes, 0, sizeof(s32) * numMessages);
      memset(messageIDs, 0, sizeof(u16) * numMessages);

      if (Stream::IsWriting)
      {
        gg_assert(messages);
        for (s32 i = 0; i < numMessages; ++i)
        {
          gg_assert(messages[i]);
          messageTypes[i] = messages[i]->GetType();
          messageIDs[i] = messages[i]->GetId();
        }
      }
      else 
      {
        Allocator& allocator = messageFactory.GetAllocator();
        messages = (Message**)GG_ALLOCATE(allocator, sizeof(Message*) * numMessages);
        for(s32 i = 0; i < numMessages; ++i)
        {
          messages[i] = nullptr;
        }
      }

      SERIALIZE_BITS(stream, messageIDs[0], 16);

      for(s32 i = 1; i < numMessages; ++i)
      {
        SERIALIZE_SEQUENCE_RELATIVE(stream, messageIDs[i - 1], messageIDs[i]);
      }

      for(s32 i = 0; i < numMessages; ++i)
      {
        if(maxMessageType > 0)
        {
          SERIALIZE_INT(stream, messageTypes[i], 0, maxMessageType);
        }
        else
        {
          messageTypes[i] = 0;
        }

        if(Stream::IsReading)
        {
          messages[i] = messageFactory.CreateMessage(messageTypes[i]);
          
          if(!messages[i])
          {
            gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to create message of type %d (SerializeOrderedMessages)\n", messageTypes[i]);
            return false;
          }
          messages[i]->SetId(messageIDs[i]);
        }

        gg_assert(messages[i]);

        if(!messages[i]->SerializeInternal(stream))
        {
          gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to serialize message of type %d (SerializeOrderedMessages)\n", messageTypes[i]);
          return false;
        }
      }
    }

    return true;
  }


  template <typename Stream>
  bool SerializeMessageBlock(Stream& stream, MessageFactory& messageFactory, BlockMessage* blockMessage, s32 maxBlockSize)
  {
    s32 blockSize = Stream::IsWriting ? blockMessage->GetBlockSize() : 0;

    SERIALIZE_INT(stream, blockSize, 1, maxBlockSize);
    
    u08* blockData;
    if(Stream::IsReading)
    {
      Allocator& allocator = messageFactory.GetAllocator();
      blockData = (u08*)GG_ALLOCATE(allocator, blockSize);
      if(!blockData)
      {
        gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to allocate message block (SerializeMessageBlock)\n");
        return false;
      }
      blockMessage->AttachBlock(allocator, blockData, blockSize);
    }
    else
    {
      blockData = blockMessage->GetBlockData();
    }

    SERIALIZE_BYTES(stream, blockData, blockSize);

    return true;
  }

  template <typename Stream>
  bool SerializeUnorderedMessages(Stream& stream, MessageFactory& messageFactory, s32& numMessages, Message**& messages, s32 maxMessagesPerPacket, s32 maxBlockSize)
  {
    const s32 maxMessageTypes = messageFactory.GetNumTypes() - 1;
    bool hasMessages = Stream::IsWriting && numMessages != 0;

    SERIALIZE_BOOL(stream, hasMessages);

    if(hasMessages)
    {
      SERIALIZE_INT(stream, numMessages, 1, maxMessagesPerPacket);

      s32* messageTypes = SCAST(s32*, alloca(sizeof(s32) * numMessages));
      memset(messageTypes, 0, sizeof(s32) * numMessages);

      if(Stream::IsWriting)
      {
        gg_assert(messages);

        for(s32 i = 0; i < numMessages; ++i)
        {
          gg_assert(messages[i]);
          messageTypes[i] = messages[i]->GetType();
        }
      }
      else
      {
        Allocator& allocator = messageFactory.GetAllocator();
        messages = SCAST(Message**, GG_ALLOCATE(allocator, sizeof(Message*) * numMessages));

        for (s32 i = 0; i < numMessages; ++i)
          messages[i] = nullptr;
      }


      for(s32 i = 0; i < numMessages; ++i)
      {
        if(maxMessageTypes > 0)
        {
          SERIALIZE_INT(stream, messageTypes[i], 0, maxMessageTypes);
        }
        else
        {
          messageTypes[i] = 0;
        }

        if(Stream::IsReading)
        {
          messages[i] = messageFactory.CreateMessage(messageTypes[i]);
          
          if(!messages[i])
          {
            gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to create message type %d (SerializeUnorderedMessages)\n", messageTypes[i]);
            return false;
          }
        }

        gg_assert(messages[i]);

        if(!messages[i]->SerializeInternal(stream))
        {
          gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to serialize message type %d (SerializeUnorderedMessages)\n", messageTypes[i]);
          return false;
        }

        if(messages[i]->IsBlockMessage())
        {
          BlockMessage* blockMessage = SCAST(BlockMessage*, messages[i]);
          if(!SerializeMessageBlock(stream, messageFactory, blockMessage, maxBlockSize))
          {
            gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to serialize message block (SerializeUnorderedMessages)\n");
            return false;
          }
        }
      }
    }// hasMessages
    return true;
  }

  template <typename Stream>
  bool SerializeBlockFragment(Stream& stream, MessageFactory& messageFactory, ChannelPacketData::BlockData& block, const ChannelConfig& channelConfig)
  {
    const s32 maxMessageType = messageFactory.GetNumTypes() - 1;

    SERIALIZE_BITS(stream, block.messageId, 16);

    if(channelConfig.GetMaxFragmentsPerBlock() > 1)
    {
      SERIALIZE_INT(stream, block.numFragments, 1, channelConfig.GetMaxFragmentsPerBlock());
    }
    else
    {
      if(Stream::IsReading)
      {
        block.numFragments = 1;
      }
    }

    if(block.numFragments > 1)
    {
      SERIALIZE_INT(stream, block.fragmentId, 0, block.numFragments - 1);
    }
    else
    {
      if (Stream::IsReading)
      {
        block.fragmentId = 0;
      }
    }

    SERIALIZE_INT(stream, block.fragmentSize, 1, channelConfig.blockFragmentSize);

    if(Stream::IsReading)
    {
      block.fragmentData = SCAST(u08*, GG_ALLOCATE(messageFactory.GetAllocator(), block.fragmentSize));

      if(!block.fragmentData)
      {
        gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to serialize block fragment (SerializeBlockFragment)\n");
        return false;
      }
    }

    SERIALIZE_BYTES(stream, block.fragmentData, block.fragmentSize);


    if(block.fragmentId == 0)
    {
      // block message
      if (maxMessageType > 0)
        SERIALIZE_INT(stream, block.messageType, 0, maxMessageType);
      else
        block.messageType = 0;

      if(Stream::IsReading)
      {
        Message* message = messageFactory.CreateMessage(block.messageType);

        if(!message)
        {
          gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to create block message type %d (SerializeBlockFragment)\n", block.messageType);
          return false;
        }

        if(!message->IsBlockMessage())
        {
          gg_printf(GG_LOG_LEVEL_ERROR, "Error: received block fragment attached to non-block message (SerializeBlockFragment)\n");
          return false;
        }

        block.message = SCAST(BlockMessage*, message);
      }

      gg_assert(block.message);

      if(!block.message->SerializeInternal(stream))
      {
        gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to serialize block message of type %d (SerializeBlockFragment)\n", block.messageType);
        return false;
      }
    }
    else
    {
      if(Stream::IsReading)
      {
        block.message = nullptr;
      }
    }

    return true;
  }


  template<typename Stream>
  inline bool ChannelPacketData::Serialize(Stream & stream, MessageFactory & messageFactory, const ChannelConfig * channelConfigs, s32 numChannels)
  {
    gg_assert(initialized);

#if GG_DEBUG_MESSAGE_BUDGET
    s32 startBits = stream.GetBitsProcessed();
#endif

    if(numChannels > 1)
    {
      SERIALIZE_INT(stream, channelIndex, 0, numChannels - 1);
    }
    else
    {
      channelIndex = 0;
    }

    const ChannelConfig& channelConfig = channelConfigs[channelIndex];

    SERIALIZE_BOOL(stream, blockMessage);


    if(!blockMessage)
    {
      switch(channelConfig.type)
      {
      case E_CHANNEL_TYPE_RELIABLE_ORDERED:
        {
          if(!SerializeOrderedMessages(stream, messageFactory, message.numMessages, message.messages, channelConfig.maxMessagesPerPacket))
          {
            messageFailedToSerialize = 1;
            return true;
          }
        }
        break;
      case E_CHANNEL_TYPE_UNRELIABLE_UNORDERED:
        {
          if(!SerializeUnorderedMessages(stream, messageFactory, message.numMessages, message.messages, channelConfig.maxMessagesPerPacket, channelConfig.maxBlockSize))
          {
            messageFailedToSerialize = 1;
            return true;
          }
        }
        break;
      }

#if GG_DEBUG_MESSAGE_BUDGET
      if(channelConfig.packetBudget > 0)
      {
        gg_assert(stream.GetBitsProcessed() - startBits <= channelConfig.packetBudget * 8);
      }
#endif
    }
    else
    {
      if(channelConfig.disableBlocks)
        return false;
      if (!SerializeBlockFragment(stream, messageFactory, block, channelConfig))
        return false;
    }

    return true;
  }

}// namespace GG

