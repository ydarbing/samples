#pragma once
#include "Channel.h"
#include "Queue.h"


namespace GG
{
  class Message;
  class MessageFactory;
  class Allocator;

  // Messages sent over this channel are not guaranteed to arrive, and may be received in a different order than they were sent
  // this channel best used for time time critical data like snapshots and object state
  class ChannelUnreliableUnordered : public Channel
  {
  public:
    // allocator - allocator to use
    // messageFactory - used for creating and destroying messages
    // config - configuration of this channel
    // channelIndex - channel index [0, numChannels-1]
    ChannelUnreliableUnordered(Allocator& allocator, MessageFactory& messageFactory, const ChannelConfig& config, s32 channelIndex, f64 time);
    // any messages still in send or receive queues will be realeased
    ~ChannelUnreliableUnordered();
    void Reset();
    bool CanSendMessage()const;
    bool HasMessagesToSend()const;
    void SendMessage(Message* message, void* context);
    Message* ReceiveMessage();
    void AdvanceTime(f64 time);
    s32 GetPacketData(void* context, ChannelPacketData& packetData, u16 packetSequence, s32 availableBits);
    void ProcessPacketData(const ChannelPacketData& packetData, u16 packetSequence);
    
    void ProcessAck(u16 ack);

  protected:
    Queue<Message*>* m_messageSendQueue;    // send queue
    Queue<Message*>* m_messageReceiveQueue; // receive queue
  private:
    ChannelUnreliableUnordered(const ChannelUnreliableUnordered& other);
    ChannelUnreliableUnordered& operator =(const ChannelUnreliableUnordered& other);
  };

}// namespace GG
