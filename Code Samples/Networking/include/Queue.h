#pragma once

#include "Allocator/Allocator.h"

namespace GG
{

  // FIFO queue
  template <typename T>
  class Queue
  {
  public:
    // size - max number of entries in the queue
    Queue(Allocator& allocator, s32 size)
    {
      gg_assert(size > 0);
      m_arraySize = size;
      m_startIndex = 0;
      m_numEntries = 0;
      m_allocator = &allocator;
      m_entries = SCAST(T*, GG_ALLOCATE(allocator, sizeof(T) * size));
      memset(m_entries, 0, sizeof(T) * size);
    }

    ~Queue()
    {
      gg_assert(m_allocator);
      GG_FREE(*m_allocator, m_entries);
      m_arraySize = 0;
      m_startIndex = 0;
      m_numEntries = 0;
      m_allocator = nullptr;
    }

    // clear all entries in queue and reset back to default state
    void Clear()
    {
      m_numEntries = 0;
      m_startIndex = 0;
    }
    // pop value off queue.  
    // Will assert if empty.  Check Queue::IsEmpty() or Queue::GetNumEntries() first
    T Pop()
    {
      gg_assert(!IsEmpty());
      const T& entry = m_entries[m_startIndex];
      m_startIndex = (m_startIndex + 1) % m_arraySize;
      --m_numEntries;
      return entry;
    }

    // Push value on to queue
    // Will assert if queue is already full.  Check Queue::IsFull before calling
    void Push(const T& value)
    {
      gg_assert(!IsFull());
      const s32 index = (m_startIndex + m_numEntries) % m_arraySize;
      m_entries[index] = value;
      ++m_numEntries;
    }

    // random access operator
    // index - index in queue, 0 is oldest entry, Queue::GetNumEntries() - 1 is the newest
    // return - value in the queue at the index
    T& operator[](s32 index)
    {
      gg_assert(!IsFull());
      gg_assert(index >= 0);
      gg_assert(index < m_numEntries);
      return m_entries[(m_startIndex + index) % m_arraySize];
    }
    // random access operator
    // index - index in queue, 0 is oldest entry, Queue::GetNumEntries() - 1 is the newest
    // return - value in the queue at the index
    const T& operator[](s32 index)const
    {
      gg_assert(!IsFull());
      gg_assert(index >= 0);
      gg_assert(index < m_numEntries);
      return m_entries[(m_startIndex + index) % m_arraySize];
    }

    // Get size of queue
    // This is max number of entries that can be pushed on the queue
    s32 GetSize()const
    {
      return m_arraySize;
    }

    // return - true if queue is full
    bool IsFull()const
    {
      return m_numEntries == m_arraySize;
    }

    // return - true if no entries in queue
    bool IsEmpty()const
    {
      return m_numEntries == 0;
    }

    // return - [0, GetSize()]
    s32 GetNumEntries()const
    {
      return m_numEntries;
    }

  private:
    Allocator* m_allocator;
    T* m_entries;     // array of entries backing the queue (circualar buffer)
    s32 m_arraySize;  //size of the array in number of entries.  This is the "size" of the queue
    s32 m_startIndex; // start index for the queue. This is the next value that gets popped off
    s32 m_numEntries; // number of entries currently stored in the queue
  };

}// namespace GG
