#pragma once

#include <math.h>

namespace GG
{

#define RecipSqrt(x) ((f32)(f32(1.0) / Sqrt(f32(x)))) /* reciprocal square root */

    const f32 EPSILON = 1.0e-5f;
    const s32 LOMONT_EPSILON = *(s32*)& EPSILON;
    const f32 MAX_FLOAT = 3.402823466e+38f;
    const f32 PI = f32(4.0 * atan(1.0));
    const f32 TWO_PI = f32(2.0) * PI;
    const f32 HALF_PI = f32(0.5) * PI;
    const f32 QUARTER_PI = f32(0.25) * PI;
    const f32 THREE_HALF_PI = f32(3.0) * HALF_PI;
    const f32 INV_PI = 1.0f / PI;
    const f32 INV_TWO_PI = 1.0f / TWO_PI;
    const f32 DEG_TO_RAD = PI / 180.0f;
    const f32 RAD_TO_DEG = 180.0f / PI;
    const f32 RADS_PER_DEG = (TWO_PI / f32(360.0));
    const f32 DEGS_PER_RAD = (f32(360.0) / TWO_PI);
    const f32 SQRT12 = f32(0.7071067811865475244008443621048490);
    const f32 LN_2 = f32(log(2.0));
    const f32 LN_10 = f32(log(10.0));
    const f32 INV_LN_2 = 1.0f / LN_2;
    const f32 INV_LN_10 = 1.0f / LN_10;
    const f32 ROOT_TWO = f32(sqrt(2.0));
    const f32 INV_ROOT_2 = 1.0f / ROOT_TWO;
    const f32 ROOT_THREE = f32(sqrt(3.0));
    const f32 INV_ROOT_THREE = 1.0f / ROOT_THREE;



    inline f32 Cos(f32 x) { return cosf(x); }
    inline f32 Sin(f32 x) { return sinf(x); }
    inline f32 Tan(f32 x) { return tanf(x); }
    inline f32 Asin(f32 x)
    {
        if (x < f32(-1))
            x = f32(-1);
        if (x > f32(1))
            x = f32(1);
        return asinf(x);
    }

#ifndef Fsels
    inline f32 Fsel(f32 a, f32 b, f32 c)
    {
        return a >= 0 ? b : c;
    }
#endif
#define Fsels(a, b, c) (f32) Fsel(a, b, c)


    template <typename T>
    inline T AbsVal(const T& val)
    {
        return (val < T(0) ? -val : val);
    }


    inline f32 Sqrt(f32 y)
    {
        return sqrtf(y);
    }

    inline bool Equal(f32 a, f32 b, s32 maxDiff = LOMONT_EPSILON)
    {
        // solid, fast routine across all platforms with constant time behavior
        s32 ai = *reinterpret_cast<s32*>(&a);
        s32 bi = *reinterpret_cast<s32*>(&b);
        s32 blah = -42, blahdi = -100;
        s32 test = (((unsigned)(ai ^ bi)) >> 31) - 1;
        s32 diff = (((0x80000000 - ai) & (~test)) | (ai & test)) - bi;
        s32 v1 = maxDiff + diff;
        s32 v2 = maxDiff - diff;
        s32 res = (v1 | v2);
        return (v1 | v2) >= 0;
    }

    inline bool FloatEqual(f32 a, f32 b, f32 maxDiff = EPSILON)
    {
        return (a <= b + maxDiff && a >= b - maxDiff);
    }

    template <typename T>
    inline T Min(const T& a, const T& b)
    {
        return (b < a ? b : a);
    }

    template <typename T>
    inline T Max(const T& a, const T& b)
    {
        return (a < b ? b : a);
    }

    template <typename T>
    inline T Clamp(const T& val, const T& min, const T& max)
    {
        return Max(min, Min(val, max));
    }


    template <typename T>
    void Swap(T& a, T& b)
    {
        T tmp = a;
        a = b;
        b = tmp;
    };


    template <typename T>
    inline s32 Round(const T& val)
    {
        if (val < T(0))
            return (T(-0.5f) < val - s32(val) ? s32(val) : s32(val - 1));
        else
            return (val - s32(val) < T(0.5f) ? s32(val) : s32(val + 1));
    }

    inline f32 Random(void)
    {
        return (f32(rand()) / f32(RAND_MAX));
    }

    template <typename T>
    inline T Random(const T& min, const T& max)
    {
        return T(min + ((max - min) * Random()));
    }

    template <>
    inline s32 Random(const s32& min, const s32& max)
    {
        return Round(min + ((max - min) * Random()));
    }

    template <>
    inline unsigned Random(const unsigned& min, const unsigned& max)
    {
        return Round(min + ((max - min) * Random()));
    }

    inline void ZeroIfZero(f32& val)
    {
        if (Equal(val, 0.0f))
            val = 0.0f;
    }
    //inline void ZeroIfZero(f64& val)
    //{
    //    if (Equal(val, 0.0))
    //        val = 0.0;
    //}

    inline void GetSinCos(f32 angle, f32& sina, f32& cosa)
    {
        sina = sinf(angle);
        cosa = cosf(angle);
        ZeroIfZero(sina);
        ZeroIfZero(cosa);
    }
    //inline void GetSinCos(f64 angle, f64& sina, f64& cosa)
    //{
    //    sina = sin(angle);
    //    cosa = cos(angle);
    //    ZeroIfZero(sina);
    //    ZeroIfZero(cosa);
    //}


    // generate random s32 between a and b
    inline s32 RandomInt(s32 a, s32 b)
    {
        gg_assert(a < b);
        s32 result = a + rand() % (b - a + 1);
        gg_assert(result >= a);
        gg_assert(result <= b);
        return result;
    }
    // generate random f32 between a and b
    inline f32 RandomFloat(f32 a, f32 b)
    {
        gg_assert(a < b);
        f32 random = Random();
        f32 diff = b - a;
        f32 r = random * diff;
        return a + r;
    }

    // Calculates the population count of an unsigned 32 bit integer at compile time.
    // Population count is the number of bits in the integer that set to 1.
    //   See "Hacker's Delight" and http://www.hackersdelight.org/hdcodetxt/popArrayHS.c.txt
    template <u32 x> struct PopCount
    {
        enum {
            a = x - ((x >> 1) & 0x55555555),
            b = (((a >> 2) & 0x33333333) + (a & 0x33333333)),
            c = (((b >> 4) + b) & 0x0f0f0f0f),
            d = c + (c >> 8),
            e = d + (d >> 16),

            result = e & 0x0000003f
        };
    };

    // Calculates the log 2 of an unsigned 32 bit integer at compile time.
    template <u32 x> struct Log2
    {
        enum {
            a = x | (x >> 1),
            b = a | (a >> 2),
            c = b | (b >> 4),
            d = c | (c >> 8),
            e = d | (d >> 16),
            f = e >> 1,

            result = PopCount<f>::result
        };
    };

    //Calculates the number of bits required to serialize an integer value in [min,max] at compile time.
    template <s64 min, s64 max>
    struct BitsRequired
    {
        static const u32 result = (min == max) ? 0 : (Log2<u32(max - min)>::result + 1);
    };

    //Calculates the population count of an unsigned 32 bit integer.
    //The population count is the number of bits in the integer set to 1.
    inline u32 popcount(u32 x)
    {
#ifdef __GNUC__
        return __builtin_popcount(x);
#else // #ifdef __GNUC__
        const u32 a = x - ((x >> 1) & 0x55555555);
        const u32 b = (((a >> 2) & 0x33333333) + (a & 0x33333333));
        const u32 c = (((b >> 4) + b) & 0x0f0f0f0f);
        const u32 d = c + (c >> 8);
        const u32 e = d + (d >> 16);
        const u32 result = e & 0x0000003f;
        return result;
#endif // #ifdef __GNUC__
    }

    //Calculates the log base 2 of an unsigned 32 bit integer.
    inline u32 log2(u32 x)
    {
        const u32 a = x | (x >> 1);
        const u32 b = a | (a >> 2);
        const u32 c = b | (b >> 4);
        const u32 d = c | (c >> 8);
        const u32 e = d | (d >> 16);
        const u32 f = e >> 1;
        return popcount(f);
    }


    //Calculates the number of bits required to serialize an integer in range [min,max].
    inline s32 BitsRequired(u32 min, u32 max)
    {
#ifdef __GNUC__
        return 32 - __builtin_clz(max - min);
#else // #ifdef __GNUC__
        return (min == max) ? 0 : log2(max - min) + 1;
#endif // #ifdef __GNUC__
    }

    //Reverse the order of bytes in a 64 bit integer.
    inline u64 BitSwap(u64 value)
    {
#ifdef __GNUC__
        return __builtin_bswap64(value);
#else // #ifdef __GNUC__
        value = (value & 0x00000000FFFFFFFF) << 32 | (value & 0xFFFFFFFF00000000) >> 32;
        value = (value & 0x0000FFFF0000FFFF) << 16 | (value & 0xFFFF0000FFFF0000) >> 16;
        value = (value & 0x00FF00FF00FF00FF) << 8 | (value & 0xFF00FF00FF00FF00) >> 8;
        return value;
#endif // #ifdef __GNUC__
    }

    //Reverse the order of bytes in a 32 bit integer.
    inline u32 BitSwap(u32 value)
    {
#ifdef __GNUC__
        return __builtin_bswap32(value);
#else // #ifdef __GNUC__
        return (value & 0x000000ff) << 24 | (value & 0x0000ff00) << 8 | (value & 0x00ff0000) >> 8 | (value & 0xff000000) >> 24;
#endif // #ifdef __GNUC__
    }

    // Reverse the order of bytes in a 16 bit integer
    inline u16 BitSwap(u16 value)
    {
        return (value & 0x00ff) << 8 | (value & 0xff00) >> 8;
    }


    template <typename T>
    T HostToNetwork(T value)
    {
#if GG_BIG_ENDIAN
        return BitSwap(value);
#else // #if GG_BIG_ENDIAN
        return value;
#endif // #if GG_BIG_ENDIAN
    }

    template <typename T>
    T NetworkToHost(T value)
    {
#if GG_BIG_ENDIAN
        return BitSwap(value);
#else // #if GG_BIG_ENDIAN
        return value;
#endif // #if GG_BIG_ENDIAN
    }

    // Compares two 16 bit sequence numbers and returns true if the first one is greater than the second (considering wrapping)
    // IMPORTANT: This is not the same as s1 > s2!
    // Greater than is defined specially to handle wrapping sequence numbers.
    // If the two sequence numbers are close together, it is as normal, but they are far apart, it is assumed that they have wrapped around.
    // Thus, sequence_greater_than( 1, 0 ) returns true, and so does sequence_greater_than( 0, 65535 )!
    // returns - true if the s1 is greater than s2, with sequence number wrapping considered.
    inline bool SequenceGreaterThan(u16 s1, u16 s2)
    {
        return ((s1 > s2) && (s1 - s2 <= 32768)) ||
            ((s1 < s2) && (s2 - s1 > 32768));
    }

    // Compares two 16 bit sequence numbers and returns true if the first one is less than the second (considering wrapping)
    // IMPORTANT: This is not the same as s1 < s2!
    // Greater than is defined specially to handle wrapping sequence numbers.
    // If the two sequence numbers are close together, it is as normal, but they are far apart, it is assumed that they have wrapped around.
    // Thus, sequence_less_than( 0, 1 ) returns true, and so does sequence_greater_than( 65535, 0 )!
    // returns - true if the s1 is less than s2, with sequence number wrapping considered.
    inline bool SequenceLessThan(u16 s1, u16 s2)
    {
        return SequenceGreaterThan(s2, s1);
    }

    //Convert a signed integer to an unsigned integer with zig-zag encoding.
    // 0,-1,+1,-2,+2... becomes 0,1,2,3,4 ...
    // @returns The input value converted from signed to unsigned with zig-zag encoding.
    inline s32 SignedToUnsigned(s32 n)
    {
        return (n << 1) ^ (n >> 31);
    }

    // Convert an unsigned integer to as signed integer with zig-zag encoding.
    // 0,1,2,3,4... becomes 0,-1,+1,-2,+2...
    // returns - input value converted from unsigned to signed with zig-zag encoding.
    inline s32 UnsignedToSigned(u32 n)
    {
        return (n >> 1) ^ (-s32(n & 1));
    }


    // Implementation of the 64 bit murmur hash
    // key - The input value
    // length - The length of the key (bytes)
    // seed - The initial seed for the hash. Used to chain together multiple hash calls
    // return - A 64 bit hash of the input value
    u64 MurmurHash64(const void* key, u32 length, u64 seed);

#if GG_WITH_MBEDTLS

    // Base 64 encode a string
    // input - The input string value. Must be null terminated
    // output - The output base64 encoded string. Will be null terminated
    // outputSize - The size of the output buffer (bytes). Must be large enough to store the base 64 encoded string
    // return - The number of bytes in the base64 encoded string, including terminating null. -1 if the base64 encode failed because the output buffer was too small
    s32 Base64EncodeString(const c08* input, c08* output, s32 outputSize);

    // Base 64 decode a string
    // input - The base64 encoded string
    // output - The decoded string. Guaranteed to be null terminated, even if the base64 is maliciously encoded
    // outputSize - The size of the output buffer (bytes)
    // return - The number of bytes in the decoded string, including terminating null. -1 if the base64 decode failed
    s32 Base64DecodeString(const c08* input, c08* output, s32 outputSize);

    // Base 64 encode a block of data
    // input - The data to encode
    // inputLength - The length of the input data (bytes)
    // output - The output base64 encoded string. Will be null terminated
    // outputSize - The size of the output buffer. Must be large enough to store the base 64 encoded string
    // return - The number of bytes in the base64 encoded string, including terminating null. -1 if the base64 encode failed because the output buffer was too small
    s32 Base64EncodeData(const u08* input, s32 inputLength, c08* output, s32 outputSize);

    // Base 64 decode a block of data
    // input - The base 64 data to decode. Must be a null terminated string
    // output - The output data. Will *not* be null terminated
    // outputSize - The size of the output buffer
    // returns - The number of bytes of decoded data. -1 if the base64 decode failed
    s32 Base64DecodeData(const c08* input, u08* output, s32 outputSize);

    // Print bytes with a label
    // Useful for printing out packets, encryption keys, nonce etc
    // label - The label to print out before the bytes
    // data - The data to print out to stdout
    // dataBytes - The number of bytes of data to print
    void PrintBytes(const c08* label, const u08* data, s32 dataBytes);

#endif // #if GG_WITH_MBEDTLS


    // Sleep for this number of seconds
    // time - number of seconds to sleep for
    void gg_sleep(f64 time);

    // High precision time in seconds since application started
    // returns - time value in seconds
    f64 gg_time();

    // Generate cryptographically secure random data
    // data - buffer to store the random data
    // bytes - number of bytes to generate
    void RandomBytes(u08* data, s32 bytes);

}// namespace GG
