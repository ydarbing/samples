#pragma once
#include "BaseClient.h"
#include "Address.h"


namespace GG
{
  struct netcode_client_t;

  // client for dedicated servers
  class Client : public BaseClient
  {
  public:
    explicit Client(Allocator& allocator, const Address& address, const ClientServerConfig& config, Adapter& adapter, f64 time);
    ~Client();
    void InsecureConnect(const u08 privateKey[], u64 clientId, const Address& address);
    void InsecureConnect(const u08 privateKey[], u64 clientId, const Address serverAddresses[], s32 numServerAddresses);

    void Connect(u64 clientId, u08* connectToken);
    void Disconnect();
    void SendPackets();
    void ReceivePackets();
    void AdvanceTime(f64 time);
    s32 GetClientIndex()const;
    u64 GetClientId()const;
    void ConnectLoopback(s32 clientIndex, u64 clientId, s32 maxClients);
    void DisconnectLoopback();
    bool IsLoopback() const;
    void ProcessLoopbackPacket(const u08* packetData, s32 packetBytes, u64 packetSequence);
    const Address& GetAddress()const;

  private:
    bool GenerateInsecureConnectToken(u08* connectToken, const u08 privateKey[], u64 clientId, const Address serverAddresses[], s32 numServerAddresses);
    void CreateClient(const Address& address);
    void DestroyClient();
    void StateChangeCallbackFunction(s32 previous, s32 current);
    void TransmitPacketFunction(u16 packetSequence, u08* packetData, s32 packetBytes);
    s32 ProcessPacketFunction(u16 packetSequence, u08* packetData, s32 packetBytes);
    void SendLoopbackPacketCallbackFunction(s32 clientIndex, const u08* packetData, s32 packetBytes, u64 packetSequence);
    
    static void StaticStateChangeCallbackFunction(void* context, s32 previous, s32 current);
    static void StaticSendLoopbackPacketCallbackFunction(void* context, s32 clientIndex, const u08* packetData, s32 packetBytes, u64 packetSequence);

  private:
    ClientServerConfig m_config;// client/server configuration
    netcode_client_t* m_client; // netcode.io client data
    Address m_address;          // original address passed to ctor
    Address m_boundAddress;     // address after socket bind.  ex. with valid port
    u64 m_clientId;             // globally unique client id (set on each call to connect

  };
}// namespace GG
