#pragma once
#include "IClient.h"
#include "Config/ClientServerConfig.h"

namespace GG
{
  class Allocator;
  class Adapter;
  class NetworkSimulator;
  class Connection;
  class MessageFactory;
  struct reliable_endpoint_t;

  // functionality that is common across all client implementations
  class BaseClient : public IClient
  {
  public:
    /* 
     * allocator - the allocator for all memory used by the client
     * config - the base client/server configuration
     * adapter - the adapter to the game program.  Specifies allocators, message factory to use, ect.
     * time - current time in seconds.  See IClient::AdvanceTime
     */
    explicit BaseClient(Allocator& allocator, const ClientServerConfig& config, Adapter& adapter, f64 time);

    ~BaseClient();
    void SetContext(void* context);
    void Disconnect();
    void AdvanceTime(f64 time);

    bool IsConnecting() const;
    bool IsConnected() const;
    bool IsDisconnected() const;
    bool ConnectionFailed() const;
    ClientState GetClientState() const;
    s32 GetClientIndex() const;
    f64 GetTime() const;

    void SetLatency(f32 milliseconds);
    void SetJitter(f32 milliseconds);
    void SetPacketLoss(f32 percent);
    void SetDuplicates(f32 percent);

    Message* CreateMessage(s32 type);
    u08* AllocateBlock(s32 bytes);
    void AttachBlockToMessage(Message* message, u08* block, s32 bytes);
    void FreeBlock(u08* block);
    bool CanSendMessage(s32 channelIndex) const;
    void SendMessage(s32 channelIndex, Message* message);
    Message* ReceiveMessage(s32 channelIndex);
    void ReleaseMessage(Message* message);
    void GetNetworkInfo(NetworkInfo& info) const;

  protected:

    u08* GetPacketBuffer();
    void* GetContext();
    Adapter& GetAdapter();

    void CreateInternal();
    void DestroyInternal();
    void SetClientState(ClientState clientState);

    Allocator& GetClientAllocator();
    MessageFactory& GetMessageFactory();
    NetworkSimulator* GetNetworkSimulator();
    reliable_endpoint_t* GetEndpoint();
    Connection& GetConnection();


    virtual void TransmitPacketFunction(u16 packetSequence, u08* packetData, s32 packetBytes) = 0;
    virtual s32 ProcessPacketFunction(u16 packetSequence, u08* packetData, s32 packetBytes) = 0;
    
    static void StaticTransmitPacketFunction(void* context, s32 index, u16 packetSequence, u08* packetData, s32 packetBytes);
    static s32 StaticProcessPacketFunction(void* context, s32 index, u16 packetSequence, u08* packetData, s32 packetBytes);
    static void* StaticAllocateFunction(void* context, u64 bytes);
    static void StaticFreeFunction(void* context, void* pointer);

  private:
    BaseClient(const BaseClient& other);
    const BaseClient& operator= (const BaseClient& other);

  private:
    ClientServerConfig m_config;         // client/server configuration
    Allocator* m_allocator;              // allocator passed to the client on creation
    Adapter* m_adapter;                  // adapter specifies the allocator to use, and the message factory class
    void* m_context;                     // context lets the user pass information to packet serialize functions
    u08* m_clientMemory;                 // memory backing the client allocator.  Allocated from m_allocator
    Allocator* m_clientAllocator;        // the client allocator, everything allocated between connect and disconnected is allocated and freed with this allocator
    reliable_endpoint_t* m_endpoint;     // reliable.io endpoint
    MessageFactory* m_messageFactory;    // client message factory.  Created and destroyed on each connection attempt
    Connection* m_connection;            // client connection for exchanging messages with the server
    NetworkSimulator* m_networkSimulator;// simulates packet loss, latency, jitter, ect. (optional)
    ClientState m_clientState;           // current client state (see IClient::GetClientState)
    s32 m_clientIndex;                   // client slot index on the server [0, maxClients-1]   -1 if not connected 
    f64 m_time;                          // current client time (see IClient::AdvanceTime)
    u08* m_packetBuffer;                 // buffer used to read and write packets

  };
}// namespace GG
