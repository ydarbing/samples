#pragma once

namespace GG
{
  class Message;
  struct NetworkInfo;

  enum ClientState
  {
    E_CLIENT_STATE_ERROR = -1,
    E_CLIENT_STATE_DISCONNECTED = 0,
    E_CLIENT_STATE_CONNECTING,
    E_CLIENT_STATE_CONNECTED,
  };



  // common interface for all clients
  class IClient
  {
  public:
    virtual ~IClient() {}
    /*
     *  Set context for reading or writing packets
     *  This is optional.  Lets you pass in a pointer to some structure that you want to have available when reading or writing packets via Stream::GetContext
     *  Typical use case is to pass in an array of min/max ranges for values determined by some data that is loaded from a toolchain vs. being known at compile time
     *  if you do use a context, make sure the same context data is set on client and server and includes a checksum of the context data in the protocol id
     */
    virtual void SetContext(void* context) = 0;
    /*
     * Disconnect from the server
     */
    virtual void Disconnect() = 0;
    /*
     *  Send packets to server
     */
    virtual void SendPackets() = 0;
    /*
    *  Receive packets from the server
    */
    virtual void ReceivePackets() = 0;
    /*
    *   Advance client time
    *   Call this at the end of each frame to advance client tim forward
    *   IMPORTANT:  Use a double (f64) for your time to maintain accuracy as time increases
    */ 
    virtual void AdvanceTime(f64 time) = 0;
    /*
    *  Is the client connecting to the server?
    *  True while client is negotiating connection with a server
    *  return - true if client is currently connecting to but not yet connected to a server
    */
    virtual bool IsConnecting()const = 0;
    /*
    *  Is the client connected to the server?
    *  True while client is connected to a server.  False while connecting to a server
    *  return - true if client is connected to a server
    */
    virtual bool IsConnected()const = 0;
    /*
    *  Is the client in a disconnected state?
    *  Disconnected state corresponds to the client being in the disconnected or in an error state.  Both are logically disconnected
    *  return - true if the client is disconnected
    */
    virtual bool IsDisconnected()const = 0;
    /*
    *  Is the client in an error state?
    *  When client disconnects because of an error, it enters into this error state
    *  return - true if client is in an error state
    */
    virtual bool ConnectionFailed()const = 0;
    /*
    *  Get current client state
    */
    virtual ClientState GetClientState()const = 0;
    /*
     * Get client index
     * Client index is the slot number that the client is occupying on the server
     * return - client index from [0, MAX_CLIENTS - 1] where MAX_CLIENTS is the number of client slots allocated on the srever in Server::Start
     */
    virtual s32 GetClientIndex()const = 0;
    /*
     *  Get client ID
     *  Client ID is a unique identifier of this client
     *  return - the client ID
     */
    virtual u64 GetClientId()const = 0;
    /*
    * Get the current clent time
    */
    virtual f64 GetTime()const = 0;
    /*
     * Create message of the specified type
     * type - the type of message to create.  the message corresponds to the message factory created by the adapter set on this client
     */
    virtual Message* CreateMessage(s32 type) = 0;
    /*
    * Helper function to allocate a data block
    * Typically used to create blocks of data to attach to block messages.  See BlockMessage for details
    * bytes - number of bytes to allocate
    * return - pointer to the data block.  Must be attached to a message via Client::AttachBlockToMessage, or freed via Client::FreeBlock
    */
    virtual u08* AllocateBlock(s32 bytes) = 0;
    /*
     * Attach data block to message
     * message - message to attach the block to.  This messag must be derived from BlockMessage
     * block - pointer to block of data to attach.  must be created via Client::AllocateBlock
     * bytes - length of the block of data in bytes
     */
    virtual void AttachBlockToMessage(Message* message, u08* block, s32 bytes) = 0;
    /*
    * Free a block of memory
    * block - the block of memory created by Client::AllocateBlock
    */
    virtual void FreeBlock(u08* block) = 0;
    /*
    * Can we send a message on a channel?
    * channelIndex - the channel index in range [0, numChannels - 1]
    * return - true if a message can be sent over the channel, false otherwise
    */
    virtual bool CanSendMessage(s32 channelIndex) const = 0;
    /*
    *  send a message on a channel
    *  channelIndex - the channel index in range [0, numChannels - 1]
    *  message - the message to send
    */
    virtual void SendMessage(s32 channelIndex, Message* message) = 0;
    /*
     * Receive a message from a channel
     * channelIndex - channel index in range [0. numChannels - 1]
     * return - the message received or nullptr if no message is available.  
     *          make sure to realease this message by calling Client::ReleaseMessage
     */
    virtual Message* ReceiveMessage(s32 channelIndex) = 0;
    /*
     * Release a message
     * Call this for messages received by Client::ReceiveMessage
     * message - message to release
     */
    virtual void ReleaseMessage(Message* message) = 0;
    /*
     *  Get client network info
     *  call this to receive info about the client network connection to the server.  ex. RTT, packet loss %, # of packets sent
     *  info - the struct to be filled with network info (out)
     */
    virtual void GetNetworkInfo(NetworkInfo& info)const = 0;
    /*
    *  Connect to server over loopback
    *  This allows you to have local clients connected to a server.  
    *  ex.  for integrated server of single player
    *  clientIndex - index of the client
    *  clientId - the unique client Id
    *  maxClients - max number of clients supported by the server
    */
    virtual void ConnectLoopback(s32 clientIndex, u64 clientId, s32 maxClients) = 0;
    /*
    * Disconnect from server over loopback
    */
    virtual void DisconnectLoopback() = 0;
    /*
    *  Is this a loopback client
    *  return - true if client is a loopback, false otherwise
    */
    virtual bool IsLoopback() const = 0;
    /*
    *  Process loopback packet
    *  Use this to pass packets from a server directly to the loopback client
    *  packetData - the packet data to process
    *  packetBytes - number of bytes of packet data
    *  packetSequence - the packet sequence number
    */
    virtual void ProcessLoopbackPacket(const u08* packetData, s32 packetBytes, u64 packetSequence) = 0;
  };

}// namespace GG
