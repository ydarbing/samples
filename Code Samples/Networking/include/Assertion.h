#pragma once

#include <cstdlib> // exit()
#include <cstring> // memcpy

namespace GG
{
  /**
  Set the log level.
  Valid log levels are: GG_LOG_LEVEL_NONE, GG_LOG_LEVEL_ERROR, GG_LOG_LEVEL_INFO and GG_LOG_LEVEL_DEBUG
  @param level The log level to set. Initially set to GG_LOG_LEVEL_NONE.
  */

  void gg_log_level(s32 level);

  /**
  Printf function used to emit logs.
  This function internally calls the printf callback set by the user.
  */

  void gg_printf(s32 level, const c08 * format, ...);

  extern void(*gg_assert_function)(const c08 *, const c08 *, const c08 * file, s32 line);

#ifndef NDEBUG
#define gg_assert( condition )                                                      \
do                                                                                  \
{                                                                                   \
    if ( !(condition) )                                                             \
    {                                                                               \
        gg_assert_function( #condition, __FUNCTION__, __FILE__, __LINE__ );         \
        exit(1);                                                                    \
    }                                                                               \
} while(0)
#else
#define gg_assert( ignore ) ((void)0)
#endif

} // namespace GG
