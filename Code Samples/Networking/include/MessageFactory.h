#pragma once

namespace GG
{
  class Message;
  class Allocator;

  enum MessageFactoryErrorLevel
  {
    E_MESSAGE_FACTORY_ERROR_NONE, // no error
    E_MESSAGE_FACTORY_ERROR_FAILED_TO_ALLOCATE_MESSAGE
  };


  /* defines the set of messae types that can be created

  You can derive a message factory yourself to create your own message types, or you can use these helper macros to do it for you:
    GG_MESSAGE_FACTORY_START
    GG_DECLARE_MESSAGE_TYPE
    GG_MESSAGE_FACTORY_FINISH

  */
  class MessageFactory
  {
  public:
    // Pass in number of message types for the message factory from the derived class
    // valid types are in [0, numTypes-1]
    MessageFactory(Allocator& allocator, s32 numTypes);

    virtual ~MessageFactory();

    /*
      Create Message by type
      IMPORTANT: Check the message pointer returned by this call. It can be NULL if there is no memory to create a message!
      Message return from this function have one reference added to them.  When finished with the message, pass it to MessageFactory::Release
    */
    Message* CreateMessage(s32 type);

    // add reference to a message
    void AcquireMessage(Message* message);

    void ReleaseMessage(Message* message);

    // Get number of message types supported by this message factory
    s32 GetNumTypes()const;

    // get allocator used to create message
    Allocator& GetAllocator();

    //Get the error level.
    //When used with a client or server, an error level on a message factory other than MESSAGE_FACTORY_ERROR_NONE triggers a client disconnect.
    MessageFactoryErrorLevel GetErrorLevel() const;
    // clear error back to no error
    void ClearErrorLevel();


  protected:
    // This method is overwritten to create messages by type
    virtual Message* CreateMessageInternal(s32 type);
    // set message type of a message
    void SetMessageType(Message* message, s32 type);

  private:
#if GG_DEBUG_MESSAGE_LEAKS
    std::map<void*, s32> allocated_messages; // set of allocated messages for this factory, used to track down message leaks
#endif
    Allocator* m_allocator;         // Allocator used to create messages
    s32 m_numTypes;                       // number of message types
    MessageFactoryErrorLevel m_errorLevel;
  };
}// namespace GG
