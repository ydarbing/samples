#pragma once



#define RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_SENT                          0
#define RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_RECEIVED                      1
#define RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_ACKED                         2
#define RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_STALE                         3
#define RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_INVALID                       4
#define RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_TOO_LARGE_TO_SEND             5
#define RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_TOO_LARGE_TO_RECEIVE          6
#define RELIABLE_ENDPOINT_COUNTER_NUM_FRAGMENTS_SENT                        7
#define RELIABLE_ENDPOINT_COUNTER_NUM_FRAGMENTS_RECEIVED                    8
#define RELIABLE_ENDPOINT_COUNTER_NUM_FRAGMENTS_INVALID                     9
#define RELIABLE_ENDPOINT_NUM_COUNTERS                                      10


#define RELIABLE_MAX_PACKET_HEADER_BYTES 9
#define RELIABLE_FRAGMENT_HEADER_BYTES 5

#define RELIABLE_OK         1
#define RELIABLE_ERROR      0

#ifndef RELAIBLE_ENABLE_TESTS
#define RELAIBLE_ENABLE_TESTS 1
#endif // #ifndef RELAIBLE_ENABLE_TESTS

namespace GG
{
  struct reliable_endpoint_t; // forward declare


  s32 reliable_init();
  void reliable_term();

  struct reliable_config_t
  {
    c08 name[256];
    void* context;
    s32 index;
    s32 maxPacketSize;
    s32 fragmentAbove;
    s32 maxFragments;
    s32 fragmentSize;
    s32 ackBufferSize;
    s32 sentPacketsBufferSize;
    s32 receivedPacketsBufferSize;
    s32 fragmentReassemblyBufferSize;
    f32 rttSmoothingFactor;
    f32 packetLossSmoothingFactor;
    f32 bandwidthSmoothingFactor;
    s32 packetHeaderSize;

    void* allocatorContext;

    void(*transmit_packet_function)(void*, s32, u16, u08*, s32);
    s32(*process_packet_function)(void*, s32, u16, u08*, s32);
    void* (*allocate_function)(void*, u64);
    void(*free_function)(void*, void*);
  };


  void reliable_default_config(reliable_config_t* config);

  reliable_endpoint_t* reliable_endpoint_create(reliable_config_t* config, f64 time);

  u16 reliable_endpoint_next_packet_sequence(reliable_endpoint_t* endpoint);

  void reliable_endpoint_send_packet(reliable_endpoint_t* endpoint, u08* packetData, s32 packetBytes);

  void reliable_endpoint_receive_packet(reliable_endpoint_t* endpoint, u08* packetData, s32 packetBytes);

  void reliable_endpoint_free_packet(reliable_endpoint_t* endpoint, void* packet);

  u16* reliable_endpoint_get_acks(reliable_endpoint_t* endpoint, s32* numAcks);

  void reliable_endpoint_clear_acks(reliable_endpoint_t* endpoint);

  void reliable_endpoint_reset(reliable_endpoint_t* endpoint);

  void reliable_endpoint_update(reliable_endpoint_t* endpoint, f64 time);

  f32 reliable_endpoint_rtt(reliable_endpoint_t* endpoint);

  f32 reliable_endpoint_packet_loss(reliable_endpoint_t* endpoint);

  void reliable_endpoint_bandwidth(reliable_endpoint_t* endpoint, f32* sentBandwidthKbps, f32* receivedBandwidthKbps, f32* ackedBandwidthKbps);

  const u64* reliable_endpoint_counters(reliable_endpoint_t* endpoint);

  void reliable_endpoint_destroy(reliable_endpoint_t* endpoint);

  void reliable_log_level(s32 level);
  void reliable_set_printf_function(s32(*function)(const c08*, ...));
  extern void(*reliable_assert_function)(const c08*, const c08*, const c08* file, s32 line);

#ifndef NDEBUG
#define reliable_assert( condition )                                                        \
do                                                                                          \
{                                                                                           \
    if ( !(condition) )                                                                     \
    {                                                                                       \
        reliable_assert_function( #condition, __FUNCTION__, __FILE__, __LINE__ );           \
        exit(1);                                                                            \
    }                                                                                       \
} while(0)
#else
#define reliable_assert( ignore ) ((void)0)
#endif

  void reliable_set_assert_function(void(*function)(const c08* /*condition*/,
                                                    const c08* /*function*/,
                                                    const c08* /*file*/,
                                                    s32 /*line*/));



#if RELAIBLE_ENABLE_TESTS
  void reliable_test();
#endif

}// namespace GG
