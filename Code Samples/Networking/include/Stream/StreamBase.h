#pragma once
#include "Allocator/Allocator.h"

namespace GG
{
  // functionality for all stream classes
  class StreamBase
  {
  public:
    explicit StreamBase(Allocator& allocator) : m_allocator(&allocator), m_context(nullptr)
    {}

    // set context of the stream
    // Used by a the library supply data that is needed to read and write packets
    void SetContext(void* context);
    // get context pointer of the stream.  May be nullptr
    void* GetContext(void)const;
    //Get the allocator set on the stream.
    //You can use this allocator to dynamically allocate memory while reading and writing packets.
    Allocator& GetAllocator(void)const;

  private:
    Allocator* m_allocator;
    void* m_context; // context pointer of the stream, may be nullptr
  };
  
}// namespace GG
