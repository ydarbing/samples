#pragma once

#include "Stream\StreamBase.h"
#include "BitReader.h"

namespace GG
{
  class Allocator;
  // stream class for reading bitpacked data
  // Generally you don't call methods on this class directly.  Use the serialize_* macros instead
  class StreamRead : public StreamBase
  {
  public:
    enum { IsWriting = 0 };
    enum { IsReading = 1 };

    // buffer - buffer to read from
    // bytes - number of bytes in buffer.  May be non-multiple of 4
    StreamRead(Allocator& allocator, const u08* buffer, s32 bytes) : StreamBase(allocator), m_reader(buffer, bytes)
    {
    }



    // all serialize functions return true if the serialize read succeeded, false otherwise

    bool SerializeInteger(s32& value, s32 min, s32 max);
    // serialze a number of bits
    // value - unsigned s32 value to serialze (nust be in range [0, (1 << bits) - 1])
    // bits - number of bits to write [1, 32]
    bool SerializeBits(u32& value, s32 bits);
    // serialize array of bytes
    // data - array of bytes to be written
    // bytes - number of bytes to write
    bool SerializeBytes(u08* data, s32 bytes);
    // serialze an align
    bool SerializeAlign();
    // how many bits are required to align right now
    s32 GetAlignBits()const;
    // Serialize a safety check to the stream
    // tracks down desyncs.  a check is written to the stream and on the other side if the check is not present it asserts and fails the serialize
    bool SerializeCheck();
    // Get number of bits read so far
    s32 GetBitsProcessed() const;
    // How many bytes read so far
    // returns number of ytes written, Effectively number of bits read, rounded up to the next byte where necessary.
    s32 GetBytesProcessed() const;

  private:
    BitReader m_reader;
  };


}// namespace GG
