#pragma once
#include "StreamBase.h"

namespace GG
{
  class Allocator;
    // Stream class for estimating how many bits it would take to serialize something.
  // This class acts like a bit writer (IsWriting is 1, IsReading is 0), but instead of writing data, it counts how many bits would be written.
  // It's used by the connection channel classes to work out how many messages will fit in the channel packet budget.
  // Note that when the serialization includes alignment to byte (see MeasureStream::SerializeAlign), this is an estimate and not an exact measurement. The estimate is guaranteed to be conservative.
  class StreamMeasure : public StreamBase
  {
  public:

    enum { IsWriting = 1 };
    enum { IsReading = 0 };

    explicit StreamMeasure(Allocator& allocator) : StreamBase(allocator), m_bitsWritten(0) {}

    bool SerializeInteger(s32 value, s32 min, s32 max);
    bool SerializeBits(u32 value, s32 bits);
    bool SerializeBytes(const u08* data, s32 bytes);
    bool SerializeAlign();
    // IMPORTANT: The number of bits required for alignment depends on where an object is written in the final bit stream, this measurement is conservative. 
    // Always returns worst case 7 bits.
    s32 GetAlignBits() const;
    bool SerializeCheck();
    s32 GetBitsProcessed() const;
    s32 GetBytesProcessed() const;

  private:
    s32 m_bitsWritten;
  };

}// namespace GG
