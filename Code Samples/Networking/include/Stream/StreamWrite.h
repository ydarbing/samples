#pragma once

#include "Stream\StreamBase.h"
#include "BitWriter.h"
#include "Allocator/Allocator.h"

namespace GG
{
  
  // stream class for writing bitpacked data
  // Generally you don't call methods on this class directly.  Use the serialize_* macros instead
  class StreamWrite :public StreamBase
  {
  public:
    enum { IsWriting = 1 };
    enum { IsReading = 0 };

    // buffer -  buffer to write to
    // bytes -  number of bytes in buffer, MUST BE multiple of 4
    StreamWrite(Allocator& allocator, u08* buffer, s32 bytes) : StreamBase(allocator), m_writer(buffer, bytes)
    {
    }

    // always serialze functions returns true, all checking performed by debug asserts on write
    bool SerializeInteger(s32 value, s32 min, s32 max);
    // serialze a number of bits
    // value - unsigned s32 value to serialze (nust be in range [0, (1 << bits) - 1])
    // bits - number of bits to write [1, 32]
    bool SerializeBits(u32 value, s32 bits);
    // serialize array of bytes
    // data - array of bytes to be written
    // bytes - number of bytes to write
    bool SerializeBytes(const u08* data, s32 bytes);
    // serialze an align
    bool SerializeAlign();
    // how many bits are required to align right now
    s32 GetAlignBits()const;
    // Serialize a safety check to the stream
    // tracks down desyncs.  a check is written to the stream and on the other side if the check is not present it asserts and fails the serialize
    bool SerializeCheck();
    // flush stream to memory after finish writing
    // always call after you finish writing and before you call WriteStream::GetData
    void Flush();
    // Get pointer to data written to stream
    // CALL WriteStream::Flush before you call this function
    const u08* GetData();
    // Get number of bits written so far
    s32 GetBitsProcessed() const;
    // How many bytes written so far
    // returns number of ytes written, Effectively the packet size
    s32 GetBytesProcessed() const;

  private:
    BitWriter m_writer; // used for all bitpacked write operations
  };

}// namespace GG
