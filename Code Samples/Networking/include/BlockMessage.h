#pragma once
#include "Message.h"
#include "Allocator/Allocator.h"

namespace GG
{
    class BlockMessage : public Message
  {
  public:
    // don't call this directly, use a message factory instead
    explicit BlockMessage() : Message(1), m_allocator(nullptr), m_blockData(nullptr), m_blockSize(0) {}

    // Attach block to this message
    // Can only attach one block, will assert if block is already attached
    void AttachBlock(Allocator& allocator, u08* blockData, s32 blockSize)
    {
      gg_assert(blockData);
      gg_assert(blockSize > 0);
      gg_assert(!m_blockData);
      m_allocator = &allocator;
      m_blockData = blockData;
      m_blockSize = blockSize;
    }
    // By doing this, you are responsible for copying the block pointer and allocator and making sure the block is freed
    // Could be used if you wanted to copy off the block and store it somewhere, without the cost of copy it
    void DetachBlock()
    {
      m_allocator = nullptr;
      m_blockData = nullptr;
      m_blockSize = 0;
    }
    // get allocator used to allocate the block
    Allocator* GetAllocator()
    {
      return m_allocator;
    }
    // get block data pointer
    u08* GetBlockData()
    {
      return m_blockData;
    }
    const u08* GetBlockData() const
    {
      return m_blockData;
    }
    s32 GetBlockSize() const
    {
      return m_blockSize;
    }

    // Templated serialize function for the block message. Doesn't do anything. The block data is serialized elsewhere.
    // You can override the serialize methods on a block message to implement your own serialize function. It's just like a regular message with a block attached to it.
    template <typename Stream>
    bool Serialize(Stream & stream) { (void)stream; return true; }

    GG_VIRTUAL_SERIALIZE_FUNCTIONS();


  protected:
    ~BlockMessage()
    {
      if (m_allocator)
      {
        GG_FREE(*m_allocator, m_blockData);
        m_blockSize = 0;
        m_allocator = nullptr;
      }
    }

  private:
    Allocator* m_allocator; // allocator for the block attached to message.  nullptr if no block attached
    u08* m_blockData; // nullptr if no block is attached
    s32 m_blockSize; // block size in bytes, 0 if no block is attached
  };
}// namespace GG
