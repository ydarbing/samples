#pragma once
 
#include "Allocator/Allocator.h"
#include "Utils.h"

namespace GG
{

  // Data structure that stores data indexed by sequence number
  // Entries may or may not exist.  If they don't, the sequence value for the entry at that index is set to 0xFFFFFFFF
  // This provides a constant time lookup for an entry by sequence number.  If the entry and sequence modulo buffer size doesn't have the same sequence number, that sequence number is not stored
  // Very useful and used as foundation of the packet level ack system and the reliable message send and receive queues
  template <typename T>
  class SequenceBuffer
  {
  public:
    SequenceBuffer(GG::Allocator& allocator, s32 size)
    {
      gg_assert(size > 0);
      m_allocator = &allocator;
      m_size = size;
      m_sequence = 0;
      m_entrySequence = (u32*)GG_ALLOCATE(allocator, sizeof(u32) * size);
      m_entries = (T*)GG_ALLOCATE(allocator, sizeof(T) * size);
      Reset();
    }

    ~SequenceBuffer()
    {
      gg_assert(m_allocator);
      GG_FREE(*m_allocator, m_entries);
      GG_FREE(*m_allocator, m_entrySequence);
      m_allocator = nullptr;
    }

    // Reset sequence buffer
    // removes all entries from the sequence buffer and restores it to the initial state
    void Reset()
    {
      m_sequence = 0;
      memset(m_entrySequence, 0xFF, sizeof(u32) * m_size);
    }

    // insert entry into sequence buffer
    // If another entry exists at the sequence modulo buffer size, it is overwritten
    // return - sequence buffer entry, which you must fill with your data. nullptr if a sequence buffer could not be added for your sequence number (ex. the given sequence number is too old)
    T* Insert(u16 sequence)
    {
      if (GG::SequenceGreaterThan(sequence + 1, m_sequence))
      {
        RemoveEntries(m_sequence, sequence);
        m_sequence = sequence + 1;
      }
      else if (GG::SequenceLessThan(sequence, m_sequence - m_size))
      {
        return nullptr;
      }

      const s32 index = sequence % m_size;
      m_entrySequence[index] = sequence;
      return &m_entries[index];
    }

    // remove entry from the sequence buffer
    void Remove(u16 sequence)
    {
      m_entrySequence[sequence % m_size] = 0xFFFFFFFF;
    }

    // Is the entry corresponding to the sequence number available? ex. Currently unoccupied
    // This works because older entries are automatically set back to unoccupied state as the sequence buffer advances forward
    // return - true if the sequence buffer entry is available, false if already occupied
    bool Available(u16 sequence)const
    {
      return m_entrySequence[sequence % m_size] == 0xFFFFFFFF;
    }

    // does an entry exist for a sequence number
    // return - true if entry exists for this sequence number
    bool Exists(u16 sequence) const
    {
      return m_entrySequence[sequence % m_size] == u32(sequence);
    }

    // Get the entry corresponding to a sequence number
    // return - the entry if it exists, nullptr if no entry is in the buffer at this sequence
    T* Find(u16 sequence)
    {
      const s32 index = sequence % m_size;
      if (m_entrySequence[index] == u32(sequence))
        return &m_entries[index];
      else
        return nullptr;
    }

    // Get the entry corresponding to a sequence number
    // return - the entry if it exists, nullptr if no entry is in the buffer at this sequence
    const T* Find(u16 sequence)const
    {
      const s32 index = sequence % m_size;
      if (m_entrySequence[index] == u32(sequence))
        return &m_entries[index];
      else
        return nullptr;
    }

    // Get the entry at the specified index
    // use this to iterate across entries in the sequence buffer
    // index - [0, GetSize()-1]
    // return - the entry if it exists, nullptr otherwise
    T* GetAtIndex(s32 index)
    {
      gg_assert(index >= 0);
      gg_assert(index < m_size);
      return m_entrySequence[index] != 0xFFFFFFFF ? &m_entries[index] : nullptr;
    }

    // Get the entry at the specified index
    // use this to iterate across entries in the sequence buffer
    // index - [0, GetSize()-1]
    // return - the entry if it exists, nullptr otherwise
    const T* GetAtIndex(s32 index)const
    {
      gg_assert(index >= 0);
      gg_assert(index < m_size);
      return m_entrySequence[index] != 0xFFFFFFFF ? &m_entries[index] : nullptr;
    }

    // get most recent sequence number added to the buffer
    // This sequence number can wrap around, so if you are at 65535 and add an entry for sequence 0, then 0 becomes the new "most recent" sequence number
    u16 GetSequence() const
    {
      return m_sequence;
    }

    // Get the entry index for a sequence number
    // This is just the sequence number modulo the seqeuence buffer size
    // return - sequence buffer index corresponding to the sequence number
    s32 GetIndex(u16 sequence)const
    {
      return sequence % m_size;
    }
    // get size of the sequence buffer
    // return - size of the sequence buffer (number of entries)
    s32 GetSize()const
    {
      return m_size;
    }

  protected:
    // Removes old entries as we advance the sequence buffer forward

    void RemoveEntries(s32 startSequence, s32 endSequence)
    {
      if (endSequence < startSequence)
      {
        endSequence += 65535;
      }
      gg_assert(endSequence >= startSequence);
      if (endSequence - startSequence < m_size)
      {
        for (s32 sequence = startSequence; sequence <= endSequence; ++sequence)
        {
          m_entrySequence[sequence % m_size] = 0xFFFFFFFF;
        }
      }
      else
      {
        for (s32 i = 0; i < m_size; ++i)
        {
          m_entrySequence[i] = 0xFFFFFFFF;
        }
      }
    }

  private:
    GG::Allocator* m_allocator;
    s32 m_size;                  // size of sequence buffer
    u16 m_sequence;         // most recent sequence number added to buffer
    u32* m_entrySequence;   // array of sequence numbers corresponding to each sequence buffer entry for fast lookup.  Set to 0xFFFFFFFF if no entry exists at that index
    T* m_entries;                // The sequence buffer entries.  This is where the data is stored per-entry.  Seperate from sequence numbers for fast lookup (hot/cold split) when the data per-sequence number is relatively large

    SequenceBuffer(const SequenceBuffer<T>& other);
    SequenceBuffer<T>& operator=(const SequenceBuffer<T>& other);
  };

}// namespace GG