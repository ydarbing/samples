#pragma once
#include "Allocator\Allocator.h"

namespace GG
{
  // simulates packet loss, latency, jitter and duplicate packets
  // works on packet send, so if you want 125ms of latency (round trip) you must add 125/2 = 62.5 ms of latency to each side
  class NetworkSimulator
  {
  public:
    NetworkSimulator(Allocator& allocator, s32 numPackets, f64 time);
    // any packet data still in simulator is destroyed
    ~NetworkSimulator();
    // set latency in ms.  Latency is added to packet send.  To simulate round trip of 150ms, add 75ms of latency to both side of the connection
    void SetLatency(f32 milliseconds);
    // Set amound of packet loss to apply on send
    // 0% - no packet loss
    // 100% -  all packets are dropped
    void SetPacketLoss(f32 percent);
    // Jitter is applied +/- this amount in milliseconds.  To be truly effective, jitter must be applied together with som latency
    void SetJitter(f32 milliseconds);
    // if the duplicate chance succeeds, a duplicate packet is added to the queue with a random delay of up to 1 second
    // 0% - no duplicate packets
    // 100% - all packets have a duplicate sent
    void SetDuplicates(f32 percent);
    // network simulator is actuve when packet loss, latency, jitter or duplicates are non-zero values
    // used by the transport to know whether it shuld shunt packets through the simulator or send them directly to the network
    bool IsActive() const;
    // queue a packet to send
    // Ownership of packet data pointer is NOT transferred to the network simulator, it makes of copy of the data instead
    // to - slot index the packet should be sent to
    // packetData - the packet data
    // packetBytes -  size of packet in bytes
    void SendPacket(s32 to, u08* packetData, s32 packetBytes);
    // receive packets sent to any address
    // YOU TAKE OWNERSHIP of the packet data you receive and are responsible for freeing it
    // maxPackets - max number of packets to receive
    // packetData - array of packet data pointers to be filled [out]
    // packetBytes - array of packet sizes to be filled [out]
    // to - array of indices to be filled [out]
    // return - number of packets received
    s32 ReceivePackets(s32 maxPackets, u08* packetData[], s32 packetBytes[], s32 to[]);
    // discard all packets in the network simulator
    // useful if simulator needs to be reset and used for another purpose
    void DiscardPackets();
    // Discard packets sent to a particular client index
    // called when a client disconnects from the server
    void DiscardClientPackets(s32 clientIndex);
    // Advance network simulator time
    // must call this regularly otherwise the network simulator won't work
    // time - the current time value (must be a double)
    void AdvanceTime(f64 time);
    // Get the allocator to use to free packet data
    // return - the allocator the packet data is allocated with
    Allocator& GetAllocator();
  protected:
    // Helper function to ipdate the active flag whenever network simulator settings are changed
    // active when any network conditions are non-zero.  Allows you to quickly check if network simulator is active and would actually do something
    void UpdateActive();

  private:
    Allocator* m_allocator; // allocator passed in to the constructor.  Used to allocate and free packet data
    f32 m_latency;          // latency in milliseconds
    f32 m_jitter;           // jitter in milliseconds +/-
    f32 m_packetLoss;       // packet loss percentage
    f32 m_duplicates;       // duplicate packet percentage
    bool m_active;          // true if any of the network settings above are enabled

    // a packet buffered in the network simulator
    struct PacketEntry
    {
      PacketEntry()
      {
        to = 0;
        deliveryTime = 0.0;
        packetData = nullptr;
        packetBytes = 0;
      }
      s32 to;          // index this packet should be sent to (for server -> client packets)
      f64 deliveryTime;// delivery time for this packet (seconds)
      u08* packetData; // packet data (owns this pointer)
      s32 packetBytes; // size of packet in bytes
    };

    f64 m_time;                  // current time from last call to advance time
    s32 m_currentIndex;          // current index in the packet entry array.  New packets are inserted here
    s32 m_numPacketEntries;      // number of elements in the packet entry array
    PacketEntry* m_packetEntries;// pointer to dynamically allocated packet entries.  This is where buffered packets are stored
  };

}// namespace GG