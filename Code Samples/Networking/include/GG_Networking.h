#pragma once

#include "CommonDefines.h"
#include "GG_NetworkingConstants.h"

#ifndef NDEBUG
#define GG_DEBUG_MEMORY_LEAKS         1
#define GG_DEBUG_MESSAGE_LEAKS        1
#define GG_DEBUG_MESSAGE_BUDGET       1
#else // #ifndef NDEBUG
#define GG_DEBUG_MEMORY_LEAKS         0
#define GG_DEBUG_MESSAGE_LEAKS        0
#define GG_DEBUG_MESSAGE_BUDGET       0
#endif // #ifndef NDEBUG

#define GG_ENABLE_LOGGING               1

#define GG_LOG_LEVEL_NONE               0
#define GG_LOG_LEVEL_ERROR              1
#define GG_LOG_LEVEL_INFO               2
#define GG_LOG_LEVEL_DEBUG              3

// platform detection                   
#define GG_PLATFORM_WINDOWS             1
#define GG_PLATFORM_MAC                 2
#define GG_PLATFORM_UNIX                3

#if defined(_WIN32)
#define GG_PLATFORM GG_PLATFORM_WINDOWS
#elif defined(__APPLE__)
#define GG_PLATFORM GG_PLATFORM_MAC
#else
#define GG_PLATFORM GG_PLATFORM_UNIX
#endif



#if !defined (GG_LITTLE_ENDIAN ) && !defined( GG_BIG_ENDIAN )

#ifdef __BYTE_ORDER__
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#define GG_LITTLE_ENDIAN 1
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#define GG_BIG_ENDIAN 1
#else
#error Unknown machine endianess detected. User needs to define GG_LITTLE_ENDIAN or GG_BIG_ENDIAN.
#endif // __BYTE_ORDER__

// Detect with GLIBC's endian.h
#elif defined(__GLIBC__)
#include <endian.h>
#if (__BYTE_ORDER == __LITTLE_ENDIAN)
#define GG_LITTLE_ENDIAN 1
#elif (__BYTE_ORDER == __BIG_ENDIAN)
#define GG_BIG_ENDIAN 1
#else
#error Unknown machine endianess detected. User needs to define GG_LITTLE_ENDIAN or GG_BIG_ENDIAN.
#endif // __BYTE_ORDER

// Detect with _LITTLE_ENDIAN and _BIG_ENDIAN macro
#elif defined(_LITTLE_ENDIAN) && !defined(_BIG_ENDIAN)
#define GG_LITTLE_ENDIAN 1
#elif defined(_BIG_ENDIAN) && !defined(_LITTLE_ENDIAN)
#define GG_BIG_ENDIAN 1

// Detect with architecture macros
#elif    defined(__sparc)     || defined(__sparc__)                           \
        || defined(_POWER)      || defined(__powerpc__)                         \
        || defined(__ppc__)     || defined(__hpux)      || defined(__hppa)      \
        || defined(_MIPSEB)     || defined(_POWER)      || defined(__s390__)
#define GG_BIG_ENDIAN 1
#elif    defined(__i386__)    || defined(__alpha__)   || defined(__ia64)      \
        || defined(__ia64__)    || defined(_M_IX86)     || defined(_M_IA64)     \
        || defined(_M_ALPHA)    || defined(__amd64)     || defined(__amd64__)   \
        || defined(_M_AMD64)    || defined(__x86_64)    || defined(__x86_64__)  \
        || defined(_M_X64)      || defined(__bfin__)
#define GG_LITTLE_ENDIAN 1
#elif defined(_MSC_VER) && defined(_M_ARM)
#define GG_LITTLE_ENDIAN 1
#else
#error Unknown machine endianess detected. User needs to define GG_LITTLE_ENDIAN or GG_BIG_ENDIAN. 
#endif
#endif

#ifndef GG_LITTLE_ENDIAN
#define GG_LITTLE_ENDIAN 0
#endif

#ifndef GG_BIG_ENDIAN
#define GG_BIG_ENDIAN 0
#endif


#ifndef GG_DEFAULT_TIMEOUT
#define GG_DEFAULT_TIMEOUT 5
#endif

#if !defined( GG_WITH_MBEDTLS )
#define GG_WITH_MBEDTLS 1
#endif 

#if GG_DEBUG_MESSAGE_LEAKS
#include <map>
#endif // #if GG_DEBUG_MESSAGE_LEAKS


#if GG_PLATFORM == GG_PLATFORM_WINDOWS  
#include <winsock2.h>  
#include <ws2ipdef.h> 
#include <ws2tcpip.h> //socklen_t
typedef s32 socklen_t;
typedef SOCKET Socket;
//#pragma comment (lib, "ws2_32.lib")
#pragma comment( lib, "wsock32.lib" )
#elif GG_PLATFORM == GG_PLATFORM_MAC ||  GG_PLATFORM == GG_PLATFORM_UNIX  
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <fcntl.h>  

#else
#error unknown platform!
#endif


// windows 
#ifdef SendMessage
#undef SendMessage
#endif