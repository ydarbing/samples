#pragma once
 

namespace GG
{

  // Macro for creating a new object instance with a GG allocator.
#define GG_NEW( a, T, ... ) ( new ( (a).Allocate( sizeof(T), __FILE__, __LINE__ ) ) T(__VA_ARGS__) )

    // Macro for deleting an object created with a GG allocator.
#define GG_DELETE( a, T, p ) do { if (p) { (p)->~T(); (a).Free( p, __FILE__, __LINE__ ); p = NULL; } } while (0)    

    // Macro for allocating a block of memory with a GG allocator. 
#define GG_ALLOCATE( a, bytes ) (a).Allocate( (bytes), __FILE__, __LINE__ )

    // Macro for freeing a block of memory created with a GG allocator.
#define GG_FREE( a, p ) do { if ( p ) { (a).Free( p, __FILE__, __LINE__ ); p = NULL; } } while(0)


  enum AllocatorErrorLevel
  {
    E_ALLOCATOR_ERROR_NONE = 0,      // No error
    E_ALLOCATOR_ERROR_OUT_OF_MEMORY  // The allocator is out of memory!
  };
  
  // Helper function to convert an allocator error to a user friendly string.
  inline const char * GetAllocatorErrorString(AllocatorErrorLevel error)
  {
    switch (error)
    {
    case E_ALLOCATOR_ERROR_NONE:                  return "none";
    case E_ALLOCATOR_ERROR_OUT_OF_MEMORY:         return "out of memory";
    default:
      gg_assert(false);
      return "(unknown)";
    }
  }

#if GG_DEBUG_MEMORY_LEAKS
  // Debug structure used to track allocations and find memory leaks.
  // Active in debug build only. Disabled in release builds for performance reasons.
  struct AllocatorEntry
  {
    size_t size;         // size of the allocation in bytes
    const char * file;   // Filename of source code file that made the allocation
    int line;            // Line number in source code where the allocation was made
  };

#endif // #if GG_DEBUG_MEMORY_LEAKS

  // Common functionality to all allocators
  // IMPORTANT: This allocator is not thread safe.  Only call it from one thread
  class Allocator
  {

  public:
    // sets error level to E_ALLOCATOR_ERROR_NONE
    Allocator();
    // Make sure all allocations made from this allocator are freed before you destroy this allocator
    // in debug, validates this is true walks the map of allocator entries.  Any outstanding entries are considered memory leaks and printed to stdout
    virtual ~Allocator();
    // Allocate block of memory
    // IMPORTANT: dont call this directly.  USE GG_NEW or GG_ALLOCATE macros instead
    //   because they automatically pass in the source filename and line number for you
    // return - block of memory of the requested size, nullptr if allocation could not be performed
    virtual void * Allocate(size_t size, const char * file, int line) = 0;

    // Free block of memory
    // IMPORTANT: dont call this directly.  USE GG_DELETE or GG_FREE macros instead
    //   because they automatically pass in the source filename and line number for you
    // return - block of memory of the requested size, nullptr if allocation could not be performed
    virtual void Free(void * p, const char * file, int line) = 0;
    // Use this to check if an allocation has failed.  Used in client/server to disconnect a client with a failed allocation
    AllocatorErrorLevel GetErrorLevel() const { return m_errorLevel; }
    // clear allocator error level back to default
    void ClearError() { m_errorLevel = E_ALLOCATOR_ERROR_NONE; }
  
  protected:
    // For correct client/server behavior when an allocation fails, make sure you call this function to set the error level to E_ALLOCATOR_ERROR_FAILED_TO_ALLOCATE
    void SetErrorLevel(AllocatorErrorLevel errorLevel);
    // Call this to track an allocation made by your derived allocator class
    // in debug, tracked allocations are automatically check for leaks when allocator is destroyed
    void TrackAlloc(void * p, size_t size, const char * file, int line);
    // call this to track a free made by your derived allocator class
    // in debug any allocation tracked without a corresponding free is considered a memory leak when the allocator is destroyed
    void TrackFree(void * p, const char * file, int line);
    AllocatorErrorLevel m_errorLevel;

#if GG_DEBUG_MEMORY_LEAKS
    std::map<void*, AllocatorEntry> m_allocMap; // Debug only data structure used to find and report memory leaks.
#endif // #if GG_DEBUG_MEMORY_LEAKS

  private:
    Allocator(const Allocator & other);
    Allocator & operator = (const Allocator & other);
  };
}// namespace GG
