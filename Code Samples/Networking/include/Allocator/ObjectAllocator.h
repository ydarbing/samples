#pragma once

#ifdef _MSC_VER
#pragma warning( disable : 4290 ) // suppress warning: C++ Exception Specification ignored
#pragma warning( disable : 4996 ) // stop wanting me to use strcpy_s
#endif

#include <string>
#include <iostream>

// If the client doesn't specify these:
static const s32 DEFAULT_OBJECTS_PER_PAGE = 4;  
static const s32 DEFAULT_MAX_PAGES = 3;

class OAException
{
  public:
      // Possible exception codes
    enum OA_EXCEPTION 
    {
      E_NO_MEMORY,      // out of physical memory (operator new fails)
      E_NO_PAGES,       // out of logical memory (max pages has been reached)
      E_BAD_BOUNDARY,   // block address is on a page, but not on any block-boundary
      E_MULTIPLE_FREE,  // block has already been freed
      E_CORRUPTED_BLOCK // block has been corrupted (pad bytes have been overwritten)
    };

    OAException(OA_EXCEPTION ErrCode, const istring& Message) : error_code_(ErrCode), message_(Message) {};

    virtual ~OAException() {
    }

    OA_EXCEPTION code(void) const { 
      return error_code_; 
    }

    virtual const c08 *what(void) const {
      return message_.c_str();
    }
  private:  
    OA_EXCEPTION error_code_;
    istring message_;
};

// Allocator configuration parameters
struct OAConfig
{
  static const size_t BASIC_HEADER_SIZE = sizeof(u32) + 1; // allocation number + flags
  static const size_t EXTERNAL_HEADER_SIZE = sizeof(void*);     // just a pointer

  enum HBLOCK_TYPE{hbNone, hbBasic, hbExtended, hbExternal};
  struct HeaderBlockInfo
  {
    HBLOCK_TYPE type_;
    size_t size_;
    size_t additional_;
    HeaderBlockInfo(HBLOCK_TYPE type = hbNone, unsigned additional = 0) : type_(type), size_(0), additional_(additional)
    {
      if (type_ == hbBasic)
        size_ = BASIC_HEADER_SIZE;
      else if (type_ == hbExtended) // alloc # + use counter + flag byte + user-defined
        size_ = sizeof(u32) + sizeof(u16) + sizeof(c08) + additional_;
      else if (type_ == hbExternal)
        size_ = EXTERNAL_HEADER_SIZE;
    };
  };

  OAConfig(bool UseCPPMemManager = false,
           u32 ObjectsPerPage = DEFAULT_OBJECTS_PER_PAGE, 
           u32 MaxPages = DEFAULT_MAX_PAGES, 
           bool DebugOn = false, 
           u32 PadBytes = 0,
           const HeaderBlockInfo &HBInfo = HeaderBlockInfo(),
           u32 Alignment = 0) : UseCPPMemManager_(UseCPPMemManager),
                                     ObjectsPerPage_(ObjectsPerPage), 
                                     MaxPages_(MaxPages), 
                                     DebugOn_(DebugOn), 
                                     PadBytes_(PadBytes),
                                     HBlockInfo_(HBInfo),
                                     Alignment_(Alignment)
  {
    HBlockInfo_ = HBInfo;
    LeftAlignSize_ = 0;  
    InterAlignSize_ = 0;
  }

  bool UseCPPMemManager_;   // by-pass the functionality of the OA and use new/delete
  u32 ObjectsPerPage_; // number of objects on each page
  u32 MaxPages_;       // maximum number of pages the OA can allocate (0=unlimited)
  bool DebugOn_;            // enable/disable debugging code (signatures, checks, etc.)
  u32 PadBytes_;          // size of the left/right padding for each block
  HeaderBlockInfo HBlockInfo_; // size of the header for each block (0=no headers)
  u32 Alignment_;      // address alignment of each block

  u32 LeftAlignSize_;  // number of alignment bytes required to align first block
  u32 InterAlignSize_; // number of alignment bytes required between remaining blocks
};

// Allocator statistical info
struct OAStats
{
  OAStats(void) : ObjectSize_(0), PageSize_(0), FreeObjects_(0), ObjectsInUse_(0), PagesInUse_(0),
                  MostObjects_(0), Allocations_(0), Deallocations_(0) {};

  size_t ObjectSize_;      // size of each object
  size_t PageSize_;        // size of a page including all headers, padding, etc.
  u32 FreeObjects_;   // number of objects on the free list
  u32 ObjectsInUse_;  // number of objects in use by client
  u32 PagesInUse_;    // number of pages allocated
  u32 MostObjects_;   // most objects in use by client at one time
  u32 Allocations_;   // total requests to allocate memory
  u32 Deallocations_; // total requests to free memory
};

// This allows us to easily treat raw objects as nodes in a linked list
struct GenericObject
{
  GenericObject *Next;
};

struct MemBlockInfo
{
  bool in_use;        // Is the block free or in use?
  u32 alloc_num; // The allocation number (count) of this block
  c08 *label;        // A dynamically allocated NUL-terminated string
};

// This memory manager class 
class Allocator
{
  public:
      // Defined by the client (pointer to a block, size of block)
    typedef void (*DUMPCALLBACK)(const void *, size_t);
    typedef void (*VALIDATECALLBACK)(const void *, size_t);
      // Predefined values for memory signatures
    static const u08 UNALLOCATED_PATTERN = 0xAA;
    static const u08 ALLOCATED_PATTERN   = 0xBB;
    static const u08 FREED_PATTERN       = 0xCC;
    static const u08 PAD_PATTERN         = 0xDD;
    static const u08 ALIGN_PATTERN       = 0xEE;
      // Creates the ObjectManager per the specified values
      // Throws an exception if the construction fails. (Memory allocation problem)
    Allocator(size_t ObjectSize, const OAConfig& config) throw(OAException);
      // Destroys the ObjectManager (never throws)
    ~Allocator() throw();
      // Take an object from the free list and give it to the client (simulates new)
      // Throws an exception if the object can't be allocated. (Memory allocation problem)
    void* Allocate(const c08* label = 0) throw(OAException);
      // Returns an object to the free list for the client (simulates delete)
      // Throws an exception if the the object can't be freed. (Invalid object)
    void Free(void* Object) throw(OAException);
      // Calls the callback fn for each block still in use
    u32 DumpMemoryInUse(DUMPCALLBACK fn) const;
      // Calls the callback fn for each block that is potentially corrupted
    u32 ValidatePages(VALIDATECALLBACK fn) const;
      // Frees all empty pages (extra credit)
    u32 FreeEmptyPages(void);
      // Returns true if FreeEmptyPages and alignments are implemented
    static bool ImplementedExtraCredit(void);

      // Testing/Debugging/Statistic methods
    void SetDebugState(bool State);       // true=enable, false=disable
    const void* GetFreeList(void) const;  // returns a pointer to the internal free list
    const void* GetPageList(void) const;  // returns a pointer to the internal page list
    OAConfig GetConfig(void) const;       // returns the configuration parameters
    OAStats GetStats(void) const;         // returns the statistics for the allocator

  private:
      // Make private to prevent copy construction and assignment
    Allocator(const Allocator &oa);
    Allocator &operator=(const Allocator &oa);

    void ActuallyAllocatePage(void) throw(OAException);
    void AllocateExternalHeader(void* objStart, const c08* label);
    void FreeExternalHeader(void* obj);
    bool OnFreeList(void* obj)const;
    GenericObject* GetPageOfObject(void* obj);

    void WriteNewPageSignatures(void* page);
    // this is only called twice, once when allocing to set the bit
    // and once when freeing to set it back to 0
    void ToggleHeaderSignatures(void* obj);

    bool OnBoundry(GenericObject* page, void* obj);
    bool DoubleFreeCheck(void* obj);
    bool IsHeaderFlagOn(c08* temp)const;
    s32  PaddingCheck(void* obj)const;

    u32  ValidateObjectsOnPage(GenericObject* page, VALIDATECALLBACK fn)const;
    void UsedObjectsOnPage(GenericObject* page, DUMPCALLBACK fn)const;
    
  private:
      // Other private fields and methods..
    GenericObject* m_freeList;    // pointer to the linked list of free blocks
    GenericObject* m_pageList;    // pointer to the allocated pages of memory
    OAConfig  m_Config;    // configuration parameters
    OAStats   m_OAStats;     // accumulating statistics
    // these are used for the poiner movement when allocating
    size_t m_left;
    size_t m_inner;
};

