#pragma once

#include "Allocator\Allocator.h"


/*
Initialize the GG library.
Call this before calling any GG library functions.
returns - True if the library was successfully initialized, false otherwise.
*/
bool InitializeGG();

/*
Shutdown the GG library.
Call this after you finish using the library and it will run some checks for you (for example, checking for memory leaks in debug build).
*/
void ShutdownGG();


namespace GG
{

  // Get the default allocator.
  // Use this allocator when you just want to use malloc/free, but in the form of a GG allocator.
  // This allocator instance is created inside InitializeGG and destroyed in ShutdownGG.
  // In debug build, it will automatically check for memory leaks and print them out for you when you shutdown the library.
  // returns - The default allocator instances backed by malloc and free.
  class Allocator& GetDefaultAllocator();



  // default allocator based around malloc and free
  class DefaultAllocator : public Allocator
  {
  public:
    DefaultAllocator() {}
    // Allocates a block of memory using malloc
    // IMPORTANT: dont call this directly.  USE GG_NEW or GG_ALLOCATE macros instead
    //   because they automatically pass in the source filename and line number for you
    // size - Size of the block of memory to allocate in bytes
    // return - block of memory of the requested size, nullptr if allocation could not be performed
    void* Allocate(size_t size, const c08* file, s32 line);
    // Free block of memory
    // IMPORTANT: dont call this directly.  USE GG_DELETE or GG_FREE macros instead
    //   because they automatically pass in the source filename and line number for you
    // return - block of memory of the requested size, nullptr if allocation could not be performed
    void Free(void* p, const c08* file, int line);

  private:
    DefaultAllocator(const DefaultAllocator& other);
    DefaultAllocator& operator=(const DefaultAllocator& other);
  };
}// namespace GG