#pragma once
#include"Allocator\Allocator.h"

#include "Allocator\tlsf.h"

namespace GG
{
  typedef void* tlsf_t;

  // An allocator built on the TLSF allocator implementation by Matt Conte. 
  // Fast allocator that supports multiple heaps. Used inside the GG server to silo allocations for each client to their own heap.
  class TLSF_Allocator : public Allocator
  {
  public:
    // If you want to integrate your own allocator with GG for use with the client and server, this class is a good template to start from. 
    // Make sure your constructor has the same signature as this one, and it will work with the GG_SERVER_ALLOCATOR and GG_CLIENT_ALLOCATOR helper macros.
    // memory - Block of memory in which the allocator will work.This block must remain valid while this allocator exists.The allocator does not assume ownership of it, you must free it elsewhere, if necessary.
    // size - Size of the block of memory(bytes).The maximum amount of memory you can allocate will be less, due to allocator overhead.
    TLSF_Allocator(void* memory, size_t size);

    ~TLSF_Allocator();

    // Allocates a block of memory using malloc
    // IMPORTANT: dont call this directly.  USE GG_NEW or GG_ALLOCATE macros instead
    //   because they automatically pass in the source filename and line number for you
    // size - Size of the block of memory to allocate in bytes
    // return - block of memory of the requested size, nullptr if allocation could not be performed
    void* Allocate(size_t size, const c08* file, s32 line);
    // Free block of memory
    // IMPORTANT: dont call this directly.  USE GG_DELETE or GG_FREE macros instead
    //   because they automatically pass in the source filename and line number for you
    // return - block of memory of the requested size, nullptr if allocation could not be performed
    void Free(void* p, const c08* file, int line);

  private:
    tlsf_t m_tlsf; // TLSF allocator instance backing this allocator

    TLSF_Allocator(const TLSF_Allocator& other);
    TLSF_Allocator& operator=(const TLSF_Allocator &other);
  };

} // namespace GG
