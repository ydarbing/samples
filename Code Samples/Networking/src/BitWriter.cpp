#include "NetworkPrecompiled.h"
#include "BitWriter.h"
#include "Utils.h"
 


namespace GG
{
  BitWriter::BitWriter(void * data, s32 bytes) : m_data((u32*)data), m_numWords(bytes / 4)
  {
    gg_assert(data);
    gg_assert((bytes % 4) == 0);
    m_numBits = m_numWords * 32;
    m_bitsWritten = 0;
    m_wordIndex = 0;
    m_scratch = 0;
    m_scratchbits = 0;
  }
    
    
  void BitWriter::WriteBits(u32 value, s32 bits)
  {
    gg_assert(bits > 0);
    gg_assert(bits <= 32);
    gg_assert(m_bitsWritten + bits <= m_numBits);
    gg_assert(u64(value) <= ((1ULL << bits) - 1));

    m_scratch |= u64(value) << m_scratchbits;

    m_scratchbits += bits;
    if (m_scratchbits >= 32)
    {
      gg_assert(m_wordIndex < m_numWords);
      m_data[m_wordIndex] = HostToNetwork(u32(m_scratch & 0xFFFFFFFF));
      m_scratch >>= 32;
      m_scratchbits -= 32;
      ++m_wordIndex;
    }
    m_bitsWritten += bits;
  }

  void BitWriter::WriteAlign()
  {
    const s32 remainderBits = m_bitsWritten % 8;
    if (remainderBits != 0)
    {
      u32 zero = 0;
      WriteBits(zero, 8 - remainderBits);
      gg_assert((m_bitsWritten % 8) == 0);
    }
  }

  void BitWriter::WriteBytes(const u08 * dataToWrite, s32 numBytesToWrite)
  {
    gg_assert(GetAlignBits() == 0);
    gg_assert(m_bitsWritten + (numBytesToWrite * 8) <= m_numBits);
    gg_assert((m_bitsWritten % 32) == 0 || (m_bitsWritten % 32) == 8 || (m_bitsWritten % 32) == 16 || (m_bitsWritten % 32) == 24);

    s32 headBytes = (4 - (m_bitsWritten % 32) / 8) % 4;
    if (headBytes > numBytesToWrite)
      headBytes = numBytesToWrite;

    for (s32 i = 0; i < headBytes; ++i)
      WriteBits(dataToWrite[i], 8);
    if (headBytes == numBytesToWrite)
      return;

    FlushBits();

    gg_assert(GetAlignBits() == 0);

    s32 numWords = (numBytesToWrite - headBytes) / 4;
    if (numWords > 0)
    {
      gg_assert((m_bitsWritten % 32) == 0);
      memcpy(&m_data[m_wordIndex], dataToWrite + headBytes, numWords * 4);
      m_bitsWritten += numWords * 32;
      m_wordIndex += numWords;
      m_scratch = 0;
    }

    gg_assert(GetAlignBits() == 0);

    s32 tailStart = headBytes + numWords * 4;
    s32 tailBytes = numBytesToWrite - tailStart;
    gg_assert(tailBytes >= 0 && tailBytes < 4);
    for (s32 i = 0; i < tailBytes; ++i)
      WriteBits(dataToWrite[tailStart + i], 8);

    gg_assert(GetAlignBits() == 0);
    gg_assert(headBytes + numWords * 4 + tailBytes == numBytesToWrite);

  }

  void BitWriter::FlushBits()
  {
    if (m_scratchbits != 0)
    {
      gg_assert( m_scratchbits <= 32);
      gg_assert( m_wordIndex < m_numWords);
      m_data[m_wordIndex] = HostToNetwork(u32(m_scratch & 0xFFFFFFFF));
      m_scratch >>= 32;
      m_scratchbits = 0;
      ++m_wordIndex;
    }
  }

  s32 BitWriter::GetAlignBits() const
  {
    return (8 - (m_bitsWritten % 8)) % 8;
  }

  s32 BitWriter::GetBitsWritten() const
  {
    return m_bitsWritten;
  }

  s32 BitWriter::GetBitsAvailable() const
  {
    return m_numBits - m_bitsWritten;
  }

  const u08* BitWriter::GetData() const
  {
    return (u08*) m_data;
  }

  s32 BitWriter::GetBytesWritten() const
  {
    return (m_bitsWritten + 7) / 8;
  }
}// namespace GG
