#include "NetworkPrecompiled.h"
#include "Assertion.h"


namespace GG
{

  static int logLevel = 0;
  static s32(*printf_function)(const c08*, ...) = printf;



  void gg_log_level(s32 level)
  {
    logLevel = level;
    //netcode_log_level(level);
    //reliable_log_level(level);
  }

  static void default_assert_handler(const c08* condition, const c08* function, const c08* file, s32 line)
  {
    printf("assert failed: ( %s ), function %s, file %s, line %d\n", condition, function, file, line);
#if defined( __GNUC__ )
    __builtin_trap();
#elif defined( _MSC_VER )
    __debugbreak();
#endif
  }

  void(*gg_assert_function)(const char *, const char *, const char * file, int line) = default_assert_handler;

  void gg_set_printf_function(s32(*function)(const c08*, ...))
  {
    gg_assert(function);
    printf_function = function;
    //netcode_set_printf_function(function);
    //reliable_set_printf_function(function);
  }

  void gg_set_assert_function(void(*function)(const c08*, const c08*, const c08* file, s32 line))
  {
    gg_assert_function = function;
    //netcode_set_assert_function(function);
    //reliable_set_assert_function(function);
  }




#if GG_ENABLE_LOGGING

  void gg_printf(s32 level, const c08* format, ...)
  {
    if (level > logLevel)
      return;
    va_list args;
    va_start(args, format);
    c08 buffer[4 * 1024];
    vsprintf(buffer, format, args);
    printf_function("%s", buffer);
    va_end(args);
  }

#else // #if GG_ENABLE_LOGGING

  void gg_printf(int level, const char * format, ...)
  {
    (void)level;
    (void)format;
  }

#endif // #if GG_ENABLE_LOGGING


}// namespace GG