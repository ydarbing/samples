#include "NetworkPrecompiled.h"
#include "BitReader.h"
#include "Utils.h" // NetworkToHost
 

namespace GG
{
#ifndef NDEBUG
  BitReader::BitReader(const void * data, s32 bytes) : 
    m_data((const u32*)data), m_numBytes(bytes), m_numWords((bytes + 3) / 4)
#else // #ifndef NDEBUG
  BitReader::BitReader(const void * data, s32 bytes) : 
    m_data((const u32*)data), m_numBytes(bytes)
#endif // #ifndef NDEBUG
  {
    gg_assert(data);
    m_numBits = m_numBytes * 8;
    m_bitsRead = 0;
    m_scratch = 0;
    m_scratchBits = 0;
    m_wordIndex = 0;
  }

  bool BitReader::WouldReadPastEnd(s32 bits) const
  {
    return (m_bitsRead + bits) > m_numBits;
  }

  u32 BitReader::ReadBits(s32 bits)
  {
    gg_assert(bits > 0);
    gg_assert(bits <= 32);
    gg_assert(m_bitsRead + bits <= m_numBits);

    m_bitsRead += bits;

    gg_assert(m_scratchBits >= 0 && m_scratchBits <= 64);

    if (m_scratchBits < bits)
    {
      gg_assert(m_wordIndex < m_numWords);
      m_scratch |= u64(NetworkToHost(m_data[m_wordIndex])) << m_scratchBits;
      m_scratchBits += 32;
      ++m_wordIndex;
    }

    gg_assert(m_scratchBits >= bits);

    const u32 output = m_scratch & ((u64(1) << bits) - 1);
    m_scratch >>= bits;
    m_scratchBits -= bits;

    return output;
  }

  bool BitReader::ReadAlign()
  {
    const s32 remainderBits = m_bitsRead % 8;
    
    if (remainderBits != 0)
    {
      u32 value = ReadBits(8 - remainderBits);
      gg_assert(m_bitsRead % 8 == 0);
      if (value != 0)
        return false;
    }
    return true;
  }

  void BitReader::ReadBytes(u08 * data, s32 bytes)
  {
    gg_assert(GetAlignBits() == 0);
    gg_assert(m_bitsRead + bytes * 8 <= m_numBits);
    gg_assert((m_bitsRead % 32) == 0 
              || (m_bitsRead % 32) == 8 
              || (m_bitsRead % 32) == 16 
              || (m_bitsRead % 32) == 24);

    s32 headBytes = (4 - (m_bitsRead % 32) / 8) % 4;
    if (headBytes > bytes)
      headBytes = bytes;

    for (s32 i = 0; i < headBytes; ++i)
      data[i] = (u08)ReadBits(8);
    if (headBytes == bytes)
      return;

    gg_assert(GetAlignBits() == 0);

    s32 numWords = (bytes - headBytes) / 4;
    if (numWords > 0)
    {
      gg_assert((m_bitsRead % 32) == 0);
      memcpy(data + headBytes, &m_data[m_wordIndex], numWords * 4);
      m_bitsRead += numWords * 32;
      m_wordIndex += numWords;
      m_scratchBits = 0;
    }

    gg_assert(GetAlignBits() == 0);

    s32 tailStart = headBytes + numWords * 4;
    s32 tailBytes = bytes - tailStart;
    gg_assert(tailBytes >= 0 && tailBytes < 4);
    for (s32 i = 0; i < tailBytes; ++i)
      data[tailStart + i] = (u08)ReadBits(8);

    gg_assert(GetAlignBits() == 0);
    gg_assert(headBytes + numWords * 4 + tailBytes == bytes);
  }

  s32 BitReader::GetAlignBits() const
  {
    return (8 - m_bitsRead % 8) % 8;
  }

  s32 BitReader::GetBitsRead() const
  {
    return m_bitsRead;
  }

  s32 BitReader::GetBitsRemaining() const
  {
    return m_numBits - m_bitsRead;
  }



}//namespace GG
