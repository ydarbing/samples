#include "NetworkPrecompiled.h"
#include "MessageFactory.h"
#include "Message.h"
#include "Allocator/Allocator.h"

namespace GG
{
  MessageFactory::MessageFactory(Allocator& allocator, s32 numTypes)
  {
    m_allocator = &allocator;
    m_numTypes = numTypes;
    m_errorLevel = E_MESSAGE_FACTORY_ERROR_NONE;
  }

  MessageFactory::~MessageFactory()
  {
    gg_assert(m_allocator);
    m_allocator = nullptr;

#if GG_DEBUG_MESSAGE_LEAKS
    if (allocated_messages.size())
    {
      printf("You leaked messages!\n");
      printf("%d messages leaked\n", SCAST(s32, allocated_messages.size()));
      typedef std::map<void*, s32>::iterator itor_type;
      for (itor_type i = allocated_messages.begin(); i != allocated_messages.end(); ++i)
      {
        Message * message = SCAST(Message*, i->first);
        printf("leaked message %p (type %d, refcount %d)\n", message, message->GetType(), message->GetRefCount());
      }
      exit(1);
    }
#endif
  }


  Message* MessageFactory::CreateMessage(s32 type)
  {
    gg_assert(type >= 0);
    gg_assert(type < m_numTypes);
    Message* message = CreateMessageInternal(type);
    if (!message)
    {
      m_errorLevel = E_MESSAGE_FACTORY_ERROR_FAILED_TO_ALLOCATE_MESSAGE;
      return nullptr;
    }

#if GG_DEBUG_MESSAGE_LEAKS
    allocated_messages[message] = 1;
    gg_assert(allocated_messages.find(message) != allocated_messages.end());
#endif // #if GG_DEBUG_MESSAGE_LEAKS

    return message;
  }

  void MessageFactory::AcquireMessage(Message* message)
  {
    gg_assert(message);
    if (message)
    {
      message->Acquire();
    }
  }

  void MessageFactory::ReleaseMessage(Message* message)
  {
    gg_assert(message);
    if (!message)
      return;

    message->Release();
    if (message->GetRefCount() == 0)
    {
#if GG_DEBUG_MESSAGE_LEAKS
      gg_assert(allocated_messages.find(message) != allocated_messages.end());
      allocated_messages.erase(message);
#endif // #if GG_DEBUG_MESSAGE_LEAKS
      gg_assert(m_allocator);
      GG_DELETE(*m_allocator, Message, message);
    }
  }

  
  s32 MessageFactory::GetNumTypes()const
  {
    return m_numTypes;
  }

  
  Allocator& MessageFactory::GetAllocator()
  {
    gg_assert(m_allocator);
    return *m_allocator;
  }

  
  MessageFactoryErrorLevel MessageFactory::GetErrorLevel() const
  {
    return m_errorLevel;
  }
  
  void MessageFactory::ClearErrorLevel()
  {
    m_errorLevel = E_MESSAGE_FACTORY_ERROR_NONE;
  }

  Message* MessageFactory::CreateMessageInternal(s32 type)
  {
    (void)type; 
    return nullptr;
  }
  
  void MessageFactory::SetMessageType(Message* message, s32 type)
  {
    message->SetType(type);
  }
}// namespace GG