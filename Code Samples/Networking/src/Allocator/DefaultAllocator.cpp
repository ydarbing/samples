#include "NetworkPrecompiled.h"
#include "Allocator\DefaultAllocator.h"

#ifdef _MSC_VER
#define SODIUM_STATIC
#endif // #ifdef _MSC_VER
#include <sodium.h>

static GG::Allocator * g_defaultAllocator = nullptr;

namespace GG
{
  extern s32 netcode_init();
  extern s32 reliable_init();
  extern void netcode_term();
  extern void reliable_term();
}

#define NETCODE_OK 1
#define RELIABLE_OK 1

bool InitializeGG()
{
  using namespace GG;
  g_defaultAllocator = new DefaultAllocator();

  if (netcode_init() != NETCODE_OK)
    return false;

  if (reliable_init() != RELIABLE_OK)
    return false;

  return sodium_init() != -1;
}

void ShutdownGG()
{
  using namespace GG;
  reliable_term();

  netcode_term();

  gg_assert(g_defaultAllocator);
  delete g_defaultAllocator;
  g_defaultAllocator = nullptr;
}



namespace GG
{

  Allocator& GetDefaultAllocator()
  {
    gg_assert(g_defaultAllocator);
    return *g_defaultAllocator;
  }




  void* DefaultAllocator::Allocate(size_t size, const c08* file, s32 line)
  {
    void* p = malloc(size);
    if (!p)
    {
      SetErrorLevel(E_ALLOCATOR_ERROR_OUT_OF_MEMORY);
      return nullptr;
    }

    TrackAlloc(p, size, file, line);

    return p;
  }
  void DefaultAllocator::Free(void* p, const c08* file, s32 line)
  {
    if (!p)
      return;

    TrackFree(p, file, line);

    free(p);
  }
}// namespace GG