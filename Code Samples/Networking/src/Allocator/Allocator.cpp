#include "NetworkPrecompiled.h"
#include "Allocator\Allocator.h"


namespace GG
{

  Allocator::Allocator() : m_errorLevel(E_ALLOCATOR_ERROR_NONE)
  {
  }

  Allocator::~Allocator()
  {
#if GG_DEBUG_MEMORY_LEAKS
    if (m_allocMap.size())
    {
      std::cout << "You leaked memory!" << std::endl << std::endl;
      auto iter = m_allocMap.begin();
      for (; iter != m_allocMap.end(); ++iter)
      {
        void* p = iter->first;
        AllocatorEntry entry = iter->second;
        printf("Leaked block %p (%d bytes) - %s:%d\n", p, (s32)entry.size, entry.file, entry.line);
      }
      std::cout << std::endl;
      exit(1);
    }
#endif
  }

  void Allocator::SetErrorLevel(AllocatorErrorLevel errorLevel)
  {
    if (m_errorLevel == E_ALLOCATOR_ERROR_NONE && errorLevel != E_ALLOCATOR_ERROR_NONE)
    {
      gg_printf(GG_LOG_LEVEL_ERROR, "Allocator went into error state %s\n", GetAllocatorErrorString(errorLevel));
    }
    m_errorLevel = errorLevel;
  }

  void Allocator::TrackAlloc(void* p, size_t size, const c08* file, s32 line)
  {
#if GG_DEBUG_MEMORY_LEAKS
    gg_assert(m_allocMap.find(p) == m_allocMap.end());

    AllocatorEntry entry;
    entry.size = size;
    entry.file = file;
    entry.line = line;
    m_allocMap[p] = entry;
#else
    (void)p;
    (void)size;
    (void)file;
    (void)line;
#endif
  }

  void Allocator::TrackFree(void* p, const c08* file, s32 line)
  {
    (void)p;
    (void)file;
    (void)line;
#if GG_DEBUG_MEMORY_LEAKS
    gg_assert(m_allocMap.find(p) != m_allocMap.end());
    m_allocMap.erase(p);
#endif
  }
}// namepsace GG