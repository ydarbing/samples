#include "NetworkPrecompiled.h"
#include "Allocator\TLSF_Allocator.h"
 




namespace GG
{
  static void* AlignPointerUp(void* memory, s32 align)
  {
    gg_assert((align & (align - 1)) == 0);
    uintptr_t p = (uintptr_t)memory;
    return (void*)((p + (align - 1)) & ~(align - 1));
  }

  static void* AlignPointerDown(void* memory, s32 align)
  {
    gg_assert((align & (align - 1)) == 0);
    uintptr_t p = (uintptr_t)memory;
    return (void*)(p - (p & (align - 1)));
  }

  TLSF_Allocator::TLSF_Allocator(void* memory, size_t size)
  {
    gg_assert(size > 0);
    SetErrorLevel(E_ALLOCATOR_ERROR_NONE);

    const int AlignBytes = 8;
    u08* aligned_memory_start = (u08*)AlignPointerUp(memory, AlignBytes);
    u08* aligned_memory_finish = (u08*)AlignPointerDown(((u08*)memory) + size, AlignBytes);

    gg_assert(aligned_memory_start < aligned_memory_finish);

    size_t aligned_memory_size = aligned_memory_finish - aligned_memory_start;

    m_tlsf = tlsf_create_with_pool(aligned_memory_start, aligned_memory_size);
  }

  TLSF_Allocator::~TLSF_Allocator()
  {
    tlsf_destroy(m_tlsf);
  }


  void * TLSF_Allocator::Allocate(size_t size, const c08* file, s32 line)
  {
    void* p = tlsf_malloc(m_tlsf, size);

    if (!p)
    {
      SetErrorLevel(E_ALLOCATOR_ERROR_OUT_OF_MEMORY);
      return nullptr;
    }

    TrackAlloc(p, size, file, line);

    return p;
  }

  void TLSF_Allocator::Free(void* p, const c08* file, s32 line)
  {
    if (!p)
      return;

    TrackFree(p, file, line);

    tlsf_free(m_tlsf, p);
  }
}// namepsace GG