#include "NetworkPrecompiled.h"
#include "Server\Server.h"
#include "Adapter.h"
#include "Connection.h"
#include "NetworkSimulator.h"

#include "Reliable/Reliable.h"
#include "Netcode/Netcode.h" // netcode_server_t


namespace GG
{
  Server::Server(Allocator & allocator, const u08 privateKey[], const Address & address, const ClientServerConfig & config, Adapter & adapter, f64 time)
    : BaseServer(allocator, config, adapter, time)
  {
    gg_assert(KEY_BYTES == NETCODE_KEY_BYTES);
    memcpy(m_privateKey, privateKey, NETCODE_KEY_BYTES);
    m_address = address;
    m_boundAddress = address;
    m_config = config;
    m_server = nullptr;
  }
  Server::~Server()
  {
    // IMPORTANT: Stop the server before destroying it
    gg_assert(!m_server);
  }
  void Server::Start(s32 maxClients)
  {
    if (IsRunning())
      Stop();

    BaseServer::Start(maxClients);

    c08 addressString[MAX_ADDRESS_LENGTH];
    m_address.ToString(addressString, MAX_ADDRESS_LENGTH);

    netcode_server_config_t netcodeConfig;
    netcode_default_server_config(&netcodeConfig);
    netcodeConfig.protocolId = m_config.protocolID;
    memcpy(netcodeConfig.privateKey, m_privateKey, NETCODE_KEY_BYTES);
    netcodeConfig.allocatorContext = &GetGlobalAllocator();
    netcodeConfig.allocate_function = StaticAllocateFunction;
    netcodeConfig.free_function = StaticFreeFunction;
    netcodeConfig.callbackContext = this;
    netcodeConfig.connect_disconnect_callback = StaticConnectDisconnectCallbackFunction;
    netcodeConfig.send_loopback_packet_callback = StaticSendLoopbackPacketCallbackFunction;

    m_server = netcode_server_create(addressString, &netcodeConfig, GetTime());

    if(!m_server)
    {
      Stop();
      return;
    }

    netcode_server_start(m_server, maxClients);

    m_boundAddress.SetPort(netcode_server_get_port(m_server));
  }
  void Server::Stop()
  {
    if(m_server)
    {
      m_boundAddress = m_address;
      netcode_server_stop(m_server);
      netcode_server_destroy(m_server);
      m_server = nullptr;
    }
    BaseServer::Stop();
  }

  void Server::DisconnectClient(s32 clientIndex)
  {
    gg_assert(m_server);
    netcode_server_disconnect_client(m_server, clientIndex);
  }

  void Server::DisconnectAllClients()
  {
    gg_assert(m_server);
    netcode_server_disconnect_all_clients(m_server);
  }

  void Server::SendPackets()
  {
    if(m_server)
    {
      const s32 maxClients = GetMaxClients();
      for(s32 i = 0; i < maxClients; ++i)
      {
        if (IsClientConnected(i))
        {
          u08* packetData = GetPacketBuffer();
          s32 packetBytes;
          u16 packetSequence = reliable_endpoint_next_packet_sequence(GetClientEndpoint(i));
          if(GetClientConnection(i).GeneratePacket(GetContext(), packetSequence, packetData, m_config.maxPacketSize, packetBytes))
          {
            reliable_endpoint_send_packet(GetClientEndpoint(i), packetData, packetBytes);
          }
        }
      }
    }
  }

  void Server::ReceivePackets()
  {
    if(m_server)
    {
      const s32 maxClients = GetMaxClients();
      for(s32 i = 0; i < maxClients; ++i)
      {
        while(true)
        {
          s32 packetBytes;
          u64 packetSequence;
          u08* packetData = netcode_server_receive_packet(m_server, i, &packetBytes, &packetSequence);
          if (!packetData)
            break;
          reliable_endpoint_receive_packet(GetClientEndpoint(i), packetData, packetBytes);
          netcode_server_free_packet(m_server, packetData);
        }
      }
    }
  }

  void Server::AdvanceTime(f64 time)
  {
    if (m_server)
      netcode_server_update(m_server, time);

    BaseServer::AdvanceTime(time);
    NetworkSimulator* networkSimulator = GetNetworkSimulator();
    if (networkSimulator && networkSimulator->IsActive())
    {
      u08** packetData  = SCAST(u08**, alloca(sizeof(u08*) * m_config.maxSimulatorPackets));
      s32*  packetBytes = SCAST(s32*,  alloca(sizeof(s32) * m_config.maxSimulatorPackets));
      s32*  to          = SCAST(s32*,  alloca(sizeof(s32) * m_config.maxSimulatorPackets));

      s32 numPackets = networkSimulator->ReceivePackets(m_config.maxSimulatorPackets, packetData, packetBytes, to);
      for(s32 i = 0; i < numPackets; ++i)
      {
        netcode_server_send_packet(m_server, to[i], packetData[i], packetBytes[i]);
        GG_FREE(networkSimulator->GetAllocator(), packetData[i]);
      }
    }
  }
  bool Server::IsClientConnected(s32 clientIndex) const
  {
    return netcode_server_client_connected(m_server, clientIndex) != 0;
  }
  s32 Server::GetNumConnectedClients() const
  {
    return netcode_server_num_connected_clients(m_server);
  }
  void Server::ConnectLoopbackClient(s32 clientIndex, u64 clientID, const u08 * userData)
  {
    netcode_server_connect_loopback_client(m_server, clientIndex, clientID, userData);
  }
  void Server::DisconnectLoopbackClient(s32 clientIndex)
  {
    netcode_server_disconnect_loopback_client(m_server, clientIndex);
  }
  bool Server::IsLoopbackClient(s32 clientIndex) const
  {
    return netcode_server_client_loopback(m_server, clientIndex) != 0;
  }
  void Server::ProcessLoopbackPacket(s32 clientIndex, const u08 * packetData, s32 packetBytes, u64 packetSequence)
  {
    netcode_server_process_loopback_packet(m_server, clientIndex, packetData, packetBytes, packetSequence);
  }
  const Address& Server::GetAddress() const
  {
    return m_boundAddress;
  }
  void Server::TransmitPacketFunction(s32 clientIndex, u16 packetSequence, u08 * packetData, s32 packetBytes)
  {
    (void)packetSequence;
    NetworkSimulator* networkSimulator = GetNetworkSimulator();
    if(networkSimulator && networkSimulator->IsActive())
    {
      networkSimulator->SendPacket(clientIndex, packetData, packetBytes);
    }
    else
    {
      netcode_server_send_packet(m_server, clientIndex, packetData, packetBytes);
    }
  }
  s32 Server::ProcessPacketFunction(s32 clientIndex, u16 packetSequence, u08 * packetData, s32 packetBytes)
  {
    return SCAST(s32, GetClientConnection(clientIndex).ProcessPacket(GetContext(), packetSequence, packetData, packetBytes));
  }
  void Server::ConnectDisconnectCallbackFunction(s32 clientIndex, s32 connected)
  {
    if(connected == 0)
    {
      GetAdapter().OnServerClientDisconnected(clientIndex);
      reliable_endpoint_reset(GetClientEndpoint(clientIndex));
      GetClientConnection(clientIndex).Reset();
      NetworkSimulator* networkSimulator = GetNetworkSimulator();
      if (networkSimulator && networkSimulator->IsActive())
      {
        networkSimulator->DiscardClientPackets(clientIndex);
      }
    }
    else
    {
      GetAdapter().OnServerClientConnected(clientIndex);
    }
  }
  void Server::SendLoopbackPacketCallbackFunction(s32 clientIndex, const u08 * packetData, s32 packetBytes, u64 packetSequence)
  {
    GetAdapter().ServerSendLoopbackPacket(clientIndex, packetData, packetBytes, packetSequence);
  }
  void Server::StaticConnectDisconnectCallbackFunction(void * context, s32 clientIndex, s32 connected)
  {
    Server* server = SCAST(Server*, context);
    server->ConnectDisconnectCallbackFunction(clientIndex, connected);
  }
  void Server::StaticSendLoopbackPacketCallbackFunction(void * context, s32 clientIndex, const u08 * packetData, s32 packetBytes, u64 packetSequence)
  {
    Server* server = SCAST(Server*, context);
    server->SendLoopbackPacketCallbackFunction(clientIndex, packetData, packetBytes, packetSequence);
  }
}// namepsace GG
