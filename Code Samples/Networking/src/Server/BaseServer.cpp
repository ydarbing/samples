#include "NetworkPrecompiled.h"
#include "Server\BaseServer.h"
#include "Allocator/Allocator.h"
#include "Adapter.h"
#include "Reliable\Reliable.h" // reliable_endpoint_t
#include "NetworkSimulator.h"
#include "Connection.h"
#include "Adapter.h"

namespace GG
{
  BaseServer::BaseServer(Allocator & allocator, const ClientServerConfig & config, Adapter & adapter, f64 time)
    : m_config(config)
  {
    m_allocator = &allocator;
    m_adapter = &adapter;
    m_time = time;
    m_maxClients = 0;
    m_running = false;
    m_context = nullptr;
    m_globalMemory = nullptr;
    m_globalAllocator = nullptr;
    for (s32 i = 0; i < MAX_CLIENTS; ++i)
    {
      m_clientMemory[i] = nullptr;
      m_clientAllocator[i] = nullptr;
      m_clientMessageFactory[i] = nullptr;
      m_clientConnection[i] = nullptr;
      m_clientEndpoint[i] = nullptr;
    }
    m_networkSimulator = nullptr;
    m_packetBuffer = nullptr;
  }

  BaseServer::~BaseServer()
  {
    // IMPORTANT: please stop the server before destroying this
    gg_assert(!IsRunning());
    m_allocator = nullptr;
  }

  void BaseServer::SetContext(void * context)
  {
    gg_assert(!IsRunning());
    m_context = context;
  }

  void BaseServer::Start(s32 maxClients)
  {
    Stop();
    m_running = true;
    m_maxClients = maxClients;
    gg_assert(!m_globalMemory);
    gg_assert(!m_globalAllocator);
    m_globalMemory = (u08*)GG_ALLOCATE(*m_allocator, m_config.serverGlobalMemory);
    m_globalAllocator = m_adapter->CreateAllocator(*m_allocator, m_globalMemory, m_config.serverGlobalMemory);
    gg_assert(m_globalAllocator);
    if(m_config.networkSimulator)
    {
      m_networkSimulator = GG_NEW(*m_globalAllocator, NetworkSimulator, *m_globalAllocator, m_config.maxSimulatorPackets, m_time); 
    }

    for(s32 i = 0; i < m_maxClients; ++i)
    {
      gg_assert(!m_clientMemory[i]);
      gg_assert(!m_clientAllocator[i]);
      m_clientMemory[i] = (u08*)GG_ALLOCATE(*m_allocator, m_config.serverPerClientMemory);
      m_clientAllocator[i] = m_adapter->CreateAllocator(*m_allocator, m_clientMemory[i], m_config.serverPerClientMemory);
      gg_assert(m_clientAllocator[i]);

      m_clientMessageFactory[i] = m_adapter->CreateMessageFactory(*m_clientAllocator[i]);
      gg_assert(m_clientMessageFactory[i]);

      m_clientConnection[i] = GG_NEW(*m_clientAllocator[i], Connection, *m_clientAllocator[i], *m_clientMessageFactory[i], m_config, m_time);
      gg_assert(m_clientConnection[i]);


      reliable_config_t reliableConfig;
      reliable_default_config(&reliableConfig);
      strcpy(reliableConfig.name, "server endpoint");
      reliableConfig.context = (void*) this;
      reliableConfig.index = i;
      reliableConfig.maxPacketSize = m_config.maxPacketSize;
      reliableConfig.fragmentAbove = m_config.fragmentPacketsAbove;
      reliableConfig.maxFragments = m_config.maxPacketFragments;
      reliableConfig.fragmentSize = m_config.packetFragmentSize;
      reliableConfig.ackBufferSize = m_config.ackedPacketsBufferSize;
      reliableConfig.receivedPacketsBufferSize = m_config.receivedPacketsBufferSize;
      reliableConfig.fragmentReassemblyBufferSize = m_config.packetReassemblyBufferSize;
      reliableConfig.transmit_packet_function = BaseServer::StaticTransmitPacketFunction;
      reliableConfig.process_packet_function = BaseServer::StaticProcessPacketFunction;
      reliableConfig.allocatorContext = &GetGlobalAllocator();
      reliableConfig.allocate_function = BaseServer::StaticAllocateFunction;
      reliableConfig.free_function = BaseServer::StaticFreeFunction;
      m_clientEndpoint[i] = reliable_endpoint_create(&reliableConfig, m_time);
      reliable_endpoint_reset(m_clientEndpoint[i]);
    }
    m_packetBuffer = (u08*)GG_ALLOCATE(*m_globalAllocator, m_config.maxPacketSize);
  }

  void BaseServer::Stop()
  {
    if(IsRunning())
    {
      GG_FREE(*m_globalAllocator, m_packetBuffer);
      gg_assert(m_globalMemory);
      gg_assert(m_globalAllocator);
      GG_DELETE(*m_globalAllocator, NetworkSimulator, m_networkSimulator);
      for(s32 i = 0; i < m_maxClients; ++i)
      {
        gg_assert(m_clientMemory[i]);
        gg_assert(m_clientAllocator[i]);
        gg_assert(m_clientMessageFactory[i]);
        gg_assert(m_clientEndpoint[i]);
        reliable_endpoint_destroy(m_clientEndpoint[i]);
        m_clientEndpoint[i] = nullptr;
        GG_DELETE(*m_clientAllocator[i], Connection, m_clientConnection[i]);
        GG_DELETE(*m_clientAllocator[i], MessageFactory, m_clientMessageFactory[i]);
        GG_DELETE(*m_allocator, Allocator, m_clientAllocator[i]);
        GG_FREE(*m_allocator, m_clientMemory[i]);
      }
      GG_DELETE(*m_allocator, Allocator, m_globalAllocator);
      GG_FREE(*m_allocator, m_globalMemory);
    }

    m_running = false;
    m_maxClients = 0;
    m_packetBuffer = nullptr;
  }

  void BaseServer::AdvanceTime(f64 time)
  {
    m_time = time;
    if(IsRunning())
    {
      for(s32 i = 0; i < m_maxClients; ++i)
      {
        m_clientConnection[i]->AdvanceTime(time);
        if(m_clientConnection[i]->GetErrorLevel() != E_CONNECTION_ERROR_NONE)
        {
          gg_printf(GG_LOG_LEVEL_ERROR, "Client %d connection is in error state.  Disconnectiong client\n", m_clientConnection[i]->GetErrorLevel());
          DisconnectClient(i);
          continue;
        }
        reliable_endpoint_update(m_clientEndpoint[i], m_time);
        s32 numAcks;
        const u16* acks = reliable_endpoint_get_acks(m_clientEndpoint[i], &numAcks);
        m_clientConnection[i]->ProcessAcks(acks, numAcks);
        reliable_endpoint_clear_acks(m_clientEndpoint[i]);
      }
      NetworkSimulator* networkSimulator = GetNetworkSimulator();
      if(networkSimulator)
      {
        networkSimulator->AdvanceTime(time);
      }
    }
  }


  bool BaseServer::IsRunning() const
  {
    return m_running;
  }
  s32 BaseServer::GetMaxClients() const
  {
    return m_maxClients;
  }
  f64 BaseServer::GetTime() const
  {
    return m_time;
  }

  void BaseServer::SetLatency(f32 milliseconds)
  {
    if(m_networkSimulator)
    {
      m_networkSimulator->SetLatency(milliseconds);
    }
  }

  void BaseServer::SetJitter(f32 milliseconds)
  {
    if (m_networkSimulator)
    {
      m_networkSimulator->SetJitter(milliseconds);
    }
  }

  void BaseServer::SetPacketLoss(f32 percent)
  {
    if (m_networkSimulator)
    {
      m_networkSimulator->SetPacketLoss(percent);
    }
  }

  void BaseServer::SetDuplicates(f32 percent)
  {
    if (m_networkSimulator)
    {
      m_networkSimulator->SetDuplicates(percent);
    }
  }

  Message * BaseServer::CreateMessage(s32 clientIndex, s32 type)
  {
    gg_assert(clientIndex >= 0);
    gg_assert(clientIndex < m_maxClients);
    gg_assert(m_clientMessageFactory[clientIndex]);
    return m_clientMessageFactory[clientIndex]->CreateMessage(type);
  }

  uint8_t * BaseServer::AllocateBlock(s32 clientIndex, s32 bytes)
  {
    gg_assert(clientIndex >= 0);
    gg_assert(clientIndex < m_maxClients);
    gg_assert(m_clientMessageFactory[clientIndex]);
    return SCAST(u08*, GG_ALLOCATE(*m_clientAllocator[clientIndex], bytes));
  }

  void BaseServer::AttachBlockToMessage(s32 clientIndex, Message * message, u08 * block, s32 bytes)
  {
    gg_assert(clientIndex >= 0);
    gg_assert(clientIndex < m_maxClients);
    gg_assert(message);
    gg_assert(block);
    gg_assert(bytes > 0);
    gg_assert(message->IsBlockMessage());
    BlockMessage* blockMessage = SCAST(BlockMessage*, message);
    blockMessage->AttachBlock(*m_clientAllocator[clientIndex], block, bytes);
  }

  void BaseServer::FreeBlock(s32 clientIndex, u08 * block)
  {
    gg_assert(clientIndex >= 0);
    gg_assert(clientIndex < m_maxClients);
    GG_FREE(*m_clientAllocator[clientIndex], block);
  }

  bool BaseServer::CanSendMessage(s32 clientIndex, s32 channelIndex) const
  {
    gg_assert(clientIndex >= 0);
    gg_assert(clientIndex < m_maxClients);
    gg_assert(m_clientConnection[clientIndex]);
    return m_clientConnection[clientIndex]->CanSendMessage(channelIndex);
  }

  void BaseServer::SendMessage(s32 clientIndex, s32 channelIndex, Message * message)
  {
    gg_assert(clientIndex >= 0);
    gg_assert(clientIndex < m_maxClients);
    gg_assert(m_clientConnection[clientIndex]);
    m_clientConnection[clientIndex]->SendMessage(channelIndex, message, GetContext());
  }

  Message * BaseServer::ReceiveMessage(s32 clientIndex, s32 channelIndex)
  {
    gg_assert(clientIndex >= 0);
    gg_assert(clientIndex < m_maxClients);
    gg_assert(m_clientConnection[clientIndex]);
    return m_clientConnection[clientIndex]->ReceiveMessage(channelIndex);
  }

  void BaseServer::ReleaseMessage(s32 clientIndex, Message* message)
  {
    gg_assert(clientIndex >= 0);
    gg_assert(clientIndex < m_maxClients);
    gg_assert(m_clientConnection[clientIndex]);
    m_clientConnection[clientIndex]->ReleaseMessage(message);
  }

  void BaseServer::GetNetworkInfo(s32 clientIndex, NetworkInfo& info) const
  {
    gg_assert(IsRunning());
    gg_assert(clientIndex >= 0);
    gg_assert(clientIndex < m_maxClients);
    memset(&info, 0, sizeof(info));

    if (IsClientConnected(clientIndex))
    {
      gg_assert(m_clientEndpoint[clientIndex]);
      const u64* counters = reliable_endpoint_counters(m_clientEndpoint[clientIndex]);
      info.numPacketsSent     = counters[RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_SENT];
      info.numPacketsReceived = counters[RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_RECEIVED];
      info.numPacketsAcked    = counters[RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_ACKED];
      info.RTT        = reliable_endpoint_rtt(m_clientEndpoint[clientIndex]);
      info.packetLoss = reliable_endpoint_packet_loss(m_clientEndpoint[clientIndex]);
      reliable_endpoint_bandwidth(m_clientEndpoint[clientIndex], &info.sentBandwidth, &info.receivedBandwidth, &info.ackedBandwidth);
    }
  }

  // Protected functions
  u08* BaseServer::GetPacketBuffer()
  {
    return m_packetBuffer;
  }
  void* BaseServer::GetContext()
  {
    return m_context;
  }
  Adapter& BaseServer::GetAdapter()
  {
    gg_assert(m_adapter); 
    return *m_adapter;
  }

  Allocator& BaseServer::GetGlobalAllocator()
  {
    gg_assert(m_globalAllocator); 
    return *m_globalAllocator;
  }

  MessageFactory& BaseServer::GetClientMessageFactory(s32 clientIndex)
  {
    gg_assert(IsRunning());
    gg_assert(clientIndex >= 0);
    gg_assert(clientIndex < m_maxClients);
    return *m_clientMessageFactory[clientIndex];
  }

  NetworkSimulator* BaseServer::GetNetworkSimulator()
  {
    return m_networkSimulator;
  }

  reliable_endpoint_t* BaseServer::GetClientEndpoint(s32 clientIndex)
  {
    gg_assert(IsRunning());
    gg_assert(clientIndex >= 0);
    gg_assert(clientIndex < m_maxClients);
    return m_clientEndpoint[clientIndex];
  }

  Connection& BaseServer::GetClientConnection(s32 clientIndex)
  {
    gg_assert(IsRunning());
    gg_assert(clientIndex >= 0);
    gg_assert(clientIndex < m_maxClients);
    gg_assert(m_clientConnection[clientIndex]);
    return *m_clientConnection[clientIndex];
  }

  void BaseServer::StaticTransmitPacketFunction(void* context, s32 index, u16 packetSequence, u08 * packetData, s32 packetBytes)
  {
    BaseServer* server = SCAST(BaseServer*, context);
    server->TransmitPacketFunction(index, packetSequence, packetData, packetBytes);
  }

  s32 BaseServer::StaticProcessPacketFunction(void* context, s32 index, u16 packetSequence, u08 * packetData, s32 packetBytes)
  {
    BaseServer* server = SCAST(BaseServer*, context);
    return server->ProcessPacketFunction(index, packetSequence, packetData, packetBytes);
  }

  void * BaseServer::StaticAllocateFunction(void* context, u64 bytes)
  {
    gg_assert(context);
    Allocator* allocator = SCAST(Allocator*, context);

    return GG_ALLOCATE(*allocator, bytes);
  }

  void BaseServer::StaticFreeFunction(void* context, void * pointer)
  {
    gg_assert(context);
    gg_assert(pointer);
    Allocator* allocator = SCAST(Allocator*, context);

    GG_FREE(*allocator, pointer);
  }

}// namepsace GG
