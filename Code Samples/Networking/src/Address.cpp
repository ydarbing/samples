#include "NetworkPrecompiled.h"
#include "Address.h"
 

namespace GG
{
  Address::Address()
  {
    Clear();
  }

  Address::Address(u08 a, u08 b, u08 c, u08 d, u16 port /*= 0*/)
    : m_type(E_ADDRESS_IPV4), m_port(port)
  {
    m_address.ipv4[0] = a;
    m_address.ipv4[1] = b;
    m_address.ipv4[2] = c;
    m_address.ipv4[3] = d;
  }

  Address::Address(const u08 address[], u16 port /*= 0*/)
    : m_type(E_ADDRESS_IPV4), m_port(port)
  {
    for (s32 i = 0; i < 4; ++i)
      m_address.ipv4[i] = address[i];
  }
  Address::Address(u16 a, u16 b, u16 c, u16 d, u16 e, u16 f, u16 g, u16 h, u16 port /*= 0*/)
    : m_type(E_ADDRESS_IPV6), m_port(port)
  {
    m_address.ipv6[0] = a;
    m_address.ipv6[1] = b;
    m_address.ipv6[2] = c;
    m_address.ipv6[3] = d;
    m_address.ipv6[4] = e;
    m_address.ipv6[5] = f;
    m_address.ipv6[6] = g;
    m_address.ipv6[7] = h;
  }
  Address::Address(const u16 address[], u16 port /*= 0*/)
    : m_type(E_ADDRESS_IPV6), m_port(port)
  {
    for (s32 i = 0; i < 8; ++i)
      m_address.ipv6[i] = address[i];

  }
  Address::Address(const c08 * address)
  {
    Parse(address);
  }
  Address::Address(const c08 * address, u16 port)
  {
    Parse(address);
    m_port = port;
  }

  void Address::Parse(const c08* address_in)
  {
    // try to parse an IPv6 address
    // 1. if first character is '[' then it's probably an ipv6 in form [addr6]:portnum
    // 2. otherwise try to parse as raw ipv6 address, parse using inet_pton
    gg_assert(address_in);

    c08 buffer[MAX_ADDRESS_LENGTH];
    c08* address = buffer;
    strncpy(address, address_in, MAX_ADDRESS_LENGTH - 1);
    address[MAX_ADDRESS_LENGTH - 1] = '\0';

    s32 addressLength = (s32)strlen(address);
    m_port = 0;

    if (address[0] == '[')
    {
      const s32 baseIndex = addressLength - 1;
      for (s32 i = 0; i < 6; ++i) // no need to search past 6 chars as :65535 is longest port value
      {
        const s32 index = baseIndex - i;
        if (index < 3)
          break;
        if (address[index] == ':')
        {
          m_port = u16(atoi(&address[index + 1]));
          address[index - 1] = '\0';
        }
      }
      address += 1;
    }
    struct in6_addr sockaddr6;
    if (inet_pton(AF_INET6, address, &sockaddr6) == 1)
    {
      for (s32 i = 0; i < 8; ++i)
      {
        m_address.ipv6[i] = ntohs(((u16*)&sockaddr6)[i]);
      }
      m_type = E_ADDRESS_IPV6;
      return;
    }

    // otherwise it's probably an IPv4 address:
    // 1. look for ":portnum" if found, save portnum and strip it out
    // 2. parse remaining ipv4 address via inet_pton

    addressLength = (s32)strlen(address);
    const s32 baseIndex = addressLength - 1;
    for (s32 i = 0; i < 6; ++i)
    {
      const s32 index = baseIndex - i;
      if (index < 0)
        break;
      if (address[index] == ':')
      {
        m_port = (u16)atoi(&address[index + 1]);
        address[index] = '\0';
      }
    }

    struct sockaddr_in sockaddr4;
    if (inet_pton(AF_INET, address, &sockaddr4.sin_addr) == 1)
    {
      m_type = E_ADDRESS_IPV4;
      m_address.ipv4[3] = (u08)((sockaddr4.sin_addr.s_addr & 0xFF000000) >> 24);
      m_address.ipv4[2] = (u08)((sockaddr4.sin_addr.s_addr & 0x00FF0000) >> 16);
      m_address.ipv4[1] = (u08)((sockaddr4.sin_addr.s_addr & 0x0000FF00) >> 8);
      m_address.ipv4[0] = (u08)((sockaddr4.sin_addr.s_addr & 0x000000FF));
    }
    else
    {
      Clear(); // not a valid IPv4 address. set address to invalid
    }
  }

  void Address::Clear()
  {
    m_type = E_ADDRESS_NONE;
    memset(&m_address, 0, sizeof(m_address));
    m_port = 0;
  }
  const u08* Address::GetAddress4() const
  {
    gg_assert(m_type == E_ADDRESS_IPV4);
    return m_address.ipv4;
  }

  const u16* Address::GetAddress6() const
  {
    gg_assert(m_type == E_ADDRESS_IPV6);
    return m_address.ipv6;
  }

  void Address::SetPort(u16 port)
  {
    m_port = port;
  }

  u16 Address::GetPort() const
  {
    return m_port;
  }

  AddressType Address::GetType() const
  {
    return m_type;
  }

  const c08 * Address::ToString(c08 buffer[], s32 bufferSize) const
  {
    gg_assert(bufferSize >= MAX_ADDRESS_LENGTH);

    if (m_type == E_ADDRESS_IPV4)
    {
      const u08 a = m_address.ipv4[0];
      const u08 b = m_address.ipv4[1];
      const u08 c = m_address.ipv4[2];
      const u08 d = m_address.ipv4[3];
      if (m_port != 0)
        snprintf(buffer, bufferSize, "%d.%d.%d.%d:%d", a, b, c, d, m_port);
      else
        snprintf(buffer, bufferSize, "%d.%d.%d.%d", a, b, c, d);

      return buffer;
    }
    else if (m_type == E_ADDRESS_IPV6)
    {
      if (m_port == 0)
      {
        u16 address6[8];
        for (s32 i = 0; i < 8; ++i)
          address6[i] = ntohs( ((u16*)&m_address.ipv6)[i] );
        inet_ntop(AF_INET6, address6, buffer, bufferSize);
        return buffer;
      }
      else
      {
        c08 addressString[INET6_ADDRSTRLEN];
        u16 address6[8];
        for (s32 i = 0; i < 8; ++i)
          address6[i] = ntohs( ((u16*)&m_address.ipv6)[i] );
        inet_ntop(AF_INET6, address6, addressString, INET6_ADDRSTRLEN);
        snprintf(buffer, bufferSize, "[%s]:%d", addressString, m_port);

        return buffer;
      }
    }
    else
    {
      snprintf(buffer, bufferSize, "%s", "NONE");
      return buffer;
    }
  }

  bool Address::IsValid() const
  {
    return m_type != E_ADDRESS_NONE;
  }

  bool Address::IsLoopback() const
  {
    return (m_type == E_ADDRESS_IPV4 && m_address.ipv4[0] == 127
                                     && m_address.ipv4[1] == 0
                                     && m_address.ipv4[2] == 0
                                     && m_address.ipv4[3] == 1)
        || (m_type == E_ADDRESS_IPV6 && m_address.ipv6[0] == 0
                                     && m_address.ipv6[1] == 0
                                     && m_address.ipv6[2] == 0
                                     && m_address.ipv6[3] == 0
                                     && m_address.ipv6[4] == 0
                                     && m_address.ipv6[5] == 0
                                     && m_address.ipv6[6] == 0
                                     && m_address.ipv6[7] == 0x0001);
  }

  bool Address::IsLinkLocal() const
  {
    return m_type == E_ADDRESS_IPV6 && m_address.ipv6[0] == 0xFE80;
  }

  bool Address::IsSiteLocal() const
  {
    return m_type == E_ADDRESS_IPV6 && m_address.ipv6[0] == 0xFEC0;
  }

  bool Address::IsMulticast() const
  {
    return m_type == E_ADDRESS_IPV6 && m_address.ipv6[0] == 0xFF00;
  }

  bool Address::IsGlobalUnicast() const
  {
    return m_type == E_ADDRESS_IPV6 && m_address.ipv6[0] != 0xFE80
                                    && m_address.ipv6[0] != 0xFEC0
                                    && m_address.ipv6[0] != 0xFF00
                                    && !IsLoopback();
  }

  bool Address::operator ==(const Address & other) const
  {
    if (m_type != other.m_type)
      return false;
    if (m_port != other.m_port)
      return false;
    if (m_type == E_ADDRESS_IPV4 && m_address.ipv4 == other.m_address.ipv4)
      return true;
    else if (m_type == E_ADDRESS_IPV6 && memcmp(m_address.ipv6, other.m_address.ipv6, sizeof(m_address.ipv6)) == 0)
      return true;
    else
      return false;
  }

  bool Address::operator !=(const Address & other) const
  {
    return !(*this == other);
  }
}// namespace GG
