#include "NetworkPrecompiled.h"
#include "Stream\StreamBase.h"


namespace GG
{

  void StreamBase::SetContext(void* context)
  {
    m_context = context;
  }
  // get context pointer of the stream.  May be nullptr
  void* StreamBase::GetContext(void)const
  {
    return m_context;
  }
  //Get the allocator set on the stream.
  //You can use this allocator to dynamically allocate memory while reading and writing packets.
  Allocator& StreamBase::GetAllocator(void)const
  {
    return *m_allocator;
  }
}// namespace GG