#include "NetworkPrecompiled.h"
#include "Stream\StreamMeasure.h"
 
#include "Utils.h"

namespace GG
{

  bool StreamMeasure::SerializeInteger(s32 value, s32 min, s32 max)
  {
    (void)value;
    gg_assert(min < max);
    gg_assert(value >= min);
    gg_assert(value <= max);
    const s32 bits = BitsRequired(min, max);
    m_bitsWritten += bits;
    return true;
  }
  bool StreamMeasure::SerializeBits(u32 value, s32 bits)
  {
    (void)value;

    gg_assert(bits > 0);
    gg_assert(bits <= 32);
    m_bitsWritten += bits;
    return true;
  }
  bool StreamMeasure::SerializeBytes(const u08* data, s32 bytes)
  {
    (void)data;
    SerializeAlign();
    m_bitsWritten += bytes * 8;
    return true;
  }
  bool StreamMeasure::SerializeAlign()
  {
    const s32 alignBits = GetAlignBits();
    m_bitsWritten += alignBits;
    return true;
  }
  // IMPORTANT: The number of bits required for alignment depends on where an object is written in the final bit stream, this measurement is conservative. 
  // Always returns worst case 7 bits.
  s32 StreamMeasure::GetAlignBits() const
  {
    return 7;
  }

  bool StreamMeasure::SerializeCheck()
  {
#if GG_SERIALIZE_CHECKS
    SerializeAlign();
    m_bitsWritten += 32;
#endif
    return true;
  }
  s32 StreamMeasure::GetBitsProcessed() const
  {
    return m_bitsWritten;
  }
  s32 StreamMeasure::GetBytesProcessed() const
  {
    return (m_bitsWritten + 7) / 8;
  }

}// namespace GG