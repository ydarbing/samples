#include "NetworkPrecompiled.h"
#include "Stream\StreamWrite.h"
 
#include "Utils.h" // BitsRequired

namespace GG
{

  // always serialze functions returns true, all checking performed by debug asserts on write
  bool StreamWrite::SerializeInteger(s32 value, s32 min, s32 max)
  {
    gg_assert(min < max);
    gg_assert(value >= min);
    gg_assert(value <= max);

    const s32 bits = BitsRequired(min, max);
    u32 unsignedValue = value - min;
    m_writer.WriteBits(unsignedValue, bits);
    return true;
  }

  // serialze a number of bits
  // value - unsigned s32 value to serialze (nust be in range [0, (1 << bits) - 1])
  // bits - number of bits to write [1, 32]
  bool StreamWrite::SerializeBits(u32 value, s32 bits)
  {
    gg_assert(bits > 0);
    gg_assert(bits <= 32);

    m_writer.WriteBits(value, bits);
    return true;
  }

  // serialize array of bytes
  // data - array of bytes to be written
  // bytes - number of bytes to write
  bool StreamWrite::SerializeBytes(const u08* data, s32 bytes)
  {
    gg_assert(data);
    gg_assert(bytes >= 0);

    SerializeAlign();
    m_writer.WriteBytes(data, bytes);
    return true;
  }

  // serialze an align
  bool StreamWrite::SerializeAlign()
  {
    m_writer.WriteAlign();
    return true;
  }

  // how many bits are required to align right now
  s32 StreamWrite::GetAlignBits()const
  {
    return m_writer.GetAlignBits();
  }

  // Serialize a safety check to the stream
  // tracks down desyncs.  a check is written to the stream and on the other side if the check is not present it asserts and fails the serialize
  bool StreamWrite::SerializeCheck()
  {
#if GG_SERIALIZE_CHECKS
    SerializeAlign();
    SerializeBits(SERIALIZE_CHECK_VALUE, 32);
#else
    (void)istring;
#endif
    return true;
  }

  // flush stream to memory after finish writing
  // always call after you finish writing and before you call StreamWrite::GetData
  void StreamWrite::Flush()
  {
    m_writer.FlushBits();
  }

  // Get pointer to data written to stream
  // CALL StreamWrite::Flush before you call this function
  const u08* StreamWrite::GetData()
  {
    return m_writer.GetData();
  }

  // Get number of bits written so far
  s32 StreamWrite::GetBitsProcessed() const
  {
    return m_writer.GetBitsWritten();
  }
  // How many bytes written so far
  // returns number of ytes written, Effectively the packet size
  s32 StreamWrite::GetBytesProcessed() const
  {
    return m_writer.GetBytesWritten();
  }


} // namespace GG

