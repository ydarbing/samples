#include "NetworkPrecompiled.h"
#include "Stream\StreamRead.h"
 
#include "Utils.h" // BitsRequired


namespace GG
{

  // all serialize functions return true if the serialize read succeeded, false otherwise

  bool StreamRead::SerializeInteger(s32& value, s32 min, s32 max)
  {
    gg_assert(min < max);
    const s32 bits = BitsRequired(min, max);
    if (m_reader.WouldReadPastEnd(bits))
    {
      return false;
    }

    u32 unsignedValue = m_reader.ReadBits(bits);
    value = (s32)unsignedValue + min;
    return true;
  }
  // serialze a number of bits
  // value - unsigned s32 value to serialze (nust be in range [0, (1 << bits) - 1])
  // bits - number of bits to write [1, 32]
  bool StreamRead::SerializeBits(u32& value, s32 bits)
  {
    gg_assert(bits > 0);
    gg_assert(bits <= 32);
    if (m_reader.WouldReadPastEnd(bits))
    {
      return false;
    }
    u32 readValue = m_reader.ReadBits(bits);
    value = readValue;
    return true;
  }
  // serialize array of bytes
  // data - array of bytes to be written
  // bytes - number of bytes to write
  bool StreamRead::SerializeBytes(u08* data, s32 bytes)
  {
    if (!SerializeAlign())
      return false;
    if (m_reader.WouldReadPastEnd(bytes * 8))
      return false;
    m_reader.ReadBytes(data, bytes);
    return true;
  }
  // serialze an align
  bool StreamRead::SerializeAlign()
  {
    const s32 alignBits = m_reader.GetAlignBits();
    if (m_reader.WouldReadPastEnd(alignBits))
      return false;
    if (!m_reader.ReadAlign())
      return false;
    return true;
  }
  // how many bits are required to align right now
  s32 StreamRead::GetAlignBits()const
  {
    return m_reader.GetAlignBits();
  }
  // Serialize a safety check to the stream
  // tracks down desyncs.  a check is written to the stream and on the other side if the check is not present it asserts and fails the serialize
  bool StreamRead::SerializeCheck()
  {
#if GG_SERIALIZE_CHECKS
    if (!SerializeAlign())
      return false;
    u32 value = 0;
    if (!SerializeBits(value, 32))
      return false;
    if (value != SERIALIZE_CHECK_VALUE)
    {
      gg_printf(GG_LOG_LEVEL_DEBUG, "Serialize check failed: expected %x, got %x", SERIALIZE_CHECK_VALUE, value);
    }
    return value == SERIALIZE_CHECK_VALUE;
#else
    return true;
#endif
  }
  // Get number of bits read so far
  s32 StreamRead::GetBitsProcessed() const
  {
    return m_reader.GetBitsRead();
  }
  // How many bytes read so far
  // returns number of ytes written, Effectively number of bits read, rounded up to the next byte where necessary.
  s32 StreamRead::GetBytesProcessed() const
  {
    return (m_reader.GetBitsRead() + 7) / 8;
  }

}// namespace GG
