#include "NetworkPrecompiled.h"
#include "Reliable\Reliable.h"



#ifndef RELIABLE_ENABLE_TESTS
#define RELIABLE_ENABLE_TESTS 1
#endif // #ifndef RELIABLE_ENABLE_TESTS

#ifndef RELIABLE_ENABLE_LOGGING
#define RELIABLE_ENABLE_LOGGING 1
#endif // #ifndef RELIABLE_ENABLE_LOGGING


namespace GG
{
  static s32 logLevel = 0;
  static s32(*printf_function)(const c08*, ...) = (s32(*)(const c08*, ...)) printf;

  void reliable_log_level(s32 level)
  {
    logLevel = level;
  }

  void reliable_set_printf_function(s32(*function)(const c08*, ...))
  {
    gg_assert(function);
    printf_function = function;
  }

#if RELIABLE_ENABLE_LOGGING

  void reliable_printf(s32 level, const c08* format, ...)
  {
    if (level > logLevel)
      return;
    va_list args;
    va_start(args, format);
    char buffer[4 * 1024];
    vsprintf(buffer, format, args);
    printf_function("%s", buffer);
    va_end(args);
  }

#else // #if RELIABLE_ENABLE_LOGGING

  void reliable_printf(int level, RELIABLE_CONST char * format, ...)
  {
    (void)level;
    (void)format;
  }

#endif // #if RELIABLE_ENABLE_LOGGING

  void * reliable_default_allocate_function(void * context, uint64_t bytes)
  {
    (void)context;
    return malloc(bytes);
  }

  void reliable_default_free_function(void * context, void * pointer)
  {
    (void)context;
    free(pointer);
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  s32 reliable_init()
  {
    return RELIABLE_OK;
  }

  void reliable_term()
  {
  }

  bool reliable_sequence_greater_than(u16 s1, u16 s2)
  {
    return ((s1 > s2) && (s1 - s2 <= 32768)) ||
      ((s1 < s2) && (s2 - s1 > 32768));
  }

  bool reliable_sequence_less_than(u16 s1, u16 s2)
  {
    return reliable_sequence_greater_than(s2, s1);
  }

  struct reliable_sequence_buffer_t
  {
    void* allocatorContext;
    void* (*allocate_function)(void*, u64);
    void(*free_function)(void*, void*);
    u16 sequence;
    s32 numEntries;
    s32 entryStride;
    u32* entrySequence;
    u08* entryData;
  };

  reliable_sequence_buffer_t* reliable_sequence_buffer_create(s32 numEntries, s32 entryStride, void* allocatorContext,
                                                              void* (*allocate_function)(void*, u64),
                                                              void(*free_function)(void*, void*))
  {
    gg_assert(numEntries > 0);
    gg_assert(entryStride > 0);

    if (!allocate_function)
      allocate_function = reliable_default_allocate_function;

    if (!free_function)
      free_function = reliable_default_free_function;

    reliable_sequence_buffer_t* sequenceBuffer = (reliable_sequence_buffer_t*)allocate_function(allocatorContext, sizeof(reliable_sequence_buffer_t));

    sequenceBuffer->allocatorContext = allocatorContext;
    sequenceBuffer->allocate_function = allocate_function;
    sequenceBuffer->free_function = free_function;
    sequenceBuffer->sequence = 0;
    sequenceBuffer->numEntries = numEntries;
    sequenceBuffer->entryStride = entryStride;
    sequenceBuffer->entrySequence = (u32*)allocate_function(allocatorContext, numEntries * sizeof(u32));
    sequenceBuffer->entryData = (u08*)allocate_function(allocatorContext, numEntries * entryStride);

    gg_assert(sequenceBuffer->entrySequence);
    gg_assert(sequenceBuffer->entryData);

    memset(sequenceBuffer->entrySequence, 0xFF, numEntries * sizeof(u32));
    memset(sequenceBuffer->entryData, 0, numEntries * entryStride);

    return sequenceBuffer;
  }

  void reliable_sequence_buffer_destroy(reliable_sequence_buffer_t* sequenceBuffer)
  {
    gg_assert(sequenceBuffer);
    sequenceBuffer->free_function(sequenceBuffer->allocatorContext, sequenceBuffer->entrySequence);
    sequenceBuffer->free_function(sequenceBuffer->allocatorContext, sequenceBuffer->entryData);
    sequenceBuffer->free_function(sequenceBuffer->allocatorContext, sequenceBuffer);
  }

  void reliable_sequence_buffer_reset(reliable_sequence_buffer_t* sequenceBuffer)
  {
    gg_assert(sequenceBuffer);
    sequenceBuffer->sequence = 0;
    memset(sequenceBuffer->entrySequence, 0xFF, sequenceBuffer->numEntries * sizeof(u32));
  }

  void reliable_sequence_buffer_remove_entries(reliable_sequence_buffer_t* sequenceBuffer,
                                               s32 startSequence,
                                               s32 endSequence,
                                               void(*cleanup_function)(void*, void*, void(*free_function)(void*, void*)))
  {
    gg_assert(sequenceBuffer);
    if (endSequence < startSequence)
      endSequence += 65536;

    if (endSequence - startSequence < sequenceBuffer->numEntries)
    {
      for (s32 sequence = startSequence; sequence <= endSequence; ++sequence)
      {
        if (cleanup_function)
        {
          cleanup_function(sequenceBuffer->entryData + sequenceBuffer->entryStride * (sequence % sequenceBuffer->numEntries),
                           sequenceBuffer->allocatorContext,
                           sequenceBuffer->free_function);
        }
        sequenceBuffer->entrySequence[sequence % sequenceBuffer->numEntries] = 0xFFFFFFFF;
      }
    }
    else
    {
      for (s32 i = 0; i < sequenceBuffer->numEntries; ++i)
      {
        if (cleanup_function)
        {
          cleanup_function(sequenceBuffer->entryData + sequenceBuffer->entryStride * i,
                           sequenceBuffer->allocatorContext,
                           sequenceBuffer->free_function);
        }
        sequenceBuffer->entrySequence[i] = 0xFFFFFFFF;
      }
    }
  }

  bool reliable_sequence_buffer_test_insert(reliable_sequence_buffer_t* sequenceBuffer, u16 sequence)
  {
    gg_assert(sequenceBuffer);
    return reliable_sequence_less_than(sequence, sequenceBuffer->sequence - ((u16)sequenceBuffer->numEntries)) ? false : true;
  }

  void* reliable_sequence_buffer_insert(reliable_sequence_buffer_t* sequenceBuffer, u16 sequence)
  {
    gg_assert(sequenceBuffer);

    if (reliable_sequence_less_than(sequence, sequenceBuffer->sequence - ((u16)sequenceBuffer->numEntries)))
    {
      return nullptr;
    }

    if (reliable_sequence_greater_than(sequence + 1, sequenceBuffer->sequence))
    {
      reliable_sequence_buffer_remove_entries(sequenceBuffer, sequenceBuffer->sequence, sequence, nullptr);

      sequenceBuffer->sequence = sequence + 1;
    }

    s32 index = sequence % sequenceBuffer->numEntries;
    sequenceBuffer->entrySequence[index] = sequence;
    return sequenceBuffer->entryData + index * sequenceBuffer->entryStride;
  }

  void* reliable_sequence_buffer_insert_with_cleanup(reliable_sequence_buffer_t* sequenceBuffer, u16 sequence,
                                                     void(*cleanup_function)(void*, void*, void(*free_function)(void*, void*)))
  {
    gg_assert(sequenceBuffer);

    if (reliable_sequence_greater_than(sequence + 1, sequenceBuffer->sequence))
    {
      reliable_sequence_buffer_remove_entries(sequenceBuffer, sequenceBuffer->sequence, sequence, cleanup_function);
      sequenceBuffer->sequence = sequence + 1;
    }
    else if (reliable_sequence_less_than(sequence, sequenceBuffer->sequence - ((u16)sequenceBuffer->numEntries)))
    {
      return nullptr;
    }

    s32 index = sequence % sequenceBuffer->numEntries;

    if (sequenceBuffer->entrySequence[index] != 0xFFFFFFFF)
    {
      cleanup_function(sequenceBuffer->entryData + sequenceBuffer->entryStride * index,
                       sequenceBuffer->allocatorContext,
                       sequenceBuffer->free_function);
    }

    sequenceBuffer->entrySequence[index] = sequence;
    return sequenceBuffer->entryData + index * sequenceBuffer->entryStride;
  }

  void reliable_sequence_buffer_remove(reliable_sequence_buffer_t* sequenceBuffer, u16 sequence)
  {
    gg_assert(sequenceBuffer);
    sequenceBuffer->entrySequence[sequence % sequenceBuffer->numEntries] = 0xFFFFFFFF;
  }

  void reliable_sequence_buffer_remove_with_cleanup(reliable_sequence_buffer_t* sequenceBuffer, u16 sequence,
                                                    void(*cleanup_function)(void*, void*, void(*free_function)(void*, void*)))
  {
    gg_assert(sequenceBuffer);
    s32 index = sequence % sequenceBuffer->numEntries;
    if (sequenceBuffer->entrySequence[index] != 0xFFFFFFFF)
    {
      sequenceBuffer->entrySequence[index] = 0xFFFFFFFF;
      cleanup_function(sequenceBuffer->entryData + sequenceBuffer->entryStride * index,
                       sequenceBuffer->allocatorContext,
                       sequenceBuffer->free_function);
    }
  }

  s32 reliable_sequence_buffer_available(reliable_sequence_buffer_t* sequenceBuffer, u16 sequence)
  {
    gg_assert(sequenceBuffer);
    return sequenceBuffer->entrySequence[sequence % sequenceBuffer->numEntries] == 0xFFFFFFFF;
  }

  s32 reliable_sequence_buffer_exists(reliable_sequence_buffer_t* sequenceBuffer, u16 sequence)
  {
    gg_assert(sequenceBuffer);
    return sequenceBuffer->entrySequence[sequence % sequenceBuffer->numEntries] == (u32)sequence;
  }


  void* reliable_sequence_buffer_find(reliable_sequence_buffer_t* sequenceBuffer, u16 sequence)
  {
    gg_assert(sequenceBuffer);
    s32 index = sequence % sequenceBuffer->numEntries;

    return (sequenceBuffer->entrySequence[index] == (u32)sequence) ? (sequenceBuffer->entryData + index * sequenceBuffer->entryStride) : nullptr;
  }

  void* reliable_sequence_buffer_at_index(reliable_sequence_buffer_t* sequenceBuffer, s32 index)
  {
    gg_assert(sequenceBuffer);
    gg_assert(index >= 0);
    gg_assert(index < sequenceBuffer->numEntries);

    return sequenceBuffer->entrySequence[index] != 0xFFFFFFFF ? (sequenceBuffer->entryData + index * sequenceBuffer->entryStride) : nullptr;
  }

  void reliable_sequence_buffer_generate_ack_bits(reliable_sequence_buffer_t* sequenceBuffer, u16* ack, u32* ackBits)
  {
    gg_assert(sequenceBuffer);
    gg_assert(ack);
    gg_assert(ackBits);

    *ack = sequenceBuffer->sequence - 1;
    *ackBits = 0;
    u32 mask = 1;
    for (s32 i = 0; i < 32; ++i)
    {
      u16 sequence = *ack - ((u16)i);
      if (reliable_sequence_buffer_exists(sequenceBuffer, sequence))
        *ackBits |= mask;

      mask <<= 1;
    }
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  void reliable_write_u08(u08** p, u08 value)
  {
    **p = value;
    ++(*p);
  }


  void reliable_write_u16(u08** p, u16 value)
  {
    (*p)[0] = value & 0xFF;
    (*p)[1] = value >> 8;
    *p += 2;
  }

  void reliable_write_u32(u08** p, u32 value)
  {
    (*p)[0] = value & 0xFF;
    (*p)[1] = (value >> 8) & 0xFF;
    (*p)[2] = (value >> 16) & 0xFF;
    (*p)[3] = value >> 24;
    *p += 4;
  }

  void reliable_write_u64(u08** p, u64 value)
  {
    (*p)[0] = value & 0xFF;
    (*p)[1] = (value >> 8) & 0xFF;
    (*p)[2] = (value >> 16) & 0xFF;
    (*p)[3] = (value >> 24) & 0xFF;
    (*p)[4] = (value >> 32) & 0xFF;
    (*p)[5] = (value >> 40) & 0xFF;
    (*p)[6] = (value >> 48) & 0xFF;
    (*p)[7] = value >> 56;
    *p += 8;
  }

  void reliable_write_bytes(u08** p, u08* byteArray, s32 numBytes)
  {
    for (s32 i = 0; i < numBytes; ++i)
    {
      reliable_write_u08(p, byteArray[i]);
    }
  }


  u08 reliable_read_u08(u08** p)
  {
    u08 value = **p;
    ++(*p);
    return value;
  }

  u16 reliable_read_u16(u08** p)
  {
    u16 value;
    value = (*p)[0];
    value |= (((u16)((*p)[1])) << 8);
    *p += 2;
    return value;
  }

  u32 reliable_read_u32(u08** p)
  {
    u32 value;
    value = (*p)[0];
    value |= (((u32)((*p)[1])) << 8);
    value |= (((u32)((*p)[2])) << 16);
    value |= (((u32)((*p)[3])) << 24);
    *p += 4;
    return value;
  }

  u64 reliable_read_u64(u08** p)
  {
    u64 value;
    value = (*p)[0];
    value |= (((u64)((*p)[1])) << 8);
    value |= (((u64)((*p)[2])) << 16);
    value |= (((u64)((*p)[3])) << 24);
    value |= (((u64)((*p)[4])) << 32);
    value |= (((u64)((*p)[5])) << 40);
    value |= (((u64)((*p)[6])) << 48);
    value |= (((u64)((*p)[7])) << 56);
    *p += 8;
    return value;
  }

  void reliable_read_bytes(u08** p, u08* byte_array, s32 numBytes)
  {
    for (s32 i = 0; i < numBytes; ++i)
    {
      byte_array[i] = reliable_read_u08(p);
    }
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  struct reliable_fragment_reassembly_data_t
  {
    u16 sequence;
    u16 ack;
    u16 ackBits;
    s32 numFragmentsReceived;
    s32 numFragmentsTotal;
    u08* packetData;
    s32 packetBytes;
    s32 packetHeaderBytes;
    u08 fragmentReceived[256];
  };


  void reliable_fragment_reassembly_data_cleanup(void* data, void* allocatorContext, void(*free_function)(void*, void*))
  {
    gg_assert(free_function);
    reliable_fragment_reassembly_data_t* reassemblyData = (reliable_fragment_reassembly_data_t*)data;

    if (reassemblyData->packetData)
    {
      free_function(allocatorContext, reassemblyData->packetData);
      reassemblyData->packetData = nullptr;
    }
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  struct reliable_endpoint_t
  {
    void* allocatorContext;
    reliable_config_t config;
    f64 time;
    f32 rtt;
    f32 packetLoss;
    f32 sentBandwidthKbps;
    f32 receivedBandwidthKbps;
    f32 ackedBandwidthKbps;
    s32 numAcks;
    u16* acks;
    u16 sequence;

    reliable_sequence_buffer_t* sentPackets;
    reliable_sequence_buffer_t* receivedPackets;
    reliable_sequence_buffer_t* fragmentReassembly;

    u64 counters[RELIABLE_ENDPOINT_NUM_COUNTERS];

    void* (*allocate_function)(void*, u64);
    void(*free_function)(void*, void*);
  };

  struct reliable_sent_packet_data_t
  {
    f64 time;
    u32 acked : 1;
    u32 packetBytes : 31;
  };

  struct reliable_received_packet_data_t
  {
    f64 time;
    u32 packetBytes;
  };

  void reliable_default_config(reliable_config_t * config)
  {
    gg_assert(config);

    memset(config, 0, sizeof(reliable_config_t));
    config->name[0] = 'e';
    config->name[1] = 'n';
    config->name[2] = 'd';
    config->name[3] = 'p';
    config->name[4] = 'o';
    config->name[5] = 'i';
    config->name[6] = 'n';
    config->name[7] = 't';
    config->name[8] = '\0';

    config->maxPacketSize = 16 * 1024;
    config->fragmentAbove = 1024;
    config->maxFragments = 16;
    config->fragmentSize = 1024;
    config->ackBufferSize = 256;
    config->sentPacketsBufferSize = 256;
    config->receivedPacketsBufferSize = 256;
    config->fragmentReassemblyBufferSize = 64;

    config->rttSmoothingFactor = 0.0025f;
    config->packetLossSmoothingFactor = 0.1f;
    config->bandwidthSmoothingFactor = 0.1f;
    config->packetHeaderSize = 28; // note: UDP over IPv4 = 20 + 8 bytes.  UDP over IPv6 = 40 + 8 bytes
  }

  reliable_endpoint_t * reliable_endpoint_create(reliable_config_t * config, f64 time)
  {
    gg_assert(config);
    gg_assert(config->maxPacketSize > 0);
    gg_assert(config->fragmentAbove > 0);
    gg_assert(config->maxFragments > 0);
    gg_assert(config->maxFragments <= 256);
    gg_assert(config->fragmentSize > 0);
    gg_assert(config->ackBufferSize > 0);
    gg_assert(config->sentPacketsBufferSize > 0);
    gg_assert(config->receivedPacketsBufferSize > 0);
    gg_assert(config->transmit_packet_function != nullptr);
    gg_assert(config->process_packet_function != nullptr);

    void* allocatorContext = config->allocatorContext;
    void* (*allocate_function)(void*, u64) = config->allocate_function;
    void(*free_function)(void*, void*) = config->free_function;

    if (!allocate_function)
      allocate_function = reliable_default_allocate_function;

    if (!free_function)
      free_function = reliable_default_free_function;

    reliable_endpoint_t* endpoint = (reliable_endpoint_t *)allocate_function(allocatorContext, sizeof(reliable_endpoint_t));

    gg_assert(endpoint);

    memset(endpoint, 0, sizeof(reliable_endpoint_t));

    endpoint->allocatorContext = allocatorContext;
    endpoint->allocate_function = allocate_function;
    endpoint->free_function = free_function;
    endpoint->config = *config;
    endpoint->time = time;

    endpoint->acks = (u16*)allocate_function(allocatorContext, config->ackBufferSize * sizeof(u16));

    endpoint->sentPackets = reliable_sequence_buffer_create(config->sentPacketsBufferSize,
                                                            sizeof(reliable_sent_packet_data_t),
                                                            allocatorContext,
                                                            allocate_function,
                                                            free_function);
    endpoint->receivedPackets = reliable_sequence_buffer_create(config->receivedPacketsBufferSize,
                                                                sizeof(reliable_received_packet_data_t),
                                                                allocatorContext,
                                                                allocate_function,
                                                                free_function);
    endpoint->fragmentReassembly = reliable_sequence_buffer_create(config->fragmentReassemblyBufferSize,
                                                                   sizeof(reliable_fragment_reassembly_data_t),
                                                                   allocatorContext,
                                                                   allocate_function,
                                                                   free_function);
    memset(endpoint->acks, 0, config->ackBufferSize * sizeof(u16));

    return endpoint;
  }

  void reliable_endpoint_destroy(reliable_endpoint_t* endpoint)
  {
    gg_assert(endpoint);
    gg_assert(endpoint->acks);
    gg_assert(endpoint->sentPackets);
    gg_assert(endpoint->receivedPackets);

    for (s32 i = 0; i < endpoint->config.fragmentReassemblyBufferSize; ++i)
    {
      reliable_fragment_reassembly_data_t* reassemblyData = (reliable_fragment_reassembly_data_t*)
        reliable_sequence_buffer_at_index(endpoint->fragmentReassembly, i);

      if (reassemblyData && reassemblyData->packetData)
      {
        endpoint->free_function(endpoint->allocatorContext, reassemblyData->packetData);
        reassemblyData->packetData = nullptr;
      }
    }

    endpoint->free_function(endpoint->allocatorContext, endpoint->acks);

    reliable_sequence_buffer_destroy(endpoint->sentPackets);
    reliable_sequence_buffer_destroy(endpoint->receivedPackets);
    reliable_sequence_buffer_destroy(endpoint->fragmentReassembly);

    endpoint->free_function(endpoint->allocatorContext, endpoint);
  }

  u16 reliable_endpoint_next_packet_sequence(reliable_endpoint_t * endpoint)
  {
    gg_assert(endpoint);
    return endpoint->sequence;
  }

  s32 reliable_write_packet_header(u08* packetData, u16 sequence, u16 ack, u32 ackBits)
  {
    u08* p = packetData;
    u08 prefixByte = 0;

    if ((ackBits & 0x000000FF) != 0x000000FF)
      prefixByte |= (1 << 1);

    if ((ackBits & 0x0000FF00) != 0x0000FF00)
      prefixByte |= (1 << 2);

    if ((ackBits & 0x00FF0000) != 0x00FF0000)
      prefixByte |= (1 << 3);

    if ((ackBits & 0xFF000000) != 0xFF000000)
      prefixByte |= (1 << 4);

    s32 sequenceDifference = sequence - ack;
    if (sequenceDifference < 0)
      sequenceDifference += 65536;

    if (sequenceDifference <= 255)
      prefixByte |= (1 << 5);

    reliable_write_u08(&p, prefixByte);
    reliable_write_u16(&p, sequence);

    if (sequenceDifference <= 255)
      reliable_write_u08(&p, (u08)sequenceDifference);
    else
      reliable_write_u16(&p, ack);


    if ((ackBits & 0x000000FF) != 0x000000FF)
      reliable_write_u08(&p, (u08)((ackBits & 0x000000FF)));

    if ((ackBits & 0x0000FF00) != 0x0000FF00)
      reliable_write_u08(&p, (u08)((ackBits & 0x0000FF00) >> 8));

    if ((ackBits & 0x00FF0000) != 0x00FF0000)
      reliable_write_u08(&p, (u08)((ackBits & 0x00FF0000) >> 16));

    if ((ackBits & 0xFF000000) != 0xFF000000)
      reliable_write_u08(&p, (u08)((ackBits & 0xFF000000) >> 24));

    gg_assert(p - packetData <= RELIABLE_MAX_PACKET_HEADER_BYTES);

    return (s32)(p - packetData);
  }

  void reliable_endpoint_send_packet(reliable_endpoint_t * endpoint, u08 * packetData, s32 packetBytes)
  {
    gg_assert(endpoint);
    gg_assert(packetData);
    gg_assert(packetBytes > 0);

    if (packetBytes > endpoint->config.maxPacketSize)
    {
      reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] packet too large to send.  Packet is %d bytes, maximum is %d\n",
                      endpoint->config.name, packetBytes, endpoint->config.maxPacketSize);
      endpoint->counters[RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_TOO_LARGE_TO_SEND]++;
      return;
    }

    u16 sequence = endpoint->sequence++;
    u16 ack;
    u32 ackBits;

    reliable_sequence_buffer_generate_ack_bits(endpoint->receivedPackets, &ack, &ackBits);

    //reliable_printf(GG_LOG_LEVEL_DEBUG, "[%s] sending packet %d\n", endpoint->config.name, sequence);

    reliable_sent_packet_data_t* sentPacketData = (reliable_sent_packet_data_t*)reliable_sequence_buffer_insert(endpoint->sentPackets, sequence);

    gg_assert(sentPacketData);

    sentPacketData->time = endpoint->time;
    sentPacketData->packetBytes = endpoint->config.packetHeaderSize + packetBytes;
    sentPacketData->acked = 0;

    if (packetBytes <= endpoint->config.fragmentAbove)
    {
      // regular packet
      reliable_printf(GG_LOG_LEVEL_DEBUG, "[%s] sending packet %d without fragmentation\n", endpoint->config.name, sequence);
      
      u08* transmitPacketData = (u08*)endpoint->allocate_function(endpoint->allocatorContext, packetBytes + RELIABLE_MAX_PACKET_HEADER_BYTES);

      s32 packetHeaderBytes = reliable_write_packet_header(transmitPacketData, sequence, ack, ackBits);

      memcpy(transmitPacketData + packetHeaderBytes, packetData, packetBytes);

      endpoint->config.transmit_packet_function(endpoint->config.context, endpoint->config.index, sequence, transmitPacketData, packetHeaderBytes + packetBytes);

      endpoint->free_function(endpoint->allocatorContext, transmitPacketData);
    }
    else
    {
      // fragmented packet
      u08 packetHeader[RELIABLE_MAX_PACKET_HEADER_BYTES];
      memset(packetHeader, 0, RELIABLE_MAX_PACKET_HEADER_BYTES);

      s32 packetHeaderBytes = reliable_write_packet_header(packetHeader, sequence, ack, ackBits);

      s32 numFragments = (packetBytes / endpoint->config.fragmentSize) + ((packetBytes % endpoint->config.fragmentSize) != 0 ? 1 : 0);

      reliable_printf(GG_LOG_LEVEL_DEBUG, "[%s] sending packet %d as %d fragments\n", endpoint->config.name, sequence, numFragments);
      
      gg_assert(numFragments >= 1);
      gg_assert(numFragments <= endpoint->config.maxFragments);

      s32 fragmentBufferSize = RELIABLE_FRAGMENT_HEADER_BYTES + RELIABLE_MAX_PACKET_HEADER_BYTES + endpoint->config.fragmentSize;

      u08* fragmentPacketData = (u08*)endpoint->allocate_function(endpoint->allocatorContext, fragmentBufferSize);

      u08* q = packetData;
      u08* end = q + packetBytes;

      for (s32 fragmentId = 0; fragmentId < numFragments; ++fragmentId)
      {
        u08* p = fragmentPacketData;

        reliable_write_u08(&p, 1);
        reliable_write_u16(&p, sequence);
        reliable_write_u08(&p, (u08)fragmentId);
        reliable_write_u08(&p, (u08)(numFragments - 1));

        if (fragmentId == 0)
        {
          memcpy(p, packetHeader, packetHeaderBytes);
          p += packetHeaderBytes;
        }

        s32 bytesToCopy = endpoint->config.fragmentSize;

        if (q + bytesToCopy > end)
        {
          bytesToCopy = (s32)(end - q);
        }

        memcpy(p, q, bytesToCopy);

        p += bytesToCopy;
        q += bytesToCopy;

        s32 fragmentPacketBytes = (s32)(p - fragmentPacketData);

        endpoint->config.transmit_packet_function(endpoint->config.context, endpoint->config.index, sequence, fragmentPacketData, fragmentPacketBytes);

        endpoint->counters[RELIABLE_ENDPOINT_COUNTER_NUM_FRAGMENTS_SENT]++;
      }
      endpoint->free_function(endpoint->allocatorContext, fragmentPacketData);
    }
    endpoint->counters[RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_SENT]++;
  }

  s32 reliable_read_packet_header(const c08* name, u08* packetData, s32 packetBytes, u16* sequence, u16* ack, u32* ackBits)
  {
    if (packetBytes < 3)
    {
      gg_assert(name);
      reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] packet too small for packet header (1)\n", name);
      return -1;
    }

    u08* p = packetData;

    u08 prefixByte = reliable_read_u08(&p);

    if ((prefixByte & 1) != 0)
    {
      gg_assert(name);
      reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] prefix byte does not indicate a regular packet\n", name);
      return -1;
    }

    *sequence = reliable_read_u16(&p);

    if (prefixByte & (1 << 5))
    {
      if (packetBytes < 3 + 1)
      {
        gg_assert(name);
        reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] packet too small for packet header (2)\n", name);
        return -1;
      }
      u08 sequenceDifference = reliable_read_u08(&p);
      *ack = *sequence - sequenceDifference;
    }
    else
    {
      if (packetBytes < 3 + 2)
      {
        gg_assert(name);
        reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] packet too small for packet header (3)\n", name);
        return -1;
      }
      *ack = reliable_read_u16(&p);
    }

    s32 expectedBytes = 0;

    for (s32 i = 1; i <= 4; ++i)
    {
      if (prefixByte & (1 << i))
        expectedBytes++;
    }

    if (packetBytes < (p - packetData) + expectedBytes)
    {
      gg_assert(name);
      reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] packet too small for packet header (4)\n", name);
      return -1;
    }

    *ackBits = 0xFFFFFFFF;

    if (prefixByte & (1 << 1))
    {
      *ackBits &= 0xFFFFFF00;
      *ackBits |= (u32)(reliable_read_u08(&p));
    }

    if (prefixByte & (1 << 2))
    {
      *ackBits &= 0xFFFF00FF;
      *ackBits |= (u32)(reliable_read_u08(&p)) << 8;
    }

    if (prefixByte & (1 << 3))
    {
      *ackBits &= 0xFF00FFFF;
      *ackBits |= (u32)(reliable_read_u08(&p)) << 16;
    }

    if (prefixByte & (1 << 4))
    {
      *ackBits &= 0x00FFFFFF;
      *ackBits |= (u32)(reliable_read_u08(&p)) << 24;
    }

    return (s32)(p - packetData);
  }

  s32 reliable_read_fragment_header(c08* name, u08* packetData, s32 packetBytes, s32 maxFragments, s32 fragmentSize,
                                    s32* fragmentId, s32* numFragments, s32* fragmentBytes,
                                    u16* sequence, u16* ack, u32* ackBits)
  {
    if (packetBytes < RELIABLE_FRAGMENT_HEADER_BYTES)
    {
      gg_assert(name);
      reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] packet is too small to read fragment head\n", name);
      return -1;
    }

    u08* p = packetData;

    u08 prefixByte = reliable_read_u08(&p);

    if (prefixByte != 1)
    {
      gg_assert(name);
      reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] prefix byte is not a fragment\n", name);
      return -1;
    }

    *sequence = reliable_read_u16(&p);
    *fragmentId = (s32)reliable_read_u08(&p);
    *numFragments = ((s32)reliable_read_u08(&p)) + 1;

    if (*numFragments > maxFragments)
    {
      gg_assert(name);
      reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] num fragmnts %d outside range of max fragments %d\n", name, *fragmentId, maxFragments);
      return -1;
    }

    if (*fragmentId >= *numFragments)
    {
      gg_assert(name);
      reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] fragment ID %d outside range of num fragments %d\n", name, *fragmentId, *numFragments);
      return -1;
    }

    *fragmentBytes = packetBytes - RELIABLE_FRAGMENT_HEADER_BYTES;
    u16 packetSequence = 0;
    u16 packetAck = 0;
    u32 packetAckBits = 0;

    if (*fragmentId == 0)
    {
      s32 packetHeaderBytes = reliable_read_packet_header(name, packetData + RELIABLE_FRAGMENT_HEADER_BYTES,
                                                          packetBytes, &packetSequence, &packetAck, &packetAckBits);

      if (packetHeaderBytes < 0)
      {
        gg_assert(name);
        reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] bad packet header in fragment\n", name);
        return -1;
      }
      if (packetSequence != *sequence)
      {
        gg_assert(name);
        reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] bad packet sequence in fragment.  Expected %d, got %d\n", name, *sequence, packetSequence);
        return -1;
      }
      *fragmentBytes = packetBytes - packetHeaderBytes - RELIABLE_FRAGMENT_HEADER_BYTES;
    }

    *ack = packetAck;
    *ackBits = packetAckBits;

    if (*fragmentBytes > fragmentSize)
    {
      gg_assert(name);
      reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] fragment bytes %d > fragment size %d\n", name, *fragmentBytes, fragmentSize);
      return -1;
    }

    if (*fragmentBytes > fragmentSize)
    {
      gg_assert(name);
      reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] fragment %d is %d bytes, which is not the expected fragment size of %d\n", name, *fragmentId, *fragmentBytes, fragmentSize);
      return -1;
    }

    return (s32)(p - packetData);
  }

  void reliable_store_fragment_data(reliable_fragment_reassembly_data_t* reassemblyData,
                                    u16 sequence, u16 ack, u32 ackBits, s32 fragmentId, s32 fragmentSize,
                                    u08* fragmentData, s32 fragmentBytes)
  {
    if (fragmentId == 0)
    {
      u08 packetHeader[RELIABLE_MAX_PACKET_HEADER_BYTES];
      memset(packetHeader, 0, RELIABLE_MAX_PACKET_HEADER_BYTES);

      reassemblyData->packetHeaderBytes = reliable_write_packet_header(packetHeader, sequence, ack, ackBits);

      memcpy(reassemblyData->packetData + RELIABLE_MAX_PACKET_HEADER_BYTES - reassemblyData->packetHeaderBytes,
             packetHeader,
             reassemblyData->packetHeaderBytes);

      fragmentData += reassemblyData->packetHeaderBytes;
      fragmentBytes -= reassemblyData->packetHeaderBytes;
    }

    if (fragmentId == reassemblyData->numFragmentsTotal - 1)
    {
      reassemblyData->packetBytes = (reassemblyData->numFragmentsTotal - 1) * fragmentSize + fragmentBytes;
    }

    memcpy(reassemblyData->packetData + RELIABLE_MAX_PACKET_HEADER_BYTES + fragmentId * fragmentSize, fragmentData, fragmentBytes);
  }

  void reliable_endpoint_receive_packet(reliable_endpoint_t * endpoint, u08 * packetData, s32 packetBytes)
  {
    gg_assert(endpoint);
    gg_assert(packetData);
    gg_assert(packetBytes > 0);

    if (packetBytes > endpoint->config.maxPacketSize)
    {
      reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] packet too large to receive. Packet is %d bytes, max is %d\n", endpoint->config.name, packetBytes, endpoint->config.maxPacketSize);
      endpoint->counters[RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_TOO_LARGE_TO_RECEIVE]++;
      return;
    }

    u08 prefixByte = packetData[0];

    if ((prefixByte & 1) == 0)
    {
      // regular packet
      endpoint->counters[RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_RECEIVED]++;

      u16 sequence = 0;
      u16 ack = 0;
      u32 ackBits = 0;

      s32 packetHeaderBytes = reliable_read_packet_header(endpoint->config.name, packetData, packetBytes, &sequence, &ack, &ackBits);

      if (packetHeaderBytes < 0)
      {
        reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] ignoring invalid packet.  Could not read packet header\n", endpoint->config.name);
        endpoint->counters[RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_INVALID]++;
        return;
      }

      if (!reliable_sequence_buffer_test_insert(endpoint->receivedPackets, sequence))
      {
        reliable_printf(GG_LOG_LEVEL_DEBUG, "[%s] ignoring stale packet %d\n", endpoint->config.name, sequence);
        endpoint->counters[RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_STALE]++;
        return;
      }

      reliable_printf(GG_LOG_LEVEL_DEBUG, "[%s] processing packet %d\n", endpoint->config.name, sequence);

      if (endpoint->config.process_packet_function(endpoint->config.context,
                                                   endpoint->config.index,
                                                   sequence,
                                                   packetData + packetHeaderBytes,
                                                   packetBytes - packetHeaderBytes))
      {
        reliable_printf(GG_LOG_LEVEL_DEBUG, "[%s] process packet %d successful\n", endpoint->config.name, sequence);

        reliable_received_packet_data_t* receivedPacketData = (reliable_received_packet_data_t *)reliable_sequence_buffer_insert(endpoint->receivedPackets, sequence);

        gg_assert(receivedPacketData);

        receivedPacketData->time = endpoint->time;
        receivedPacketData->packetBytes = endpoint->config.packetHeaderSize + packetBytes;

        for (s32 i = 0; i < 32; ++i)
        {
          if (ackBits & 1)
          {
            u16 ackSequence = ack - ((u16)i);
            reliable_sent_packet_data_t* sentPacketData = (reliable_sent_packet_data_t*)reliable_sequence_buffer_find(endpoint->sentPackets, ackSequence);

            if (sentPacketData && !sentPacketData->acked && endpoint->numAcks < endpoint->config.ackBufferSize)
            {
              reliable_printf(GG_LOG_LEVEL_DEBUG, "[%s] acked packet %d\n", endpoint->config.name, ackSequence);

              endpoint->acks[endpoint->numAcks++] = ackSequence;
              endpoint->counters[RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_ACKED]++;
              sentPacketData->acked = 1;

              f32 rtt = (f32)(endpoint->time - sentPacketData->time) * 1000.0f;
              gg_assert(rtt >= 0.0);

              if ((endpoint->rtt == 0.0f && rtt > 0.0f) || fabs(endpoint->rtt - rtt) < 0.00001)
                endpoint->rtt = rtt;
              else
                endpoint->rtt += (rtt - endpoint->rtt) * endpoint->config.rttSmoothingFactor;
            }
          }

          ackBits >>= 1;
        }
      }
      else
      {
        reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] process packet failed\n", endpoint->config.name);
      }
    }
    else
    {
      // fragmented packet
      s32 fragmentId, numFragments, fragmentBytes;
      u16 sequence, ack;
      u32 ackBits;

      s32 fragmentHeaderBytes = reliable_read_fragment_header(endpoint->config.name,
                                                              packetData,
                                                              packetBytes,
                                                              endpoint->config.maxFragments,
                                                              endpoint->config.fragmentSize,
                                                              &fragmentId,
                                                              &numFragments,
                                                              &fragmentBytes,
                                                              &sequence,
                                                              &ack,
                                                              &ackBits);

      if (fragmentHeaderBytes < 0)
      {
        reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] ignoring invalid fragment.  Could not read fragment header\n", endpoint->config.name);
        endpoint->counters[RELIABLE_ENDPOINT_COUNTER_NUM_FRAGMENTS_INVALID]++;
        return;
      }

      reliable_fragment_reassembly_data_t* reassemblyData = (reliable_fragment_reassembly_data_t*)reliable_sequence_buffer_find(endpoint->fragmentReassembly, sequence);

      if (!reassemblyData)
      {
        reassemblyData = (reliable_fragment_reassembly_data_t*)reliable_sequence_buffer_insert_with_cleanup(endpoint->fragmentReassembly, sequence, reliable_fragment_reassembly_data_cleanup);

        if (!reassemblyData)
        {
          reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] ignoring invalid fragment.  Could not insert in reassembly buffer (stale)\n", endpoint->config.name);
          endpoint->counters[RELIABLE_ENDPOINT_COUNTER_NUM_FRAGMENTS_INVALID]++;
          return;
        }

        s32 packetBufferSize = RELIABLE_MAX_PACKET_HEADER_BYTES + numFragments * endpoint->config.fragmentSize;

        reassemblyData->sequence = sequence;
        reassemblyData->ack = 0;
        reassemblyData->ackBits = 0;
        reassemblyData->numFragmentsReceived = 0;
        reassemblyData->numFragmentsTotal = numFragments;
        reassemblyData->packetData = (u08*)endpoint->allocate_function(endpoint->allocatorContext, packetBufferSize);
        reassemblyData->packetBytes = 0;
        memset(reassemblyData->fragmentReceived, 0, sizeof(reassemblyData->numFragmentsReceived));
      }

      if (numFragments != (s32)reassemblyData->numFragmentsTotal)
      {
        reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] ignoring invalid fragment.  Fragment count mismatch.  Expected %d, got %d\n", endpoint->config.name, (s32)reassemblyData->numFragmentsTotal, numFragments);
        endpoint->counters[RELIABLE_ENDPOINT_COUNTER_NUM_FRAGMENTS_INVALID]++;
        return;
      }

      if (reassemblyData->fragmentReceived[fragmentId])
      {
        reliable_printf(GG_LOG_LEVEL_ERROR, "[%s] ignoring fragment %d of packet %d.  Fragment alread received\n", endpoint->config.name, fragmentId, sequence);
        return;
      }

      reliable_printf(GG_LOG_LEVEL_DEBUG, "[%s] received fragment %d of packet %d (%d/%d)\n", endpoint->config.name, fragmentId, sequence, reassemblyData->numFragmentsReceived + 1, numFragments);

      reassemblyData->numFragmentsReceived++;
      reassemblyData->fragmentReceived[fragmentId] = 1;

      reliable_store_fragment_data(reassemblyData, sequence, ack, ackBits, fragmentId, endpoint->config.fragmentSize,
                                   packetData + fragmentHeaderBytes,
                                   packetBytes - fragmentHeaderBytes);

      if (reassemblyData->numFragmentsReceived == reassemblyData->numFragmentsTotal)
      {
        reliable_printf(GG_LOG_LEVEL_DEBUG, "[%s] completed reassembly of packet %d\n", endpoint->config.name, sequence);

        reliable_endpoint_receive_packet(endpoint,
                                         reassemblyData->packetData + RELIABLE_MAX_PACKET_HEADER_BYTES - reassemblyData->packetHeaderBytes,
                                         reassemblyData->packetHeaderBytes + reassemblyData->packetBytes);

        reliable_sequence_buffer_remove_with_cleanup(endpoint->fragmentReassembly, sequence, reliable_fragment_reassembly_data_cleanup);
      }

      endpoint->counters[RELIABLE_ENDPOINT_COUNTER_NUM_FRAGMENTS_RECEIVED]++;
    }
  }

  void reliable_endpoint_free_packet(reliable_endpoint_t * endpoint, void * packet)
  {
    gg_assert(endpoint);
    gg_assert(packet);
    endpoint->free_function(endpoint->allocatorContext, packet);
  }
  u16* reliable_endpoint_get_acks(reliable_endpoint_t * endpoint, s32 * numAcks)
  {
    gg_assert(endpoint);
    gg_assert(numAcks);
    *numAcks = endpoint->numAcks;
    return endpoint->acks;
  }
  void reliable_endpoint_clear_acks(reliable_endpoint_t * endpoint)
  {
    gg_assert(endpoint);
    endpoint->numAcks = 0;
  }
  void reliable_endpoint_reset(reliable_endpoint_t * endpoint)
  {
    gg_assert(endpoint);

    endpoint->numAcks = 0;
    endpoint->sequence = 0;

    memset(endpoint->acks, 0, endpoint->config.ackBufferSize * sizeof(u16));

    for (s32 i = 0; i < endpoint->config.fragmentReassemblyBufferSize; ++i)
    {
      reliable_fragment_reassembly_data_t* reassemblyData = (reliable_fragment_reassembly_data_t*)reliable_sequence_buffer_at_index(endpoint->fragmentReassembly, i);

      if (reassemblyData && reassemblyData->packetData)
      {
        endpoint->free_function(endpoint->allocatorContext, reassemblyData->packetData);
        reassemblyData->packetData = nullptr;
      }
    }

    reliable_sequence_buffer_reset(endpoint->sentPackets);
    reliable_sequence_buffer_reset(endpoint->receivedPackets);
    reliable_sequence_buffer_reset(endpoint->fragmentReassembly);
  }
  void reliable_endpoint_update(reliable_endpoint_t * endpoint, f64 time)
  {
    gg_assert(endpoint);

    endpoint->time = time;

    // calculate packet loss
    {
      u32 baseSequence = (endpoint->sentPackets->sequence - endpoint->config.sentPacketsBufferSize + 1) + 0xFFFF;

      s32 numDropped = 0;
      s32 numSamples = endpoint->config.sentPacketsBufferSize / 2;

      for (s32 i = 0; i < numSamples; ++i)
      {
        u16 sequence = (u16)(baseSequence + i);
        reliable_sent_packet_data_t* sentPacketData = (reliable_sent_packet_data_t*)reliable_sequence_buffer_find(endpoint->sentPackets, sequence);

        if (sentPacketData && !sentPacketData->acked)
        {
          ++numDropped;
        }
      }

      f32 packetLoss = (((f32)numDropped) / ((f32)numSamples)) * 100.0f;
      if (fabs(endpoint->packetLoss - packetLoss) > 0.00001)
      {
        endpoint->packetLoss += (packetLoss - endpoint->packetLoss) * endpoint->config.packetLossSmoothingFactor;
      }
      else
      {
        endpoint->packetLoss = packetLoss;
      }
    }

    // calculate sent bandwidth
    {
      u32 baseSequence = (endpoint->sentPackets->sequence - endpoint->config.sentPacketsBufferSize + 1) + 0xFFFF;

      s32 bytesSent = 0;
      f64 startTime = FLT_MAX;
      f64 finishTime = 0.0;
      s32 numSamples = endpoint->config.sentPacketsBufferSize / 2;

      for (s32 i = 0; i < numSamples; ++i)
      {
        u16 sequence = (u16)(baseSequence + i);
        reliable_sent_packet_data_t* sentPacketData = (reliable_sent_packet_data_t*)reliable_sequence_buffer_find(endpoint->sentPackets, sequence);

        if (!sentPacketData)
          continue;

        bytesSent += sentPacketData->packetBytes;
        if (sentPacketData->time < startTime)
        {
          startTime = sentPacketData->time;
        }
        if (sentPacketData->time > finishTime)
        {
          finishTime = sentPacketData->time;
        }
      }
      if (startTime != FLT_MAX && finishTime != 0.0)
      {
        f32 sentBandwidthKbps = (f32)(((f64)bytesSent) / (finishTime - startTime) * 8.0f / 1000.0f);

        if (fabs(endpoint->sentBandwidthKbps - sentBandwidthKbps) > 0.00001)
        {
          endpoint->sentBandwidthKbps += (sentBandwidthKbps - endpoint->sentBandwidthKbps) * endpoint->config.bandwidthSmoothingFactor;
        }
        else
        {
          endpoint->sentBandwidthKbps = sentBandwidthKbps;
        }
      }
    }

    // calculate received bandwidth
    {
      u32 baseSequence = (endpoint->receivedPackets->sequence - endpoint->config.receivedPacketsBufferSize + 1) + 0xFFFF;

      s32 bytesSent = 0;
      f64 startTime = FLT_MAX;
      f64 finishTime = 0.0;
      s32 numSamples = endpoint->config.receivedPacketsBufferSize / 2;

      for (s32 i = 0; i < numSamples; ++i)
      {
        u16 sequence = (u16)(baseSequence + i);
        reliable_received_packet_data_t* receivedPacketData = (reliable_received_packet_data_t*)reliable_sequence_buffer_find(endpoint->receivedPackets, sequence);

        if (!receivedPacketData)
          continue;

        bytesSent += receivedPacketData->packetBytes;
        if (receivedPacketData->time < startTime)
        {
          startTime = receivedPacketData->time;
        }
        if (receivedPacketData->time > finishTime)
        {
          finishTime = receivedPacketData->time;
        }
      }
      if (startTime != FLT_MAX && finishTime != 0.0)
      {
        f32 receivedBandwidthKbps = (f32)(((f64)bytesSent) / (finishTime - startTime) * 8.0f / 1000.0f);

        if (fabs(endpoint->receivedBandwidthKbps - receivedBandwidthKbps) > 0.00001)
        {
          endpoint->receivedBandwidthKbps += (receivedBandwidthKbps - endpoint->sentBandwidthKbps) * endpoint->config.bandwidthSmoothingFactor;
        }
        else
        {
          endpoint->receivedBandwidthKbps = receivedBandwidthKbps;
        }
      }
    }

    // calculate acked bandwidth
    {
      u32 baseSequence = (endpoint->sentPackets->sequence - endpoint->config.sentPacketsBufferSize + 1) + 0xFFFF;

      s32 bytesSent = 0;
      f64 startTime = FLT_MAX;
      f64 finishTime = 0.0;
      s32 numSamples = endpoint->config.sentPacketsBufferSize / 2;

      for (s32 i = 0; i < numSamples; ++i)
      {
        u16 sequence = (u16)(baseSequence + i);
        reliable_sent_packet_data_t* sentPacketData = (reliable_sent_packet_data_t*)reliable_sequence_buffer_find(endpoint->sentPackets, sequence);

        if (!sentPacketData || !sentPacketData->acked)
          continue;

        bytesSent += sentPacketData->packetBytes;
        if (sentPacketData->time < startTime)
        {
          startTime = sentPacketData->time;
        }
        if (sentPacketData->time > finishTime)
        {
          finishTime = sentPacketData->time;
        }
      }
      if (startTime != FLT_MAX && finishTime != 0.0)
      {
        f32 ackedBandwidthKbps = (f32)(((f64)bytesSent) / (finishTime - startTime) * 8.0f / 1000.0f);

        if (fabs(endpoint->ackedBandwidthKbps - ackedBandwidthKbps) > 0.00001)
        {
          endpoint->ackedBandwidthKbps += (ackedBandwidthKbps - endpoint->ackedBandwidthKbps) * endpoint->config.bandwidthSmoothingFactor;
        }
        else
        {
          endpoint->ackedBandwidthKbps = ackedBandwidthKbps;
        }
      }
    }

  }
  f32 reliable_endpoint_rtt(reliable_endpoint_t * endpoint)
  {
    gg_assert(endpoint);
    return endpoint->rtt;
  }
  f32 reliable_endpoint_packet_loss(reliable_endpoint_t * endpoint)
  {
    gg_assert(endpoint);
    return endpoint->packetLoss;
  }
  void reliable_endpoint_bandwidth(reliable_endpoint_t * endpoint, f32 * sentBandwidthKbps, f32 * receivedBandwidthKbps, f32 * ackedBandwidthKbps)
  {
    gg_assert(endpoint);
    gg_assert(sentBandwidthKbps);
    gg_assert(receivedBandwidthKbps);
    gg_assert(ackedBandwidthKbps);
    *sentBandwidthKbps = endpoint->sentBandwidthKbps;
    *receivedBandwidthKbps = endpoint->receivedBandwidthKbps;
    *ackedBandwidthKbps = endpoint->ackedBandwidthKbps;

  }
  const u64 * reliable_endpoint_counters(reliable_endpoint_t * endpoint)
  {
    gg_assert(endpoint);
    return endpoint->counters;
  }




#if RELIABLE_ENABLE_TESTS

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

  static void check_handler(const c08* condition,
                            const c08* function,
                            const c08* file,
                            s32 line)
  {
    printf("check failed: ( %s ), function %s, file %s, line %d\n", condition, function, file, line);
#ifndef NDEBUG
#if defined( __GNUC__ )
    __builtin_trap();
#elif defined( _MSC_VER )
    __debugbreak();
#endif
#endif
    exit(1);
  }

#define check( condition )                                                                  \
do                                                                                          \
{                                                                                           \
    if ( !(condition) )                                                                     \
    {                                                                                       \
        check_handler( #condition, (const c08*) __FUNCTION__, (c08*) __FILE__, __LINE__ );  \
    }                                                                                       \
} while(0)


  static void test_endian()
  {
    u32 value = 0x11223344;
    c08* bytes = (c08*)&value;

#if GG_LITTLE_ENDIAN
    check(bytes[0] == 0x44);
    check(bytes[1] == 0x33);
    check(bytes[2] == 0x22);
    check(bytes[3] == 0x11);
#else
    check(bytes[0] == 0x11);
    check(bytes[1] == 0x22);
    check(bytes[2] == 0x33);
    check(bytes[3] == 0x44);

#endif
  }

  struct test_sequence_data_t
  {
    u16 sequence;
  };

#define TEST_SEQUENCE_BUFFER_SIZE 256

  static void test_sequence_buffer()
  {
    reliable_sequence_buffer_t* sequenceBuffer = reliable_sequence_buffer_create(TEST_SEQUENCE_BUFFER_SIZE,
                                                                                 sizeof(test_sequence_data_t),
                                                                                 nullptr, nullptr, nullptr);

    check(sequenceBuffer);
    check(sequenceBuffer->sequence == 0);
    check(sequenceBuffer->numEntries == TEST_SEQUENCE_BUFFER_SIZE);
    check(sequenceBuffer->entryStride == sizeof(test_sequence_data_t));

    for (s32 i = 0; i < TEST_SEQUENCE_BUFFER_SIZE; ++i)
    {
      check(reliable_sequence_buffer_find(sequenceBuffer, ((u16)i)) == nullptr);

    }
    for (s32 i = 0; i <= TEST_SEQUENCE_BUFFER_SIZE * 4; ++i)
    {
      test_sequence_data_t* entry = (test_sequence_data_t*)reliable_sequence_buffer_insert(sequenceBuffer, ((u16)i));
      check(entry);
      entry->sequence = (u16)i;
      check(sequenceBuffer->sequence == i + 1);
    }

    for (s32 i = 0; i <= TEST_SEQUENCE_BUFFER_SIZE; ++i)
    {
      test_sequence_data_t* entry = (test_sequence_data_t*)reliable_sequence_buffer_insert(sequenceBuffer, ((u16)i));
      check(entry == nullptr);
    }

    s32 index = TEST_SEQUENCE_BUFFER_SIZE * 4;
    for (s32 i = 0; i < TEST_SEQUENCE_BUFFER_SIZE; ++i)
    {
      test_sequence_data_t* entry = (test_sequence_data_t*)reliable_sequence_buffer_find(sequenceBuffer, ((u16)index));
      check(entry);
      check(entry->sequence == (u32)index);
      --index;
    }

    reliable_sequence_buffer_reset(sequenceBuffer);

    check(sequenceBuffer);
    check(sequenceBuffer->sequence == 0);
    check(sequenceBuffer->numEntries == TEST_SEQUENCE_BUFFER_SIZE);
    check(sequenceBuffer->entryStride == sizeof(test_sequence_data_t));

    for (s32 i = 0; i < TEST_SEQUENCE_BUFFER_SIZE; ++i)
    {
      check(reliable_sequence_buffer_find(sequenceBuffer, ((u16)i)) == nullptr);

    }

    reliable_sequence_buffer_destroy(sequenceBuffer);
  }

  static void test_generate_ack_bits()
  {
    reliable_sequence_buffer_t* sequenceBuffer = reliable_sequence_buffer_create(TEST_SEQUENCE_BUFFER_SIZE,
                                                                                 sizeof(test_sequence_data_t),
                                                                                 nullptr, nullptr, nullptr);

    u16 ack = 0;
    u32 ackBits = 0xFFFFFFFF;

    reliable_sequence_buffer_generate_ack_bits(sequenceBuffer, &ack, &ackBits);
    check(ack == 0xFFFF);
    check(ackBits == 0);

    for (s32 i = 0; i <= TEST_SEQUENCE_BUFFER_SIZE; ++i)
      reliable_sequence_buffer_insert(sequenceBuffer, (u16)i);

    reliable_sequence_buffer_generate_ack_bits(sequenceBuffer, &ack, &ackBits);
    check(ack == TEST_SEQUENCE_BUFFER_SIZE);
    check(ackBits == 0xFFFFFFFF);

    reliable_sequence_buffer_reset(sequenceBuffer);

    u16 inputAcks[] = { 1, 5, 9, 11 };
    s32 inputNumAcks = sizeof(inputAcks) / sizeof(u16);
    for (s32 i = 0; i < inputNumAcks; ++i)
      reliable_sequence_buffer_insert(sequenceBuffer, inputAcks[i]);

    reliable_sequence_buffer_generate_ack_bits(sequenceBuffer, &ack, &ackBits);

    check(ack == 11);
    check(ackBits == (1 | (1 << (11 - 9)) | (1 << (11 - 5)) | (1 << (11 - 1))));

    reliable_sequence_buffer_destroy(sequenceBuffer);
  }

  static void test_packet_header()
  {
    u16 writeSequence;
    u16 writeAck;
    u32 writeAckBits;

    u16 readSequence;
    u16 readAck;
    u32 readAckBits;

    u08 packetData[RELIABLE_MAX_PACKET_HEADER_BYTES];

    // worst case, sequence and ack are far apart, no packets acked
    writeSequence = 10000;
    writeAck = 100;
    writeAckBits = 0;

    s32 bytesWritten = reliable_write_packet_header(packetData, writeSequence, writeAck, writeAckBits);
    check(bytesWritten == RELIABLE_MAX_PACKET_HEADER_BYTES);

    s32 bytesRead = reliable_read_packet_header("test_packet_header", packetData, bytesWritten, &readSequence, &readAck, &readAckBits);

    check(bytesRead == bytesWritten);
    check(readSequence == writeSequence);
    check(readAck == writeAck);
    check(readAckBits == writeAckBits);

    // rare case, sequence and ack are far apart, significant number of acks are missing
    writeSequence = 10000;
    writeAck = 100;
    writeAckBits = 0xFEFEFFFE;

    bytesWritten = reliable_write_packet_header(packetData, writeSequence, writeAck, writeAckBits);
    check(bytesWritten == 1 + 2 + 2 + 3);

    bytesRead = reliable_read_packet_header("test_packet_header", packetData, bytesWritten, &readSequence, &readAck, &readAckBits);

    check(bytesRead == bytesWritten);
    check(readSequence == writeSequence);
    check(readAck == writeAck);
    check(readAckBits == writeAckBits);

    // common case under packet loss.  sequence and ack are close together, some acks are missing
    writeSequence = 200;
    writeAck = 100;
    writeAckBits = 0xFFFEFFFF;

    bytesWritten = reliable_write_packet_header(packetData, writeSequence, writeAck, writeAckBits);
    check(bytesWritten == 1 + 2 + 1 + 1);

    bytesRead = reliable_read_packet_header("test_packet_header", packetData, bytesWritten, &readSequence, &readAck, &readAckBits);

    check(bytesRead == bytesWritten);
    check(readSequence == writeSequence);
    check(readAck == writeAck);
    check(readAckBits == writeAckBits);

    // ideal case.  no packet loss
    writeSequence = 200;
    writeAck = 100;
    writeAckBits = 0xFFFFFFFF;

    bytesWritten = reliable_write_packet_header(packetData, writeSequence, writeAck, writeAckBits);
    check(bytesWritten == 1 + 2 + 1);

    bytesRead = reliable_read_packet_header("test_packet_header", packetData, bytesWritten, &readSequence, &readAck, &readAckBits);

    check(bytesRead == bytesWritten);
    check(readSequence == writeSequence);
    check(readAck == writeAck);
    check(readAckBits == writeAckBits);
  }

  struct test_context_t
  {
    s32 drop;
    reliable_endpoint_t* sender;
    reliable_endpoint_t* receiver;
  };

  static void test_transmit_packet_function(void* context_, s32 index, u16 sequence, u08* packetData, s32 packetBytes)
  {
    (void)sequence;

    test_context_t* context = (test_context_t*)context_;

    if (context->drop)
    {
      return;
    }

    if (index == 0)
    {
      reliable_endpoint_receive_packet(context->receiver, packetData, packetBytes);
    }
    else if (index == 1)
    {
      reliable_endpoint_receive_packet(context->sender, packetData, packetBytes);
    }
  }

  static s32 test_process_packet_function(void* context_, s32 index, u16 sequence, u08* packetData, s32 packetBytes)
  {
    test_context_t* context = (test_context_t*)context_;

    (void)context;
    (void)index;
    (void)sequence;
    (void)packetData;
    (void)packetBytes;

    return 1;
  }

#define TEST_ACKS_NUM_ITERATIONS 256
  static void test_acks()
  {
    f64 time = 100.0;
    f64 deltaTime = 0.01;

    test_context_t context;
    memset(&context, 0, sizeof(test_context_t));

    reliable_config_t senderConfig;
    reliable_config_t receiverConfig;

    reliable_default_config(&senderConfig);
    reliable_default_config(&receiverConfig);

    senderConfig.context = &context;
    senderConfig.index = 0;
    senderConfig.transmit_packet_function = &test_transmit_packet_function;
    senderConfig.process_packet_function = &test_process_packet_function;

    receiverConfig.context = &context;
    receiverConfig.index = 1;
    receiverConfig.transmit_packet_function = &test_transmit_packet_function;
    receiverConfig.process_packet_function = &test_process_packet_function;

    context.sender = reliable_endpoint_create(&senderConfig, time);
    context.receiver = reliable_endpoint_create(&receiverConfig, time);

    for (s32 i = 0; i < TEST_ACKS_NUM_ITERATIONS; ++i)
    {
      u08 dummyPacket[8];
      memset(dummyPacket, 0, sizeof(dummyPacket));

      reliable_endpoint_send_packet(context.sender, dummyPacket, sizeof(dummyPacket));
      reliable_endpoint_send_packet(context.receiver, dummyPacket, sizeof(dummyPacket));

      reliable_endpoint_update(context.sender, time);
      reliable_endpoint_update(context.receiver, time);

      time += deltaTime;
    }

    u08 senderAckedPacket[TEST_ACKS_NUM_ITERATIONS];
    memset(senderAckedPacket, 0, sizeof(senderAckedPacket));

    s32 senderNumAcks;
    u16* senderAcks = reliable_endpoint_get_acks(context.sender, &senderNumAcks);

    for (s32 i = 0; i < senderNumAcks; ++i)
    {
      if (senderAcks[i] < TEST_ACKS_NUM_ITERATIONS)
      {
        senderAckedPacket[senderAcks[i]] = 1;
      }
    }

    for (s32 i = 0; i < TEST_ACKS_NUM_ITERATIONS / 2; ++i)
    {
      check(senderAckedPacket[i] == 1);
    }

    u08 receiverAckedPacket[TEST_ACKS_NUM_ITERATIONS];
    memset(receiverAckedPacket, 0, sizeof(receiverAckedPacket));

    s32 receiverNumAcks;
    u16* receiverAcks = reliable_endpoint_get_acks(context.sender, &receiverNumAcks);
    for (s32 i = 0; i < receiverNumAcks; ++i)
    {
      if (receiverAcks[i] < TEST_ACKS_NUM_ITERATIONS)
      {
        receiverAckedPacket[receiverAcks[i]] = 1;
      }
    }

    for (s32 i = 0; i < TEST_ACKS_NUM_ITERATIONS / 2; ++i)
    {
      check(receiverAckedPacket[i] == 1);
    }

    reliable_endpoint_destroy(context.sender);
    reliable_endpoint_destroy(context.receiver);
  }

  static void test_acks_packet_loss()
  {
    f64 time = 100.0;
    f64 deltaTime = 0.1;

    test_context_t context;
    memset(&context, 0, sizeof(test_context_t));

    reliable_config_t senderConfig;
    reliable_config_t receiverConfig;

    reliable_default_config(&senderConfig);
    reliable_default_config(&receiverConfig);

    senderConfig.context = &context;
    senderConfig.index = 0;
    senderConfig.transmit_packet_function = &test_transmit_packet_function;
    senderConfig.process_packet_function = &test_process_packet_function;

    receiverConfig.context = &context;
    receiverConfig.index = 1;
    receiverConfig.transmit_packet_function = &test_transmit_packet_function;
    receiverConfig.process_packet_function = &test_process_packet_function;

    context.sender = reliable_endpoint_create(&senderConfig, time);
    context.receiver = reliable_endpoint_create(&receiverConfig, time);

    for (s32 i = 0; i < TEST_ACKS_NUM_ITERATIONS; ++i)
    {
      u08 dummyPacket[8];
      memset(dummyPacket, 0, sizeof(dummyPacket));

      context.drop = (i % 2);

      reliable_endpoint_send_packet(context.sender, dummyPacket, sizeof(dummyPacket));
      reliable_endpoint_send_packet(context.receiver, dummyPacket, sizeof(dummyPacket));

      reliable_endpoint_update(context.sender, time);
      reliable_endpoint_update(context.receiver, time);

      time += deltaTime;
    }

    u08 senderAckedPacket[TEST_ACKS_NUM_ITERATIONS];
    memset(senderAckedPacket, 0, sizeof(senderAckedPacket));

    s32 senderNumAcks;
    u16* senderAcks = reliable_endpoint_get_acks(context.sender, &senderNumAcks);

    for (s32 i = 0; i < senderNumAcks; ++i)
    {
      if (senderAcks[i] < TEST_ACKS_NUM_ITERATIONS)
      {
        senderAckedPacket[senderAcks[i]] = 1;
      }
    }

    for (s32 i = 0; i < TEST_ACKS_NUM_ITERATIONS / 2; ++i)
    {
      check(senderAckedPacket[i] == (i+1) % 2);
    }

    u08 receiverAckedPacket[TEST_ACKS_NUM_ITERATIONS];
    memset(receiverAckedPacket, 0, sizeof(receiverAckedPacket));

    s32 receiverNumAcks;
    u16* receiverAcks = reliable_endpoint_get_acks(context.sender, &receiverNumAcks);
    for (s32 i = 0; i < receiverNumAcks; ++i)
    {
      if (receiverAcks[i] < TEST_ACKS_NUM_ITERATIONS)
      {
        receiverAckedPacket[receiverAcks[i]] = 1;
      }
    }

    for (s32 i = 0; i < TEST_ACKS_NUM_ITERATIONS / 2; ++i)
    {
      check(receiverAckedPacket[i] == (i + 1) % 2);
    }

    reliable_endpoint_destroy(context.sender);
    reliable_endpoint_destroy(context.receiver);

  }

#define TEST_MAX_PACKET_BYTES (4*1024)

  static s32 generate_packet_data(u16 sequence, u08* packetData)
  {
    s32 packetBytes = (((s32)sequence * 1023) % (TEST_MAX_PACKET_BYTES - 2)) + 2;
    gg_assert(packetBytes >= 2);
    gg_assert(packetBytes <= TEST_MAX_PACKET_BYTES);
    packetData[0] = (u08)(sequence & 0xFF);
    packetData[1] = (u08)((sequence >> 8) & 0xFF);

    for (s32 i = 2; i < packetBytes; ++i)
    {
      packetData[i] = (u08)(((s32)i + sequence) % 256);
    }
    return packetBytes;
  }

  static void validate_packet_data(u08* packetData, s32 packetBytes)
  {
    gg_assert(packetBytes >= 2);
    gg_assert(packetBytes <= TEST_MAX_PACKET_BYTES);
    u16 sequence = 0;
    sequence |= (u16)packetData[0];
    sequence |= ((u16)packetData[1]) << 8;
    
    check(packetBytes == (((s32)sequence * 1023) % (TEST_MAX_PACKET_BYTES - 2)) + 2);
    for (s32 i = 2; i < packetBytes; ++i)
    {
      check(packetData[i] == (u08)(((s32)i + sequence) % 256));
    }
  }

  static s32 test_process_packet_function_validate(void* context, s32 index, u16 sequence, u08* packetData, s32 packetBytes)
  {
    gg_assert(packetData);
    gg_assert(packetBytes > 0);
    gg_assert(packetBytes <= TEST_MAX_PACKET_BYTES);

    (void)context;
    (void)index;
    (void)sequence;

    validate_packet_data(packetData, packetBytes);

    return 1;
  }

  void test_packets()
  {
    f64 time = 100.0;
    f64 deltaTime = 0.1;

    test_context_t context;
    memset(&context, 0, sizeof(test_context_t));

    reliable_config_t senderConfig;
    reliable_config_t receiverConfig;

    reliable_default_config(&senderConfig);
    reliable_default_config(&receiverConfig);

    senderConfig.fragmentAbove = 500;
    receiverConfig.fragmentAbove = 500;

#if defined(_MSC_VER)
    strcpy_s(senderConfig.name, sizeof(senderConfig.name), "sender");
#else
    strcpy(senderConfig.name, "sender");
#endif
    senderConfig.context = &context;
    senderConfig.index = 0;
    senderConfig.transmit_packet_function = &test_transmit_packet_function;
    senderConfig.process_packet_function = &test_process_packet_function;

#if defined(_MSC_VER)
    strcpy_s(receiverConfig.name, sizeof(receiverConfig.name), "sender");
#else
    strcpy(receiverConfig.name, "sender");
#endif
    receiverConfig.context = &context;
    receiverConfig.index = 1;
    receiverConfig.transmit_packet_function = &test_transmit_packet_function;
    receiverConfig.process_packet_function = &test_process_packet_function;

    context.sender = reliable_endpoint_create(&senderConfig, time);
    context.receiver = reliable_endpoint_create(&receiverConfig, time);

    for (s32 i = 0; i < 16; ++i)
    {
      {
        u08 packetData[TEST_MAX_PACKET_BYTES];
        u16 sequence = reliable_endpoint_next_packet_sequence(context.sender);
        s32 packetBytes = generate_packet_data(sequence, packetData);
        reliable_endpoint_send_packet(context.sender, packetData, packetBytes);
      }
      {
        u08 packetData[TEST_MAX_PACKET_BYTES];
        u16 sequence = reliable_endpoint_next_packet_sequence(context.sender);
        s32 packetBytes = generate_packet_data(sequence, packetData);
        reliable_endpoint_send_packet(context.sender, packetData, packetBytes);
      }

      reliable_endpoint_update(context.sender, time);
      reliable_endpoint_update(context.receiver, time);

      reliable_endpoint_clear_acks(context.sender);
      reliable_endpoint_clear_acks(context.receiver);

      time += deltaTime;
    }

    reliable_endpoint_destroy(context.sender);
    reliable_endpoint_destroy(context.receiver);
  }

#define RUN_TEST(test_function)           \
    do                                    \
    {                                     \
        printf( #test_function "\n" );    \
        test_function();                  \
    }                                     \
    while (0)


  void reliable_test()
  {
    //while(1)
    {
      RUN_TEST(test_endian);
      RUN_TEST(test_sequence_buffer);
      RUN_TEST(test_generate_ack_bits);
      RUN_TEST(test_packet_header);
      RUN_TEST(test_acks);
      RUN_TEST(test_acks_packet_loss);
      RUN_TEST(test_packets);
    }
  }

#endif //RELIABLE_ENABLE_TESTS
}// namespace GG
