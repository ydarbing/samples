#include "NetworkPrecompiled.h"
#include "Netcode\ReplayProtection.h"

//extern const u32 NETCODE_REPLAY_PROTECTION_BUFFER_SIZE{ 256 };

namespace GG
{

  void netcode_replay_protection_reset(netcode_replay_protection_t* replayProtection)
  {
    gg_assert(replayProtection);
  
    replayProtection->mostRecentSequence = 0;
    memset(replayProtection->receivedPacket, 0xFF, sizeof(replayProtection->receivedPacket));
  }
  
  
  bool netcode_replay_protection_already_received(netcode_replay_protection_t* replayProtection, u64 sequence)
  {
    gg_assert(replayProtection);
  
    if (sequence + NETCODE_REPLAY_PROTECTION_BUFFER_SIZE <= replayProtection->mostRecentSequence)
      return true;
  
    s32 index = (s32)(sequence % NETCODE_REPLAY_PROTECTION_BUFFER_SIZE);
  
    if (replayProtection->receivedPacket[index] == 0xFFFFFFFFFFFFFFFFLL)
      return false;
  
    if (replayProtection->receivedPacket[index] >= sequence)
      return true;
  
    return false;
  }

  void netcode_replay_protection_advance_sequence(netcode_replay_protection_t* replayProtection, u64 sequence)
  {
	  gg_assert(replayProtection);

	  if (sequence > replayProtection->mostRecentSequence)
		  replayProtection->mostRecentSequence = sequence;

	  s32 index = (s32)(sequence % NETCODE_REPLAY_PROTECTION_BUFFER_SIZE);

	  replayProtection->receivedPacket[index] = sequence;
  }

}// namespace GG
