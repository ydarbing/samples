#include "NetworkPrecompiled.h"
#include "Netcode/ConnectToken.h"


namespace GG
{

  /*

  Connect token stuff

  */

  /*

  void netcode_write_connect_token(netcode_connect_token_t* connectToken, u08* buffer, s32 bufferLength)
  {
    gg_assert(connectToken);
    gg_assert(buffer);
    gg_assert(bufferLength >= NETCODE_CONNECT_TOKEN_BYTES);

    u08* start = buffer;

    (void)start;
    (void)bufferLength;

    netcode_write_bytes(&buffer, connectToken->versionInfo, NETCODE_VERSION_INFO_BYTES);
    netcode_write_u64(&buffer, connectToken->protocolId);
    netcode_write_u64(&buffer, connectToken->createTimestamp);
    netcode_write_u64(&buffer, connectToken->expireTimestamp);
    netcode_write_u64(&buffer, connectToken->sequence);
    netcode_write_bytes(&buffer, connectToken->privateData, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);

    netcode_write_u32(&buffer, connectToken->timeoutSeconds);
    netcode_write_u32(&buffer, connectToken->numServerAddresses);


    netcode_write_address(connectToken->serverAddresses, connectToken->numServerAddresses, buffer);

    netcode_write_bytes(&buffer, connectToken->clientToServerKey, NETCODE_KEY_BYTES);
    netcode_write_bytes(&buffer, connectToken->serverToClientKey, NETCODE_KEY_BYTES);

    gg_assert(buffer - start <= NETCODE_CONNECT_TOKEN_BYTES);

    memset(buffer, 0, NETCODE_CONNECT_TOKEN_BYTES - (buffer - start));
  }


  s32 netcode_read_connect_token(u08* buffer, s32 bufferLength, netcode_connect_token_t* connectToken)
  {
    gg_assert(buffer);
    gg_assert(connectToken);

    if (bufferLength != NETCODE_CONNECT_TOKEN_BYTES)
    {
      gg_printf(GG_LOG_LEVEL_ERROR, "Error: read connect data has bad buffer length (%d)\n", bufferLength);
      return NETCODE_ERROR;
    }

    netcode_read_bytes(&buffer, connectToken->versionInfo, NETCODE_VERSION_INFO_BYTES);
    if (connectToken->versionInfo[0] != 'N' || // NETCODE_VERSION_INFO[0] || 
        connectToken->versionInfo[1] != 'E' ||
        connectToken->versionInfo[2] != 'T' ||
        connectToken->versionInfo[3] != 'C' ||
        connectToken->versionInfo[4] != 'O' ||
        connectToken->versionInfo[5] != 'D' ||
        connectToken->versionInfo[6] != 'E' ||
        connectToken->versionInfo[7] != ' ' ||
        connectToken->versionInfo[8] != '1' ||
        connectToken->versionInfo[9] != '.' ||
        connectToken->versionInfo[10] != '0' ||
        connectToken->versionInfo[11] != '1' ||
        connectToken->versionInfo[12] != '\0')
    {
      connectToken->versionInfo[12] = '\0';
      gg_printf(GG_LOG_LEVEL_ERROR, "Error: read connect data has bad version info. (Got %s, expected %s)\n", connectToken->versionInfo, NETCODE_VERSION_INFO);
      return NETCODE_ERROR;
    }

    connectToken->protocolId = netcode_read_u64(&buffer);
    connectToken->createTimestamp = netcode_read_u64(&buffer);
    connectToken->expireTimestamp = netcode_read_u64(&buffer);

    if (connectToken->createTimestamp > connectToken->expireTimestamp)
      return NETCODE_ERROR;

    connectToken->sequence = netcode_read_u64(&buffer);
    netcode_read_bytes(&buffer, connectToken->privateData, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);

    connectToken->timeoutSeconds = SCAST(s32, netcode_read_u32(&buffer));
    connectToken->numServerAddresses = netcode_read_u32(&buffer);

    if (connectToken->numServerAddresses <= 0 || connectToken->numServerAddresses > NETCODE_MAX_SERVERS_PER_CONNECT)
    {
      gg_printf(GG_LOG_LEVEL_ERROR, "Error: read connect data has bad number of server addresses (%d)\n", connectToken->numServerAddresses);
      return NETCODE_ERROR;
    }

    s32 ret = netcode_read_address(connectToken->serverAddresses, connectToken->numServerAddresses, buffer);

    if (ret != NETCODE_OK)
      return ret;

    netcode_read_bytes(&buffer, connectToken->clientToServerKey, NETCODE_KEY_BYTES);
    netcode_read_bytes(&buffer, connectToken->serverToClientKey, NETCODE_KEY_BYTES);

    return NETCODE_OK;
  }
  */

}// namespace GG