#include "NetworkPrecompiled.h"
#include "Netcode/Netcode.h"
#include "Netcode/ReplayProtection.h"
//#include "Netcode/PacketQueue.h"


#include <ws2tcpip.h>
#include <cinttypes>

#ifdef _MSC_VER
#define SODIUM_STATIC
#pragma warning(disable:4996)
#endif

#include <sodium.h>


namespace GG
{
	static bool initialized = false;
	void* netcode_default_allocate_function(void* context, u64 bytes)
	{
		(void)context;
		return malloc(bytes);
	}

	void netcode_default_free_function(void* context, void* pointer)
	{
		(void)context;
		free(pointer);
	}

	void netcode_socket_destroy(netcode_socket_t* socket)
	{
		gg_assert(socket);
		gg_assert(initialized);

		if (socket->handle != 0)
		{
#if GG_PLATFORM == GG_PLATFORM_MAC || GG_PLATFORM == GG_PLATFORM_UNIX
			close(socket->handle);
#elif GG_PLATFORM == GG_PLATFORM_WINDOWS
			closesocket(socket->handle);
#else
#error unsupported platform
#endif
			socket->handle = 0;
		}
	}

	s32 netcode_socket_create(netcode_socket_t* s, netcode_address_t* address, s32 send_buffer_size, s32 receive_buffer_size)
	{
		gg_assert(s);
		gg_assert(address);
		gg_assert(initialized);

		gg_assert(address->type != NETCODE_ADDRESS_NONE);


		s->address = *address;

		s->handle = socket((address->type == NETCODE_ADDRESS_IPV6) ? AF_INET6 : AF_INET, SOCK_DGRAM, IPPROTO_UDP);

#if GG_PLATFORM == GG_PLATFORM_WINDOWS
		if (s->handle == INVALID_SOCKET)
#else
		if (s->handle <= 0)
#endif
		{
			gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to create socket\n");
			return NETCODE_SOCKET_ERROR_CREATE_FAILED;
		}

		// force IPv6 only if necessary
		if (address->type == NETCODE_ADDRESS_IPV6)
		{
			s32 yes = 1;
			if (setsockopt(s->handle, IPPROTO_IPV6, IPV6_V6ONLY, (c08*)& yes, sizeof(yes)) != 0)
			{
				gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to set socket ipv6 only\n");
				netcode_socket_destroy(s);
				return NETCODE_SOCKET_ERROR_SOCKOPT_IPV6_ONLY_FAILED;
			}
		}

		// increate socket send and receive buffer sizes

		if (setsockopt(s->handle, SOL_SOCKET, SO_SNDBUF, (c08*)& send_buffer_size, sizeof(s32)) != 0)
		{
			gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to set socket send buffer size\n");
			netcode_socket_destroy(s);
			return NETCODE_SOCKET_ERROR_SOCKOPT_SNDBUF_FAILED;
		}

		if (setsockopt(s->handle, SOL_SOCKET, SO_RCVBUF, (c08*)& receive_buffer_size, sizeof(s32)) != 0)
		{
			gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to set socket receive buffer size\n");
			netcode_socket_destroy(s);
			return NETCODE_SOCKET_ERROR_SOCKOPT_RCVBUF_FAILED;
		}

		// bind to port

		if (address->type == NETCODE_ADDRESS_IPV6)
		{
			sockaddr_in6 socketAddress;
			memset(&socketAddress, 0, sizeof(sockaddr_in6));
			socketAddress.sin6_family = AF_INET6;
			for (s32 i = 0; i < 8; ++i)
			{
				((u16*)& socketAddress.sin6_addr)[i] = htons(address->data.ipv6[i]);
			}

			socketAddress.sin6_port = htons(address->port);

			if (bind(s->handle, (sockaddr*)& socketAddress, sizeof(socketAddress)) < 0)
			{
				gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to bind socket (ipv6)\n");
				netcode_socket_destroy(s);
				return NETCODE_SOCKET_ERROR_BIND_IPV6_FAILED;
			}
		}
		else
		{
			sockaddr_in socketAddress;
			memset(&socketAddress, 0, sizeof(sockaddr_in));
			socketAddress.sin_family = AF_INET;

			socketAddress.sin_addr.s_addr = (((u32)address->data.ipv4[0])) |
				(((u32)address->data.ipv4[1]) << 8) |
				(((u32)address->data.ipv4[2]) << 16) |
				(((u32)address->data.ipv4[3]) << 24);

			socketAddress.sin_port = htons(address->port);

			if (bind(s->handle, (sockaddr*)& socketAddress, sizeof(socketAddress)) < 0)
			{
				gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to bind socket (ipv4) \n");
				netcode_socket_destroy(s);
				return NETCODE_SOCKET_ERROR_BIND_IPV4_FAILED;
			}
		}

		// if bound to port 0, find the actual port we got
		if (address->port == 0)
		{
			if (address->type == NETCODE_ADDRESS_IPV6)
			{
				sockaddr_in6 sin;
				socklen_t len = sizeof(sin);
				if (getsockname(s->handle, (sockaddr*)& sin, &len) == -1)
				{
					gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to get socket port (ipv6)\n");
					netcode_socket_destroy(s);
					return NETCODE_SOCKET_ERROR_GET_SOCKNAME_IPV6_FAILED;
				}
				s->address.port = ntohs(sin.sin6_port);
			}
			else //ipv4
			{
				sockaddr_in sin;
				socklen_t len = sizeof(sin);
				if (getsockname(s->handle, (sockaddr*)& sin, &len) == -1)
				{
					gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to get socket port (ipv4)\n");
					netcode_socket_destroy(s);
					return NETCODE_SOCKET_ERROR_GET_SOCKNAME_IPV4_FAILED;
				}
				s->address.port = ntohs(sin.sin_port);
			}
		}

		// socket is ready to send/receive packets
		// set socket to non-blocking
#if GG_PLATFORM == GG_PLATFORM_WINDOWS
		DWORD nonBlocking = 1;
		if (ioctlsocket(s->handle, FIONBIO, &nonBlocking) != 0)
		{
			gg_printf(GG_LOG_LEVEL_ERROR, "Error: Failed to set non-blocking\n");
			netcode_socket_destroy(s);
			return NETCODE_SOCKET_ERROR_SET_NON_BLOCKING_FAILED;
		}
#elif GG_PLATFORM == GG_PLATFORM_MAC || GG_PLATFORM == GG_PLATFORM_UNIX
		s32 nonBlocking = 1;
		if (fcntl(handle, F_SETFL, O_NONBLOCK, nonBlocking) == -1)
		{
			gg_printf(GG_LOG_LEVEL_ERROR, "Error: Failed to set non-blocking\n");
			netcode_socket_destroy(s);
			return NETCODE_SOCKET_ERROR_SET_NON_BLOCKING_FAILED;
#else
#error unsupported platform
#endif

		return NETCODE_SOCKET_ERROR_NONE;
	}




	void netcode_socket_send_packet(netcode_socket_t * socket, netcode_address_t * to, void* packet_data, s32 packet_bytes)
	{
		gg_assert(socket);
		gg_assert(socket->handle != 0);
		gg_assert(to);
		gg_assert(to->type == NETCODE_ADDRESS_IPV6 || to->type == NETCODE_ADDRESS_IPV4);
		gg_assert(packet_data);
		gg_assert(packet_bytes > 0);

		if (to->type == NETCODE_ADDRESS_IPV6)
		{
			sockaddr_in6 socketAddress;
			memset(&socketAddress, 0, sizeof(socketAddress));
			socketAddress.sin6_family = AF_INET6;
			for (s32 i = 0; i < 8; ++i)
			{
				((u16*)& socketAddress.sin6_addr)[i] = htons(to->data.ipv6[i]);
			}
			socketAddress.sin6_port = htons(to->port);

			s32 result = sendto(socket->handle, SCAST(c08*, packet_data), packet_bytes, 0, (sockaddr*)& socketAddress, sizeof(sockaddr_in6));
			(void)result;
		}
		else if (to->type == NETCODE_ADDRESS_IPV4)
		{
			sockaddr_in socketAddress;
			memset(&socketAddress, 0, sizeof(socketAddress));
			socketAddress.sin_family = AF_INET;
			socketAddress.sin_addr.s_addr = (((u32)to->data.ipv4[0])) |
				(((u32)to->data.ipv4[1]) << 8) |
				(((u32)to->data.ipv4[2]) << 16) |
				(((u32)to->data.ipv4[3]) << 24);

			socketAddress.sin_port = htons(to->port);
			s32 result = sendto(socket->handle, SCAST(const c08*, packet_data), packet_bytes, 0, (sockaddr*)& socketAddress, sizeof(sockaddr_in));
			(void)result;
		}
	}


	s32 netcode_socket_receive_packet(netcode_socket_t * socket, netcode_address_t * from, void* packet_data, s32 max_packet_size)
	{
		gg_assert(socket);
		gg_assert(socket->handle != 0);
		gg_assert(from);
		gg_assert(packet_data);
		gg_assert(max_packet_size > 0);

#if GG_PLATFORM == GG_PLATFORM_WINDOWS
		typedef s32 socklen_t;
#endif
		sockaddr_storage sockaddrFrom;
		socklen_t fromLength = sizeof(sockaddrFrom);

		s32 receivedBytes = recvfrom(socket->handle, SCAST(c08*, packet_data), max_packet_size, 0, (sockaddr*)& sockaddrFrom, &fromLength);

#if GG_PLATFORM == GG_PLATFORM_WINDOWS
		if (receivedBytes == SOCKET_ERROR)
		{
			s32 error = WSAGetLastError();
			if (error == WSAEWOULDBLOCK || error == WSAECONNRESET)
				return 0;

			gg_printf(GG_LOG_LEVEL_ERROR, "Error: recvfrom failed with error %d\n", error);

			return 0;
		}
#else
		if (receivedBytes <= 0)
		{
			if (errno == EAGAIN)
				return 0;
			gg_printf(GG_LOG_LEVEL_ERROR, "Error: recvfrom failed with error %d\n", errno);
			return 0;
		}
#endif

		if (sockaddrFrom.ss_family == AF_INET6)
		{
			sockaddr_in6* addr_ipv6 = (sockaddr_in6*)& sockaddrFrom;
			from->type = NETCODE_ADDRESS_IPV6;
			for (s32 i = 0; i < 8; ++i)
			{
				from->data.ipv6[i] = ntohs(((u16*)& addr_ipv6->sin6_addr)[i]);
			}
			from->port = ntohs(addr_ipv6->sin6_port);
		}
		else if (sockaddrFrom.ss_family == AF_INET)
		{
			sockaddr_in* addr_ipv4 = (sockaddr_in*)& sockaddrFrom;
			from->type = NETCODE_ADDRESS_IPV4;
			from->data.ipv4[0] = (u08)((addr_ipv4->sin_addr.s_addr & 0x000000FF));
			from->data.ipv4[1] = (u08)((addr_ipv4->sin_addr.s_addr & 0x0000FF00) >> 8);
			from->data.ipv4[2] = (u08)((addr_ipv4->sin_addr.s_addr & 0x00FF0000) >> 16);
			from->data.ipv4[3] = (u08)((addr_ipv4->sin_addr.s_addr & 0xFF000000) >> 24);
			from->port = ntohs(addr_ipv4->sin_port);
		}
		else
		{
			gg_assert(0);
			return 0;
		}

		gg_assert(receivedBytes >= 0);

		//s32 bytesRead = result;
		return receivedBytes;
	}



	void netcode_write_u08(u08 * *p, u08 value)
	{
		**p = value;
		++(*p);
	}
	void netcode_write_u16(u08 * *p, u16 value)
	{
		(*p)[0] = value & 0xFF;
		(*p)[1] = value >> 8;
		*p += 2;
	}
	void netcode_write_u32(u08 * *p, u32 value)
	{
		(*p)[0] = value & 0xFF;
		(*p)[1] = (value >> 8) & 0xFF;
		(*p)[2] = (value >> 16) & 0xFF;
		(*p)[3] = value >> 24;
		*p += 4;
	}
	void netcode_write_u64(u08 * *p, u64 value)
	{
		(*p)[0] = value & 0xFF;
		(*p)[1] = (value >> 8) & 0xFF;
		(*p)[2] = (value >> 16) & 0xFF;
		(*p)[3] = (value >> 24) & 0xFF;
		(*p)[4] = (value >> 32) & 0xFF;
		(*p)[5] = (value >> 40) & 0xFF;
		(*p)[6] = (value >> 48) & 0xFF;
		(*p)[7] = value >> 56;
		*p += 8;
	}

	void netcode_write_bytes(u08 * *p, u08 * byte_array, s32 num_bytes)
	{
		for (s32 i = 0; i < num_bytes; ++i)
		{
			netcode_write_u08(p, byte_array[i]);
		}
	}

	u08 netcode_read_u08(u08 * *p)
	{
		u08 value = **p;
		++(*p);
		return value;
	}


	u16 netcode_read_u16(u08 * *p)
	{
		u16 value;
		value = (*p)[0];
		value |= (((u16)((*p)[1])) << 8);
		*p += 2;
		return value;
	}

	u32 netcode_read_u32(u08 * *p)
	{
		u32 value;
		value = (*p)[0];
		value |= (((u32)((*p)[1])) << 8);
		value |= (((u32)((*p)[2])) << 16);
		value |= (((u32)((*p)[3])) << 24);
		*p += 4;
		return value;
	}

	u64 netcode_read_u64(u08 * *p)
	{
		u64 value;
		value = (*p)[0];
		value |= (((u64)((*p)[1])) << 8);
		value |= (((u64)((*p)[2])) << 16);
		value |= (((u64)((*p)[3])) << 24);
		value |= (((u64)((*p)[4])) << 32);
		value |= (((u64)((*p)[5])) << 40);
		value |= (((u64)((*p)[6])) << 48);
		value |= (((u64)((*p)[7])) << 56);
		*p += 8;
		return value;
	}

	void netcode_read_bytes(u08 * *p, u08 * byte_array, s32 num_bytes)
	{
		for (s32 i = 0; i < num_bytes; ++i)
		{
			byte_array[i] = netcode_read_u08(p);
		}
	}

#if SODIUM_LIBRARY_VERSION_MAJOR > 7 || ( SODIUM_LIBRARY_VERSION_MAJOR && SODIUM_LIBRARY_VERSION_MINOR >= 3 )
#define SODIUM_SUPPORTS_OVERLAPPING_BUFFERS 1
#endif


	void netcode_generate_key(u08 * key)
	{
		gg_assert(key);
		randombytes_buf(key, NETCODE_KEY_BYTES);
	}

	void netcode_generate_nonce(u08 * nonce)
	{
		gg_assert(nonce);
		randombytes_buf(nonce, NETCODE_CONNECT_TOKEN_NONCE_BYTES);
	}

	void netcode_random_bytes(u08 * data, s32 bytes)
	{
		gg_assert(data);
		gg_assert(bytes > 0);

		randombytes_buf(data, bytes);
	}

	s32 netcode_encrypt_aead_bignonce(u08 * message, u64 messageLength, u08 * additional, u64 additionalLength, const u08 * nonce, const u08 * key)
	{
		unsigned long long encryptedLength;

		s32 result = crypto_aead_xchacha20poly1305_ietf_encrypt(message, &encryptedLength,
			message, (unsigned long long)messageLength,
			additional, (unsigned long long)additionalLength,
			nullptr, nonce, key);

		if (result != 0)
			return NETCODE_ERROR;

		gg_assert(encryptedLength == messageLength + NETCODE_MAC_BYTES);

		return NETCODE_OK;
	}

	s32 netcode_decrypt_aead_bignonce(u08 * message, u64 messageLength, u08 * additional, u64 additionalLength, u08 * nonce, u08 * key)
	{
		unsigned long long decryptedLength;

		int result = crypto_aead_xchacha20poly1305_ietf_decrypt(message, &decryptedLength,
			nullptr,
			message, (unsigned long long) messageLength,
			additional, (unsigned long long) additionalLength,
			nonce, key);

		if (result != 0)
			return NETCODE_ERROR;

		gg_assert(decryptedLength == messageLength - NETCODE_MAC_BYTES);

		return NETCODE_OK;
	}

	s32 netcode_encrypt_aead(u08 * message, u64 messageLength, u08 * additional, u64 additionalLength, u08 * nonce, const u08 * key)
	{
		unsigned long long encryptedLength;

		s32 result = crypto_aead_chacha20poly1305_ietf_encrypt(message, &encryptedLength,
			message, (unsigned long long) messageLength,
			additional, (unsigned long long) additionalLength,
			nullptr, nonce, key);

		if (result != 0)
			return NETCODE_ERROR;

		gg_assert(encryptedLength == messageLength + NETCODE_MAC_BYTES);

		return NETCODE_OK;
	}


	s32 netcode_decrypt_aead(u08 * message, u64 messageLength,
		u08 * additional, u64 additionalLength,
		u08 * nonce,
		u08 * key)
	{
		unsigned long long decryptedLength;

		s32 result = crypto_aead_chacha20poly1305_ietf_decrypt(message, &decryptedLength,
			nullptr,
			message, (unsigned long long) messageLength,
			additional, (unsigned long long) additionalLength,
			nonce, key);

		if (result != 0)
			return NETCODE_ERROR;

		gg_assert(decryptedLength == messageLength - NETCODE_MAC_BYTES);

		return NETCODE_OK;
	}



	typedef std::array<netcode_address_t, NETCODE_MAX_SERVERS_PER_CONNECT> ServerAddressArray;

	struct netcode_connect_token_private_t
	{
		u64 clientId;
		s32 timeoutSeconds;
		s32 numServerAddresses;

		//netcode_address_t server_addresses[NETCODE_MAX_SERVERS_PER_CONNECT];
		ServerAddressArray serverAddresses;

		u08 clientToServerKey[NETCODE_KEY_BYTES];
		u08 serverToClientKey[NETCODE_KEY_BYTES];
		u08 userData[NETCODE_USER_DATA_BYTES];
	};

	void netcode_generate_connect_token_private(netcode_connect_token_private_t * connectToken, u64 clientId, s32 timeoutSeconds, s32 numServerAddresses,
		netcode_address_t * serverAddresses, u08 * userData)
	{
		gg_assert(connectToken);
		gg_assert(numServerAddresses > 0);
		gg_assert(numServerAddresses <= NETCODE_MAX_SERVERS_PER_CONNECT);
		gg_assert(serverAddresses);
		gg_assert(userData);

		connectToken->clientId = clientId;
		connectToken->timeoutSeconds = timeoutSeconds;
		connectToken->numServerAddresses = numServerAddresses;

		for (s32 i = 0; i < numServerAddresses; ++i)
		{
			memcpy(&connectToken->serverAddresses[i], &serverAddresses[i], sizeof(netcode_address_t));
		}

		netcode_generate_key(connectToken->clientToServerKey);
		netcode_generate_key(connectToken->serverToClientKey);

		if (userData != nullptr)
		{
			memcpy(connectToken->userData, userData, NETCODE_USER_DATA_BYTES);
		}
		else
		{
			memset(connectToken->userData, 0, NETCODE_USER_DATA_BYTES);
		}
	}

	TODO("test this function")
		void netcode_write_address(ServerAddressArray & serverAddresses, const s32 numServerAddresses, u08 * &buffer)
	{
		for (s32 i = 0; i < numServerAddresses; ++i)
		{
			if (serverAddresses[i].type == NETCODE_ADDRESS_IPV4)
			{
				netcode_write_u08(&buffer, NETCODE_ADDRESS_IPV4);
				for (s32 j = 0; j < 4; ++j)
				{
					netcode_write_u08(&buffer, serverAddresses[i].data.ipv4[j]);
				}
				netcode_write_u16(&buffer, serverAddresses[i].port);
			}
			else if (serverAddresses[i].type == NETCODE_ADDRESS_IPV6)
			{
				netcode_write_u08(&buffer, NETCODE_ADDRESS_IPV6);
				for (s32 j = 0; j < 8; ++j)
				{
					netcode_write_u16(&buffer, serverAddresses[i].data.ipv6[j]);
				}
				netcode_write_u16(&buffer, serverAddresses[i].port);

			}
#if NETCODE_NETWORK_NEXT
			else if (serverAddresses[i].type == NETCODE_ADDRESS_NEXT)
			{
				netcode_write_u08(&buffer, NETCODE_ADDRESS_NEXT);
				netcode_write_u64(&buffer, serverAddresses[i].data.flow_id);
			}
#endif
			else
			{
				gg_assert(0);
			}
		}
	}

	TODO("test this function")
		s32 netcode_read_address(ServerAddressArray & serverAddresses, const s32 numServerAddresses, u08 * &buffer)
	{
		for (s32 i = 0; i < numServerAddresses; ++i)
		{
			serverAddresses[i].type = netcode_read_u08(&buffer);

			if (serverAddresses[i].type == NETCODE_ADDRESS_IPV4)
			{
				for (s32 j = 0; j < 4; ++j)
				{
					serverAddresses[i].data.ipv4[j] = netcode_read_u08(&buffer);
				}
				serverAddresses[i].port = netcode_read_u16(&buffer);

			}
			else if (serverAddresses[i].type == NETCODE_ADDRESS_IPV6)
			{
				for (s32 j = 0; j < 8; ++j)
				{
					serverAddresses[i].data.ipv6[j] = netcode_read_u16(&buffer);
				}
				serverAddresses[i].port = netcode_read_u16(&buffer);
			}
#if NETCODE_NETWORK_NEXT
			else if (serverAddresses[i].type == NETCODE_ADDRESS_NEXT)
			{
				serverAddresses[i].data.flow_id = netcode_read_u64(&buffer);
			}
#endif
			else
			{
				gg_printf(GG_LOG_LEVEL_ERROR, "Error: read connect data has bad address type (%d)\n", serverAddresses[i].type);
				return NETCODE_ERROR;
			}
		}

		return NETCODE_OK;
	}

	void netcode_write_connect_token_private(netcode_connect_token_private_t * connectToken, u08 * buffer, s32 bufferLength)
	{
		(void)bufferLength;

		gg_assert(connectToken);
		gg_assert(connectToken->numServerAddresses > 0);
		gg_assert(connectToken->numServerAddresses <= NETCODE_MAX_SERVERS_PER_CONNECT);
		gg_assert(buffer);
		gg_assert(bufferLength >= NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);

		u08* start = buffer;

		netcode_write_u64(&buffer, connectToken->clientId);
		netcode_write_u32(&buffer, connectToken->timeoutSeconds);
		netcode_write_u32(&buffer, connectToken->numServerAddresses);

		netcode_write_address(connectToken->serverAddresses, connectToken->numServerAddresses, buffer);

		netcode_write_bytes(&buffer, connectToken->clientToServerKey, NETCODE_KEY_BYTES);
		netcode_write_bytes(&buffer, connectToken->serverToClientKey, NETCODE_KEY_BYTES);
		netcode_write_bytes(&buffer, connectToken->userData, NETCODE_USER_DATA_BYTES);

		gg_assert(buffer - start <= NETCODE_CONNECT_TOKEN_PRIVATE_BYTES - NETCODE_MAC_BYTES);

		memset(buffer, 0, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES - (buffer - start));
	}



	s32 netcode_encrypt_connect_token_private(u08 * buffer, s32 bufferLength, u08 * versionInfo, u64 protocolId, u64 expireTimestamp, const u08 * nonce, const u08 * key)
	{
		gg_assert(buffer);
		gg_assert(bufferLength == NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);
		gg_assert(key);

		(void)bufferLength;

		u08 additionalData[NETCODE_VERSION_INFO_BYTES + 8 + 8];
		{
			u08* p = additionalData;
			netcode_write_bytes(&p, versionInfo, NETCODE_VERSION_INFO_BYTES);
			netcode_write_u64(&p, protocolId);
			netcode_write_u64(&p, expireTimestamp);
		}

		return netcode_encrypt_aead_bignonce(buffer, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES - NETCODE_MAC_BYTES, additionalData, sizeof(additionalData), nonce, key);
	}


	s32 netcode_decrypt_connect_token_private(u08 * buffer, s32 bufferLength, u08 * versionInfo, u64 protocolId, u64 expireTimestamp, u08 * nonce, u08 * key)
	{
		gg_assert(buffer);
		gg_assert(bufferLength == NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);
		gg_assert(key);
		(void)bufferLength;

		u08 additionalData[NETCODE_VERSION_INFO_BYTES + 8 + 8];
		{
			u08* p = additionalData;
			netcode_write_bytes(&p, versionInfo, NETCODE_VERSION_INFO_BYTES);
			netcode_write_u64(&p, protocolId);
			netcode_write_u64(&p, expireTimestamp);
		}

		return netcode_decrypt_aead_bignonce(buffer, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES, additionalData, sizeof(additionalData), nonce, key);
	}


	s32 netcode_read_connect_token_private(u08 * buffer, s32 bufferLength, netcode_connect_token_private_t * connectToken)
	{
		gg_assert(buffer);
		gg_assert(connectToken);

		if (bufferLength < NETCODE_CONNECT_TOKEN_PRIVATE_BYTES)
		{
			return NETCODE_ERROR;
		}

		connectToken->clientId = netcode_read_u64(&buffer);
		connectToken->timeoutSeconds = (s32)netcode_read_u32(&buffer);
		connectToken->numServerAddresses = netcode_read_u32(&buffer);

		if (connectToken->numServerAddresses <= 0)
			return NETCODE_ERROR;

		if (connectToken->numServerAddresses > NETCODE_MAX_SERVERS_PER_CONNECT)
			return NETCODE_ERROR;

		s32 ret = netcode_read_address(connectToken->serverAddresses, connectToken->numServerAddresses, buffer);

		if (ret != NETCODE_OK)
			return ret;

		netcode_read_bytes(&buffer, connectToken->clientToServerKey, NETCODE_KEY_BYTES);
		netcode_read_bytes(&buffer, connectToken->serverToClientKey, NETCODE_KEY_BYTES);
		netcode_read_bytes(&buffer, connectToken->userData, NETCODE_USER_DATA_BYTES);

		return NETCODE_OK;
	}

	struct netcode_challenge_token_t
	{
		u64 client_id;
		u08 user_data[NETCODE_USER_DATA_BYTES];
	};

	void netcode_write_challenge_token(netcode_challenge_token_t * challengeToken, u08 * buffer, s32 bufferLength)
	{
		(void)bufferLength;

		gg_assert(challengeToken);
		gg_assert(buffer);
		gg_assert(bufferLength >= NETCODE_CHALLENGE_TOKEN_BYTES);


		memset(buffer, 0, NETCODE_CHALLENGE_TOKEN_BYTES);

		u08* start = buffer;
		(void)start;

		netcode_write_u64(&buffer, challengeToken->client_id);
		netcode_write_bytes(&buffer, challengeToken->user_data, NETCODE_USER_DATA_BYTES);

		gg_assert(buffer - start <= NETCODE_CHALLENGE_TOKEN_BYTES - NETCODE_MAC_BYTES);

	}


	s32 netcode_encrypt_challenge_token(u08 * buffer, s32 bufferLength, u64 sequence, u08 * key)
	{
		gg_assert(buffer);
		gg_assert(bufferLength >= NETCODE_CHALLENGE_TOKEN_BYTES);
		gg_assert(key);

		(void)bufferLength;

		u08 nonce[12];
		{
			u08* p = nonce;
			netcode_write_u32(&p, 0);
			netcode_write_u64(&p, sequence);
		}

		return netcode_encrypt_aead(buffer, NETCODE_CHALLENGE_TOKEN_BYTES - NETCODE_MAC_BYTES, nullptr, 0, nonce, key);
	}

	s32 netcode_decrypt_challenge_token(u08 * buffer, s32 bufferLength, u64 sequence, u08 * key)
	{
		gg_assert(buffer);
		gg_assert(bufferLength >= NETCODE_CHALLENGE_TOKEN_BYTES);
		gg_assert(key);

		(void)bufferLength;

		u08 nonce[12];
		{
			u08* p = nonce;
			netcode_write_u32(&p, 0);
			netcode_write_u64(&p, sequence);
		}

		return netcode_decrypt_aead(buffer, NETCODE_CHALLENGE_TOKEN_BYTES, nullptr, 0, nonce, key);
	}

	s32 netcode_read_challenge_token(u08 * buffer, s32 bufferLength, netcode_challenge_token_t * challengeToken)
	{
		gg_assert(buffer);
		gg_assert(challengeToken);

		if (bufferLength < NETCODE_CHALLENGE_TOKEN_BYTES)
			return NETCODE_ERROR;

		u08* start = buffer;

		(void)start;

		challengeToken->client_id = netcode_read_u64(&buffer);
		netcode_read_bytes(&buffer, challengeToken->user_data, NETCODE_USER_DATA_BYTES);

		gg_assert(buffer - start == 8 + NETCODE_USER_DATA_BYTES);

		return NETCODE_OK;

	}

	/*

	Connect token stuff

	*/

	void netcode_write_connect_token(netcode_connect_token_t * connectToken, u08 * buffer, s32 bufferLength)
	{
		gg_assert(connectToken);
		gg_assert(buffer);
		gg_assert(bufferLength >= NETCODE_CONNECT_TOKEN_BYTES);

		u08* start = buffer;

		(void)start;
		(void)bufferLength;

		netcode_write_bytes(&buffer, connectToken->versionInfo, NETCODE_VERSION_INFO_BYTES);
		netcode_write_u64(&buffer, connectToken->protocolId);
		netcode_write_u64(&buffer, connectToken->createTimestamp);
		netcode_write_u64(&buffer, connectToken->expireTimestamp);
		netcode_write_bytes(&buffer, connectToken->nonce, NETCODE_CONNECT_TOKEN_NONCE_BYTES);
		netcode_write_bytes(&buffer, connectToken->privateData, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);

		netcode_write_u32(&buffer, connectToken->timeoutSeconds);
		netcode_write_u32(&buffer, connectToken->numServerAddresses);


		netcode_write_address(connectToken->serverAddresses, connectToken->numServerAddresses, buffer);

		netcode_write_bytes(&buffer, connectToken->clientToServerKey, NETCODE_KEY_BYTES);
		netcode_write_bytes(&buffer, connectToken->serverToClientKey, NETCODE_KEY_BYTES);

		gg_assert(buffer - start <= NETCODE_CONNECT_TOKEN_BYTES);

		memset(buffer, 0, NETCODE_CONNECT_TOKEN_BYTES - (buffer - start));
	}


	s32 netcode_read_connect_token(u08 * buffer, s32 bufferLength, netcode_connect_token_t * connectToken)
	{
		gg_assert(buffer);
		gg_assert(connectToken);

		if (bufferLength != NETCODE_CONNECT_TOKEN_BYTES)
		{
			gg_printf(GG_LOG_LEVEL_ERROR, "Error: read connect data has bad buffer length (%d)\n", bufferLength);
			return NETCODE_ERROR;
		}

		netcode_read_bytes(&buffer, connectToken->versionInfo, NETCODE_VERSION_INFO_BYTES);
		if (connectToken->versionInfo[0] != 'N' || // NETCODE_VERSION_INFO[0] || 
			connectToken->versionInfo[1] != 'E' ||
			connectToken->versionInfo[2] != 'T' ||
			connectToken->versionInfo[3] != 'C' ||
			connectToken->versionInfo[4] != 'O' ||
			connectToken->versionInfo[5] != 'D' ||
			connectToken->versionInfo[6] != 'E' ||
			connectToken->versionInfo[7] != ' ' ||
			connectToken->versionInfo[8] != '1' ||
			connectToken->versionInfo[9] != '.' ||
			connectToken->versionInfo[10] != '0' ||
			connectToken->versionInfo[11] != '2' ||
			connectToken->versionInfo[12] != '\0')
		{
			connectToken->versionInfo[12] = '\0';
			gg_printf(GG_LOG_LEVEL_ERROR, "Error: read connect data has bad version info. (Got %s, expected %s)\n", connectToken->versionInfo, NETCODE_VERSION_INFO);
			return NETCODE_ERROR;
		}

		connectToken->protocolId = netcode_read_u64(&buffer);
		connectToken->createTimestamp = netcode_read_u64(&buffer);
		connectToken->expireTimestamp = netcode_read_u64(&buffer);

		if (connectToken->createTimestamp > connectToken->expireTimestamp)
			return NETCODE_ERROR;

		netcode_read_bytes(&buffer, connectToken->nonce, NETCODE_CONNECT_TOKEN_NONCE_BYTES);

		netcode_read_bytes(&buffer, connectToken->privateData, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);

		connectToken->timeoutSeconds = SCAST(s32, netcode_read_u32(&buffer));
		connectToken->numServerAddresses = netcode_read_u32(&buffer);

		if (connectToken->numServerAddresses <= 0 || connectToken->numServerAddresses > NETCODE_MAX_SERVERS_PER_CONNECT)
		{
			gg_printf(GG_LOG_LEVEL_ERROR, "Error: read connect data has bad number of server addresses (%d)\n", connectToken->numServerAddresses);
			return NETCODE_ERROR;
		}

		s32 ret = netcode_read_address(connectToken->serverAddresses, connectToken->numServerAddresses, buffer);

		if (ret != NETCODE_OK)
			return ret;

		netcode_read_bytes(&buffer, connectToken->clientToServerKey, NETCODE_KEY_BYTES);
		netcode_read_bytes(&buffer, connectToken->serverToClientKey, NETCODE_KEY_BYTES);

		return NETCODE_OK;
	}



	/*
		  Packet stuff

	*/


#define NETCODE_CONNECTION_REQUEST_PACKET           0
#define NETCODE_CONNECTION_DENIED_PACKET            1
#define NETCODE_CONNECTION_CHALLENGE_PACKET         2
#define NETCODE_CONNECTION_RESPONSE_PACKET          3
#define NETCODE_CONNECTION_KEEP_ALIVE_PACKET        4
#define NETCODE_CONNECTION_PAYLOAD_PACKET           5
#define NETCODE_CONNECTION_DISCONNECT_PACKET        6
#define NETCODE_CONNECTION_NUM_PACKETS              7


	struct netcode_connection_request_packet_t
	{
		u08 packetType;
		u08 versionInfo[NETCODE_VERSION_INFO_BYTES];
		u64 protocolId;
		u64 connectTokenExpireTimestamp;
		u08 connectTokenNonce[NETCODE_CONNECT_TOKEN_NONCE_BYTES];
		u08 connectTokenData[NETCODE_CONNECT_TOKEN_PRIVATE_BYTES];
	};

	struct netcode_connection_denied_packet_t
	{
		u08 packetType;
	};

	struct netcode_connection_challenge_packet_t
	{
		u08 packetType;
		u64 challengeTokenSequence;
		u08 challengeTokenData[NETCODE_CHALLENGE_TOKEN_BYTES];
	};

	struct netcode_connection_response_packet_t
	{
		u08 packetType;
		u64 challengeTokenSequence;
		u08 challengeTokenData[NETCODE_CHALLENGE_TOKEN_BYTES];
	};

	struct netcode_connection_keep_alive_packet_t
	{
		u08 packetType;
		s32 clientIndex;
		s32 maxClients;
	};

	struct netcode_connection_payload_packet_t
	{
		u08 packetType;
		u32 payloadBytes;
		u08 payloadData[1];
	};

	struct netcode_connection_disconnect_packet_t
	{
		u08 packetType;
	};


	netcode_connection_payload_packet_t* netcode_create_payload_packet(s32 payloadBytes, void* allocatorContext, void* (*allocateFunction)(void*, uint64_t))
	{
		gg_assert(payloadBytes >= 0);
		gg_assert(payloadBytes <= NETCODE_MAX_PAYLOAD_BYTES);

		if (allocateFunction == nullptr)
		{
			allocateFunction = netcode_default_allocate_function;
		}

		netcode_connection_payload_packet_t* packet = SCAST(netcode_connection_payload_packet_t*, allocateFunction(allocatorContext, sizeof(netcode_connection_payload_packet_t) + payloadBytes));

		if (!packet)
			return nullptr;

		packet->packetType = NETCODE_CONNECTION_PAYLOAD_PACKET;
		packet->payloadBytes = payloadBytes;

		return packet;
	}


	s32 netcode_sequence_number_bytes_required(u64 sequence)
	{
		s32 i = 0;
		u64 mask = 0xFF00000000000000UL;
		for (; i < 7; ++i)
		{
			if (sequence & mask)
				break;
			mask >>= 8;
		}
		return 8 - i;
	}

	s32 netcode_write_packet(void* packet, u08 * buffer, s32 bufferLength, u64 sequence, u08 * writePacketKey, u64 protocolId)
	{
		gg_assert(packet);
		gg_assert(buffer);
		gg_assert(writePacketKey);
		(void)bufferLength;

		u08 packetType = ((u08*)packet)[0];

		if (packetType == NETCODE_CONNECTION_REQUEST_PACKET)
		{
			// unencrypted packet
			// connection request packet: first byte is zero
			gg_assert(bufferLength >= 1 + 13 + 8 + 8 + NETCODE_CONNECT_TOKEN_NONCE_BYTES + NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);

			netcode_connection_request_packet_t* p = (netcode_connection_request_packet_t*)packet;

			u08* start = buffer;

			netcode_write_u08(&buffer, NETCODE_CONNECTION_REQUEST_PACKET);
			netcode_write_bytes(&buffer, p->versionInfo, NETCODE_VERSION_INFO_BYTES);
			netcode_write_u64(&buffer, p->protocolId);
			netcode_write_u64(&buffer, p->connectTokenExpireTimestamp);
			netcode_write_bytes(&buffer, p->connectTokenNonce, NETCODE_CONNECT_TOKEN_NONCE_BYTES);
			netcode_write_bytes(&buffer, p->connectTokenData, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);

			gg_assert(buffer - start == 1 + 13 + 8 + 8 + NETCODE_CONNECT_TOKEN_NONCE_BYTES + NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);

			return (s32)(buffer - start);
		}
		else// encrypted packet
		{
			// write the prefix byte (this is a combination of the packet type and number of sequence bytes
			u08* start = buffer;

			u08 sequenceBytes = (u08)netcode_sequence_number_bytes_required(sequence);

			gg_assert(sequenceBytes >= 1);
			gg_assert(sequenceBytes <= 8);
			gg_assert(packetType <= 0xF);

			u08 prefixByte = packetType | (sequenceBytes << 4);

			netcode_write_u08(&buffer, prefixByte);

			// write the variable length sequence number [1,8] bytes
			u64 sequenceTemp = sequence;

			for (s32 i = 0; i < sequenceBytes; ++i)
			{
				netcode_write_u08(&buffer, (u08)(sequenceTemp & 0xFF));
				sequenceTemp >>= 8;
			}

			// write packet data according to type  this data will be encrypted
			u08* encryptedStart = buffer;

			switch (packetType)
			{
			case NETCODE_CONNECTION_DENIED_PACKET:
			{
				// ...
			}
			break;

			case NETCODE_CONNECTION_CHALLENGE_PACKET:
			{
				netcode_connection_challenge_packet_t* p = (netcode_connection_challenge_packet_t*)packet;
				netcode_write_u64(&buffer, p->challengeTokenSequence);
				netcode_write_bytes(&buffer, p->challengeTokenData, NETCODE_CHALLENGE_TOKEN_BYTES);
			}
			break;
			case NETCODE_CONNECTION_RESPONSE_PACKET:
			{
				netcode_connection_response_packet_t* p = (netcode_connection_response_packet_t*)packet;
				netcode_write_u64(&buffer, p->challengeTokenSequence);
				netcode_write_bytes(&buffer, p->challengeTokenData, NETCODE_CHALLENGE_TOKEN_BYTES);
			}
			break;

			case NETCODE_CONNECTION_KEEP_ALIVE_PACKET:
			{
				netcode_connection_keep_alive_packet_t* p = (netcode_connection_keep_alive_packet_t*)packet;
				netcode_write_u32(&buffer, p->clientIndex);
				netcode_write_u32(&buffer, p->maxClients);
			}
			break;

			case NETCODE_CONNECTION_PAYLOAD_PACKET:
			{
				netcode_connection_payload_packet_t* p = (netcode_connection_payload_packet_t*)packet;
				gg_assert(p->payloadBytes <= NETCODE_MAX_PAYLOAD_BYTES);

				netcode_write_bytes(&buffer, p->payloadData, p->payloadBytes);
			}
			break;

			case NETCODE_CONNECTION_DISCONNECT_PACKET:
			{
				// ...
			}
			break;
			default:
				gg_assert(0);
			}

			gg_assert(buffer - start <= bufferLength - NETCODE_MAC_BYTES);

			u08* encryptedFinish = buffer;

			// Encrypt the per-packet packet written with the prefix byte, protocol id and version as the associated data.
			// This must match to decrypt

			u08 additionalData[NETCODE_VERSION_INFO_BYTES + 8 + 1];
			{
				u08* p = additionalData;
				netcode_write_bytes(&p, NETCODE_VERSION_INFO, NETCODE_VERSION_INFO_BYTES);
				netcode_write_u64(&p, protocolId);
				netcode_write_u08(&p, prefixByte);
			}

			u08 nonce[12];
			{
				u08* p = nonce;
				netcode_write_u32(&p, 0);
				netcode_write_u64(&p, sequence);
			}

			if (netcode_encrypt_aead(encryptedStart,
				encryptedFinish - encryptedStart,
				additionalData,
				sizeof(additionalData),
				nonce,
				writePacketKey) != NETCODE_OK)
			{
				return NETCODE_ERROR;
			}

			buffer += NETCODE_MAC_BYTES;

			gg_assert(buffer - start <= bufferLength);

			return (s32)(buffer - start);
		}
	}

	void* netcode_read_packet(u08 * buffer,
		s32 bufferLength,
		u64 * sequence,
		u08 * readPacketKey,
		u64 protocolId,
		u64 currentTimestamp,
		u08 * privateKey,
		u08 * allowedPackets,
		netcode_replay_protection_t * replayProtection,
		void* allocatorContext,
		void* (*allocateFunction)(void*, u64))
	{
		gg_assert(sequence);
		gg_assert(allowedPackets);

		// todo is this still necessary? probably not
		if (allocateFunction == nullptr)
			allocateFunction = netcode_default_allocate_function;

		*sequence = 0;

		if (bufferLength < 1)
		{
			gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored packet.  Buffer length is less that 1\n");
			return nullptr;
		}

		u08* start = buffer;
		u08 prefixByte = netcode_read_u08(&buffer);

		if (prefixByte == NETCODE_CONNECTION_REQUEST_PACKET)
		{
			// connection request packet:  first byte is zero
			if (!allowedPackets[NETCODE_CONNECTION_REQUEST_PACKET])
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection request packet.  packet type is not allowed.\n");
				return nullptr;
			}

			const s32 expectedPacketLength = 1 + NETCODE_VERSION_INFO_BYTES + 8 + 8 + NETCODE_CONNECT_TOKEN_NONCE_BYTES + NETCODE_CONNECT_TOKEN_PRIVATE_BYTES;

			if (bufferLength != expectedPacketLength)
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection request packet.  Bad packet length (expected %d, got %d)\n", expectedPacketLength, bufferLength);
				return nullptr;
			}

			if (!privateKey)
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection request packet.  No private key\n");
				return nullptr;
			}

			u08 versionInfo[NETCODE_VERSION_INFO_BYTES];
			netcode_read_bytes(&buffer, versionInfo, NETCODE_VERSION_INFO_BYTES);

			if (versionInfo[0] != 'N' ||
				versionInfo[1] != 'E' ||
				versionInfo[2] != 'T' ||
				versionInfo[3] != 'C' ||
				versionInfo[4] != 'O' ||
				versionInfo[5] != 'D' ||
				versionInfo[6] != 'E' ||
				versionInfo[7] != ' ' ||
				versionInfo[8] != '1' ||
				versionInfo[9] != '.' ||
				versionInfo[10] != '0' ||
				versionInfo[11] != '2' ||
				versionInfo[12] != '\0')
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection request packet.  Bad version info\n");
				return nullptr;
			}

			u64 packetProtocolId = netcode_read_u64(&buffer);
			if (packetProtocolId != protocolId)
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection request packet.  Wrong protocol ID.  Expected %.16" PRIx64 ", got %.16" PRIx64 "\n", protocolId, packetProtocolId);
				return nullptr;
			}

			u64 packetConnectTokenExpireTimestamp = netcode_read_u64(&buffer);
			if (packetConnectTokenExpireTimestamp <= currentTimestamp)
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection request packet.  Connect token expired\n");
				return nullptr;
			}


			u08 packetConnectTokenNonce[NETCODE_CONNECT_TOKEN_NONCE_BYTES];
			netcode_read_bytes(&buffer, packetConnectTokenNonce, sizeof(packetConnectTokenNonce));
			gg_assert(buffer - start == 1 + NETCODE_VERSION_INFO_BYTES + 8 + 8 + NETCODE_CONNECT_TOKEN_NONCE_BYTES);

			if (netcode_decrypt_connect_token_private(buffer,
				NETCODE_CONNECT_TOKEN_PRIVATE_BYTES,
				versionInfo,
				protocolId,
				packetConnectTokenExpireTimestamp,
				packetConnectTokenNonce,
				privateKey) != NETCODE_OK)
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection request packet.  Connect token failed to decrypt\n");
				return nullptr;
			}


			netcode_connection_request_packet_t* packet = (netcode_connection_request_packet_t*)allocateFunction(allocatorContext, sizeof(netcode_connection_request_packet_t));
			if (!packet)
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection request packet.  Failed to allocate packet\n");
				return nullptr;
			}

			packet->packetType = NETCODE_CONNECTION_REQUEST_PACKET;
			memcpy(packet->versionInfo, versionInfo, NETCODE_VERSION_INFO_BYTES);
			packet->protocolId = packetProtocolId;
			packet->connectTokenExpireTimestamp = packetConnectTokenExpireTimestamp;
			memcpy(packet->connectTokenNonce, packetConnectTokenNonce, NETCODE_CONNECT_TOKEN_NONCE_BYTES);
			netcode_read_bytes(&buffer, packet->connectTokenData, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);

			gg_assert(buffer - start == 1 + NETCODE_VERSION_INFO_BYTES + 8 + 8 + NETCODE_CONNECT_TOKEN_NONCE_BYTES + NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);
			return packet;
		}
		else
		{
			// encrypted packets
			if (!readPacketKey)
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored encrypted packet.  No read packet key for this address\n");
				return nullptr;
			}

			if (bufferLength < 1 + 1 + NETCODE_MAC_BYTES)
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored encrypted packet.  Packet too small to be valid (%d bytes)\n", bufferLength);
				return nullptr;
			}

			// extract the packet type and number of sequence bytes from the prefix byte

			s32 packetType = prefixByte & 0xF;

			if (packetType >= NETCODE_CONNECTION_NUM_PACKETS)
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored encrypted packet.  Packet type %d is invalid\n", packetType);
				return nullptr;
			}

			if (!allowedPackets[packetType])
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored encrypted packet.  packet type %d is not allowed\n", packetType);
				return nullptr;
			}

			s32 sequenceBytes = prefixByte >> 4;
			if (sequenceBytes < 1 || sequenceBytes > 8)
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored encrypted packet.  sequence bytes %d is out or range [1,8]\n", sequenceBytes);
				return nullptr;
			}

			if (bufferLength < 1 + sequenceBytes + NETCODE_MAC_BYTES)
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored encrypted packet.  Buffer is too small for sequence bytes + encryption mac\n");
				return nullptr;
			}

			// read variable length sequence number [1, 8]
			for (s32 i = 0; i < sequenceBytes; ++i)
			{
				u08 value = netcode_read_u08(&buffer);
				(*sequence) |= (u64)(value) << (8 * i);
			}

			// replay protection (optional) ignore the packet if it has already been received
			if (replayProtection && packetType >= NETCODE_CONNECTION_KEEP_ALIVE_PACKET)
			{
				if (netcode_replay_protection_already_received(replayProtection, *sequence))
				{
					gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection payload packet.  sequence %.16" PRIx64 " already received (replay protection)\n", *sequence);
					return nullptr;
				}
			}


			// decrypt the per-packet type data
			u08 additionalData[NETCODE_VERSION_INFO_BYTES + 8 + 1];
			{
				u08* p = additionalData;
				netcode_write_bytes(&p, NETCODE_VERSION_INFO, NETCODE_VERSION_INFO_BYTES);
				netcode_write_u64(&p, protocolId);
				netcode_write_u08(&p, prefixByte);
			}
			u08 nonce[12];
			{
				u08* p = nonce;
				netcode_write_u32(&p, 0);
				netcode_write_u64(&p, *sequence);
			}

			s32 encryptedBytes = (s32)bufferLength - (buffer - start);
			if (encryptedBytes < NETCODE_MAC_BYTES)
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored encrypted packet.  Encrypted payload too small (%d bytes)\n", encryptedBytes);
				return nullptr;
			}

			if (netcode_decrypt_aead(buffer, encryptedBytes, additionalData, sizeof(additionalData), nonce, readPacketKey) != NETCODE_OK)
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored encrypted packet.  Failed to decrypt");
				return nullptr;
			}

			// update the latest replay protection sequence number
			if (replayProtection && packetType >= NETCODE_CONNECTION_KEEP_ALIVE_PACKET)
				netcode_replay_protection_advance_sequence(replayProtection, *sequence);

			// process the per-packet type data that was just decrypted
			s32 decryptedBytes = encryptedBytes - NETCODE_MAC_BYTES;
			switch (packetType)
			{
			case NETCODE_CONNECTION_DENIED_PACKET:
			{
				if (decryptedBytes != 0)
				{
					gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection denied packet.  Decrypted packet data is wrong size\n");
					return nullptr;
				}

				netcode_connection_denied_packet_t* packet = SCAST(netcode_connection_denied_packet_t*, allocateFunction(allocatorContext, sizeof(netcode_connection_denied_packet_t)));

				if (!packet)
				{
					gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection denied packet.  Unable to allocate packet struct\n");
					return nullptr;
				}

				packet->packetType = NETCODE_CONNECTION_DENIED_PACKET;
				return packet;
			}
			break;

			case NETCODE_CONNECTION_CHALLENGE_PACKET:
			{
				if (decryptedBytes != 8 + NETCODE_CHALLENGE_TOKEN_BYTES)
				{
					gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection challenge packet.  Decrypted packet data is wrong size\n");
					return nullptr;
				}

				netcode_connection_challenge_packet_t* packet = SCAST(netcode_connection_challenge_packet_t*, allocateFunction(allocatorContext, sizeof(netcode_connection_challenge_packet_t)));
				if (!packet)
				{
					gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection challenge packet.  Unable to allocate packet struct\n");
					return nullptr;
				}

				packet->packetType = NETCODE_CONNECTION_CHALLENGE_PACKET;
				packet->challengeTokenSequence = netcode_read_u64(&buffer);
				netcode_read_bytes(&buffer, packet->challengeTokenData, NETCODE_CHALLENGE_TOKEN_BYTES);
				return packet;
			}
			break;

			case NETCODE_CONNECTION_RESPONSE_PACKET:
			{
				if (decryptedBytes != 8 + NETCODE_CHALLENGE_TOKEN_BYTES)
				{
					gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection response packet.  Decrypted packet data is wrong size\n");
					return nullptr;
				}

				netcode_connection_response_packet_t* packet = SCAST(netcode_connection_response_packet_t*, allocateFunction(allocatorContext, sizeof(netcode_connection_response_packet_t)));
				if (!packet)
				{
					gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection response packet.  Unable to allocate packet struct\n");
					return nullptr;
				}

				packet->packetType = NETCODE_CONNECTION_RESPONSE_PACKET;
				packet->challengeTokenSequence = netcode_read_u64(&buffer);
				netcode_read_bytes(&buffer, packet->challengeTokenData, NETCODE_CHALLENGE_TOKEN_BYTES);
				return packet;
			}
			break;

			case NETCODE_CONNECTION_KEEP_ALIVE_PACKET:
			{
				if (decryptedBytes != 8)
				{
					gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection keep alive packet.  Decrypted packet data is wrong size\n");
					return nullptr;
				}

				netcode_connection_keep_alive_packet_t* packet = SCAST(netcode_connection_keep_alive_packet_t*, allocateFunction(allocatorContext, sizeof(netcode_connection_keep_alive_packet_t)));
				if (!packet)
				{
					gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection keep alive packet.  Unable to allocate packet struct\n");
					return nullptr;
				}

				packet->packetType = NETCODE_CONNECTION_KEEP_ALIVE_PACKET;
				packet->clientIndex = netcode_read_u32(&buffer);
				packet->maxClients = netcode_read_u32(&buffer);
				return packet;
			}
			break;

			case NETCODE_CONNECTION_PAYLOAD_PACKET:
			{
				if (decryptedBytes < 1)
				{
					gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection payload packet.  Payload is too small\n");
					return nullptr;
				}

				if (decryptedBytes > NETCODE_MAX_PAYLOAD_BYTES)
				{
					gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection payload packet.  Payload is too large\n");
					return nullptr;
				}

				netcode_connection_payload_packet_t* packet = netcode_create_payload_packet(decryptedBytes, allocatorContext, allocateFunction);
				if (!packet)
				{
					gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection payload packet.  Unable to allocate packet struct\n");
					return nullptr;
				}

				memcpy(packet->payloadData, buffer, decryptedBytes);
				return packet;
			}
			break;

			case NETCODE_CONNECTION_DISCONNECT_PACKET:
			{
				if (decryptedBytes != 0)
				{
					gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored disconnect packet.  Decrypted packet data is wrong size\n");
					return nullptr;
				}

				netcode_connection_disconnect_packet_t* packet = SCAST(netcode_connection_disconnect_packet_t*, allocateFunction(allocatorContext, sizeof(netcode_connection_disconnect_packet_t)));
				if (!packet)
				{
					gg_printf(GG_LOG_LEVEL_DEBUG, "Ignored connection disconnect packet.  Unable to allocate packet struct\n");
					return nullptr;
				}

				packet->packetType = NETCODE_CONNECTION_DISCONNECT_PACKET;
				return packet;
			}
			break;
			default:
				return nullptr;
			}
		}
	}



	/*

		network simulator packet entry

	*/

#define NETCODE_NETWORK_SIMULATOR_NUM_PACKET_ENTRIES          (NETCODE_MAX_CLIENTS * 256)
#define NETCODE_NETWORK_SIMULATOR_NUM_PENDING_RECEIVE_PACKETS (NETCODE_MAX_CLIENTS * 64)

	struct netcode_network_simulator_packet_entry_t
	{
		netcode_address_t from;
		netcode_address_t to;
		f64 deliveryTime;
		u08* packetData;
		s32 packetBytes;
	};


	typedef std::array<netcode_network_simulator_packet_entry_t, NETCODE_NETWORK_SIMULATOR_NUM_PACKET_ENTRIES> PacketEntries_t;
	typedef std::array<netcode_network_simulator_packet_entry_t, NETCODE_NETWORK_SIMULATOR_NUM_PENDING_RECEIVE_PACKETS> PendingReceivePackets_t;

	struct netcode_network_simulator_t
	{
		void* allocatorContext;
		void* (*allocateFunction)(void*, u64);
		void(*freeFunction)(void*, void*);
		f32 latencyMilliseconds;
		f32 jitterMilliseconds;
		f32 packetLossPercent;
		f32 duplicatePacketPercent;
		f64 time;
		s32 currentIndex;
		s32 numPendingReceivePackets;

		PacketEntries_t packetEntries;
		PendingReceivePackets_t pendingReceivePackets;
	};

	netcode_network_simulator_t* netcode_network_simulator_create(void* allocatorContext,
		void* (*allocateFunction_)(void*, u64),
		void(*freeFunction_)(void*, void*))
	{
		if (allocateFunction_ == nullptr)
			allocateFunction_ = netcode_default_allocate_function;
		if (freeFunction_ == nullptr)
			freeFunction_ = netcode_default_free_function;

		netcode_network_simulator_t* networkSimulator = (netcode_network_simulator_t*)allocateFunction_(allocatorContext, sizeof(netcode_network_simulator_t));

		gg_assert(networkSimulator);

		memset(networkSimulator, 0, sizeof(netcode_network_simulator_t));

		networkSimulator->allocatorContext = allocatorContext;
		networkSimulator->allocateFunction = allocateFunction_;
		networkSimulator->freeFunction = freeFunction_;

		return networkSimulator;
	}


	void netcode_network_simulator_reset(netcode_network_simulator_t * networkSimulator)
	{
		gg_assert(networkSimulator);

		gg_printf(GG_LOG_LEVEL_DEBUG, "Network simulator reset\n");

		for (s32 i = 0; i < NETCODE_NETWORK_SIMULATOR_NUM_PACKET_ENTRIES; ++i)
		{
			networkSimulator->freeFunction(networkSimulator->allocatorContext, networkSimulator->packetEntries[i].packetData);
			memset(&networkSimulator->packetEntries[i], 0, sizeof(netcode_network_simulator_packet_entry_t));
		}

		for (s32 i = 0; i < networkSimulator->numPendingReceivePackets; ++i)
		{
			networkSimulator->freeFunction(networkSimulator->allocatorContext, networkSimulator->pendingReceivePackets[i].packetData);
			memset(&networkSimulator->pendingReceivePackets[i], 0, sizeof(netcode_network_simulator_packet_entry_t));
		}

		networkSimulator->currentIndex = 0;
		networkSimulator->numPendingReceivePackets = 0;
	}

	void netcode_network_simulator_destroy(netcode_network_simulator_t * networkSimulator)
	{
		gg_assert(networkSimulator);
		netcode_network_simulator_reset(networkSimulator);
		networkSimulator->freeFunction(networkSimulator->allocatorContext, networkSimulator);
	}

	f32 netcode_random_f32(f32 a, f32 b)
	{
		gg_assert(a < b);
		f32 random = ((f32)rand() / (f32)RAND_MAX);
		f32 diff = b - a;
		f32 r = random * diff;
		return a + r;
	}

	void netcode_network_simulator_queue_packet(netcode_network_simulator_t * networkSimulator, netcode_address_t * from, netcode_address_t * to, u08 * packetData, s32 packetBytes, f32 delay)
	{
		gg_assert(networkSimulator);
		gg_assert(from);
		gg_assert(to);
		gg_assert(packetData);
		gg_assert(packetBytes > 0);
		gg_assert(packetBytes <= NETCODE_MAX_PACKET_BYTES);

		networkSimulator->packetEntries[networkSimulator->currentIndex].from = *from;
		networkSimulator->packetEntries[networkSimulator->currentIndex].to = *to;
		networkSimulator->packetEntries[networkSimulator->currentIndex].packetData = (u08*)networkSimulator->allocateFunction(networkSimulator->allocatorContext, packetBytes);
		memcpy(networkSimulator->packetEntries[networkSimulator->currentIndex].packetData, packetData, packetBytes);

		networkSimulator->packetEntries[networkSimulator->currentIndex].packetBytes = packetBytes;
		networkSimulator->packetEntries[networkSimulator->currentIndex].deliveryTime = networkSimulator->time + delay;
		networkSimulator->currentIndex++;
		networkSimulator->currentIndex %= NETCODE_NETWORK_SIMULATOR_NUM_PACKET_ENTRIES;
	}

	void netcode_network_simulator_send_packet(netcode_network_simulator_t * networkSimulator, netcode_address_t * from, netcode_address_t * to, u08 * packetData, s32 packetBytes)
	{
		gg_assert(networkSimulator);
		gg_assert(from);
		gg_assert(from->type != 0);
		gg_assert(to);
		gg_assert(to->type != 0);
		gg_assert(packetData);
		gg_assert(packetBytes > 0);
		gg_assert(packetBytes <= NETCODE_MAX_PACKET_BYTES);

		if (netcode_random_f32(0.0f, 100.0f) <= networkSimulator->packetLossPercent)
			return;

		if (networkSimulator->packetEntries[networkSimulator->currentIndex].packetData)
		{
			networkSimulator->freeFunction(networkSimulator->allocatorContext, networkSimulator->packetEntries[networkSimulator->currentIndex].packetData);
			networkSimulator->packetEntries[networkSimulator->currentIndex].packetData = nullptr;
		}

		f32 delay = networkSimulator->latencyMilliseconds / 1000.0f;

		if (networkSimulator->jitterMilliseconds > 0)
		{
			delay += netcode_random_f32(-networkSimulator->jitterMilliseconds, +networkSimulator->jitterMilliseconds) / 1000.0f;
		}

		netcode_network_simulator_queue_packet(networkSimulator, from, to, packetData, packetBytes, delay);

		if (netcode_random_f32(0.0f, 100.0f) <= networkSimulator->duplicatePacketPercent)
		{
			netcode_network_simulator_queue_packet(networkSimulator, from, to, packetData, packetBytes, delay + netcode_random_f32(0.0f, 1.0f));
		}
	}

	s32 netcode_network_simulator_receive_packets(netcode_network_simulator_t * networkSimulator, netcode_address_t * to, s32 maxPackets, u08 * *packetData, s32 * packetBytes, netcode_address_t * from)
	{
		gg_assert(networkSimulator);
		gg_assert(packetData);
		gg_assert(packetBytes);
		gg_assert(from);
		gg_assert(to);
		gg_assert(maxPackets >= 0);

		s32 numPackets = 0;

		for (s32 i = 0; i < networkSimulator->numPendingReceivePackets; ++i)
		{
			if (numPackets == maxPackets)
				break;

			if (!networkSimulator->pendingReceivePackets[i].packetData)
				continue;

			if (!netcode_address_equal(&networkSimulator->pendingReceivePackets[i].to, to))
				continue;

			packetData[numPackets] = networkSimulator->pendingReceivePackets[i].packetData;
			packetBytes[numPackets] = networkSimulator->pendingReceivePackets[i].packetBytes;
			from[numPackets] = networkSimulator->pendingReceivePackets[i].from;

			networkSimulator->pendingReceivePackets[i].packetData = nullptr;

			++numPackets;
		}

		gg_assert(numPackets <= maxPackets);

		return numPackets;
	}


	void netcode_network_simulator_update(netcode_network_simulator_t * networkSimulator, f64 time)
	{
		gg_assert(networkSimulator);

		networkSimulator->time = time;

		// discard any pending receive packets that are still in the buffer
		for (s32 i = 0; i < networkSimulator->numPendingReceivePackets; ++i)
		{
			if (networkSimulator->pendingReceivePackets[i].packetData)
			{
				networkSimulator->freeFunction(networkSimulator->allocatorContext, networkSimulator->pendingReceivePackets[i].packetData);
				networkSimulator->pendingReceivePackets[i].packetData = nullptr;
			}
		}

		networkSimulator->numPendingReceivePackets = 0;

		// walk across packet entries and move any that are ready to be received into the pending receive buffer

		for (s32 i = 0; i < NETCODE_NETWORK_SIMULATOR_NUM_PACKET_ENTRIES; ++i)
		{
			if (!networkSimulator->packetEntries[i].packetData)
				continue;

			if (networkSimulator->numPendingReceivePackets == NETCODE_NETWORK_SIMULATOR_NUM_PENDING_RECEIVE_PACKETS)
				break;

			if (networkSimulator->packetEntries[i].packetData && networkSimulator->packetEntries[i].deliveryTime <= time)
			{
				networkSimulator->pendingReceivePackets[networkSimulator->numPendingReceivePackets] = networkSimulator->packetEntries[i];
				networkSimulator->numPendingReceivePackets++;
				networkSimulator->packetEntries[i].packetData = nullptr;
			}
		}
	}



	/*
	  Client State
	*/
	const istring netcode_client_state_name(s32 clientState)
	{
		switch (clientState)
		{
		case NETCODE_CLIENT_STATE_CONNECT_TOKEN_EXPIRED:                return "connect token expired";
		case NETCODE_CLIENT_STATE_INVALID_CONNECT_TOKEN:                return "invalid connect token";
		case NETCODE_CLIENT_STATE_CONNECTION_TIMED_OUT:                 return "connection timed out";
		case NETCODE_CLIENT_STATE_CONNECTION_REQUEST_TIMED_OUT:         return "connection request timed out";
		case NETCODE_CLIENT_STATE_CONNECTION_RESPONSE_TIMED_OUT:        return "connection response timed out";
		case NETCODE_CLIENT_STATE_CONNECTION_DENIED:                    return "connection denied";
		case NETCODE_CLIENT_STATE_DISCONNECTED:                         return "disconnected";
		case NETCODE_CLIENT_STATE_SENDING_CONNECTION_REQUEST:           return "sending connection request";
		case NETCODE_CLIENT_STATE_SENDING_CONNECTION_RESPONSE:          return "sending connection response";
		case NETCODE_CLIENT_STATE_CONNECTED:                            return "connected";
		default:
			gg_assert(0);
			return "Invalid client state";
		}
	}

	void netcode_default_client_config(netcode_client_config_t * config)
	{
		gg_assert(config);
		config->allocatorContext = nullptr;
		config->allocate_function = netcode_default_allocate_function;
		config->free_function = netcode_default_free_function;
		config->networkSimulator = nullptr;
		config->callbackContext = nullptr;
		config->state_change_callback = nullptr;
		config->send_loopback_packet_callback = nullptr;
		config->overrideSendAndReceive = 0;
		config->send_packet_override = nullptr;
		config->receive_packet_override = nullptr;
	}

	struct netcode_client_t
	{
		netcode_client_config_t config;
		s32 state;
		f64 time;
		f64 connectStartTime;
		f64 lastPacketSendTime;
		f64 lastPacketReceiveTime;
		s32 shouldDisconnect;
		s32 shouldDisconnectState;
		u64 sequence;
		s32 clientIndex;
		s32 maxClients;
		s32 serverAddressIndex;
		netcode_address_t address;
		netcode_address_t serverAddress;
		netcode_connect_token_t connectToken;
		netcode_socket_holder_t socketHolder;
		netcode_context_t context;
		netcode_replay_protection_t replayProtection;
		netcode_packet_queue_t packetReceiveQueue;
		u64 challengeTokenSequence;
		u08 challengeTokenData[NETCODE_CHALLENGE_TOKEN_BYTES];
		u08* receivePacketData[NETCODE_CLIENT_MAX_RECEIVE_PACKETS];
		s32 receivePacketBytes[NETCODE_CLIENT_MAX_RECEIVE_PACKETS];
		netcode_address_t receiveFrom[NETCODE_CLIENT_MAX_RECEIVE_PACKETS];
		bool loopback;
	};

	bool netcode_client_socket_create(netcode_socket_t * socket, netcode_address_t * address, s32 sendBufferSize, s32 receiveBufferSize, const netcode_client_config_t * config)
	{
		gg_assert(socket);
		gg_assert(address);
		gg_assert(config);

		if (!config->networkSimulator)
		{
			if (!config->overrideSendAndReceive)
			{
				if (netcode_socket_create(socket, address, sendBufferSize, receiveBufferSize) != NETCODE_SOCKET_ERROR_NONE)
				{
					return false;
				}
			}
		}
		else
		{
			if (address->port == 0)
			{
				gg_printf(GG_LOG_LEVEL_ERROR, "Error: must bind to a specific port when using network simulator\n");
				return false;
			}
		}
		return true;
	}

	netcode_client_t* netcode_client_create_overload(const c08 * address1String, const c08 * address2String, const netcode_client_config_t * config, f64 time)
	{
		gg_assert(config);
		gg_assert(initialized);

		netcode_address_t address1, address2;
		memset(&address1, 0, sizeof(address1));
		memset(&address2, 0, sizeof(address2));

		if (netcode_parse_address(address1String, &address1) != NETCODE_OK)
		{
			gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to parse client address\n");
			return nullptr;
		}

		if (address2String != nullptr && netcode_parse_address(address2String, &address2) != NETCODE_OK)
		{
			gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to parse client address2\n");
			return nullptr;
		}

		netcode_socket_t socketIpv4;
		netcode_socket_t socketIpv6;

		memset(&socketIpv4, 0, sizeof(socketIpv4));
		memset(&socketIpv6, 0, sizeof(socketIpv6));

		if (address1.type == NETCODE_ADDRESS_IPV4 || address2.type == NETCODE_ADDRESS_IPV4)
		{
			if (!netcode_client_socket_create(&socketIpv4, address1.type == NETCODE_ADDRESS_IPV4 ? &address1 : &address2, NETCODE_CLIENT_SOCKET_SNDBUF_SIZE, NETCODE_CLIENT_SOCKET_RCVBUF_SIZE, config))
			{
				return nullptr;
			}
		}

		if (address1.type == NETCODE_ADDRESS_IPV6 || address2.type == NETCODE_ADDRESS_IPV6)
		{
			if (!netcode_client_socket_create(&socketIpv6, address1.type == NETCODE_ADDRESS_IPV6 ? &address1 : &address2, NETCODE_CLIENT_SOCKET_SNDBUF_SIZE, NETCODE_CLIENT_SOCKET_RCVBUF_SIZE, config))
			{
				return nullptr;
			}
		}


		netcode_client_t* client = (netcode_client_t*)config->allocate_function(config->allocatorContext, sizeof(netcode_client_t));

		if (!client)
		{
			netcode_socket_destroy(&socketIpv4);
			netcode_socket_destroy(&socketIpv6);
			return nullptr;
		}

		netcode_address_t socketAddress = address1.type == NETCODE_ADDRESS_IPV4 ? socketIpv4.address : socketIpv6.address;

		if (!config->networkSimulator)
		{
			gg_printf(GG_LOG_LEVEL_INFO, "Client started on port %d\n", socketAddress.port);
		}
		else
		{
			gg_printf(GG_LOG_LEVEL_INFO, "Client started on port %d (network simulator)\n", socketAddress.port);
		}


		client->config = *config;
		client->socketHolder.ipv4 = socketIpv4;
		client->socketHolder.ipv6 = socketIpv6;
		client->address = config->networkSimulator ? address1 : socketAddress;
		client->state = NETCODE_CLIENT_STATE_DISCONNECTED;
		client->time = time;
		client->connectStartTime = 0.0;
		client->lastPacketSendTime = -1000.0;
		client->lastPacketReceiveTime = -1000.0;
		client->shouldDisconnect = 0;
		client->shouldDisconnectState = NETCODE_CLIENT_STATE_DISCONNECTED;
		client->sequence = 0;
		client->clientIndex = 0;
		client->maxClients = 0;
		client->serverAddressIndex = 0;
		client->challengeTokenSequence = 0;
		client->loopback = 0;

		memset(&client->serverAddress, 0, sizeof(netcode_address_t));
		memset(&client->connectToken, 0, sizeof(netcode_connect_token_t));
		memset(&client->context, 0, sizeof(netcode_context_t));
		memset(client->challengeTokenData, 0, NETCODE_CHALLENGE_TOKEN_BYTES);

		netcode_packet_queue_init(&client->packetReceiveQueue, config->allocatorContext, config->allocate_function, config->free_function);

		netcode_replay_protection_reset(&client->replayProtection);

		return client;
	}

	netcode_client_t* netcode_client_create(const c08 * address, const netcode_client_config_t * config, f64 time)
	{
		return netcode_client_create_overload(address, nullptr, config, time);
	}

	void netcode_client_destroy(netcode_client_t * client)
	{
		gg_assert(client);

		if (!client->loopback)
			netcode_client_disconnect(client);
		else
			netcode_client_disconnect_loopback(client);

		netcode_socket_destroy(&client->socketHolder.ipv4);
		netcode_socket_destroy(&client->socketHolder.ipv6);
		netcode_packet_queue_clear(&client->packetReceiveQueue);
		client->config.free_function(client->config.allocatorContext, client);
	}


	void netcode_client_set_state(netcode_client_t * client, s32 clientState)
	{
		gg_printf(GG_LOG_LEVEL_DEBUG, "Client changed state from '%s' to '%s'\n", netcode_client_state_name(client->state).c_str(), netcode_client_state_name(clientState).c_str());

		if (client->config.state_change_callback)
			client->config.state_change_callback(client->config.callbackContext, client->state, clientState);

		client->state = clientState;
	}

	void netcode_client_reset_before_next_connect(netcode_client_t * client)
	{
		gg_assert(client);
		client->connectStartTime = client->time;
		client->lastPacketSendTime = client->time - 1.0f;
		client->lastPacketReceiveTime = client->time;
		client->shouldDisconnect = false;
		client->shouldDisconnectState = NETCODE_CLIENT_STATE_DISCONNECTED;
		client->challengeTokenSequence = 0;

		memset(client->challengeTokenData, 0, NETCODE_CHALLENGE_TOKEN_BYTES);

		netcode_replay_protection_reset(&client->replayProtection);
	}

	void netcode_client_reset_connection_data(netcode_client_t * client, s32 clientState)
	{
		gg_assert(client);

		client->sequence = 0;
		client->loopback = 0;
		client->clientIndex = 0;
		client->maxClients = 0;
		client->connectStartTime = 0.0;
		client->serverAddressIndex = 0;

		memset(&client->serverAddress, 0, sizeof(netcode_address_t));
		memset(&client->connectToken, 0, sizeof(netcode_connect_token_t));
		memset(&client->context, 0, sizeof(netcode_context_t));

		netcode_client_set_state(client, clientState);
		netcode_client_reset_before_next_connect(client);

		while (1)
		{
			void* packet = netcode_packet_queue_pop(&client->packetReceiveQueue, nullptr);
			if (!packet)
				break;
			client->config.free_function(client->config.allocatorContext, packet);
		}

		netcode_packet_queue_clear(&client->packetReceiveQueue);
	}

	void netcode_client_disconnect_internal(netcode_client_t * client, s32 destinationState, bool sendDisconnectPackets);

	void netcode_client_connect(netcode_client_t * client, u08 * connectToken)
	{
		gg_assert(client);
		gg_assert(connectToken);

		netcode_client_disconnect(client);

		if (netcode_read_connect_token(connectToken, NETCODE_CONNECT_TOKEN_BYTES, &client->connectToken) != NETCODE_OK)
		{
			netcode_client_set_state(client, NETCODE_CLIENT_STATE_INVALID_CONNECT_TOKEN);
			return;
		}
		client->serverAddressIndex = 0;
		client->serverAddress = client->connectToken.serverAddresses[0];

		c08 serverAddressString[NETCODE_MAX_ADDRESS_STRING_LENGTH];

		gg_printf(GG_LOG_LEVEL_INFO, "Client connecting to server %s [%d/%d]\n",
			netcode_address_to_string(&client->serverAddress, serverAddressString),
			client->serverAddressIndex + 1,
			client->connectToken.numServerAddresses);

		memcpy(client->context.readPacketKey, client->connectToken.serverToClientKey, NETCODE_KEY_BYTES);
		memcpy(client->context.writePacketKey, client->connectToken.clientToServerKey, NETCODE_KEY_BYTES);

		netcode_client_reset_before_next_connect(client);
		netcode_client_set_state(client, NETCODE_CLIENT_STATE_SENDING_CONNECTION_REQUEST);
	}

	void netcode_client_process_packet_internal(netcode_client_t * client, netcode_address_t * from, u08 * packet, u64 sequence)
	{
		gg_assert(client);
		gg_assert(packet);

		u08 packetType = ((u08*)packet)[0];

		switch (packetType)
		{
		case NETCODE_CONNECTION_DENIED_PACKET:
		{
			if ((client->state == NETCODE_CLIENT_STATE_SENDING_CONNECTION_REQUEST ||
				client->state == NETCODE_CLIENT_STATE_SENDING_CONNECTION_RESPONSE)
				&&
				netcode_address_equal(from, &client->serverAddress))
			{
				client->shouldDisconnect = 1;
				client->shouldDisconnectState = NETCODE_CLIENT_STATE_CONNECTION_DENIED;
				client->lastPacketReceiveTime = client->time;
			}
		}
		break;

		case NETCODE_CONNECTION_CHALLENGE_PACKET:
		{
			if (client->state == NETCODE_CLIENT_STATE_SENDING_CONNECTION_REQUEST && netcode_address_equal(from, &client->serverAddress))
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Client received connection challenge packet from server\n");

				netcode_connection_challenge_packet_t* p = RECAST(netcode_connection_challenge_packet_t*, packet);

				client->challengeTokenSequence = p->challengeTokenSequence;
				memcpy(client->challengeTokenData, p->challengeTokenData, NETCODE_CHALLENGE_TOKEN_BYTES);
				client->lastPacketReceiveTime = client->time;

				netcode_client_set_state(client, NETCODE_CLIENT_STATE_SENDING_CONNECTION_RESPONSE);
			}
		}
		break;

		case NETCODE_CONNECTION_KEEP_ALIVE_PACKET:
		{
			if (netcode_address_equal(from, &client->serverAddress))
			{
				netcode_connection_keep_alive_packet_t* p = RECAST(netcode_connection_keep_alive_packet_t*, packet);

				if (client->state == NETCODE_CLIENT_STATE_CONNECTED)
				{
					gg_printf(GG_LOG_LEVEL_DEBUG, "Client received connection keep alive packet from server\n");
					client->lastPacketReceiveTime = client->time;
				}
				else if (client->state == NETCODE_CLIENT_STATE_SENDING_CONNECTION_RESPONSE)
				{
					gg_printf(GG_LOG_LEVEL_DEBUG, "Client received connection keep alive packet from server\n");

					client->lastPacketReceiveTime = client->time;
					client->clientIndex = p->clientIndex;
					client->maxClients = p->maxClients;

					netcode_client_set_state(client, NETCODE_CLIENT_STATE_CONNECTED);
					gg_printf(GG_LOG_LEVEL_INFO, "Client connected to server\n");
				}
			}
		}
		break;

		case NETCODE_CONNECTION_PAYLOAD_PACKET:
		{
			if (client->state == NETCODE_CLIENT_STATE_CONNECTED && netcode_address_equal(from, &client->serverAddress))
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Client received connection payload packet from server\n");

				netcode_packet_queue_push(&client->packetReceiveQueue, packet, sequence);
				client->lastPacketReceiveTime = client->time;

				return;
			}
		}
		break;

		case NETCODE_CONNECTION_DISCONNECT_PACKET:
		{
			if (client->state == NETCODE_CLIENT_STATE_CONNECTED && netcode_address_equal(from, &client->serverAddress))
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Client received disconnect packet from server\n");

				client->shouldDisconnect = 1;
				client->shouldDisconnectState = NETCODE_CLIENT_STATE_DISCONNECTED;
				client->lastPacketReceiveTime = client->time;
			}
		}
		break;

		default:
			gg_printf(GG_LOG_LEVEL_DEBUG, "Client receuved unknown packet type %d\n", packetType);
			break;
		}

		client->config.free_function(client->config.allocatorContext, packet);
	}

	void netcode_client_process_packet(netcode_client_t * client, netcode_address_t * from, u08 * packetData, s32 packetBytes)
	{
		(void)client;
		(void)from;
		(void)packetData;
		(void)packetBytes;

		// TODO: switch back to using std::array
		//std::array < u08, NETCODE_CONNECTION_NUM_PACKETS > allowedPackets{};
		u08 allowedPackets[NETCODE_CONNECTION_NUM_PACKETS];
		memset(allowedPackets, 0, sizeof(allowedPackets));

		allowedPackets[NETCODE_CONNECTION_DENIED_PACKET] = 1;
		allowedPackets[NETCODE_CONNECTION_CHALLENGE_PACKET] = 1;
		allowedPackets[NETCODE_CONNECTION_KEEP_ALIVE_PACKET] = 1;
		allowedPackets[NETCODE_CONNECTION_PAYLOAD_PACKET] = 1;
		allowedPackets[NETCODE_CONNECTION_DISCONNECT_PACKET] = 1;

		u64 currentTimestamp = (u64)time(nullptr);

		u64 sequence;
		void* packet = netcode_read_packet(packetData,
			packetBytes,
			&sequence,
			client->context.readPacketKey,
			client->connectToken.protocolId,
			currentTimestamp,
			nullptr,
			allowedPackets,
			&client->replayProtection,
			client->config.allocatorContext,
			client->config.allocate_function);


		if (!packet)
			return;

		netcode_client_process_packet_internal(client, from, (u08*)packet, sequence);
	}

	void netcode_client_receive_packets(netcode_client_t * client)
	{
		gg_assert(client);
		gg_assert(!client->loopback);

		// TODO: switch back to using std::array
		//std::array < u08, NETCODE_CONNECTION_NUM_PACKETS > allowedPackets{};
		u08 allowedPackets[NETCODE_CONNECTION_NUM_PACKETS];
		memset(allowedPackets, 0, sizeof(allowedPackets));

		allowedPackets[NETCODE_CONNECTION_DENIED_PACKET] = 1;
		allowedPackets[NETCODE_CONNECTION_CHALLENGE_PACKET] = 1;
		allowedPackets[NETCODE_CONNECTION_KEEP_ALIVE_PACKET] = 1;
		allowedPackets[NETCODE_CONNECTION_PAYLOAD_PACKET] = 1;
		allowedPackets[NETCODE_CONNECTION_DISCONNECT_PACKET] = 1;

		u64 currentTimestamp = (u64)time(nullptr);

		if (!client->config.networkSimulator)
		{
			// process packets received from socket
			while (1)
			{

				netcode_address_t from;
				//std::array < u08, NETCODE_MAX_PACKET_BYTES > packetData;
				u08 packetData[NETCODE_MAX_PACKET_BYTES];
				s32 packetBytes = 0;

				if (client->config.overrideSendAndReceive)
				{
					packetBytes = client->config.receive_packet_override(client->config.callbackContext, &from, packetData, NETCODE_MAX_PACKET_BYTES);
				}
				else if (client->serverAddress.type == NETCODE_ADDRESS_IPV4)
				{
					packetBytes = netcode_socket_receive_packet(&client->socketHolder.ipv4, &from, packetData, NETCODE_MAX_PACKET_BYTES);
				}
				else if (client->serverAddress.type == NETCODE_ADDRESS_IPV6)
				{
					packetBytes = netcode_socket_receive_packet(&client->socketHolder.ipv6, &from, packetData, NETCODE_MAX_PACKET_BYTES);
				}

				if (packetBytes == 0)
					break;

				u64 sequence;
				void* packet = netcode_read_packet(packetData,
					packetBytes,
					&sequence,
					client->context.readPacketKey,
					client->connectToken.protocolId,
					currentTimestamp,
					nullptr,
					allowedPackets,
					&client->replayProtection,
					client->config.allocatorContext,
					client->config.allocate_function);

				if (!packet)
					return;

				netcode_client_process_packet_internal(client, &from, (u08*)packet, sequence);
			}
		}
		else
		{
			// process packets received from network simulator

			s32 numPacketsReceived = netcode_network_simulator_receive_packets(client->config.networkSimulator,
				&client->address,
				NETCODE_CLIENT_MAX_RECEIVE_PACKETS,
				client->receivePacketData,
				client->receivePacketBytes,
				client->receiveFrom);

			for (s32 i = 0; i < numPacketsReceived; ++i)
			{
				u64 sequence;

				void* packet = netcode_read_packet(client->receivePacketData[i],
					client->receivePacketBytes[i],
					&sequence,
					client->context.readPacketKey,
					client->connectToken.protocolId,
					currentTimestamp,
					nullptr,
					allowedPackets,
					&client->replayProtection,
					client->config.allocatorContext,
					client->config.allocate_function);

				client->config.free_function(client->config.allocatorContext, client->receivePacketData[i]);

				if (!packet)
					continue;

				netcode_client_process_packet_internal(client, &client->receiveFrom[i], (u08*)packet, sequence);
			}
		}
	}

	void netcode_client_send_packet_to_server_internal(netcode_client_t * client, void* packet)
	{
		gg_assert(client);
		gg_assert(!client->loopback);

		// TODO: switch back to using std::array
		//std::array < u08, NETCODE_MAX_PACKET_BYTES > packetData;
		u08 packetData[NETCODE_MAX_PACKET_BYTES];

		s32 packetBytes = netcode_write_packet(packet,
			packetData,
			NETCODE_MAX_PACKET_BYTES,
			client->sequence++,
			client->context.writePacketKey,
			client->connectToken.protocolId);

		gg_assert(packetBytes <= NETCODE_MAX_PACKET_BYTES);

		if (client->config.networkSimulator)
		{
			netcode_network_simulator_send_packet(client->config.networkSimulator, &client->address, &client->serverAddress, packetData, packetBytes);
		}
		else
		{
			if (client->config.overrideSendAndReceive)
			{
				client->config.send_packet_override(client->config.callbackContext, &client->serverAddress, packetData, packetBytes);
			}
			else if (client->serverAddress.type == NETCODE_ADDRESS_IPV4)
			{
				netcode_socket_send_packet(&client->socketHolder.ipv4, &client->serverAddress, packetData, packetBytes);
			}
			else if (client->serverAddress.type == NETCODE_ADDRESS_IPV6)
			{
				netcode_socket_send_packet(&client->socketHolder.ipv6, &client->serverAddress, packetData, packetBytes);
			}
		}
		client->lastPacketSendTime = client->time;
	}

	void netcode_client_send_packets(netcode_client_t * client)
	{
		gg_assert(client);
		gg_assert(!client->loopback);

		switch (client->state)
		{
		case NETCODE_CLIENT_STATE_SENDING_CONNECTION_REQUEST:
		{
			if (client->lastPacketSendTime + (1.0 / NETCODE_PACKET_SEND_RATE) >= client->time)
				return;

			gg_printf(GG_LOG_LEVEL_DEBUG, "Client sent connection request packet to server\n");

			netcode_connection_request_packet_t packet;
			packet.packetType = NETCODE_CONNECTION_REQUEST_PACKET;
			memcpy(packet.versionInfo, NETCODE_VERSION_INFO, NETCODE_VERSION_INFO_BYTES);

			packet.protocolId = client->connectToken.protocolId;
			packet.connectTokenExpireTimestamp = client->connectToken.expireTimestamp;
			memcpy(packet.connectTokenNonce, client->connectToken.nonce, NETCODE_CONNECT_TOKEN_NONCE_BYTES);
			memcpy(packet.connectTokenData, client->connectToken.privateData, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);

			netcode_client_send_packet_to_server_internal(client, &packet);
		}
		break;
		case NETCODE_CLIENT_STATE_SENDING_CONNECTION_RESPONSE:
		{
			if (client->lastPacketSendTime + (1.0 / NETCODE_PACKET_SEND_RATE) >= client->time)
				return;

			gg_printf(GG_LOG_LEVEL_DEBUG, "Client sent connection response packet to server\n");

			netcode_connection_response_packet_t packet;
			packet.packetType = NETCODE_CONNECTION_RESPONSE_PACKET;
			packet.challengeTokenSequence = client->challengeTokenSequence;
			memcpy(packet.challengeTokenData, client->challengeTokenData, NETCODE_CHALLENGE_TOKEN_BYTES);

			netcode_client_send_packet_to_server_internal(client, &packet);
		}
		break;
		case NETCODE_CLIENT_STATE_CONNECTED:
		{
			if (client->lastPacketSendTime + (1.0 / NETCODE_PACKET_SEND_RATE) >= client->time)
				return;

			gg_printf(GG_LOG_LEVEL_DEBUG, "Client sent connection keep alive packet to server\n");

			netcode_connection_keep_alive_packet_t packet;
			packet.packetType = NETCODE_CONNECTION_KEEP_ALIVE_PACKET;
			packet.clientIndex = 0;
			packet.maxClients = 0;

			netcode_client_send_packet_to_server_internal(client, &packet);
		}
		break;

		default:
			break;
		}
	}

	bool netcode_client_connect_to_next_server(netcode_client_t * client)
	{
		gg_assert(client);

		if (client->serverAddressIndex + 1 >= client->connectToken.numServerAddresses)
		{
			gg_printf(GG_LOG_LEVEL_DEBUG, "Client has no more servers to connect to\n");
			return false;
		}

		client->serverAddressIndex++;
		client->serverAddress = client->connectToken.serverAddresses[client->serverAddressIndex];

		netcode_client_reset_before_next_connect(client);

		c08 serverAddressString[NETCODE_MAX_ADDRESS_STRING_LENGTH];

		gg_printf(GG_LOG_LEVEL_INFO, "Client connecting to next server %s [%d/%d]\n", netcode_address_to_string(&client->serverAddress, serverAddressString),
			client->serverAddressIndex + 1,
			client->connectToken.numServerAddresses);

		netcode_client_set_state(client, NETCODE_CLIENT_STATE_SENDING_CONNECTION_REQUEST);

		return true;
	}

	void netcode_client_update(netcode_client_t * client, f64 time)
	{
		gg_assert(client);

		client->time = time;

		if (client->loopback)
			return;

		netcode_client_receive_packets(client);

		netcode_client_send_packets(client);

		if (client->state > NETCODE_CLIENT_STATE_DISCONNECTED && client->state < NETCODE_CLIENT_STATE_CONNECTED)
		{
			// in the connecting process
			u64 connectTokenExpireSeconds = (client->connectToken.expireTimestamp - client->connectToken.createTimestamp);

			if (client->time - client->connectStartTime >= connectTokenExpireSeconds)
			{
				gg_printf(GG_LOG_LEVEL_INFO, "Client connect failed.  Connect token expired\n");

				netcode_client_disconnect_internal(client, NETCODE_CLIENT_STATE_CONNECT_TOKEN_EXPIRED, false);
				return;
			}
		}

		if (client->shouldDisconnect)
		{
			gg_printf(GG_LOG_LEVEL_DEBUG, "Client should disconnect -> %s\n", netcode_client_state_name(client->shouldDisconnectState).c_str());
			if (netcode_client_connect_to_next_server(client))
				return;

			netcode_client_disconnect_internal(client, client->shouldDisconnectState, false);
			return;
		}

		switch (client->state)
		{
		case NETCODE_CLIENT_STATE_SENDING_CONNECTION_REQUEST:
		{
			if (client->connectToken.timeoutSeconds > 0 && client->lastPacketReceiveTime + client->connectToken.timeoutSeconds < time)
			{
				gg_printf(GG_LOG_LEVEL_INFO, "Client connect failed.  Connection request timed out\n");
				if (netcode_client_connect_to_next_server(client))
					return;
				netcode_client_disconnect_internal(client, NETCODE_CLIENT_STATE_CONNECTION_REQUEST_TIMED_OUT, false);
				return;
			}
		}
		break;
		case NETCODE_CLIENT_STATE_SENDING_CONNECTION_RESPONSE:
		{
			if (client->connectToken.timeoutSeconds > 0 && client->lastPacketReceiveTime + client->connectToken.timeoutSeconds < time)
			{
				gg_printf(GG_LOG_LEVEL_INFO, "Client connect failed.  Connection response timed out\n");
				if (netcode_client_connect_to_next_server(client))
					return;
				netcode_client_disconnect_internal(client, NETCODE_CLIENT_STATE_CONNECTION_RESPONSE_TIMED_OUT, false);
				return;
			}
		}
		break;
		case NETCODE_CLIENT_STATE_CONNECTED:
		{
			if (client->connectToken.timeoutSeconds > 0 && client->lastPacketReceiveTime + client->connectToken.timeoutSeconds < time)
			{
				gg_printf(GG_LOG_LEVEL_INFO, "Client connection timed out\n");
				if (netcode_client_connect_to_next_server(client))
					return;
				netcode_client_disconnect_internal(client, NETCODE_CLIENT_STATE_CONNECTION_TIMED_OUT, false);
				return;
			}
		}
		break;

		default:
			break;
		}
	}

	u64 netcode_client_next_packet_sequence(netcode_client_t * client)
	{
		gg_assert(client);

		return client->sequence;
	}

	void netcode_client_send_packet(netcode_client_t * client, const u08 * packetData, s32 packetBytes)
	{
		gg_assert(client);
		gg_assert(packetData);
		gg_assert(packetBytes >= 0);
		gg_assert(packetBytes <= NETCODE_MAX_PACKET_SIZE);

		if (client->state != NETCODE_CLIENT_STATE_CONNECTED)
			return;

		if (!client->loopback)
		{
			u08 buffer[NETCODE_MAX_PAYLOAD_BYTES * 2];

			netcode_connection_payload_packet_t* packet = RECAST(netcode_connection_payload_packet_t*, buffer);

			packet->packetType = NETCODE_CONNECTION_PAYLOAD_PACKET;
			packet->payloadBytes = packetBytes;

			memcpy(packet->payloadData, packetData, packetBytes);

			netcode_client_send_packet_to_server_internal(client, packet);
		}
		else
		{
			client->config.send_loopback_packet_callback(client->config.callbackContext, client->clientIndex, packetData, packetBytes, client->sequence++);
		}
	}

	u08* netcode_client_receive_packet(netcode_client_t * client, s32 * packetBytes, u64 * packetSequence)
	{
		gg_assert(client);
		gg_assert(packetBytes);

		netcode_connection_payload_packet_t* packet = SCAST(netcode_connection_payload_packet_t*,
			netcode_packet_queue_pop(&client->packetReceiveQueue, packetSequence));

		if (packet)
		{
			gg_assert(packet->packetType == NETCODE_CONNECTION_PAYLOAD_PACKET);

			*packetBytes = packet->payloadBytes;
			gg_assert(*packetBytes >= 0);
			gg_assert(*packetBytes <= NETCODE_MAX_PACKET_BYTES);

			return (u08*)& packet->payloadData;
		}
		else
		{
			return nullptr;
		}
	}

	void netcode_client_free_packet(netcode_client_t * client, u08 * packet)
	{
		gg_assert(client);
		gg_assert(packet);

		s32 offset = offsetof(netcode_connection_payload_packet_t, payloadData);

		client->config.free_function(client->config.allocatorContext, packet - offset);
	}

	void netcode_client_disconnect(netcode_client_t * client)
	{
		gg_assert(client);
		gg_assert(!client->loopback);
		netcode_client_disconnect_internal(client, NETCODE_CLIENT_STATE_DISCONNECTED, true);
	}

	void netcode_client_disconnect_internal(netcode_client_t * client, s32 destinationState, bool sendDisconnectPackets)
	{
		gg_assert(!client->loopback);
		gg_assert(destinationState <= NETCODE_CLIENT_STATE_DISCONNECTED);

		if (client->state <= NETCODE_CLIENT_STATE_DISCONNECTED || client->state == destinationState)
			return;

		gg_printf(GG_LOG_LEVEL_INFO, "Client disconnected\n");

		if (!client->loopback && sendDisconnectPackets && client->state > NETCODE_CLIENT_STATE_DISCONNECTED)
		{
			gg_printf(GG_LOG_LEVEL_DEBUG, "Client sent disconnect packets to server\n");

			for (s32 i = 0; i < NETCODE_NUM_DISCONNECT_PACKETS; ++i)
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Client sent disconnect packet %d\n", i);

				netcode_connection_disconnect_packet_t packet;
				packet.packetType = NETCODE_CONNECTION_DISCONNECT_PACKET;

				netcode_client_send_packet_to_server_internal(client, &packet);
			}
		}

		netcode_client_reset_connection_data(client, destinationState);
	}

	s32 netcode_client_state(netcode_client_t * client)
	{
		gg_assert(client);
		return client->state;
	}
	s32 netcode_client_index(netcode_client_t * client)
	{
		gg_assert(client);
		return client->clientIndex;
	}
	s32 netcode_client_max_clients(netcode_client_t * client)
	{
		gg_assert(client);
		return client->maxClients;
	}

	void netcode_client_connect_loopback(netcode_client_t * client, s32 clientIndex, s32 maxClients)
	{
		gg_assert(client);
		gg_assert(client->state <= NETCODE_CLIENT_STATE_DISCONNECTED);
		gg_printf(GG_LOG_LEVEL_INFO, "Client connected to server via loopback as client %d", clientIndex);

		client->state = NETCODE_CLIENT_STATE_CONNECTED;
		client->clientIndex = clientIndex;
		client->maxClients = maxClients;
		client->loopback = true;
	}


	void netcode_client_disconnect_loopback(netcode_client_t * client)
	{
		gg_assert(client);
		gg_assert(client->loopback);
		netcode_client_reset_connection_data(client, NETCODE_CLIENT_STATE_DISCONNECTED);
	}

	bool netcode_client_loopback(netcode_client_t * client)
	{
		gg_assert(client);
		return client->loopback;
	}

	void netcode_client_process_loopback_packet(netcode_client_t * client, const u08 * packetData, s32 packetBytes, u64 packetSequence)
	{
		gg_assert(client);
		gg_assert(client->loopback);

		netcode_connection_payload_packet_t* packet = netcode_create_payload_packet(packetBytes, client->config.allocatorContext, client->config.allocate_function);

		if (!packet)
			return;

		memcpy(packet->payloadData, packetData, packetBytes);
		gg_printf(GG_LOG_LEVEL_DEBUG, "Client processing loopback packet from server\n");

		netcode_packet_queue_push(&client->packetReceiveQueue, packet, packetSequence);
	}
	u16 netcode_client_get_port(netcode_client_t * client)
	{
		gg_assert(client);
		return client->address.type == NETCODE_ADDRESS_IPV4 ? client->socketHolder.ipv4.address.port
			: client->socketHolder.ipv6.address.port;
	}



	/*

		Encryption manager

	*/

	void netcode_encryption_manager_reset(netcode_encryption_manager_t * encryptionManager)
	{
		gg_assert(encryptionManager);

		gg_printf(GG_LOG_LEVEL_DEBUG, "Reset encryption manager\n");

		encryptionManager->numEncryptionMappings = 0;

		for (s32 i = 0; i < NETCODE_MAX_ENCRYPTION_MAPPINGS; ++i)
		{
			encryptionManager->expireTime[i] = -1.0;
			encryptionManager->lastAccessTime[i] = -1000.0;
			memset(&encryptionManager->address[i], 0, sizeof(netcode_address_t));
		}

		memset(encryptionManager->timeout, 0, sizeof(encryptionManager->timeout));
		memset(encryptionManager->sendKey, 0, sizeof(encryptionManager->sendKey));
		memset(encryptionManager->receiveKey, 0, sizeof(encryptionManager->receiveKey));
	}

	bool netcode_encryption_manager_entry_expired(netcode_encryption_manager_t * encryptionManager, s32 index, f64 time)
	{
		return (encryptionManager->timeout[index] > 0 && (encryptionManager->lastAccessTime[index] + encryptionManager->timeout[index]) < time) ||
			(encryptionManager->expireTime[index] >= 0.0 && encryptionManager->expireTime[index] < time);
	}

	bool netcode_encryption_manager_add_encryption_mapping(netcode_encryption_manager_t * encryptionManager, netcode_address_t * address, u08 * sendKey, u08 * receiveKey, f64 time, f64 expireTime, s32 timeout)
	{
		for (s32 i = 0; i < encryptionManager->numEncryptionMappings; ++i)
		{
			if (netcode_address_equal(&encryptionManager->address[i], address) &&
				!netcode_encryption_manager_entry_expired(encryptionManager, i, time))
			{
				encryptionManager->timeout[i] = timeout;
				encryptionManager->expireTime[i] = expireTime;
				encryptionManager->lastAccessTime[i] = time;
				memcpy(encryptionManager->sendKey + i * NETCODE_KEY_BYTES, sendKey, NETCODE_KEY_BYTES);
				memcpy(encryptionManager->receiveKey + i * NETCODE_KEY_BYTES, receiveKey, NETCODE_KEY_BYTES);
				return true;
			}
		}

		for (s32 i = 0; i < NETCODE_MAX_ENCRYPTION_MAPPINGS; ++i)
		{
			if (encryptionManager->address[i].type == NETCODE_ADDRESS_NONE ||
				netcode_encryption_manager_entry_expired(encryptionManager, i, time))
			{
				encryptionManager->timeout[i] = timeout;
				encryptionManager->address[i] = *address;
				encryptionManager->expireTime[i] = expireTime;
				encryptionManager->lastAccessTime[i] = time;
				memcpy(encryptionManager->sendKey + i * NETCODE_KEY_BYTES, sendKey, NETCODE_KEY_BYTES);
				memcpy(encryptionManager->receiveKey + i * NETCODE_KEY_BYTES, receiveKey, NETCODE_KEY_BYTES);

				if (i + 1 > encryptionManager->numEncryptionMappings)
					encryptionManager->numEncryptionMappings = i + 1;

				return true;
			}
		}

		return false;
	}

	bool netcode_encryption_manager_remove_encryption_mapping(netcode_encryption_manager_t * encryptionManager, netcode_address_t * address, f64 time)
	{
		gg_assert(encryptionManager);
		gg_assert(address);

		for (s32 i = 0; i < encryptionManager->numEncryptionMappings; ++i)
		{
			if (netcode_address_equal(&encryptionManager->address[i], address))
			{
				encryptionManager->expireTime[i] = -1.0;
				encryptionManager->lastAccessTime[i] = -1000.0;
				memset(&encryptionManager->address[i], 0, sizeof(netcode_address_t));
				memset(encryptionManager->sendKey + i * NETCODE_KEY_BYTES, 0, NETCODE_KEY_BYTES);
				memset(encryptionManager->receiveKey + i * NETCODE_KEY_BYTES, 0, NETCODE_KEY_BYTES);

				if (i + 1 == encryptionManager->numEncryptionMappings)
				{
					s32 index = i - 1;

					while (index >= 0)
					{
						if (!netcode_encryption_manager_entry_expired(encryptionManager, index, time))
							break;

						encryptionManager->address[index].type = NETCODE_ADDRESS_NONE;
						--index;
					}
					encryptionManager->numEncryptionMappings = index + 1;
				}

				return true;
			}
		}

		return false;
	}

	s32 netcode_encryption_manager_find_encryption_mapping(netcode_encryption_manager_t * encryptionManager, netcode_address_t * address, f64 time)
	{
		for (s32 i = 0; i < encryptionManager->numEncryptionMappings; ++i)
		{
			if (netcode_address_equal(&encryptionManager->address[i], address) &&
				!netcode_encryption_manager_entry_expired(encryptionManager, i, time))
			{
				encryptionManager->lastAccessTime[i] = time;
				return i;
			}
		}

		return -1;
	}

	s32 netcode_encryption_manager_touch(netcode_encryption_manager_t * encryptionManager, s32 index, netcode_address_t * address, f64 time)
	{
		gg_assert(encryptionManager);
		gg_assert(index >= 0);
		gg_assert(index < encryptionManager->numEncryptionMappings);

		if (!netcode_address_equal(&encryptionManager->address[index], address))
			return false;

		encryptionManager->lastAccessTime[index] = time;
		return true;
	}

	void netcode_encryption_manager_set_expire_time(netcode_encryption_manager_t * encryptionManager, s32 index, f64 expireTime)
	{
		gg_assert(encryptionManager);
		gg_assert(index >= 0);
		gg_assert(index < encryptionManager->numEncryptionMappings);

		encryptionManager->expireTime[index] = expireTime;
	}

	u08* netcode_encryption_manager_get_send_key(netcode_encryption_manager_t * encryptionManager, s32 index)
	{
		gg_assert(encryptionManager);
		if (index == -1)
			return nullptr;

		gg_assert(index >= 0);
		gg_assert(index < encryptionManager->numEncryptionMappings);
		return encryptionManager->sendKey + index * NETCODE_KEY_BYTES;
	}

	u08* netcode_encryption_manager_get_receive_key(netcode_encryption_manager_t * encryptionManager, s32 index)
	{
		gg_assert(encryptionManager);
		if (index == -1)
			return nullptr;

		gg_assert(index >= 0);
		gg_assert(index < encryptionManager->numEncryptionMappings);
		return encryptionManager->receiveKey + index * NETCODE_KEY_BYTES;
	}

	s32 netcode_encryption_manager_get_timeout(netcode_encryption_manager_t * encryptionManager, s32 index)
	{
		gg_assert(encryptionManager);
		if (index == -1)
			return 0;

		gg_assert(index >= 0);
		gg_assert(index < encryptionManager->numEncryptionMappings);
		return encryptionManager->timeout[index];
	}

	/*

		connect token entry

	*/

	void netcode_connect_token_entries_reset(netcode_connect_token_entry_t * connectTokenEntries)
	{
		for (s32 i = 0; i < NETCODE_MAX_CONNECT_TOKEN_ENTRIES; ++i)
		{
			connectTokenEntries[i].time = -1000.0;
			memset(connectTokenEntries[i].mac, 0, NETCODE_MAC_BYTES);
			memset(&connectTokenEntries[i].address, 0, sizeof(netcode_address_t));
		}
	}

	bool netcode_connect_token_entries_find_or_add(netcode_connect_token_entry_t * connectTokenEntries, netcode_address_t * address, u08 * mac, f64 time)
	{
		gg_assert(connectTokenEntries);
		gg_assert(address);
		gg_assert(mac);

		// find the matching entry for the token mac and the oldest entry token.  Constant time worst case.  This is intentional
		s32 matchingTokenIndex = -1;
		s32 oldestTokenIndex = -1;
		f64 oldestTokenTime = 0.0;

		for (s32 i = 0; i < NETCODE_MAX_CONNECT_TOKEN_ENTRIES; ++i)
		{
			if (memcmp(mac, connectTokenEntries[i].mac, NETCODE_MAC_BYTES) == 0)
			{
				matchingTokenIndex = i;
			}

			if (oldestTokenIndex == -1 || connectTokenEntries[i].time < oldestTokenTime)
			{
				oldestTokenTime = connectTokenEntries[i].time;
				oldestTokenIndex = i;
			}
		}

		// if no entry is found with the max, this is a new connect token.  replace the oldest token entry
		gg_assert(oldestTokenIndex != -1);

		if (matchingTokenIndex == -1)
		{
			connectTokenEntries[oldestTokenIndex].time = time;
			connectTokenEntries[oldestTokenIndex].address = *address;
			memcpy(connectTokenEntries[oldestTokenIndex].mac, mac, NETCODE_MAC_BYTES);
			return true;
		}

		// allow connect tokens we have already seen from the same address

		gg_assert(matchingTokenIndex >= 0);
		gg_assert(matchingTokenIndex < NETCODE_MAX_CONNECT_TOKEN_ENTRIES);

		if (netcode_address_equal(&connectTokenEntries[matchingTokenIndex].address, address))
			return true;


		return false;
	}


	/*
		General netcode stuff

	*/

	s32 netcode_init()
	{
		gg_assert(!initialized);

#if GG_PLATFORM == GG_PLATFORM_WINDOWS
		WSADATA wsaData;
		if (WSAStartup(MAKEWORD(2, 2), &wsaData) != NO_ERROR)
			return NETCODE_ERROR;
#endif
		if (sodium_init() == -1)
			return NETCODE_ERROR;

		initialized = true;

		return NETCODE_OK;
	}
	void netcode_term()
	{
		gg_assert(initialized);
#if GG_PLATFORM == GG_PLATFORM_WINDOWS
		WSACleanup();
#endif
		initialized = false;
	}


	s32 netcode_parse_address(const c08 * address_string_in, netcode_address_t * address)
	{
		gg_assert(address_string_in);
		gg_assert(address);

		memset(address, 0, sizeof(netcode_address_t));

#if NETCODE_NETWORK_NEXT
		if (address_string_in[0] == 'f' &&
			address_string_in[1] == 'l' &&
			address_string_in[2] == 'o' &&
			address_string_in[3] == 'w' &&
			address_string_in[4] == ':')
		{
			char* end;
			errno = 0;
			u64 flow_id = strtoull(&address_string_in[5], &end, 16);
			if (errno == 0)
			{
				address->type = NETCODE_ADDRESS_NEXT;
				address->data.flow_id = flow_id;
				return NETCODE_OK;
			}
		}
#endif

		// first try to parse the string as an IPv6 address:
		// 1. if first character is '[' then it's probably an ipv6 in form "[addr6]:portnum"
		// 2. otherwise try to parse as a raw IPv6 address using inet_pton
#define NETCODE_ADDRESS_BUFFER_SAFETY 32
		c08 buffer[NETCODE_MAX_ADDRESS_STRING_LENGTH + NETCODE_ADDRESS_BUFFER_SAFETY * 2];
		c08* address_string = buffer + NETCODE_ADDRESS_BUFFER_SAFETY;

		strncpy(address_string, address_string_in, NETCODE_MAX_ADDRESS_STRING_LENGTH - 1);

		s32 address_string_length = SCAST(s32, strlen(address_string));

		if (address_string[0] == '[')
		{
			s32 baseIndex = address_string_length - 1;

			for (s32 i = 0; i < 6; ++i)// only need to search a max of 6 characters since :65535 is the longest possible port value
			{
				s32 index = baseIndex - i;

				if (index < 3)
					return NETCODE_ERROR;

				if (address_string[index] == ':')
				{
					address->port = SCAST(u16, atoi(&address_string[index + 1]));
					address_string[index - 1] = '\0';
				}
			}
			address_string += 1;
		}

		in6_addr sockaddr6;
		if (inet_pton(AF_INET6, address_string, &sockaddr6) == 1)
		{
			address->type = NETCODE_ADDRESS_IPV6;
			for (s32 i = 0; i < 8; ++i)
			{
				address->data.ipv6[i] = ntohs(((u16*)& sockaddr6)[i]);
			}
			return NETCODE_OK;
		}


		// probably an IPv4 address
		// 1. look for ":portnum", if found save the portnum and strip it out
		// 2. parse remaining ipv4 address via inet_pton
		address_string_length = SCAST(s32, strlen(address_string));
		s32 baseIndex = address_string_length - 1;
		for (s32 i = 0; i < 6; ++i)
		{
			s32 index = baseIndex - i;
			if (index < 0)
				break;
			if (address_string[index] == ':')
			{
				address->port = SCAST(u16, atoi(&address_string[index + 1]));
				address_string[index] = '\0';
			}
		}

		sockaddr_in sockaddr4;
		if (inet_pton(AF_INET, address_string, &sockaddr4.sin_addr) == 1)
		{
			address->type = NETCODE_ADDRESS_IPV4;
			address->data.ipv4[3] = SCAST(u08, ((sockaddr4.sin_addr.s_addr & 0xFF000000) >> 24));
			address->data.ipv4[2] = SCAST(u08, ((sockaddr4.sin_addr.s_addr & 0x00FF0000) >> 16));
			address->data.ipv4[1] = SCAST(u08, ((sockaddr4.sin_addr.s_addr & 0x0000FF00) >> 8));
			address->data.ipv4[0] = SCAST(u08, ((sockaddr4.sin_addr.s_addr & 0x000000FF)));
			return NETCODE_OK;
		}

		return NETCODE_ERROR;
	}

	c08* netcode_address_to_string(netcode_address_t * address, c08 * buffer)
	{
		gg_assert(address);
		gg_assert(buffer);

		if (address->type == NETCODE_ADDRESS_IPV6)
		{
			if (address->port == 0)
			{
				u16 ipv6NetworkOrder[8];
				for (s32 i = 0; i < 8; ++i)
				{
					ipv6NetworkOrder[i] = htons(address->data.ipv6[i]);
				}
				inet_ntop(AF_INET6, SCAST(void*, ipv6NetworkOrder), buffer, NETCODE_MAX_ADDRESS_STRING_LENGTH);
				return buffer;
			}
			else
			{
				c08 addressString[INET6_ADDRSTRLEN];
				u16 ipv6NetworkOrder[8];
				for (s32 i = 0; i < 8; ++i)
				{
					ipv6NetworkOrder[i] = htons(address->data.ipv6[i]);
				}
				inet_ntop(AF_INET6, SCAST(void*, ipv6NetworkOrder), addressString, INET6_ADDRSTRLEN);
				snprintf(buffer, NETCODE_MAX_ADDRESS_STRING_LENGTH, "[%s]:%d", addressString, address->port);
				return buffer;
			}
		}
		else if (address->type == NETCODE_ADDRESS_IPV4)
		{
			if (address->port != 0)
			{
				snprintf(buffer, NETCODE_MAX_ADDRESS_STRING_LENGTH, "%d.%d.%d.%d:%d",
					address->data.ipv4[0],
					address->data.ipv4[1],
					address->data.ipv4[2],
					address->data.ipv4[3], address->port);
			}
			else
			{
				snprintf(buffer, NETCODE_MAX_ADDRESS_STRING_LENGTH, "%d.%d.%d.%d",
					address->data.ipv4[0],
					address->data.ipv4[1],
					address->data.ipv4[2],
					address->data.ipv4[3]);
			}
			return buffer;
		}
#if NETCODE_NETWORK_NEXT
		else if (address->type == NETCODE_ADDRESS_NEXT)
		{
			snprintf(buffer, NETCODE_MAX_ADDRESS_STRING_LENGTH, "flow:%.16" PRIx64, address->data.flow_id);
			return buffer;
		}
#endif
		else
		{
			snprintf(buffer, NETCODE_MAX_ADDRESS_STRING_LENGTH, "%s", "NONE");
			return buffer;
		}
	}

	bool netcode_address_equal(netcode_address_t * a, netcode_address_t * b)
	{
		gg_assert(a);
		gg_assert(b);

		if (a->type != b->type)
			return false;

#if NETCODE_NETWORK_NEXT
		if (a->type == NETCODE_ADDRESS_NEXT)
		{
			return a->data.flow_id == b->data.flow_id;
		}
#endif

		if (a->port != b->port)
			return false;

		if (a->type == NETCODE_ADDRESS_IPV4)
		{
			for (s32 i = 0; i < 4; ++i)
			{
				if (a->data.ipv4[i] != b->data.ipv4[i])
					return false;
			}
		}
		else if (a->type == NETCODE_ADDRESS_IPV6)
		{
			for (s32 i = 0; i < 8; ++i)
			{
				if (a->data.ipv6[i] != b->data.ipv6[i])
					return false;
			}
		}
		else
		{
			return false;
		}

		return true;
	}


	/*

		netcode Server

	*/
#define NETCODE_SERVER_FLAG_IGNORE_CONNECTION_REQUEST_PACKETS 1
#define NETCODE_SERVER_FLAG_IGNORE_CONNECTION_RESPONSE_PACKETS (1<<1)

	void netcode_default_server_config(netcode_server_config_t * config)
	{
		gg_assert(config);
		config->allocatorContext = nullptr;
		config->allocate_function = netcode_default_allocate_function;
		config->free_function = netcode_default_free_function;
		config->networkSimulator = nullptr;
		config->callbackContext = nullptr;
		config->connect_disconnect_callback = nullptr;
		config->send_loopback_packet_callback = nullptr;
		config->overrideSendAndReceive = 0;
		config->send_packet_override = nullptr;
		config->receive_packet_override = nullptr;
	}


	struct netcode_server_t
	{
		netcode_server_config_t config;
		netcode_socket_holder_t socketHolder;
		netcode_address_t address;
		u32 flags;
		f64 time;
		bool running;
		s32 maxClients;
		s32 numConnectedClients;
		u64 globalSequence;
		u64 challengeSequence;
		u08 challengeKey[NETCODE_KEY_BYTES];
		s32 clientConnected[NETCODE_MAX_CLIENTS];
		s32 clientTimeout[NETCODE_MAX_CLIENTS];
		s32 clientLoopback[NETCODE_MAX_CLIENTS];
		s32 clientConfirmed[NETCODE_MAX_CLIENTS];
		s32 clientEncryptionIndex[NETCODE_MAX_CLIENTS];

		u64 clientId[NETCODE_MAX_CLIENTS];
		u64 clientSequence[NETCODE_MAX_CLIENTS];

		f64 clientLastPacketSendTime[NETCODE_MAX_CLIENTS];
		f64 clientLastPacketReceiveTime[NETCODE_MAX_CLIENTS];
		u08 clientUserData[NETCODE_MAX_CLIENTS][NETCODE_USER_DATA_BYTES];

		netcode_replay_protection_t clientReplayProtection[NETCODE_MAX_CLIENTS];
		netcode_packet_queue_t clientPacketQueue[NETCODE_MAX_CLIENTS];
		netcode_address_t clientAddress[NETCODE_MAX_CLIENTS];
		netcode_connect_token_entry_t connectTokenEntries[NETCODE_MAX_CONNECT_TOKEN_ENTRIES];

		netcode_encryption_manager_t encryptionManager;

		u08* receivePacketData[NETCODE_SERVER_MAX_RECEIVE_PACKETS];

		s32 receivePacketBytes[NETCODE_SERVER_MAX_RECEIVE_PACKETS];

		netcode_address_t receiveFrom[NETCODE_SERVER_MAX_RECEIVE_PACKETS];
	};



	bool netcode_server_socket_create(netcode_socket_t * socket, netcode_address_t * address, s32 sendBufferSize, s32 receiveBufferSize, const netcode_server_config_t * config)
	{
		gg_assert(socket);
		gg_assert(address);
		gg_assert(config);

		if (!config->networkSimulator)
		{
			if (!config->overrideSendAndReceive)
			{
				if (netcode_socket_create(socket, address, sendBufferSize, receiveBufferSize) != NETCODE_SOCKET_ERROR_NONE)
				{
					return false;
				}
			}
		}

		return true;
	}

	netcode_server_t* netcode_server_create_overload(const c08 * serverAddressString1, const c08 * serverAddressString2, const netcode_server_config_t * config, f64 time)
	{
		gg_assert(config);
		gg_assert(initialized);

		netcode_address_t serverAddress1;
		netcode_address_t serverAddress2;

		memset(&serverAddress1, 0, sizeof(serverAddress1));
		memset(&serverAddress2, 0, sizeof(serverAddress2));

		if (netcode_parse_address(serverAddressString1, &serverAddress1) != NETCODE_OK)
		{
			gg_printf(GG_LOG_LEVEL_ERROR, "Error: Failed to parse server public address\n");
			return nullptr;
		}

		if (serverAddressString2 != nullptr && netcode_parse_address(serverAddressString2, &serverAddress2) != NETCODE_OK)
		{
			gg_printf(GG_LOG_LEVEL_ERROR, "Error: Failed to parse server public address2\n");
			return nullptr;
		}


		netcode_address_t bindAddressIpv4;
		netcode_address_t bindAddressIpv6;
		netcode_socket_t socketIpv4;
		netcode_socket_t socketIpv6;

		memset(&bindAddressIpv4, 0, sizeof(bindAddressIpv4));
		memset(&bindAddressIpv6, 0, sizeof(bindAddressIpv6));
		memset(&socketIpv4, 0, sizeof(socketIpv4));
		memset(&socketIpv6, 0, sizeof(socketIpv6));


		if (serverAddress1.type == NETCODE_ADDRESS_IPV4 || serverAddress2.type == NETCODE_ADDRESS_IPV4)
		{
			bindAddressIpv4.type = NETCODE_ADDRESS_IPV4;
			bindAddressIpv4.port = serverAddress1.type == NETCODE_ADDRESS_IPV4 ? serverAddress1.port : serverAddress2.port;

			if (!netcode_server_socket_create(&socketIpv4, &bindAddressIpv4, NETCODE_SERVER_SOCKET_SNDBUF_SIZE, NETCODE_SERVER_SOCKET_RCVBUF_SIZE, config))
			{
				return nullptr;
			}
		}

		if (serverAddress1.type == NETCODE_ADDRESS_IPV6 || serverAddress2.type == NETCODE_ADDRESS_IPV6)
		{
			bindAddressIpv6.type = NETCODE_ADDRESS_IPV6;
			bindAddressIpv6.port = serverAddress1.type == NETCODE_ADDRESS_IPV6 ? serverAddress1.port : serverAddress2.port;

			if (!netcode_server_socket_create(&socketIpv6, &bindAddressIpv6, NETCODE_SERVER_SOCKET_SNDBUF_SIZE, NETCODE_SERVER_SOCKET_RCVBUF_SIZE, config))
			{
				return nullptr;
			}
		}

		netcode_server_t* server = SCAST(netcode_server_t*, config->allocate_function(config->allocatorContext, sizeof(netcode_server_t)));

		if (!server)
		{
			netcode_socket_destroy(&socketIpv4);
			netcode_socket_destroy(&socketIpv6);
			return nullptr;
		}

		if (!config->networkSimulator)
		{
			gg_printf(GG_LOG_LEVEL_INFO, "Server listening on: %s\n", serverAddressString1);
		}
		else
		{
			gg_printf(GG_LOG_LEVEL_INFO, "Server listening on: %s (network simulator)\n", serverAddressString1);
		}

		// TODO 
		TODO("probably std::move config to server config");
		server->config = *config;
		server->socketHolder.ipv4 = socketIpv4;
		server->socketHolder.ipv6 = socketIpv6;
		server->address = serverAddress1;
		server->flags = 0;
		server->time = time;
		server->running = false;
		server->maxClients = 0;
		server->numConnectedClients = 0;
		server->globalSequence = 1ULL << 63;

		memset(server->clientConnected, 0, sizeof(server->clientConnected));
		memset(server->clientLoopback, 0, sizeof(server->clientLoopback));
		memset(server->clientConfirmed, 0, sizeof(server->clientConfirmed));
		memset(server->clientId, 0, sizeof(server->clientId));
		memset(server->clientSequence, 0, sizeof(server->clientSequence));
		memset(server->clientLastPacketSendTime, 0, sizeof(server->clientLastPacketSendTime));
		memset(server->clientLastPacketReceiveTime, 0, sizeof(server->clientLastPacketReceiveTime));
		memset(server->clientAddress, 0, sizeof(server->clientAddress));
		memset(server->clientUserData, 0, sizeof(server->clientUserData));

		for (s32 i = 0; i < NETCODE_MAX_CLIENTS; ++i)
		{
			server->clientEncryptionIndex[i] = -1;
			netcode_replay_protection_reset(&server->clientReplayProtection[i]);
		}

		netcode_connect_token_entries_reset(server->connectTokenEntries);
		netcode_encryption_manager_reset(&server->encryptionManager);

		memset(&server->clientPacketQueue, 0, sizeof(server->clientPacketQueue));

		return server;
	}

	netcode_server_t* netcode_server_create(const c08 * serverAddress, const netcode_server_config_t * config, f64 time)
	{
		return netcode_server_create_overload(serverAddress, nullptr, config, time);
	}

	void netcode_server_stop(netcode_server_t * server);

	void netcode_server_destroy(netcode_server_t * server)
	{
		gg_assert(server);
		netcode_server_stop(server);
		netcode_socket_destroy(&server->socketHolder.ipv4);
		netcode_socket_destroy(&server->socketHolder.ipv6);

		server->config.free_function(server->config.allocatorContext, server);
	}
	void netcode_server_start(netcode_server_t * server, s32 maxClients)
	{
		gg_assert(server);
		gg_assert(maxClients > 0);
		gg_assert(maxClients <= NETCODE_MAX_CLIENTS);

		if (server->running)
			netcode_server_stop(server);

		gg_printf(GG_LOG_LEVEL_INFO, "Server started with %d client slots\n", maxClients);

		server->running = true;
		server->maxClients = maxClients;
		server->numConnectedClients = 0;
		server->challengeSequence = 0;

		netcode_generate_key(server->challengeKey);

		for (s32 i = 0; i < maxClients; ++i)
		{
			netcode_packet_queue_init(&server->clientPacketQueue[i], server->config.allocatorContext, server->config.allocate_function, server->config.free_function);
		}
	}

	void netcode_server_send_global_packet(netcode_server_t * server, void* packet, netcode_address_t * to, u08 * packetKey)
	{
		gg_assert(server);
		gg_assert(packet);
		gg_assert(to);
		gg_assert(packetKey);

		u08 packetData[NETCODE_MAX_PACKET_BYTES];
		//std::array<u08, NETCODE_MAX_PACKET_BYTES> packetData;

		s32 packetBytes = netcode_write_packet(packet, packetData, NETCODE_MAX_PACKET_BYTES, server->globalSequence, packetKey, server->config.protocolId);

		gg_assert(packetBytes <= NETCODE_MAX_PACKET_BYTES);

		if (server->config.networkSimulator)
		{
			netcode_network_simulator_send_packet(server->config.networkSimulator, &server->address, to, packetData, packetBytes);
		}
		else
		{
			if (server->config.overrideSendAndReceive)
			{
				server->config.send_packet_override(server->config.callbackContext, to, packetData, packetBytes);
			}
			else if (to->type == NETCODE_ADDRESS_IPV4)
			{
				netcode_socket_send_packet(&server->socketHolder.ipv4, to, packetData, packetBytes);
			}
			else if (to->type == NETCODE_ADDRESS_IPV6)
			{
				netcode_socket_send_packet(&server->socketHolder.ipv6, to, packetData, packetBytes);
			}
		}

		server->globalSequence++;
	}

	void netcode_server_send_client_packet(netcode_server_t * server, void* packet, s32 clientIndex)
	{
		gg_assert(server);
		gg_assert(packet);
		gg_assert(clientIndex >= 0);
		gg_assert(clientIndex < server->maxClients);
		gg_assert(server->clientConnected[clientIndex]);
		gg_assert(!server->clientLoopback[clientIndex]);

		u08 packetData[NETCODE_MAX_PACKET_BYTES];
		//std::array<u08, NETCODE_MAX_PACKET_BYTES> packetData;

		if (!netcode_encryption_manager_touch(&server->encryptionManager,
			server->clientEncryptionIndex[clientIndex],
			&server->clientAddress[clientIndex],
			server->time))
		{
			gg_printf(GG_LOG_LEVEL_ERROR, "Error: encryption mapping is out of date for client %d\n", clientIndex);
			return;
		}

		u08* packetKey = netcode_encryption_manager_get_send_key(&server->encryptionManager, server->clientEncryptionIndex[clientIndex]);

		s32 packetBytes = netcode_write_packet(packet, packetData, NETCODE_MAX_PACKET_BYTES, server->clientSequence[clientIndex], packetKey, server->config.protocolId);

		gg_assert(packetBytes <= NETCODE_MAX_PACKET_BYTES);

		if (server->config.networkSimulator)
		{
			netcode_network_simulator_send_packet(server->config.networkSimulator, &server->address, &server->clientAddress[clientIndex], packetData, packetBytes);
		}
		else
		{
			if (server->config.overrideSendAndReceive)
			{
				server->config.send_packet_override(server->config.callbackContext, &server->clientAddress[clientIndex], packetData, packetBytes);
			}
			else
			{
				if (server->clientAddress[clientIndex].type == NETCODE_ADDRESS_IPV4)
				{
					netcode_socket_send_packet(&server->socketHolder.ipv4, &server->clientAddress[clientIndex], packetData, packetBytes);
				}
				else if (server->clientAddress[clientIndex].type == NETCODE_ADDRESS_IPV6)
				{
					netcode_socket_send_packet(&server->socketHolder.ipv6, &server->clientAddress[clientIndex], packetData, packetBytes);
				}
			}
		}

		server->clientSequence[clientIndex]++;

		server->clientLastPacketSendTime[clientIndex] = server->time;
	}

	void netcode_server_disconnect_client_internal(netcode_server_t * server, s32 clientIndex, s32 sendDisconnectPackets)
	{
		gg_assert(server);
		gg_assert(server->running);
		gg_assert(clientIndex >= 0);
		gg_assert(clientIndex < server->maxClients);
		gg_assert(server->clientConnected[clientIndex]);
		gg_assert(!server->clientLoopback[clientIndex]);

		gg_printf(GG_LOG_LEVEL_INFO, "Server disconnected client %d\n", clientIndex);

		if (server->config.connect_disconnect_callback)
			server->config.connect_disconnect_callback(server->config.callbackContext, clientIndex, 0);

		if (sendDisconnectPackets)
		{
			gg_printf(GG_LOG_LEVEL_DEBUG, "Server sent disconnect packets to client %d\n", clientIndex);

			for (s32 i = 0; i < NETCODE_NUM_DISCONNECT_PACKETS; ++i)
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Server sent disconnect packet %d\n", i);

				netcode_connection_disconnect_packet_t packet;
				packet.packetType = NETCODE_CONNECTION_DISCONNECT_PACKET;
				netcode_server_send_client_packet(server, &packet, clientIndex);
			}
		}

		while (1)
		{
			void* packet = netcode_packet_queue_pop(&server->clientPacketQueue[clientIndex], nullptr);

			if (!packet)
				break;

			server->config.free_function(server->config.allocatorContext, packet);
		}


		netcode_packet_queue_clear(&server->clientPacketQueue[clientIndex]);

		netcode_replay_protection_reset(&server->clientReplayProtection[clientIndex]);

		netcode_encryption_manager_remove_encryption_mapping(&server->encryptionManager, &server->clientAddress[clientIndex], server->time);

		server->clientConnected[clientIndex] = 0;
		server->clientConfirmed[clientIndex] = 0;
		server->clientId[clientIndex] = 0;
		server->clientSequence[clientIndex] = 0;

		server->clientLastPacketSendTime[clientIndex] = 0.0;
		server->clientLastPacketReceiveTime[clientIndex] = 0.0;

		memset(&server->clientAddress[clientIndex], 0, sizeof(netcode_address_t));
		server->clientEncryptionIndex[clientIndex] = -1;
		memset(server->clientUserData[clientIndex], 0, NETCODE_USER_DATA_BYTES);

		server->numConnectedClients--;

		gg_assert(server->numConnectedClients >= 0);
	}

	void netcode_server_disconnect_client(netcode_server_t * server, s32 clientIndex)
	{
		gg_assert(server);

		if (!server->running)
			return;

		gg_assert(clientIndex >= 0);
		gg_assert(clientIndex < server->maxClients);
		gg_assert(server->clientLoopback[clientIndex] == 0);

		if (!server->clientConnected[clientIndex])
			return;

		if (server->clientLoopback[clientIndex])
			return;

		netcode_server_disconnect_client_internal(server, clientIndex, 1);
	}

	void netcode_server_disconnect_all_clients(netcode_server_t * server)
	{
		gg_assert(server);

		if (!server->running)
			return;

		for (s32 i = 0; i < server->maxClients; ++i)
		{
			if (server->clientConnected[i] && !server->clientLoopback[i])
				netcode_server_disconnect_client_internal(server, i, 1);
		}
	}


	void netcode_server_stop(netcode_server_t * server)
	{
		gg_assert(server);

		if (!server->running)
			return;

		netcode_server_disconnect_all_clients(server);

		server->running = false;
		server->maxClients = 0;
		server->numConnectedClients = 0;
		server->globalSequence = 0;
		server->challengeSequence = 0;
		memset(server->challengeKey, 0, NETCODE_KEY_BYTES);


		netcode_connect_token_entries_reset(server->connectTokenEntries);
		netcode_encryption_manager_reset(&server->encryptionManager);

		gg_printf(GG_LOG_LEVEL_INFO, "Server stopped\n");
	}

	s32 netcode_server_find_client_index_by_id(netcode_server_t * server, u64 clientId)
	{
		gg_assert(server);

		for (s32 i = 0; i < server->maxClients; ++i)
		{
			if (server->clientConnected[i] && server->clientId[i] == clientId)
				return i;
		}

		return -1;
	}

	s32 netcode_server_find_client_index_by_address(netcode_server_t * server, netcode_address_t * address)
	{
		gg_assert(server);
		gg_assert(address);

		if (address->type == 0)
			return -1;

		for (s32 i = 0; i < server->maxClients; ++i)
		{
			if (server->clientConnected[i] && netcode_address_equal(&server->clientAddress[i], address))
				return i;
		}

		return -1;
	}


	void netcode_server_process_connection_request_packet(netcode_server_t * server,
		netcode_address_t * from,
		netcode_connection_request_packet_t * packet)
	{
		gg_assert(server);

		(void)from;

		netcode_connect_token_private_t connect_token_private;

		if (netcode_read_connect_token_private(packet->connectTokenData, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES, &connect_token_private) != NETCODE_OK)
		{
			gg_printf(GG_LOG_LEVEL_DEBUG, "Server ignored connection request. FAiled to read connect token\n");
			return;
		}

		s32 foundServerAddress = 0;
		for (s32 i = 0; i < connect_token_private.numServerAddresses; ++i)
		{
			if (netcode_address_equal(&server->address, &connect_token_private.serverAddresses[i]))
			{
				foundServerAddress = 1;
				break;
			}
		}

		if (!foundServerAddress)
		{
			gg_printf(GG_LOG_LEVEL_DEBUG, "Server ignored connection request.  Server address not in connect token whitelist\n");
			return;
		}

		if (netcode_server_find_client_index_by_address(server, from) != -1)
		{
			gg_printf(GG_LOG_LEVEL_DEBUG, "Server ignored connection request.  A client with this address is already connected\n");
			return;
		}

		if (netcode_server_find_client_index_by_id(server, connect_token_private.clientId) != -1)
		{
			gg_printf(GG_LOG_LEVEL_DEBUG, "Server ignored connection request.  A client with this id is already connected\n");
			return;
		}

		if (!netcode_connect_token_entries_find_or_add(server->connectTokenEntries, from, packet->connectTokenData + NETCODE_CONNECT_TOKEN_PRIVATE_BYTES - NETCODE_MAC_BYTES, server->time))
		{
			gg_printf(GG_LOG_LEVEL_DEBUG, "Server ignored connection request.  Connect token has already been used\n");
			return;
		}

		if (server->numConnectedClients == server->maxClients)
		{
			gg_printf(GG_LOG_LEVEL_DEBUG, "Server denied connection request.  Server is full\n");

			netcode_connection_denied_packet_t packet;
			packet.packetType = NETCODE_CONNECTION_DENIED_PACKET;

			netcode_server_send_global_packet(server, &packet, from, connect_token_private.serverToClientKey);
			return;
		}

		f64 expireTime = (connect_token_private.timeoutSeconds >= 0) ? server->time + connect_token_private.timeoutSeconds : -1.0;

		if (!netcode_encryption_manager_add_encryption_mapping(&server->encryptionManager,
			from,
			connect_token_private.serverToClientKey,
			connect_token_private.clientToServerKey,
			server->time,
			expireTime,
			connect_token_private.timeoutSeconds))
		{
			gg_printf(GG_LOG_LEVEL_DEBUG, "Server ignored connection request.  Failed to add encryption mapping\n");
			return;
		}

		netcode_challenge_token_t challengeToken;
		challengeToken.client_id = connect_token_private.clientId;
		memcpy(challengeToken.user_data, connect_token_private.userData, NETCODE_USER_DATA_BYTES);

		netcode_connection_challenge_packet_t challengePacket;
		challengePacket.packetType = NETCODE_CONNECTION_CHALLENGE_PACKET;
		challengePacket.challengeTokenSequence = server->challengeSequence;
		netcode_write_challenge_token(&challengeToken, challengePacket.challengeTokenData, NETCODE_CHALLENGE_TOKEN_BYTES);

		if (netcode_encrypt_challenge_token(challengePacket.challengeTokenData, NETCODE_CHALLENGE_TOKEN_BYTES, server->challengeSequence, server->challengeKey) != NETCODE_OK)
		{
			gg_printf(GG_LOG_LEVEL_DEBUG, "Server ignored connection request.  Failed to encrypt challenge token\n");
			return;
		}

		server->challengeSequence++;

		gg_printf(GG_LOG_LEVEL_DEBUG, "Server sent connection challenge packet\n");

		netcode_server_send_global_packet(server, &challengePacket, from, connect_token_private.serverToClientKey);
	}

	s32 netcode_server_find_free_client_index(netcode_server_t * server)
	{
		gg_assert(server);

		for (s32 i = 0; i < server->maxClients; ++i)
		{
			if (!server->clientConnected[i])
				return i;
		}
		return -1;
	}

	void netcode_server_connect_client(netcode_server_t * server,
		s32 clientIndex,
		netcode_address_t * address,
		u64 clientId,
		s32 encryptionIndex,
		s32 timeoutSeconds,
		void* userData)
	{
		gg_assert(server);
		gg_assert(server->running);
		gg_assert(clientIndex >= 0);
		gg_assert(clientIndex < server->maxClients);
		gg_assert(address);
		gg_assert(encryptionIndex != -1);
		gg_assert(userData);

		server->numConnectedClients++;

		gg_assert(server->numConnectedClients <= server->maxClients);
		gg_assert(server->clientConnected[clientIndex] == 0);

		netcode_encryption_manager_set_expire_time(&server->encryptionManager, encryptionIndex, -1.0);

		server->clientConnected[clientIndex] = 1;
		server->clientTimeout[clientIndex] = timeoutSeconds;
		server->clientEncryptionIndex[clientIndex] = encryptionIndex;
		server->clientId[clientIndex] = clientId;
		server->clientSequence[clientIndex] = 0;
		server->clientAddress[clientIndex] = *address;
		server->clientLastPacketSendTime[clientIndex] = server->time;
		server->clientLastPacketReceiveTime[clientIndex] = server->time;
		memcpy(server->clientUserData[clientIndex], userData, NETCODE_USER_DATA_BYTES);

		c08 addressString[NETCODE_MAX_ADDRESS_STRING_LENGTH];
		gg_printf(GG_LOG_LEVEL_INFO, "Server accepted client %s %.16" PRIx64 " in slot %d\n", netcode_address_to_string(address, addressString), clientId, clientIndex);

		netcode_connection_keep_alive_packet_t packet;
		packet.packetType = NETCODE_CONNECTION_KEEP_ALIVE_PACKET;
		packet.clientIndex = clientIndex;
		packet.maxClients = server->maxClients;

		netcode_server_send_client_packet(server, &packet, clientIndex);

		if (server->config.connect_disconnect_callback)
			server->config.connect_disconnect_callback(server->config.callbackContext, clientIndex, 1);
	}

	void netcode_server_process_connection_response_packet(netcode_server_t * server, netcode_address_t * from,
		netcode_connection_response_packet_t * packet,
		s32 encryptionIndex)
	{
		gg_assert(server);

		if (netcode_decrypt_challenge_token(packet->challengeTokenData, NETCODE_CHALLENGE_TOKEN_BYTES, packet->challengeTokenSequence, server->challengeKey) != NETCODE_OK)
		{
			gg_printf(GG_LOG_LEVEL_DEBUG, "Server ignored connection response.  Failed to decrypt challenge token\n");
			return;
		}

		netcode_challenge_token_t challengeToken;
		if (netcode_read_challenge_token(packet->challengeTokenData, NETCODE_CHALLENGE_TOKEN_BYTES, &challengeToken) != NETCODE_OK)
		{
			gg_printf(GG_LOG_LEVEL_DEBUG, "Server ignored connection response.  Failed to read challenge token\n");
			return;
		}

		u08* packetSendKey = netcode_encryption_manager_get_send_key(&server->encryptionManager, encryptionIndex);

		if (!packetSendKey)
		{
			gg_printf(GG_LOG_LEVEL_DEBUG, "Server ignored connection response.  No packet send key\n");
			return;
		}

		if (netcode_server_find_client_index_by_address(server, from) != -1)
		{
			gg_printf(GG_LOG_LEVEL_DEBUG, "Server ignored connection response.  A client with this address is already connected\n");
			return;
		}
		if (netcode_server_find_client_index_by_id(server, challengeToken.client_id) != -1)
		{
			gg_printf(GG_LOG_LEVEL_DEBUG, "Server ignored connection response.  A client with this id is already connected\n");
			return;
		}

		if (server->numConnectedClients == server->maxClients)
		{
			gg_printf(GG_LOG_LEVEL_DEBUG, "Server denied connection response.  Server is full\n");

			netcode_connection_denied_packet_t p;
			p.packetType = NETCODE_CONNECTION_DENIED_PACKET;

			netcode_server_send_global_packet(server, &p, from, packetSendKey);

			return;
		}

		s32 clientIndex = netcode_server_find_free_client_index(server);

		gg_assert(clientIndex != -1);

		s32 timeoutSeconds = netcode_encryption_manager_get_timeout(&server->encryptionManager, encryptionIndex);

		netcode_server_connect_client(server, clientIndex, from, challengeToken.client_id, encryptionIndex, timeoutSeconds, challengeToken.user_data);
	}

	void netcode_server_process_packet_internal(netcode_server_t * server,
		netcode_address_t * from,
		void* packet,
		u64 sequence,
		s32 encryptionIndex,
		s32 clientIndex)
	{
		gg_assert(server);
		gg_assert(packet);

		(void)from;
		(void)sequence;

		u08 packetType = ((u08*)packet)[0];

		switch (packetType)
		{
		case NETCODE_CONNECTION_REQUEST_PACKET:
		{
			if ((server->flags & NETCODE_SERVER_FLAG_IGNORE_CONNECTION_REQUEST_PACKETS) == 0)
			{
				c08 fromAddressString[NETCODE_MAX_ADDRESS_STRING_LENGTH];
				gg_printf(GG_LOG_LEVEL_DEBUG, "Server received connection request from %s\n", netcode_address_to_string(from, fromAddressString));
				netcode_server_process_connection_request_packet(server, from, (netcode_connection_request_packet_t*)packet);
			}
		}
		break;
		case NETCODE_CONNECTION_RESPONSE_PACKET:
		{

			if ((server->flags & NETCODE_SERVER_FLAG_IGNORE_CONNECTION_RESPONSE_PACKETS) == 0)
			{
				c08 fromAddressString[NETCODE_MAX_ADDRESS_STRING_LENGTH];
				gg_printf(GG_LOG_LEVEL_DEBUG, "Server received connection response from %s\n", netcode_address_to_string(from, fromAddressString));
				netcode_server_process_connection_response_packet(server, from, (netcode_connection_response_packet_t*)packet, encryptionIndex);
			}
		}
		break;
		case NETCODE_CONNECTION_KEEP_ALIVE_PACKET:
		{
			if (clientIndex != -1)
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Server received connection keep alive packet from client %d\n", clientIndex);
				server->clientLastPacketReceiveTime[clientIndex] = server->time;
				if (!server->clientConfirmed[clientIndex])
				{
					gg_printf(GG_LOG_LEVEL_DEBUG, "Server confirmed connection with client %d\n", clientIndex);
					server->clientConfirmed[clientIndex] = 1;
				}
			}
		}
		break;
		case NETCODE_CONNECTION_PAYLOAD_PACKET:
		{
			if (clientIndex != -1)
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Server received connection payload packet from client %d\n", clientIndex);

				server->clientLastPacketReceiveTime[clientIndex] = server->time;
				if (!server->clientConfirmed[clientIndex])
				{
					gg_printf(GG_LOG_LEVEL_DEBUG, "Server confirmed connection with client %d\n", clientIndex);
					server->clientConfirmed[clientIndex] = 1;
				}

				netcode_packet_queue_push(&server->clientPacketQueue[clientIndex], packet, sequence);
				return;
			}
		}
		break;
		case NETCODE_CONNECTION_DISCONNECT_PACKET:
		{
			if (clientIndex != -1)
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Server received disconnect packet from client %d\n", clientIndex);
				netcode_server_disconnect_client_internal(server, clientIndex, 0);
			}
		}
		break;

		default:
			break;
		}

		server->config.free_function(server->config.allocatorContext, packet);
	}


	s32 netcode_get_encryption_index(netcode_server_t * server, netcode_address_t * from, s32 & clientIndex)
	{
		clientIndex = netcode_server_find_client_index_by_address(server, from);

		if (clientIndex != -1)
		{
			gg_assert(clientIndex >= 0);
			gg_assert(clientIndex < server->maxClients);
			return  server->clientEncryptionIndex[clientIndex];
		}
		else
		{
			return netcode_encryption_manager_find_encryption_mapping(&server->encryptionManager, from, server->time);
		}
	}


	void netcode_server_process_packet(netcode_server_t * server, netcode_address_t * from, u08 * packetData, s32 packetBytes)
	{
		u08 allowedPackets[NETCODE_CONNECTION_NUM_PACKETS];
		memset(allowedPackets, 0, sizeof(allowedPackets));
		allowedPackets[NETCODE_CONNECTION_REQUEST_PACKET] = 1;
		allowedPackets[NETCODE_CONNECTION_RESPONSE_PACKET] = 1;
		allowedPackets[NETCODE_CONNECTION_KEEP_ALIVE_PACKET] = 1;
		allowedPackets[NETCODE_CONNECTION_PAYLOAD_PACKET] = 1;
		allowedPackets[NETCODE_CONNECTION_DISCONNECT_PACKET] = 1;

		u64 currentTimestamp = (u64)time(nullptr);

		u64 sequence;
		s32 clientIndex = -1;
		s32 encryptionIndex = netcode_get_encryption_index(server, from, clientIndex);

		u08* readPacketKey = netcode_encryption_manager_get_receive_key(&server->encryptionManager, encryptionIndex);

		if (!readPacketKey && packetData[0] != 0)
		{
			c08 addressString[NETCODE_MAX_ADDRESS_STRING_LENGTH];
			gg_printf(GG_LOG_LEVEL_DEBUG, "Server could not process packet because no encryption mapping exists for %s\n", netcode_address_to_string(from, addressString));
			return;
		}

		void* packet = netcode_read_packet(packetData,
			packetBytes,
			&sequence,
			readPacketKey,
			server->config.protocolId,
			currentTimestamp,
			server->config.privateKey,
			allowedPackets,
			(clientIndex != -1) ? &server->clientReplayProtection[clientIndex] : nullptr,
			server->config.allocatorContext,
			server->config.allocate_function);

		if (!packet)
			return;

		netcode_server_process_packet_internal(server, from, packet, sequence, encryptionIndex, clientIndex);
	}


	void netcode_server_read_and_process_packet(netcode_server_t * server, netcode_address_t * from, u08 * packetData, s32 packetBytes, u64 currentTimestamp, u08 * allowedPackets)
	{
		if (!server->running)
			return;

		if (packetBytes <= 1)
			return;

		u64 sequence;
		s32 encryptionIndex = -1;
		s32 clientIndex = netcode_server_find_client_index_by_address(server, from);

		if (clientIndex != -1)
		{
			gg_assert(clientIndex >= 0);
			gg_assert(clientIndex < server->maxClients);
			encryptionIndex = server->clientEncryptionIndex[clientIndex];
		}
		else
		{
			encryptionIndex = netcode_encryption_manager_find_encryption_mapping(&server->encryptionManager, from, server->time);
		}

		u08* readPacketKey = netcode_encryption_manager_get_receive_key(&server->encryptionManager, encryptionIndex);

		if (!readPacketKey && packetData[0] != 0)
		{
			c08 addressString[NETCODE_MAX_ADDRESS_STRING_LENGTH];
			gg_printf(GG_LOG_LEVEL_DEBUG, "Server could not process packet because no encryption mapping exists for %s\n", netcode_address_to_string(from, addressString));
			return;
		}

		void* packet = netcode_read_packet(packetData,
			packetBytes,
			&sequence,
			readPacketKey,
			server->config.protocolId,
			currentTimestamp,
			server->config.privateKey,
			allowedPackets,
			(clientIndex != -1) ? &server->clientReplayProtection[clientIndex] : nullptr,
			server->config.allocatorContext,
			server->config.allocate_function);

		if (!packet)
			return;

		netcode_server_process_packet_internal(server, from, packet, sequence, encryptionIndex, clientIndex);
	}

	void netcode_server_receive_packets(netcode_server_t * server)
	{
		gg_assert(server);

		u08 allowedPackets[NETCODE_CONNECTION_NUM_PACKETS];
		memset(allowedPackets, 0, sizeof(allowedPackets));
		allowedPackets[NETCODE_CONNECTION_REQUEST_PACKET] = 1;
		allowedPackets[NETCODE_CONNECTION_RESPONSE_PACKET] = 1;
		allowedPackets[NETCODE_CONNECTION_KEEP_ALIVE_PACKET] = 1;
		allowedPackets[NETCODE_CONNECTION_PAYLOAD_PACKET] = 1;
		allowedPackets[NETCODE_CONNECTION_DISCONNECT_PACKET] = 1;


		u64 currentTimestamp = (u64)time(nullptr);

		if (!server->config.networkSimulator)
		{
			// process packets received from socket
			while (1)
			{
				netcode_address_t from;
				u08 packetData[NETCODE_MAX_PACKET_BYTES];
				s32 packetBytes = 0;
				if (server->config.overrideSendAndReceive)
				{
					packetBytes = server->config.receive_packet_override(server->config.callbackContext, &from, packetData, NETCODE_MAX_PACKET_BYTES);
				}
				else
				{
					if (server->socketHolder.ipv4.handle != 0)
						packetBytes = netcode_socket_receive_packet(&server->socketHolder.ipv4, &from, packetData, NETCODE_MAX_PACKET_BYTES);

					if (packetBytes == 0 && server->socketHolder.ipv6.handle != 0)
						packetBytes = netcode_socket_receive_packet(&server->socketHolder.ipv6, &from, packetData, NETCODE_MAX_PACKET_BYTES);
				}

				if (packetBytes == 0)
					break;

				netcode_server_read_and_process_packet(server, &from, packetData, packetBytes, currentTimestamp, allowedPackets);
			}
		}
		else
		{
			// process packets received from network simulator

			s32 numPacketsReceived = netcode_network_simulator_receive_packets(server->config.networkSimulator,
				&server->address,
				NETCODE_SERVER_MAX_RECEIVE_PACKETS,
				server->receivePacketData,
				server->receivePacketBytes,
				server->receiveFrom);

			for (s32 i = 0; i < numPacketsReceived; ++i)
			{
				netcode_server_read_and_process_packet(server,
					&server->receiveFrom[i],
					server->receivePacketData[i],
					server->receivePacketBytes[i],
					currentTimestamp,
					allowedPackets);
				server->config.free_function(server->config.allocatorContext, server->receivePacketData[i]);
			}
		}
	}

	void netcode_server_send_packets(netcode_server_t * server)
	{
		gg_assert(server);
		if (!server->running)
			return;

		for (s32 i = 0; i < server->maxClients; ++i)
		{
			if (server->clientConnected[i] && !server->clientLoopback[i] &&
				(server->clientLastPacketSendTime[i] + (1.0 / NETCODE_PACKET_SEND_RATE) <= server->time))
			{
				gg_printf(GG_LOG_LEVEL_DEBUG, "Server sent connection keep alive packet to client %d\n", i);
				netcode_connection_keep_alive_packet_t packet;
				packet.packetType = NETCODE_CONNECTION_KEEP_ALIVE_PACKET;
				packet.clientIndex = i;
				packet.maxClients = server->maxClients;

				netcode_server_send_client_packet(server, &packet, i);
			}
		}
	}

	void netcode_server_check_for_timeouts(netcode_server_t * server)
	{
		gg_assert(server);

		if (!server->running)
			return;

		for (s32 i = 0; i < server->maxClients; ++i)
		{
			if (server->clientConnected[i] && server->clientTimeout[i] > 0 && !server->clientLoopback[i] &&
				(server->clientLastPacketReceiveTime[i] + server->clientTimeout[i] <= server->time))
			{
				gg_printf(GG_LOG_LEVEL_INFO, "Server timed out client %d\n", i);
				netcode_server_disconnect_client_internal(server, i, 0);
				return;
			}
		}
	}


	s32 netcode_server_client_connected(netcode_server_t * server, s32 clientIndex)
	{
		gg_assert(server);

		if (!server->running)
			return 0;

		if (clientIndex < 0 || clientIndex >= server->maxClients)
			return 0;

		return server->clientConnected[clientIndex];
	}

	u64 netcode_server_client_id(netcode_server_t * server, s32 clientIndex)
	{
		gg_assert(server);

		if (!server->running)
			return 0;

		if (clientIndex < 0 || clientIndex >= server->maxClients)
			return 0;

		return server->clientId[clientIndex];
	}


	bool netcode_server_running(netcode_server_t * server)
	{
		gg_assert(server);
		return server->running;
	}

	s32 netcode_server_max_clients(netcode_server_t * server)
	{
		gg_assert(server);
		return server->maxClients;
	}
	void netcode_server_update(netcode_server_t * server, f64 time)
	{
		gg_assert(server);
		server->time = time;
		netcode_server_receive_packets(server);
		netcode_server_send_packets(server);
		netcode_server_check_for_timeouts(server);
	}

	u64 netcode_server_next_packet_sequence(netcode_server_t * server, s32 clientIndex)
	{
		gg_assert(server);
		gg_assert(clientIndex >= 0);
		gg_assert(clientIndex < server->maxClients);

		if (!server->clientConnected[clientIndex])
			return 0;

		return server->clientSequence[clientIndex];
	}

	void netcode_server_send_packet(netcode_server_t * server, s32 clientIndex, const u08 * packetData, s32 packetBytes)
	{
		gg_assert(server);
		gg_assert(packetData);
		gg_assert(packetBytes >= 0);
		gg_assert(packetBytes <= NETCODE_MAX_PACKET_SIZE);

		if (!server->running)
			return;


		gg_assert(clientIndex >= 0);
		gg_assert(clientIndex < server->maxClients);

		if (!server->clientConnected[clientIndex])
			return;

		if (!server->clientLoopback[clientIndex])
		{
			u08 buffer[NETCODE_MAX_PAYLOAD_BYTES * 2];

			netcode_connection_payload_packet_t* packet = (netcode_connection_payload_packet_t*)buffer;

			packet->packetType = NETCODE_CONNECTION_PAYLOAD_PACKET;
			packet->payloadBytes = packetBytes;
			memcpy(packet->payloadData, packetData, packetBytes);

			if (!server->clientConfirmed[clientIndex])
			{
				netcode_connection_keep_alive_packet_t keepAlivePacket;
				keepAlivePacket.packetType = NETCODE_CONNECTION_KEEP_ALIVE_PACKET;
				keepAlivePacket.clientIndex = clientIndex;
				keepAlivePacket.maxClients = server->maxClients;
				netcode_server_send_client_packet(server, &keepAlivePacket, clientIndex);
			}

			netcode_server_send_client_packet(server, packet, clientIndex);
		}
		else // client loopback on this clientIndex
		{
			gg_assert(server->config.send_loopback_packet_callback);

			server->config.send_loopback_packet_callback(server->config.callbackContext,
				clientIndex,
				packetData,
				packetBytes,
				server->clientSequence[clientIndex]++);

			server->clientLastPacketSendTime[clientIndex] = server->time;
		}

	}

	u08* netcode_server_receive_packet(netcode_server_t * server, s32 clientIndex, s32 * packetBytes, u64 * packetSequence)
	{
		gg_assert(server);
		gg_assert(packetBytes);

		if (!server->running)
			return nullptr;

		gg_assert(clientIndex >= 0);
		gg_assert(clientIndex < server->maxClients);

		if (!server->clientConnected[clientIndex])
			return nullptr;

		netcode_connection_payload_packet_t* packet = (netcode_connection_payload_packet_t*)netcode_packet_queue_pop(&server->clientPacketQueue[clientIndex], packetSequence);

		if (packet)
		{
			gg_assert(packet->packetType == NETCODE_CONNECTION_PAYLOAD_PACKET);
			*packetBytes = packet->payloadBytes;
			gg_assert(*packetBytes >= 0);
			gg_assert(*packetBytes <= NETCODE_MAX_PACKET_BYTES);
			return (u08*)& packet->payloadData;
		}
		else
		{
			return nullptr;
		}
	}

	void netcode_server_free_packet(netcode_server_t * server, void* packet)
	{
		gg_assert(server);
		gg_assert(packet);
		(void)server;
		s32 offset = offsetof(netcode_connection_payload_packet_t, payloadData);
		server->config.free_function(server->config.allocatorContext, ((u08*)packet) - offset);
	}

	s32 netcode_server_num_connected_clients(netcode_server_t * server)
	{
		gg_assert(server);
		return server->numConnectedClients;
	}

	void* netcode_server_client_user_data(netcode_server_t * server, s32 clientIndex)
	{
		gg_assert(server);
		gg_assert(clientIndex >= 0);
		gg_assert(clientIndex < server->maxClients);
		return server->clientUserData[clientIndex];
	}


	//////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void netcode_server_connect_loopback_client(netcode_server_t * server, s32 clientIndex, u64 clientId, const u08 * userData)
	{
		gg_assert(server);
		gg_assert(server->running);
		gg_assert(clientIndex >= 0);
		gg_assert(clientIndex < server->maxClients);
		gg_assert(!server->clientConnected[clientIndex]);

		server->numConnectedClients++;

		gg_assert(server->numConnectedClients <= server->maxClients);

		server->clientLoopback[clientIndex] = 1;
		server->clientConnected[clientIndex] = 1;
		server->clientConfirmed[clientIndex] = 1;
		server->clientEncryptionIndex[clientIndex] = -1;
		server->clientId[clientIndex] = clientId;
		server->clientSequence[clientIndex] = 0;

		memset(&server->clientAddress[clientIndex], 0, sizeof(netcode_address_t));

		server->clientLastPacketSendTime[clientIndex] = server->time;
		server->clientLastPacketReceiveTime[clientIndex] = server->time;

		if (userData)
			memcpy(server->clientUserData[clientIndex], userData, NETCODE_USER_DATA_BYTES);
		else
			memset(server->clientUserData[clientIndex], 0, NETCODE_USER_DATA_BYTES);

		gg_printf(GG_LOG_LEVEL_INFO, "Server connected loopback client %.16" PRIx64 " in slot %d\n", clientId, clientIndex);

		if (server->config.connect_disconnect_callback)
			server->config.connect_disconnect_callback(server->config.callbackContext, clientIndex, 1);
	}

	void netcode_server_disconnect_loopback_client(netcode_server_t * server, s32 clientIndex)
	{
		gg_assert(server);
		gg_assert(server->running);
		gg_assert(clientIndex >= 0);
		gg_assert(clientIndex < server->maxClients);
		gg_assert(server->clientConnected[clientIndex]);
		gg_assert(server->clientLoopback[clientIndex]);

		gg_printf(GG_LOG_LEVEL_INFO, "Server disconnect loopback client %d\n", clientIndex);

		if (server->config.connect_disconnect_callback)
			server->config.connect_disconnect_callback(server->config.callbackContext, clientIndex, 0);

		while (1)
		{
			void* packet = netcode_packet_queue_pop(&server->clientPacketQueue[clientIndex], nullptr);
			if (!packet)
				break;
			server->config.free_function(server->config.allocatorContext, packet);
		}

		netcode_packet_queue_clear(&server->clientPacketQueue[clientIndex]);

		server->clientLoopback[clientIndex] = 0;
		server->clientConnected[clientIndex] = 0;
		server->clientConfirmed[clientIndex] = 0;
		server->clientEncryptionIndex[clientIndex] = -1;
		server->clientId[clientIndex] = 0;
		server->clientSequence[clientIndex] = 0;
		server->clientLastPacketSendTime[clientIndex] = 0.0;
		server->clientLastPacketReceiveTime[clientIndex] = 0.0;

		memset(&server->clientAddress[clientIndex], 0, sizeof(netcode_address_t));
		memset(server->clientUserData[clientIndex], 0, NETCODE_USER_DATA_BYTES);

		server->numConnectedClients--;

		gg_assert(server->numConnectedClients >= 0);
	}

	s32 netcode_server_client_loopback(netcode_server_t * server, s32 clientIndex)
	{
		gg_assert(server);
		gg_assert(server->running);
		gg_assert(clientIndex >= 0);
		gg_assert(clientIndex < server->maxClients);

		return server->clientLoopback[clientIndex];
	}

	void netcode_server_process_loopback_packet(netcode_server_t * server, s32 clientIndex, const u08 * packetData, s32 packetBytes, u64 packetSequence)
	{
		gg_assert(server);
		gg_assert(server->running);
		gg_assert(clientIndex >= 0);
		gg_assert(clientIndex < server->maxClients);
		gg_assert(server->clientConnected[clientIndex]);
		gg_assert(server->clientLoopback[clientIndex]);
		gg_assert(packetData);
		gg_assert(packetBytes >= 0);
		gg_assert(packetBytes <= NETCODE_MAX_PACKET_SIZE);

		netcode_connection_payload_packet_t* packet = netcode_create_payload_packet(packetBytes, server->config.allocatorContext, server->config.allocate_function);

		if (!packet)
			return;

		memcpy(packet->payloadData, packetData, packetBytes);

		gg_printf(GG_LOG_LEVEL_DEBUG, "Server processing loopback packet from client %d\n", clientIndex);

		server->clientLastPacketReceiveTime[clientIndex] = server->time;

		netcode_packet_queue_push(&server->clientPacketQueue[clientIndex], packet, packetSequence);
	}

	u16 netcode_server_get_port(netcode_server_t * server)
	{
		gg_assert(server);
		return server->address.type == NETCODE_ADDRESS_IPV4 ? server->socketHolder.ipv4.address.port : server->socketHolder.ipv6.address.port;
	}


	/////////////////////////////////////////////////////////////////////////////////////////////////////


	s32 netcode_generate_connect_token(s32 numServerAddresses,
		const c08 * *publicServerAddresses,
		const c08 * *internalServerAddresses,
		s32 expireSeconds,
		s32 timeoutSeconds,
		u64 clientId,
		u64 protocolId,
		const u08 * privateKey,
		u08 * userData,
		u08 * outputBuffer)
	{
		gg_assert(numServerAddresses > 0);
		gg_assert(numServerAddresses <= NETCODE_MAX_SERVERS_PER_CONNECT);
		gg_assert(publicServerAddresses);
		gg_assert(internalServerAddresses);
		gg_assert(privateKey);
		gg_assert(userData);
		gg_assert(outputBuffer);

		// parse public and internal server addresses
		netcode_address_t parsedPublicServerAddresses[NETCODE_MAX_SERVERS_PER_CONNECT];
		netcode_address_t parsedInternalServerAddresses[NETCODE_MAX_SERVERS_PER_CONNECT];

		for (s32 i = 0; i < numServerAddresses; ++i)
		{
			if (netcode_parse_address(publicServerAddresses[i], &parsedPublicServerAddresses[i]) != NETCODE_OK)
				return NETCODE_ERROR;
			if (netcode_parse_address(internalServerAddresses[i], &parsedInternalServerAddresses[i]) != NETCODE_OK)
				return NETCODE_ERROR;
		}

		// generate a connect token
		u08 nonce[NETCODE_CONNECT_TOKEN_NONCE_BYTES];
		netcode_generate_nonce(nonce);

		netcode_connect_token_private_t connectTokenPrivate;
		netcode_generate_connect_token_private(&connectTokenPrivate, clientId, timeoutSeconds, numServerAddresses, parsedInternalServerAddresses, userData);

		// write it to a buffer
		u08 connectTokenData[NETCODE_CONNECT_TOKEN_PRIVATE_BYTES];
		netcode_write_connect_token_private(&connectTokenPrivate, connectTokenData, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);

		// encrypt the buffer
		u64 createTimestamp = time(nullptr);
		u64 expireTimestamp = (expireSeconds >= 0) ? (createTimestamp + expireSeconds) : 0xFFFFFFFFFFFFFFFFULL;

		if (netcode_encrypt_connect_token_private(connectTokenData, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES, NETCODE_VERSION_INFO, protocolId, expireTimestamp, nonce, privateKey) != NETCODE_OK)
			return NETCODE_ERROR;

		// wrap a connect token around the private connect token data
		netcode_connect_token_t connectToken;
		memcpy(connectToken.versionInfo, NETCODE_VERSION_INFO, NETCODE_VERSION_INFO_BYTES);
		connectToken.protocolId = protocolId;
		connectToken.createTimestamp = createTimestamp;
		connectToken.expireTimestamp = expireTimestamp;
		memcpy(connectToken.nonce, nonce, NETCODE_CONNECT_TOKEN_NONCE_BYTES);
		memcpy(connectToken.privateData, connectTokenData, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);
		connectToken.numServerAddresses = numServerAddresses;

		for (s32 i = 0; i < numServerAddresses; ++i)
		{
			connectToken.serverAddresses[i] = parsedPublicServerAddresses[i];
		}

		memcpy(connectToken.clientToServerKey, connectTokenPrivate.clientToServerKey, NETCODE_KEY_BYTES);
		memcpy(connectToken.serverToClientKey, connectTokenPrivate.serverToClientKey, NETCODE_KEY_BYTES);
		connectToken.timeoutSeconds = timeoutSeconds;

		// write the connect token to the output buffer
		netcode_write_connect_token(&connectToken, outputBuffer, NETCODE_CONNECT_TOKEN_BYTES);

		return NETCODE_OK;
	}



#if __APPLE__

#include <unistd.h>
#include <mach/mach.h>
#include <mach/mach_time.h>
	void netcode_sleep(f64 seconds)
	{
		usleep((int)(time * 1000000));
	}

	static u64 start = 0;
	static mach_timebase_info_data_t timebase_info;
	f64 netcode_time()
	{
		if (start == 0)
		{
			mach_timebase_info(&timebase_info);
			start = mach_absolute_time();
			return 0.0;
		}
		u64 current = mach_absolute_time();
		return ((f64)(current - start)) * ((f64)timebase_info.numer) / ((f64)timebase_info.denom) / 1000000000.0;
	}

#elif __linux

#include <unistd.h>

	void netcode_sleep(f64 time)
	{
		usleep((s32)(time * 1000000));
	}

	f64 netcode_time()
	{
		static f64 start = -1;
		if (start == -1)
		{
			struct timespec ts;
			clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
			start = ts.tv_sec + ((f64)(ts.tv_nsec)) / 1000000000.0;
			return 0.0;
		}
		struct timespec ts;
		clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
		f64 current = ts.tv_sec + ((f64)(ts.tv_nsec)) / 1000000000.0;
		return current - start;
	}

#elif defined(_WIN32)

#define NOMINMAX
#include <windows.h>

	void netcode_sleep(f64 seconds)
	{
		s32 milliseconds = (s32)(seconds * 1000);
		Sleep(milliseconds);
	}

	static s32 timer_initialized = 0;
	static LARGE_INTEGER timer_frequency;
	static LARGE_INTEGER timer_start;

	f64 netcode_time()
	{
		if (!timer_initialized)
		{
			QueryPerformanceFrequency(&timer_frequency);
			QueryPerformanceCounter(&timer_start);
			timer_initialized = 1;
		}
		LARGE_INTEGER now;
		QueryPerformanceCounter(&now);
		return ((f64)(now.QuadPart - timer_start.QuadPart)) / ((f64)(timer_frequency.QuadPart));
	}
#else
#error unsupported platform

#endif





	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///                                   TESTS
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#if NETCODE_ENABLE_TESTS

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <time.h>

	static void check_handler(const c08 * condition,
		const c08 * function,
		const c08 * file,
		s32 line)
	{
		printf("check failed: ( %s ), function %s, file %s, line %d\n", condition, function, file, line);
#ifndef NDEBUG
#if defined( __GNUC__ )
		__builtin_trap();
#elif defined( _MSC_VER )
		__debugbreak();
#endif
#endif
		exit(1);
	}

#define check( condition )                                                                  \
do                                                                                          \
{                                                                                           \
    if ( !(condition) )                                                                     \
    {                                                                                       \
        check_handler( #condition, (const c08*) __FUNCTION__, (c08*) __FILE__, __LINE__ );  \
    }                                                                                       \
} while(0)



	static void test_queue()
	{
		struct netcode_packet_queue_t queue;

		netcode_packet_queue_init(&queue, nullptr, nullptr, nullptr);

		check(queue.numPackets == 0);
		check(queue.startIndex == 0);

		// attempting to pop a packet off an empty queue, should return nullptr
		check(netcode_packet_queue_pop(&queue, nullptr) == nullptr);

		// add some packets to the queue and make sure they pop off in the correct order
		{
#define NUM_PACKETS 100

			void* packets[NUM_PACKETS];

			for (s32 i = 0; i < NUM_PACKETS; ++i)
			{
				packets[i] = malloc((i + 1) * 256);
				check(netcode_packet_queue_push(&queue, packets[i], (u64)i) == 1);
			}

			check(queue.numPackets == NUM_PACKETS);
			for (s32 i = 0; i < NUM_PACKETS; ++i)
			{
				u64 sequence = 0;
				void* packet = netcode_packet_queue_pop(&queue, &sequence);
				check(sequence == (u64)i);
				check(packet == packets[i]);
				free(packet);
			}
		}

		// after all entries are popped off, the queue is empty, so calls to pop should return nullptr
		check(queue.numPackets == 0);
		check(netcode_packet_queue_pop(&queue, nullptr) == nullptr);

		// test that the packet queue can be filled to max capacity

		void* packets[NETCODE_PACKET_QUEUE_SIZE];

		for (s32 i = 0; i < NETCODE_PACKET_QUEUE_SIZE; ++i)
		{
			packets[i] = malloc(i * 256);
			check(netcode_packet_queue_push(&queue, packets[i], (u64)i) == 1);
		}

		check(queue.numPackets == NETCODE_PACKET_QUEUE_SIZE);

		// when the queue is full, attempting to push a packet should fail and return 0
		check(netcode_packet_queue_push(&queue, malloc(100), 0) == 0);

		// make sure all packets pop off in the correct order
		for (s32 i = 0; i < NETCODE_PACKET_QUEUE_SIZE; ++i)
		{
			u64 sequence = 0;
			void* packet = netcode_packet_queue_pop(&queue, &sequence);
			check(sequence == (u64)i);
			check(packet == packets[i]);
			free(packet);
		}

		// add some packets again
		for (s32 i = 0; i < NETCODE_PACKET_QUEUE_SIZE; ++i)
		{
			packets[i] = malloc(i * 256);
			check(netcode_packet_queue_push(&queue, packets[i], (u64)i) == 1);
		}

		// clear the queue and make sure that all packets are freed
		netcode_packet_queue_clear(&queue);

		check(queue.startIndex == 0);
		check(queue.numPackets == 0);
		for (s32 i = 0; i < NETCODE_PACKET_QUEUE_SIZE; ++i)
			check(queue.packetData[i] == nullptr);
	}

	static void test_endian()
	{
		u32 value = 0x11223344;
		c08* bytes = (c08*)& value;

#if GG_LITTLE_ENDIAN
		check(bytes[0] == 0x44);
		check(bytes[1] == 0x33);
		check(bytes[2] == 0x22);
		check(bytes[3] == 0x11);
#else
		check(bytes[0] == 0x11);
		check(bytes[1] == 0x22);
		check(bytes[2] == 0x33);
		check(bytes[3] == 0x44);

#endif
	}

	static void test_sequence()
	{
		check(netcode_sequence_number_bytes_required(0) == 1);
		check(netcode_sequence_number_bytes_required(0x11) == 1);
		check(netcode_sequence_number_bytes_required(0x1122) == 2);
		check(netcode_sequence_number_bytes_required(0x112233) == 3);
		check(netcode_sequence_number_bytes_required(0x11223344) == 4);
		check(netcode_sequence_number_bytes_required(0x1122334455) == 5);
		check(netcode_sequence_number_bytes_required(0x112233445566) == 6);
		check(netcode_sequence_number_bytes_required(0x11223344556677) == 7);
		check(netcode_sequence_number_bytes_required(0x1122334455667788) == 8);
	}

	static void test_address()
	{
		{
			netcode_address_t address;
			check(netcode_parse_address("", &address) == NETCODE_ERROR);
			check(netcode_parse_address("[", &address) == NETCODE_ERROR);
			check(netcode_parse_address("[]", &address) == NETCODE_ERROR);
			check(netcode_parse_address("[]:", &address) == NETCODE_ERROR);
			check(netcode_parse_address(":", &address) == NETCODE_ERROR);
			check(netcode_parse_address("1", &address) == NETCODE_ERROR);
			check(netcode_parse_address("12", &address) == NETCODE_ERROR);
			check(netcode_parse_address("123", &address) == NETCODE_ERROR);
			check(netcode_parse_address("1234", &address) == NETCODE_ERROR);
			check(netcode_parse_address("1234.0.12313.0000", &address) == NETCODE_ERROR);
			check(netcode_parse_address("1234.0.12313.0000.0.0.0.0.0", &address) == NETCODE_ERROR);
			check(netcode_parse_address("1312313:123131:1312313:123131:1312313:123131:1312313:123131:1312313:123131:1312313:123131", &address) == NETCODE_ERROR);
			check(netcode_parse_address(".", &address) == NETCODE_ERROR);
			check(netcode_parse_address("..", &address) == NETCODE_ERROR);
			check(netcode_parse_address("...", &address) == NETCODE_ERROR);
			check(netcode_parse_address("....", &address) == NETCODE_ERROR);
			check(netcode_parse_address(".....", &address) == NETCODE_ERROR);
		}

		{
			netcode_address_t address;
			check(netcode_parse_address("107.77.207.77", &address) == NETCODE_OK);
			check(address.type == NETCODE_ADDRESS_IPV4);
			check(address.port == 0);
			check(address.data.ipv4[0] == 107);
			check(address.data.ipv4[1] == 77);
			check(address.data.ipv4[2] == 207);
			check(address.data.ipv4[3] == 77);
		}

		{
			netcode_address_t address;
			check(netcode_parse_address("127.0.0.1", &address) == NETCODE_OK);
			check(address.type == NETCODE_ADDRESS_IPV4);
			check(address.port == 0);
			check(address.data.ipv4[0] == 127);
			check(address.data.ipv4[1] == 0);
			check(address.data.ipv4[2] == 0);
			check(address.data.ipv4[3] == 1);
		}

		{
			netcode_address_t address;
			check(netcode_parse_address("107.77.207.77:40000", &address) == NETCODE_OK);
			check(address.type == NETCODE_ADDRESS_IPV4);
			check(address.port == 40000);
			check(address.data.ipv4[0] == 107);
			check(address.data.ipv4[1] == 77);
			check(address.data.ipv4[2] == 207);
			check(address.data.ipv4[3] == 77);
		}


		{
			netcode_address_t address;
			check(netcode_parse_address("fe80::202:b3ff:fe1e:8329", &address) == NETCODE_OK);
			check(address.type == NETCODE_ADDRESS_IPV6);
			check(address.port == 0);
			check(address.data.ipv6[0] == 0xfe80);
			check(address.data.ipv6[1] == 0x0000);
			check(address.data.ipv6[2] == 0x0000);
			check(address.data.ipv6[3] == 0x0000);
			check(address.data.ipv6[4] == 0x0202);
			check(address.data.ipv6[5] == 0xb3ff);
			check(address.data.ipv6[6] == 0xfe1e);
			check(address.data.ipv6[7] == 0x8329);
		}

		{
			netcode_address_t address;
			check(netcode_parse_address("::", &address) == NETCODE_OK);
			check(address.type == NETCODE_ADDRESS_IPV6);
			check(address.port == 0);
			check(address.data.ipv6[0] == 0x0000);
			check(address.data.ipv6[1] == 0x0000);
			check(address.data.ipv6[2] == 0x0000);
			check(address.data.ipv6[3] == 0x0000);
			check(address.data.ipv6[4] == 0x0000);
			check(address.data.ipv6[5] == 0x0000);
			check(address.data.ipv6[6] == 0x0000);
			check(address.data.ipv6[7] == 0x0000);
		}

		{
			netcode_address_t address;
			check(netcode_parse_address("::1", &address) == NETCODE_OK);
			check(address.type == NETCODE_ADDRESS_IPV6);
			check(address.port == 0);
			check(address.data.ipv6[0] == 0x0000);
			check(address.data.ipv6[1] == 0x0000);
			check(address.data.ipv6[2] == 0x0000);
			check(address.data.ipv6[3] == 0x0000);
			check(address.data.ipv6[4] == 0x0000);
			check(address.data.ipv6[5] == 0x0000);
			check(address.data.ipv6[6] == 0x0000);
			check(address.data.ipv6[7] == 0x0001);
		}

		{
			netcode_address_t address;
			check(netcode_parse_address("[fe80::202:b3ff:fe1e:8329]:40000", &address) == NETCODE_OK);
			check(address.type == NETCODE_ADDRESS_IPV6);
			check(address.port == 40000);
			check(address.data.ipv6[0] == 0xfe80);
			check(address.data.ipv6[1] == 0x0000);
			check(address.data.ipv6[2] == 0x0000);
			check(address.data.ipv6[3] == 0x0000);
			check(address.data.ipv6[4] == 0x0202);
			check(address.data.ipv6[5] == 0xb3ff);
			check(address.data.ipv6[6] == 0xfe1e);
			check(address.data.ipv6[7] == 0x8329);
		}


		{
			struct netcode_address_t address;
			check(netcode_parse_address("[::]:40000", &address) == NETCODE_OK);
			check(address.type == NETCODE_ADDRESS_IPV6);
			check(address.port == 40000);
			check(address.data.ipv6[0] == 0x0000);
			check(address.data.ipv6[1] == 0x0000);
			check(address.data.ipv6[2] == 0x0000);
			check(address.data.ipv6[3] == 0x0000);
			check(address.data.ipv6[4] == 0x0000);
			check(address.data.ipv6[5] == 0x0000);
			check(address.data.ipv6[6] == 0x0000);
			check(address.data.ipv6[7] == 0x0000);
		}
		{
			struct netcode_address_t address;
			check(netcode_parse_address("[::1]:40000", &address) == NETCODE_OK);
			check(address.type == NETCODE_ADDRESS_IPV6);
			check(address.port == 40000);
			check(address.data.ipv6[0] == 0x0000);
			check(address.data.ipv6[1] == 0x0000);
			check(address.data.ipv6[2] == 0x0000);
			check(address.data.ipv6[3] == 0x0000);
			check(address.data.ipv6[4] == 0x0000);
			check(address.data.ipv6[5] == 0x0000);
			check(address.data.ipv6[6] == 0x0000);
			check(address.data.ipv6[7] == 0x0001);
		}

#if NETCODE_NETWORK_NEXT
		{
			struct netcode_address_t address;
			check(netcode_parse_address("flow:0x1122334455667788", &address) == NETCODE_OK);
			check(address.type == NETCODE_ADDRESS_NEXT);
			check(address.data.flow_id == 0x1122334455667788ULL);
		}
#endif
	}

#define TEST_PROTOCOL_ID            0x1122334455667788ULL
#define TEST_CLIENT_ID              0x1ULL
#define TEST_SERVER_PORT            40000
#define TEST_CONNECT_TOKEN_EXPIRY   30
#define TEST_TIMEOUT_SECONDS        15

	static void test_connect_token()
	{
		// generate connect token
		netcode_address_t serverAddress;
		serverAddress.type = NETCODE_ADDRESS_IPV4;
		serverAddress.data.ipv4[0] = 127;
		serverAddress.data.ipv4[1] = 0;
		serverAddress.data.ipv4[2] = 0;
		serverAddress.data.ipv4[3] = 1;
		serverAddress.port = TEST_SERVER_PORT;

		u08 userData[NETCODE_USER_DATA_BYTES];
		netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

		netcode_connect_token_private_t inputToken;
		netcode_generate_connect_token_private(&inputToken, TEST_CLIENT_ID, TEST_TIMEOUT_SECONDS, 1, &serverAddress, userData);

		check(inputToken.clientId == TEST_CLIENT_ID);
		check(inputToken.numServerAddresses == 1);
		check(memcmp(inputToken.userData, userData, NETCODE_USER_DATA_BYTES) == 0);
		check(netcode_address_equal(&inputToken.serverAddresses[0], &serverAddress));

		// write it to a buffer
		u08 buffer[NETCODE_CONNECT_TOKEN_PRIVATE_BYTES];

		netcode_write_connect_token_private(&inputToken, buffer, NETCODE_CONNECT_TOKEN_BYTES);

		// encrypt the buffer
		u64 expireTimestamp = time(nullptr) + 30;
		u08 nonce[NETCODE_CONNECT_TOKEN_NONCE_BYTES];
		netcode_generate_nonce(nonce);
		u08 key[NETCODE_KEY_BYTES];
		netcode_generate_key(key);

		check(netcode_encrypt_connect_token_private(buffer,
			NETCODE_CONNECT_TOKEN_PRIVATE_BYTES,
			NETCODE_VERSION_INFO,
			TEST_PROTOCOL_ID,
			expireTimestamp,
			nonce,
			key) == NETCODE_OK);


		// decrypt the buffer
		check(netcode_decrypt_connect_token_private(buffer, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES,
			NETCODE_VERSION_INFO,
			TEST_PROTOCOL_ID,
			expireTimestamp,
			nonce,
			key) == NETCODE_OK);

		// read connect token back in
		netcode_connect_token_private_t outputToken;
		check(netcode_read_connect_token_private(buffer, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES, &outputToken) == NETCODE_OK);

		// check everything we got from reading with the original connect token
		check(outputToken.clientId == inputToken.clientId);
		check(outputToken.timeoutSeconds == inputToken.timeoutSeconds);
		check(outputToken.numServerAddresses == inputToken.numServerAddresses);
		check(netcode_address_equal(&outputToken.serverAddresses[0], &inputToken.serverAddresses[0]));
		check(memcmp(outputToken.clientToServerKey, inputToken.clientToServerKey, NETCODE_KEY_BYTES) == 0);
		check(memcmp(outputToken.serverToClientKey, inputToken.serverToClientKey, NETCODE_KEY_BYTES) == 0);
		check(memcmp(outputToken.userData, inputToken.userData, NETCODE_USER_DATA_BYTES) == 0);
	}


	static void test_challenge_token()
	{
		// generate challenge token
		netcode_challenge_token_t inputToken;

		inputToken.client_id = TEST_CLIENT_ID;
		netcode_random_bytes(inputToken.user_data, NETCODE_USER_DATA_BYTES);

		// write token to a buffer
		u08 buffer[NETCODE_CHALLENGE_TOKEN_BYTES];
		netcode_write_challenge_token(&inputToken, buffer, NETCODE_CHALLENGE_TOKEN_BYTES);

		// encrypt buffer
		u64 sequence = 1000;
		u08 key[NETCODE_KEY_BYTES];
		netcode_generate_key(key);

		check(netcode_encrypt_challenge_token(buffer, NETCODE_CHALLENGE_TOKEN_BYTES, sequence, key) == NETCODE_OK);

		// decrypt the buffer
		check(netcode_decrypt_challenge_token(buffer, NETCODE_CHALLENGE_TOKEN_BYTES, sequence, key) == NETCODE_OK);

		// read challenge token back in
		netcode_challenge_token_t outputToken;
		check(netcode_read_challenge_token(buffer, NETCODE_CHALLENGE_TOKEN_BYTES, &outputToken) == NETCODE_OK);

		// check everything we got from reading with the original connect token
		check(outputToken.client_id == inputToken.client_id);
		check(memcmp(outputToken.user_data, inputToken.user_data, NETCODE_USER_DATA_BYTES) == 0);
	}

	static void test_connection_request_packet()
	{
		netcode_address_t serverAddress;
		serverAddress.type = NETCODE_ADDRESS_IPV4;
		serverAddress.data.ipv4[0] = 127;
		serverAddress.data.ipv4[1] = 0;
		serverAddress.data.ipv4[2] = 0;
		serverAddress.data.ipv4[3] = 1;
		serverAddress.port = TEST_SERVER_PORT;

		u08 userData[NETCODE_USER_DATA_BYTES];
		netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

		netcode_connect_token_private_t inputToken;

		netcode_generate_connect_token_private(&inputToken, TEST_CLIENT_ID, TEST_TIMEOUT_SECONDS, 1, &serverAddress, userData);

		check(inputToken.clientId == TEST_CLIENT_ID);
		check(inputToken.numServerAddresses == 1);
		check(memcmp(inputToken.userData, userData, NETCODE_USER_DATA_BYTES) == 0);
		check(netcode_address_equal(&inputToken.serverAddresses[0], &serverAddress));

		// write the connect token to a buffer (non-encrypted)
		u08 connectTokenData[NETCODE_CONNECT_TOKEN_PRIVATE_BYTES];
		netcode_write_connect_token_private(&inputToken, connectTokenData, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);

		// copy to a second buffer then encrypt it in place (need the unencrypted token for verification later on)
		u08 encryptedConnectTokenData[NETCODE_CONNECT_TOKEN_PRIVATE_BYTES];
		memcpy(encryptedConnectTokenData, connectTokenData, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);

		u64 connectTokenExpireTimestamp = time(nullptr) + 30;
		u08 connectTokenNonce[NETCODE_CONNECT_TOKEN_NONCE_BYTES];
		netcode_generate_nonce(connectTokenNonce);
		u08 connectTokenKey[NETCODE_KEY_BYTES];
		netcode_generate_key(connectTokenKey);


		check(netcode_encrypt_connect_token_private(encryptedConnectTokenData,
			NETCODE_CONNECT_TOKEN_PRIVATE_BYTES,
			NETCODE_VERSION_INFO,
			TEST_PROTOCOL_ID,
			connectTokenExpireTimestamp,
			connectTokenNonce,
			connectTokenKey) == NETCODE_OK);

		// setup a connection request packet wrapping the encrypted connect token
		netcode_connection_request_packet_t inputPacket;

		inputPacket.packetType = NETCODE_CONNECTION_REQUEST_PACKET;
		memcpy(inputPacket.versionInfo, NETCODE_VERSION_INFO, NETCODE_VERSION_INFO_BYTES);
		inputPacket.protocolId = TEST_PROTOCOL_ID;
		inputPacket.connectTokenExpireTimestamp = connectTokenExpireTimestamp;
		memcpy(inputPacket.connectTokenNonce, connectTokenNonce, NETCODE_CONNECT_TOKEN_NONCE_BYTES);
		memcpy(inputPacket.connectTokenData, encryptedConnectTokenData, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);

		// write the connect request packet to a buffer
		u08 buffer[2048];
		u08 packetKey[NETCODE_KEY_BYTES];

		netcode_generate_key(packetKey);

		s32 bytesWritten = netcode_write_packet(&inputPacket, buffer, sizeof(buffer), 1000, packetKey, TEST_PROTOCOL_ID);

		check(bytesWritten > 0);

		// read the connection request packet back in from the buffer (the connect token data is decrypted as part of the read packet validation)
		u64 sequence = 1000;

		u08 allowedPackets[NETCODE_CONNECTION_NUM_PACKETS];
		memset(allowedPackets, 1, sizeof(allowedPackets));

		netcode_connection_request_packet_t* outputPacket = (netcode_connection_request_packet_t*)netcode_read_packet(buffer, bytesWritten, &sequence, packetKey,
			TEST_PROTOCOL_ID, time(nullptr), connectTokenKey, allowedPackets, nullptr, nullptr, nullptr);

		check(outputPacket);

		// make sure the read packet matches what was written
		check(outputPacket->packetType == NETCODE_CONNECTION_REQUEST_PACKET);
		check(memcmp(outputPacket->versionInfo, inputPacket.versionInfo, NETCODE_VERSION_INFO_BYTES) == 0);
		check(outputPacket->protocolId == inputPacket.protocolId);
		check(outputPacket->connectTokenExpireTimestamp == inputPacket.connectTokenExpireTimestamp);
		check(memcmp(outputPacket->connectTokenNonce, connectTokenNonce, NETCODE_CONNECT_TOKEN_NONCE_BYTES) == 0);
		check(memcmp(outputPacket->connectTokenData, connectTokenData, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES - NETCODE_MAC_BYTES) == 0);

		free(outputPacket);
	}

	void test_connection_denied_packet()
	{
		netcode_connection_denied_packet_t inputPacket;

		inputPacket.packetType = NETCODE_CONNECTION_DENIED_PACKET;

		// write packet to a buffer
		u08 buffer[NETCODE_MAX_PACKET_BYTES];
		u08 packetKey[NETCODE_KEY_BYTES];

		netcode_generate_key(packetKey);

		s32 bytesWritten = netcode_write_packet(&inputPacket, buffer, sizeof(buffer), 1000, packetKey, TEST_PROTOCOL_ID);

		check(bytesWritten > 0);

		// read packet back in from the buffer
		u64 sequence;

		u08 allowedPacketTypes[NETCODE_CONNECTION_NUM_PACKETS];
		memset(allowedPacketTypes, 1, sizeof(allowedPacketTypes));

		netcode_connection_denied_packet_t* outputPacket = (netcode_connection_denied_packet_t*)
			netcode_read_packet(buffer,
				bytesWritten,
				&sequence,
				packetKey,
				TEST_PROTOCOL_ID,
				time(nullptr),
				nullptr,
				allowedPacketTypes,
				nullptr, nullptr, nullptr);

		check(outputPacket);

		// make sure the read packet matches what was written
		check(outputPacket->packetType == NETCODE_CONNECTION_DENIED_PACKET);

		free(outputPacket);
	}

	void test_connection_challenge_packet()
	{
		netcode_connection_challenge_packet_t inputPacket;

		inputPacket.packetType = NETCODE_CONNECTION_CHALLENGE_PACKET;
		inputPacket.challengeTokenSequence = 0;
		netcode_random_bytes(inputPacket.challengeTokenData, NETCODE_CHALLENGE_TOKEN_BYTES);

		// write packet to a buffer
		u08 buffer[NETCODE_MAX_PACKET_BYTES];
		u08 packetKey[NETCODE_KEY_BYTES];
		netcode_generate_key(packetKey);

		s32 bytesWritten = netcode_write_packet(&inputPacket, buffer, sizeof(buffer), 1000, packetKey, TEST_PROTOCOL_ID);

		check(bytesWritten > 0);

		// read packet back in from the buffer
		u64 sequence;

		u08 allowedPacketTypes[NETCODE_CONNECTION_NUM_PACKETS];
		memset(allowedPacketTypes, 1, sizeof(allowedPacketTypes));

		netcode_connection_challenge_packet_t* outputPacket = (netcode_connection_challenge_packet_t*)
			netcode_read_packet(buffer,
				bytesWritten,
				&sequence,
				packetKey,
				TEST_PROTOCOL_ID,
				time(nullptr),
				nullptr,
				allowedPacketTypes,
				nullptr, nullptr, nullptr);

		check(outputPacket);

		// make sure the read packet matches what was written
		check(outputPacket->packetType == NETCODE_CONNECTION_CHALLENGE_PACKET);
		check(outputPacket->challengeTokenSequence == inputPacket.challengeTokenSequence);
		check(memcmp(outputPacket->challengeTokenData, inputPacket.challengeTokenData, NETCODE_CHALLENGE_TOKEN_BYTES) == 0);

		free(outputPacket);
	}

	void test_connection_response_packet()
	{
		netcode_connection_response_packet_t inputPacket;

		inputPacket.packetType = NETCODE_CONNECTION_RESPONSE_PACKET;
		inputPacket.challengeTokenSequence = 0;
		netcode_random_bytes(inputPacket.challengeTokenData, NETCODE_CHALLENGE_TOKEN_BYTES);

		// write packet to a buffer
		u08 buffer[NETCODE_MAX_PACKET_BYTES];
		u08 packetKey[NETCODE_KEY_BYTES];
		netcode_generate_key(packetKey);

		s32 bytesWritten = netcode_write_packet(&inputPacket, buffer, sizeof(buffer), 1000, packetKey, TEST_PROTOCOL_ID);

		check(bytesWritten > 0);

		// read packet back in from the buffer
		u64 sequence;

		u08 allowedPacketTypes[NETCODE_CONNECTION_NUM_PACKETS];
		memset(allowedPacketTypes, 1, sizeof(allowedPacketTypes));

		netcode_connection_response_packet_t* outputPacket = (netcode_connection_response_packet_t*)
			netcode_read_packet(buffer,
				bytesWritten,
				&sequence,
				packetKey,
				TEST_PROTOCOL_ID,
				time(nullptr),
				nullptr,
				allowedPacketTypes,
				nullptr, nullptr, nullptr);

		check(outputPacket);

		// make sure the read packet matches what was written
		check(outputPacket->packetType == NETCODE_CONNECTION_RESPONSE_PACKET);
		check(outputPacket->challengeTokenSequence == inputPacket.challengeTokenSequence);
		check(memcmp(outputPacket->challengeTokenData, inputPacket.challengeTokenData, NETCODE_CHALLENGE_TOKEN_BYTES) == 0);

		free(outputPacket);
	}

	void test_connection_keep_alive_packet()
	{
		netcode_connection_keep_alive_packet_t inputPacket;

		inputPacket.packetType = NETCODE_CONNECTION_KEEP_ALIVE_PACKET;
		inputPacket.clientIndex = 10;
		inputPacket.maxClients = 16;

		// write packet to a buffer
		u08 buffer[NETCODE_MAX_PACKET_BYTES];
		u08 packetKey[NETCODE_KEY_BYTES];
		netcode_generate_key(packetKey);

		s32 bytesWritten = netcode_write_packet(&inputPacket, buffer, sizeof(buffer), 1000, packetKey, TEST_PROTOCOL_ID);

		check(bytesWritten > 0);

		// read packet back in from the buffer
		u64 sequence;

		u08 allowedPacketTypes[NETCODE_CONNECTION_NUM_PACKETS];
		memset(allowedPacketTypes, 1, sizeof(allowedPacketTypes));

		netcode_connection_keep_alive_packet_t* outputPacket = (netcode_connection_keep_alive_packet_t*)
			netcode_read_packet(buffer,
				bytesWritten,
				&sequence,
				packetKey,
				TEST_PROTOCOL_ID,
				time(nullptr),
				nullptr,
				allowedPacketTypes,
				nullptr, nullptr, nullptr);

		check(outputPacket);

		// make sure the read packet matches what was written
		check(outputPacket->packetType == NETCODE_CONNECTION_KEEP_ALIVE_PACKET);
		check(outputPacket->clientIndex == inputPacket.clientIndex);
		check(outputPacket->maxClients == inputPacket.maxClients);

		free(outputPacket);
	}

	void test_connection_payload_packet()
	{
		netcode_connection_payload_packet_t* inputPacket = netcode_create_payload_packet(NETCODE_MAX_PAYLOAD_BYTES, nullptr, nullptr);

		check(inputPacket->packetType == NETCODE_CONNECTION_PAYLOAD_PACKET);
		check(inputPacket->payloadBytes == NETCODE_MAX_PAYLOAD_BYTES);

		netcode_random_bytes(inputPacket->payloadData, NETCODE_MAX_PAYLOAD_BYTES);

		// write packet to a buffer
		u08 buffer[NETCODE_MAX_PACKET_BYTES];
		u08 packetKey[NETCODE_KEY_BYTES];
		netcode_generate_key(packetKey);

		s32 bytesWritten = netcode_write_packet(inputPacket, buffer, sizeof(buffer), 1000, packetKey, TEST_PROTOCOL_ID);

		check(bytesWritten > 0);

		// read packet back in from the buffer
		u64 sequence;

		u08 allowedPacketTypes[NETCODE_CONNECTION_NUM_PACKETS];
		memset(allowedPacketTypes, 1, sizeof(allowedPacketTypes));

		netcode_connection_payload_packet_t* outputPacket = (netcode_connection_payload_packet_t*)
			netcode_read_packet(buffer,
				bytesWritten,
				&sequence,
				packetKey,
				TEST_PROTOCOL_ID,
				time(nullptr),
				nullptr,
				allowedPacketTypes,
				nullptr, nullptr, nullptr);

		check(outputPacket);

		// make sure the read packet matches what was written
		check(outputPacket->packetType == NETCODE_CONNECTION_PAYLOAD_PACKET);
		check(outputPacket->payloadBytes == inputPacket->payloadBytes);
		check(memcmp(outputPacket->payloadData, inputPacket->payloadData, NETCODE_MAX_PAYLOAD_BYTES) == 0);

		free(inputPacket);
		free(outputPacket);
	}


	void test_connection_disconnect_packet()
	{
		netcode_connection_disconnect_packet_t inputPacket;

		inputPacket.packetType = NETCODE_CONNECTION_DISCONNECT_PACKET;

		// write packet to a buffer
		u08 buffer[NETCODE_MAX_PACKET_BYTES];
		u08 packetKey[NETCODE_KEY_BYTES];
		netcode_generate_key(packetKey);

		s32 bytesWritten = netcode_write_packet(&inputPacket, buffer, sizeof(buffer), 1000, packetKey, TEST_PROTOCOL_ID);

		check(bytesWritten > 0);

		// read packet back in from the buffer
		u64 sequence;

		u08 allowedPacketTypes[NETCODE_CONNECTION_NUM_PACKETS];
		memset(allowedPacketTypes, 1, sizeof(allowedPacketTypes));

		netcode_connection_disconnect_packet_t* outputPacket = (netcode_connection_disconnect_packet_t*)
			netcode_read_packet(buffer,
				bytesWritten,
				&sequence,
				packetKey,
				TEST_PROTOCOL_ID,
				time(nullptr),
				nullptr,
				allowedPacketTypes,
				nullptr, nullptr, nullptr);

		check(outputPacket);

		// make sure the read packet matches what was written
		check(outputPacket->packetType == NETCODE_CONNECTION_DISCONNECT_PACKET);

		free(outputPacket);
	}

	void test_connect_token_public()
	{
		netcode_address_t serverAddress;
		serverAddress.type = NETCODE_ADDRESS_IPV4;
		serverAddress.data.ipv4[0] = 127;
		serverAddress.data.ipv4[1] = 0;
		serverAddress.data.ipv4[2] = 0;
		serverAddress.data.ipv4[3] = 1;
		serverAddress.port = TEST_SERVER_PORT;

		u08 userData[NETCODE_USER_DATA_BYTES];
		netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

		netcode_connect_token_private_t connectTokenPrivate;
		netcode_generate_connect_token_private(&connectTokenPrivate, TEST_CLIENT_ID, TEST_TIMEOUT_SECONDS, 1, &serverAddress, userData);

		check(connectTokenPrivate.clientId == TEST_CLIENT_ID);
		check(connectTokenPrivate.numServerAddresses == 1);
		check(memcmp(connectTokenPrivate.userData, userData, NETCODE_USER_DATA_BYTES) == 0);
		check(netcode_address_equal(&connectTokenPrivate.serverAddresses[0], &serverAddress));

		// write private connect token to buffer
		u08 connectTokenPrivateData[NETCODE_CONNECT_TOKEN_PRIVATE_BYTES];
		netcode_write_connect_token_private(&connectTokenPrivate, connectTokenPrivateData, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);

		// encrypt it to a buffer
		u64 createTimestamp = time(nullptr);
		u64 expireTimestamp = createTimestamp + 30;
		u08 connectTokenNonce[NETCODE_CONNECT_TOKEN_NONCE_BYTES];
		netcode_generate_nonce(connectTokenNonce);
		u08 key[NETCODE_KEY_BYTES];
		netcode_generate_key(key);
		check(netcode_encrypt_connect_token_private(connectTokenPrivateData,
			NETCODE_CONNECT_TOKEN_PRIVATE_BYTES,
			NETCODE_VERSION_INFO,
			TEST_PROTOCOL_ID,
			expireTimestamp,
			connectTokenNonce,
			key) == 1);
		// wrap a public conenct token around the private connect token data
		netcode_connect_token_t inputConnectToken;
		memset(&inputConnectToken, 0, sizeof(netcode_connect_token_t));
		memcpy(inputConnectToken.versionInfo, NETCODE_VERSION_INFO, NETCODE_VERSION_INFO_BYTES);
		inputConnectToken.protocolId = TEST_PROTOCOL_ID;
		inputConnectToken.createTimestamp = createTimestamp;
		inputConnectToken.expireTimestamp = expireTimestamp;
		memcpy(inputConnectToken.nonce, connectTokenNonce, NETCODE_CONNECT_TOKEN_NONCE_BYTES);
		memcpy(inputConnectToken.privateData, connectTokenPrivateData, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES);
		inputConnectToken.numServerAddresses = 1;
		inputConnectToken.serverAddresses[0] = serverAddress;
		memcpy(inputConnectToken.clientToServerKey, connectTokenPrivate.clientToServerKey, NETCODE_KEY_BYTES);
		memcpy(inputConnectToken.serverToClientKey, connectTokenPrivate.serverToClientKey, NETCODE_KEY_BYTES);
		inputConnectToken.timeoutSeconds = (s32)TEST_TIMEOUT_SECONDS;

		// write connect token to a buffer
		u08 buffer[NETCODE_CONNECT_TOKEN_BYTES];
		netcode_write_connect_token(&inputConnectToken, buffer, NETCODE_CONNECT_TOKEN_BYTES);

		// read the bufferr back in
		netcode_connect_token_t outputConnectToken;
		memset(&outputConnectToken, 0, sizeof(netcode_connect_token_t));
		check(netcode_read_connect_token(buffer, NETCODE_CONNECT_TOKEN_BYTES, &outputConnectToken) == 1);

		// make sure the public connect token matches what was written
		check(memcmp(outputConnectToken.versionInfo, inputConnectToken.versionInfo, NETCODE_VERSION_INFO_BYTES) == 0);
		check(memcmp(outputConnectToken.nonce, inputConnectToken.nonce, NETCODE_CONNECT_TOKEN_NONCE_BYTES) == 0);
		check(memcmp(outputConnectToken.privateData, inputConnectToken.privateData, NETCODE_CONNECT_TOKEN_PRIVATE_BYTES) == 0);
		check(memcmp(outputConnectToken.clientToServerKey, inputConnectToken.clientToServerKey, NETCODE_KEY_BYTES) == 0);
		check(memcmp(outputConnectToken.serverToClientKey, inputConnectToken.serverToClientKey, NETCODE_KEY_BYTES) == 0);
		check(outputConnectToken.protocolId == inputConnectToken.protocolId);
		check(outputConnectToken.createTimestamp == inputConnectToken.createTimestamp);
		check(outputConnectToken.expireTimestamp == inputConnectToken.expireTimestamp);
		check(outputConnectToken.timeoutSeconds == inputConnectToken.timeoutSeconds);
		check(outputConnectToken.numServerAddresses == inputConnectToken.numServerAddresses);
		check(netcode_address_equal(&outputConnectToken.serverAddresses[0], &inputConnectToken.serverAddresses[0]));
	}

	void test_encryption_manager()
	{
		netcode_encryption_manager_t encryptionManager;
		netcode_encryption_manager_reset(&encryptionManager);

		f64 time = 100.0;

		// generate test encryption mappings
		struct encryption_mapping_t
		{
			netcode_address_t address;
			u08 sendKey[NETCODE_KEY_BYTES];
			u08 receiveKey[NETCODE_KEY_BYTES];
		};

#define NUM_ENCRYPTION_MAPPINGS 5

		encryption_mapping_t encryptionMapping[NUM_ENCRYPTION_MAPPINGS];
		memset(encryptionMapping, 0, sizeof(encryptionMapping));
		for (s32 i = 0; i < NUM_ENCRYPTION_MAPPINGS; ++i)
		{
			encryptionMapping[i].address.type = NETCODE_ADDRESS_IPV6;
			encryptionMapping[i].address.data.ipv6[7] = 1;
			encryptionMapping[i].address.port = (u16)(20000 + i);
			netcode_generate_key(encryptionMapping[i].sendKey);
			netcode_generate_key(encryptionMapping[i].receiveKey);
		}

		// add the encryption mappings to the manager and make sure they can be looked up by address

		for (s32 i = 0; i < NUM_ENCRYPTION_MAPPINGS; ++i)
		{
			s32 encryptionIndex = netcode_encryption_manager_find_encryption_mapping(&encryptionManager, &encryptionMapping[i].address, time);

			check(encryptionIndex == -1);

			check(netcode_encryption_manager_get_send_key(&encryptionManager, encryptionIndex) == nullptr);
			check(netcode_encryption_manager_get_receive_key(&encryptionManager, encryptionIndex) == nullptr);

			check(netcode_encryption_manager_add_encryption_mapping(&encryptionManager,
				&encryptionMapping[i].address,
				encryptionMapping[i].sendKey,
				encryptionMapping[i].receiveKey,
				time,
				-1.0,
				TEST_TIMEOUT_SECONDS));

			encryptionIndex = netcode_encryption_manager_find_encryption_mapping(&encryptionManager, &encryptionMapping[i].address, time);

			u08* sendKey = netcode_encryption_manager_get_send_key(&encryptionManager, encryptionIndex);
			u08* receiveKey = netcode_encryption_manager_get_receive_key(&encryptionManager, encryptionIndex);

			check(sendKey);
			check(receiveKey);

			check(memcmp(sendKey, encryptionMapping[i].sendKey, NETCODE_KEY_BYTES) == 0);
			check(memcmp(receiveKey, encryptionMapping[i].receiveKey, NETCODE_KEY_BYTES) == 0);
		}

		// removing an encryption mapping that doesn't exist should return 0
		{
			netcode_address_t address;
			address.type = NETCODE_ADDRESS_IPV6;
			address.data.ipv6[7] = 1;
			address.port = 50000;
			check(netcode_encryption_manager_remove_encryption_mapping(&encryptionManager, &address, time) == 0);
		}

		// remove the first and last encryption mappings
		check(netcode_encryption_manager_remove_encryption_mapping(&encryptionManager, &encryptionMapping[0].address, time) == 1);
		check(netcode_encryption_manager_remove_encryption_mapping(&encryptionManager, &encryptionMapping[NUM_ENCRYPTION_MAPPINGS - 1].address, time) == 1);

		// make sure the encryption mappings that were removed can no longer be looked up by address

		for (s32 i = 0; i < NUM_ENCRYPTION_MAPPINGS; ++i)
		{
			s32 encryptionIndex = netcode_encryption_manager_find_encryption_mapping(&encryptionManager, &encryptionMapping[i].address, time);
			u08* sendKey = netcode_encryption_manager_get_send_key(&encryptionManager, encryptionIndex);
			u08* receiveKey = netcode_encryption_manager_get_receive_key(&encryptionManager, encryptionIndex);

			if (i != 0 && i != NUM_ENCRYPTION_MAPPINGS - 1)
			{
				check(sendKey);
				check(receiveKey);

				check(memcmp(sendKey, encryptionMapping[i].sendKey, NETCODE_KEY_BYTES) == 0);
				check(memcmp(receiveKey, encryptionMapping[i].receiveKey, NETCODE_KEY_BYTES) == 0);
			}
			else
			{
				check(!sendKey);
				check(!receiveKey);
			}
		}

		// add the encryption mappings back in
		check(netcode_encryption_manager_add_encryption_mapping(&encryptionManager,
			&encryptionMapping[0].address,
			encryptionMapping[0].sendKey,
			encryptionMapping[0].receiveKey,
			time,
			-1.0,
			TEST_TIMEOUT_SECONDS));
		check(netcode_encryption_manager_add_encryption_mapping(&encryptionManager,
			&encryptionMapping[NUM_ENCRYPTION_MAPPINGS - 1].address,
			encryptionMapping[NUM_ENCRYPTION_MAPPINGS - 1].sendKey,
			encryptionMapping[NUM_ENCRYPTION_MAPPINGS - 1].receiveKey,
			time,
			-1.0,
			TEST_TIMEOUT_SECONDS));

		// all encryption mappings should be able to be looked up by address again
		for (s32 i = 0; i < NUM_ENCRYPTION_MAPPINGS; ++i)
		{
			s32 encryptionIndex = netcode_encryption_manager_find_encryption_mapping(&encryptionManager, &encryptionMapping[i].address, time);

			u08* sendKey = netcode_encryption_manager_get_send_key(&encryptionManager, encryptionIndex);
			u08* receiveKey = netcode_encryption_manager_get_receive_key(&encryptionManager, encryptionIndex);

			check(sendKey);
			check(receiveKey);

			check(memcmp(sendKey, encryptionMapping[i].sendKey, NETCODE_KEY_BYTES) == 0);
			check(memcmp(receiveKey, encryptionMapping[i].receiveKey, NETCODE_KEY_BYTES) == 0);
		}

		// check that encryption mappings time out properly
		time += TEST_TIMEOUT_SECONDS * 2;

		for (s32 i = 0; i < NUM_ENCRYPTION_MAPPINGS; ++i)
		{
			s32 encryptionIndex = netcode_encryption_manager_find_encryption_mapping(&encryptionManager, &encryptionMapping[i].address, time);

			u08* sendKey = netcode_encryption_manager_get_send_key(&encryptionManager, encryptionIndex);
			u08* receiveKey = netcode_encryption_manager_get_receive_key(&encryptionManager, encryptionIndex);

			check(!sendKey);
			check(!receiveKey);
		}

		// add the same encrpyion mappings after timeout
		for (s32 i = 0; i < NUM_ENCRYPTION_MAPPINGS; ++i)
		{
			s32 encryptionIndex = netcode_encryption_manager_find_encryption_mapping(&encryptionManager, &encryptionMapping[i].address, time);

			check(encryptionIndex == -1);

			check(netcode_encryption_manager_get_send_key(&encryptionManager, encryptionIndex) == nullptr);
			check(netcode_encryption_manager_get_receive_key(&encryptionManager, encryptionIndex) == nullptr);

			check(netcode_encryption_manager_add_encryption_mapping(&encryptionManager,
				&encryptionMapping[i].address,
				encryptionMapping[i].sendKey,
				encryptionMapping[i].receiveKey,
				time,
				-1.0,
				TEST_TIMEOUT_SECONDS));

			encryptionIndex = netcode_encryption_manager_find_encryption_mapping(&encryptionManager, &encryptionMapping[i].address, time);

			u08* sendKey = netcode_encryption_manager_get_send_key(&encryptionManager, encryptionIndex);
			u08* receiveKey = netcode_encryption_manager_get_receive_key(&encryptionManager, encryptionIndex);

			check(sendKey);
			check(receiveKey);

			check(memcmp(sendKey, encryptionMapping[i].sendKey, NETCODE_KEY_BYTES) == 0);
			check(memcmp(receiveKey, encryptionMapping[i].receiveKey, NETCODE_KEY_BYTES) == 0);
		}

		// reset the encryption manager and verify that all encryption mappings have been removed
		netcode_encryption_manager_reset(&encryptionManager);
		for (s32 i = 0; i < NUM_ENCRYPTION_MAPPINGS; ++i)
		{
			s32 encryptionIndex = netcode_encryption_manager_find_encryption_mapping(&encryptionManager, &encryptionMapping[i].address, time);

			u08* sendKey = netcode_encryption_manager_get_send_key(&encryptionManager, encryptionIndex);
			u08* receiveKey = netcode_encryption_manager_get_receive_key(&encryptionManager, encryptionIndex);

			check(!sendKey);
			check(!receiveKey);
		}

		// test the expire time for encryption mapping works as expected

		check(netcode_encryption_manager_add_encryption_mapping(&encryptionManager,
			&encryptionMapping[0].address,
			encryptionMapping[0].sendKey,
			encryptionMapping[0].receiveKey,
			time,
			time + 1.0,
			TEST_TIMEOUT_SECONDS));

		s32 encryptionIndex = netcode_encryption_manager_find_encryption_mapping(&encryptionManager, &encryptionMapping[0].address, time);

		check(encryptionIndex != -1);

		check(netcode_encryption_manager_find_encryption_mapping(&encryptionManager, &encryptionMapping[0].address, time + 1.1f) == -1);

		netcode_encryption_manager_set_expire_time(&encryptionManager, encryptionIndex, -1.0);

		check(netcode_encryption_manager_find_encryption_mapping(&encryptionManager, &encryptionMapping[0].address, time) == encryptionIndex);
	}


	void test_replay_protection()
	{
		netcode_replay_protection_t replayProtection;

		for (s32 i = 0; i < 2; ++i)
		{
			netcode_replay_protection_reset(&replayProtection);

			check(replayProtection.mostRecentSequence == 0);

			// the first time we receive packets, they should not be already received
#define MAX_SEQUENCE ( NETCODE_REPLAY_PROTECTION_BUFFER_SIZE * 4 )

			u64 sequence;
			for (sequence = 0; sequence < MAX_SEQUENCE; ++sequence)
			{
				check(netcode_replay_protection_already_received(&replayProtection, sequence) == 0);
				netcode_replay_protection_advance_sequence(&replayProtection, sequence);
			}

			// old packets outside buffer should be considered already received
			check(netcode_replay_protection_already_received(&replayProtection, 0) == 1);

			// packets received a second time should be flagged already received
			for (sequence = MAX_SEQUENCE - 10; sequence < MAX_SEQUENCE; ++sequence)
			{
				check(netcode_replay_protection_already_received(&replayProtection, sequence) == 1);
			}

			// jumping ahead to a much higher sequence should be considered not already received
			check(netcode_replay_protection_already_received(&replayProtection, MAX_SEQUENCE + NETCODE_REPLAY_PROTECTION_BUFFER_SIZE) == 0);

			// old packets should be considered already received
			for (sequence = 0; sequence < MAX_SEQUENCE; ++sequence)
			{
				check(netcode_replay_protection_already_received(&replayProtection, sequence) == 1);
			}
		}
	}

	void test_client_create()
	{
		{
			netcode_client_config_t clientConfig;
			netcode_default_client_config(&clientConfig);

			netcode_client_t* client = netcode_client_create("127.0.0.1:40000", &clientConfig, 0.0);

			netcode_address_t testAddress;
			netcode_parse_address("127.0.0.1:40000", &testAddress);

			check(client);
			check(client->socketHolder.ipv4.handle != 0);
			check(client->socketHolder.ipv6.handle == 0);
			check(netcode_address_equal(&client->address, &testAddress));

			netcode_client_destroy(client);
		}

		{
			netcode_client_config_t clientConfig;
			netcode_default_client_config(&clientConfig);

			netcode_client_t* client = netcode_client_create("[::]:50000", &clientConfig, 0.0);

			netcode_address_t testAddress;
			netcode_parse_address("[::]:50000", &testAddress);

			check(client);
			check(client->socketHolder.ipv4.handle == 0);
			check(client->socketHolder.ipv6.handle != 0);
			check(netcode_address_equal(&client->address, &testAddress));

			netcode_client_destroy(client);
		}

		{
			netcode_client_config_t clientConfig;
			netcode_default_client_config(&clientConfig);

			netcode_client_t* client = netcode_client_create_overload("127.0.0.1:40000", "[::]:50000", &clientConfig, 0.0);

			netcode_address_t testAddress;
			netcode_parse_address("127.0.0.1:40000", &testAddress);

			check(client);
			check(client->socketHolder.ipv4.handle != 0);
			check(client->socketHolder.ipv6.handle != 0);
			check(netcode_address_equal(&client->address, &testAddress));

			netcode_client_destroy(client);
		}

		{
			netcode_client_config_t clientConfig;
			netcode_default_client_config(&clientConfig);

			netcode_client_t* client = netcode_client_create_overload("[::]:50000", "127.0.0.1:40000", &clientConfig, 0.0);

			netcode_address_t testAddress;
			netcode_parse_address("[::]:50000", &testAddress);

			check(client);
			check(client->socketHolder.ipv4.handle != 0);
			check(client->socketHolder.ipv6.handle != 0);
			check(netcode_address_equal(&client->address, &testAddress));

			netcode_client_destroy(client);
		}
	}

	void test_server_create()
	{
		{
			netcode_server_config_t serverConfig;
			netcode_default_server_config(&serverConfig);

			netcode_server_t* server = netcode_server_create("127.0.0.1:40000", &serverConfig, 0.0);

			netcode_address_t testAddress;
			netcode_parse_address("127.0.0.1:40000", &testAddress);

			check(server);
			check(server->socketHolder.ipv4.handle != 0);
			check(server->socketHolder.ipv6.handle == 0);
			check(netcode_address_equal(&server->address, &testAddress));

			netcode_server_destroy(server);
		}

		{
			netcode_server_config_t serverConfig;
			netcode_default_server_config(&serverConfig);

			netcode_server_t* server = netcode_server_create("[::1]:50000", &serverConfig, 0.0);

			netcode_address_t testAddress;
			netcode_parse_address("[::1]:50000", &testAddress);

			check(server);
			check(server->socketHolder.ipv4.handle == 0);
			check(server->socketHolder.ipv6.handle != 0);
			check(netcode_address_equal(&server->address, &testAddress));

			netcode_server_destroy(server);
		}

		{
			netcode_server_config_t serverConfig;
			netcode_default_server_config(&serverConfig);

			netcode_server_t* server = netcode_server_create_overload("127.0.0.1:40000", "[::1]:50000", &serverConfig, 0.0);

			netcode_address_t testAddress;
			netcode_parse_address("127.0.0.1:40000", &testAddress);

			check(server);
			check(server->socketHolder.ipv4.handle != 0);
			check(server->socketHolder.ipv6.handle != 0);
			check(netcode_address_equal(&server->address, &testAddress));

			netcode_server_destroy(server);
		}

		{
			netcode_server_config_t serverConfig;
			netcode_default_server_config(&serverConfig);

			netcode_server_t* server = netcode_server_create_overload("[::1]:50000", "127.0.0.1:40000", &serverConfig, 0.0);

			netcode_address_t testAddress;
			netcode_parse_address("[::1]:50000", &testAddress);

			check(server);
			check(server->socketHolder.ipv4.handle != 0);
			check(server->socketHolder.ipv6.handle != 0);
			check(netcode_address_equal(&server->address, &testAddress));

			netcode_server_destroy(server);
		}
	}


	static u08 privateKey[NETCODE_KEY_BYTES] = { 0x60, 0x6a, 0xbe, 0x6e, 0xc9, 0x19, 0x10, 0xea,
												 0x9a, 0x65, 0x62, 0xf6, 0x6f, 0x2b, 0x30, 0xe4,
												 0x43, 0x71, 0xd6, 0x2c, 0xd1, 0x99, 0x27, 0x26,
												 0x6b, 0x3c, 0x60, 0xf4, 0xb7, 0x15, 0xab, 0xa1 };

	void test_client_server_connect()
	{
		netcode_network_simulator_t* networkSimulator = netcode_network_simulator_create(nullptr, nullptr, nullptr);
		networkSimulator->latencyMilliseconds = 250;
		networkSimulator->jitterMilliseconds = 250;
		networkSimulator->packetLossPercent = 5;
		networkSimulator->duplicatePacketPercent = 10;

		f64 time = 0.0;
		f64 deltaTime = 1.0 / 10.0;

		netcode_client_config_t clientConfig;
		netcode_default_client_config(&clientConfig);
		clientConfig.networkSimulator = networkSimulator;

		netcode_client_t* client = netcode_client_create("[::]:50000", &clientConfig, time);

		check(client);

		netcode_server_config_t serverConfig;
		netcode_default_server_config(&serverConfig);
		serverConfig.protocolId = TEST_PROTOCOL_ID;
		serverConfig.networkSimulator = networkSimulator;
		memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

		netcode_server_t* server = netcode_server_create("[::1]:40000", &serverConfig, time);
		check(server);

		netcode_server_start(server, 1);

		const c08* serverAddress = "[::1]:40000";
		u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

		u64 clientId = 0;
		netcode_random_bytes((u08*)& clientId, 8);

		u08 userData[NETCODE_USER_DATA_BYTES];
		netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

		check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

		netcode_client_connect(client, connectToken);

		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);

			netcode_client_update(client, time);

			netcode_server_update(server, time);

			if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			if (netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED)
				break;

			time += deltaTime;

		}

		check(netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED);
		check(netcode_client_index(client) == 0);
		check(netcode_server_client_connected(server, 0) == 1);
		check(netcode_server_num_connected_clients(server) == 1);

		s32 clientNumPacketsReceived = 0;
		s32 serverNumPacketsReceived = 0;

		u08 packetData[NETCODE_MAX_PACKET_SIZE];
		for (s32 i = 0; i < NETCODE_MAX_PACKET_SIZE; ++i)
		{
			packetData[i] = (u08)i;
		}

		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);

			netcode_client_update(client, time);

			netcode_server_update(server, time);

			netcode_client_send_packet(client, packetData, NETCODE_MAX_PACKET_SIZE);

			netcode_server_send_packet(server, 0, packetData, NETCODE_MAX_PACKET_SIZE);

			while (1)
			{
				s32 packetBytes;
				u64 packetSequence;
				u08* packet = netcode_client_receive_packet(client, &packetBytes, &packetSequence);
				if (!packet)
					break;

				(void)packetSequence;
				gg_assert(packetBytes == NETCODE_MAX_PACKET_SIZE);
				gg_assert(memcmp(packet, packetData, NETCODE_MAX_PACKET_SIZE) == 0);
				++clientNumPacketsReceived;
				netcode_client_free_packet(client, packet);
			}

			while (1)
			{
				s32 packetBytes;
				u64 packetSequence;
				u08* packet = netcode_server_receive_packet(server, 0, &packetBytes, &packetSequence);
				if (!packet)
					break;

				(void)packetSequence;
				gg_assert(packetBytes == NETCODE_MAX_PACKET_SIZE);
				gg_assert(memcmp(packet, packetData, NETCODE_MAX_PACKET_SIZE) == 0);
				++serverNumPacketsReceived;
				netcode_server_free_packet(server, packet);
			}

			if (clientNumPacketsReceived >= 10 && serverNumPacketsReceived >= 10)
			{
				if (netcode_server_client_connected(server, 0))
				{
					netcode_server_disconnect_client(server, 0);
				}
			}

			if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			time += deltaTime;
		}

		check(clientNumPacketsReceived >= 10 && serverNumPacketsReceived >= 10);

		netcode_server_destroy(server);
		netcode_client_destroy(client);

		netcode_network_simulator_destroy(networkSimulator);
	}

	void test_client_server_ipv4_socket_connect()
	{
		{
			f64 time = 0.0;
			f64 deltaTime = 1.0 / 10.0;

			netcode_client_config_t clientConfig;
			netcode_default_client_config(&clientConfig);

			netcode_client_t* client = netcode_client_create("0.0.0.0:50000", &clientConfig, time);

			check(client);

			netcode_server_config_t serverConfig;
			netcode_default_server_config(&serverConfig);
			serverConfig.protocolId = TEST_PROTOCOL_ID;
			memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

			netcode_server_t* server = netcode_server_create("127.0.0.1:40000", &serverConfig, time);
			check(server);

			netcode_server_start(server, 1);

			const c08* serverAddress = "127.0.0.1:40000";
			u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

			u64 clientId = 0;
			netcode_random_bytes((u08*)& clientId, 8);

			u08 userData[NETCODE_USER_DATA_BYTES];
			netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

			check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

			netcode_client_connect(client, connectToken);

			while (1)
			{
				netcode_client_update(client, time);

				netcode_server_update(server, time);

				if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
					break;

				if (netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED)
					break;

				time += deltaTime;
			}

			netcode_server_destroy(server);
			netcode_client_destroy(client);
		}

		{
			f64 time = 0.0;
			f64 deltaTime = 1.0 / 10.0;

			netcode_client_config_t clientConfig;
			netcode_default_client_config(&clientConfig);

			netcode_client_t* client = netcode_client_create("0.0.0.0:50000", &clientConfig, time);

			check(client);

			netcode_server_config_t serverConfig;
			netcode_default_server_config(&serverConfig);
			serverConfig.protocolId = TEST_PROTOCOL_ID;
			memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

			netcode_server_t* server = netcode_server_create_overload("127.0.0.1:40000", "[::1]:40000", &serverConfig, time);
			check(server);

			netcode_server_start(server, 1);

			const c08* serverAddress = "127.0.0.1:40000";
			u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

			u64 clientId = 0;
			netcode_random_bytes((u08*)& clientId, 8);

			u08 userData[NETCODE_USER_DATA_BYTES];
			netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

			check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

			netcode_client_connect(client, connectToken);

			while (1)
			{
				netcode_client_update(client, time);

				netcode_server_update(server, time);

				if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
					break;

				if (netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED)
					break;

				time += deltaTime;
			}

			netcode_server_destroy(server);
			netcode_client_destroy(client);
		}

		{
			f64 time = 0.0;
			f64 deltaTime = 1.0 / 10.0;

			netcode_client_config_t clientConfig;
			netcode_default_client_config(&clientConfig);

			netcode_client_t* client = netcode_client_create_overload("0.0.0.0:50000", "[::]:50000", &clientConfig, time);

			check(client);

			netcode_server_config_t serverConfig;
			netcode_default_server_config(&serverConfig);
			serverConfig.protocolId = TEST_PROTOCOL_ID;
			memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

			netcode_server_t* server = netcode_server_create("127.0.0.1:40000", &serverConfig, time);
			check(server);

			netcode_server_start(server, 1);

			const c08* serverAddress = "127.0.0.1:40000";
			u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

			u64 clientId = 0;
			netcode_random_bytes((u08*)& clientId, 8);

			u08 userData[NETCODE_USER_DATA_BYTES];
			netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

			check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

			netcode_client_connect(client, connectToken);

			while (1)
			{
				netcode_client_update(client, time);

				netcode_server_update(server, time);

				if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
					break;

				if (netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED)
					break;

				time += deltaTime;
			}

			netcode_server_destroy(server);
			netcode_client_destroy(client);
		}


		{
			f64 time = 0.0;
			f64 deltaTime = 1.0 / 10.0;

			netcode_client_config_t clientConfig;
			netcode_default_client_config(&clientConfig);

			netcode_client_t* client = netcode_client_create_overload("0.0.0.0:50000", "[::]:50000", &clientConfig, time);

			check(client);

			netcode_server_config_t serverConfig;
			netcode_default_server_config(&serverConfig);
			serverConfig.protocolId = TEST_PROTOCOL_ID;
			memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

			netcode_server_t* server = netcode_server_create_overload("127.0.0.1:40000", "[::1]:40000", &serverConfig, time);
			check(server);

			netcode_server_start(server, 1);

			const c08* serverAddress = "127.0.0.1:40000";
			u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

			u64 clientId = 0;
			netcode_random_bytes((u08*)& clientId, 8);

			u08 userData[NETCODE_USER_DATA_BYTES];
			netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

			check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

			netcode_client_connect(client, connectToken);

			while (1)
			{
				netcode_client_update(client, time);

				netcode_server_update(server, time);

				if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
					break;

				if (netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED)
					break;

				time += deltaTime;
			}

			netcode_server_destroy(server);
			netcode_client_destroy(client);
		}
	}


	void test_client_server_ipv6_socket_connect()
	{
		{
			f64 time = 0.0;
			f64 deltaTime = 1.0 / 10.0;

			netcode_client_config_t clientConfig;
			netcode_default_client_config(&clientConfig);

			netcode_client_t* client = netcode_client_create("[::]:50000", &clientConfig, time);

			check(client);

			netcode_server_config_t serverConfig;
			netcode_default_server_config(&serverConfig);
			serverConfig.protocolId = TEST_PROTOCOL_ID;
			memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

			netcode_server_t* server = netcode_server_create("[::1]:40000", &serverConfig, time);
			check(server);

			netcode_server_start(server, 1);

			const c08* serverAddress = "[::1]:40000";
			u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

			u64 clientId = 0;
			netcode_random_bytes((u08*)& clientId, 8);

			u08 userData[NETCODE_USER_DATA_BYTES];
			netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

			check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

			netcode_client_connect(client, connectToken);

			while (1)
			{
				netcode_client_update(client, time);

				netcode_server_update(server, time);

				if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
					break;

				if (netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED)
					break;

				time += deltaTime;
			}

			netcode_server_destroy(server);
			netcode_client_destroy(client);
		}

		{
			f64 time = 0.0;
			f64 deltaTime = 1.0 / 10.0;

			netcode_client_config_t clientConfig;
			netcode_default_client_config(&clientConfig);

			netcode_client_t* client = netcode_client_create("[::]:50000", &clientConfig, time);

			check(client);

			netcode_server_config_t serverConfig;
			netcode_default_server_config(&serverConfig);
			serverConfig.protocolId = TEST_PROTOCOL_ID;
			memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

			netcode_server_t* server = netcode_server_create_overload("127.0.0.1:40000", "[::1]:40000", &serverConfig, time);
			check(server);

			netcode_server_start(server, 1);

			const c08* serverAddress = "[::1]:40000";
			u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

			u64 clientId = 0;
			netcode_random_bytes((u08*)& clientId, 8);

			u08 userData[NETCODE_USER_DATA_BYTES];
			netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

			check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

			netcode_client_connect(client, connectToken);

			while (1)
			{
				netcode_client_update(client, time);

				netcode_server_update(server, time);

				if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
					break;

				if (netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED)
					break;

				time += deltaTime;
			}

			netcode_server_destroy(server);
			netcode_client_destroy(client);
		}

		{
			f64 time = 0.0;
			f64 deltaTime = 1.0 / 10.0;

			netcode_client_config_t clientConfig;
			netcode_default_client_config(&clientConfig);

			netcode_client_t* client = netcode_client_create_overload("0.0.0.0:50000", "[::]:50000", &clientConfig, time);

			check(client);

			netcode_server_config_t serverConfig;
			netcode_default_server_config(&serverConfig);
			serverConfig.protocolId = TEST_PROTOCOL_ID;
			memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

			netcode_server_t* server = netcode_server_create("[::1]:40000", &serverConfig, time);
			check(server);

			netcode_server_start(server, 1);

			const c08* serverAddress = "[::1]:40000";
			u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

			u64 clientId = 0;
			netcode_random_bytes((u08*)& clientId, 8);

			u08 userData[NETCODE_USER_DATA_BYTES];
			netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

			check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

			netcode_client_connect(client, connectToken);

			while (1)
			{
				netcode_client_update(client, time);

				netcode_server_update(server, time);

				if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
					break;

				if (netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED)
					break;

				time += deltaTime;
			}

			netcode_server_destroy(server);
			netcode_client_destroy(client);
		}


		{
			f64 time = 0.0;
			f64 deltaTime = 1.0 / 10.0;

			netcode_client_config_t clientConfig;
			netcode_default_client_config(&clientConfig);

			netcode_client_t* client = netcode_client_create_overload("0.0.0.0:50000", "[::]:50000", &clientConfig, time);

			check(client);

			netcode_server_config_t serverConfig;
			netcode_default_server_config(&serverConfig);
			serverConfig.protocolId = TEST_PROTOCOL_ID;
			memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

			netcode_server_t* server = netcode_server_create_overload("127.0.0.1:40000", "[::1]:40000", &serverConfig, time);
			check(server);

			netcode_server_start(server, 1);

			const c08* serverAddress = "[::1]:40000";
			u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

			u64 clientId = 0;
			netcode_random_bytes((u08*)& clientId, 8);

			u08 userData[NETCODE_USER_DATA_BYTES];
			netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

			check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

			netcode_client_connect(client, connectToken);

			while (1)
			{
				netcode_client_update(client, time);

				netcode_server_update(server, time);

				if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
					break;

				if (netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED)
					break;

				time += deltaTime;
			}

			netcode_server_destroy(server);
			netcode_client_destroy(client);
		}
	}

	void test_client_server_keep_alive()
	{
		netcode_network_simulator_t* networkSimulator = netcode_network_simulator_create(nullptr, nullptr, nullptr);
		networkSimulator->latencyMilliseconds = 250;
		networkSimulator->jitterMilliseconds = 250;
		networkSimulator->packetLossPercent = 5;
		networkSimulator->duplicatePacketPercent = 10;

		f64 time = 0.0;
		f64 deltaTime = 1.0 / 10.0;

		netcode_client_config_t clientConfig;
		netcode_default_client_config(&clientConfig);
		clientConfig.networkSimulator = networkSimulator;

		netcode_client_t* client = netcode_client_create("[::]:50000", &clientConfig, time);

		check(client);

		netcode_server_config_t serverConfig;
		netcode_default_server_config(&serverConfig);
		serverConfig.protocolId = TEST_PROTOCOL_ID;
		serverConfig.networkSimulator = networkSimulator;
		memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

		netcode_server_t* server = netcode_server_create("[::1]:40000", &serverConfig, time);
		check(server);

		netcode_server_start(server, 1);

		const c08* serverAddress = "[::1]:40000";
		u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

		u64 clientId = 0;
		netcode_random_bytes((u08*)& clientId, 8);

		u08 userData[NETCODE_USER_DATA_BYTES];
		netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

		check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

		netcode_client_connect(client, connectToken);

		// pump the client and server long enough that they would timeout without keep alive packets
		s32 numIterations = (s32)ceil(1.25 * TEST_TIMEOUT_SECONDS / deltaTime);
		for (s32 i = 0; i < numIterations; ++i)
		{
			netcode_network_simulator_update(networkSimulator, time);

			netcode_client_update(client, time);

			netcode_server_update(server, time);

			if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			time += deltaTime;
		}

		check(netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED);
		check(netcode_client_index(client) == 0);
		check(netcode_server_client_connected(server, 0) == 1);
		check(netcode_server_num_connected_clients(server) == 1);

		netcode_server_destroy(server);
		netcode_client_destroy(client);
		netcode_network_simulator_destroy(networkSimulator);
	}

	void test_client_server_multiple_clients()
	{
#define NUM_START_STOP_ITERATIONS 3

		s32 maxClients[NUM_START_STOP_ITERATIONS] = { 2, 32, 5 };

		netcode_network_simulator_t* networkSimulator = netcode_network_simulator_create(nullptr, nullptr, nullptr);
		networkSimulator->latencyMilliseconds = 250;
		networkSimulator->jitterMilliseconds = 250;
		networkSimulator->packetLossPercent = 5;
		networkSimulator->duplicatePacketPercent = 10;

		f64 time = 0.0;
		f64 deltaTime = 1.0 / 10.0;

		netcode_server_config_t serverConfig;
		netcode_default_server_config(&serverConfig);
		serverConfig.protocolId = TEST_PROTOCOL_ID;
		serverConfig.networkSimulator = networkSimulator;
		memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

		netcode_server_t* server = netcode_server_create("[::1]:40000", &serverConfig, time);
		check(server);

		u64 tokenSequence = 0;

		for (s32 i = 0; i < NUM_START_STOP_ITERATIONS; ++i)
		{
			// start the server with max number of clients for this iteration
			netcode_server_start(server, maxClients[i]);

			// create number of client objects for this iteration and connect to server

			netcode_client_t** client = (netcode_client_t * *)malloc(sizeof(netcode_client_t*) * maxClients[i]);

			check(client);

			for (s32 j = 0; j < maxClients[i]; ++j)
			{
				c08 clientAddress[NETCODE_MAX_ADDRESS_STRING_LENGTH];
				sprintf(clientAddress, "[::]:%d", 50000 + j);

				netcode_client_config_t clientConfig;
				netcode_default_client_config(&clientConfig);
				clientConfig.networkSimulator = networkSimulator;

				client[j] = netcode_client_create(clientAddress, &clientConfig, time);
				check(client[j]);

				u64 clientId = j;
				netcode_random_bytes((u08*)& clientId, 8);

				const c08* serverAddress = "[::1]:40000";
				u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

				u08 userData[NETCODE_USER_DATA_BYTES];
				netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

				check(netcode_generate_connect_token(1, &serverAddress, &serverAddress,
					TEST_CONNECT_TOKEN_EXPIRY,
					TEST_TIMEOUT_SECONDS,
					clientId,
					TEST_PROTOCOL_ID,
					privateKey,
					userData,
					connectToken));

				netcode_client_connect(client[j], connectToken);
			}

			// make sure all clients can connect
			while (1)
			{
				netcode_network_simulator_update(networkSimulator, time);

				for (s32 j = 0; j < maxClients[i]; ++j)
				{
					netcode_client_update(client[j], time);
				}

				netcode_server_update(server, time);

				s32 numConnectedClients = 0;

				for (s32 j = 0; j < maxClients[i]; ++j)
				{
					if (netcode_client_state(client[j]) <= NETCODE_CLIENT_STATE_DISCONNECTED)
						break;
					if (netcode_client_state(client[j]) == NETCODE_CLIENT_STATE_CONNECTED)
						++numConnectedClients;
				}

				if (numConnectedClients == maxClients[i])
					break;

				time += deltaTime;
			}

			check(netcode_server_num_connected_clients(server) == maxClients[i]);

			for (s32 j = 0; j < maxClients[i]; ++j)
			{
				check(netcode_client_state(client[j]) == NETCODE_CLIENT_STATE_CONNECTED);
				check(netcode_server_client_connected(server, j) == 1);
			}

			// make sure all clients can exchange packets with the server
			s32* serverNumPacketsReceived = (s32*)malloc(sizeof(s32) * maxClients[i]);
			s32* clientNumPacketsReceived = (s32*)malloc(sizeof(s32) * maxClients[i]);

			memset(serverNumPacketsReceived, 0, sizeof(s32) * maxClients[i]);
			memset(clientNumPacketsReceived, 0, sizeof(s32) * maxClients[i]);

			u08 packetData[NETCODE_MAX_PACKET_SIZE];
			for (s32 j = 0; j < NETCODE_MAX_PACKET_SIZE; ++j)
				packetData[j] = (u08)j;

			while (1)
			{
				netcode_network_simulator_update(networkSimulator, time);

				for (s32 j = 0; j < maxClients[i]; ++j)
				{
					netcode_client_update(client[j], time);
				}

				netcode_server_update(server, time);

				for (s32 j = 0; j < maxClients[i]; ++j)
				{
					netcode_client_send_packet(client[j], packetData, NETCODE_MAX_PACKET_SIZE);
				}

				for (s32 j = 0; j < maxClients[i]; ++j)
				{
					netcode_server_send_packet(server, j, packetData, NETCODE_MAX_PACKET_SIZE);
				}


				for (s32 j = 0; j < maxClients[i]; ++j)
				{
					while (1)
					{
						s32 packetBytes;
						u64 packetSequence;
						u08* packet = netcode_client_receive_packet(client[j], &packetBytes, &packetSequence);

						if (!packet)
							break;
						(void)packetSequence;
						gg_assert(packetBytes == NETCODE_MAX_PACKET_SIZE);
						gg_assert(memcmp(packet, packetData, NETCODE_MAX_PACKET_SIZE) == 0);

						clientNumPacketsReceived[j]++;
						netcode_client_free_packet(client[j], packet);
					}
				}

				for (s32 j = 0; j < maxClients[i]; ++j)
				{
					while (1)
					{
						s32 packetBytes;
						u64 packetSequence;
						u08* packet = netcode_server_receive_packet(server, j, &packetBytes, &packetSequence);

						if (!packet)
							break;
						(void)packetSequence;
						gg_assert(packetBytes == NETCODE_MAX_PACKET_SIZE);
						gg_assert(memcmp(packet, packetData, NETCODE_MAX_PACKET_SIZE) == 0);

						serverNumPacketsReceived[j]++;
						netcode_server_free_packet(server, packet);
					}
				}


				s32 numClientsReady = 0;

				for (s32 j = 0; j < maxClients[i]; ++j)
				{
					if (clientNumPacketsReceived[j] >= 1 && serverNumPacketsReceived[j] >= 1)
						++numClientsReady;
				}

				if (numClientsReady == maxClients[i])
					break;

				for (s32 j = 0; j < maxClients[i]; ++j)
				{
					if (netcode_client_state(client[j]) <= NETCODE_CLIENT_STATE_DISCONNECTED)
						break;
				}

				time += deltaTime;
			}

			s32 numClientsReady = 0;
			for (s32 j = 0; j < maxClients[i]; ++j)
			{
				if (clientNumPacketsReceived[j] >= 1 && serverNumPacketsReceived[j] >= 1)
					++numClientsReady;
			}

			check(numClientsReady == maxClients[i]);

			free(serverNumPacketsReceived);
			free(clientNumPacketsReceived);

			netcode_network_simulator_reset(networkSimulator);


			for (s32 j = 0; j < maxClients[i]; ++j)
			{
				netcode_client_destroy(client[j]);
			}

			free(client);
			netcode_server_stop(server);
		}

		netcode_server_destroy(server);
		netcode_network_simulator_destroy(networkSimulator);
	}

	void test_client_server_multiple_servers()
	{
		netcode_network_simulator_t* networkSimulator = netcode_network_simulator_create(nullptr, nullptr, nullptr);
		networkSimulator->latencyMilliseconds = 250;
		networkSimulator->jitterMilliseconds = 250;
		networkSimulator->packetLossPercent = 5;
		networkSimulator->duplicatePacketPercent = 10;

		f64 time = 0.0;
		f64 deltaTime = 1.0 / 10.0;

		netcode_client_config_t clientConfig;
		netcode_default_client_config(&clientConfig);
		clientConfig.networkSimulator = networkSimulator;

		netcode_client_t* client = netcode_client_create("[::]:50000", &clientConfig, time);

		check(client);

		netcode_server_config_t serverConfig;
		netcode_default_server_config(&serverConfig);
		serverConfig.protocolId = TEST_PROTOCOL_ID;
		serverConfig.networkSimulator = networkSimulator;
		memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

		netcode_server_t* server = netcode_server_create("[::1]:40000", &serverConfig, time);
		check(server);

		netcode_server_start(server, 1);

		const c08* serverAddress[] = { "10.10.10.10:1000", "100.100.100.100:50000", "[::1]:40000" };
		u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

		u64 clientId = 0;
		netcode_random_bytes((u08*)& clientId, 8);

		u08 userData[NETCODE_USER_DATA_BYTES];
		netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

		check(netcode_generate_connect_token(3, serverAddress, serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

		netcode_client_connect(client, connectToken);

		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);

			netcode_client_update(client, time);

			netcode_server_update(server, time);

			if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			if (netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED)
				break;

			time += deltaTime;
		}

		check(netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED);
		check(netcode_client_index(client) == 0);
		check(netcode_server_client_connected(server, 0) == 1);
		check(netcode_server_num_connected_clients(server) == 1);

		s32 clientNumPacketsReceived = 0;
		s32 serverNumPacketsReceived = 0;

		u08 packetData[NETCODE_MAX_PACKET_SIZE];
		for (s32 i = 0; i < NETCODE_MAX_PACKET_SIZE; ++i)
		{
			packetData[i] = (u08)i;
		}

		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);

			netcode_client_update(client, time);

			netcode_server_update(server, time);

			netcode_client_send_packet(client, packetData, NETCODE_MAX_PACKET_SIZE);

			netcode_server_send_packet(server, 0, packetData, NETCODE_MAX_PACKET_SIZE);

			while (1)
			{
				s32 packetBytes;
				u64 packetSequence;
				u08* packet = netcode_client_receive_packet(client, &packetBytes, &packetSequence);
				if (!packet)
					break;

				(void)packetSequence;
				gg_assert(packetBytes == NETCODE_MAX_PACKET_SIZE);
				gg_assert(memcmp(packet, packetData, NETCODE_MAX_PACKET_SIZE) == 0);
				++clientNumPacketsReceived;
				netcode_client_free_packet(client, packet);
			}

			while (1)
			{
				s32 packetBytes;
				u64 packetSequence;
				u08* packet = netcode_server_receive_packet(server, 0, &packetBytes, &packetSequence);
				if (!packet)
					break;

				(void)packetSequence;
				gg_assert(packetBytes == NETCODE_MAX_PACKET_SIZE);
				gg_assert(memcmp(packet, packetData, NETCODE_MAX_PACKET_SIZE) == 0);
				++serverNumPacketsReceived;
				netcode_server_free_packet(server, packet);
			}

			if (clientNumPacketsReceived >= 10 && serverNumPacketsReceived >= 10)
			{
				if (netcode_server_client_connected(server, 0))
				{
					netcode_server_disconnect_client(server, 0);
				}
			}

			if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			time += deltaTime;
		}

		check(clientNumPacketsReceived >= 10 && serverNumPacketsReceived >= 10);

		netcode_server_destroy(server);
		netcode_client_destroy(client);

		netcode_network_simulator_destroy(networkSimulator);
	}

	void test_client_error_connect_token_expired()
	{
		netcode_network_simulator_t* networkSimulator = netcode_network_simulator_create(nullptr, nullptr, nullptr);
		networkSimulator->latencyMilliseconds = 250;
		networkSimulator->jitterMilliseconds = 250;
		networkSimulator->packetLossPercent = 5;
		networkSimulator->duplicatePacketPercent = 10;

		f64 time = 0.0;

		netcode_client_config_t clientConfig;
		netcode_default_client_config(&clientConfig);
		clientConfig.networkSimulator = networkSimulator;

		netcode_client_t* client = netcode_client_create("[::]:50000", &clientConfig, time);

		check(client);

		const c08* serverAddress = "[::1]:40000";
		u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

		u64 clientId = 0;
		netcode_random_bytes((u08*)& clientId, 8);

		u08 userData[NETCODE_USER_DATA_BYTES];
		netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

		check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, 0, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

		netcode_client_connect(client, connectToken);

		netcode_client_update(client, time);

		check(netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECT_TOKEN_EXPIRED);

		netcode_client_destroy(client);
		netcode_network_simulator_destroy(networkSimulator);
	}

	void test_client_error_invalid_connect_token()
	{
		netcode_network_simulator_t* networkSimulator = netcode_network_simulator_create(nullptr, nullptr, nullptr);
		networkSimulator->latencyMilliseconds = 250;
		networkSimulator->jitterMilliseconds = 250;
		networkSimulator->packetLossPercent = 5;
		networkSimulator->duplicatePacketPercent = 10;

		f64 time = 0.0;

		netcode_client_config_t clientConfig;
		netcode_default_client_config(&clientConfig);
		clientConfig.networkSimulator = networkSimulator;

		netcode_client_t* client = netcode_client_create("[::]:50000", &clientConfig, time);
		check(client);

		u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];
		netcode_random_bytes(connectToken, NETCODE_CONNECT_TOKEN_BYTES);

		u64 clientId = 0;
		netcode_random_bytes((u08*)& clientId, 8);

		netcode_client_connect(client, connectToken);

		check(netcode_client_state(client) == NETCODE_CLIENT_STATE_INVALID_CONNECT_TOKEN);

		netcode_client_destroy(client);
		netcode_network_simulator_destroy(networkSimulator);
	}

	void test_client_error_connection_timed_out()
	{
		netcode_network_simulator_t* networkSimulator = netcode_network_simulator_create(nullptr, nullptr, nullptr);
		networkSimulator->latencyMilliseconds = 250;
		networkSimulator->jitterMilliseconds = 250;
		networkSimulator->packetLossPercent = 5;
		networkSimulator->duplicatePacketPercent = 10;

		f64 time = 0.0;
		f64 deltaTime = 1.0 / 10.0;

		// connect a client to the server
		netcode_client_config_t clientConfig;
		netcode_default_client_config(&clientConfig);
		clientConfig.networkSimulator = networkSimulator;

		netcode_client_t* client = netcode_client_create("[::]:50000", &clientConfig, time);
		check(client);

		netcode_server_config_t serverConfig;
		netcode_default_server_config(&serverConfig);
		serverConfig.protocolId = TEST_PROTOCOL_ID;
		serverConfig.networkSimulator = networkSimulator;
		memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

		netcode_server_t* server = netcode_server_create("[::1]:40000", &serverConfig, time);
		check(server);

		netcode_server_start(server, 1);

		const c08* serverAddress = "[::1]:40000";
		u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

		u64 clientId = 0;
		netcode_random_bytes((u08*)& clientId, 8);

		u08 userData[NETCODE_USER_DATA_BYTES];
		netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

		check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

		netcode_client_connect(client, connectToken);

		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);

			netcode_client_update(client, time);

			netcode_server_update(server, time);

			if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			if (netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED)
				break;

			time += deltaTime;
		}

		check(netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED);
		check(netcode_client_index(client) == 0);
		check(netcode_server_client_connected(server, 0) == 1);
		check(netcode_server_num_connected_clients(server) == 1);

		// now disable updating the server and verify that the client times out
		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);
			netcode_client_update(client, time);

			if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			time += deltaTime;
		}
		check(netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTION_TIMED_OUT);

		netcode_server_destroy(server);
		netcode_client_destroy(client);
		netcode_network_simulator_destroy(networkSimulator);
	}

	void test_client_error_connection_response_timeout()
	{
		netcode_network_simulator_t* networkSimulator = netcode_network_simulator_create(nullptr, nullptr, nullptr);
		networkSimulator->latencyMilliseconds = 250;
		networkSimulator->jitterMilliseconds = 250;
		networkSimulator->packetLossPercent = 5;
		networkSimulator->duplicatePacketPercent = 10;

		f64 time = 0.0;
		f64 deltaTime = 1.0 / 10.0;

		// connect a client to the server
		netcode_client_config_t clientConfig;
		netcode_default_client_config(&clientConfig);
		clientConfig.networkSimulator = networkSimulator;

		netcode_client_t* client = netcode_client_create("[::]:50000", &clientConfig, time);
		check(client);

		netcode_server_config_t serverConfig;
		netcode_default_server_config(&serverConfig);
		serverConfig.protocolId = TEST_PROTOCOL_ID;
		serverConfig.networkSimulator = networkSimulator;
		memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

		netcode_server_t* server = netcode_server_create("[::1]:40000", &serverConfig, time);
		check(server);

		server->flags = NETCODE_SERVER_FLAG_IGNORE_CONNECTION_RESPONSE_PACKETS;

		netcode_server_start(server, 1);

		const c08* serverAddress = "[::1]:40000";
		u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

		u64 clientId = 0;
		netcode_random_bytes((u08*)& clientId, 8);

		u08 userData[NETCODE_USER_DATA_BYTES];
		netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

		check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

		netcode_client_connect(client, connectToken);

		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);

			netcode_client_update(client, time);

			netcode_server_update(server, time);

			if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			if (netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED)
				break;

			time += deltaTime;
		}

		check(netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTION_RESPONSE_TIMED_OUT);

		netcode_server_destroy(server);
		netcode_client_destroy(client);
		netcode_network_simulator_destroy(networkSimulator);
	}

	void test_client_error_connection_request_timeout()
	{
		netcode_network_simulator_t* networkSimulator = netcode_network_simulator_create(nullptr, nullptr, nullptr);
		networkSimulator->latencyMilliseconds = 250;
		networkSimulator->jitterMilliseconds = 250;
		networkSimulator->packetLossPercent = 5;
		networkSimulator->duplicatePacketPercent = 10;

		f64 time = 0.0;
		f64 deltaTime = 1.0 / 60.0;

		// connect a client to the server
		netcode_client_config_t clientConfig;
		netcode_default_client_config(&clientConfig);
		clientConfig.networkSimulator = networkSimulator;

		netcode_client_t* client = netcode_client_create("[::]:50000", &clientConfig, time);
		check(client);

		netcode_server_config_t serverConfig;
		netcode_default_server_config(&serverConfig);
		serverConfig.protocolId = TEST_PROTOCOL_ID;
		serverConfig.networkSimulator = networkSimulator;
		memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

		netcode_server_t* server = netcode_server_create("[::1]:40000", &serverConfig, time);
		check(server);

		server->flags = NETCODE_SERVER_FLAG_IGNORE_CONNECTION_REQUEST_PACKETS;

		netcode_server_start(server, 1);

		const c08* serverAddress = "[::1]:40000";
		u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

		u64 clientId = 0;
		netcode_random_bytes((u08*)& clientId, 8);

		u08 userData[NETCODE_USER_DATA_BYTES];
		netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

		check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

		netcode_client_connect(client, connectToken);

		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);

			netcode_client_update(client, time);

			netcode_server_update(server, time);

			if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			if (netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED)
				break;

			time += deltaTime;
		}

		check(netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTION_REQUEST_TIMED_OUT);

		netcode_server_destroy(server);
		netcode_client_destroy(client);
		netcode_network_simulator_destroy(networkSimulator);
	}


	void test_client_error_connection_denied()
	{
		netcode_network_simulator_t* networkSimulator = netcode_network_simulator_create(nullptr, nullptr, nullptr);
		networkSimulator->latencyMilliseconds = 250;
		networkSimulator->jitterMilliseconds = 250;
		networkSimulator->packetLossPercent = 5;
		networkSimulator->duplicatePacketPercent = 10;

		f64 time = 0.0;
		f64 deltaTime = 1.0 / 10.0;

		// connect a client to the server
		netcode_client_config_t clientConfig;
		netcode_default_client_config(&clientConfig);
		clientConfig.networkSimulator = networkSimulator;

		netcode_client_t* client = netcode_client_create("[::]:50000", &clientConfig, time);
		check(client);

		netcode_server_config_t serverConfig;
		netcode_default_server_config(&serverConfig);
		serverConfig.protocolId = TEST_PROTOCOL_ID;
		serverConfig.networkSimulator = networkSimulator;
		memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

		netcode_server_t* server = netcode_server_create("[::1]:40000", &serverConfig, time);
		check(server);

		netcode_server_start(server, 1);

		const c08* serverAddress = "[::1]:40000";
		u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

		u64 clientId = 0;
		netcode_random_bytes((u08*)& clientId, 8);

		u08 userData[NETCODE_USER_DATA_BYTES];
		netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

		check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

		netcode_client_connect(client, connectToken);

		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);

			netcode_client_update(client, time);

			netcode_server_update(server, time);

			if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			if (netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED)
				break;

			time += deltaTime;
		}

		check(netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED);
		check(netcode_client_index(client) == 0);
		check(netcode_server_client_connected(server, 0) == 1);
		check(netcode_server_num_connected_clients(server) == 1);

		// now attempt to connect a second client.  The connection should be denied because max clients on server is 1
		netcode_client_t* client2 = netcode_client_create("[::]:50001", &clientConfig, time);
		check(client2);

		u08 connectToken2[NETCODE_CONNECT_TOKEN_BYTES];

		u64 clientId2 = 0;
		netcode_random_bytes((u08*)& clientId2, 8);

		u08 userData2[NETCODE_USER_DATA_BYTES];
		netcode_random_bytes(userData2, NETCODE_USER_DATA_BYTES);

		check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId2, TEST_PROTOCOL_ID, privateKey, userData2, connectToken2));

		netcode_client_connect(client2, connectToken2);

		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);

			netcode_client_update(client, time);
			netcode_client_update(client2, time);

			netcode_server_update(server, time);

			if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			if (netcode_client_state(client2) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			time += deltaTime;
		}

		check(netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED);
		check(netcode_client_state(client2) == NETCODE_CLIENT_STATE_CONNECTION_DENIED);
		check(netcode_server_client_connected(server, 0) == 1);
		check(netcode_server_num_connected_clients(server) == 1);

		netcode_server_destroy(server);
		netcode_client_destroy(client);
		netcode_client_destroy(client2);
		netcode_network_simulator_destroy(networkSimulator);
	}

	void test_client_side_disconnect()
	{
		netcode_network_simulator_t* networkSimulator = netcode_network_simulator_create(nullptr, nullptr, nullptr);
		networkSimulator->latencyMilliseconds = 250;
		networkSimulator->jitterMilliseconds = 250;
		networkSimulator->packetLossPercent = 5;
		networkSimulator->duplicatePacketPercent = 10;

		f64 time = 0.0;
		f64 deltaTime = 1.0 / 10.0;

		// connect a client to the server
		netcode_client_config_t clientConfig;
		netcode_default_client_config(&clientConfig);
		clientConfig.networkSimulator = networkSimulator;

		netcode_client_t* client = netcode_client_create("[::]:50000", &clientConfig, time);
		check(client);

		netcode_server_config_t serverConfig;
		netcode_default_server_config(&serverConfig);
		serverConfig.protocolId = TEST_PROTOCOL_ID;
		serverConfig.networkSimulator = networkSimulator;
		memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

		netcode_server_t* server = netcode_server_create("[::1]:40000", &serverConfig, time);
		check(server);

		netcode_server_start(server, 1);

		const c08* serverAddress = "[::1]:40000";
		u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

		u64 clientId = 0;
		netcode_random_bytes((u08*)& clientId, 8);

		u08 userData[NETCODE_USER_DATA_BYTES];
		netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

		check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

		netcode_client_connect(client, connectToken);

		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);
			netcode_client_update(client, time);
			netcode_server_update(server, time);

			if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			if (netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED)
				break;

			time += deltaTime;
		}

		check(netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED);
		check(netcode_client_index(client) == 0);
		check(netcode_server_client_connected(server, 0) == 1);
		check(netcode_server_num_connected_clients(server) == 1);

		// disconnect client and verify that the server sees the cient disconnected cleanly, rather than timing out
		netcode_client_disconnect(client);

		for (s32 i = 0; i < 10; ++i)
		{
			netcode_network_simulator_update(networkSimulator, time);
			netcode_client_update(client, time);
			netcode_server_update(server, time);

			if (netcode_server_client_connected(server, 0) == 0)
				break;

			time += deltaTime;
		}

		check(netcode_server_client_connected(server, 0) == 0);
		check(netcode_server_num_connected_clients(server) == 0);

		netcode_server_destroy(server);
		netcode_client_destroy(client);
		netcode_network_simulator_destroy(networkSimulator);
	}

	void test_server_side_disconnect()
	{
		netcode_network_simulator_t* networkSimulator = netcode_network_simulator_create(nullptr, nullptr, nullptr);
		//networkSimulator->latencyMilliseconds = 250;
		//networkSimulator->jitterMilliseconds = 250;
		//networkSimulator->packetLossPercent = 5;
		//networkSimulator->duplicatePacketPercent = 10;

		f64 time = 0.0;
		f64 deltaTime = 1.0 / 10.0;

		// connect one client to a server
		netcode_client_config_t clientConfig;
		netcode_default_client_config(&clientConfig);
		clientConfig.networkSimulator = networkSimulator;

		netcode_client_t* client = netcode_client_create("[::]:50000", &clientConfig, time);
		check(client);

		netcode_server_config_t serverConfig;
		netcode_default_server_config(&serverConfig);
		serverConfig.protocolId = TEST_PROTOCOL_ID;
		serverConfig.networkSimulator = networkSimulator;
		memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

		netcode_server_t* server = netcode_server_create("[::1]:40000", &serverConfig, time);
		check(server);

		netcode_server_start(server, 1);

		const c08* serverAddress = "[::1]:40000";
		u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

		u64 clientId = 0;
		netcode_random_bytes((u08*)& clientId, 8);

		u08 userData[NETCODE_USER_DATA_BYTES];
		netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

		check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

		netcode_client_connect(client, connectToken);

		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);
			netcode_client_update(client, time);
			netcode_server_update(server, time);

			if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			if (netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED)
				break;

			time += deltaTime;
		}

		check(netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED);
		check(netcode_client_index(client) == 0);
		check(netcode_server_client_connected(server, 0) == 1);
		check(netcode_server_num_connected_clients(server) == 1);

		// disconnect server side and verify that the client disconnects cleanly, rather than timing out
		netcode_server_disconnect_client(server, 0);

		for (s32 i = 0; i < 10; ++i)
		{
			netcode_network_simulator_update(networkSimulator, time);
			netcode_client_update(client, time);
			netcode_server_update(server, time);

			if (netcode_client_state(client) == NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			time += deltaTime;
		}

		check(netcode_client_state(client) == NETCODE_CLIENT_STATE_DISCONNECTED);
		check(netcode_server_client_connected(server, 0) == 0);
		check(netcode_server_num_connected_clients(server) == 0);

		netcode_server_destroy(server);
		netcode_client_destroy(client);
		netcode_network_simulator_destroy(networkSimulator);
	}

	void test_client_reconnect()
	{
		netcode_network_simulator_t* networkSimulator = netcode_network_simulator_create(nullptr, nullptr, nullptr);
		networkSimulator->latencyMilliseconds = 250;
		networkSimulator->jitterMilliseconds = 250;
		networkSimulator->packetLossPercent = 5;
		networkSimulator->duplicatePacketPercent = 10;

		f64 time = 0.0;
		f64 deltaTime = 1.0 / 10.0;

		// connect one client to a server
		netcode_client_config_t clientConfig;
		netcode_default_client_config(&clientConfig);
		clientConfig.networkSimulator = networkSimulator;

		netcode_client_t* client = netcode_client_create("[::]:50000", &clientConfig, time);
		check(client);

		netcode_server_config_t serverConfig;
		netcode_default_server_config(&serverConfig);
		serverConfig.protocolId = TEST_PROTOCOL_ID;
		serverConfig.networkSimulator = networkSimulator;
		memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

		netcode_server_t* server = netcode_server_create("[::1]:40000", &serverConfig, time);
		check(server);

		netcode_server_start(server, 1);

		const c08* serverAddress = "[::1]:40000";
		u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

		u64 clientId = 0;
		netcode_random_bytes((u08*)& clientId, 8);

		u08 userData[NETCODE_USER_DATA_BYTES];
		netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

		check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

		netcode_client_connect(client, connectToken);

		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);
			netcode_client_update(client, time);
			netcode_server_update(server, time);

			if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			if (netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED)
				break;

			time += deltaTime;
		}

		check(netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED);
		check(netcode_client_index(client) == 0);
		check(netcode_server_client_connected(server, 0) == 1);
		check(netcode_server_num_connected_clients(server) == 1);

		// disconnect client on the server side and wait until client sees the disconnect
		netcode_network_simulator_reset(networkSimulator);
		netcode_server_disconnect_client(server, 0);

		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);
			netcode_client_update(client, time);
			netcode_server_update(server, time);

			if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			time += deltaTime;
		}

		check(netcode_client_state(client) == NETCODE_CLIENT_STATE_DISCONNECTED);
		check(netcode_server_client_connected(server, 0) == 0);
		check(netcode_server_num_connected_clients(server) == 0);

		// now reconnect the client and verify they connect
		netcode_network_simulator_reset(networkSimulator);

		check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

		netcode_client_connect(client, connectToken);

		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);
			netcode_client_update(client, time);
			netcode_server_update(server, time);

			if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			if (netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED)
				break;

			time += deltaTime;
		}

		check(netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED);
		check(netcode_client_index(client) == 0);
		check(netcode_server_client_connected(server, 0) == 1);
		check(netcode_server_num_connected_clients(server) == 1);

		netcode_server_destroy(server);
		netcode_client_destroy(client);
		netcode_network_simulator_destroy(networkSimulator);
	}


	struct test_loopback_context_t
	{
		netcode_client_t* client;
		netcode_server_t* server;
		s32 numLoopbackPacketsSentToClient;
		s32 numLoopbackPacketsSentToServer;
	};

	void client_send_loopback_packet_callback(void* context_, s32 clientIndex, const u08 * packetData, s32 packetBytes, u64 packetSequence)
	{
		(void)packetSequence;
		check(context_);
		check(clientIndex == 0);
		check(packetData);
		check(packetBytes == NETCODE_MAX_PACKET_SIZE);
		for (s32 i = 0; i < packetBytes; ++i)
		{
			check(packetData[i] == (u08)i);
		}
		test_loopback_context_t* context = (test_loopback_context_t*)context_;
		context->numLoopbackPacketsSentToServer++;
		netcode_server_process_loopback_packet(context->server, clientIndex, packetData, packetBytes, packetSequence);
	}
	void server_send_loopback_packet_callback(void* context_, s32 clientIndex, const u08 * packetData, s32 packetBytes, u64 packetSequence)
	{
		(void)packetSequence;
		check(context_);
		check(clientIndex == 0);
		check(packetData);
		check(packetBytes == NETCODE_MAX_PACKET_SIZE);
		for (s32 i = 0; i < packetBytes; ++i)
		{
			check(packetData[i] == (u08)i);
		}
		test_loopback_context_t* context = (test_loopback_context_t*)context_;
		context->numLoopbackPacketsSentToClient++;
		netcode_client_process_loopback_packet(context->client, packetData, packetBytes, packetSequence);
	}

	void test_disable_timeout()
	{
		netcode_network_simulator_t* networkSimulator = netcode_network_simulator_create(nullptr, nullptr, nullptr);
		networkSimulator->latencyMilliseconds = 250;
		networkSimulator->jitterMilliseconds = 250;
		networkSimulator->packetLossPercent = 5;
		networkSimulator->duplicatePacketPercent = 10;

		f64 time = 0.0;
		f64 deltaTime = 1.0 / 10.0;

		// connect one client to a server
		netcode_client_config_t clientConfig;
		netcode_default_client_config(&clientConfig);
		clientConfig.networkSimulator = networkSimulator;

		netcode_client_t* client = netcode_client_create("[::]:50000", &clientConfig, time);
		check(client);

		netcode_server_config_t serverConfig;
		netcode_default_server_config(&serverConfig);
		serverConfig.protocolId = TEST_PROTOCOL_ID;
		serverConfig.networkSimulator = networkSimulator;
		memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

		netcode_server_t* server = netcode_server_create("[::1]:40000", &serverConfig, time);
		check(server);

		netcode_server_start(server, 1);

		const c08* serverAddress = "[::1]:40000";
		u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

		u64 clientId = 0;
		netcode_random_bytes((u08*)& clientId, 8);

		u08 userData[NETCODE_USER_DATA_BYTES];
		netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

		check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, -1, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

		netcode_client_connect(client, connectToken);

		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);
			netcode_client_update(client, time);
			netcode_server_update(server, time);

			if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			if (netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED)
				break;

			time += deltaTime;
		}

		check(netcode_client_state(client) == NETCODE_CLIENT_STATE_CONNECTED);
		check(netcode_client_index(client) == 0);
		check(netcode_server_client_connected(server, 0) == 1);
		check(netcode_server_num_connected_clients(server) == 1);

		s32 serverNumPacketsReceived = 0;
		s32 clientNumPacketsReceived = 0;

		u08 packetData[NETCODE_MAX_PACKET_SIZE];
		for (s32 i = 0; i < NETCODE_MAX_PACKET_SIZE; ++i)
			packetData[i] = (u08)i;

		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);
			netcode_client_update(client, time);
			netcode_server_update(server, time);

			netcode_client_send_packet(client, packetData, NETCODE_MAX_PACKET_SIZE);
			netcode_server_send_packet(server, 0, packetData, NETCODE_MAX_PACKET_SIZE);

			while (1)
			{
				s32 packetBytes;
				u64 packetSequence;
				u08* packet = netcode_client_receive_packet(client, &packetBytes, &packetSequence);
				if (!packet)
					break;
				(void)packetSequence;
				gg_assert(packetBytes == NETCODE_MAX_PACKET_SIZE);
				gg_assert(memcmp(packet, packetData, NETCODE_MAX_PACKET_SIZE) == 0);
				clientNumPacketsReceived++;
				netcode_client_free_packet(client, packet);
			}

			while (1)
			{
				s32 packetBytes;
				u64 packetSequence;
				u08* packet = netcode_server_receive_packet(server, 0, &packetBytes, &packetSequence);
				if (!packet)
					break;
				(void)packetSequence;
				gg_assert(packetBytes == NETCODE_MAX_PACKET_SIZE);
				gg_assert(memcmp(packet, packetData, NETCODE_MAX_PACKET_SIZE) == 0);
				serverNumPacketsReceived++;
				netcode_server_free_packet(server, packet);
			}

			if (clientNumPacketsReceived >= 10 && serverNumPacketsReceived >= 10)
			{
				if (netcode_server_client_connected(server, 0))
				{
					netcode_server_disconnect_client(server, 0);
				}
			}

			if (netcode_client_state(client) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			time += 1000.0; // normally this would timeout the client
		}

		check(clientNumPacketsReceived >= 10 && serverNumPacketsReceived >= 10);

		netcode_server_destroy(server);
		netcode_client_destroy(client);
		netcode_network_simulator_destroy(networkSimulator);
	}

	void test_loopback()
	{
		test_loopback_context_t context;
		memset(&context, 0, sizeof(context));

		netcode_network_simulator_t* networkSimulator = netcode_network_simulator_create(nullptr, nullptr, nullptr);
		networkSimulator->latencyMilliseconds = 250;
		networkSimulator->jitterMilliseconds = 250;
		networkSimulator->packetLossPercent = 5;
		networkSimulator->duplicatePacketPercent = 10;

		f64 time = 0.0;
		f64 deltaTime = 1.0 / 10.0;

		netcode_server_config_t serverConfig;
		netcode_default_server_config(&serverConfig);
		serverConfig.protocolId = TEST_PROTOCOL_ID;
		serverConfig.networkSimulator = networkSimulator;
		serverConfig.callbackContext = &context;
		serverConfig.send_loopback_packet_callback = server_send_loopback_packet_callback;
		memcpy(&serverConfig.privateKey, privateKey, NETCODE_KEY_BYTES);

		netcode_server_t* server = netcode_server_create("[::1]:40000", &serverConfig, time);
		check(server);


		s32 maxClients = 2;
		netcode_server_start(server, maxClients);

		context.server = server;

		// connect a loopback client in slot 0
		netcode_client_config_t clientConfig;
		netcode_default_client_config(&clientConfig);
		clientConfig.networkSimulator = networkSimulator;
		clientConfig.callbackContext = &context;
		clientConfig.send_loopback_packet_callback = client_send_loopback_packet_callback;

		netcode_client_t* loopbackClient = netcode_client_create("[::]:50000", &clientConfig, time);
		check(loopbackClient);

		netcode_client_connect_loopback(loopbackClient, 0, maxClients);

		context.client = loopbackClient;

		check(netcode_client_index(loopbackClient) == 0);
		check(netcode_client_loopback(loopbackClient) == 1);
		check(netcode_client_max_clients(loopbackClient) == maxClients);
		check(netcode_client_state(loopbackClient) == NETCODE_CLIENT_STATE_CONNECTED);


		u64 clientId = 0;
		netcode_random_bytes((u08*)& clientId, 8);
		netcode_server_connect_loopback_client(server, 0, clientId, nullptr);

		check(netcode_server_client_loopback(server, 0) == 1);
		check(netcode_server_client_connected(server, 0) == 1);
		check(netcode_server_num_connected_clients(server) == 1);

		// connect a regular client in the other available server slot
		netcode_client_t* regularClient = netcode_client_create("[::]:50001", &clientConfig, time);
		check(regularClient);
		const c08* serverAddress = "[::1]:40000";
		u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];
		netcode_random_bytes((u08*)& clientId, 8);

		u08 userData[NETCODE_USER_DATA_BYTES];
		netcode_random_bytes(userData, NETCODE_USER_DATA_BYTES);

		check(netcode_generate_connect_token(1, &serverAddress, &serverAddress, TEST_CONNECT_TOKEN_EXPIRY, TEST_TIMEOUT_SECONDS, clientId, TEST_PROTOCOL_ID, privateKey, userData, connectToken));

		netcode_client_connect(regularClient, connectToken);

		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);
			netcode_client_update(regularClient, time);
			netcode_server_update(server, time);

			if (netcode_client_state(regularClient) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			if (netcode_client_state(regularClient) == NETCODE_CLIENT_STATE_CONNECTED)
				break;

			time += deltaTime;
		}

		check(netcode_client_state(regularClient) == NETCODE_CLIENT_STATE_CONNECTED);
		check(netcode_client_index(regularClient) == 1);
		check(netcode_server_client_connected(server, 0) == 1);
		check(netcode_server_client_connected(server, 1) == 1);
		check(netcode_server_client_loopback(server, 0) == 1);
		check(netcode_server_client_loopback(server, 1) == 0);
		check(netcode_server_num_connected_clients(server) == 2);

		// test that we can exchange packets for the regular client and the loopback client
		s32 loopbackClientNumPacketsReceived = 0;
		s32 loopbackServerNumPacketsReceived = 0;
		s32 regularClientNumPacketsReceived = 0;
		s32 regularServerNumPacketsReceived = 0;

		u08 packetData[NETCODE_MAX_PACKET_SIZE];
		for (s32 i = 0; i < NETCODE_MAX_PACKET_SIZE; ++i)
			packetData[i] = (u08)i;

		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);
			netcode_client_update(regularClient, time);
			netcode_server_update(server, time);

			netcode_client_send_packet(loopbackClient, packetData, NETCODE_MAX_PACKET_SIZE);
			netcode_client_send_packet(regularClient, packetData, NETCODE_MAX_PACKET_SIZE);

			netcode_server_send_packet(server, 0, packetData, NETCODE_MAX_PACKET_SIZE);
			netcode_server_send_packet(server, 1, packetData, NETCODE_MAX_PACKET_SIZE);

			while (1)
			{
				s32 packetBytes;
				u64 packetSequence;
				u08* packet = netcode_client_receive_packet(loopbackClient, &packetBytes, &packetSequence);
				if (!packet)
					break;
				(void)packetSequence;
				gg_assert(packetBytes == NETCODE_MAX_PACKET_SIZE);
				gg_assert(memcmp(packet, packetData, NETCODE_MAX_PACKET_SIZE) == 0);
				loopbackClientNumPacketsReceived++;
				netcode_client_free_packet(loopbackClient, packet);
			}

			while (1)
			{
				s32 packetBytes;
				u64 packetSequence;
				u08* packet = netcode_client_receive_packet(regularClient, &packetBytes, &packetSequence);
				if (!packet)
					break;
				(void)packetSequence;
				gg_assert(packetBytes == NETCODE_MAX_PACKET_SIZE);
				gg_assert(memcmp(packet, packetData, NETCODE_MAX_PACKET_SIZE) == 0);
				regularClientNumPacketsReceived++;
				netcode_client_free_packet(regularClient, packet);
			}

			while (1)
			{
				s32 packetBytes;
				u64 packetSequence;
				void* packet = netcode_server_receive_packet(server, 0, &packetBytes, &packetSequence);
				if (!packet)
					break;
				(void)packetSequence;
				gg_assert(packetBytes == NETCODE_MAX_PACKET_SIZE);
				gg_assert(memcmp(packet, packetData, NETCODE_MAX_PACKET_SIZE) == 0);
				loopbackServerNumPacketsReceived++;
				netcode_server_free_packet(server, packet);
			}

			while (1)
			{
				s32 packetBytes;
				u64 packetSequence;
				void* packet = netcode_server_receive_packet(server, 1, &packetBytes, &packetSequence);
				if (!packet)
					break;
				(void)packetSequence;
				gg_assert(packetBytes == NETCODE_MAX_PACKET_SIZE);
				gg_assert(memcmp(packet, packetData, NETCODE_MAX_PACKET_SIZE) == 0);
				regularServerNumPacketsReceived++;
				netcode_server_free_packet(server, packet);
			}

			if (loopbackClientNumPacketsReceived >= 10 && loopbackServerNumPacketsReceived >= 10
				&& regularClientNumPacketsReceived >= 10 && regularServerNumPacketsReceived >= 10)
				break;

			if (netcode_client_state(regularClient) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			time += deltaTime;
		}

		check(loopbackClientNumPacketsReceived >= 10);
		check(loopbackServerNumPacketsReceived >= 10);
		check(regularClientNumPacketsReceived >= 10);
		check(regularServerNumPacketsReceived >= 10);
		check(context.numLoopbackPacketsSentToClient >= 10);
		check(context.numLoopbackPacketsSentToServer >= 10);

		// verify that we can disconnect the loopback client
		check(netcode_server_client_loopback(server, 0) == 1);
		check(netcode_server_client_connected(server, 0) == 1);
		check(netcode_server_num_connected_clients(server) == 2);

		netcode_server_disconnect_loopback_client(server, 0);

		check(netcode_server_client_loopback(server, 0) == 0);
		check(netcode_server_client_connected(server, 0) == 0);
		check(netcode_server_num_connected_clients(server) == 1);

		netcode_client_disconnect_loopback(loopbackClient);

		check(netcode_client_state(loopbackClient) == NETCODE_CLIENT_STATE_DISCONNECTED);

		// verify that we can reconnect the loopback client
		netcode_random_bytes((u08*)& clientId, 8);
		netcode_server_connect_loopback_client(server, 0, clientId, nullptr);

		check(netcode_server_client_loopback(server, 0) == 1);
		check(netcode_server_client_loopback(server, 1) == 0);
		check(netcode_server_client_connected(server, 0) == 1);
		check(netcode_server_client_connected(server, 1) == 1);
		check(netcode_server_num_connected_clients(server) == 2);

		netcode_client_connect_loopback(loopbackClient, 0, maxClients);

		check(netcode_client_index(loopbackClient) == 0);
		check(netcode_client_loopback(loopbackClient) == 1);
		check(netcode_client_max_clients(loopbackClient) == maxClients);
		check(netcode_client_state(loopbackClient) == NETCODE_CLIENT_STATE_CONNECTED);

		// verify that we can exchange packets for both regualr and loopback clients post reconnect
		loopbackClientNumPacketsReceived = 0;
		loopbackServerNumPacketsReceived = 0;
		regularClientNumPacketsReceived = 0;
		regularServerNumPacketsReceived = 0;
		context.numLoopbackPacketsSentToClient = 0;
		context.numLoopbackPacketsSentToServer = 0;

		while (1)
		{
			netcode_network_simulator_update(networkSimulator, time);
			netcode_client_update(regularClient, time);
			netcode_server_update(server, time);

			netcode_client_send_packet(loopbackClient, packetData, NETCODE_MAX_PACKET_SIZE);
			netcode_client_send_packet(regularClient, packetData, NETCODE_MAX_PACKET_SIZE);

			netcode_server_send_packet(server, 0, packetData, NETCODE_MAX_PACKET_SIZE);
			netcode_server_send_packet(server, 1, packetData, NETCODE_MAX_PACKET_SIZE);

			while (1)
			{
				s32 packetBytes;
				u64 packetSequence;
				u08* packet = netcode_client_receive_packet(loopbackClient, &packetBytes, &packetSequence);
				if (!packet)
					break;
				(void)packetSequence;
				gg_assert(packetBytes == NETCODE_MAX_PACKET_SIZE);
				gg_assert(memcmp(packet, packetData, NETCODE_MAX_PACKET_SIZE) == 0);
				loopbackClientNumPacketsReceived++;
				netcode_client_free_packet(loopbackClient, packet);
			}

			while (1)
			{
				s32 packetBytes;
				u64 packetSequence;
				u08* packet = netcode_client_receive_packet(regularClient, &packetBytes, &packetSequence);
				if (!packet)
					break;
				(void)packetSequence;
				gg_assert(packetBytes == NETCODE_MAX_PACKET_SIZE);
				gg_assert(memcmp(packet, packetData, NETCODE_MAX_PACKET_SIZE) == 0);
				regularClientNumPacketsReceived++;
				netcode_client_free_packet(regularClient, packet);
			}

			while (1)
			{
				s32 packetBytes;
				u64 packetSequence;
				void* packet = netcode_server_receive_packet(server, 0, &packetBytes, &packetSequence);
				if (!packet)
					break;
				(void)packetSequence;
				gg_assert(packetBytes == NETCODE_MAX_PACKET_SIZE);
				gg_assert(memcmp(packet, packetData, NETCODE_MAX_PACKET_SIZE) == 0);
				loopbackServerNumPacketsReceived++;
				netcode_server_free_packet(server, packet);
			}

			while (1)
			{
				s32 packetBytes;
				u64 packetSequence;
				void* packet = netcode_server_receive_packet(server, 1, &packetBytes, &packetSequence);
				if (!packet)
					break;
				(void)packetSequence;
				gg_assert(packetBytes == NETCODE_MAX_PACKET_SIZE);
				gg_assert(memcmp(packet, packetData, NETCODE_MAX_PACKET_SIZE) == 0);
				regularServerNumPacketsReceived++;
				netcode_server_free_packet(server, packet);
			}

			if (loopbackClientNumPacketsReceived >= 10 && loopbackServerNumPacketsReceived >= 10
				&& regularClientNumPacketsReceived >= 10 && regularServerNumPacketsReceived >= 10)
				break;

			if (netcode_client_state(regularClient) <= NETCODE_CLIENT_STATE_DISCONNECTED)
				break;

			time += deltaTime;
		}

		check(loopbackClientNumPacketsReceived >= 10);
		check(loopbackServerNumPacketsReceived >= 10);
		check(regularClientNumPacketsReceived >= 10);
		check(regularServerNumPacketsReceived >= 10);
		check(context.numLoopbackPacketsSentToClient >= 10);
		check(context.numLoopbackPacketsSentToServer >= 10);

		// verify that regular client times out but loopback client doesn't
		time += 100000.0;

		netcode_server_update(server, time);

		check(netcode_server_client_connected(server, 0) == 1);
		check(netcode_server_client_connected(server, 1) == 0);

		netcode_client_update(loopbackClient, time);

		check(netcode_client_state(loopbackClient) == NETCODE_CLIENT_STATE_CONNECTED);

		// verify that disconnect all clients leaves loopback clients left alone
		netcode_server_disconnect_all_clients(server);

		check(netcode_server_client_connected(server, 0) == 1);
		check(netcode_server_client_connected(server, 1) == 0);
		check(netcode_server_client_loopback(server, 0) == 1);

		//cleanup
		netcode_client_destroy(regularClient);
		netcode_client_destroy(loopbackClient);
		netcode_server_destroy(server);
		netcode_network_simulator_destroy(networkSimulator);
	}

#define RUN_TEST(test_function)  \
  do                             \
  {                              \
    printf(#test_function "\n"); \
    test_function();             \
  }                              \
  while(0)

	void netcode_test()
	{
		//while ( 1 )
		{
			RUN_TEST(test_queue);
			RUN_TEST(test_endian);
			RUN_TEST(test_address);
			RUN_TEST(test_sequence);
			RUN_TEST(test_connect_token);
			RUN_TEST(test_challenge_token);
			RUN_TEST(test_connection_request_packet);
			RUN_TEST(test_connection_denied_packet);
			RUN_TEST(test_connection_challenge_packet);
			RUN_TEST(test_connection_response_packet);
			RUN_TEST(test_connection_payload_packet);
			RUN_TEST(test_connection_disconnect_packet);
			RUN_TEST(test_connect_token_public);
			RUN_TEST(test_encryption_manager);
			RUN_TEST(test_replay_protection);
			RUN_TEST(test_client_create);
			RUN_TEST(test_server_create);
			RUN_TEST(test_client_server_connect);
			RUN_TEST(test_client_server_ipv4_socket_connect);
			RUN_TEST(test_client_server_ipv6_socket_connect);
			RUN_TEST(test_client_server_keep_alive);
			RUN_TEST(test_client_server_multiple_clients);
			RUN_TEST(test_client_server_multiple_servers);
			RUN_TEST(test_client_error_connect_token_expired);
			RUN_TEST(test_client_error_invalid_connect_token);
			RUN_TEST(test_client_error_connection_timed_out);
			RUN_TEST(test_client_error_connection_response_timeout);
			RUN_TEST(test_client_error_connection_request_timeout);
			RUN_TEST(test_client_error_connection_denied);
			RUN_TEST(test_client_side_disconnect);
			RUN_TEST(test_server_side_disconnect);
			RUN_TEST(test_client_reconnect);
			RUN_TEST(test_disable_timeout);
			RUN_TEST(test_loopback);
		}
	}

#endif  // NETCODE_ENABLE_TESTS

}// namespace GG