#include "NetworkPrecompiled.h"
#include "Netcode\PacketQueue.h"
#include "Netcode/Netcode.h"// default netcode functions
 

namespace GG
{

  void netcode_packet_queue_init(netcode_packet_queue_t* queue, void* allocatorContext, void* (*allocateFunction) (void*, u64), void(*freeFunction)(void*, void*))
  {
    if (allocateFunction == nullptr)
      allocateFunction = netcode_default_allocate_function;

    if (freeFunction == nullptr)
      freeFunction = netcode_default_free_function;

    gg_assert(queue);

    queue->allocatorContext = allocatorContext;
    queue->allocateFunction = allocateFunction;
    queue->freeFunction = freeFunction;
    queue->numPackets = 0;
    queue->startIndex = 0;
    memset(queue->packetData, 0, sizeof(queue->packetData));
    memset(queue->packetSequence, 0, sizeof(queue->packetSequence));
  }

  void netcode_packet_queue_clear(netcode_packet_queue_t* queue)
  {
    gg_assert(queue);
    for (s32 i = 0; i < queue->numPackets; ++i)
    {
      queue->freeFunction(queue->allocatorContext, queue->packetData[i]);
    }

    queue->numPackets = 0;
    queue->startIndex = 0;
    memset(queue->packetData, 0, sizeof(queue->packetData));
    memset(queue->packetSequence, 0, sizeof(queue->packetSequence));
  }

  bool netcode_packet_queue_push(netcode_packet_queue_t* queue, void* packetData, u64 packetSequence)
  {
    gg_assert(queue);
    gg_assert(packetData);

    if (queue->numPackets == NETCODE_PACKET_QUEUE_SIZE)
    {
      queue->freeFunction(queue->allocatorContext, packetData);
      return false;
    }

    s32 index = (queue->startIndex + queue->numPackets) % NETCODE_PACKET_QUEUE_SIZE;
    queue->packetData[index] = packetData;
    queue->packetSequence[index] = packetSequence;
    queue->numPackets++;
    return true;
  }

  void* netcode_packet_queue_pop(netcode_packet_queue_t* queue, u64* packetSequence)
  {
    if (queue->numPackets == 0)
      return nullptr;

    void* packet = queue->packetData[queue->startIndex];

    if (packetSequence)
      *packetSequence = queue->packetSequence[queue->startIndex];

    queue->startIndex = (queue->startIndex + 1) % NETCODE_PACKET_QUEUE_SIZE;
    queue->numPackets--;
    return packet;
  }


}// namespace GG