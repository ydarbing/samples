#include "NetworkPrecompiled.h"
#include "Connection.h"
#include "Channel/ChannelReliableOrdered.h"
#include "Channel/ChannelUnreliableUnordered.h"
#include "MessageFactory.h"
#include "Stream/StreamWrite.h"
#include "Stream/StreamRead.h"


namespace GG
{
  Connection::Connection(Allocator & allocator, MessageFactory & messageFactory, const ConnectionConfig & connectionConfig, f64 time)
    : m_connectionConfig(connectionConfig)
  {
    m_allocator = &allocator;
    m_messageFactory = &messageFactory;
    m_errorLevel = E_CONNECTION_ERROR_NONE;
    memset(m_channel, 0, sizeof(m_channel));
    gg_assert(m_connectionConfig.numChannels >= 1);
    gg_assert(m_connectionConfig.numChannels <= MAX_CHANNELS);

    for (s32 channelIndex = 0; channelIndex < m_connectionConfig.numChannels; ++channelIndex)
    {
      switch (m_connectionConfig.channel[channelIndex].type)
      {
      case E_CHANNEL_TYPE_RELIABLE_ORDERED:
      {
        m_channel[channelIndex] = GG_NEW(*m_allocator, ChannelReliableOrdered, *m_allocator, messageFactory, m_connectionConfig.channel[channelIndex], channelIndex, time);
      }
      break;
      case E_CHANNEL_TYPE_UNRELIABLE_UNORDERED:
      {
        m_channel[channelIndex] = GG_NEW(*m_allocator, ChannelUnreliableUnordered, *m_allocator, messageFactory, m_connectionConfig.channel[channelIndex], channelIndex, time);
      }
      break;
      default:
        gg_assert(!"unknown channel type");
      }
    }
  }
  Connection::~Connection()
  {
    gg_assert(m_allocator);
    Reset();
    for (s32 i = 0; i < m_connectionConfig.numChannels; ++i)
    {
      GG_DELETE(*m_allocator, Channel, m_channel[i]);
    }
    m_allocator = nullptr;
  }
  void Connection::Reset()
  {
    m_errorLevel = E_CONNECTION_ERROR_NONE;
    for (s32 i = 0; i < m_connectionConfig.numChannels; ++i)
    {
      m_channel[i]->Reset();
    }
  }
  bool Connection::CanSendMessage(s32 channelIndex) const
  {
    gg_assert(channelIndex >= 0);
    gg_assert(channelIndex < m_connectionConfig.numChannels);
    return m_channel[channelIndex]->CanSendMessage();
  }
  void Connection::SendMessage(s32 channelIndex, Message * message, void* context)
  {
    gg_assert(channelIndex >= 0);
    gg_assert(channelIndex < m_connectionConfig.numChannels);
    return m_channel[channelIndex]->SendMessage(message, context);
  }
  Message * Connection::ReceiveMessage(s32 channelIndex)
  {
    gg_assert(channelIndex >= 0);
    gg_assert(channelIndex < m_connectionConfig.numChannels);
    return m_channel[channelIndex]->ReceiveMessage();
  }
  void Connection::ReleaseMessage(Message * message)
  {
    gg_assert(message);
    m_messageFactory->ReleaseMessage(message);
  }

  static s32 WritePacket(void* context, MessageFactory& messageFactory, const ConnectionConfig& connectionConfig, ConnectionPacket& packet, u08* buffer, s32 bufferSize)
  {
    StreamWrite stream(messageFactory.GetAllocator(), buffer, bufferSize);
    stream.SetContext(context);
    if (!packet.SerializeInternal(stream, messageFactory, connectionConfig))
    {
      gg_printf(GG_LOG_LEVEL_ERROR, "Error: serialize connection packet failed (write packet)\n");
      return 0;
    }

#if GG_SERIALIZE_CHECKS
    if (!stream.SerializeCheck())
    {
      gg_printf(GG_LOG_LEVEL_ERROR, "Error: serialze check at end of connection packet failed (write packet)\n");
      return 0;
    }
#endif

    stream.Flush();
    return stream.GetBytesProcessed();
  }

  bool Connection::GeneratePacket(void * context, u16 packetSequence, u08 * packetData, s32 maxPacketBytes, s32 & packetBytes)
  {
    ConnectionPacket packet;

    if (m_connectionConfig.numChannels > 0)
    {
      s32 numChannelsWithData = 0;
      bool channelHasData[MAX_CHANNELS];
      memset(channelHasData, 0, sizeof(channelHasData));
      ChannelPacketData channelData[MAX_CHANNELS];

      s32 availableBits = maxPacketBytes * 8 - CONSERVATIVE_PACKET_HEADER_BITS;

      for (s32 channelIndex = 0; channelIndex < m_connectionConfig.numChannels; ++channelIndex)
      {
        s32 packetDataBits = m_channel[channelIndex]->GetPacketData(context, channelData[channelIndex], packetSequence, availableBits);
        if (packetDataBits > 0)
        {
          availableBits -= CONSERVATIVE_CHANNEL_HEADER_BITS;
          availableBits -= packetDataBits;
          channelHasData[channelIndex] = true;
          numChannelsWithData++;
        }
      }

      if (numChannelsWithData > 0)
      {
        if (!packet.AllocateChannelData(*m_messageFactory, numChannelsWithData))
        {
          gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to allocate channel data \n");
          return false;
        }

        s32 index = 0;
        for (s32 channelIndex = 0; channelIndex < m_connectionConfig.numChannels; ++channelIndex)
        {
          if (channelHasData[channelIndex])
          {
            memcpy(&packet.channelEntry[index], &channelData[channelIndex], sizeof(ChannelPacketData));
            ++index;
          }
        }
      }
    }

    packetBytes = WritePacket(context, *m_messageFactory, m_connectionConfig, packet, packetData, maxPacketBytes);

    return true;
  }



  static bool ReadPacket(void* context, MessageFactory& messageFactory, const ConnectionConfig& connectionConfig, ConnectionPacket& packet, const u08* buffer, s32 bufferSize)
  {
    gg_assert(buffer);
    gg_assert(bufferSize > 0);

    StreamRead stream(messageFactory.GetAllocator(), buffer, bufferSize);

    stream.SetContext(context);

    if (!packet.SerializeInternal(stream, messageFactory, connectionConfig))
    {
      gg_printf(GG_LOG_LEVEL_ERROR, "Error: serialize connection packet failed (read packet)\n");
      return false;
    }

#if GG_SERIALIZE_CHECKS
    if (!stream.SerializeCheck())
    {
      gg_printf(GG_LOG_LEVEL_ERROR, "Error: serialize check failed at end of connection packet (read packet)\n");
      return false;
    }
#endif

    return true;
  }

  bool Connection::ProcessPacket(void * context, u16 packetSequence, const u08* packetData, s32 packetBytes)
  {
    if (m_errorLevel != E_CONNECTION_ERROR_NONE)
    {
      gg_printf(GG_LOG_LEVEL_DEBUG, "Failed to read packet because connection is in error state.\n");
      return false;
    }

    ConnectionPacket packet;

    if (!ReadPacket(context, *m_messageFactory, m_connectionConfig, packet, packetData, packetBytes))
    {
      gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to read packet.\n");
      m_errorLevel = E_CONNECTION_ERROR_READ_PACKET_FAILED;
      return false;
    }

    for (s32 i = 0; i < packet.numChannelEntries; ++i)
    {
      const s32 channelIndex = packet.channelEntry[i].channelIndex;
      gg_assert(channelIndex >= 0);
      gg_assert(channelIndex <= m_connectionConfig.numChannels);

      m_channel[channelIndex]->ProcessPacketData(packet.channelEntry[i], packetSequence);
      if (m_channel[channelIndex]->GetErrorLevel() != E_CHANNEL_ERROR_NONE)
      {
        gg_printf(GG_LOG_LEVEL_DEBUG, "Failed to read packet because channel %d is in error state.\n", channelIndex);
        return false;
      }
    }
    return true;
  }
  void Connection::ProcessAcks(const u16 * acks, s32 numAcks)
  {
    for (s32 i = 0; i < numAcks; ++i)
    {
      for (s32 channelIndex = 0; channelIndex < m_connectionConfig.numChannels; ++channelIndex)
      {
        m_channel[channelIndex]->ProcessAck(acks[i]);
      }
    }
  }
  void Connection::AdvanceTime(f64 time)
  {
    for (s32 i = 0; i < m_connectionConfig.numChannels; ++i)
    {
      m_channel[i]->AdvanceTime(time);
      if (m_channel[i]->GetErrorLevel() != E_CHANNEL_ERROR_NONE)
      {
        m_errorLevel = E_CONNECTION_ERROR_CHANNEL;
        return;
      }
    }
    if (m_allocator->GetErrorLevel() != E_ALLOCATOR_ERROR_NONE)
    {
      m_errorLevel = E_CONNECTION_ERROR_ALLOCATOR;
      return;
    }
    if (m_messageFactory->GetErrorLevel() != E_MESSAGE_FACTORY_ERROR_NONE)
    {
      m_errorLevel = E_CONNECTION_ERROR_MESSAGE_FACTORY;
      return;
    }
  }

  ConnectionErrorLevel Connection::GetErrorLevel() const
  {
    return m_errorLevel;
  }

}// namespace GG
