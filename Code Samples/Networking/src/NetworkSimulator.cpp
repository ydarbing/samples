#include "NetworkPrecompiled.h"
#include "NetworkSimulator.h"
 
#include "Utils.h"

namespace GG
{
  NetworkSimulator::NetworkSimulator(Allocator& allocator, s32 numPackets, f64 time)
  {
    gg_assert(numPackets > 0);
    m_allocator = &allocator;
    m_numPacketEntries = numPackets;
    m_time = time;
    m_currentIndex = 0;
    m_latency = 0.0f;
    m_jitter = 0.0f;
    m_packetLoss = 0.0f;
    m_duplicates = 0.0f;
    m_active = false;
    m_packetEntries = SCAST(PacketEntry*, GG_ALLOCATE(allocator, sizeof(PacketEntry) * numPackets));
    gg_assert(m_packetEntries);
    memset(m_packetEntries, 0, sizeof(PacketEntry) * numPackets);
  }
  NetworkSimulator::~NetworkSimulator()
  {
    gg_assert(m_allocator);
    gg_assert(m_packetEntries);
    gg_assert(m_numPacketEntries > 0);
    DiscardPackets();
    GG_FREE(*m_allocator, m_packetEntries);
    m_numPacketEntries = 0;
    m_allocator = nullptr;
  }
  void NetworkSimulator::SetLatency(f32 milliseconds)
  {
    m_latency = milliseconds;
    UpdateActive();
  }
  void NetworkSimulator::SetPacketLoss(f32 percent)
  {
    m_packetLoss = percent;
    UpdateActive();
  }
  void NetworkSimulator::SetJitter(f32 milliseconds)
  {
    m_jitter = milliseconds;
    UpdateActive();
  }
  void NetworkSimulator::SetDuplicates(f32 percent)
  {
    m_duplicates = percent;
    UpdateActive();
  }
  bool NetworkSimulator::IsActive() const
  {
    return m_active;
  }
  void NetworkSimulator::SendPacket(s32 to, u08* packetData, s32 packetBytes)
  {
    gg_assert(m_allocator);
    gg_assert(packetData);
    gg_assert(packetBytes > 0);

    if (RandomFloat(0.0f, 100.0f) <= m_packetLoss)
      return;

    PacketEntry& packetEntry = m_packetEntries[m_currentIndex];

    if(packetEntry.packetData)
    {
      GG_FREE(*m_allocator, packetEntry.packetData);
      packetEntry = PacketEntry();
    }

    f64 delay = m_latency / 1000.0;

    if (m_jitter > 0)
      delay += RandomFloat(-m_jitter, +m_jitter) / 1000.0;

    packetEntry.to = to;
    packetEntry.packetData = (u08*)GG_ALLOCATE(*m_allocator, packetBytes);
    memcpy(packetEntry.packetData, packetData, packetBytes);
    packetEntry.packetBytes = packetBytes;
    packetEntry.deliveryTime = m_time + delay;
    m_currentIndex = (m_currentIndex + 1) % m_numPacketEntries;

    if(RandomFloat(0.0f, 100.0f) <= m_duplicates)
    {
      PacketEntry& nextPacketEntry = m_packetEntries[m_currentIndex];
      nextPacketEntry.to = to;
      nextPacketEntry.packetData = (u08*)GG_ALLOCATE(*m_allocator, packetBytes);
      memcpy(nextPacketEntry.packetData, packetData, packetBytes);
      nextPacketEntry.packetBytes = packetBytes;
      nextPacketEntry.deliveryTime = m_time + delay + RandomFloat(0.0f, 1.0f);
      m_currentIndex = (m_currentIndex + 1) % m_numPacketEntries;
    }
  }
  s32 NetworkSimulator::ReceivePackets(s32 maxPackets, u08* packetData[], s32 packetBytes[], s32 to[])
  {
    if (!IsActive())
      return 0;

    s32 numPackets = 0;
    s32 size = Min(m_numPacketEntries, maxPackets);

    for(s32 i = 0; i < size; ++i)
    {
      if (!m_packetEntries[i].packetData)
        continue;

      if(m_packetEntries[i].deliveryTime < m_time)
      {
        packetData[numPackets] = m_packetEntries[i].packetData;
        packetBytes[numPackets] = m_packetEntries[i].packetBytes;
        if(to)
        {
          to[numPackets] = m_packetEntries[i].to;
        }
        m_packetEntries[i].packetData = nullptr;
        ++numPackets;
      }
    }
    return numPackets;
  }
  void NetworkSimulator::DiscardPackets()
  {
    for(s32 i = 0; i < m_numPacketEntries; ++i)
    {
      PacketEntry& packetEntry = m_packetEntries[i];
      if (!packetEntry.packetData)
        continue;

      GG_FREE(*m_allocator, packetEntry.packetData);
      packetEntry = PacketEntry();
    }
  }
  void NetworkSimulator::DiscardClientPackets(s32 clientIndex)
  {
    for (s32 i = 0; i < m_numPacketEntries; ++i)
    {
      PacketEntry& packetEntry = m_packetEntries[i];
      if (!packetEntry.packetData || packetEntry.to != clientIndex)
        continue;

      GG_FREE(*m_allocator, packetEntry.packetData);
      packetEntry = PacketEntry();
    }
  }
  void NetworkSimulator::AdvanceTime(f64 time)
  {
    m_time = time;
  }
  Allocator& NetworkSimulator::GetAllocator()
  {
    gg_assert(m_allocator);
    return *m_allocator;
  }
  void NetworkSimulator::UpdateActive()
  {
    bool previous = m_active;
    m_active = m_latency != 0.0f || m_jitter != 0.0f || m_packetLoss != 0.0f || m_duplicates != 0.0f;
    if (previous && !m_active)
      DiscardPackets();
  }
}// namespace GG