#include "NetworkPrecompiled.h"
#include "BitArray.h"

namespace GG
{

  BitArray::BitArray(Allocator& allocator, s32 size)
  {
    gg_assert(size > 0);
    m_allocator = &allocator;
    m_size = size;
    m_bytes = 8 * ( (size / 64) + ( (size % 64) ? 1 : 0));
    gg_assert(m_bytes > 0);
    m_data = (u64*)GG_ALLOCATE(allocator, m_bytes);
    Clear();
  }

  BitArray::~BitArray()
  {
    gg_assert(m_data);
    gg_assert(m_allocator);

    GG_FREE(*m_allocator, m_data);
    m_allocator = nullptr;
  }

  void BitArray::Clear()
  {
    gg_assert(m_data);
    memset(m_data, 0, m_bytes);
  }

  void BitArray::SetBit(s32 index)
  {
    gg_assert(index >= 0);
    gg_assert(index < m_size);
    const s32 data_index = index >> 6;
    const s32 bit_index = index & ((1 << 6) - 1);
    gg_assert(bit_index >= 0);
    gg_assert(bit_index < 64);
    m_data[data_index] |= u64(1) << bit_index;
  }

  void BitArray::ClearBit(s32 index)
  {
    gg_assert(index >= 0);
    gg_assert(index < m_size);
    const s32 data_index = index >> 6;
    const s32 bit_index = index & ((1 << 6) - 1);
    m_data[data_index] &= ~(u64(1) << bit_index);
  }

  u64 BitArray::GetBit(s32 index)const
  {
    gg_assert(index >= 0);
    gg_assert(index < m_size);
    const s32 data_index = index >> 6;
    const s32 bit_index = index & ((1 << 6) - 1);
    gg_assert(bit_index >= 0);
    gg_assert(bit_index < 64);
    return (m_data[data_index] >> bit_index) & 1;
  }

  s32 BitArray::GetSize() const
  {
    return m_size;
  }
}