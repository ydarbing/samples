#include "NetworkPrecompiled.h"
#include "Adapter.h"
#include "Allocator/TLSF_Allocator.h"
#include "MessageFactory.h"


namespace GG
{
  Allocator* Adapter::CreateAllocator(Allocator & allocator, void * memory, size_t bytes)
  {
    return GG_NEW(allocator, TLSF_Allocator, memory, bytes);
  }

  MessageFactory* Adapter::CreateMessageFactory(Allocator & allocator)
  {
    (void)allocator;
    gg_assert(false);
    return nullptr;
  }

  void Adapter::ClientSendLoopbackPacket(s32 clientIndex, const u08 * packetData, s32 packetBytes, u64 packetSequence)
  {
    (void)clientIndex;
    (void)packetData;
    (void)packetBytes;
    (void)packetSequence;
    gg_assert(false);
  }

  void Adapter::ServerSendLoopbackPacket(s32 clientIndex, const u08 * packetData, s32 packetBytes, u64 packetSequence)
  {
    (void)clientIndex;
    (void)packetData;
    (void)packetBytes;
    (void)packetSequence;
    gg_assert(false);
  }

  void Adapter::OnServerClientConnected(s32 clientIndex)
  {
    (void)clientIndex;
  }

  void Adapter::OnServerClientDisconnected(s32 clientIndex)
  {
    (void)clientIndex;
  }

}// namespace GG
