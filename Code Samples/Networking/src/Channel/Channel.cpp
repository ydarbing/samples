#include "NetworkPrecompiled.h"
#include "Channel\Channel.h"
#include "Allocator/Allocator.h"

namespace GG
{
  void ChannelPacketData::Initialize()
  {
    channelIndex = 0;
    blockMessage = 0;
    messageFailedToSerialize = 0;
    message.numMessages = 0;
    initialized = 1;
  }

  void ChannelPacketData::Free(MessageFactory & messageFactory)
  {
    gg_assert(initialized);
    Allocator& allocator = messageFactory.GetAllocator();

    if(!blockMessage)
    {
      if(message.numMessages > 0)
      {
        for(s32 i = 0; i < message.numMessages; ++i)
        {
          if(message.messages[i])
          {
            messageFactory.ReleaseMessage(message.messages[i]);
          }
        }
        GG_FREE(allocator, message.messages);
      }
    }
    else
    {
      if(block.message)
      {
        messageFactory.ReleaseMessage(block.message);
        block.message = nullptr;
      }
      GG_FREE(allocator, block.fragmentData);
    }
    initialized = 0;
  }

  bool ChannelPacketData::SerializeInternal(StreamRead & stream, MessageFactory & messageFactory, const ChannelConfig * channelConfigs, s32 numChannels)
  {
    return Serialize(stream, messageFactory, channelConfigs, numChannels);
  }
  bool ChannelPacketData::SerializeInternal(StreamWrite & stream, MessageFactory & messageFactory, const ChannelConfig * channelConfigs, s32 numChannels)
  {
    return Serialize(stream, messageFactory, channelConfigs, numChannels);
  }
  bool ChannelPacketData::SerializeInternal(StreamMeasure & stream, MessageFactory & messageFactory, const ChannelConfig * channelConfigs, s32 numChannels)
  {
    return Serialize(stream, messageFactory, channelConfigs, numChannels);
  }







  Channel::Channel(Allocator & allocator, MessageFactory & messageFactory, const ChannelConfig & config, s32 channelIndex, f64 time)
    : m_config(config), m_time(time)
  {
    gg_assert(channelIndex >= 0);
    gg_assert(channelIndex < MAX_CHANNELS);

    m_channelIndex = channelIndex;
    m_allocator = &allocator;
    m_messageFactory = &messageFactory;
    m_errorLevel = E_CHANNEL_ERROR_NONE;

    ResetCounters();
  }

  void Channel::ResetCounters()
  {
    memset(m_counters, 0, sizeof(m_counters));
  }

  void Channel::SetErrorLevel(ChannelErrorLevel errorLevel)
  {
    if(errorLevel != m_errorLevel && errorLevel != E_CHANNEL_ERROR_NONE)
    {
      gg_printf(GG_LOG_LEVEL_ERROR, "Channel went into error state %s\n", GetChannelErrorString(errorLevel));
    }
    m_errorLevel = errorLevel;
  }

  ChannelErrorLevel Channel::GetErrorLevel() const
  {
    return m_errorLevel;
  }

  s32 Channel::GetChannelIndex() const
  {
    return m_channelIndex;
  }

  u64 Channel::GetCounter(s32 index) const
  {
    gg_assert(index >= 0);
    gg_assert(index < E_CHANNEL_COUNTER_NUM_COUNTERS);
    return m_counters[index];
  }
}// namespace GG

