#include "NetworkPrecompiled.h"
#include "Channel\ChannelReliableOrdered.h"
#include "Stream/StreamMeasure.h"
#include "Serializer.h"

#include "Allocator\DefaultAllocator.h" // GetDefaultAllocator()

namespace GG
{
  ChannelReliableOrdered::ChannelReliableOrdered(Allocator & allocator, MessageFactory & messageFactory, const ChannelConfig & config, s32 channelIndex, f64 time)
    : Channel(allocator, messageFactory, config, channelIndex, time)
  {
    gg_assert(config.type == E_CHANNEL_TYPE_RELIABLE_ORDERED);
    gg_assert((65536 % config.sentPacketBufferSize) == 0);
    gg_assert((65536 % config.messageSendQueueSize) == 0);
    gg_assert((65536 % config.messageReceiveQueueSize) == 0);

    m_sentPackets = GG_NEW(*m_allocator, SequenceBuffer<SentPacketEntry>, *m_allocator, m_config.sentPacketBufferSize);
    m_messageSendQueue = GG_NEW(*m_allocator, SequenceBuffer<MessageSendQueueEntry>, *m_allocator, m_config.messageSendQueueSize);
    m_messageReceiveQueue = GG_NEW(*m_allocator, SequenceBuffer<MessageReceiveQueueEntry>, *m_allocator, m_config.messageSendQueueSize);
    m_sentPacketMessageIds = SCAST(u16*, GG_ALLOCATE(*m_allocator, sizeof(u16) * m_config.maxMessagesPerPacket * m_config.sentPacketBufferSize));

    if (!config.disableBlocks)
    {
      m_sendBlock = GG_NEW(*m_allocator, SendBlockData, *m_allocator, m_config.GetMaxFragmentsPerBlock());
      m_receiveBlock = GG_NEW(*m_allocator, ReceiveBlockData, *m_allocator, m_config.maxBlockSize, m_config.GetMaxFragmentsPerBlock());
    }
    else
    {
      m_sendBlock = nullptr;
      m_receiveBlock = nullptr;
    }
    Reset();
  }

  ChannelReliableOrdered::~ChannelReliableOrdered()
  {
    Reset();

    GG_DELETE(*m_allocator, SendBlockData, m_sendBlock);
    GG_DELETE(*m_allocator, ReceiveBlockData, m_receiveBlock);
    GG_DELETE(*m_allocator, SequenceBuffer<SentPacketEntry>, m_sentPackets);
    GG_DELETE(*m_allocator, SequenceBuffer<MessageSendQueueEntry>, m_messageSendQueue);
    GG_DELETE(*m_allocator, SequenceBuffer<MessageReceiveQueueEntry>, m_messageReceiveQueue);

    GG_FREE(*m_allocator, m_sentPacketMessageIds);

    m_sentPacketMessageIds = nullptr;
  }

  void ChannelReliableOrdered::Reset()
  {
    SetErrorLevel(E_CHANNEL_ERROR_NONE);

    m_sendMessageId = 0;
    m_receiveMessageId = 0;
    m_oldestUnackedMessageId = 0;

    for (s32 i = 0; i < m_messageSendQueue->GetSize(); ++i)
    {
      MessageSendQueueEntry* entry = m_messageSendQueue->GetAtIndex(i);
      if (entry && entry->message)
        m_messageFactory->ReleaseMessage(entry->message);
    }


    for (s32 i = 0; i < m_messageReceiveQueue->GetSize(); ++i)
    {
      MessageReceiveQueueEntry* entry = m_messageReceiveQueue->GetAtIndex(i);
      if (entry && entry->message)
        m_messageFactory->ReleaseMessage(entry->message);
    }

    m_sentPackets->Reset();
    m_messageSendQueue->Reset();
    m_messageReceiveQueue->Reset();

    if (m_sendBlock)
    {
      m_sendBlock->Reset();
    }

    if (m_receiveBlock)
    {
      m_receiveBlock->Reset();

      if (m_receiveBlock->blockMessage)
      {
        m_messageFactory->ReleaseMessage(m_receiveBlock->blockMessage);
        m_receiveBlock->blockMessage = nullptr;
      }
    }

    ResetCounters();
  }

  bool ChannelReliableOrdered::CanSendMessage() const
  {
    gg_assert(m_messageSendQueue);
    return m_messageSendQueue->Available(m_sendMessageId);
  }

  void ChannelReliableOrdered::SendMessage(Message * message, void* context)
  {
    gg_assert(message);

    gg_assert(CanSendMessage());

    if (GetErrorLevel() != E_CHANNEL_ERROR_NONE)
    {
      m_messageFactory->ReleaseMessage(message);
      return;
    }

    if (!CanSendMessage())
    {
      // should probably increate send queue size
      SetErrorLevel(E_CHANNEL_ERROR_SEND_QUEUE_FULL);  
      m_messageFactory->ReleaseMessage(message);
      return;
    }

    gg_assert(!(message->IsBlockMessage() && m_config.disableBlocks));

    if (message->IsBlockMessage() && m_config.disableBlocks)
    {
      // tried to send a block message but block messages are disabled for this channel
      SetErrorLevel(E_CHANNEL_ERROR_BLOCKS_DISABLED);
      m_messageFactory->ReleaseMessage(message);
      return;
    }

    message->SetId(m_sendMessageId);

    MessageSendQueueEntry* entry = m_messageSendQueue->Insert(m_sendMessageId);

    gg_assert(entry);

    entry->block = message->IsBlockMessage();
    entry->message = message;
    entry->measuredBits = 0;
    entry->timeLastSent = -1.0;

    if (message->IsBlockMessage())
    {
      gg_assert(((BlockMessage*)message)->GetBlockSize() > 0);
      gg_assert(((BlockMessage*)message)->GetBlockSize() <= m_config.maxBlockSize);
    }

    StreamMeasure measureStream(m_messageFactory->GetAllocator());
    measureStream.SetContext(context);
    message->SerializeInternal(measureStream);
    entry->measuredBits = measureStream.GetBitsProcessed();
    m_counters[E_CHANNEL_COUNTER_MESSAGES_SENT]++;
    m_sendMessageId++;
  }

  Message* ChannelReliableOrdered::ReceiveMessage()
  {
    if (GetErrorLevel() != E_CHANNEL_ERROR_NONE)
      return nullptr;

    MessageReceiveQueueEntry* entry = m_messageReceiveQueue->Find(m_receiveMessageId);

    if (!entry)
      return nullptr;

    Message* message = entry->message;
    gg_assert(message);
    gg_assert(message->GetId() == m_receiveMessageId);

    m_messageReceiveQueue->Remove(m_receiveMessageId);
    m_counters[E_CHANNEL_COUNTER_MESSAGES_RECEIVED]++;
    m_receiveMessageId++;

    return message;
  }

  void ChannelReliableOrdered::AdvanceTime(f64 time)
  {
    m_time = time;
  }

  s32 ChannelReliableOrdered::GetPacketData(void* context, ChannelPacketData & packetData, u16 packetSequence, s32 availableBits)
  {
    if (!HasMessagesToSend())
      return 0;

    if (SendingBlockMessage())
    {
      if (m_config.blockFragmentSize * 8 > availableBits)
        return 0;

      u16 messageId;
      u16 fragmentId;
      s32 fragmentBytes;
      s32 numFragments;
      s32 messageType;

      u08* fragmentData = GetFragmentToSend(messageId, fragmentId, fragmentBytes, numFragments, messageType);

      if (fragmentData)
      {
        const s32 fragmentBits = GetFragmentPacketData(packetData, messageId, fragmentId, fragmentData, fragmentBytes, numFragments, messageType);
        AddFragmentPacketEntry(messageId, fragmentId, packetSequence);
        return fragmentBits;
      }
    }
    else
    {
      s32 numMessageIds = 0;
      u16* messageIds = SCAST(u16*, alloca(m_config.maxMessagesPerPacket * sizeof(u16)));
      const s32 messageBits = GetMessagesToSend(messageIds, numMessageIds, availableBits, context);

      if (numMessageIds > 0)
      {
        GetMessagePacketData(packetData, messageIds, numMessageIds);
        AddMessagePacketEntry(messageIds, numMessageIds, packetSequence);
        return messageBits;
      }
    }

    return 0;
  }

  void ChannelReliableOrdered::ProcessPacketData(const ChannelPacketData & packetData, u16 packetSequence)
  {
    if (m_errorLevel != E_CHANNEL_ERROR_NONE)
      return;


    if (packetData.messageFailedToSerialize)
    {
      // a message failed to serialize read for some reason. ex. mismatched read/write
      SetErrorLevel(E_CHANNEL_ERROR_FAILED_TO_SERIALIZE);
      return;
    }

    (void)packetSequence;

    if (packetData.blockMessage)
    {
      ProcessPacketFragment(packetData.block.messageType,
                            packetData.block.messageId,
                            packetData.block.numFragments,
                            packetData.block.fragmentId,
                            packetData.block.fragmentData,
                            packetData.block.fragmentSize,
                            packetData.block.message);
    }
    else
    {
      ProcessPacketMessages(packetData.message.numMessages, packetData.message.messages);
    }
  }

  void ChannelReliableOrdered::ProcessAck(u16 ack)
  {
    SentPacketEntry* sentPacketEntry = m_sentPackets->Find(ack);

    if (!sentPacketEntry)
      return;

    gg_assert(!sentPacketEntry->acked);

    for (u32 i = 0; i < sentPacketEntry->numMessageIds; ++i)
    {
      const u16 messageId = sentPacketEntry->messageIds[i];
      MessageSendQueueEntry* sendQueueEntry = m_messageSendQueue->Find(messageId);

      if (sendQueueEntry)
      {
        gg_assert(sendQueueEntry->message);
        gg_assert(sendQueueEntry->message->GetId() == messageId);

        m_messageFactory->ReleaseMessage(sendQueueEntry->message);
        m_messageSendQueue->Remove(messageId);
        UpdateOldestUnackedMessageId();
      }
    }

    if (!m_config.disableBlocks && sentPacketEntry->block && m_sendBlock->active
        && m_sendBlock->blockMessageId == sentPacketEntry->blockMessageId)
    {
      const s32 messageId = sentPacketEntry->blockMessageId;
      const s32 fragmentId = sentPacketEntry->blockFragmentId;

      if (!m_sendBlock->ackedFragment->GetBit(fragmentId))
      {
        m_sendBlock->ackedFragment->SetBit(fragmentId);
        m_sendBlock->numAckedFragments++;
        if (m_sendBlock->numAckedFragments == m_sendBlock->numFragments)
        {
          m_sendBlock->active = false;
          MessageSendQueueEntry* sendQueueEntry = m_messageSendQueue->Find(messageId);
          gg_assert(sendQueueEntry);
          m_messageFactory->ReleaseMessage(sendQueueEntry->message);
          m_messageSendQueue->Remove(messageId);
          UpdateOldestUnackedMessageId();
        }
      }
    }
  }

  bool ChannelReliableOrdered::HasMessagesToSend() const
  {
    return m_oldestUnackedMessageId != m_sendMessageId;
  }

  s32 ChannelReliableOrdered::GetMessagesToSend(u16 * messageIds, s32 & numMessageIds, s32 remainingPacketBits, void* context)
  {
    gg_assert(HasMessagesToSend());

    numMessageIds = 0;

    if (m_config.packetBudget > 0)
      remainingPacketBits = Min(m_config.packetBudget * 8, remainingPacketBits);

    const s32 giveUpBits = 4 * 8;
    const s32 messageTypeBits = BitsRequired(0, m_messageFactory->GetNumTypes() - 1);
    const s32 messageLimit = Min(m_config.messageSendQueueSize, m_config.messageReceiveQueueSize);
    u16 previousMessageId = 0;
    s32 usedBits = CONSERVATIVE_MESSAGE_HEADER_BITS;
    s32 giveUpCounter = 0;

    for (s32 i = 0; i < messageLimit; ++i)
    {
      if (remainingPacketBits - usedBits < giveUpBits)
        break;

      if (giveUpCounter > m_config.messageSendQueueSize)
        break;

      u16 messageId = m_oldestUnackedMessageId + i;
      MessageSendQueueEntry* entry = m_messageSendQueue->Find(messageId);
      if (!entry)
        continue;
      if (entry->block)
        break;

      if (entry->timeLastSent + m_config.messageResendTime <= m_time && remainingPacketBits >= (s32)entry->measuredBits)
      {
        s32 messageBits = entry->measuredBits + messageTypeBits;
        if (numMessageIds == 0)
        {
          messageBits += 16;
        }
        else
        {
          StreamMeasure stream(GetDefaultAllocator());
          serialize_sequence_relative_internal(stream, previousMessageId, messageId);
          messageBits += stream.GetBitsProcessed();
        }

        if (usedBits + messageBits > remainingPacketBits)
        {
          ++giveUpCounter;
          continue;
        }

        usedBits += messageBits;
        messageIds[numMessageIds++] = messageId;
        previousMessageId = messageId;
        entry->timeLastSent = m_time;
      }

      if (numMessageIds == m_config.maxMessagesPerPacket)
        break;
    }

    return usedBits;
  }

  void ChannelReliableOrdered::GetMessagePacketData(ChannelPacketData& packetData, const u16* messageIds, s32 numMessageIds)
  {
    gg_assert(messageIds);

    packetData.Initialize();
    packetData.channelIndex = GetChannelIndex();
    packetData.message.numMessages = numMessageIds;

    if (numMessageIds == 0)
      return;

    packetData.message.messages = SCAST(Message**, GG_ALLOCATE(m_messageFactory->GetAllocator(), sizeof(Message*) * numMessageIds));

    for (s32 i = 0; i < numMessageIds; ++i)
    {
      MessageSendQueueEntry* entry = m_messageSendQueue->Find(messageIds[i]);
      gg_assert(entry);
      gg_assert(entry->message);
      gg_assert(entry->message->GetRefCount() > 0);
      packetData.message.messages[i] = entry->message;
      m_messageFactory->AcquireMessage(packetData.message.messages[i]);
    }
  }

  void ChannelReliableOrdered::AddMessagePacketEntry(const u16 * messageIds, s32 numMessageIds, u16 sequence)
  {
    SentPacketEntry* sentPacket = m_sentPackets->Insert(sequence);
    gg_assert(sentPacket);

    if (sentPacket)
    {
      sentPacket->acked = 0;
      sentPacket->block = 0;
      sentPacket->timeSent = m_time;
      const s32 index = (sequence % m_config.sentPacketBufferSize) * m_config.maxMessagesPerPacket;
      sentPacket->messageIds = &m_sentPacketMessageIds[index];
      sentPacket->numMessageIds = numMessageIds;
      for (s32 i = 0; i < numMessageIds; ++i)
      {
        sentPacket->messageIds[i] = messageIds[i];
      }
    }
  }

  void ChannelReliableOrdered::ProcessPacketMessages(s32 numMessages, Message ** messages)
  {
    const u16 minMessageId = m_receiveMessageId;
    const u16 maxMessageId = m_receiveMessageId + m_config.messageReceiveQueueSize - 1;

    for (s32 i = 0; i < numMessages; ++i)
    {
      Message* message = messages[i];

      gg_assert(message);

      const u16 messageId = message->GetId();

      if (SequenceLessThan(messageId, minMessageId))
        continue;
      if (SequenceGreaterThan(messageId, maxMessageId))
      {
        // did you forget to dequeue messages on the receiver?... probably
        SetErrorLevel(E_CHANNEL_ERROR_DESYNC);
        return;
      }

      if (m_messageReceiveQueue->Find(messageId))
        continue;;

      gg_assert(!m_messageReceiveQueue->GetAtIndex(m_messageReceiveQueue->GetIndex(messageId)));

      MessageReceiveQueueEntry* entry = m_messageReceiveQueue->Insert(messageId);
      if (!entry)
      {
        SetErrorLevel(E_CHANNEL_ERROR_DESYNC); // we can't insert the message in the receive queue
        return;
      }

      entry->message = message;

      m_messageFactory->AcquireMessage(message);
    }
  }

  void ChannelReliableOrdered::UpdateOldestUnackedMessageId()
  {
    const u16 stopMessageId = m_messageSendQueue->GetSequence();

    while (true)
    {
      if (m_oldestUnackedMessageId == stopMessageId || m_messageSendQueue->Find(m_oldestUnackedMessageId))
        break;

      ++m_oldestUnackedMessageId;
    }

    gg_assert(!SequenceGreaterThan(m_oldestUnackedMessageId, stopMessageId));
  }

  bool ChannelReliableOrdered::SendingBlockMessage()
  {
    gg_assert(HasMessagesToSend());

    MessageSendQueueEntry* entry = m_messageSendQueue->Find(m_oldestUnackedMessageId);

    return entry ? entry->block : false;
  }

  u08 * ChannelReliableOrdered::GetFragmentToSend(u16& messageId, u16& fragmentId, s32& fragmentBytes, s32& numFragments, s32& messageType)
  {
    MessageSendQueueEntry* entry = m_messageSendQueue->Find(m_oldestUnackedMessageId);

    gg_assert(entry);
    gg_assert(entry->block);

    BlockMessage* blockMessage = SCAST(BlockMessage*, entry->message);

    gg_assert(blockMessage);

    messageId = blockMessage->GetId();

    const s32 blockSize = blockMessage->GetBlockSize();

    if (!m_sendBlock->active)
    {
      // start sending this block
      m_sendBlock->active = true;
      m_sendBlock->blockSize = blockSize;
      m_sendBlock->blockMessageId = messageId;
      m_sendBlock->numFragments = (s32)ceil(blockSize / f32(m_config.blockFragmentSize));
      m_sendBlock->numAckedFragments = 0;

      const s32 maxFragmentsPerBlock = m_config.GetMaxFragmentsPerBlock();

      gg_assert(m_sendBlock->numFragments > 0);
      gg_assert(m_sendBlock->numFragments <= maxFragmentsPerBlock);

      m_sendBlock->ackedFragment->Clear();

      for (s32 i = 0; i < maxFragmentsPerBlock; ++i)
      {
        m_sendBlock->fragmentSendTime[i] = -1.0;
      }
    }

    numFragments = m_sendBlock->numFragments;

    // find next fragment to send (there may not be one)
    fragmentId = 0xFFFF;

    for (s32 i = 0; i < m_sendBlock->numFragments; ++i)
    {
      if (!m_sendBlock->ackedFragment->GetBit(i) && m_sendBlock->fragmentSendTime[i] + m_config.blockFragmentResendTime < m_time)
      {
        fragmentId = u16(i);
        break;
      }
    }

    if (fragmentId == 0xFFFF)
      return nullptr;

    // allocate and return a copy of the fragment
    messageType = blockMessage->GetType();
    fragmentBytes = m_config.blockFragmentSize;

    const s32 fragmentRemainder = blockSize % m_config.blockFragmentSize;

    if (fragmentRemainder && fragmentId == m_sendBlock->numFragments - 1)
    {
      fragmentBytes = fragmentRemainder;
    }

    u08* fragmentData = SCAST(u08*, GG_ALLOCATE(m_messageFactory->GetAllocator(), fragmentBytes));

    if (fragmentData)
    {
      memcpy(fragmentData, blockMessage->GetBlockData() + fragmentId * m_config.blockFragmentSize, fragmentBytes);
      m_sendBlock->fragmentSendTime[fragmentId] = m_time;
    }

    return fragmentData;
  }

  s32 ChannelReliableOrdered::GetFragmentPacketData(ChannelPacketData & packetData, u16 messageId, u16 fragmentId, u08* fragmentData, s32 fragmentSize, s32 numFragments, s32 messageType)
  {
    packetData.Initialize();

    packetData.channelIndex = GetChannelIndex();
    packetData.blockMessage = 1;

    packetData.block.fragmentData = fragmentData;
    packetData.block.messageId = messageId;
    packetData.block.fragmentId = fragmentId;
    packetData.block.fragmentSize = fragmentSize;
    packetData.block.numFragments = numFragments;
    packetData.block.messageType = messageType;

    const s32 messageTypeBits = BitsRequired(0, m_messageFactory->GetNumTypes() - 1);

    s32 fragmentBits = CONSERVATIVE_FRAGMENT_HEADER_BITS + fragmentSize * 8;

    if (fragmentId == 0)
    {
      MessageSendQueueEntry* entry = m_messageSendQueue->Find(packetData.block.messageId);

      gg_assert(entry);
      gg_assert(entry->message);

      packetData.block.message = SCAST(BlockMessage*, entry->message);

      m_messageFactory->AcquireMessage(packetData.block.message);

      fragmentBits += entry->measuredBits + messageTypeBits;
    }
    else
    {
      packetData.block.message = nullptr;
    }

    return fragmentBits;
  }

  void ChannelReliableOrdered::AddFragmentPacketEntry(u16 messageId, u16 fragmentId, u16 sequence)
  {
    SentPacketEntry* sentPacket = m_sentPackets->Insert(sequence);
    gg_assert(sentPacket);
    if (sentPacket)
    {
      sentPacket->numMessageIds = 0;
      sentPacket->messageIds = nullptr;
      sentPacket->timeSent = m_time;
      sentPacket->acked = 0;
      sentPacket->block = 1;
      sentPacket->blockMessageId = messageId;
      sentPacket->blockFragmentId = fragmentId;
    }
  }

  void ChannelReliableOrdered::ProcessPacketFragment(s32 messageType, u16 messageId, s32 numFragments, u16 fragmentId, const u08 * fragmentData, s32 fragmentBytes, BlockMessage * blockMessage)
  {
    gg_assert(!m_config.disableBlocks);

    if (fragmentData)
    {
      const u16 expectedMessageId = m_messageReceiveQueue->GetSequence();
      if (messageId != expectedMessageId)
        return;

      // start receiving a new block

      if (!m_receiveBlock->active)
      {
        gg_assert(numFragments >= 0);
        gg_assert(numFragments <= m_config.GetMaxFragmentsPerBlock());

        m_receiveBlock->active = true;
        m_receiveBlock->numFragments = numFragments;
        m_receiveBlock->numReceivedFragments = 0;
        m_receiveBlock->messageId = messageId;
        m_receiveBlock->blockSize = 0;
        m_receiveBlock->receivedFragment->Clear();
      }

      // validate fragment
      if (fragmentId >= m_receiveBlock->numFragments)
      {
        // fragment id is out of range;
        SetErrorLevel(E_CHANNEL_ERROR_DESYNC);
        return;
      }

      if (numFragments != m_receiveBlock->numFragments)
      {
        // number of fragments is out of range
        SetErrorLevel(E_CHANNEL_ERROR_DESYNC);
        return;
      }

      // now receive the fragment
      if (!m_receiveBlock->receivedFragment->GetBit(fragmentId))
      {
        m_receiveBlock->receivedFragment->SetBit(fragmentId);

        memcpy(m_receiveBlock->blockData + fragmentId * m_config.blockFragmentSize, fragmentData, fragmentBytes);

        if (fragmentId == 0)
        {
          m_receiveBlock->messageType = messageType;
        }

        if (fragmentId == m_receiveBlock->numFragments - 1)
        {
          m_receiveBlock->blockSize = (m_receiveBlock->numFragments - 1)* m_config.blockFragmentSize + fragmentBytes;

          if (m_receiveBlock->blockSize > SCAST(u32, m_config.maxBlockSize))
          {
            // block size is outside range
            SetErrorLevel(E_CHANNEL_ERROR_DESYNC);
            return;
          }
        }

        m_receiveBlock->numReceivedFragments++;

        if (fragmentId == 0)
        {
          // save block message (sent with fragment 0)
          m_receiveBlock->blockMessage = blockMessage;
          m_messageFactory->AcquireMessage(m_receiveBlock->blockMessage);
        }

        if (m_receiveBlock->numReceivedFragments == m_receiveBlock->numFragments)
        {
          // we are finished receiving the block
          if (m_messageReceiveQueue->GetAtIndex(m_messageReceiveQueue->GetIndex(messageId)))
          {
            // did you forget to dequeue messages on the receiver?.. probably
            SetErrorLevel(E_CHANNEL_ERROR_DESYNC);
            return;
          }

          blockMessage = m_receiveBlock->blockMessage;

          gg_assert(blockMessage);

          u08* blockData = SCAST(u08*, GG_ALLOCATE(m_messageFactory->GetAllocator(), m_receiveBlock->blockSize));

          if (!blockData)
          {
            // not enough memory to allocate block data
            SetErrorLevel(E_CHANNEL_ERROR_OUT_OF_MEMORY);
            return;
          }

          memcpy(blockData, m_receiveBlock->blockData, m_receiveBlock->blockSize);

          blockMessage->AttachBlock(m_messageFactory->GetAllocator(), blockData, m_receiveBlock->blockSize);

          blockMessage->SetId(messageId);

          MessageReceiveQueueEntry* entry = m_messageReceiveQueue->Insert(messageId);
          gg_assert(entry);
          entry->message = blockMessage;
          m_receiveBlock->active = false;
          m_receiveBlock->blockMessage = nullptr;
        }
      }
    }
  }
}// namespace GG
