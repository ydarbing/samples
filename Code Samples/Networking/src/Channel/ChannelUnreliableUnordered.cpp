#include "NetworkPrecompiled.h"
#include "Channel\ChannelUnreliableUnordered.h"
#include "Queue.h"
#include "Utils.h"
#include "Stream/StreamMeasure.h"

namespace GG
{
  ChannelUnreliableUnordered::ChannelUnreliableUnordered(Allocator & allocator, MessageFactory & messageFactory, const ChannelConfig & config, s32 channelIndex, f64 time)
    : Channel(allocator, messageFactory, config, channelIndex, time)
  {
    gg_assert(config.type == E_CHANNEL_TYPE_UNRELIABLE_UNORDERED);

    m_messageSendQueue = GG_NEW(*m_allocator, Queue<Message*>, *m_allocator, m_config.messageSendQueueSize);
    m_messageReceiveQueue = GG_NEW(*m_allocator, Queue<Message*>, *m_allocator, m_config.messageReceiveQueueSize);

    Reset();
  }

  ChannelUnreliableUnordered::~ChannelUnreliableUnordered()
  {
    Reset();
    GG_DELETE(*m_allocator, Queue<Message*>, m_messageSendQueue);
    GG_DELETE(*m_allocator, Queue<Message*>, m_messageReceiveQueue);
  }

  void ChannelUnreliableUnordered::Reset()
  {
    SetErrorLevel(E_CHANNEL_ERROR_NONE);

    for (s32 i = 0; i < m_messageSendQueue->GetNumEntries(); ++i)
      m_messageFactory->ReleaseMessage((*m_messageSendQueue)[i]);

    for (s32 i = 0; i < m_messageReceiveQueue->GetNumEntries(); ++i)
      m_messageFactory->ReleaseMessage((*m_messageReceiveQueue)[i]);

    m_messageSendQueue->Clear();
    m_messageReceiveQueue->Clear();

    ResetCounters();
  }

  bool ChannelUnreliableUnordered::CanSendMessage() const
  {
    gg_assert(m_messageSendQueue);
    return !m_messageSendQueue->IsFull();
  }

  bool ChannelUnreliableUnordered::HasMessagesToSend() const
  {
    gg_assert(m_messageSendQueue);
    return !m_messageSendQueue->IsEmpty();
  }

  void ChannelUnreliableUnordered::SendMessage(Message * message, void* context)
  {
    gg_assert(message);
    gg_assert(CanSendMessage());
    (void)context;

    if (GetErrorLevel() != E_CHANNEL_ERROR_NONE)
    {
      m_messageFactory->ReleaseMessage(message);
      return;
    }

    if (!CanSendMessage())
    {
      SetErrorLevel(E_CHANNEL_ERROR_SEND_QUEUE_FULL);
      m_messageFactory->ReleaseMessage(message);
      return;
    }

    gg_assert(!(message->IsBlockMessage() && m_config.disableBlocks));

    if (message->IsBlockMessage() && m_config.disableBlocks)
    {
      SetErrorLevel(E_CHANNEL_ERROR_BLOCKS_DISABLED);
      m_messageFactory->ReleaseMessage(message);
      return;
    }

    if (message->IsBlockMessage())
    {
      gg_assert((SCAST(BlockMessage*, message))->GetBlockSize() > 0);
      gg_assert((SCAST(BlockMessage*, message))->GetBlockSize() <= m_config.maxBlockSize);
    }

    m_messageSendQueue->Push(message);

    m_counters[E_CHANNEL_COUNTER_MESSAGES_SENT]++;
  }

  Message * ChannelUnreliableUnordered::ReceiveMessage()
  {
    if (GetErrorLevel() != E_CHANNEL_ERROR_NONE)
      return nullptr;

    if (m_messageReceiveQueue->IsEmpty())
      return nullptr;

    m_counters[E_CHANNEL_COUNTER_MESSAGES_RECEIVED]++;

    return m_messageReceiveQueue->Pop();
  }

  s32 ChannelUnreliableUnordered::GetPacketData(void* context, ChannelPacketData & packetData, u16 packetSequence, s32 availableBits)
  {
    (void)packetSequence;

    if (m_messageSendQueue->IsEmpty())
      return 0;

    if (m_config.packetBudget > 0)
      availableBits = Min(m_config.packetBudget * 8, availableBits);

    const s32 giveUpBits = 4 * 8;
    const s32 messageTypeBits = BitsRequired(0, m_messageFactory->GetNumTypes() - 1);

    s32 usedBits = CONSERVATIVE_MESSAGE_HEADER_BITS;
    s32 numMessages = 0;

    Message** messages = SCAST(Message**, alloca(sizeof(Message*) * m_config.maxMessagesPerPacket));

    while (true)
    {
      if (m_messageSendQueue->IsEmpty())
        break;

      if (availableBits - usedBits < giveUpBits)
        break;

      if (numMessages == m_config.maxMessagesPerPacket)
        break;

      Message* message = m_messageSendQueue->Pop();

      gg_assert(message);

      StreamMeasure measureStream(m_messageFactory->GetAllocator());
      measureStream.SetContext(context);
      message->SerializeInternal(measureStream);

      if (message->IsBlockMessage())
      {
        BlockMessage* blockMessage = SCAST(BlockMessage*, message);
        SerializeMessageBlock(measureStream, *m_messageFactory, blockMessage, m_config.maxBlockSize);
      }

      const s32 messageBits = messageTypeBits + measureStream.GetBitsProcessed();

      if (usedBits + messageBits > availableBits)
      {
        m_messageFactory->ReleaseMessage(message);
        continue;
      }

      usedBits += messageBits;

      gg_assert(usedBits <= availableBits);

      messages[numMessages++] = message;
    }

    if (numMessages == 0)
      return 0;

    Allocator& allocator = m_messageFactory->GetAllocator();

    packetData.Initialize();
    packetData.channelIndex = GetChannelIndex();
    packetData.message.numMessages = numMessages;
    packetData.message.messages = SCAST(Message**, GG_ALLOCATE(allocator, sizeof(Message*) * numMessages));

    for (s32 i = 0; i < numMessages; ++i)
    {
      packetData.message.messages[i] = messages[i];
    }

    return usedBits;
  }

  void ChannelUnreliableUnordered::ProcessPacketData(const ChannelPacketData & packetData, u16 packetSequence)
  {
    if (m_errorLevel != E_CHANNEL_ERROR_NONE)
      return;

    if (packetData.messageFailedToSerialize)
    {
      SetErrorLevel(E_CHANNEL_ERROR_FAILED_TO_SERIALIZE);
      return;
    }

    for (s32 i = 0; i < packetData.message.numMessages; ++i)
    {
      Message* message = packetData.message.messages[i];
      gg_assert(message);

      message->SetId(packetSequence);
      if (!m_messageReceiveQueue->IsFull())
      {
        m_messageFactory->AcquireMessage(message);
        m_messageReceiveQueue->Push(message);
      }
    }
  }


  void ChannelUnreliableUnordered::AdvanceTime(f64 time)
  {
    (void)time;
  }
  void ChannelUnreliableUnordered::ProcessAck(u16 ack)
  {
    (void)ack;
  }

}// namespace GG
