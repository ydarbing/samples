#include "NetworkPrecompiled.h"
#include "Client/Client.h"
#include "Adapter.h"
#include "Connection.h"
#include "NetworkSimulator.h"
#include "Netcode/Netcode.h"

#include "Reliable\Reliable.h"

namespace GG
{
  Client::Client(Allocator & allocator, const Address & address, const ClientServerConfig & config, Adapter & adapter, f64 time)
    : BaseClient(allocator, config, adapter, time), m_config(config), m_address(address)
  {
    m_clientId = 0;
    m_client = nullptr;
    m_boundAddress = m_address;
  }

  Client::~Client()
  {
    // Important: disconnect the client before destroying it
    gg_assert(m_client == nullptr);
  }

  void Client::InsecureConnect(const u08 privateKey[], u64 clientId, const Address & address)
  {
    InsecureConnect(privateKey, clientId, &address, 1);
  }

  void Client::InsecureConnect(const u08 privateKey[], u64 clientId, const Address serverAddresses[], s32 numServerAddresses)
  {
    gg_assert(serverAddresses);
    gg_assert(numServerAddresses > 0);
    gg_assert(numServerAddresses <= NETCODE_MAX_SERVERS_PER_CONNECT);

    Disconnect();
    CreateInternal();
    m_clientId = clientId;
    CreateClient(m_address);
    if (!m_client)
    {
      Disconnect();
      return;
    }

    u08 connectToken[NETCODE_CONNECT_TOKEN_BYTES];

    if (!GenerateInsecureConnectToken(connectToken, privateKey, clientId, serverAddresses, numServerAddresses))
    {
      gg_printf(GG_LOG_LEVEL_ERROR, "Error: failed to generate insecure connect token\n");
      SetClientState(E_CLIENT_STATE_ERROR);
      return;
    }

    netcode_client_connect(m_client, connectToken);
    SetClientState(E_CLIENT_STATE_CONNECTING);
  }

  bool Client::GenerateInsecureConnectToken(u08* connectToken, const u08 privateKey[], u64 clientId, const Address serverAddresses[], s32 numServerAddresses)
  {
    c08 serverAddressStrings[NETCODE_MAX_SERVERS_PER_CONNECT][MAX_ADDRESS_LENGTH];
    const c08* serverAddressStringPointers[NETCODE_MAX_SERVERS_PER_CONNECT];
    for (auto i = 0; i < numServerAddresses; ++i)
    {
      serverAddresses[i].ToString(serverAddressStrings[i], MAX_ADDRESS_LENGTH);
      serverAddressStringPointers[i] = serverAddressStrings[i];
    }

    u08 userData[256];
    memset(&userData, 0, sizeof(userData));

    return netcode_generate_connect_token(numServerAddresses,
                                          serverAddressStringPointers,
                                          serverAddressStringPointers,
                                          m_config.timeout,
                                          m_config.timeout,
                                          clientId,
                                          m_config.protocolID,
                                          (u08*)privateKey,
                                          &userData[0],
                                          connectToken) == NETCODE_OK;
  }

  void Client::Connect(u64 clientId, u08 * connectToken)
  {
    gg_assert(connectToken);
    Disconnect();
    CreateInternal();
    m_clientId = clientId;
    CreateClient(m_address);
    netcode_client_connect(m_client, connectToken);

    if (netcode_client_state(m_client) > NETCODE_CLIENT_STATE_DISCONNECTED)
    {
      SetClientState(E_CLIENT_STATE_CONNECTING);
    }
    else
    {
      Disconnect();
    }
  }

  void Client::Disconnect()
  {
    BaseClient::Disconnect();
    DestroyClient();
    DestroyInternal();
    m_clientId = 0;
  }

  void Client::SendPackets()
  {
    if (!IsConnected())
      return;

    gg_assert(m_client);
    u08* packetData = GetPacketBuffer();
    s32 packetBytes;
    u16 packetSequence = reliable_endpoint_next_packet_sequence(GetEndpoint());

    if (GetConnection().GeneratePacket(GetContext(), packetSequence, packetData, m_config.maxPacketSize, packetBytes))
    {
      reliable_endpoint_send_packet(GetEndpoint(), packetData, packetBytes);
    }
  }

  void Client::ReceivePackets()
  {
    if (!IsConnected())
      return;

    gg_assert(m_client);
    while (true)
    {
      s32 packetBytes;
      u64 packetSequence;
      u08* packetData = netcode_client_receive_packet(m_client, &packetBytes, &packetSequence);
      if (!packetData)
        return;

      reliable_endpoint_receive_packet(GetEndpoint(), packetData, packetBytes);
      netcode_client_free_packet(m_client, packetData);
    }
  }

  void Client::AdvanceTime(f64 time)
  {
    BaseClient::AdvanceTime(time);

    if (m_client)
    {
      netcode_client_update(m_client, time);
      const s32 state = netcode_client_state(m_client);
      if (state < NETCODE_CLIENT_STATE_DISCONNECTED)
      {
        Disconnect();
        SetClientState(E_CLIENT_STATE_ERROR);
      }
      else if (state == NETCODE_CLIENT_STATE_DISCONNECTED)
      {
        Disconnect();
        SetClientState(E_CLIENT_STATE_DISCONNECTED);
      }
      else if (state == NETCODE_CLIENT_STATE_SENDING_CONNECTION_REQUEST)
      {
        SetClientState(E_CLIENT_STATE_CONNECTING);
      }
      else
      {
        SetClientState(E_CLIENT_STATE_CONNECTED);
      }

      NetworkSimulator* networkSimulator = GetNetworkSimulator();
      if (networkSimulator && networkSimulator->IsActive())
      {
        u08** packetData = SCAST(u08**, alloca(sizeof(u08*) * m_config.maxSimulatorPackets));
        s32* packetBytes = SCAST(s32*, alloca(sizeof(s32) * m_config.maxSimulatorPackets));

        s32 numPackets = networkSimulator->ReceivePackets(m_config.maxSimulatorPackets, packetData, packetBytes, nullptr);

        for (s32 i = 0; i < numPackets; ++i)
        {
          netcode_client_send_packet(m_client, SCAST(u08*, packetData[i]), packetBytes[i]);
          GG_FREE(networkSimulator->GetAllocator(), packetData[i]);
        }
      }
    }
  }
  s32 Client::GetClientIndex() const
  {
    return m_client ? netcode_client_index(m_client) : -1;
  }
  u64 Client::GetClientId() const
  {
    return m_clientId;
  }
  void Client::ConnectLoopback(s32 clientIndex, u64 clientId, s32 maxClients)
  {
    Disconnect();
    CreateInternal();
    m_clientId = clientId;
    CreateClient(m_address);
    netcode_client_connect_loopback(m_client, clientIndex, maxClients);
    SetClientState(E_CLIENT_STATE_CONNECTED);
  }
  void Client::DisconnectLoopback()
  {
    netcode_client_disconnect_loopback(m_client);
    BaseClient::Disconnect();
    DestroyClient();
    DestroyInternal();
    m_clientId = 0;
  }

  bool Client::IsLoopback() const
  {
    return netcode_client_loopback(m_client) != 0;
  }
  void Client::ProcessLoopbackPacket(const u08 * packetData, s32 packetBytes, u64 packetSequence)
  {
    netcode_client_process_loopback_packet(m_client, packetData, packetBytes, packetSequence);
  }
  const Address& Client::GetAddress() const
  {
    return m_boundAddress;
  }
  void Client::CreateClient(const Address & address)
  {
    DestroyClient();
    c08 addressString[MAX_ADDRESS_LENGTH];
    address.ToString(addressString, MAX_ADDRESS_LENGTH);

    netcode_client_config_t netcodeConfig;
    netcode_default_client_config(&netcodeConfig);
    netcodeConfig.allocatorContext = &GetClientAllocator();
    netcodeConfig.allocate_function = StaticAllocateFunction;
    netcodeConfig.free_function = StaticFreeFunction;
    netcodeConfig.callbackContext = this;
    netcodeConfig.state_change_callback = StaticStateChangeCallbackFunction;
    netcodeConfig.send_loopback_packet_callback = StaticSendLoopbackPacketCallbackFunction;

    m_client = netcode_client_create(addressString, &netcodeConfig, GetTime());

    if (m_client)
    {
      m_boundAddress.SetPort(netcode_client_get_port(m_client));
    }
  }
  void Client::DestroyClient()
  {
    if (m_client)
    {
      m_boundAddress = m_address;
      netcode_client_destroy(m_client);
      m_client = nullptr;
    }
  }
  void Client::StateChangeCallbackFunction(s32 previous, s32 current)
  {
    (void)previous;
    (void)current;
  }
  void Client::StaticStateChangeCallbackFunction(void * context, s32 previous, s32 current)
  {
    Client* client = SCAST(Client*, context);
    client->StateChangeCallbackFunction(previous, current);
  }
  void Client::TransmitPacketFunction(u16 packetSequence, u08 * packetData, s32 packetBytes)
  {
    (void)packetSequence;
    NetworkSimulator* networkSimulator = GetNetworkSimulator();
    if (networkSimulator && networkSimulator->IsActive())
      networkSimulator->SendPacket(0, packetData, packetBytes);
    else
      netcode_client_send_packet(m_client, packetData, packetBytes);
  }
  s32 Client::ProcessPacketFunction(u16 packetSequence, u08 * packetData, s32 packetBytes)
  {
    return (s32)GetConnection().ProcessPacket(GetContext(), packetSequence, packetData, packetBytes);
  }
  void Client::SendLoopbackPacketCallbackFunction(s32 clientIndex, const u08 * packetData, s32 packetBytes, u64 packetSequence)
  {
    GetAdapter().ClientSendLoopbackPacket(clientIndex, packetData, packetBytes, packetSequence);
  }
  void Client::StaticSendLoopbackPacketCallbackFunction(void * context, s32 clientIndex, const u08 * packetData, s32 packetBytes, u64 packetSequence)
  {
    Client* client = SCAST(Client*, context);
    client->SendLoopbackPacketCallbackFunction(clientIndex, packetData, packetBytes, packetSequence);
  }
}// namespace GG