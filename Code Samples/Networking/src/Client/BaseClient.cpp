#include "NetworkPrecompiled.h"
#include "Client/BaseClient.h"
#include "Connection.h"
#include "NetworkSimulator.h"
#include "Adapter.h"
#include "Reliable\Reliable.h"


#include "Server/IServer.h"

namespace GG
{
  BaseClient::BaseClient(Allocator & allocator, const ClientServerConfig & config, Adapter & adapter, f64 time)
    : m_config(config)
  {
    m_allocator = &allocator;
    m_adapter = &adapter;
    m_time = time;
    m_context = nullptr;
    m_clientMemory = nullptr;
    m_clientAllocator = nullptr;
    m_endpoint = nullptr;
    m_connection = nullptr;
    m_messageFactory = nullptr;
    m_networkSimulator = nullptr;
    m_clientState = E_CLIENT_STATE_DISCONNECTED;
    m_clientIndex = -1;
    m_packetBuffer = SCAST(u08*, GG_ALLOCATE(allocator, config.maxPacketSize));
  }

  BaseClient::~BaseClient()
  {
    // important: please disconnect the client before destroying it
    gg_assert(m_clientState <= E_CLIENT_STATE_DISCONNECTED);
    GG_FREE(*m_allocator, m_packetBuffer);
    m_allocator = nullptr;
  }

  void BaseClient::Disconnect()
  {
    SetClientState(E_CLIENT_STATE_DISCONNECTED);
  }

  void BaseClient::AdvanceTime(f64 time)
  {
    m_time = time;
    if(m_endpoint)
    {
      m_connection->AdvanceTime(time);
      if(m_connection->GetErrorLevel() != E_CONNECTION_ERROR_NONE)
      {
        gg_printf(GG_LOG_LEVEL_DEBUG, "Connection Error.  Disconnecting client\n");
        Disconnect();
        return;
      }

      reliable_endpoint_update(m_endpoint, m_time);
      s32 numAcks;
      const u16* acks = reliable_endpoint_get_acks(m_endpoint, &numAcks);
      m_connection->ProcessAcks(acks, numAcks);
      reliable_endpoint_clear_acks(m_endpoint);
    }

    NetworkSimulator* networkSimulator = GetNetworkSimulator();
    if(networkSimulator)
    {
      networkSimulator->AdvanceTime(time);
    }
  }

  void BaseClient::SetContext(void* context)
  {
    gg_assert(IsDisconnected());
    m_context = context;
  }

  bool BaseClient::IsConnecting() const
  {
    return m_clientState == E_CLIENT_STATE_CONNECTING;
  }

  bool BaseClient::IsConnected() const
  {
    return m_clientState == E_CLIENT_STATE_CONNECTED;
  }

  bool BaseClient::IsDisconnected() const
  {
    return m_clientState <= E_CLIENT_STATE_DISCONNECTED;
  }

  bool BaseClient::ConnectionFailed() const
  {
    return m_clientState == E_CLIENT_STATE_ERROR;
  }

  ClientState BaseClient::GetClientState() const
  {
    return m_clientState;
  }

  s32 BaseClient::GetClientIndex() const
  {
    return m_clientIndex;
  }
  f64 BaseClient::GetTime() const
  {
    return m_time;
  }

  void BaseClient::SetLatency(f32 milliseconds)
  {
    if(m_networkSimulator)
    {
      m_networkSimulator->SetLatency(milliseconds);
    }
  }
  void BaseClient::SetJitter(f32 milliseconds)
  {
    if (m_networkSimulator)
    {
      m_networkSimulator->SetJitter(milliseconds);
    }
  }
  void BaseClient::SetPacketLoss(f32 percent)
  {
    if (m_networkSimulator)
    {
      m_networkSimulator->SetPacketLoss(percent);
    }
  }
  void BaseClient::SetDuplicates(f32 percent)
  {
    if (m_networkSimulator)
    {
      m_networkSimulator->SetDuplicates(percent);
    }
  }

  Message * BaseClient::CreateMessage(s32 type)
  {
    gg_assert(m_messageFactory);
    return m_messageFactory->CreateMessage(type);
  }

  u08 * BaseClient::AllocateBlock(s32 bytes)
  {
    return SCAST(u08*, GG_ALLOCATE(*m_clientAllocator, bytes));
  }

  void BaseClient::AttachBlockToMessage(Message * message, u08 * block, s32 bytes)
  {
    gg_assert(message);
    gg_assert(block);
    gg_assert(bytes > 0);
    gg_assert(message->IsBlockMessage());
    BlockMessage* blockMessage = SCAST(BlockMessage*, message);
    blockMessage->AttachBlock(*m_clientAllocator, block, bytes);
  }

  void BaseClient::FreeBlock(u08 * block)
  {
    GG_FREE(*m_clientAllocator, block);
  }

  bool BaseClient::CanSendMessage(s32 channelIndex) const
  {
    gg_assert(m_connection);
    return m_connection->CanSendMessage(channelIndex);
  }

  void BaseClient::SendMessage(s32 channelIndex, Message * message)
  {
    gg_assert(m_connection);
    return m_connection->SendMessage(channelIndex, message, GetContext());
  }

  Message * BaseClient::ReceiveMessage(s32 channelIndex)
  {
    gg_assert(m_connection);
    return m_connection->ReceiveMessage(channelIndex);
  }

  void BaseClient::ReleaseMessage(Message * message)
  {
    gg_assert(m_connection);
    m_connection->ReleaseMessage(message);
  }

  void BaseClient::GetNetworkInfo(NetworkInfo & info) const
  {
    memset(&info, 0, sizeof(info));

    if(m_connection)
    {
      gg_assert(m_endpoint);
      const u64* counters = reliable_endpoint_counters(m_endpoint);
      info.numPacketsSent = counters[RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_RECEIVED];
      info.numPacketsReceived = counters[RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_SENT];
      info.numPacketsAcked = counters[RELIABLE_ENDPOINT_COUNTER_NUM_PACKETS_ACKED];
      info.RTT = reliable_endpoint_rtt(m_endpoint);
      info.packetLoss = reliable_endpoint_packet_loss(m_endpoint);
      reliable_endpoint_bandwidth(m_endpoint, &info.sentBandwidth, &info.receivedBandwidth, &info.ackedBandwidth);
    }
  }

  void BaseClient::CreateInternal()
  {
    gg_assert(m_allocator);
    gg_assert(m_adapter);
    gg_assert(m_clientMemory == nullptr);
    gg_assert(m_clientAllocator == nullptr);
    gg_assert(m_messageFactory == nullptr);
    m_clientMemory = SCAST(u08*, GG_ALLOCATE(*m_allocator, m_config.clientMemory));
    m_clientAllocator = m_adapter->CreateAllocator(*m_allocator, m_clientMemory, m_config.clientMemory);
    m_messageFactory = m_adapter->CreateMessageFactory(*m_clientAllocator);
    m_connection = GG_NEW(*m_clientAllocator, Connection, *m_clientAllocator, *m_messageFactory, m_config, m_time);
    gg_assert(m_connection);

    if(m_config.networkSimulator)
    {
      m_networkSimulator = GG_NEW(*m_clientAllocator, NetworkSimulator, *m_clientAllocator, m_config.maxSimulatorPackets, m_time);
    }

    reliable_config_t reliable_config;
    reliable_default_config(&reliable_config);
    strcpy(reliable_config.name, "client endpoint");
    reliable_config.context = SCAST(void*, this);
    reliable_config.maxPacketSize = m_config.maxPacketSize;
    reliable_config.fragmentAbove = m_config.fragmentPacketsAbove;
    reliable_config.maxFragments = m_config.maxPacketFragments;
    reliable_config.fragmentSize = m_config.packetFragmentSize;
    reliable_config.ackBufferSize = m_config.ackedPacketsBufferSize;
    reliable_config.receivedPacketsBufferSize = m_config.receivedPacketsBufferSize;
    reliable_config.fragmentReassemblyBufferSize = m_config.packetReassemblyBufferSize;
    reliable_config.transmit_packet_function = BaseClient::StaticTransmitPacketFunction;
    reliable_config.process_packet_function = BaseClient::StaticProcessPacketFunction;
    reliable_config.allocatorContext = m_clientAllocator;
    reliable_config.allocate_function = BaseClient::StaticAllocateFunction;
    reliable_config.free_function = BaseClient::StaticFreeFunction;

    m_endpoint = reliable_endpoint_create(&reliable_config, m_time);
    reliable_endpoint_reset(m_endpoint);

  }

  void BaseClient::DestroyInternal()
  {
    gg_assert(m_allocator);

    if (m_endpoint)
    {
      reliable_endpoint_destroy(m_endpoint);
      m_endpoint = nullptr;
    }

    GG_DELETE(*m_clientAllocator, NetworkSimulator, m_networkSimulator);
    GG_DELETE(*m_clientAllocator, Connection, m_connection);
    GG_DELETE(*m_clientAllocator, MessageFactory, m_messageFactory);
    GG_DELETE(*m_allocator, Allocator, m_clientAllocator);
    GG_FREE(*m_allocator, m_clientMemory);
  }

  void BaseClient::SetClientState(ClientState clientState)
  {
    m_clientState = clientState;
  }

  void BaseClient::StaticTransmitPacketFunction(void * context, s32 index, u16 packetSequence, u08 * packetData, s32 packetBytes)
  {
    (void)index;
    BaseClient* client = SCAST(BaseClient*, context);
    return client->TransmitPacketFunction(packetSequence, packetData, packetBytes);
  }
  s32 BaseClient::StaticProcessPacketFunction(void * context, s32 index, u16 packetSequence, u08 * packetData, s32 packetBytes)
  {
    (void)index;
    BaseClient* client = SCAST(BaseClient*, context);
    return client->ProcessPacketFunction(packetSequence, packetData, packetBytes);
  }
  void * BaseClient::StaticAllocateFunction(void * context, u64 bytes)
  {
    gg_assert(context);
    Allocator* allocator = SCAST(Allocator*, context);
    return GG_ALLOCATE(*allocator, bytes);
  }
  void BaseClient::StaticFreeFunction(void * context, void * pointer)
  {
    gg_assert(context);
    gg_assert(pointer);
    Allocator* allocator = SCAST(Allocator*, context);
    GG_FREE(*allocator, pointer);
  }




  Allocator& BaseClient::GetClientAllocator()
  {
    gg_assert(m_clientAllocator); 
    return *m_clientAllocator;
  }

  MessageFactory& BaseClient::GetMessageFactory()
  {
    gg_assert(m_messageFactory); 
    return *m_messageFactory;
  }

  NetworkSimulator* BaseClient::GetNetworkSimulator()
  {
    return m_networkSimulator;
  }

  reliable_endpoint_t* BaseClient::GetEndpoint()
  {
    return m_endpoint;
  }

  Connection& BaseClient::GetConnection()
  {
    gg_assert(m_connection); 
    return *m_connection;
  }


  u08* BaseClient::GetPacketBuffer()
  {
    return m_packetBuffer;
  }

  void* BaseClient::GetContext()
  {
    return m_context;
  }

  Adapter& BaseClient::GetAdapter()
  {
    gg_assert(m_adapter); 
    return *m_adapter;
  }


 
}// namespace GG