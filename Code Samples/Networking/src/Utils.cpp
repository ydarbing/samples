#include "NetworkPrecompiled.h"
#include "Utils.h"
#include "Netcode\Netcode.h"


#if GG_WITH_MBEDTLS
//#include "base64.h"
#include <mbedtls/base64.h>
#endif // #if YOJIMBO_WITH_MBEDTLS

namespace GG
{
#if __APPLE__

#include <unistd.h>
#include <mach/mach.h>
#include <mach/mach_time.h>

  void gg_sleep(double time)
  {
    usleep((int)(time * 1000000));
  }

  double gg_time()
  {
    static uint64_t start = 0;

    static mach_timebase_info_data_t timebase_info;

    if (start == 0)
    {
      mach_timebase_info(&timebase_info);
      start = mach_absolute_time();
      return 0.0;
    }

    uint64_t current = mach_absolute_time();

    if (current < start)
      current = start;

    return (double(current - start) * double(timebase_info.numer) / double(timebase_info.denom)) / 1000000000.0;
  }

#elif __linux
#include <unistd.h>
#include <time.h>

  void gg_sleep(double time)
  {
    usleep((int)(time * 1000000));
  }

  double gg_time()
  {
    static double start = -1;

    if (start == -1)
    {
      timespec ts;
      clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
      start = ts.tv_sec + double(ts.tv_nsec) / 1000000000.0;
      return 0.0;
    }

    timespec ts;
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    double current = ts.tv_sec + double(ts.tv_nsec) / 1000000000.0;
    if (current < start)
      current = start;
    return current - start;
  }

#elif defined(_WIN32)

#define NOMINMAX
#include <windows.h>

  void gg_sleep(double time)
  {
    const int milliseconds = time * 1000;
    Sleep(milliseconds);
  }

  static bool timer_initialized = false;
  static LARGE_INTEGER timer_frequency;
  static LARGE_INTEGER timer_start;

  double gg_time()
  {
    if (!timer_initialized)
    {
      QueryPerformanceFrequency(&timer_frequency);
      QueryPerformanceCounter(&timer_start);
      timer_initialized = true;
    }
    LARGE_INTEGER now;
    QueryPerformanceCounter(&now);
    if (now.QuadPart < timer_start.QuadPart)
      now.QuadPart = timer_start.QuadPart;
    return double(now.QuadPart - timer_start.QuadPart) / double(timer_frequency.QuadPart);
  }

#else
#error unsupported platform!
#endif



  void RandomBytes(u08* data, s32 bytes)
  {
    netcode_random_bytes(data, bytes);
  }


  u64 MurmurHash64(const void* key, u32 length, u64 seed)
  {
    const u64 m = 0xc6a4a7935bd1e995ULL;
    const u32 r = 47;

    u64 h = seed ^ (length * m);

    const u64* data = (const u64*)key;
    const u64* end = data + length / 8;

    while (data != end)
    {
#if GG_LITTLE_ENDIAN
      u64 k = *data++;
#else // #if GG_LITTLE_ENDIAN
      uint64_t k = *data++;
      uint8_t * p = (uint8_t*)&k;
      uint8_t c;
      c = p[0]; p[0] = p[7]; p[7] = c;
      c = p[1]; p[1] = p[6]; p[6] = c;
      c = p[2]; p[2] = p[5]; p[5] = c;
      c = p[3]; p[3] = p[4]; p[4] = c;
#endif // #if GG_LITTLE_ENDIAN

      k *= m;
      k ^= k >> r;
      k *= m;

      h ^= k;
      h *= m;
    }

    const u08* data2 = (const u08*)data;

    switch (length & 7)
    {
    case 7: h ^= u64(data2[6]) << 48;
    case 6: h ^= u64(data2[5]) << 40;
    case 5: h ^= u64(data2[4]) << 32;
    case 4: h ^= u64(data2[3]) << 24;
    case 3: h ^= u64(data2[2]) << 16;
    case 2: h ^= u64(data2[1]) << 8;
    case 1: h ^= u64(data2[0]);
      h *= m;
    };

    h ^= h >> r;
    h *= m;
    h ^= h >> r;

    return h;
  }

  void PrintBytes(const c08* label, const u08* data, s32 dataBytes)
  {
    printf("%s: ", label);
    for (s32 i = 0; i < dataBytes; ++i)
    {
      printf("0x%02x,", (s32)data[i]);
    }
    printf(" (%d bytes)\n", dataBytes);
  }

#if GG_WITH_MBEDTLS

  s32 Base64EncodeString(const c08* input, c08* output, s32 outputSize)
  {
    gg_assert(input);
    gg_assert(output);
    gg_assert(outputSize > 0);
    size_t outputLength = 0;
    const s32 inputLength = (s32)(strlen(input) + 1);

    s32 result = mbedtls_base64_encode((u08*)output, outputSize, &outputLength, (u08*)input, inputLength);
  
    return (result == 0) ? (s32)outputLength + 1 : -1;
  }

  s32 Base64DecodeString(const c08* input, c08* output, s32 outputSize)
  {
    gg_assert(input);
    gg_assert(output);
    gg_assert(outputSize > 0);

    size_t outputLength = 0;

    s32 result = mbedtls_base64_decode((u08*)output, outputSize, &outputLength, (const u08*)input, strlen(input));

    if (result != 0 || output[outputLength - 1] != '\0')
    {
      output[0] = '\0';
      return -1;
    }

    return (s32)outputLength;
  }

  s32 Base64EncodeData(const u08* input, s32 inputLength, c08* output, s32 outputSize)
  {
    gg_assert(input);
    gg_assert(output);
    gg_assert(outputSize > 0);
    size_t outputLength = 0;

    s32 result = mbedtls_base64_encode((u08*)output, outputSize, &outputLength, (u08*)input, inputLength);

    return (result == 0) ? (s32)outputLength : -1;
  }

  s32 Base64DecodeData(const c08* input, u08* output, s32 outputSize)
  {
    gg_assert(input);
    gg_assert(output);
    gg_assert(outputSize > 0);
    size_t outputLength = 0;

    s32 result = mbedtls_base64_decode((u08*)output, outputSize, &outputLength, (const u08*)input, strlen(input));

    return (result == 0) ? (s32)outputLength : -1;
  }

#endif // GG_WITH_MBEDTLS
}