#pragma once
#include "PacketQueue.h"

namespace Net
{

  class ReliabilitySystem
  {
  public:
    ReliabilitySystem(u32 maxSequence = 0xFFFFFFFF);
    void Reset();
    void PacketSent(s32 size);
    void PacketReceived(u32 sequence, s32 size);

    u32 GenerateAckBits();
    void ProcessAck(u32 ack, u32 ackBits);
    
    void Update(f32 dt);
    void Validate();

    static bool SequenceMoreRecent(u32 s1, u32 s2, u32 maxSequence);
    static s32 BitIndexForSequence(u32 sequence, u32 ack, u32 maxSequence);
    static u32 GenerateAckBits(u32 ack, const PacketQueue& receivedQueue, u32 maxSequence);
    static void ProcessAck(u32 ack, u32 ackBits, PacketQueue& pendingAckQueue, PacketQueue& ackedQueue,
      std::vector<u32>& acks, u32& ackedPackets, f32& rtt, u32 maxSequence);

    u32 GetMaxSequence()const;
    u32 GetLocalSequence()const;
    u32 GetRemoteSequence()const;

    void GetAcks(u32** acks, s32& count);
    u32 GetSentPackets()const;
    u32 GetReceivedPackets()const;
    u32 GetLostPackets()const;
    u32 GetAckedPackets()const;
    f32 GetSentBandwidth()const;
    f32 GetAckedBandwidth()const;
    f32 GetRoundTripTime()const;
    s32 GetHeaderSize()const;

  protected:
    void AdvanceQueueTime(f32 dt);
    void UpdateQueues();
    void UpdateStats();

  private:
    u32 m_maxSequence; // maximum sequence value before wrap around (used to test sequence wrap at low # values)
    u32 m_localSequence; // local sequence number for most recently sent packet
    u32 m_remoteSequence;// remote sequence number for most recently received packet
    u32 m_sentPackets;
    u32 m_recvPackets;
    u32 m_lostPackets;
    u32 m_ackedPackets;

    f32 m_sentBandwidth; // approx sent bandwidth over last second
    f32 m_ackedBandwidth; // approx acked bandwidth over last second
    f32 m_rtt;  // estimated RTT
    f32 m_rttMax; // max expected RTT

    std::vector<u32> m_acks; // acked packets from last set of packtet receives. Cleared each update

    PacketQueue m_sentQueue;    // sent packets used to calculate bandwidth (kept until m_rttMax)
    PacketQueue m_pendingAckQueue; // sent packets which have not been acked (kept until m_rttMax * 2)
    PacketQueue m_receivedQueue;// received packets for determining acks to send (kept up to most recent recv sequence - 32)
    PacketQueue m_ackedQueue;   // acked packets (kept until m_rttMax * 2)
  };

}// namespace Net