#pragma once

namespace Net
{
  class FlowControl
  {
  public:
    FlowControl();
    void Reset();
    void Update(f32 dt, f32 rtt);
    f32 GetSendRate();
  
  private:
    enum Mode{
      M_GOOD,
      M_BAD
    };

    Mode m_mode;
    f32 m_penaltyTime;
    f32 m_goodConnectionTime;
    f32 m_penaltyReductionAccumulator;
  };
}// namespace Net
