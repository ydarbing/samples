#pragma once

#define NET_UNIT_TEST

#include <chrono>
#include <thread>
#include <mutex>


// platform detection  
#define PLATFORM_WINDOWS 1 
#define PLATFORM_MAC 2 
#define PLATFORM_UNIX 3  

#if defined(_WIN32) 
  #define PLATFORM PLATFORM_WINDOWS 
#elif defined(__APPLE__) 
  #define PLATFORM PLATFORM_MAC 
#else 
  #define PLATFORM PLATFORM_UNIX 
#endif

#if PLATFORM == PLATFORM_WINDOWS  
  #include <winsock2.h>  
  #include <ws2ipdef.h> 
  typedef int socklen_t;
  typedef SOCKET Socket;
  //#pragma comment (lib, "ws2_32.lib")
  #pragma comment( lib, "wsock32.lib" )
#elif PLATFORM == PLATFORM_MAC ||  PLATFORM == PLATFORM_UNIX  
  #include <sys/socket.h> 
  #include <netinet/in.h> 
  #include <fcntl.h>  

#else
  #error unknown platform!
#endif


///flags
#define F_SERVER 0x0001

//packet header flags
enum PacketHeaderFlags
{
  PHF_SYN = 1 << 0, // sync used in handshake
  PHF_RST = 1 << 1, // reset
  PHF_FIN = 1 << 2, // end of connection
  PHF_WND = 1 << 3, // change in window size
  PHF_HRT = 1 << 4, // heartbeat
  PHF_DBG = 1 << 5, // debug
  PHF_RSN = 1 << 6, // resend
};
// HRT + RST = echo of a heartbeat
// SYN + RST = echo of a syn
// FIN + RST = echo of a fin


#include "CommonDefines.h"


