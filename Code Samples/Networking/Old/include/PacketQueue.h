#pragma once
#include "Packet.h"

namespace Net
{

  class PacketQueue : public std::list < PacketData >
  {
  public:
    bool Exists(u32 sequence);
    void InsertSorted(const PacketData& pd, u32 maxSequence);
    void VerifySorted(u32 maxSequence);

  protected:
    inline bool SequenceMoreRecent(u32 s1, u32 s2, u32 maxSequence)
    {
      return (s1 > s2) && (s1 - s2 <= maxSequence / 2) || (s2 > s1) && (s2 - s1 > maxSequence / 2);
    }
  };

}//namespace Net
   