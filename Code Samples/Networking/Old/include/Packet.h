#pragma once

namespace Net
{

  struct PacketData
  {
    u32 sequence;
    f32 time; // tune offset since packet was sent/received 
    s32 size; // packet size in bytes
  };
}//namespace Net
