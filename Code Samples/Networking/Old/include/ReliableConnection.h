#pragma once
#include "Connection.h"
#include "ReliabilitySystem.h"

namespace Net
{
  class ReliableConnection : public Connection
  {
  public:
    ReliableConnection(u32 protocolID, f32 timeout, u32 maxSequence = 0xFFFFFFFF);
    ~ReliableConnection();

    bool SendPacket(const u08 data[], s32 size);
    s32 ReceivePacket(u08 data[], s32 size);
    void Update(f32 dt);
    s32 GetHeaderSize()const;

    ReliabilitySystem& GetReliabilitySystem();

    // if NET_UNIT_TEST is not defined, this function will do nothing
    void SetPacketLossMask(u32 mask);

  protected:
    void WriteInteger(u08* data, u32 value);
    void ReadInteger(const u08* data, u32& value);

    void WriteHeader(u08* header, u32 sequence, u32 ack, u32 ackBits);
    void ReadHeader(const u08* header, u32& sequence, u32& ack, u32& ackBits);

    virtual void OnStop();
    virtual void OnDisconnect();

  private:
    void ClearData();

#ifdef NET_UNIT_TEST
    u32 m_packetLossMask;// mask sequence number, if non-zero, drop packet - for unit test only
#endif // NET_UNIT_TEST
    ReliabilitySystem m_reliabilitySystem;// reliability system: manages sequence numbers and acks, tracks network stats etc.
  };

}// namespace Net