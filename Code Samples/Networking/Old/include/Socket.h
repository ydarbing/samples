#pragma once

#include "Address.h"

namespace Net
{

  // platform independent wait for n seconds
#if PLATFORM == PLATFORM_WINDOWS
  void wait(f32 seconds);
#else
#include <unistd.h>
  void wait(f32 seconds);
#endif

  inline bool InitializeSockets()
  {
#if PLATFORM == PLATFORM_WINDOWS
    WSADATA wsaData;
    return WSAStartup(MAKEWORD(2, 2), &wsaData) == NO_ERROR;
#else
    return true;
#endif
  }

  inline void ShutdownSockets()
  {
#if PLATFORM == PLATFORM_WINDOWS 
    WSACleanup();
#endif
  }
  class Socket
  {
  public:
    Socket();
    ~Socket();

    bool Open(unsigned short port);
    void Close();
    bool IsOpen()const;
    bool Send(const Address& destination, const void* data, s32 size);
    s32 Receive(Address& sender, void* data, s32 size);

  private:
    s32 m_socket;
  };

}// namespace Net
