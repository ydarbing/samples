

#pragma once

#include "Address.h"
#include "Socket.h"

namespace Net
{
  class Connection
  {
  public:
    enum Mode
    {
      M_NONE,
      M_CLIENT,
      M_SERVER
    };

    Connection(u32 protocolID, f32 timeout);
    virtual ~Connection();

    bool Start(s32 port);
    void Stop();
    void Listen();
    void Connect(const Address& address);
    bool IsRunning()const;
    bool IsConnecting()const;
    bool ConnectFailed()const;
    bool IsConnected()const;
    bool IsListening()const;
    Mode GetMode()const;

    virtual void Update(f32 dt);
    virtual bool SendPacket(const u08 data[], s32 size);
    virtual s32 ReceivePacket(u08 data[], s32 size);

    virtual s32 GetHeaderSize()const;
  protected:
    void ClearData();
    virtual void OnStart();
    virtual void OnStop();
    virtual void OnConnect();
    virtual void OnDisconnect();

  private:
    enum State
    {
      S_DISCONNECTED,
      S_LISTENING,
      S_CONNECTING,
      S_CONNECT_FAIL,
      S_CONNECTED
    };
    
    u32 m_protocolID;
    f32 m_timeout;
    f32 m_timeoutAccumulator;
    bool m_running;
    Mode m_mode;
    State m_state;
    Socket m_socket;
    Address m_address;
  };

}// namespace Net