
#pragma once
#include <ostream>

namespace Net
{
  class Address
  {
  public:
    Address();
    Address(u08 a, u08 b, u08 c, u08 d, u16 port);
    Address(u32 address, u16 port);

    bool operator==(const Address& rhs) const;
    bool operator!=(const Address & rhs) const;
    bool operator<(const Address & other) const;

    void SetAddress(u08 a, u08 b, u08 c, u08 d, u16 port);
    void SetAddress(u32 addr, u16 port);

    u32 GetAddress() const;
    u08 GetA() const;
    u08 GetB() const;
    u08 GetC() const;
    u08 GetD() const;
    u16 GetPort() const;

    //void SetAddrPort(u16 port);
    //friend std::ostream& operator<<(std::ostream& str, const Address& addr);
    //istring GetStringAddress() const;
    //const sockaddr_in& GetSendToAddress() const;

  private:
    //char* GetIpStr(const struct sockaddr *sa, char *s, size_t maxlen);

  private:
    u32 m_address;
    u16 m_port;
    //sockaddr_in m_sockAddr;
  };

}//namespace Net