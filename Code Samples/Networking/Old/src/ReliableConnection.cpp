#include "NetworkPrecompiled.h"
#include "ReliableConnection.h"

namespace Net
{


  ReliableConnection::ReliableConnection(u32 protocolID, f32 timeout, u32 maxSequence /*= 0xFFFFFFFF*/)
    : Connection(protocolID, timeout), m_reliabilitySystem(maxSequence)
  {
    ClearData();
#ifdef NET_UNIT_TEST
    m_packetLossMask = 0;
#endif
  }

  ReliableConnection::~ReliableConnection()
  {
    if (IsRunning())
    {
      Stop();
    }
  }

  bool ReliableConnection::SendPacket(const u08 data[], s32 size)
  {
#ifdef NET_UNIT_TEST
    if (m_reliabilitySystem.GetLocalSequence() & m_packetLossMask)
    {
      m_reliabilitySystem.PacketSent(size);
      return true;
    }
#endif
    const s32 header = GetHeaderSize();
    u08* packet = new u08[header + size];
    u32 seq = m_reliabilitySystem.GetLocalSequence();
    u32 ack = m_reliabilitySystem.GetRemoteSequence();
    u32 ackBits = m_reliabilitySystem.GenerateAckBits();

    WriteHeader(packet, seq, ack, ackBits);
    memcpy(packet + header, data, size);
    if (!Connection::SendPacket(packet, size + header))
      return false;
    m_reliabilitySystem.PacketSent(size);
    delete[] packet;
    return true;
  }

  s32 ReliableConnection::ReceivePacket(u08 data[], s32 size)
  {
    const s32 header = GetHeaderSize();
    if (size <= header)
      return false;

    u08* packet = new u08[header + size];
    s32 receivedBytes = Connection::ReceivePacket(packet, header + size);
    if (receivedBytes == 0)
      return false;
    if (receivedBytes <= header)
      return false;

    u32 packetSequence = 0;
    u32 packetAck = 0;
    u32 packetAckBits = 0;

    ReadHeader(packet, packetSequence, packetAck, packetAckBits);
    m_reliabilitySystem.PacketReceived(packetSequence, receivedBytes - header);
    m_reliabilitySystem.ProcessAck(packetAck, packetAckBits);
    memcpy(data, packet + header, receivedBytes - header);
    delete[] packet;
    return receivedBytes - header;
  }

  void ReliableConnection::Update(f32 dt)
  {
    Connection::Update(dt);
    m_reliabilitySystem.Update(dt);
  }

  s32 ReliableConnection::GetHeaderSize() const
  {
    return Connection::GetHeaderSize() + m_reliabilitySystem.GetHeaderSize();
  }

  ReliabilitySystem& ReliableConnection::GetReliabilitySystem()
  {
    return m_reliabilitySystem;
  }

  void ReliableConnection::SetPacketLossMask(u32 mask)
  {
#ifdef NET_UNIT_TEST
    m_packetLossMask = mask;
#endif // NET_UNIT_TEST
  }


  void ReliableConnection::WriteInteger(u08* data, u32 value)
  {
    data[0] = (u08)(value >> 24);
    data[1] = (u08)((value >> 16) & 0xFF);
    data[2] = (u08)((value >> 8) & 0xFF);
    data[3] = (u08)(value & 0xFF);
  }

  void ReliableConnection::ReadInteger(const u08* data, u32& value)
  {
    value = (((u08)data[0] << 24) | ((u08)data[1] << 16) | ((u08)data[2] << 8) | ((u08)data[3]));
  }

  void ReliableConnection::WriteHeader(u08* header, u32 sequence, u32 ack, u32 ackBits)
  {
    WriteInteger(header, sequence);
    WriteInteger(header + 4, ack);
    WriteInteger(header + 8, ackBits);
  }

  void ReliableConnection::ReadHeader(const u08* header, u32& sequence, u32& ack, u32& ackBits)
  {
    ReadInteger(header, sequence);
    ReadInteger(header + 4, ack);
    ReadInteger(header + 8, ackBits);
  }

  void ReliableConnection::OnStop()
  {
    ClearData();
  }

  void ReliableConnection::OnDisconnect()
  {
    ClearData();
  }

  void ReliableConnection::ClearData()
  {
    m_reliabilitySystem.Reset();
  }

}

