#include "NetworkPrecompiled.h"

#include "Address.h"
#include "Ws2tcpip.h" // inet_ntop()

namespace Net
{
  Address::Address()
    : m_address(0), m_port(0)
  {
    //SetAddress(127, 0, 0, 1, 0);
  }

  Address::Address(u08 a, u08 b, u08 c, u08 d, u16 port)
    : m_port(port)
  {
    SetAddress(a, b, c, d, port);
  }

  Address::Address(u32 address, u16 port)
    : m_port(port)
  {
    m_address = address;//ntohl(address);
    //m_sockAddr.sin_family = AF_INET;
    //m_sockAddr.sin_addr.S_un.S_addr = address;
    //m_sockAddr.sin_port = htons(port);
  }

  void Address::SetAddress(u08 a, u08 b, u08 c, u08 d, u16 port)
  {
    SetAddress((a << 24) | (b << 16) | (c << 8) | d, port);
  }

  void Address::SetAddress(u32 addr, u16 port)
  {
    m_address = addr;
    m_port = port;

    //m_sockAddr.sin_family = AF_INET;
    //m_sockAddr.sin_addr.s_addr = htonl(m_address);
    //SetAddrPort(port);
  }

  u32 Address::GetAddress() const
  {
    return m_address;//m_sockAddr.sin_addr.S_un.S_addr;
  }

  u08 Address::GetA() const
  {
    return (u08)(m_address >> 24);
  }

  u08 Address::GetB() const
  {
    return (u08)(m_address >> 16);
  }

  u08 Address::GetC() const
  {
    return (u08)(m_address >> 8);
  }

  u08 Address::GetD() const
  {
    return (u08)(m_address);
  }

  u16 Address::GetPort() const
  {
    return m_port;
  }

  bool Address::operator==(const Address& rhs) const
  {
    if (rhs.m_address == m_address && rhs.m_port == m_port)
      return true;
    else
      return false;
  }


  bool Address::operator!=(const Address & rhs) const
  {
    return !(*this == rhs);
  }

  bool Address::operator<(const Address & rhs) const
  {
    // this is so we can use address as a key in std::map
    if (m_address < rhs.m_address)
      return true;
    if (m_address > rhs.m_address)
      return false;
    else
      return m_port < rhs.m_port;
  }

  /*

  void Address::SetAddrPort(u16 port)
  {
  m_port = port;
  m_sockAddr.sin_port = htons(port);
  }

  istring Address::GetStringAddress() const
  {
    istring ret;
    char str[16]; // length of IPV4 string
    ret.assign(inet_ntop(AF_INET, (PVOID)&(m_sockAddr.sin_addr), str, 16));
    
    return ret;
  }

  char* GetIpStr(const struct sockaddr* sa, char* s, size_t maxlen)
  {
    switch (sa->sa_family)
    {
    case AF_INET:
      inet_ntop(AF_INET, &(((struct sockaddr_in *)sa)->sin_addr),
        s, maxlen);
      break;
    case AF_INET6:
      inet_ntop(AF_INET6, &(((struct sockaddr_in6 *)sa)->sin6_addr),
        s, maxlen);
      break;
    default:
      strncpy(s, "Unknown AF", maxlen);
      return NULL;
    }

    return s;
  }

  const sockaddr_in& Address::GetSendToAddress() const
  {
    return m_sockAddr;
  }

  std::ostream& operator<<(std::ostream& str, const Address& addr)
  {
  str << addr.GetStringAddress() << ":" << addr.GetPort();

  return str;
  }

  */

} // namespace Net
