#include "NetworkPrecompiled.h"

#include "Connection.h"

namespace Net
{


  Connection::Connection(u32 protocolID, f32 timeout)
    : m_protocolID(protocolID), m_timeout(timeout), m_mode(M_NONE), m_running(false)
  {
    ClearData();
  }

  Connection::~Connection()
  {
    if (m_running)
      Stop();
  }

  bool Connection::Start(s32 port)
  {
    assertion(!m_running);

    std::cout << "Start connection on port " << port << std::endl;
    if (!m_socket.Open(port))
      return false;

    m_running = true;
    OnStart();
    return true;
  }

  void Connection::Stop()
  {
    assertion(m_running);

    std::cout << "Stop connection" << std::endl;
    bool connected = IsConnected();
    ClearData();
    m_socket.Close();
    m_running = false;
    if (connected)
      OnDisconnect();
    OnStop();
  }

  void Connection::Listen()
  {
    std::cout << "Server listening for connection" << std::endl;
    bool connected = IsConnected();
    ClearData();
    if (connected)
      OnDisconnect();
    m_mode = M_SERVER;
    m_state = S_LISTENING;
  }

  void Connection::Connect(const Address& address)
  {
    printf("client connecting to %d.%d.%d.%d:%d\n",
      address.GetA(), address.GetB(), address.GetC(), address.GetD(), address.GetPort());

    bool connected = IsConnected();

    ClearData();
    if (connected)
      OnDisconnect();
    m_mode = M_CLIENT;
    m_state = S_CONNECTING;
    m_address = address;
  }

  bool Connection::IsRunning() const
  {
    return m_running;
  }

  bool Connection::IsConnecting() const
  {
    return m_state == S_CONNECTING;
  }

  bool Connection::ConnectFailed() const
  {
    return m_state == S_CONNECT_FAIL;
  }

  bool Connection::IsConnected() const
  {
    return m_state == S_CONNECTED;
  }

  bool Connection::IsListening() const
  {
    return m_state == S_LISTENING;
  }

  Connection::Mode Connection::GetMode() const
  {
    return m_mode;
  }

  void Connection::Update(f32 dt)
  {
    assertion(m_running);

    m_timeoutAccumulator += dt;
    if (m_timeoutAccumulator > m_timeout)
    {
      if (m_state == S_CONNECTING)
      {
        std::cout << "Connect timed out" << std::endl;
        ClearData();
        m_state = S_CONNECT_FAIL;
        OnDisconnect();
      }
      else if (m_state == S_CONNECTED)
      {
        std::cout << "Connection timed out" << std::endl;
        ClearData();
        if (m_state == S_CONNECTING)
          m_state = S_CONNECT_FAIL;
        OnDisconnect();
      }
    }
  }

  bool Connection::SendPacket(const u08 data[], s32 size)
  {
    assertion(m_running);
    
    if (m_address.GetAddress() == 0)
      return false;

    u08* packet = new u08[size + 4];
    packet[0] = (u08) (m_protocolID >> 24);
    packet[1] = (u08)((m_protocolID >> 16) & 0xFF );
    packet[2] = (u08)((m_protocolID >> 8)  & 0xFF );
    packet[3] = (u08)((m_protocolID)       & 0xFF );
    memcpy(&packet[4], data, size);
    bool ret = m_socket.Send(m_address, packet, size + 4);
    delete[] packet;
    return ret;
  }

  s32 Connection::ReceivePacket(u08 data[], s32 size)
  {
    assertion(m_running);

    u08* packet = new u08[size + 4];
    Address sender;
    s32 bytesRead = m_socket.Receive(sender, packet, size + 4);
    if (bytesRead == 0)
    {
      return 0;
    }
    if (bytesRead <= 4)
    {
      return 0;
    }
    if (packet[0] != (u08)(m_protocolID >> 24) ||
        packet[1] != (u08)((m_protocolID >> 16) & 0xFF) ||
        packet[2] != (u08)((m_protocolID >> 8) & 0xFF) ||
        packet[3] != (u08)(m_protocolID & 0xFF))
    {
      return 0;
    }

    if (m_mode == M_SERVER && !IsConnected())
    {
      printf("server accepts connection from client %d.%d.%d.%d:%d\n",
        sender.GetA(), sender.GetB(), sender.GetC(), sender.GetD(), sender.GetPort());
      m_state = S_CONNECTED;
      m_address = sender;
      OnConnect();
    }
    if (sender == m_address)
    {
      if (m_mode == M_CLIENT && m_state == S_CONNECTING)
      {
        std::cout << "Client completed connection with server" << std::endl;
        m_state = S_CONNECTED;
        OnConnect();
      }
      m_timeoutAccumulator = 0.0f;
      memcpy(data, &packet[4], bytesRead - 4);
      delete[] packet;
      return bytesRead - 4;
    }
    return 0;
  }

  void Connection::ClearData()
  {
    m_state = S_DISCONNECTED;
    m_timeoutAccumulator = 0.0f;
    m_address = Address();
  }

  // size is in bytes
  s32 Connection::GetHeaderSize() const
  {
    return 4;
  }

  void Connection::OnStart()
  {
  }

  void Connection::OnStop()
  {
  }

  void Connection::OnConnect()
  {
  }

  void Connection::OnDisconnect()
  {
  }

}// namespace Net