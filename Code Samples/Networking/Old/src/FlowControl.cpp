#include "NetworkPrecompiled.h"
#include "FlowControl.h"

namespace Net
{
  // connection can either be good or bad with this class
  FlowControl::FlowControl()
  {
    Reset();
  }

  void FlowControl::Reset()
  {
    m_mode = M_BAD;
    m_penaltyTime = 4.0f;
    m_goodConnectionTime = 0.0f;
    m_penaltyReductionAccumulator = 0.0f;
  }

  void FlowControl::Update(f32 dt, f32 rtt)
  {
    const f32 rttThreshold = 250.0f;

    if (m_mode == M_GOOD)
    {
      if (rtt > rttThreshold)
      {
        std::cout << "*******Dropping to bad mode*******" << std::endl;
        m_mode = M_BAD;
        if (m_goodConnectionTime < 10.0f && m_penaltyTime < 60.0f)
        {
          m_penaltyTime *= 2.0f;
          if (m_penaltyTime > 60.0f)
          {
            m_penaltyTime = 60.0f;
          }
          printf("Penalty time increased to %.1f\n", m_penaltyTime);
        }
        m_goodConnectionTime = 0.0f;
        m_penaltyReductionAccumulator = 0.0f;
        return;
      }
      m_goodConnectionTime += dt;
      m_penaltyReductionAccumulator += dt;

      if (m_penaltyReductionAccumulator > 10.0f && m_penaltyTime > 1.0f)
      {
        m_penaltyTime /= 2.0f;
        if (m_penaltyTime < 1.0f)
        {
          m_penaltyTime = 1.0f;
        }
        printf("Penalty time decreased to %.1f\n", m_penaltyTime);
        m_penaltyReductionAccumulator = 0.0f;
      }
    }

    if (m_mode == M_BAD)
    {
      if (rtt <= rttThreshold)
        m_goodConnectionTime += dt;
      else
        m_goodConnectionTime = 0.0f;

      if (m_goodConnectionTime > m_penaltyTime)
      {
        std::cout << "*******Upgrading to goood mode*******" << std::endl;
        m_goodConnectionTime = 0.0f;
        m_penaltyReductionAccumulator = 0.0f;
        m_mode = M_GOOD;
        return;
      }
    }
  }

  // send rate = packets sent per second
  f32 FlowControl::GetSendRate()
  {
    return m_mode == M_GOOD ? 30.0f : 10.0f;
  }

}

