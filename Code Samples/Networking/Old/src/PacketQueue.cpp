#include "NetworkPrecompiled.h"
#include "PacketQueue.h"

namespace Net
{

  bool PacketQueue::Exists(u32 sequence)
  {
    for (iterator iter = begin(); iter != end(); ++iter)
    {
      if (iter->sequence == sequence)
      {
        return true;
      }
    }
    return false;
  }

  void PacketQueue::InsertSorted(const PacketData& pd, u32 maxSequence)
  {
    if (empty())
    {
      push_back(pd);
    }
    else
    {
      if (!SequenceMoreRecent(pd.sequence, front().sequence, maxSequence))
      {
        push_front(pd);
      }
      else if (SequenceMoreRecent(pd.sequence, back().sequence, maxSequence))
      {
        push_back(pd);
      }
      else
      {
        for (PacketQueue::iterator iter = begin(); iter != end(); ++iter)
        {
          assertion(iter->sequence != pd.sequence);
          if (SequenceMoreRecent(iter->sequence, pd.sequence, maxSequence))
          {
            insert(iter, pd);
            break;
          }
        }
      }
    }
  }

  void PacketQueue::VerifySorted(u32 maxSequence)
  {
    PacketQueue::iterator prev = end();
    for (PacketQueue::iterator iter = begin(); iter != end(); ++iter)
    {
      assertion(iter->sequence <= maxSequence);
      if (prev != end())
      {
        assertion(SequenceMoreRecent(iter->sequence, prev->sequence, maxSequence));
        prev = iter;
      }
    }
  }
}//namespace Net
