#include "NetworkPrecompiled.h"

#include "Socket.h"


namespace Net
{
  // platform independent wait for n seconds
#if PLATFORM == PLATFORM_WINDOWS
  void wait(f32 seconds)  { Sleep((int)(seconds * 1000.0f)); }
#else
#include <unistd.h>
  void wait(f32 seconds) { usleep((int)(seconds * 1000000.0f)); }
#endif

  Socket::Socket()
    : m_socket(0)
  {
  }

  Socket::~Socket()
  {
    Close();
  }

  bool Socket::Open(unsigned short port)
  {
    assertion(!IsOpen());

    m_socket = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (m_socket <= 0)
    {
      std::cout << "Failed to create socket" << std::endl;
      m_socket = 0;
      return false;
    }

    // bind socket to port
    sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    // convert host byte order to network byte order (big endian)
    address.sin_port = htons((u16)port);

    if (bind(m_socket, (const sockaddr*)&address, sizeof(sockaddr_in)) < 0)
    {
      std::cout << "Failed to create socket" << std::endl;
      Close();
      return false;
    }

    // socket is ready to send/receive packets
    // set socket to non-blocking
#if  PLATFORM == PLATFORM_WINDOWS  
    DWORD nonBlocking = 1;
    if (ioctlsocket(m_socket, FIONBIO, &nonBlocking) != 0)
    {
      std::cout << "Failed to set non-blocking" << std::endl;
      Close();
      return false;
    }
#elif PLATFORM == PLATFORM_MAC ||  PLATFORM == PLATFORM_UNIX 
    s32 nonBlocking = 1;
    if (fcntl(handle, F_SETFL, O_NONBLOCK, nonBlocking) == -1)
    {
      std::cout << "Failed to set non-blocking" << std::endl;
      Close();
      return false;
    }
#endif

    return true;
  }

  void Socket::Close()
  {
    if (m_socket != 0)
    {
#if PLATFORM == PLATFORM_WINDOWS
      closesocket(m_socket);
#elif PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX
      close(m_socket);
#endif
      m_socket = 0;
    }
  }

  bool Socket::IsOpen() const
  {
    return m_socket != 0;
  }

  bool Socket::Send(const Address& destination, const void* data, s32 size)
  {
    assertion(data);
    assertion(size > 0);

    if (m_socket == 0)
      return false;

    sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = htonl(destination.GetAddress());
    address.sin_port = htons((u16)destination.GetPort());
    
    s32 sentBytes = sendto(m_socket, (const c08*)data, size, 0, (sockaddr*)&address, sizeof(sockaddr_in));

    return sentBytes == size;
  }

  s32 Socket::Receive(Address& sender, void* data, s32 size)
  {
    assertion(data);
    assertion(size > 0);

    if (m_socket == 0)
      return false;

    sockaddr_in from;
    socklen_t fromLength = sizeof(from);

    s32 receivedBytes = recvfrom(m_socket, (c08*)data, size, 0, (sockaddr*)&from, &fromLength);

    if (receivedBytes <= 0)
      return 0;

    u32 address = ntohl(from.sin_addr.s_addr);
    u32 port = ntohs(from.sin_port);

    sender = Address(address, port);

    return receivedBytes;
  }

}// namespace Net
