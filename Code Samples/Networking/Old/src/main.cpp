
#include "NetworkPrecompiled.h"

#include "FlowControl.h"
#include "Address.h"
#include "ReliabilitySystem.h"
#include "ReliableConnection.h"
#include "Socket.h"

#pragma warning( disable : 4996 ) // _s functions



const int ServerPort = 30000;
const int ClientPort = 30001;
const int ProtocolId = 0x11223344;
const float DeltaTime = 1.0f / 30.0f;
const float SendRate = 1.0f / 30.0f;
const float TimeOut = 10.0f;
const int PacketSize = 256;



s32 main2(s32 argc, char* argv[])
{
  // parse command line

  enum Mode
  {
    Client,
    Server
  };

  Mode mode = Server;
  Net::Address address;

  if (argc >= 2)
  {
    s32 a, b, c, d;
    if (sscanf(argv[1], "%d.%d.%d.%d", &a, &b, &c, &d))
    {
      mode = Client;
      address = Net::Address(a, b, c, d, ServerPort);
    }
  }

  if (!Net::InitializeSockets())
  {
    std::cout << "Failed to initialize sockets" << std::endl;
    return 1;
  }

  Net::ReliableConnection connection(ProtocolId, TimeOut);
  const s32 port = mode == Server ? ServerPort : ClientPort;
  
  if (!connection.Start(port))
  {
    std::cout << "Could not start connection on port " << port << std::endl;
    return 1;
  }

  if (mode == Client)
    connection.Connect(address);
  else
    connection.Listen();

  bool connected = false;
  f32 sendAccumulator = 0.0f;
  f32 statsAccumulator = 0.0f;

  Net::FlowControl flowControl;

  while (true)
  {
    //update flow control

    if (connection.IsConnected())
      flowControl.Update(DeltaTime, connection.GetReliabilitySystem().GetRoundTripTime() * 1000.0f);

    const f32 sendRate = flowControl.GetSendRate();

    if (mode == Server && connected && !connection.IsConnected())
    {
      std::cout << "Reset flow control" << std::endl;
      flowControl.Reset();
      connected = false;
    }

    if (!connected && connection.IsConnected())
    {
      std::cout << "Client connected to server" << std::endl;
      connected = true;
    }

    if (!connected && connection.ConnectFailed())
    {
      std::cout << "Connection failed" << std::endl;
      break;
    }

    sendAccumulator += DeltaTime;
    while (sendAccumulator > 1.0f / SendRate)
    {
      u08 packet[PacketSize];
      memset(packet, 0, sizeof(packet));
      connection.SendPacket(packet, sizeof(packet));
      sendAccumulator -= 1.0f / SendRate;
    }

    while (true)
    {
      u08 packet[256];
      s32 bytesRead = connection.ReceivePacket(packet, sizeof(packet));
      if (bytesRead == 0)
      {
        break;
      }
    }

#ifdef SHOW_ACKS
    u08* acks = nullptr;
    s32 ackCount = 0;
    connection.GetReliabilitySystem().GetAcks(&acks, ackCount);

    if (ackCount > 0)
    {
      std::cout << "acks: " << acks[0];
      for (s32 i = 1; i < ackCount; ++i)
      {
        std::cout << "," << acks[i];
      }
      std::cout << std::endl;
    }
#endif

    connection.Update(DeltaTime);

    statsAccumulator += DeltaTime;

    while (statsAccumulator > 0.25f && connection.IsConnected())
    {
      f32 rtt = connection.GetReliabilitySystem().GetRoundTripTime();
      u32 sentPackets = connection.GetReliabilitySystem().GetSentPackets();
      u32 ackedPackets = connection.GetReliabilitySystem().GetAckedPackets();
      u32 lostPackets = connection.GetReliabilitySystem().GetLostPackets();

      f32 sentBandwidth = connection.GetReliabilitySystem().GetSentBandwidth();
      f32 ackedBandwidth = connection.GetReliabilitySystem().GetAckedBandwidth();

      printf("rtt %.1fms, sent %d, acked %d, lost %d (%.1f%%), sent bandwidth = %.1fkbps, acked bandwidth = %.1fkbps\n",
        rtt * 1000.0f, sentPackets, ackedPackets, lostPackets,
        sentPackets > 0 ? ((float)lostPackets/ (float)sentPackets*  100.0f) : 0.0f,
        sentBandwidth, ackedBandwidth);

      statsAccumulator -= 0.25f;
    }

    Net::wait(DeltaTime);
  }


  Net::ShutdownSockets();
  return 0;
}