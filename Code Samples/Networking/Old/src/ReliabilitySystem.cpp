#include "NetworkPrecompiled.h"

#include "ReliabilitySystem.h"


namespace Net
{

  ReliabilitySystem::ReliabilitySystem(u32 maxSequence /*= 0xFFFFFFFF*/)
  {
    m_maxSequence = maxSequence;
    Reset();
  }

  void ReliabilitySystem::Reset()
  {
    m_sentQueue.clear();
    m_receivedQueue.clear();
    m_pendingAckQueue.clear();
    m_ackedQueue.clear();

    m_localSequence = 0;
    m_remoteSequence = 0;
    m_sentPackets = 0;
    m_recvPackets = 0;
    m_lostPackets = 0;
    m_ackedPackets = 0;
    m_sentBandwidth = 0.0f;
    m_ackedBandwidth = 0.0f;
    m_rtt = 0.0f;
    m_rttMax = 1.0f;
  }

  void ReliabilitySystem::PacketSent(s32 size)
  {
    if (m_sentQueue.Exists(m_localSequence))
    {
      std::cout << "Local sequence " << m_localSequence << " exists" << std::endl;
      for (PacketQueue::iterator iter = m_sentQueue.begin(); iter != m_sentQueue.end(); ++iter)
      {
        std::cout << " + " << iter->sequence << std::endl;
      }
    }
    assertion(!m_sentQueue.Exists(m_localSequence));
    assertion(!m_pendingAckQueue.Exists(m_localSequence));

    PacketData data;
    data.sequence = m_localSequence;
    data.time = 0.0f;
    data.size = size;

    m_sentQueue.push_back(data);
    m_pendingAckQueue.push_back(data);
    ++m_sentPackets;
    ++m_localSequence;
    if (m_localSequence > m_maxSequence)
    {
      m_localSequence = 0;
    }
  }

  void ReliabilitySystem::PacketReceived(u32 sequence, s32 size)
  {
    ++m_recvPackets;
    if (m_receivedQueue.Exists(sequence))
    {
      return;
    }
    PacketData data;
    data.sequence = sequence;
    data.time = 0.0f;
    data.size = size;
    m_receivedQueue.push_back(data);
    if (SequenceMoreRecent(sequence, m_remoteSequence, m_maxSequence))
    {
      m_remoteSequence = sequence;
    }
  }

  u32 ReliabilitySystem::GenerateAckBits()
  {
    return GenerateAckBits(GetRemoteSequence(), m_receivedQueue, m_maxSequence);
  }

  u32 ReliabilitySystem::GenerateAckBits(u32 ack, const PacketQueue& receivedQueue, u32 maxSequence)
  {
    u32 ackBits = 0;
    for (PacketQueue::const_iterator iter = receivedQueue.begin(); iter != receivedQueue.end(); ++iter)
    {
      if (iter->sequence == ack || SequenceMoreRecent(iter->sequence, ack, maxSequence))
      {
        break;
      }
      s32 bitIndex = BitIndexForSequence(iter->sequence, ack, maxSequence);
      if (bitIndex <= 31)
      {
        ackBits |= 1 << bitIndex;
      }
    }
    return ackBits;
  }

  void ReliabilitySystem::ProcessAck(u32 ack, u32 ackBits)
  {
    ProcessAck(ack, ackBits, m_pendingAckQueue, m_ackedQueue, m_acks, m_ackedPackets, m_rtt, m_maxSequence);
  }

  void ReliabilitySystem::ProcessAck(u32 ack, u32 ackBits, PacketQueue& pendingAckQueue, PacketQueue& ackedQueue, std::vector<u32>& acks, u32& ackedPackets, f32& rtt, u32 maxSequence)
  {
    if (pendingAckQueue.empty())
      return;

    PacketQueue::iterator iter = pendingAckQueue.begin();
    while (iter != pendingAckQueue.end())
    {
      bool acked = false;

      if (iter->sequence == ack)
      {
        acked = true;
      }
      else if (!SequenceMoreRecent(iter->sequence, ack, maxSequence))
      {
        int bitIndex = BitIndexForSequence(iter->sequence, ack, maxSequence);
        if (bitIndex <= 31)
        {
          acked = (ackBits >> bitIndex) & 1;
        }
      }

      if (acked)
      {
        rtt += (iter->time - rtt) * 0.1f;

        ackedQueue.InsertSorted(*iter, maxSequence);
        acks.push_back(iter->sequence);
        ++ackedPackets;
        iter = pendingAckQueue.erase(iter);
      }
      else
      {
        ++iter;
      }
    }
  }

  void ReliabilitySystem::Update(f32 dt)
  {
    m_acks.clear();
    AdvanceQueueTime(dt);
    UpdateQueues();
    UpdateStats();
#ifdef NET_UNIT_TEST
    Validate();
#endif
  }

  void ReliabilitySystem::Validate()
  {
    m_sentQueue.VerifySorted(m_maxSequence);
    m_receivedQueue.VerifySorted(m_maxSequence);
    m_pendingAckQueue.VerifySorted(m_maxSequence);
    m_ackedQueue.VerifySorted(m_maxSequence);
  }

  // accounts for sequence number wrap around because 0 will be more recent than 255 (if maxSequence is 255)
  bool ReliabilitySystem::SequenceMoreRecent(u32 s1, u32 s2, u32 maxSequence)
  {
    return (s1 > s2) && (s1 - s2 <= maxSequence / 2) || (s2 > s1) && (s2 - s1 > maxSequence / 2);
  }

  s32 ReliabilitySystem::BitIndexForSequence(u32 sequence, u32 ack, u32 maxSequence)
  {
    assertion(sequence != ack);
    assertion(!SequenceMoreRecent(sequence, ack, maxSequence));

    if (sequence > ack)
    {
      assertion(ack < 33);
      assertion(maxSequence >= sequence);
      return ack + (maxSequence - sequence);
    }
    else
    {
      assertion(ack >= 1);
      assertion(sequence <= ack - 1);
      return ack - 1 - sequence;
    }
  }

  u32 ReliabilitySystem::GetMaxSequence() const
  {
    return m_maxSequence;
  }

  u32 ReliabilitySystem::GetLocalSequence() const
  {
    return m_localSequence;
  }

  u32 ReliabilitySystem::GetRemoteSequence() const
  {
    return m_remoteSequence;
  }

  void ReliabilitySystem::GetAcks(u32** acks, s32& count)
  {
    if (m_acks.empty())
    {
      count = 0;
      *acks = nullptr;
    }
    else
    {
      *acks = &m_acks[0];
      count = (s32)m_acks.size();
    }
  }

  u32 ReliabilitySystem::GetSentPackets() const
  {
    return m_sentPackets;
  }

  u32 ReliabilitySystem::GetReceivedPackets() const
  {
    return m_recvPackets;
  }

  u32 ReliabilitySystem::GetLostPackets() const
  {
    return m_lostPackets;
  }

  u32 ReliabilitySystem::GetAckedPackets() const
  {
    return m_ackedPackets;
  }

  f32 ReliabilitySystem::GetSentBandwidth() const
  {
    return m_sentBandwidth;
  }

  f32 ReliabilitySystem::GetAckedBandwidth() const
  {
    return m_ackedBandwidth;
  }

  f32 ReliabilitySystem::GetRoundTripTime() const
  {
    return m_rtt;
  }

  // size in bytes
  s32 ReliabilitySystem::GetHeaderSize() const
  {
    return 12;
  }

  void ReliabilitySystem::AdvanceQueueTime(f32 dt)
  {
    for (PacketQueue::iterator iter = m_sentQueue.begin(); iter != m_sentQueue.end(); ++iter)
      iter->time += dt;
    for (PacketQueue::iterator iter = m_receivedQueue.begin(); iter != m_receivedQueue.end(); ++iter)
      iter->time += dt;
    for (PacketQueue::iterator iter = m_pendingAckQueue.begin(); iter != m_pendingAckQueue.end(); ++iter)
      iter->time += dt;
    for (PacketQueue::iterator iter = m_ackedQueue.begin(); iter != m_ackedQueue.end(); ++iter)
      iter->time += dt;
  }

  void ReliabilitySystem::UpdateQueues()
  {
    const f32 epsilon = 0.001f;

    while (m_sentQueue.size() && m_sentQueue.front().time > m_rttMax + epsilon)
      m_sentQueue.pop_front();

    if (m_receivedQueue.empty() == false)
    {
      const u32 latestSequence = m_receivedQueue.back().sequence;
      const u32 minSequence = latestSequence >= 34 ? (latestSequence - 34) : m_maxSequence - (34 - latestSequence);

      while (m_receivedQueue.size() && !SequenceMoreRecent(m_receivedQueue.front().sequence, minSequence, m_maxSequence))
        m_receivedQueue.pop_front();
    }

    while (m_ackedQueue.size() && m_ackedQueue.front().time > m_rttMax * 2.0f - epsilon)
      m_ackedQueue.pop_front();

    while (m_pendingAckQueue.size() && m_pendingAckQueue.front().time > m_rttMax + epsilon)
    {
      m_pendingAckQueue.pop_front();
      ++m_lostPackets;
    }
  }

  void ReliabilitySystem::UpdateStats()
  {
    s32 sentBytesPerSecond = 0;
    for (PacketQueue::iterator iter = m_sentQueue.begin(); iter != m_sentQueue.end(); ++iter)
    {
      sentBytesPerSecond += iter->size;
    }

    s32 ackedPacketsPerSecond = 0;
    s32 ackedBytesPerSecond = 0;

    for (PacketQueue::iterator iter = m_ackedQueue.begin(); iter != m_ackedQueue.end(); ++iter)
    {
      if (iter->time >= m_rttMax)
      {
        ++ackedPacketsPerSecond;
        ackedBytesPerSecond += iter->size;
      }
    }

    sentBytesPerSecond = (s32)(sentBytesPerSecond / m_rttMax);
    ackedBytesPerSecond = (s32)(ackedBytesPerSecond / m_rttMax);
    m_sentBandwidth = sentBytesPerSecond * (8 / 1000.0f);
    m_ackedBandwidth = ackedBytesPerSecond * (8 / 1000.0f);
  }
}// namespace Net
