#include "PageRank.h"



PageRankTable::PageRankTable(double damp /*= DEFAULT_DAMP*/, double convg /*= DEFAULT_CONVERGENCE*/, size_t maxIters /*= DEFAULT_MAX_ITERATIONS*/)
  : m_damping(damp), m_convergence(convg), m_maxIterations(maxIters)
{
}


void PageRankTable::Reset()
{
  m_nodesToIndex.clear();
  m_indexToNodes.clear();
  m_pageRank.clear();
  m_rows.clear();
  m_numOutgoing.clear();
}

const size_t PageRankTable::GetNumRows() const
{
  return m_rows.size();
}

void PageRankTable::SetNumRows(size_t numRows)
{
  m_numOutgoing.resize(numRows);
  m_rows.resize(numRows);
}

const double PageRankTable::GetDamping() const
{
  return m_damping;
}

void PageRankTable::SetDamping(double damp)
{
  m_damping = damp;
}

void PageRankTable::SetConvergence(double convg)
{
  m_convergence = convg;
}

const double PageRankTable::GetConvergence()const
{
  return m_convergence;
}

const size_t PageRankTable::GetMaxIterations() const
{
  return m_maxIterations;
}

void PageRankTable::SetMaxIterations(size_t maxIterations)
{
  m_maxIterations = maxIterations;
}

const std::vector<double>& PageRankTable::GetPageRank()
{
  return m_pageRank;
}

const std::string PageRankTable::GetNodeName(size_t index)
{
  return m_indexToNodes[index];
}

const std::map<size_t, std::string>& PageRankTable::GetMapping()
{
  return m_indexToNodes;
}

// returns index from nodes to index where key is 
size_t PageRankTable::InsertNode(const std::string& key)
{
  size_t index = 0;

  std::map<std::string, size_t>::const_iterator i = m_nodesToIndex.find(key);
  if (i != m_nodesToIndex.end())
  {
    index = i->second;
  }
  else
  {
    index = m_nodesToIndex.size();
    m_nodesToIndex.insert(std::pair<std::string, size_t>(key, index));
    m_indexToNodes.insert(std::pair<size_t, std::string>(index, key));
  }
  return index;
}


bool PageRankTable::AddEdge(size_t from, size_t to)
{
  bool ret = false;
  size_t maxDim = std::max(from, to);

  if (m_rows.size() <= maxDim)
  {
    maxDim = maxDim + 1;
    m_rows.resize(maxDim);

    if (m_numOutgoing.size() <= maxDim)
    {
      m_numOutgoing.resize(maxDim);
    }
  }

  ret = InsertIntoVector(m_rows[to], from);

  if (ret)
  {
    ++m_numOutgoing[from];
  }


  return ret;
}

void PageRankTable::PageRank()
{
  std::vector<size_t>::iterator ci; // current incoming
  double diff = 1;
  size_t i = 0;
  double sumPR; // sum of current PageRank vector elements
  double danglingPR; // sum of current PageRank vector elements for dangling nodes
  size_t numIterations = 0;
  std::vector<double> oldPR;
  
  size_t numRows = m_rows.size();

  if (numRows == 0)
    return;
  
  m_pageRank.resize(numRows);

  m_pageRank[0] = 1;

  while (diff > m_convergence && numIterations < m_maxIterations)
  {
    sumPR = 0;
    danglingPR = 0;

    for (size_t k = 0; k < m_pageRank.size(); ++k)
    {
      double cpr = m_pageRank[k];
      sumPR += cpr;
      if (m_numOutgoing[k] == 0)
        danglingPR += cpr;

    }

    if (numIterations == 0)
    {
      oldPR = m_pageRank;
    }
    else
    {
      // normalize so we start with sum equal to 1
      for (i = 0; i < m_pageRank.size(); ++i)
        oldPR[i] = m_pageRank[i] / sumPR;
    }

    // after normalization the elements of the pagerank vector sum to 1
    sumPR = 1;

    // an element of the A x I vector  all elements are identical
    double oneAV = m_damping * danglingPR / numRows;
    // an element of 1 x I vector all elements are identical
    double oneIV = (1.0 - m_damping) * sumPR / numRows;

    diff = 0;
    for (i = 0; i < numRows; ++i)
    {
      // corresponding element of the H multiplication
      double h = 0.0;
      for (ci = m_rows[i].begin(); ci != m_rows[i].end(); ++ci)
      {
        double h_v = (m_numOutgoing[*ci]) ? 1.0 / m_numOutgoing[*ci] : 0.0;
        h += h_v * oldPR[*ci];
      }
      h *= m_damping;
      m_pageRank[i] = h + oneAV + oneIV;
      diff += fabs(m_pageRank[i] - oldPR[i]);
    }

    ++numIterations;
  }

}