#include "MergeSort.h"
#include "Quicksort.h"
#include <string>
#include <iostream> //cout
const unsigned TOO_LONG_TO_PRINT = 100;

template <typename T>
bool CheckSorted(const std::vector<T>& vec)
{
  if (vec.size() <= 1)
    return true;

  for (unsigned i = 1; i < vec.size(); ++i)
  {
    if (vec[i - 1] > vec[i])
      return false;
  }

  return true;
}

template <typename T>
void PrintVec(const std::vector<T>& vec)
{
  if (vec.empty() || vec.size() > TOO_LONG_TO_PRINT) // don't print empty or large arrays
    return;

  std::cout << vec[0];
  for (unsigned i = 1; i < vec.size(); ++i)
  {
    std::cout << ", " << vec[i];
  }
  std::cout << std::endl;
}

template <class T>
void PrintResults(const std::string& func, const std::vector<T>& v)
{
  PrintVec(v);
  std::cout << (CheckSorted(v) ? "PASSED: " : "FAILED: ") << func << std::endl;
}


void TestTopDown_empty()
{
  std::vector<int> testVec = {};
  std::vector<int> result;

  PrintVec(testVec);
  TopDownMergeSort(testVec, result);

  PrintResults(__FUNCTION__, result);
}

void TestTopDown_1_element()
{
  std::vector<int> testVec = { 100 };
  std::vector<int> result;

  PrintVec(testVec);
  TopDownMergeSort(testVec, result);

  PrintResults(__FUNCTION__, result);
}

void TestTopDown_already_sorted()
{
  std::vector<int> testVec = { 1, 2, 3, 4, 5, 8, 9, 10, 1100, 1111000 };
  std::vector<int> result;

  PrintVec(testVec);
  TopDownMergeSort(testVec, result);

  PrintResults(__FUNCTION__, result);
}

void TestTopDown_random_size_random_numbers()
{

}

void TestTopDown()
{
  TestTopDown_empty();
  TestTopDown_1_element();
  TestTopDown_already_sorted();
  //std::vector<int> testVec = { 1, 5, 7, 3, 2, 1, 6, 11, 2, 67, 91, 55 };
  //std::vector<int> result;
  //
  //PrintVec(testVec);
  //TopDownMergeSort(testVec, result);
  //
  //PrintResults(__FUNCTION__, result);

}

void TestBottomUp()
{
  std::vector<int> testVec = { 1, 5, 7, 3, 2, 1, 6, 11, 2, 67, 91, 55 };
  std::vector<int> result;

  PrintVec(testVec);
  BottomUpMergeSort(testVec, result);

  PrintResults(__FUNCTION__, result);
}



void TestMergeSort()
{
  TestTopDown();
  TestBottomUp();
}


void TestQuickSort()
{
  std::vector<int> testVec = { 1, 5, 7, 3, 2, 1, 6, 11, 2, 67, 91, 55 };
  Quicksort::Sort(testVec);
  PrintResults(__FUNCTION__, testVec);
}

int main(int argc, char ** argv)
{
  TestMergeSort();
  TestQuickSort();

  return 0;
}