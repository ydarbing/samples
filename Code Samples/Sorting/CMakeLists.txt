cmake_minimum_required(VERSION 3.17)

project("Sorting")

set(header_files
   include/MergeSort.h
   include/MergeSort.hpp
   include/PageRank.h
   include/PageRank.hpp   
   include/QuickSort.h
   include/QuickSort.hpp
)

set(source_files
  src/main.cpp
  src/PageRank.cpp
  )

add_executable("${PROJECT_NAME}" 
               ${source_files}
               ${header_files}
               )

target_include_directories("${PROJECT_NAME}" PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")
