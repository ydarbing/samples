#pragma once
#include <vector>

class Quicksort
{
public:
  Quicksort() {}
  ~Quicksort() {}

  template <class T>
  static void Sort(std::vector<T>& vec);
  template <class T>
  static void DoublePivotSort(std::vector<T>& vec);

private:
  template <class T>
  static void Sort(std::vector<T>& vec, const size_t& left, const size_t& right);

  template <class T>
  static void DoublePivotSort(std::vector<T>& vec, const size_t& left, const size_t& right);

  template <class T>
  static size_t Partition(std::vector<T>& vec, const size_t& left, const size_t& right);

  template <class T>
  static inline void SwapElements(std::vector<T>& vec, const size_t& x, const size_t& y);
};

#include "Quicksort.hpp"

