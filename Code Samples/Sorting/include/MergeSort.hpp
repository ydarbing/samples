#include <algorithm> // min

/****************************************************************
                      TOP DOWN 
                     MERGE SORT
*****************************************************************/
template <class T>
void TopDownMergeSort(std::vector<T>& toSort, std::vector<T>& sorted)
{
  TopDownMergeSort(toSort, sorted, toSort.size());
}

template <class T>
void TopDownMergeSort(std::vector<T>& toSort, std::vector<T>& sorted, size_t size)
{
  if (toSort.size() != sorted.size())
    sorted = toSort;
  
  TopDownSplitMerge(toSort, sorted, 0, size);
}

template <class T>
void TopDownSplitMerge(std::vector<T>& toSort, std::vector<T>& sorted, size_t iBegin, size_t iEnd)
{
  if (iEnd - iBegin < 2) // size is == 1 and is sorted
    return;

  size_t middle = (iEnd + iBegin) / 2;
  TopDownSplitMerge(sorted, toSort, iBegin, middle);
  TopDownSplitMerge(sorted, toSort, middle, iEnd);
  if (toSort[middle - 1] <= toSort[middle])// largest item in first half <= smallest item in second half  
    return;

  Merge(toSort, sorted, iBegin, middle, iEnd);
}

template <class T>
void Merge(std::vector<T>& toSort, std::vector<T>& sorted, size_t iBegin, size_t iMid, size_t iEnd)
{
  size_t i = iBegin;
  size_t j = iMid;

  for (size_t k = iBegin; k < iEnd; ++k)
  {
    if (i < iMid && (j >= iEnd || toSort[i] <= toSort[j]))
    {
      sorted[k] = toSort[i];
      ++i;
    }
    else
    {
      sorted[k] = toSort[j];
      ++j;
    }
  }
}

/****************************************************************
                       BOTTOM UP
                       MERGE SORT
*****************************************************************/

template <class T>
void BottomUpMergeSort(std::vector<T>& toSort, std::vector<T>& sorted)
{
  BottomUpMergeSort(toSort, sorted, toSort.size());
}

template <class T>
void BottomUpMergeSort(std::vector<T>& toSort, std::vector<T>& sorted, size_t size)
{
  if (toSort.size() != sorted.size())
    sorted = toSort;

  for (size_t width = 1; width < size; width *= 2)
  {
    // "toSort" is full of runs of length width.
    for (size_t i = 0; i < size; i += (2 * width))
    {
      // Merge two runs: toSort[i...i+width-1] and toSort[i+width ... i+2*width-1] to sorted[]
      // or copy toSort[i ... n-1] to sorted[] ( if(i+width >= size) )
      Merge(toSort, sorted, i, std::min(i + width, size), std::min(i + 2 * width, size));
    }
    // Now sorted array is full of runs of length 2*width.
    // Copy "sorted" to "toSort" for next iteration
    CopyVector(sorted, toSort, size);
    // Now "toSort" is full of runs of length 2*width.
  }
}



template <class T>
void CopyVector(std::vector<T>& takeFrom, std::vector<T>& copyTo, size_t size)
{
  for (size_t i = 0; i < size; ++i)
    copyTo[i] = takeFrom[i];
}

