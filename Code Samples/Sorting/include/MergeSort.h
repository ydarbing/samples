#pragma once
#include <vector>

/*
class MergeSort
{
public:
  template <class T>
  static void TopDownMergeSort(std::vector<T>& toSort, std::vector<T>& sorted);
  template <class T>
  static void TopDownMergeSort(std::vector<T>& toSort, std::vector<T>& sorted, size_t size);
  
  template <class T>
  static void BottomUpMergeSort(std::vector<T>& toSort, std::vector<T>& sorted);
  template <class T>
  static void BottomUpMergeSort(std::vector<T>& toSort, std::vector<T>& sorted, size_t size);

private:
  template <class T>
  static void TopDownSplitMerge(std::vector<T>& toSort, size_t begin, size_t end, std::vector<T>& sorted);
  template <class T>
  static void TopDownMerge(std::vector<T>& toSort, std::vector<T>& sorted, size_t size);
  template <class T>
  static void BottomUpMerge(std::vector<T>& toSort, size_t left, size_t right, size_t end, std::vector<T>& sorted);

  template <class T>
  static void CopyVector(std::vector<T>& B, size_t begin, size_t end, std::vector<T>& A);
  template <class T>
  static void CopyVector(std::vector<T>& B, std::vector<T>& A, size_t size);
};
*/

template <class T>
void TopDownMergeSort(std::vector<T>& toSort, std::vector<T>& sorted);

template <class T>
void TopDownMergeSort(std::vector<T>& toSort, std::vector<T>& sorted, size_t size);

template <class T>
void TopDownSplitMerge(std::vector<T>& toSort,  std::vector<T>& sorted, size_t iBegin, size_t iEnd);



template <class T>
void Merge(std::vector<T>& toSort, std::vector<T>& sorted, size_t iBegin, size_t iMid, size_t iEnd);



template <class T>
void BottomUpMergeSort(std::vector<T>& toSort, std::vector<T>& sorted);

template <class T>
void BottomUpMergeSort(std::vector<T>& toSort, std::vector<T>& sorted, size_t size);


template <class T>
void CopyVector(std::vector<T>& takeFrom, std::vector<T>& copyTo, size_t size);

#include "MergeSort.hpp"


