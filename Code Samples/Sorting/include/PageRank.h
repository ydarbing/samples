#pragma once
#include <map>
#include <string>
#include <vector>


const double DEFAULT_DAMP = 0.85;
const double DEFAULT_CONVERGENCE = 0.00001;
const size_t DEFAULT_MAX_ITERATIONS = 80;//150;//10000;

class PageRankTable
{
private:
  double m_damping;
  double m_convergence;
  size_t m_maxIterations;
  std::map<std::string, size_t> m_nodesToIndex;// mapping from string node ID to numeric
  std::map<size_t, std::string> m_indexToNodes;// mapping from numeric node ID to string
  std::vector<double> m_pageRank; // pagerank Table
  std::vector<size_t> m_numOutgoing;
  std::vector< std::vector<size_t> > m_rows;


public:
  PageRankTable(double damp = DEFAULT_DAMP, double convg = DEFAULT_CONVERGENCE, size_t maxIters = DEFAULT_MAX_ITERATIONS);
  const size_t GetNumRows() const;
  void SetNumRows(size_t numRows);
  const double GetDamping() const;
  void SetDamping(double damp);
  void SetConvergence(double convg);
  const double GetConvergence()const;
  const size_t GetMaxIterations() const;
  void SetMaxIterations(size_t maxIterations);
  const std::vector<double>& GetPageRank();
  const std::string GetNodeName(size_t index);
  const std::map<size_t, std::string>& GetMapping();
  void PageRank();
private:
  
  template <class Vector, class T>
  bool InsertIntoVector(Vector& v, const T& t);


  size_t InsertNode(const std::string& key);
  bool AddEdge(size_t from, size_t to);
  void Reset();



};

#include "PageRank.hpp"
