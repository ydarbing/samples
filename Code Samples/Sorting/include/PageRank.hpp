#include <algorithm> // max, lower_bound

/*
 From: M. H. Austern, "Why You Shouldn't Use set - and What You Should
 Use Instead", C++ Report 12:4, April 2000.
*/
template <class Vector, class T>
bool PageRankTable::InsertIntoVector(Vector& v, const T& t)
{
  typename Vector::iterator i = std::lower_bound(v.begin(), v.end(), t);

  if (i == v.end() || t < *i)
  {
    v.insert(i, t);
    return true;
  }
  else
  {
    return false;
  }
}
