

template <class T>
void Quicksort::Sort(std::vector<T>& vec)
{
  // shuffle the array

  Sort(vec, 0, vec.size() - 1);
}

template <class T>
void Quicksort::Sort(std::vector<T>& vec, const size_t& left, const size_t& right)
{
  if (right <= left)
    return;

  size_t i = Partition(vec, left, right);
  Sort(vec, left, i - 1);
  Sort(vec, i + 1, right);
}

template <class T>
size_t Quicksort::Partition(std::vector<T>& vec, const size_t& left, const size_t& right)
{
  size_t i = left;
  size_t j = right + 1;

  T v = vec[left];

  while (true)
  {
    while (vec[++i] < v)
      if (i == right)
        break;

    while (v < vec[--j])
      if (j == left)
        break;

    if (i >= j)
      break;

    SwapElements(vec, i, j);
  }
  // put partitioning item v at vec[j]
  SwapElements(vec, left, j);
  // now, vec[left .. j-1] <= vec[j] <= vec[j+1 .. right]
  return j;
}


template <class T>
void Quicksort::SwapElements(std::vector<T>& vec, const size_t& x, const size_t& y)
{
  T temp = vec[x];
  vec[x] = vec[y];
  vec[y] = temp;
}





template <class T>
void Quicksort::DoublePivotSort(std::vector<T>& vec)
{
  // could shuffle
  DoublePivotSort(vec, 0, vec.size() - 1);
}

template <class T>
void Quicksort::DoublePivotSort(std::vector<T>& vec, const size_t& left, const size_t& right)
{
  if (right <= left)
    return;

  T pivot1 = vec[left];
  T pivot2 = vec[right];


  if (pivot1 > pivot2){
    SwapElements(vec, left, right);
    pivot1 = vec[left];
    pivot2 = vec[right];
    //sort(input, lowIndex, highIndex);
  }
  else if (pivot1 == pivot2)
  {
    while (pivot1 == pivot2 && left < right)
    {
      ++left;
      pivot1 = vec[left];
    }
  }


  int i = left + 1;
  int lt = left + 1;
  int gt = right - 1;

  while (i <= gt)
  {
    if (vec[i] < pivot1)
      SwapElements(vec, i++, lt++);
    else if (pivot2 < vec[i])
      SwapElements(vec, i, gt--);
    else
      ++i;
  }


  SwapElements(vec, left, --lt);
  SwapElements(vec, right, ++gt);

  DoublePivotSort(vec, left, lt - 1);
  DoublePivotSort(vec, lt + 1, gt - 1);
  DoublePivotSort(vec, gt + 1, right);
}
