#include <stdio.h>
#include <iostream>

int main(int argc, const char * argv[])
{
  // just code for a binary search
  return 0;
}



int array(int* arr, int size, int val){

  for (int i = 0; i < size; ++i)
  {
    if (arr[i] == val)
      return i;
  }

  return -1;
}

//  0  2  4  5  7  8
// size = 5
// val = 0
int arraysorted(int* arr, int size, int val){
  int mid = size / 2;
  int newSize = mid;

  while (newSize != 0)
  {
    newSize /= 2;

    if (arr[mid] == val) //
      return mid;
    else if (arr[mid] < val)
      mid -= newSize;
    else // arra[mid] > val  
      mid += newSize;
  }

  if (arr[0] == val)
    return 0;
  else if (arr[size - 1] == val)
    return size - 1;

  return -1;
}

//  0  2  4  5  7  8
int arraysorted2(int* arr, int size, int val)
{
  int low = 0;
  int high = size - 1;

  while (low <= high)
  {
    int mid = (high + low) / 2;

    if (arr[mid] == val)
      return mid;
    else if (arr[mid] < val)
      low = mid + 1;
    else
      high = mid - 1;
  }

  return -1;
}
