#pragma once

#include <vector>


struct Data
{
  Data() {};
  Data(long _r, long _g, long _b, int _group) : r(_r), g(_g), b(_b), group(_group) {};
  long r = 0;
  long g = 0;
  long b = 0;
  int group = 0;
};

double Random2(double f);
double SqDistance(Data& p0, Data& p1);

void KPP(std::vector<Data>& points, std::vector<Data>& clusters);
int ClosestCluster(Data& pt, std::vector<Data>& clusters, double* closestDist);
void InitializeClusters(std::vector<Data>& points, std::vector<Data>& clusters, bool useKPP);


std::vector<Data> KMean(std::vector<Data>& points, int K, int maxIterations, bool useKPP);

