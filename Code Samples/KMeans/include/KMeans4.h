#pragma once

#include <vector>



template <typename T>
class KMeans4
{
public:
  KMeans4(unsigned K);
  ~KMeans4();


  public:
  class Data4
  {
  public:
    Data4() {};
    std::vector < T > vars;
    int group = 0;
  };

public:
  void SetK(unsigned K);
  void SetDataSize(unsigned size);
  void AddData(std::vector<T>& d);
  void AddData(Data4& data);
  // no checking for valid bounds
  void SetDataAt(Data4& data, int index);
  
  const std::vector<Data4>& GetData(void)const;
  const std::vector<Data4>& GetClusters(void)const;

  const std::vector<Data4>& KMean(int maxIterations, bool useKPP);

protected:
  void InitializeClusters(bool useKPP);
  void KPP();
  int ClosestCluster(Data4& pt, double& closestDist);
  double SqDistance(Data4& p0, Data4& p1);
  double Random(double dMin = 0, double dMax = 100.0);

private:
  std::vector<Data4> m_data;
  std::vector<Data4> m_clusters;
  unsigned m_K;

  bool m_clustersReady;
  bool m_clustersFinished;
};

#include "KMeans4.hpp"
