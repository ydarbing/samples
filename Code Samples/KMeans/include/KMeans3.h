#pragma once

#include <vector>

 struct Data3
 {
   Data3() {};
   std::vector < double > vars;
   int group;
 };


class KMeans3
{
public:
  KMeans3(unsigned K);
  ~KMeans3();

  void SetK(unsigned K);
  void AddData(Data3& data);
  void AddData(std::vector<Data3>& data); 
  void SetData(std::vector<Data3>& data);
  // only reserves space, does not allow [] operator
  void SetDataSize(unsigned size);
  // no checking for valid bounds
  void SetDataAt(Data3& data, int index);

  const std::vector<Data3>& GetData(void)const;
  const std::vector<Data3>& GetClusters(void)const;
  const std::vector<Data3>& KMean(int maxIterations, bool useKPP = false);

protected:
  void InitializeClusters(bool useKPP);
  void KPP();
  int ClosestCluster(Data3& pt, double& closestDist);
  double SqDistance(Data3& p0,Data3& p1);
  double Random(double dMin = 0, double dMax = 100.0);

private:
  std::vector<Data3> m_data;
  std::vector<Data3> m_clusters;
  unsigned m_K;

  bool m_clustersReady;
  bool m_clustersFinished;
};
