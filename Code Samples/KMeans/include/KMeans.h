#pragma once

struct Point
{
  float x;
  float y;
  int group;
};

float Random1(float f);
// returns an array or points
Point* CreateDistribution(int count, float radius);

float SqDistance(Point& p0, Point& p1);

int NearestCluster(Point& pt, Point* clusterCenter, int numClusters, float* d2);
// K Means Plus Plus
void KPP(Point* points, int numPoints, Point* clusterCenters, int numCenters);

Point* MakeClusters(Point* points, int numPoints, int numClusters);
// print distribution and clusters
void MakeResultFile(Point* points, int numPoints, Point* clusterCenters, int numClusters);
