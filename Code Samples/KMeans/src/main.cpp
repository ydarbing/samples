#include "KMeans.h"
#include "KMeans2.h"
#include "KMeans3.h"
#include "KMeans4.h"

#include <iostream>
#include "CImg.h"// for getting information from images

using namespace cimg_library;


void FirstTry(int K);
void SecondTry(int K, int maxIterations, char* filename, bool useKPP);
void ThirdTry(int  K, int maxIterations, char* filename, bool useKPP);
void FourthTry(int K, int maxIterations, char* filename, bool useKPP);

typedef const std::vector < Data3 > D3;
typedef const std::vector < KMeans4<long>::Data4 > D4;

void UpdateImage(CImg<unsigned char>& image, D3& data, D3& clusters);
void UpdateImage(CImg<unsigned char>& image, D4& data, D4& clusters);
void UpdateImage(CImg<unsigned char>& image, std::vector<Data>&data, std::vector<Data>& clusters);



int main(int argc, char** argv)
{
  srand((unsigned)time(nullptr));
  bool useKPP = false;
  int maxIterations = 20;
  int K = 5;// number of Clusters / colors to be in final image
  char* filename;

  if (argc == 2)
    filename = argv[1];
  else
    filename = "test1.jpg";

  /*  First Try K-Means   (KMeans)
    Positional based K-Means (x,y) */
  //FirstTry(K);

  /*  Second Try K-Means    (KMeans2)
    Ability to use RBG  (for images)   */
  SecondTry(K, maxIterations, filename, useKPP);

  /*  Third Try K-Means    (KMeans3)
    Have as many dimensions as you want */
  //ThirdTry(K, maxIterations, filename, useKPP);

  /*  Four Try K-Means    (KMeans4)
    Templated  */
  //FourthTry(K, maxIterations, filename, useKPP);

  return 0;
}

void FirstTry(int K)
{
  const float distributionRadius = 10.0f;
  const int numPoints = 100000;
  Point* dataSet = CreateDistribution(numPoints, distributionRadius);
  Point* clusters = MakeClusters(dataSet, numPoints, K);
  MakeResultFile(dataSet, numPoints, clusters, K);
  delete[] dataSet;
  delete[] clusters;
}


void SecondTry(int K, int maxIterations, char* filename, bool useKPP)
{
  CImg<unsigned char> image(filename);
  std::vector<Data> data;
  std::vector<Data> clusters;
  data.resize(image.width() * image.height());
  clusters.resize(K);

  for (int y = 0; y < image.height(); ++y)
  {
    for (int x = 0; x < image.width(); ++x)
    {
      int index = y * image.width() + x;
      data[index] = (Data(image(x, y, 0), image(x, y, 1), image(x, y, 2), -1));
    }
  }
  
  clusters = KMean(data, K, maxIterations, useKPP);

  UpdateImage(image, data, clusters);
  CImgDisplay k_disp(image, "KMean 2", 0);


  bool needsUpdate = false;
  while (!k_disp.is_closed() && !k_disp.is_keyESC() && !k_disp.is_keyQ())
  {

    // Handle display window resizing (if any)
    if (k_disp.is_resized())
      k_disp.resize().display(image);

    if (k_disp.is_keyPADADD())
    {
      ++K;
      needsUpdate = true;
    }
    else if (k_disp.is_keyPADSUB() && K >= 2)
    {
      --K;
      needsUpdate = true;
    }

    if (needsUpdate)
    {
      clusters = KMean(data, K, maxIterations, useKPP);
      UpdateImage(image, data, clusters);
      k_disp.display(image);
      needsUpdate = false;
    }
    // Temporize event loop
    cimg::wait(20);
  }
}

void ThirdTry(int K, int maxIterations, char* filename, bool useKPP)
{
  KMeans3 try3(K);

  CImg<unsigned char> image(filename);
  try3.SetDataSize(image._width * image._height);

  for (int y = 0; y < image.height(); ++y)
  {
    for (int x = 0; x < image.width(); ++x)
    {
      int index = y * image.width() + x;
      Data3 data;
      for (unsigned i = 0; i < 3; ++i)
        data.vars.push_back(image(x, y, i));
      
      try3.AddData(data);
    }
  }

  try3.KMean(maxIterations, useKPP);


  UpdateImage(image, try3.GetData(), try3.GetClusters());
  CImgDisplay k_disp(image, "KMean 3", 0);

  bool needsUpdate = false;
  while (!k_disp.is_closed() && !k_disp.is_keyESC() && !k_disp.is_keyQ())
  {

    // Handle display window resizing (if any)
    if (k_disp.is_resized())
      k_disp.resize().display(image);

    // Temporize event loop
    cimg::wait(20);
  }
}

void FourthTry(int K, int maxIterations, char* filename, bool useKPP)
{
  KMeans4<long> try4(K);
 
  CImg<unsigned char> image(filename);
  
  try4.SetDataSize(image._width * image._height);
  
  for (int y = 0; y < image.height(); ++y)
  {
    for (int x = 0; x < image.width(); ++x)
    {
      int index = y * image.width() + x;

      std::vector<long> vars;
      KMeans4<long>::Data4 data;
      data.group = -1;
      for (unsigned i = 0; i < 3; ++i)
      {
        data.vars.push_back(image(x, y, i));
        //vars.push_back(image(x, y, i));
      }
  
      //try4.AddData(vars);
      try4.AddData(data);
      //try4.SetDataAt(data, index);
    }
  }
  
  try4.KMean(maxIterations, useKPP);

  UpdateImage(image, try4.GetData(), try4.GetClusters());
  CImgDisplay k_disp(image, "KMean 4", 0);

  bool needsUpdate = false;
  while (!k_disp.is_closed() && !k_disp.is_keyESC() && !k_disp.is_keyQ())
  {

    // Handle display window resizing (if any)
    if (k_disp.is_resized())
      k_disp.resize().display(image);

    // Temporize event loop
    cimg::wait(20);
  }
}








void UpdateImage(CImg<unsigned char>& image, std::vector<Data>&data, std::vector<Data>& clusters)
{
  for (int y = 0; y < image.height(); ++y)
  {
    for (int x = 0; x < image.width(); ++x)
    {
      int dataIndex = y * image.width() + x;
      unsigned char* r = image.data(x, y, 0, 0);
      unsigned char* g = image.data(x, y, 1, 0);
      unsigned char* b = image.data(x, y, 2, 0);

      *r = (unsigned char)clusters[data[dataIndex].group].r;
      *g = (unsigned char)clusters[data[dataIndex].group].g;
      *b = (unsigned char)clusters[data[dataIndex].group].b;
    }
  }
}
void UpdateImage(CImg<unsigned char>& image, D4& data, D4& clusters)
{
  if (data.empty())
    return;
  unsigned size = static_cast<unsigned>(data[0].vars.size());

  for (int y = 0; y < image.height(); ++y)
  {
    for (int x = 0; x < image.width(); ++x)
    {
      int dataIndex = y * image.width() + x;

      for (unsigned i = 0; i < size; ++i)
      {
        unsigned char* r = image.data(x, y, i, 0);
        *r = (unsigned char)clusters[data[dataIndex].group].vars[i];
      }
    }
  }
}
void UpdateImage(CImg<unsigned char>& image, D3& data, D3& clusters)
{
  if (data.empty())
    return;
  unsigned size = static_cast<unsigned>(data[0].vars.size());

  for (int y = 0; y < image.height(); ++y)
  {
    for (int x = 0; x < image.width(); ++x)
    {
      int dataIndex = y * image.width() + x;

      for (unsigned i = 0; i < size; ++i)
      {
        unsigned char* r = image.data(x, y, i, 0);
        *r = (unsigned char)clusters[data[dataIndex].group].vars[i];
      }
    }
  }
}
