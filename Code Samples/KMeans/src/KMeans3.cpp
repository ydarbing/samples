#include "KMeans3.h"
#include <limits> // max double


KMeans3::KMeans3(unsigned K) : m_K(K), m_clustersReady(false), m_clustersFinished(false)
{
}


KMeans3::~KMeans3()
{
}


void KMeans3::InitializeClusters(bool useKPP)
{
  if (m_data.empty())
    return;

  unsigned varsSize = (unsigned)m_data[0].vars.size();
  m_clusters.clear();
  m_clusters.resize(m_K);
  for (unsigned i = 0; i < m_K; ++i)
    m_clusters[i].vars.resize(varsSize);
  

  if (useKPP)
  {
    KPP();
  }
  else
  {
    for (unsigned i = 0; i < m_K; ++i)
    {
      // get random position on the image and take its color
      int index = rand() % m_data.size();
      m_clusters[i].group = 0;
      for (unsigned j = 0; j < varsSize; ++j)
      {
        m_clusters[i].vars[j] = m_data[index].vars[j];
      }
    }
  }
  m_clustersReady = true;
}


const std::vector<Data3>& KMeans3::KMean(int maxIterations, bool useKPP)
{

  if (!m_clustersReady)
    InitializeClusters(useKPP);

  if (!useKPP) // KPP will set initial group
    for (unsigned i = 0; i < m_data.size(); ++i)
      m_data[i].group = -1;

  std::vector<Data3> tempClusters = m_clusters;

  unsigned changed = 0;
  double d = 0;

  do
  {
    for (unsigned i = 0; i < m_K; ++i)
    {
      tempClusters[i].vars = m_clusters[i].vars;
      tempClusters[i].group = 0;
    }

    changed = 0;
    // find closest Cluster Center of each point
    for (unsigned j = 0; j < m_data.size(); ++j)
    {
      int closestClusterIndex = ClosestCluster(m_data[j], d);
      if (closestClusterIndex != m_data[j].group)
      {
        changed++;
        m_data[j].group = closestClusterIndex;

        ++tempClusters[closestClusterIndex].group;

        unsigned size = (unsigned)m_data[j].vars.size();
        for (unsigned k = 0; k < size; ++k)
          tempClusters[closestClusterIndex].vars[k] += m_data[j].vars[k];
      }
    }

    // move the centroid based on collected data
    for (unsigned i = 0; i < m_K; ++i)
    {
      if (tempClusters[i].group == 0)
        continue;

      int denom = tempClusters[i].group + 1;
      unsigned size = (unsigned)m_data[i].vars.size();
      for (unsigned j = 0; j < size; ++j)
        m_clusters[i].vars[j] = tempClusters[i].vars[j] / denom;
    }

  } while (changed > (m_data.size() >> 10) && --maxIterations > 0); // stop when 99.9% of points are good


  m_clustersFinished = true;


  return m_clusters;
}


int KMeans3::ClosestCluster( Data3& pt, double& closestDist)
{
  double d = 0;
  closestDist = std::numeric_limits<double>::max();
  int closestIndex = pt.group;

  for (unsigned i = 0; i < m_K; ++i)
  {
    d = SqDistance(m_clusters[i], pt);
    if (d < closestDist)
    {
      closestDist = d;
      closestIndex = i;
    }
  }

  return closestIndex;
}

void KMeans3::KPP()
{
  double* d = new double[m_data.size()];
  double sum = 0;

  m_clusters[0] = m_data[rand() % m_data.size()];
  for (unsigned curCluster = 1; curCluster < m_clusters.size(); ++curCluster)
  {
    sum = 0;
    for (unsigned j = 0; j < m_data.size(); ++j)
    {
      ClosestCluster(m_data[j], d[j]);
      sum += d[j];
    }
    sum = Random(0, sum);
    for (unsigned j = 0; j < m_data.size(); ++j)
    {
      sum -= d[j];
      if (sum > 0)
        continue;
      m_clusters[curCluster] = m_data[j];
      break;
    }
  }

  for (unsigned i = 0; i < m_data.size(); ++i)
    m_data[i].group = ClosestCluster(m_data[i], sum);

  delete[] d;
}


double KMeans3::SqDistance( Data3& p0,  Data3& p1)
{
  unsigned size = (unsigned)p0.vars.size();
  double sqSum = 0;

  for (unsigned i = 0; i < size; ++i)
  {
    double distVec = p0.vars[i] - p1.vars[i];
    sqSum += distVec * distVec;
  }

  return sqSum;
}

void KMeans3::AddData(Data3& data)
{
  m_data.push_back(data);
}

void KMeans3::AddData(std::vector<Data3>& data)
{
  m_data.insert(std::end(m_data), std::begin(data), std::end(data));
}

void KMeans3::SetData(std::vector<Data3>& data)
{
  m_data = data;
}

void KMeans3::SetK(unsigned K)
{
  m_K = K;
  m_clustersReady = false;
  m_clustersFinished = false;
}

const std::vector<Data3>& KMeans3::GetData(void) const
{
  return m_data;
}

const std::vector<Data3>& KMeans3::GetClusters(void) const
{
  return m_clusters;
}

double KMeans3::Random(double dMin, double dMax)
{
  double f = (double)rand() / RAND_MAX;
  return dMin + f * (dMax - dMin);
}

void KMeans3::SetDataAt(Data3& data, int index)
{
  m_data[index] = data;
}

void KMeans3::SetDataSize(unsigned size)
{
  m_data.reserve(size);
}




