#include "KMeans2.h"
#include <limits> // max double


double SqDistance(Data& p0, Data& p1)
{
  double r = (p0.r - p1.r);
  double g = (p0.g - p1.g);
  double b = (p0.b - p1.b);

  return r*r + g*g + b*b;
}


int ClosestCluster(Data& pt, std::vector<Data>& clusters, double* closestDist)
{
  double d = 0;
  *closestDist = std::numeric_limits<double>::max();
  int closestIndex = pt.group;

  for (unsigned i = 0; i < clusters.size(); ++i)
  {
    d = SqDistance(clusters[i], pt);
    if (d < *closestDist)
    {
      *closestDist = d;
      closestIndex = i;
    }
  }

  return closestIndex;
}

void KPP(std::vector<Data>& points, std::vector<Data>& clusters)
{
  double* d = new double[points.size()];
  double sum = 0;

  clusters[0] = points[rand() % points.size()];
  for (unsigned curCluster = 1; curCluster < clusters.size(); ++curCluster)
  {
    sum = 0;
    for (unsigned j = 0; j < points.size(); ++j)
    {
      ClosestCluster(points[j], clusters, d + j);
      sum += d[j];
    }
    sum = Random2(sum);
    for (unsigned j = 0; j < points.size(); ++j)
    {
      sum -= d[j];
      if (sum > 0)
        continue;
      clusters[curCluster] = points[j];
      break;
    }
  }

  for (unsigned i = 0; i < points.size(); ++i)
    points[i].group = ClosestCluster(points[i], clusters, &sum);

  delete[] d;
}


std::vector<Data> KMean(std::vector<Data>& points, int K, int maxIterations, bool useKPP)
{
  std::vector<Data> clusters, tempClusters;
  clusters.resize(K);
  tempClusters.resize(K);

  InitializeClusters(points, clusters, useKPP);


  double clostestClusterDist = 0;

  unsigned changed = 0;

  do
  {
    for (int i = 0; i < K; ++i)
    {
      tempClusters[i].group = 0;
      tempClusters[i].r = clusters[i].r;
      tempClusters[i].g = clusters[i].g;
      tempClusters[i].b = clusters[i].b;
    }

    changed = 0;
    // find closest Cluster Center of each point
    for (unsigned j = 0; j < points.size(); ++j)
    {
      int closestClusterIndex = ClosestCluster(points[j], clusters, &clostestClusterDist);
      if (closestClusterIndex != points[j].group)
      {
        changed++;
        points[j].group = closestClusterIndex;

        ++tempClusters[closestClusterIndex].group;
        tempClusters[closestClusterIndex].r += points[j].r;
        tempClusters[closestClusterIndex].g += points[j].g;
        tempClusters[closestClusterIndex].b += points[j].b;
      }
    }

    // move the centroid based on collected points
    for (int i = 0; i < K; ++i)
    {
      if (tempClusters[i].group == 0)
        continue;

      clusters[i].r = tempClusters[i].r / (tempClusters[i].group + 1);
      clusters[i].g = tempClusters[i].g / (tempClusters[i].group + 1);
      clusters[i].b = tempClusters[i].b / (tempClusters[i].group + 1);
    }

  } while (changed >(points.size() >> 10) && --maxIterations > 0); // stop when 99.9% of points are good

  return clusters;
}


double Random2(double f)
{
  return static_cast<double>(rand()) / (static_cast<double>(RAND_MAX / f));
}

void InitializeClusters(std::vector<Data>& points, std::vector<Data>& clusters, bool useKPP)
{
  if (useKPP)
  {
    KPP(points, clusters);
  }
  else
  {
    for (unsigned i = 0; i < clusters.size(); ++i)
    {
      // get random position on the image and take its color
      int index = rand() % points.size();

      clusters[i].r = points[index].r;
      clusters[i].g = points[index].g;
      clusters[i].b = points[index].b;
      clusters[i].group = 0;
    }

    for (unsigned i = 0; i < points.size(); ++i)
      points[i].group = -1; // points don't start assigned to a group
  }
}

