#include "KMeans.h"
#include <stdio.h>
#include <cstdlib>
#include <cmath>

#pragma  warning(disable : 4996) // _s functions


const float PI = 3.14159f;
const float MAX_FLOAT = 3.402823466e+38f;

Point* CreateDistribution(int count, float radius)
{
  Point* ptArray = new Point[count];
  float ang, r;

  for (int i = 0; i < count; ++i)
  {
    ang = Random1(2.0f * PI);
    r = Random1(radius);

    ptArray[i].x = r * cos(ang);
    ptArray[i].y = r * sin(ang);
  }

  return ptArray;
}

float SqDistance(Point& p0, Point& p1)
{
  float x = p0.x - p1.x;
  float y = p0.y - p1.y;
  return x*x + y*y;
}


int NearestCluster(Point& pt, Point* clusterCenters, int numClusters, float* d2)
{
  float d = 0;
  float minDist = MAX_FLOAT;
  int minIndex = pt.group;

  for (int i = 0; i < numClusters; ++i)
  {
    d = SqDistance(clusterCenters[i], pt);
    if (minDist > d)
    {
      minDist = d;
      minIndex = i;
    }
  }

  if (d2 != nullptr)
    *d2 = minDist;

  return minIndex;
}

void KPP(Point* points, int numPoints, Point* clusterCenters, int numClusters)
{
  float* d = new float[numPoints];
  float sum = 0;

  clusterCenters[0] = points[rand() % numPoints];
  for (int curCluster = 1; curCluster < numClusters; ++curCluster)
  {
    sum = 0;
    for (int j = 0; j < numPoints; ++j)
    {
      NearestCluster(points[j], clusterCenters, curCluster, d + j);
      sum += d[j];
    }
    sum = Random1(sum);
    for (int j = 0; j < numPoints; ++j)
    {
      sum -= d[j];
      if (sum > 0)
        continue;
      clusterCenters[curCluster] = points[j];
      break;
    }
  }

  for (int i = 0; i < numPoints; ++i)
    points[i].group = NearestCluster(points[i], clusterCenters, numClusters, nullptr);

  delete[] d;
}



Point* MakeClusters(Point* points, int numPoints, int numClusters)
{
  Point* clusterCenters = new Point[numClusters];

  // make initial group random
  //for (int i = 0; i < numPoints; ++i)
  //  points[i].group = i % numClusters;
  // or use k++ to init
  KPP(points, numPoints, clusterCenters, numClusters);

  int changed = 0;

  do
  {
    for (int i = 0; i < numClusters; ++i)
    {
      clusterCenters[i].group = 0;
      clusterCenters[i].x = 0;
      clusterCenters[i].y = 0;
    }
    Point* c = nullptr;
    for (int j = 0; j < numPoints; ++j)
    {
      c = clusterCenters + points[j].group;
      c->group++;
      c->x += points[j].x;
      c->y += points[j].y;
    }


    for (int i = 0; i < numClusters; ++i)
    {
      clusterCenters[i].x /= clusterCenters[i].group;
      clusterCenters[i].y /= clusterCenters[i].group;
    }

    changed = 0;
    // find closest Cluster Center of each point
    for (int j = 0; j < numPoints; ++j)
    {
      int closestClusterIndex = NearestCluster(points[j], clusterCenters, numClusters, 0);
      if (closestClusterIndex != points[j].group)
      {
        changed++;
        points[j].group = closestClusterIndex;
      }
    }

  } while (changed >(numPoints >> 10)); // stop when 99.9% of points are good


  for (int i = 0; i < numClusters; ++i)
    clusterCenters[i].group = i;

  return clusterCenters;
}

void MakeResultFile(Point* points, int numPoints, Point* clusterCenters, int numClusters)
{

  FILE * pFile = fopen("KMean_1_Out.eps", "w");
  int W = 400;
  int H = 400;
  double min_x, max_x, min_y, max_y, scale, cx, cy;
  double *colors = new double[numClusters * 3];

  for (int i = 0; i < numClusters; ++i)
  {
    colors[3 * i + 0] = (3 * (i + 1) % 11) / 11.;
    colors[3 * i + 1] = (7 * i % 11) / 11.;
    colors[3 * i + 2] = (9 * i % 11) / 11.;
  }

  max_x = max_y = -(min_x = min_y = HUGE_VAL);
  for (int j = 0; j < numPoints; ++j)
  {
    if (max_x < points[j].x) max_x = points[j].x;
    if (min_x > points[j].x) min_x = points[j].x;
    if (max_y < points[j].y) max_y = points[j].y;
    if (min_y > points[j].y) min_y = points[j].y;
  }
  scale = W / (max_x - min_x);
  if (scale > H / (max_y - min_y)) scale = H / (max_y - min_y);
  cx = (max_x + min_x) / 2;
  cy = (max_y + min_y) / 2;

  fprintf(pFile, "%%!PS-Adobe-3.0 EPSF-3.0\n%%%%BoundingBox: -5 -5 %d %d\n", W + 10, H + 10);
  fprintf(pFile, "/l {rlineto} def /m {rmoveto} def\n"
    "/c { .25 sub exch .25 sub exch .5 0 360 arc fill } def\n"
    "/s { moveto -2 0 m 2 2 l 2 -2 l -2 -2 l closepath "
    "	gsave 1 setgray fill grestore gsave 3 setlinewidth"
    " 1 setgray stroke grestore 0 setgray stroke }def\n"
    );
  for (int i = 0; i < numClusters; ++i)
  {
    fprintf(pFile, "%g %g %g setrgbcolor\n",
      colors[3 * i], colors[3 * i + 1], colors[3 * i + 2]);
    for (int j = 0; j < numPoints; ++j)
    {
      if (points[j].group != i)
        continue;
      fprintf(pFile, "%.3f %.3f c\n",
        (points[j].x - cx) * scale + W / 2,
        (points[j].y - cy) * scale + H / 2);
    }
    fprintf(pFile, "\n0 setgray %g %g s\n",
      (clusterCenters[i].x - cx) * scale + W / 2,
      (clusterCenters[i].y - cy) * scale + H / 2);
  }
  fprintf(pFile, "\n%%%%EOF");
  delete[] colors;
}


float Random1(float f)
{
  return static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / f));
}

