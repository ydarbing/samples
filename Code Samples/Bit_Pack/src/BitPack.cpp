#include "BitPack.h"
#include <iostream>
#include <bitset>
#include <cassert>
// testing
#include <vector>
#include <ctime>

BitArray::BitArray(bool read)
{
  if (read)
    ResetForRead();
  else
    ResetForWrite();
}

void BitArray::PrintPushedBits(void)
{
  // have to print out byte by byte because bitset must take const value
  std::cout << "Number of Bits Pushed: " << GetNumberOfBitsPushed() << std::endl;
  for (int i = 0; i < m_numBytes; ++i)
  {
    std::cout << "m_data[" << i << "] = ";
    std::cout << std::bitset<8>(m_data[i]) << std::endl;
  }
}

void BitArray::Serialize(bool& b)
{
  int temp = b ? 1 : 0;
  TPlateSerializeInt<int>(&temp, 0, 1);
  b = temp ? 1 : 0;
}

// serialize unsigned char
void BitArray::Serialize(unsigned char& val, int imin, int imax)
{
  long temp = val;
  SerializeInt(&temp, imin, imax);
  val = static_cast<unsigned char>(temp);
}

// serialize unsigned short
void BitArray::Serialize(unsigned short& val, unsigned short imin, unsigned short imax)
{
  unsigned int temp = val;
  SerializeUInt(&temp, imin, imax);
  val = static_cast<unsigned short>(temp);
}

// serialize unsigned int
void BitArray::Serialize(unsigned int& val, unsigned int imin, unsigned int imax)
{
  unsigned int temp = val;
  SerializeUInt(&temp, imin, imax);
  val = static_cast<unsigned int>(temp);
}

// serialize unsigned int
void BitArray::Serialize(unsigned long& val, unsigned long imin, unsigned long imax)
{
  unsigned long temp = val;
  SerializeULInt(&temp, imin, imax);
  val = static_cast<unsigned long>(temp);
}

// serialize signed short
void BitArray::Serialize(short& val, short imin, short imax)
{
  long temp = val;
  SerializeInt(&temp, imin, imax);
  val = static_cast<short>(temp);
}

// serialize signed int
void BitArray::Serialize(int& val, int imin, int imax)
{
  long temp = val;
  SerializeInt(&temp, imin, imax);
  val = static_cast<int>(temp);
}

// serialize signed float
void BitArray::Serialize(float& val, float fmin, float fmax, int totalNumBits)
{
  SerializeFloat(&val, fmin, fmax, totalNumBits);
}

void BitArray::AppendData(const char* data, int numBytes)
{
  assert(m_numBytes + numBytes < MAX_BYTES);
  int actuallyNextByte = 0;
  if (m_bitPos != 0)
    actuallyNextByte = 1;

  std::memcpy(m_data + m_bytePos + actuallyNextByte, data, numBytes);
  m_numBytes += numBytes;
  m_bytePos += numBytes;
  m_bitPos = 7; // when setting the next bit it will read the next byte
}

void BitArray::AppendData(const unsigned char* data, int numBytes)
{
  assert(m_numBytes + numBytes < MAX_BYTES);
  int actuallyNextByte = 0;
  if (m_bitPos != 0)
    actuallyNextByte = 1;

  std::memcpy(m_data + m_bytePos + actuallyNextByte, data, numBytes);
  m_numBytes += numBytes;
  m_bytePos += numBytes;
  m_bitPos = 0;
}

void BitArray::SerializeInt(long* val, long imin, long imax)
{
  TPlateSerializeInt<long>(val, imin, imax);
}
void BitArray::SerializeUInt(unsigned int* val, unsigned int imin, unsigned int imax)
{
  TPlateSerializeInt<unsigned int>(val, imin, imax);
}

void BitArray::SerializeULInt(unsigned long* val, unsigned long imin, unsigned long imax)
{
  TPlateSerializeInt<unsigned long>(val, imin, imax);
}

void BitArray::SerializeFloat(float* val, float fmin, float fmax, int totalNumBits)
{
  long range = (1 << totalNumBits) - 1;

  if (m_reading)
  {
    long n = 0;
    SerializeInt(&n, 0, range);
    // try and get float back as close to the original value
    *val = static_cast<float>(n)* (fmax - fmin) / static_cast<float>(range)+fmin;
  }
  else
  {
    float clamp = *val;
    if (clamp > fmax)
      clamp = fmax;
    else if (clamp < fmin)
      clamp = fmin;
    // convert into int so it can be easily serialized
    long n = static_cast<int>((static_cast<float>(range)) / (fmax - fmin) * (clamp - fmin));
    SerializeInt(&n, 0, range);
  }
}

bool BitArray::IsReading(void)const
{
  return m_reading;
}

const int BitArray::GetNumberOfBitsPushed(void)const
{
  // plus 1 because bit pos is 0-7 based
  return (m_bytePos * 8) + m_bitPos + 1;
}

const int BitArray::GetNumberOfBytesPushed(void)const
{
  return m_numBytes;
}

void BitArray::PushBit(int bit)
{
  if (m_bytePos >= MAX_BYTES)
    assert(false); // trying to push to many bits
  // check if need to move to next byte
  if (++m_bitPos == 8)
  {
    m_bitPos = 0;
    ++m_bytePos;
    assert(m_bytePos < MAX_BYTES);
    // initialize byte
    m_data[m_bytePos] = 0;
    ++m_numBytes;
  }
  assert(m_bytePos < MAX_BYTES);
  m_data[m_bytePos] |= (bit & 1) << m_bitPos;
}

int BitArray::PopFrontBit(void)
{
  // check if ready to read next byte
  if (++m_bitPos == 8)
  {
    m_bitPos = 0;
    ++m_bytePos;
    m_readByte = m_data[m_bytePos];
  }
  unsigned char temp = m_readByte & 1;
  // move over next bit
  m_readByte >>= 1;
  return temp;
}

//unsigned char* BitArray::GetData(void)const
//{
//  return m_data;
//}

void BitArray::ResetForRead(void)
{
  m_bytePos = -1; // incremented right before it read
  m_bitPos = 7; // incremented right before it read
  m_reading = true;
}

void BitArray::ResetForWrite(void)
{
  m_bytePos = -1; // incremented right before it writes
  m_bitPos = 7; // incremented right before it writes
  m_numBytes = 0;
  m_reading = false;
}


void BitArray::ReadBits(unsigned char* pOut, int numBits)
{
  unsigned char byte = 0;

  while (numBits >= 8)
  {
    byte = PopFrontBit();
    for (int i = 1; i < 8; ++i)
      byte |= PopFrontBit() << i;
    *pOut = byte;
    numBits -= 8;
  }

  if (numBits == 0)
    return;
  // read remaining bits in last byte
  byte = PopFrontBit();
  for (int i = 1; i < numBits; ++i)
    byte |= PopFrontBit() << i;

  *pOut = byte;
}

void BitArray::WriteBits(const unsigned char* pIn, int numBits)
{
  unsigned char byte = 0;

  while (numBits >= 8)
  {
    byte = *pIn;
    // walk through byte pushing either 0 or 1
    for (int i = 0; i < 8; ++i)
      PushBit((byte >> i) & 1);
    numBits -= 8;
    pIn++;
  }

  byte = *pIn;
  // push the rest of the bits
  for (int i = 0; i < numBits; ++i)
    PushBit((byte >> i) & 1);
}
// given a range, find number of bits needed really fast
//unsigned int BitArray::BitsNeeded(unsigned int range)
//{
//	// See bit twiddling hacks (google/bing/yahoo it)
//	static const int MultiplyDeBruijnBitPosition[32] = 
//	{
//		0, 9, 1, 10, 13, 21, 2, 29, 11, 14, 16, 18, 22, 25, 3, 30,
//		8, 12, 20, 28, 15, 17, 24, 7, 19, 27, 23, 6, 26, 5, 4, 31
//	};
//
//	range |= range >> 1; // first round down to one less than a power of 2 
//	range |= range >> 2;
//	range |= range >> 4;
//	range |= range >> 8;
//	range |= range >> 16;
//
//	return 1 + MultiplyDeBruijnBitPosition[(unsigned int)(range * 0x07C4ACDDU) >> 27];
//}

int BitArray::BitsNeeded(unsigned long long range)
{
  static const int DeBruijnBitPos[64] = {
    63, 0, 58, 1, 59, 47, 53, 2,
    60, 39, 48, 27, 54, 33, 42, 3,
    61, 51, 37, 40, 49, 18, 28, 20,
    55, 30, 34, 11, 43, 14, 22, 4,
    62, 57, 46, 52, 38, 26, 32, 41,
    50, 36, 17, 19, 29, 10, 13, 21,
    56, 45, 25, 31, 35, 16, 9, 12,
    44, 24, 15, 8, 23, 7, 6, 5 };

  range |= range >> 1; // first round down to one less than a power of 2 
  range |= range >> 2;
  range |= range >> 4;
  range |= range >> 8;
  range |= range >> 16;
  range |= range >> 32;
  return 1 + DeBruijnBitPos[((unsigned long long)((range - (range >> 1)) * 0x07EDD5E59A4E28C2)) >> 58];
}
