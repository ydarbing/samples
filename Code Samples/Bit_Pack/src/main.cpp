#include "BitPack.h"
#include <cassert>
#include <ctime>
#include <cstdlib> //srand, rand
#include <vector>
#include <stdint.h> // int min and max


int main(int argc, char*argv[])
{
  BitArray bay(false);

  const float fmin = 0.0f, fmax = 3.0f;
  const float num = 2.987654f;
  float flen = num;
  bay.Serialize(flen, fmin, fmax, 10);
  flen = num;
  bay.Serialize(flen, fmin, fmax, 16);
  flen = num;
  bay.Serialize(flen, fmin, fmax, 20);
  flen = num;
  bay.Serialize(flen, fmin, fmax, 22);
  flen = num;
  bay.Serialize(flen, fmin, fmax, 25);
  flen = num;
  bay.Serialize(flen, fmin, fmax, 28);
  bay.PrintPushedBits();
  bay.ResetForRead();
  float fc1 = 0, fc2 = 0, fc3 = 0, fc4 = 0, fc5 = 0, fc6 = 0;
  bay.Serialize(fc1, fmin, fmax, 10);
  bay.Serialize(fc2, fmin, fmax, 16);
  bay.Serialize(fc3, fmin, fmax, 20);
  bay.Serialize(fc4, fmin, fmax, 22);
  bay.Serialize(fc5, fmin, fmax, 25);
  bay.Serialize(fc6, fmin, fmax, 28);
  bay.ResetForWrite();
  // Signed MIN MAX CHECKS
  short ssMin = INT16_MIN, ssMinCheck = 0;
  short ssMax = INT16_MAX, ssMaxCheck = 0;
  short ssMin1 = INT16_MIN + 1, ssMin1Check = 0;
  short ssMax1 = INT16_MAX - 1, ssMax1Check = 0;
  int sMin = INT32_MIN, sMinCheck = 0;
  int sMax = INT32_MAX, sMaxCheck = 0;
  int sMin1 = INT32_MIN + 1, sMin1Check = 0;
  int sMax1 = INT32_MAX - 1, sMax1Check = 0;
  // Unsigned MIN MAX CHECKS
  unsigned short uuMin = 0, uuMinCheck = 0;
  unsigned short uuMax = UINT16_MAX, uuMaxCheck = 0;
  unsigned int uMin = 0, uMinCheck = 0;
  unsigned int uMax = UINT32_MAX, uMaxCheck = 0;
  unsigned long Max = UINT32_MAX, MaxCheck = 0;
  // unsigned writing checks
  //bay.Serialize(uuMin, 0, UINT16_MAX);
  //bay.Serialize(uuMax, 0, UINT16_MAX);
  //bay.Serialize(uMin, 0, UINT32_MAX);
  //bay.Serialize(uMax, 0, UINT32_MAX);
  //bay.Serialize(Max, 0, UINT32_MAX);
  //// 16 writing checks
  //bay.Serialize(ssMin, INT16_MIN, INT16_MAX);
  //bay.Serialize(ssMax, INT16_MIN, INT16_MAX);
  //bay.Serialize(ssMin1, INT16_MIN, INT16_MAX);
  //bay.Serialize(ssMax1, INT16_MIN, INT16_MAX);
  // 32 writing checks
  bay.Serialize(sMin, INT32_MIN, INT32_MAX);
  bay.Serialize(sMax, INT32_MIN, INT32_MAX);
  bay.Serialize(sMin1, INT32_MIN, INT32_MAX);
  bay.Serialize(sMax1, INT32_MIN, INT32_MAX);
  bay.PrintPushedBits();
  bay.ResetForRead();
  // unsigned reading checks
  //bay.Serialize(uuMinCheck, 0, UINT16_MAX);
  //bay.Serialize(uuMaxCheck, 0, UINT16_MAX);
  //bay.Serialize(uMinCheck, 0, UINT32_MAX);
  //bay.Serialize(uMaxCheck, 0, UINT32_MAX);
  //bay.Serialize(MaxCheck, 0, UINT32_MAX);
  //// 16 reading checks
  //bay.Serialize(ssMinCheck, INT16_MIN, INT16_MAX);
  //bay.Serialize(ssMaxCheck, INT16_MIN, INT16_MAX);
  //bay.Serialize(ssMin1Check, INT16_MIN, INT16_MAX);
  //bay.Serialize(ssMax1Check, INT16_MIN, INT16_MAX);
  // 32 reading checks
  bay.Serialize(sMinCheck, INT32_MIN, INT32_MAX);
  bay.Serialize(sMaxCheck, INT32_MIN, INT32_MAX);
  bay.Serialize(sMin1Check, INT32_MIN, INT32_MAX);
  bay.Serialize(sMax1Check, INT32_MIN, INT32_MAX);

  //if (ssMin != ssMinCheck || ssMax != ssMaxCheck ||
  //  ssMin1 != ssMin1Check || ssMax1 != ssMax1Check)
  //  assert(false);
  if (sMin != sMinCheck || sMax != sMaxCheck ||
    sMin1 != sMin1Check || sMax1 != sMax1Check)
    assert(false);
  //if (uuMin != uuMinCheck || uuMax != uuMaxCheck ||
  //  uMin != uMinCheck || uMax != uMaxCheck ||
  //  Max != MaxCheck)
    assert(false);
  bay.PrintPushedBits();


  // stress test
  struct ForChecks
  {
    int min_;
    int max_;
    int val_;
    ForChecks(int val, int imax, int imin) : min_(imin), max_(imax), val_(val) {};
  };
  std::srand(static_cast<unsigned>(std::time(0)));
  // random number with random range reading and writing check
  int stress = std::rand() % 2048;
  bay.ResetForWrite();
  std::vector<ForChecks> nums, checks;
  nums.reserve(stress);
  for (int i = 0; i < stress; ++i)
  {
    int negative = (std::rand() % 2) == 0 ? 1 : -1;
    int imin = (std::rand() % INT32_MAX) * negative;
    int imax = std::rand() % INT32_MAX;
    if (imax < imin)
    {
      int temp = imin;
      imin = imax;
      imax = temp;
    }
    int val = (std::rand() % (imax - imin)) + imin;
    nums.push_back(ForChecks(val, imax, imin));
    bay.Serialize(val, imin, imax);
  }
  //bay.PrintPushedBits();
  bay.ResetForRead();
  for (int i = 0; i < stress; ++i)
  {
    int testVal = 0;
    bay.Serialize(testVal, nums[i].min_, nums[i].max_);
    assert(testVal == nums[i].val_);
  }

  // random tests
  float f = 1.23456f;
  int val = 3;
  bool b = true;
  unsigned char  c = 2;
  unsigned int  i = 11;
  unsigned int  i2 = 100;
  unsigned char  c2 = 3;
  bool bcheck;
  unsigned char ccheck, ccheck2;
  unsigned int ucheck, ucheck2;
  float fcheck;
  bay.ResetForWrite();
  bay.Serialize(f, 0, 100.0f, 10);
  bay.Serialize(b);
  bay.Serialize(c, 0, 2);
  bay.Serialize(i, 0, 11);
  bay.Serialize(i2, 0, 100);
  bay.Serialize(c2, 0, 3);
  bay.PrintPushedBits();
  bay.ResetForRead();
  bay.Serialize(fcheck, 0, 100.0f, 10);
  bay.Serialize(bcheck);
  bay.Serialize(ccheck, 0, 2);
  bay.Serialize(ucheck, 0, 11);
  bay.Serialize(ucheck2, 0, 100);
  bay.Serialize(ccheck2, 0, 3);
  int bp2 = bay.GetNumberOfBitsPushed();

  // Float Checks
  bay.ResetForWrite();
  bay.Serialize(f, 0, 2.0f, 10);
  bay.ResetForRead();
  bay.Serialize(fcheck, 0, 2.0f, 10);
  bay.ResetForWrite();
  bay.Serialize(f, 0, 1000.0f, 20);
  bay.Serialize(f, 0, 1000.0f, 21);
  bay.ResetForRead();
  bay.Serialize(fcheck, 0, 1000.0f, 20);
  bay.Serialize(fcheck, 0, 1000.0f, 21);







  // testing bitset
  //int v = 0x12345678;
  //int test = 7;
  //std::cout << std::bitset<32>(test) << std::endl;
  //unsigned bits = BitArray::BitsNeeded(test);
  //std::cout << bits << std::endl;

  return 0;
}
