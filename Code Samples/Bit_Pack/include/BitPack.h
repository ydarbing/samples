

static const int MAX_BYTES = 4096;//2048;
// packs bits with no padding
class BitArray
{
public:
  friend class Encoder;
  BitArray(bool read);
  bool IsReading(void)const;
  void ResetForRead(void);
  void ResetForWrite(void);

  const int GetNumberOfBytesPushed(void)const;
  const int GetNumberOfBitsPushed(void)const;

  void Serialize(bool& b);
  void Serialize(unsigned char& val, int imin, int imax);
  void Serialize(unsigned short &val, unsigned short imin = 0, unsigned short imax = 0xFFFF);
  void Serialize(unsigned int &val, unsigned int imin = 0, unsigned int imax = 0xFFFFFFFF);
  void Serialize(unsigned long &val, unsigned long imin = 0, unsigned long imax = 0xFFFFFFFF);

  void Serialize(short &val, short imin, short imax);
  void Serialize(int &val, int imin, int imax);
  void Serialize(float& val, float fmin, float fmax, int totalNumBits);

  // Anytime appendData is used, be sure previous data is byte aligned
  void AppendData(const unsigned char* data, int numBytes);
  void AppendData(const char* data, int numBytes);

  template <class T> void PrintBits(T val);
  void PrintPushedBits(void);
  //static unsigned int BitsNeeded(unsigned int range);
  static int BitsNeeded(unsigned long long range);

  static int UnitTest(void);

private:
  template <class T> void TPlateSerializeInt(T* val, T imin, T imax);
  void SerializeInt(long* v, long min, long max);
  void SerializeUInt(unsigned int* v, unsigned int min, unsigned int max);
  void SerializeULInt(unsigned long* v, unsigned long min, unsigned long max);
  void SerializeFloat(float* val, float fmin, float fmax, int totalNumBits = 20);

  void PushBit(int bit);
  int  PopFrontBit(void);
  void ReadBits(unsigned char* pOut, int numBits);
  void WriteBits(const unsigned char* pIn, int numBits);

  //unsigned char* GetData(void)const;

private:
  int m_bitPos;     // current bit
  int m_bytePos;    // current byte
  int m_numBytes;   // total number of bytes packed
  bool m_reading;
  unsigned char m_readByte;   // the current byte being read (used only when reading)
  unsigned char m_data[MAX_BYTES];
};

// templated functions here
#include "BitPack.hpp"