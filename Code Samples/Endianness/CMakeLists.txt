cmake_minimum_required(VERSION 3.17)

project("Endianness")

set(source_files
  src/CheckEndianness.cpp
)

ADD_EXECUTABLE("${PROJECT_NAME}" 
               ${source_files}
               )

#target_include_directories("${PROJECT_NAME}" PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")
