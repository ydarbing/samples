/*
  Bounce.cpp
  Brady Reuter
  4/3/2012
*/
#include <stdio.h>     // printf         
#include <windows.h>   // Windows stuff  
#include "DotWindow.h"
#include "Compute.h"  

#ifdef _MSC_VER
#pragma warning(disable: 4996) 
#endif

typedef struct ThreadStuff
{
  HANDLE mutex;  /* mutex shared by all threads          */
  Compute* ball; /* used to get the position of the ball */
  double x, y;   /* the postion of the ball              */
  
}ThreadStuff;

/*********************************************************************/
 /*!
   \brief
      uses a mutex to cleanly change data with multithreading
   
   \param p
      void* that contains the sturct information
   \return
     returns an unsigned long (DWORD)
 */
 /***********************************************************************/
DWORD WINAPI Bounce(LPVOID p)
{
  /* reading in the paramaters */
  ThreadStuff *params = (ThreadStuff *)p;
  DWORD result;
  char string[50]; /* used to convert tid into string*/
  HANDLE mutex = params->mutex;
  int tid = GetCurrentThreadId();
  /* place each window in a different spot on screen */
  int screen_width = GetSystemMetrics(SM_CXSCREEN);
  int screen_height = GetSystemMetrics(SM_CYSCREEN);
  int left = tid % (screen_width - screen_width / 4);
  int right = tid % (screen_height - screen_height / 4);
 
  /* create the window and name it the thread id*/
  sprintf(string,"ID: %i", tid);
  DotWindow win(string, left, right);
  
  while(!win.IsClosed())
  {
    result = WaitForSingleObject(mutex, 0L);
    
    switch(result) 
    { 
      /* Acquired the mutex */
      case WAIT_OBJECT_0:           
        /* get the new position of the dot */
        params->ball->operator()(params->x, params->y);
        ReleaseMutex(mutex); /* move to the next open thread */
        break;
        
      /* The mutex wasn't available yet */
      case WAIT_TIMEOUT: 
        break; 
    }
    /* set the new position of the dot*/
    win.SetDotPosition((int)params->x, (int)params->y);
  }
  return TRUE;
}

/*********************************************************************/
 /*!
   \brief
      Creates new threads for every new window
   
   \param argc
     the number of arguments given to the program
   \param argv
     the actual arguments supplied to the program
   \return
     returns 0 if successful
 */
 /***********************************************************************/
int main(int argc, char **argv) 
{
  int i, num_windows;  /* counter and number of windows user requested*/
  HANDLE *threads;     /*  array of threads */
  DWORD ThreadID;      /*  used so you can get the thread id*/
  ThreadStuff *params; /* pointer to the struct containing stuff for threads*/
  HANDLE mutex;        /* used so no memory is over written */
  
  if (argc < 2)
  {
    printf("Usage: bounce {num_threads}\n");
    return -1;
  }
  
  /* convert the argument into an int */
  num_windows = atoi(argv[1]);
  
  /* allocate memory for all of the threads and params */
  threads = (HANDLE*)malloc(num_windows * sizeof(HANDLE));
  params = (ThreadStuff*)malloc(num_windows * sizeof(ThreadStuff));
  
  /* create the mutex */
  mutex = CreateMutex(NULL, FALSE, NULL);
  
  if(!mutex)
  { 
    printf("Error creating mutex: %d\n", GetLastError());
    return -1;
  }
  
  /* create the comp object */
   Compute ball;
  
  /* create the threads */
  for(i = 0; i < num_windows; ++i)
  {
    /* initialize the struct */
    params[i].mutex = mutex;
    params[i].ball = &ball;
    
    threads[i] = CreateThread(NULL, 0, Bounce, &params[i], 0, &ThreadID); 
    
    if(threads[i] == NULL)
    {
      printf("Error creating thread: %d\n", GetLastError());
      return -1;
    }
  }
  
   /* Wait for each thread */
  WaitForMultipleObjects(num_windows, threads, TRUE, INFINITE);
  
  /* Release thread handles */
  for(i = 0; i < num_windows; ++i)
    CloseHandle(threads[i]);
  
    /* Release mutex */
  CloseHandle(mutex);
  
    /* Clean up memory */
  free(threads);
  free(params);
  
  return 0;
}
