// DotWindow.cpp
// -- implementation of "dot window" class
// 2012

#include <cmath>
#include "DotWindow.h"

const char* DotWindow::class_name = "DotWindow Class";

DotWindow::DotWindow(const char *title, int left, int top) : closed(FALSE) 
{
		// The size of the screen
  int screen_width = GetSystemMetrics(SM_CXSCREEN);
  int screen_height = GetSystemMetrics(SM_CYSCREEN);
  
  	// Make the window size a portion of the screen size
  int window_width = screen_width/5;
  int window_height = screen_height/5;
  
  	// Size of the dot    
  dot_radius = int(std::sqrt(0.01 * screen_width * screen_height));
  	
  	// Color/pattern of the dot
  dot_brush = CreateSolidBrush(RGB(0, 0, 255));
  
  	// Device context for entire screen
  HDC dc = GetDC(0);
  
  	// In-memory device context
  buffer_dc = CreateCompatibleDC(dc);
  
  	// Draw in this bitmap of window size
  back_buffer = CreateCompatibleBitmap(dc, window_width, window_height);
  
  	// Done with device context
  ReleaseDC(0, dc);
  
  	// Assign bitmap to device context
  SelectObject(buffer_dc, back_buffer);
  
  	// Assign brush to device context
  SelectObject(buffer_dc, dot_brush);
  
  	// This rectangle is the size of the window
  RECT rect = {0, 0, window_width, window_height};
  
  	// Paint the entire window white
  FillRect(buffer_dc, &rect, HBRUSH(GetStockObject(WHITE_BRUSH)));

		// Define attributes of the window
  WNDCLASS wc;
  wc.style = 0;
  wc.lpfnWndProc = WinProc;
  wc.cbClsExtra = 0;
  wc.cbWndExtra = sizeof(DotWindow*);
  wc.hInstance = 0;
  wc.hCursor = LoadCursor(0, IDC_CROSS);
  wc.hIcon = LoadIcon(0, IDI_HAND);
  wc.hbrBackground = 0;
  wc.lpszMenuName = 0;
  wc.lpszClassName = class_name;
  RegisterClass(&wc);
  window = CreateWindow(class_name, title,
                        WS_VSCROLL | WS_CAPTION | WS_SYSMENU,
                        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
                        0, 0, 0, 0);
  SetWindowLongPtr(window, 0, reinterpret_cast<LONG_PTR>(this));
  
  	// Adjust the window
  POINT point = {0, 0};
  ClientToScreen(window, &point);
  rect.left = point.x;
  rect.top = point.y;
  rect.right = point.x + window_width;
  rect.bottom = point.y + window_height;
  AdjustWindowRect(&rect, WS_BORDER | WS_CAPTION | WS_SYSMENU, FALSE);
  //MoveWindow(window, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, FALSE);
  
  int w = rect.right - rect.left;
  int h = rect.bottom - rect.top;
  
  MoveWindow(window, left, top, w, h, FALSE);
  ShowWindow(window, SW_SHOW);
}

DotWindow::~DotWindow(void) 
{
  if (!closed)
    DestroyWindow(window);
    
  DeleteDC(buffer_dc);
  DeleteObject(back_buffer);
  DeleteObject(dot_brush);
  UnregisterClass(class_name, 0);
}

void DotWindow::SetDotPosition(int x, int y) 
{
  MSG msg;
  if (PeekMessage(&msg, window, 0, 0, PM_REMOVE))
    DispatchMessage(&msg);
  if (closed)
    return;
    
    // Draw the dot based on its x,y position
  RECT rect;
  GetClientRect(window, &rect);
  FillRect(buffer_dc, &rect, HBRUSH(GetStockObject(WHITE_BRUSH)));
  POINT point = {x, y};
  ScreenToClient(window, &point);
  Ellipse(buffer_dc, point.x - dot_radius, point.y - dot_radius, point.x + dot_radius, point.y + dot_radius);
  HDC dc = GetDC(window);
  BitBlt(dc, 0, 0, rect.right, rect.bottom, buffer_dc, 0, 0, SRCCOPY);
  ReleaseDC(window, dc);
}

bool DotWindow::IsClosed(void) 
{
  return closed;
}

// Window proc
LRESULT CALLBACK DotWindow::WinProc(HWND win, UINT msg, WPARAM wp, LPARAM lp) 
{
  switch (msg) 
  {

    case WM_PAINT: 
    {
      DotWindow *dw = reinterpret_cast<DotWindow*>(GetWindowLongPtr(win, 0));
      PAINTSTRUCT ps;
      HDC dc = BeginPaint(win, &ps);
      BitBlt(dc, ps.rcPaint.left, ps.rcPaint.top,
             ps.rcPaint.right - ps.rcPaint.left,
	           ps.rcPaint.bottom - ps.rcPaint.top,
	           dw->buffer_dc, ps.rcPaint.left, ps.rcPaint.top, SRCCOPY);
      EndPaint(win, &ps);
      return 0; 
    }

    case WM_DESTROY:
      DotWindow *dw = reinterpret_cast<DotWindow*>(GetWindowLongPtr(win, 0));
      dw->closed = TRUE;
      PostQuitMessage(0);
      return 0;
  }

  return DefWindowProc(win, msg, wp, lp);
}
