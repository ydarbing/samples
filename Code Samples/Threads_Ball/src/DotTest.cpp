//cl /W4 /EHsc first.cpp DotWindow.cpp Compute.cpp user32.lib gdi32.lib

#include "DotWindow.h"

int DotMain(void) 
{
  DotWindow win("Bob", 0, 0);
  while (!win.IsClosed()) 
  {
    for (int i = 100; i < 300; i++)
    {
      win.SetDotPosition(i, i);
      Sleep(50);
      if (win.IsClosed())
        break;
    }
  }
  return 0;
}
