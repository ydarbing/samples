// Compute.cpp
// -- implementation of dot position updater
// 2012

#include <windows.h>
#include <cmath>
#include "Compute.h"

static int RandomInt(int low, int high)
{
  return rand() % (high - low + 1) + low;
}

double drand(void) 
{
  return double(rand())/double(RAND_MAX);
}

int Compute::stcount = 0;
	
Compute::Compute(void) 
{
  current_time = GetTickCount();
  double speed = .333 * GetSystemMetrics(SM_CXSCREEN);
  // .2 to 1.3 for nicer angles
  double angle = RandomInt(2, 13) / 10.0;
  velocity_x = speed * std::cos(angle);
  velocity_y = speed * std::sin(angle);
  int screen_width = GetSystemMetrics(SM_CXSCREEN);
  int screen_height =  GetSystemMetrics(SM_CYSCREEN);
  position_x = screen_width * drand();
  position_y =screen_height * drand();
  offset = int(std::sqrt(0.01 * screen_width * screen_height));
  counter = 0;
  stcount++;
  if (stcount > 1)
 		abort(); 	
}

void Compute::operator()(double& x, double& y) 
{
	unsigned oldcounter = counter++;
  int time = GetTickCount();
  double dt = 0.001 * (time - current_time);
  current_time = time;
  
  	// Calculate new position and check for reflection
  	// off of the sides
  position_x += velocity_x * dt;
  double xmax = GetSystemMetrics(SM_CXSCREEN) - offset;
  if (position_x < offset) 
  {
    position_x += 2 * (offset - position_x);
    velocity_x = -velocity_x;
  }
  if (position_x > xmax) 
  {
    position_x -= 2 * (position_x - xmax);
    velocity_x = -velocity_x;
  }
  position_y += velocity_y * dt;
  double ymax = GetSystemMetrics(SM_CYSCREEN) - offset;
  if (position_y < offset) 
  {
    position_y += 2 * (offset-position_y);
    velocity_y = -velocity_y;
  }
  if (position_y > ymax) 
  {
    position_y -= 2 * (position_y - ymax);
    velocity_y = -velocity_y;
  }
  
  	// Send the new coordinates back to the client
  x = position_x;
  y = position_y;
  
  if (oldcounter != counter - 1)
  	abort();
}
