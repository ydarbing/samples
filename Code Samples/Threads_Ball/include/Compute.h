// Compute.h
// -- interface for dot position updater
// 2012
#pragma once

class Compute 
{
  public:
    Compute(void);
    void operator()(double& x, double& y);
    static int stcount;
  private:
  	Compute(const Compute&);
    int current_time;
    double offset;
    double position_x, position_y;
    double velocity_x, velocity_y;
    unsigned counter;
};

