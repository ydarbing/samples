// DotWindow.h
// -- interface for the "dot window"
// 2012

#pragma once
#include <windows.h>

class DotWindow 
{
  public:
    DotWindow(const char* title, int left, int top);
    ~DotWindow(void);
    void SetDotPosition(int x, int y);
    bool IsClosed(void);
  private:
    static const char *class_name;
    int dot_radius;
    bool closed;
    HWND window;
    HBITMAP back_buffer;
    HDC buffer_dc;
    HBRUSH dot_brush;
    static LRESULT CALLBACK WinProc(HWND, UINT, WPARAM, LPARAM);
};

