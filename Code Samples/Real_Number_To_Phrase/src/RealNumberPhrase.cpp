// Brady Reuter
#include "RealNumberPhrase.h"
#include <vector>
#include <iostream> // std::cout
#include <algorithm>// std::remove

int StringToInt(char asciiNumber);
std::string PhraseLastTwoNumbers(const char* number, const size_t size);
std::string GetNumberPhrase(std::string &inNumber);
bool CleanUpNumberString(std::string& inNumber, std::string& ret);
std::string GetFractionNumberPhrase(std::string &inNumber);

// used to convert to an index into an array
int StringToInt(char asciiNumber)
{
  return asciiNumber - '0';
}

// will be limited to a size of 2
std::string PhraseLastTwoNumbers(const char* number, const size_t size)
{
  if (size == 1)
  {
    return OnesPlace[StringToInt(number[0])];
  }
  // length == 2
  int tens = StringToInt(number[0]);
  int ones = StringToInt(number[1]);
  if (tens == 0)
  {
    if (ones)
      return OnesPlace[ones];
    else
      return "";
  }
  else if (tens == 1)
  {
    return TensPlaceSpecial[ones];
  }
  else
  {
    if (ones == 0)
      return TensPlace[tens];
    else
      return TensPlace[tens] + "-" + OnesPlace[ones];
  }
}


// takes away all commas and determines if the number is negative
// returns false if the only number is 0
bool CleanUpNumberString(std::string& inNumber, std::string& ret)
{
  // remove commas if there are any since commas are only for human readability 
  inNumber.erase(std::remove(inNumber.begin(), inNumber.end(), ','), inNumber.end());
  // find the first number
  std::size_t pos = inNumber.find_first_of("0123456789");
  if (pos == std::string::npos)
  {
    return false;
  }
  else if (pos != 0)
  {
    if (inNumber[pos - 1] == '-')
    {
      ret += "negative ";
    }
    else if (inNumber[pos - 1] == '.')
    {
      return true;
    }
    inNumber = inNumber.substr(pos);
  }

  return true;
}

std::string GetFractionPhrase(std::string& fraction)
{
  int fractionPlace = static_cast<int>(floor(fraction.length() / 3.0));
  if (fractionPlace >= MAX_NUMBER_LENGTH)
  {
    return "fraction too long for program";
  }

  int fractionPlaceSub = (fraction.length() % 3);
  std::string fractionHelper = "";
  if (fractionPlaceSub)
  {
    fractionHelper = FractionHelper[fractionPlaceSub - 1];
  }

  size_t length = fraction.length();
  switch (length)
  {
  case 1:
    return PhraseLastTwoNumbers(fraction.c_str(), length) + " tenths";
  case 2:
  {  // special case such as 2.00
    std::string ret = PhraseLastTwoNumbers(fraction.c_str(), length);
    if (ret.empty())
      return OnesPlace[0];
    else
      return ret + " hundredths";
  }
  default:
    return GetNumberPhrase(fraction) + fractionHelper + ThousandsPlace[fractionPlace] + "ths";
  }
}

std::string RealNumberToReadableString(std::string inNumber)
{
  std::string integerPhrase = "";
  if (CleanUpNumberString(inNumber, integerPhrase) == false)
    return OnesPlace[0]; // return "zero"

  size_t length = inNumber.length();
  std::string fractionPhrase = "";
  size_t fractionLength = 0;
  bool hasFraction = false;
  // start at end looking for decimal point
  std::size_t decimalPos = inNumber.find_last_of('.');
  // check if it's something like "20."
  if (decimalPos == length - 1)
  {
    fractionLength = 1;
  }
  else if (decimalPos != std::string::npos)
  {
    hasFraction = true;
    // get just fraction number (not including decimal point)
    std::string fraction = inNumber.substr(decimalPos + 1);
    // + 1 to account for the decimal point
    fractionLength = fraction.length() + 1;
    fractionPhrase = GetFractionPhrase(fraction);
    // zero is the only number that contains a 'z'
    if (fractionPhrase.find('z') != std::string::npos)
      hasFraction = false;
  }
  // get just the integer part if there was a fraction
  if (fractionLength)
  {
    inNumber = inNumber.substr(0, length - fractionLength);
  }
  // go through the rest of the number 
  integerPhrase = GetNumberPhrase(inNumber);
  // if there is an extra comma take it out
  std::size_t lastComma = integerPhrase.find_last_of(',');

  if (lastComma >= integerPhrase.length() - 2 && lastComma != std::string::npos)
  {
    // take out the comma
    for (size_t i = lastComma - 1; i < integerPhrase.size(); ++i)
    {
      integerPhrase.pop_back();
    }
    integerPhrase.push_back(' ');// add space back in just in case
  }
  // zero is the only number phrase that contains a 'z' so only need to look for 'z'
  bool hasInteger = integerPhrase.find('z') == std::string::npos;

  if (hasInteger)
  {
    if (hasFraction)
    {
      return integerPhrase + "and " + fractionPhrase;
    }
    else
    {
      return integerPhrase;
    }
  }
  else // no integer
  {
    if (hasFraction)
    {
      return fractionPhrase;
    }
    else
    {
      return OnesPlace[0]; // just return 0 because no other numbers were found
    }
  }
}

std::string GetNumberPhrase(std::string &inNumber)
{
  std::string ret;
  std::size_t pos = inNumber.find_first_of("123456789");
  // quick out to see if there are any actual numbers
  if (pos == std::string::npos)
  {
    return OnesPlace[0];
  }
  // check if there were leading zeros
  if (pos != 0)
  {
    inNumber = inNumber.substr(pos);
  }
  // index of inNumber we are on
  int numProgress = 0;
  size_t length = inNumber.length();
  // are we dealing with millions, billions, ect..
  int thousands = static_cast<int>(ceil(length / 3.0)) - 1;

  if (thousands >= MAX_NUMBER_LENGTH)
  {
    return "Integer too large for program";
  }
  // before we get into the while loop take care of occurrences 
  // like 10,000 which does not start with a beginning hundreds digit
  int noFirstHundreds = length % 3;

  if (noFirstHundreds != 0)
  {
    //ret += PhraseLastTwoNumbers(inNumber.substr(0, noFirstHundreds)) + " ";
    ret += PhraseLastTwoNumbers(inNumber.c_str(), noFirstHundreds) + " ";
    if (thousands > 0)
    {
      ret += ThousandsPlace[thousands] + ", ";
      --thousands;
    }
    numProgress += noFirstHundreds;
  }

  // deal with hundred number then last two
  while (numProgress < length)
  {
    bool addDefaultHundredsLabel = false;
    int hundredNum = StringToInt(inNumber[numProgress]);
    if (hundredNum != 0)
    {
      ret += OnesPlace[hundredNum] + " hundred ";
      addDefaultHundredsLabel = true;
    }
    // hundred number taken care of
    ++numProgress;

    std::string shouldAdd = PhraseLastTwoNumbers(inNumber.c_str() + numProgress, 2);

    if (!shouldAdd.empty())
    {
      ret += shouldAdd + " ";
      addDefaultHundredsLabel = true;
    }
    // as long as there was a non-zero number in this group of 3 
    // we want to add the thousands label
    if (addDefaultHundredsLabel && thousands > 0)
    {
      ret += ThousandsPlace[thousands] + ", ";
    }
    // tens place and ones place taken care of
    numProgress += 2;
    --thousands;
  }

  return ret;
}

void Test(std::string testNumber)
{
  std::cout << testNumber << ": " << RealNumberToReadableString(testNumber) << std::endl;
}

void Test(std::vector<std::string> testNumbers)
{
  for (unsigned i = 0; i < testNumbers.size(); ++i)
  {
    Test(testNumbers[i]);
  }
}


int main(int argc, char ** argv)
{
  if (argc == 2)
  {
    // if user put in number it is assumed to be 'valid' so no checking is done
    Test(argv[1]);
  }
  else // run pre generated numbers
  {
    const char* numbers[] = {
      //////////////////////////////////////////////////////////////////////////
      //                       REAL NUMBERS
      //////////////////////////////////////////////////////////////////////////
      "200.0", "200.00", "200.000",
      "200.0000", "200.00000", "200.000000",
      "1,729,405.008365",
      "0.400",  // four hundred thousandths
      "0.00004",// four hundred-thousandths
      "0.33",
      ".33",
      "1234.33658",
      "203.",
      "200.02",
      "200.000",
      //////////////////////////////////////////////////////////////////////////
      //                       INTEGER NUMBERS
      //////////////////////////////////////////////////////////////////////////
      "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
      "10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
      "20", "21", "22", "23",
      "30", "33",
      "40", "43",
      "50", "53",
      "60", "63",
      "70", "73",
      "80", "83",
      "90", "93",
      "222,222",
      "987,654,321",
      "6,880,612,905",
      "100",
      "1000",
      "10000",
      "100000",
      "1000000",
      "10000000",
      "100000000",
      "1000000000",
      "10000000000",
      "100000000000",
      "1000000000000",
      "10000000000000",
      "100000000000000",
      "1000000000000000",
      "10000000000000000",
      "100000000000000000",
      "1000000000000000000",
      "10000000000000000000",
      "100000000000000000000",
      "1000000000000000000000",
      "10000000000000000000000",
      "100000000000000000000000",
      "1000000000000000000000000",
      "10000000000000000000000000",
      "100000000000000000000000000",
      "1000000000000000000000000000",
      "10000000000000000000000000000",
      "100000000000000000000000000000",
      "1000000000000000000000000000000",
      "10000000000000000000000000000000",
      "100000000000000000000000000000000",
      "1000000000000000000000000000000000",
      "10000000000000000000000000000000000",
      "100000000000000000000000000000000000",
      "1000000000000000000000000000000000000",
      "10000000000000000000000000000000000000",
      "100000000000000000000000000000000000000",
      "1000000000000000000000000000000000000000",
      "10000000000000000000000000000000000000000",
      "100000000000000000000000000000000000000000",
      "1000000000000000000000000000000000000000000",
      //////////////////////////////////////////////////////////////////////////
      //           gotcha's
      //////////////////////////////////////////////////////////////////////////
      "0.",
      "0.00",
      "0.10001",
      "0010.01"
    };

    std::vector<std::string> testStrings(numbers, std::end(numbers));
    Test(testStrings);
  }
  return 0;
}
