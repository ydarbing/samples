cmake_minimum_required(VERSION 3.17)

project("Data_Structure__Hash")

set(header_files
include/ObjectAllocator.h
include/support.h
include/Chained_Hash_Table/ChHashTable.h
include/Chained_Hash_Table/ChHashTable.hpp
)

set(source_files
  src/main.cpp 
  src/ObjectAllocator.cpp 
  src/support.cpp 
  )

ADD_EXECUTABLE("${PROJECT_NAME}" 
               ${source_files} 
               ${header_files}
               )


target_include_directories("${PROJECT_NAME}" PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")
