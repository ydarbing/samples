/*
  ChHashTable.hpp
  Brady Reuter
  3/14/2015
   
  Creates a chained based hash table and resizes based on users given 
  max load factor
*/ 
#include <cmath>// std::ceil, for growing table

/*********************************************************************/
 /*!
   \brief
     Constructor 
   \param allocator
     the allocator that will be used to create the table
   \param Config 
     all data needed to make table
 */
 /***********************************************************************/
template <typename T>
ChHashTable<T>::ChHashTable(const HTConfig& Config, ObjectAllocator* allocator /*= 0*/) 
  : m_allocator(allocator), m_table(0), m_stats(), m_growthFactor(Config.GrowthFactor_), 
    m_maxLoadFactor(Config.MaxLoadFactor_), m_freeFunct(Config.FreeProc_)
{
  m_stats.HashFunc_ = Config.HashFunc_;
  m_stats.TableSize_ = Config.InitialTableSize_;
  // make the initial table
  m_table = ResizeTable(m_stats.TableSize_);
  
}
/*********************************************************************/
 /*!
   \brief
     Destructor, clears and deletes whole table
 */
 /***********************************************************************/
template <typename T>
ChHashTable<T>::~ChHashTable()
{
  ClearTable(m_table, m_stats.TableSize_);
  m_stats.Count_ = 0;
  delete[] m_table;
}


/*********************************************************************/
 /*!
   \brief
     Inserts an element into the hash table, hashes key to find insertion point
   \param key
     string used to hash into table
   \param data 
     Data that is being inserted
   \exception HashTableException
     Throws if a duplicate data was inserted or if there is no memory left
 */
 /***********************************************************************/
template <typename T>
void ChHashTable<T>::insert(const char* key, const T& data) throw(HashTableException)
{
  ++m_stats.Probes_;
  if(((double)(m_stats.Count_ + 1) / (double)m_stats.TableSize_) > m_maxLoadFactor) 
    GrowTable();

  unsigned hash = m_stats.HashFunc_(key, m_stats.TableSize_);
  // check if this hash location is available
  if(m_table[hash].Nodes)
  {
    ChHTNode* node = m_table[hash].Nodes;
    // loop through all nodes checking for duplicate
    while(node)
    {
      if(CheckKey(node, key))
        throw HashTableException(HashTableException::E_DUPLICATE, "Tried to insert duplicate");
      ++m_stats.Probes_;
      node = node->Next;
    }
    // no duplicate, put node in front of list
    ChHTNode* newNode = MakeNode(key, data);
    newNode->Next = m_table[hash].Nodes;
    m_table[hash].Nodes = newNode;
  }
  else
  {
    m_table[hash].Nodes = MakeNode(key, data);
  }
  ++m_stats.Count_;
  ++m_table[hash].Count;
}

/*********************************************************************/
 /*!
   \brief
     Removes an element out of hash table
   \param key
     string used to hash into table, to find correct element to delete
   \exception HashTableException
     Throws if a item was not found
 */
 /***********************************************************************/
template <typename T>
void ChHashTable<T>::remove(const char* key) throw(HashTableException)
{
  ++m_stats.Probes_;
  unsigned hash = m_stats.HashFunc_(key, m_stats.TableSize_);
  // check if this hash location is available
  ChHTNode* node = m_table[hash].Nodes;
  ChHTNode* prevNode = node;

  while(node)
  {
    if(CheckKey(node, key))
      break;
    ++m_stats.Probes_;
    prevNode = node;
    node = node->Next;
  }

  if(node == 0)
    throw HashTableException(HashTableException::E_ITEM_NOT_FOUND, "Tried to remove item not in table");
  else
  {
    if(prevNode == node)//removing first link
      m_table[hash].Nodes = node->Next;
    else
      prevNode->Next = node->Next;
    // free memory and update stats
    if(m_freeFunct)
      m_freeFunct(node->Data);

    DeleteNode(node);
    --m_stats.Count_;
  }
}

/*********************************************************************/
 /*!
   \brief
     Inserts an element into the hash table, hashes key to find insertion point
   \param key
     string used to hash into table
   \return 
     The Data that was found at given key
   \exception HashTableException
     Throws if item was not found in table
 */
 /***********************************************************************/
template <typename T>
const T& ChHashTable<T>::find(const char* key) const throw(HashTableException)
{
  ++m_stats.Probes_;
  unsigned hash = m_stats.HashFunc_(key, m_stats.TableSize_);
  ChHTNode* node = m_table[hash].Nodes;

  while(node)
  {
    if(CheckKey(node, key))
      break;
    ++m_stats.Probes_;
    node = node->Next;
  }

  if(!node)
    throw HashTableException(HashTableException::E_ITEM_NOT_FOUND, "Tried to find item not in table");

  return node->Data;
}

/*********************************************************************/
 /*!
   \brief
     Removes all Data from table
 */
 /***********************************************************************/
template <typename T>
void ChHashTable<T>::clear(void)
{
  // does not deallocate actual table, just empties it
  ClearTable(m_table, m_stats.TableSize_);
  m_stats.Count_ = 0;
}


/*********************************************************************/
 /*!
   \brief
     Gets the current stats of the hash table
   \return
     stats
 */
 /***********************************************************************/
template <typename T>
HTStats ChHashTable<T>::GetStats(void) const
{
  return m_stats;
}

/*********************************************************************/
 /*!
   \brief
     Allows client to access table
   \return 
     const pointer to table
 */
 /***********************************************************************/
template <typename T>
const typename ChHashTable<T>::ChHTHeadNode* ChHashTable<T>::GetTable(void) const
{
  return m_table;
}


// PRIVATE FUNCTIONS
/*********************************************************************/
 /*!
   \brief
     Allocates nodes that contain data
   \param key
     How this node will be hashed into the table
   \param data
     What is actually stored in the node
   \return
     the newly made node
   \exception HashTableException
     Throws if there is no memory left
 */
 /***********************************************************************/
template <typename T>
typename ChHashTable<T>::ChHTNode* ChHashTable<T>::MakeNode(const char* key, const T& data)throw(HashTableException)
{
   ChHTNode* newNode = 0;
   try
   {
     if(m_allocator)
     {
       ChHTNode* memLocation = reinterpret_cast<ChHTNode*>(m_allocator->Allocate());
       newNode = new(memLocation) ChHTNode(data);
     }
     else
       newNode = new ChHTNode(data);
     
     int i = -1;
     while(key[++i])
       newNode->Key[i] = key[i];
     while(static_cast<unsigned>(i) < MAX_KEYLEN)// make it a null terminated string
       newNode->Key[i++] = 0;

     newNode->Next = 0;
   }
   catch(const OAException& e)
   { throw (HashTableException(HashTableException::E_NO_MEMORY, e.what())); }
   catch(std::bad_alloc&)
   { throw HashTableException(HashTableException::E_NO_MEMORY, "Allocate : Not enough system memory available."); }

  return newNode;
}


/*********************************************************************/
 /*!
   \brief
     Checks to see if a given node has the same key that was given
   \param node
     The current node that is going to be checked with the given key
   \param key
     String used to hash into table, to find correct element to delete
   \return 
     true if the key matches given node
 */
 /***********************************************************************/
template <typename T>
bool ChHashTable<T>::CheckKey(ChHTNode* node, const char* key) const
{
  unsigned i = 0;

  while(node->Key[i] && key[i])
  {
    if(node->Key[i] != key[i])
      return false;
    ++i;
  }

  if(node->Key[i] == 0 && key[i] == 0)
    return true;
  else // different lengths
    return false;
}


/*********************************************************************/
 /*!
   \brief
     Where table is allocated
   \param newSize
     Reallocates a table with this size
   \return
     The new head of the table
   \exception HashTableException
     Throws if there is no memory left
 */
 /***********************************************************************/
template <typename T>
typename ChHashTable<T>::ChHTHeadNode* ChHashTable<T>::ResizeTable(unsigned newSize) throw(HashTableException)
{
  ChHTHeadNode* newTable = 0;
  try
  {
    newTable = new ChHTHeadNode[newSize];
  }
  catch(std::bad_alloc&)
  { throw HashTableException(HashTableException::E_NO_MEMORY, "Allocate : Not enough system memory available."); }

  return newTable;
}

/*********************************************************************/
 /*!
   \brief
     Removes an element out of hash table
   \param node
     Node in table to be deleted
 */
 /***********************************************************************/
template <typename T>
void ChHashTable<T>::DeleteNode(ChHTNode* node)
{
  if(m_allocator)
  {
    // have to call destructor explicitly because we used placement new
    node->~ChHTNode();
    m_allocator->Free(node);
  }
  else
    delete node;

}

/*********************************************************************/
 /*!
   \brief
     Takes out all nodes in table but will not delete the base table
   \param tbl
     The hash table that will be cleared of all data
   \param tSize
     The size of the given table
 */
 /***********************************************************************/
template <typename T>
void ChHashTable<T>::ClearTable(ChHTHeadNode* tbl, unsigned tSize)
{
  // go through whole table deleting all nodes 
  for(unsigned i = 0; i < tSize; ++i)
  {
    ChHTNode* nodes = tbl[i].Nodes;
    tbl[i].Count = 0;
    // delete all nodes in the element of the table array
    while(nodes)
    {
      ChHTNode* temp = nodes->Next;
      DeleteNode(nodes);
      nodes = temp;
    }
    tbl[i].Nodes = 0;
  }
}

/*********************************************************************/
 /*!
   \brief
     Creates new size of table based on given config data
     Deletes old table
 */
 /***********************************************************************/
template <typename T>
void ChHashTable<T>::GrowTable(void)
{
  ++m_stats.Expansions_;
  double newSize = std::ceil(m_stats.TableSize_ * m_growthFactor);
  unsigned newTableSize = GetClosestPrime(static_cast<unsigned>(newSize));
  ChHTHeadNode* newTable = ResizeTable(newTableSize);
  unsigned oldSize = m_stats.TableSize_;
  m_stats.TableSize_ = newTableSize;
  m_stats.Count_ = 0;
  // now put all data from old table into new table
  // once all data in transfered delete old table
  ChHTHeadNode* temp = m_table;
  // assign the new table now because we will use insert
  //  which inserts into m_table
  m_table = newTable;
  for(unsigned i = 0; i < oldSize; ++i)
  {
    ChHTNode* oldNodes = temp[i].Nodes;

    while(oldNodes)
    {
      // this will rehash all keys and put them in the correct spot
      insert(oldNodes->Key, oldNodes->Data);
      oldNodes = oldNodes->Next;
    }
  }
  // all data from old table is now in, clean up old table and delete it
  ClearTable(temp, oldSize);
  delete[] temp;// free actual table
}

