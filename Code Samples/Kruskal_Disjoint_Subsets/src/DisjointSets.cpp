/*
  DisjointSets.cpp
  Dmitri Volper
  implementation of classes DisjointSets, Head, Node
*/
#include "DisjointSets.h"

void Swap(size_t& temp1, size_t& temp2){
  size_t temp = temp1;
  temp1 = temp2;
  temp2 = temp;
}
//class Node implementation
Node::Node( size_t const& value ) : value(value), next() {}

Node* Node::Next() const { return next; }

void  Node::SetNext( Node* new_next ) { next = new_next; }

size_t Node::Value() const { return value; }

std::ostream& operator<< (std::ostream& os, Node const& node) {
	os << "(" << node.value << " ) -> ";
	return os;
}

//class Head implementation
Head::Head( ) : counter(), first(), last() {} 

Head::~Head() {
  Node* toDel = GetFirst();
  Node* next = GetFirst();
  while (toDel != NULL)
  {
    next = toDel->Next();
    delete toDel;
    toDel = next;
  }
}
void   Head::Join( Head* pHead2 ) {
  Node* end = GetLast();
  end->SetNext(pHead2->GetFirst());
  last = pHead2->GetLast();
  counter += pHead2->counter;
}

size_t Head::Size() const      { return counter; }

void   Head::Reset()           { counter = 0; last = first = NULL; }

Node*  Head::GetFirst() const  { return first; }

Node*  Head::GetLast()  const  { return last; }

void   Head::Init( size_t value ) {
	first = last = new Node( value );
	counter = 1;
}


std::ostream& operator<< (std::ostream& os, Head const& head) {
	os << "[" << head.counter << " ] -> ";
	return os;
}

//class DisjointSets implementation
DisjointSets::DisjointSets( size_t const& capacity ) : 
	size(0),
	capacity(capacity),
	representatives(new size_t[capacity]),
	heads          (new Head[capacity])
{ }

DisjointSets::~DisjointSets() {
	delete [] heads;
	delete [] representatives;
}

void DisjointSets::Join( size_t const& id1, size_t const& id2 ) {
  size_t large = GetRepresentative(id2);
  size_t small = GetRepresentative(id1);

  if (small == large)
    return;
  // Add smaller list to bigger
  if (heads[large].Size() < heads[small].Size())
    Swap(large, small);

  // update representatives by changing representative of the 
  // small list to value of large representative
  Node* test = heads[small].GetFirst();
  while (test != NULL)
  {
    representatives[test->Value()] = large;
    test = test->Next();
  }

  heads[large].Join(&heads[small]);
  heads[small].Reset();
}

void DisjointSets::Make( ) {
	if ( size == capacity ) throw "DisjointSets::Make(...) out of space";
	heads[size].Init( size );
	representatives[size] = size;
	++size;
}


size_t DisjointSets::GetRepresentative( size_t const& id ) const {
	return representatives[id];
}

size_t DisjointSets::operator[]( size_t const& id ) const {
	return representatives[id];
}

std::ostream& operator<< (std::ostream& os, DisjointSets const& ds) {
	for (size_t i=0; i<ds.size; ++i ) {
		os << i << ":  ";
		Head *p_head = &ds.heads[i];
		os << *p_head;
		Node* p_node = p_head->GetFirst();
		while ( p_node ) {
			os << *p_node;
			p_node = p_node->Next();
		}
		os << "NULL (representative " << ds.representatives[i] << ")\n";
	}
	return os;
}
