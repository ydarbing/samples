/*
  TestDisjointSets.cpp
  Dmitri Volper
  test cases for disjoint subsets.
*/
#include <iostream>
#include "DisjointSets.h"
//#include "vld.h"
#pragma warning( disable : 4996 )//sscanf_s

void testDisjointSets0() {
	DisjointSets ds(10);
	for (int i=0; i<10; ++i ) ds.Make( );

	std::cout << ds << std::endl;

	for (int i=0; i<10; ++i ) 
		std::cout << i << " -> " << ds.GetRepresentative(i) << std::endl;
}

void testDisjointSets1() {
	DisjointSets ds(10);
	for (int i=0; i<10; ++i ) ds.Make( );

	ds.Join(1,0);
	std::cout << ds << std::endl;

	for (int i=0; i<10; ++i ) 
		std::cout << i << " -> " << ds.GetRepresentative(i) << std::endl;
}

void testDisjointSets2() {
	DisjointSets ds(10);
	for (int i=0; i<10; ++i ) ds.Make( );

	ds.Join(0,1);
	std::cout << ds << std::endl;

	for (int i=0; i<10; ++i ) 
		std::cout << i << " -> " << ds.GetRepresentative(i) << std::endl;
}

void testDisjointSets3() {
	DisjointSets ds(10);
	for (int i=0; i<10; ++i ) ds.Make( );

	ds.Join(0,1);
	ds.Join(0,2);
	ds.Join(0,3);
	ds.Join(0,4);
	std::cout << ds << std::endl;

	for (int i=0; i<10; ++i ) 
		std::cout << i << " -> " << ds.GetRepresentative(i) << std::endl;
}

void testDisjointSets4() {
	DisjointSets ds(10);
	for (int i=0; i<10; ++i ) ds.Make( );

	ds.Join(1,0);
	ds.Join(2,0);
	ds.Join(3,0);
	ds.Join(4,0);
	std::cout << ds << std::endl;

	for (int i=0; i<10; ++i ) 
		std::cout << i << " -> " << ds.GetRepresentative(i) << std::endl;
}

void testDisjointSets5() {
	DisjointSets ds(10);
	for (int i=0; i<10; ++i ) ds.Make( );

	ds.Join(1,0);
	ds.Join(0,2);
	ds.Join(3,0);
	ds.Join(0,4);
	std::cout << ds << std::endl;

	for (int i=0; i<10; ++i ) 
		std::cout << i << " -> " << ds.GetRepresentative(i) << std::endl;
}

void test6() {
	DisjointSets ds(10);
	for (int i=0; i<10; ++i ) ds.Make( );

	for (int i=1; i<10; ++i ) ds.Join(0,i);
	std::cout << ds << std::endl;

	for (int i=0; i<10; ++i ) 
		std::cout << i << " -> " << ds.GetRepresentative(i) << std::endl;
}

void test7() {
	DisjointSets ds(10);
	for (int i=0; i<10; ++i ) ds.Make( );

	for (int i=1; i<10; ++i ) ds.Join(i,0);
	std::cout << ds << std::endl;

	for (int i=0; i<10; ++i ) 
		std::cout << i << " -> " << ds.GetRepresentative(i) << std::endl;
}

void test8() {
	DisjointSets ds(10);
	for (int i=0; i<10; ++i ) ds.Make( );

	for (int i=1; i<10; i+=2 ) { 
    ds.Join(i, 0);
    std::cout << ds << std::endl;
    ds.Join(0, i);
    std::cout << ds << std::endl;
	}
	std::cout << ds << std::endl;

	for (int i=0; i<10; ++i ) 
		std::cout << i << " -> " << ds.GetRepresentative(i) << std::endl;
}

void test9() {
	DisjointSets ds(10);
	for (int i=0; i<10; ++i ) ds.Make( );

	for (int i=1; i<10; i+=2 ) {
		ds.Join(i, 0);
		ds.Join(0, i);
	}
	std::cout << ds << std::endl;

	for (int i=0; i<10; ++i ) 
		std::cout << i << " -> " << ds.GetRepresentative(i) << std::endl;
}

void test10() {
	DisjointSets ds(10);
	for (int i=0; i<10; ++i ) ds.Make( );

	//test self-join
	ds.Join(0,1);
	ds.Join(0,1);
	ds.Join(1,0);
	std::cout << ds << std::endl;
	for (int i=0; i<10; ++i ) { 
		std::cout << i << " -> " << ds.GetRepresentative(i) << std::endl;
	}
}

void test11() {
  DisjointSets ds(5);
  for (int i = 0; i < 5; ++i) 
    ds.Make();

  ds.Join(0, 2);
  std::cout << ds << std::endl;
  ds.Join(1, 3);
  std::cout << ds << std::endl;
  ds.Join(0, 4);
  std::cout << ds << std::endl;
  ds.Join(2, 3);
  std::cout << ds << std::endl;
  for (int i = 0; i < 5; ++i) {
    std::cout << i << " -> " << ds.GetRepresentative(i) << std::endl;
  }
}
void(*pDisjointSetsTests[])(void) = { testDisjointSets0, testDisjointSets1, testDisjointSets2, testDisjointSets3, testDisjointSets4, testDisjointSets5, test6, test7, test8, test9, test10, test11 };

int main2( int argc, char ** argv) try {
	if (argc!=2) 
    return 1;
	else 
  {
		int test = 0;
		std::sscanf(argv[1],"%i",&test);
    pDisjointSetsTests[test]();
	}
	return 0;
} catch ( char const* e ) {
	std::cout << e << std::endl;
  return 1;
}

