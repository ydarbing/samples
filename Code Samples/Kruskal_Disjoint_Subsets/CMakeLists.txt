cmake_minimum_required(VERSION 3.17)

project("Kruskal_Disjoint_Subset")

set(header_files
   include/DisjointSets.h
   include/Graph.h
   include/Kruskal.h
)

set(source_files
   src/main.cpp
   src/DisjointSets.cpp
   src/TestDisjointSets.cpp
)

ADD_EXECUTABLE("${PROJECT_NAME}" 
               ${source_files} 
               ${header_files}
               )

target_include_directories("${PROJECT_NAME}" PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")
