/*
  Brady Reuter
*/
#pragma once
#include "DisjointSets.h"
#include "graph.h"
#include <list>

//efficient version, using disjoint subsets
template <typename Vertex, typename Edge>
std::vector<Edge> kruskal_disjoint_subsets( Graph<Vertex,Edge> const& g ) {
  std::vector<Edge> mst;
  unsigned numVerts = static_cast<unsigned>(g.Size());

  DisjointSets ds(numVerts);
  for (unsigned i = 0; i < numVerts; ++i)
    ds.Make();

  std::list<Edge> edges = g.GetEdges();
  edges.sort();

  Edge e;
  while (mst.size() != (numVerts - 1) && !edges.empty())
  {
    e = edges.front();
    // check to see if this edge will create cycle
    if (ds[e.ID1()] != ds[e.ID2()])
    {  
      ds.Join(e.ID1(), e.ID2());
      mst.push_back(e);
    }
    edges.pop_front();
  }
	return mst;
}

//inefficient version, using array of vector of "colors"
template <typename Vertex, typename Edge>
std::vector<Edge> kruskal_vector( Graph<Vertex,Edge> const& g ) 
{
  std::list<Edge> edges = g.GetEdges();
  edges.sort();
  unsigned numVerts = static_cast<unsigned>(g.Size());
  //assign each vertex a unique color
  std::vector<int>  colors(numVerts);
  for (unsigned i = 0; i < numVerts; ++i)
    colors[i] = i;

  std::vector<Edge> mst;
  Edge e;
  while (mst.size() != (numVerts - 1) && !edges.empty())
  {
    e = edges.front();
    // check to see if this edge will create cycle
    if (colors[e.ID1()] != colors[e.ID2()])
    {
      int color = colors[e.ID1()];
      for (unsigned i = 0; i < numVerts; ++i)
      {
        if (colors[i] == color)// make sure to update colors
          colors[i] = colors[e.ID2()];
      }
      mst.push_back(e);
    }

    edges.pop_front();
  }
	return mst;
}
