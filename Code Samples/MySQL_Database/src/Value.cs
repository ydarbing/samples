// MySqlDB Value.

using System;

namespace mysqldb
{
    public class Value : IEquatable<Value>
    {
        public enum Type { INTEGER, FLOAT, STRING };
        Type type;
        int ival;
        float fval;
        string sval;

        // Comparators.
        public enum Comparator { EQ, NE, LT, GT };

        public Value(int ival)
        {
            type = Type.INTEGER;
            this.ival = ival;
        }

        public Value(float fval)
        {
            type = Type.FLOAT;
            this.fval = fval;
        }

        public Value(string sval)
        {
            type = Type.STRING;
            this.sval = sval;
        }

        public Type GetValueType()
        {
            return type;
        }

        public int GetIntVal()
        {
            return ival;
        }

        public float GetFloatVal()
        {
            return fval;
        }

        public string GetStringVal()
        {
            return sval;
        }

        public bool Compare(Value value, Comparator comparator)
        {
            if (value.type != type)
            {
                return false;
            }
            switch (comparator)
            {
                case Comparator.EQ:
                    switch (type)
                    {
                        case Type.INTEGER:
                            if (value.ival == ival)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        case Type.FLOAT:
                            if (value.fval == fval)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        case Type.STRING:
                            if (value.sval.Equals(sval))
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                    }
                    break;
                case Comparator.NE:
                    switch (type)
                    {
                        case Type.INTEGER:
                            if (value.ival != ival)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        case Type.FLOAT:
                            if (value.fval != fval)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        case Type.STRING:
                            if (!value.sval.Equals(sval))
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                    }
                    break;
                case Comparator.LT:
                    switch (type)
                    {
                        case Type.INTEGER:
                            if (value.ival < ival)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        case Type.FLOAT:
                            if (value.fval < fval)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        case Type.STRING:
                            if (String.CompareOrdinal(value.sval, sval) < 0)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                    }
                    break;
                case Comparator.GT:
                    switch (type)
                    {
                        case Type.INTEGER:
                            if (value.ival > ival)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        case Type.FLOAT:
                            if (value.fval > fval)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        case Type.STRING:
                            if (String.CompareOrdinal(value.sval, sval) > 0)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                    }
                    break;
            }
            return false;
        }

        // Get hash code.
        override public int GetHashCode()
        {
            switch (type)
            {
                case Type.INTEGER:
                    return ival.GetHashCode();
                case Type.FLOAT:
                    return fval.GetHashCode();
                case Type.STRING:
                    return sval.GetHashCode();
            }
            return -1;
        }


        public string GetComparatorString(Comparator comp)
        {
          switch (comp)
          {
            case Comparator.EQ:
              return "=";
            case Comparator.NE:
              return "!=";
            case Comparator.GT:
              return ">";
            default: // LT
              return "<";
          }
        }

        // Equality comparer.
        public bool Equals(Value other)
        {
            if (Compare(other, Comparator.EQ))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string PrintVal()
        {
            switch (type)
            {
                case Type.INTEGER:
                    return ival + "";
                case Type.FLOAT:
                    return fval + "";
                case Type.STRING:
                    return "'" + sval + "'";
            }
            return "";
        }
    }
}
