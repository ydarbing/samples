// MySqlDB Table.

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;

namespace mysqldb
{
    public class Table
    {
        // Table name.
        string name;

        // Column names.
        List<string> columns;

        // Column types.
        List<Value.Type> types;

        // Server connection.
        SqlConnection connection;
        private string statementSetup;
        private string statementSetupValues;

        // Constructor.
        public Table(string name, List<string> columns, List<Value.Type> types, 
            SqlConnection connection)
        {
            this.name = name;
            this.columns = columns;
            this.types = types;
            this.connection = connection;

            if(columns.Count > 0)
            {
              statementSetup = this.columns[0];
              statementSetupValues = "@" + this.columns[0];
              for (int i = 1; i < columns.Count; ++i)
              {
                statementSetup += ", " + columns[i];
                statementSetupValues += ", " + "@" + columns[i];
              }
            }
        }

        // get name
        public string GetName()
        {
            return name;
        }

        // Get columns.
        List<string> GetColumns()
        {
            return columns;
        }

        // Get column types.
        List<Value.Type> GetColumnTypes()
        {
            return types;
        }

        // Insert row.
        public bool Insert(List<Value> row)
        {
          if(row.Count != columns.Count)
            return false;
          int ret = -1;
          // Need to now allow duplicate rows being inserted
          string statement = "IF NOT EXISTS (SELECT * FROM " + name + " WHERE " + columns[0] + " = " + row[0].PrintVal();

          for (int i = 1; i < row.Count; ++i)
          {
            statement += " AND " + columns[i] + " = " + row[i].PrintVal();
          }

          // add actual insert
          statement += ") INSERT INTO " + name + " (" + statementSetup + ")"
                                  + " VALUES(" + statementSetupValues + ")";

          using( SqlCommand myCommand = new SqlCommand(statement, connection))
          {
            for (int i = 0; i < columns.Count; ++i)
            {
              string param = "@" + columns[i];
              switch (types[i])
              {
                case Value.Type.FLOAT:
                  myCommand.Parameters.Add(param, System.Data.SqlDbType.Float);
                  myCommand.Parameters[param].Value = row[i].GetFloatVal();
                  break;
                case Value.Type.INTEGER:
                  myCommand.Parameters.Add(param, System.Data.SqlDbType.Int);
                  myCommand.Parameters[param].Value = row[i].GetIntVal();
                  break;
                case Value.Type.STRING:
                  myCommand.Parameters.Add(param, System.Data.SqlDbType.VarChar, 100);
                  myCommand.Parameters[param].Value = row[i].GetStringVal();
                  break;
              }
            }

            try
            {
              ret = myCommand.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
              Debug.Assert(false, "Insert failed: " + ex.ToString());
              return false;
            }
          }

          return ret > 0;
        }

        // Delete row.
        public bool Delete(List<Value> row)
        {
          if (row.Count == 0)
            return false;

          string statement = "DELETE FROM " + name + " WHERE " + columns[0] + " = " + row[0].PrintVal();

          for (int i = 1; i < row.Count; ++i)
          {
            statement += " AND " + columns[i] + " = " + row[i].PrintVal();
          }

          int ret = NoParamQuery(statement, "Delete failed: ");
          return ret > 0;
        }

        private int NoParamQuery(string statement, string ifErrorShow = "")
        {
          int ret = -1;
          using (SqlCommand cmd = new SqlCommand(statement, connection))
          {
            try
            {
              ret = cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
              Debug.Assert(false, ifErrorShow + ex.ToString());
            }
          }

          return ret;
        }

        // Select from table based on column value and condition.
        // Evaluation: input value condition table.column.value
        public SqlDataReader Select(int columnIndex, Value value, Value.Comparator condition)
        {
          string compare = value.GetComparatorString(condition);

          string statement = "SELECT * FROM " + name + 
                            " WHERE " + columns[columnIndex] + " " + compare + " " + value.PrintVal();

          return ReaderQuery(statement, "Select failed: ");
        }

        // Project from table based on column indexes.
        public SqlDataReader Project(List<int> columnIndexes)
        {
          if(columnIndexes.Count == 0)
          {
            return null;
          }

          string projectedCols = "";
          int i = 0;
          // get first valid column to project
          while (i < columnIndexes.Count)
          {
            if (0 <= columnIndexes[i] && columnIndexes[i] < columns.Count)
            {
              projectedCols = columns[columnIndexes[i]];
              break;
            }
            ++i;
          }
          // now do comma separated section, starting at i + 1 where the previous looped stopped
          while(++i < columnIndexes.Count)
          {
            if(0 <= columnIndexes[i] && columnIndexes[i] < columns.Count)
            {
              projectedCols += ", " + columns[columnIndexes[i]];
            }
          }

          string statement = "SELECT " + projectedCols + " FROM " + name;

          return ReaderQuery(statement, "Project failed: ");
        }

        // Join from tables based on column values (inner join).
        // Evaluation: table.column1.value condition table2.column2.value
        public SqlDataReader Join(int columnIndex1, Table table2, int columnIndex2, 
            Value.Comparator condition)
        {
          Value temp = new Value(1);
          string compare = temp.GetComparatorString(condition);

          string statement = "SELECT * FROM " + name + " t1 " + 
                            "JOIN " + table2.GetName() + " t2 " + 
                            "ON " + "t1." + columns[columnIndex1] + compare + "t2." + table2.GetColumns()[columnIndex2];

          return ReaderQuery(statement, "Joined failed: ");
        }

        private SqlDataReader ReaderQuery(string statement, string ifErrorShow = "")
        {
          SqlDataReader reader = null;

          using(SqlCommand cmd = new SqlCommand(statement, connection))
          {
            try
            {
              reader = cmd.ExecuteReader();
            }
            catch (SqlException ex)
            {
              Debug.Assert(false, ifErrorShow + ex.ToString());
              return null;
            }
          }
          return reader;
        }

        // Print.
        public void Print()
        {
          SqlDataReader reader = ReaderQuery(("SELECT * FROM " + name), "Printing table failed: ");
          // print out column names
          string cols = columns[0];
          for (int i = 1; i < columns.Count; ++i)
          {
            cols += "\t" + columns[i];
          }
          Console.WriteLine(cols);

          // print out table innards
          while (reader.Read())
          {
            string row = reader[columns[0]].ToString();
            for (int i = 1; i < columns.Count; ++i)
            {
              row += "\t" + reader[columns[i]].ToString();
            }

            Console.WriteLine(row);
          }
          //
          if (reader != null)
          {
            reader.Close();
          }
        }
    }
}
