// MySqlDB database.

using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace mysqldb
{
    public class Database
    {
        // Name.
        string name;

        // Server connection.
        SqlConnection connection;

        // Tables.
        List<Table> tables;

        // Constructor.
        public Database(string name)
        {
          string DBserver = "BRADY-PC\\GURPGORK";//"(local)"
          string connectionString = GetConnectionString(DBserver, "master");
          this.name = name;
          tables = new List<Table>();

          using (SqlConnection first = new SqlConnection(connectionString))
          {
            try
            {
              first.Open();
              string dropDB = "if exists (select name from sys.databases where name = '" + name + "') drop database " + name;
              string makeDB = "if not exists (select name from sys.databases where name = '" + name + "') create database " + name;
              SqlCommand cmd = new SqlCommand(dropDB, first);
              cmd.ExecuteNonQuery();

              cmd.CommandText = makeDB;
              cmd.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
              Console.WriteLine("Cannot open connection to master database: " + exception.ToString());
            }
          }

          connection = new SqlConnection(GetConnectionString(DBserver, name));

          try
          {
            connection.Open();
          }
          catch (Exception exception)
          {
            Console.WriteLine("Cannot open connection to master database: " + exception.ToString());
          }
        }


        string GetConnectionString(String server, String database)
        {
          return ("Data Source=" + server + ";Initial Catalog=" + database + ";"
                   + "Trusted_Connection=yes;MultipleActiveResultSets=True");
        }

        // Get database name.
        string GetName()
        {
            return name;
        }

        // Get all tables.
        List<Table> GetTables()
        {
            return tables;
        }

        // Get table by name.
        Table GetTable(string name)
        {
          foreach (var table in tables)
          {
            if (table.GetName() == name)
              return table;
          }
          return null;
        }

        bool TableInDatabase(string name)
        {
          foreach(var table in tables)
          {
            if (table.GetName() == name)
              return true;
          }
          return false;
        }

        // Create table.
        public Table CreateTable(string name, List<string> columns, List<Value.Type> types)
        {
          // fast local check so their are no duplicates
          if(TableInDatabase(name))
          {
              return null;
          }

          // make the table based off given column names and types
          string tableInnards = GetTableInnards(columns, types);

          //string statement = "CREATE TABLE IF NOT EXISTS " + name + " (" + tableInnards + ");";
          string statement = "CREATE TABLE " + name + " (" + tableInnards + ");";

          try
          {
            SqlCommand myCommand = new SqlCommand(statement, connection);
            myCommand.ExecuteNonQuery();
          }
          catch (Exception exception)
          {
            Console.WriteLine("Could not make table: " + exception.ToString());
          }

          Table newTable = new Table(name, columns, types, connection);
          tables.Add(newTable);  
           
          return newTable;
        }

        // Drop table.
        public bool DropTable(string name)
        {
          Table toRemove = GetTable(name);
          if (toRemove != null)
          {
            string statement = "IF OBJECT_ID('" + name + "', 'U') IS NOT NULL DROP TABLE " + name;
            try
            {
              SqlCommand myCommand = new SqlCommand(statement, connection);
              myCommand.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
              Console.WriteLine("Error dropping table: " + exception.ToString());
            }

            return tables.Remove(toRemove);
          }
          return false;
        }

        // Print database tables.
        public void Print()
        {
          Console.WriteLine();
          Console.WriteLine("Database name: " + name);
          Console.WriteLine();

          foreach(var table in tables)
          {
            Console.WriteLine("Table: " + table.GetName());
            table.Print();
            Console.WriteLine();
          }
        }

        private string GetTableInnards(List<string> columns, List<Value.Type> types)
        {
          if (columns.Count == 0)
            return "";

          string ret = columns[0] + " " + GetDataTypeString(types[0]); 

          for (int i = 1; i < columns.Count; ++i)
          {
            // name of column
            ret += "," + columns[i] + " ";

            // plus the type of data it will contain
            ret += GetDataTypeString(types[i]);
          }

          return ret;
        }

        private string GetDataTypeString(Value.Type type)
        {
          string ret = "VARCHAR(100)"; // default to string
          switch (type)
          {
            case Value.Type.FLOAT: ret = "FLOAT"; break;
            case Value.Type.INTEGER: ret = "INTEGER"; break;
          }

          return ret;
        }

    }
}
