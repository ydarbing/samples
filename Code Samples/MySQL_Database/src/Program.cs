﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace mysqldb
{
  class Program
  {
    static void Main(string[] args)
    {
      // Create database.
      Database db = new Database("MySqlDB");

      // Create PartPrice table
      Console.WriteLine("\nCreating PartPrice table");
      List<string> columns = new List<string>();
      List<Value.Type> types = new List<Value.Type>();
      columns.Add("PartNumber");
      types.Add(Value.Type.INTEGER);
      columns.Add("Supplier");
      types.Add(Value.Type.STRING);
      columns.Add("Price");
      types.Add(Value.Type.FLOAT);
      Table PartPrice = db.CreateTable("PartPrice", columns, types);

      Console.Write("\nAttempt to create duplicate PartPrice table: ");
      if (db.CreateTable("PartPrice", columns, types) == null)
      {
        Console.WriteLine("null returned");
      }

      Console.Write("\nDropping PartPrice table: ");
      if (db.DropTable("PartPrice"))
      {
        Console.WriteLine("success");
      }

      Console.Write("\nAttempt to recreate PartPrice table: ");
      if ((PartPrice = db.CreateTable("PartPrice", columns, types)) != null)
      {
        Console.WriteLine("success");
      }

      List<Value> row = new List<Value>();
      row.Add(new Value(10010));
      row.Add(new Value("Seagate"));
      row.Add(new Value(100.0f));
      PartPrice.Insert(row);
      row = new List<Value>();
      row.Add(new Value(10010));
      row.Add(new Value("IBM"));
      row.Add(new Value(90.0f));
      PartPrice.Insert(row);
      row = new List<Value>();
      row.Add(new Value(10220));
      row.Add(new Value("Kensington"));
      row.Add(new Value(220.0f));
      PartPrice.Insert(row);
      row = new List<Value>();
      row.Add(new Value(10220));
      row.Add(new Value("IBM"));
      row.Add(new Value(290.0f));
      PartPrice.Insert(row);
      row = new List<Value>();
      row.Add(new Value(10220));
      row.Add(new Value("Sun"));
      row.Add(new Value(310.0f));
      PartPrice.Insert(row);
      row = new List<Value>();
      row.Add(new Value(10440));
      row.Add(new Value("IBM"));
      row.Add(new Value(2100.0f));
      PartPrice.Insert(row);
      Console.WriteLine("\nPartPrice table:");
      PartPrice.Print();

      bool ret = PartPrice.Insert(row);
      Console.WriteLine("\nPartPrice table after attempted duplicate last row insert, ret=" + ret);
      PartPrice.Print();

      ret = PartPrice.Delete(row);
      Console.WriteLine("\nPartPrice table after last row delete, ret=" + ret);
      PartPrice.Print();

      ret = PartPrice.Delete(row);
      Console.WriteLine("\nPartPrice table after last row duplicate delete, ret=" + ret);
      PartPrice.Print();

      ret = PartPrice.Insert(row);
      Console.WriteLine("\nPartPrice table after last row reinsert, ret=" + ret);
      PartPrice.Print();

      // Create PartDesc table.
      columns = new List<string>();
      types = new List<Value.Type>();
      columns.Add("PartNumber");
      types.Add(Value.Type.INTEGER);
      columns.Add("Description");
      types.Add(Value.Type.STRING);
      Table PartDesc = db.CreateTable("PartDesc", columns, types);
      row = new List<Value>();
      row.Add(new Value(10010));
      row.Add(new Value("20 GB disk"));
      PartDesc.Insert(row);
      row = new List<Value>();
      row.Add(new Value(10220));
      row.Add(new Value("256 MB RAM card"));
      PartDesc.Insert(row);
      row = new List<Value>();
      row.Add(new Value(10440));
      row.Add(new Value("17\" LCD monitor"));
      PartDesc.Insert(row);
      Console.WriteLine("\nPartDesc table:");
      PartDesc.Print();

      // Create SupplierAddr table.
      columns = new List<string>();
      types = new List<Value.Type>();
      columns.Add("Supplier");
      types.Add(Value.Type.STRING);
      columns.Add("Address");
      types.Add(Value.Type.STRING);
      Table SupplierAddr = db.CreateTable("SupplierAddr", columns, types);
      row = new List<Value>();
      row.Add(new Value("Seagate"));
      row.Add(new Value("Cuppertino, CA"));
      SupplierAddr.Insert(row);
      row = new List<Value>();
      row.Add(new Value("IBM"));
      row.Add(new Value("Armonk, NY"));
      SupplierAddr.Insert(row);
      row = new List<Value>();
      row.Add(new Value("Kensington"));
      row.Add(new Value("San Mateo, CA"));
      SupplierAddr.Insert(row);
      row = new List<Value>();
      row.Add(new Value("Sun"));
      row.Add(new Value("Palo Alto, CA"));
      SupplierAddr.Insert(row);
      Console.WriteLine("\nSupplierAddr table:");
      SupplierAddr.Print();

      Console.WriteLine("\nSelecting PartNumber 10010 from PartPrice");
      using (SqlDataReader reader = PartPrice.Select(0, new Value(10010), Value.Comparator.EQ))
      {
        try
        {
          while (reader.Read())
          {
            Console.Write(reader.GetSqlInt32(0) + "\t");
            Console.Write(reader.GetString(1) + "\t");
            Console.Write(reader.GetSqlDouble(2) + "");
            Console.WriteLine();
          }
        }
        catch (Exception exception)
        {
          Debug.Assert(false, "Select failed: " + exception.ToString());
        }
      }

      Console.WriteLine("\nSelecting PartNumber < 10440 from PartDesc");
      using (SqlDataReader reader = PartDesc.Select(0, new Value(10440), Value.Comparator.LT))
      {
        try
        {
          while (reader.Read())
          {
            Console.Write(reader.GetSqlInt32(0) + "\t");
            Console.Write(reader.GetString(1) + "\t");
            Console.WriteLine();
          }
        }
        catch (Exception exception)
        {
          Debug.Assert(false, "Select failed: " + exception.ToString());
        }
      }

      Console.WriteLine("\nSelecting PartNumber > 10440 from PartDesc");
      using (SqlDataReader reader = PartDesc.Select(0, new Value(10440), Value.Comparator.GT))
      {
        try
        {
          while (reader.Read())
          {
            Console.Write(reader.GetSqlInt32(0) + "\t");
            Console.Write(reader.GetString(1) + "\t");
            Console.WriteLine();
          }
        }
        catch (Exception exception)
        {
          Debug.Assert(false, "Select failed: " + exception.ToString());
        }
      }

      Console.WriteLine("\nProjecting Supplier, Price from PartPrice");
      List<int> columnIndexes = new List<int>();
      columnIndexes.Add(1);
      columnIndexes.Add(2);
      using (SqlDataReader reader = PartPrice.Project(columnIndexes))
      {
        try
        {
          while (reader.Read())
          {
            Console.Write(reader.GetString(0) + "\t");
            Console.Write(reader.GetSqlDouble(1) + "\t");
            Console.WriteLine();
          }
        }
        catch (Exception exception)
        {
          Debug.Assert(false, "Project failed: " + exception.ToString());
        }
      }

      Console.WriteLine("\nProjecting Address from SupplierAddr");
      columnIndexes = new List<int>();
      columnIndexes.Add(1);
      using (SqlDataReader reader = SupplierAddr.Project(columnIndexes))
      {
        try
        {
          while (reader.Read())
          {
            Console.Write(reader.GetString(0) + "\t");
            Console.WriteLine();
          }
        }
        catch (Exception exception)
        {
          Debug.Assert(false, "Project failed: " + exception.ToString());
        }
      }

      Console.WriteLine("\nJoining PartPrice, SupplierAddr on Supplier");
      using (SqlDataReader reader = PartPrice.Join(1, SupplierAddr, 0, Value.Comparator.EQ))
      {
        try
        {
          while (reader.Read())
          {
            Console.Write(reader.GetSqlInt32(0) + "\t");
            Console.Write(reader.GetString(1) + "\t");
            Console.Write(reader.GetSqlDouble(2) + "\t");
            Console.Write(reader.GetString(3) + "\t");
            Console.Write(reader.GetString(4) + "\t");
            Console.WriteLine();
          }
        }
        catch (Exception exception)
        {
          Debug.Assert(false, "Join failed: " + exception.ToString());
        }
      }

      Console.WriteLine("\nPrinting database:");
      db.Print();

      Console.WriteLine("\nDone with tests. Press Enter to continue.");
      Console.ReadLine();
    }
  }
}
