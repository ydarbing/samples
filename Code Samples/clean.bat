@echo off
REM delete all folders created from premake file
rd /s /q _tmp 2>NUL >NUL
rd /s /q _settings 2>NUL >NUL
rd /s /q _ipch 2>NUL >NUL
rd /s /q x64 2>NUL >NUL
rd /s /q ipch 2>NUL >NUL
rd /s /q .vs 2>NUL >NUL
REM delete all visual studio files
del /s/q *.sdf, *.sln, *.vcxproj, *.filters, *.csproj, *.user *.opensdf *.db 2>NUL >NUL
REM delete the hidden suo file
del /s/q /a:h *.suo 2>NUL >NUL
REM loop through all subdirectories deleted all folders called bin
FOR /d /r . %%d in (bin) DO @if exist "%%d" rd /s/q "%%d"
@echo on