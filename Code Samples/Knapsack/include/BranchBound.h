#pragma once
#include <vector>

#include "Item.h"

//plain vanilla backtracking
std::vector<int> knapsack_backtracking( std::vector<Item> const& items, int const& W );

//backtracking with upper bound optimization
std::vector<int> knapsack_backtracking_branchbound( std::vector<Item> const& items, int const& W );

