#pragma once
#include <vector>
#include "Item.h"


//dynamic programming
std::vector<int> knapsackDP( std::vector<Item> const& items, int const& W );

//backtracking with memory
std::vector<int> knapsackRecMem( std::vector<Item> const& items, int const& W );

