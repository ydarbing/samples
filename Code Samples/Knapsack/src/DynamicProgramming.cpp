#include "DynamicProgramming.h"
#include <iostream>
#include <numeric>
#include <algorithm> // max

////////////////////////////////////////////////////////////
typedef std::vector< std::vector<int> > Table; //2-dimensional table

void InitTable(Table& table, const int numCols, const int numRows);
std::vector<int> GetBag(std::vector<Item> const& items, Table& solution);


////////////////////////////////////////////////////////////
//the returned value is a vector of indices
std::vector<int> knapsackDP( std::vector<Item> const& items, int const& W ) {
	int num_items = (int)items.size();
  Table table;

  InitTable(table, W + 1, num_items + 1);
  Table solution = table;
  
  for (int i = 1; i <= W; ++i)
  {
    for (int j = 1; j <= num_items; ++j)
    {
      int v1 = 0;
      solution[i][j] = 0;
      if (items[j - 1].weight <= i)
      {
        v1 = table[i - items[j - 1].weight][j - 1] + items[j - 1].value;
        if (v1 > table[i][j-1])
          solution[i][j] = 1;
      }
      table[i][j] = std::max(v1, table[i][j - 1]);
    }
  }
	//print final table - for debugging?
  //do not delete this code
  if ( num_items + W < 50 ) { //print only if table is not too big
      std::cout << "   ";
      for ( int n=0; n<=num_items; ++n) { std::cout << n << "     "; }
      std::cout << "  items\n        ";
      for (int n = 0; n<num_items; ++n) { std::cout << items[n].weight << "," << items[n].value << "   "; }
      std::cout << "\n   ";
      for (int n = 0; n <= num_items; ++n) { std::cout << "------"; }
      std::cout << std::endl;

      for ( int w=0; w<=W; ++w) {
          std::cout << w << "| ";
          for (int n = 0; n <= num_items; ++n) {
              std::cout << table[w][n] << "     ";
          }
          std::cout << std::endl;
      }
  }
  //end do not delete this code 


	//figure out which items are in the bag based on the table
	return GetBag(items, solution);
}

////////////////////////////////////////////////////////////
int valueBag( std::vector<Item> const& items, std::vector<int> const& bag ) {
	std::vector<int>::const_iterator it   = bag.begin(),
		                          it_e = bag.end();

	int accum = 0;
	//std::cout << "Bag ";
	for ( ; it != it_e; ++it) { 
		accum += items[ *it ].value; 
		//std::cout << *it << " ";
	}
	//std::cout << std::endl;
	return accum;
}

////////////////////////////////////////////////////////////
//prototype
//notice that auxiliary function returns value of the vector of items
//the actual vector is determined later from the table (similar to DP solution)
int knapsackRecMemAux( std::vector<Item> const&, int const&, int, Table&, Table& );

////////////////////////////////////////////////////////////
//function to kick start
std::vector<int> knapsackRecMem( std::vector<Item> const& items, int const& W ) {
  int num_items = (int)items.size();
  Table table;
  InitTable(table, W + 1, num_items + 1);
  Table solution = table;

  /*int maxValue = */knapsackRecMemAux(items, W, num_items, table, solution);

	//print table - debugging?
    //do not delete this code
    if ( num_items + W < 50 ) { //print only if table is not too big
        std::cout << "   ";
        for (int n = 0; n <= num_items; ++n) { std::cout << n << "     "; }
        std::cout << "  items\n        ";
        for (int n = 0; n<num_items; ++n) {
            std::cout << items[n].weight << "," << items[n].value<<"   "; 
        }
        std::cout << "\n   ";
        for (int n = 0; n <= num_items; ++n) { std::cout << "------"; }
        std::cout << std::endl;

        for ( int w=0; w<=W; ++w) {
            std::cout << w << "| ";
            for (int n = 0; n <= num_items; ++n) {
                std::cout << table[w][n] << "     ";
            }
            std::cout << std::endl;
        }
    }
    //end do not delete this code 

	return GetBag(items, solution);
}

void InitTable(Table& table, const int numCols, const int numRows)
{
  // table has W on y-axis and items along x-axis
  table.resize(numCols);
  // set first column to 0
  for (int i = 0; i <= numCols - 1; ++i)
  {
    table[i].resize(numRows, -1);
    table[i][0] = 0;
  }
  // set first row to 0
  for (int i = 0; i <= numRows - 1; ++i)
    table[0][i] = 0;
}

std::vector<int> GetBag(std::vector<Item> const& items, Table& solution)
{
  std::vector<int> bag;
  int col = (int)solution.size() - 1;
  int row = (int)solution[0].size() - 1;

  while (row != 0)
  {
    if (solution[col][row] == 1)
    {
      bag.push_back(row - 1);
      col -= items[row - 1].weight;
    }
    --row;
  }
  return bag;
}
////////////////////////////////////////////////////////////
//the real recursive function
int knapsackRecMemAux( std::vector<Item> const& items, int const& W, int index, Table & table, Table& solution ) {
  if (index == 0 || W == 0)
    return 0;
  //check database
  if (table[W][index] > 0)
    return table[W][index];

  int v1 = 0;
  if (items[index - 1].weight <= W)
    v1 = knapsackRecMemAux(items, W - items[index - 1].weight, index - 1, table, solution) 
         + items[index - 1].value;

  int v2 = knapsackRecMemAux(items, W, index - 1, table, solution);
  // which was the solution
  if (v1 > v2)
    solution[W][index] = 1;
  else
    solution[W][index] = 0;

  table[W][index] = std::max(v1, v2);
  return table[W][index];
}
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
