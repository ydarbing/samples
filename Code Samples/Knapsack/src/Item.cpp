#include "Item.h"


Item::Item(int const& weight /*= 0*/, int const& value /*= 0 */)
  : weight(weight), value(value)
{
}

Item::Item(Item const& original)
  : weight(original.weight), value(original.value)
{
}
//compare values per unit of weight
bool Item::operator<(Item const& arg2) const
{
  return value*arg2.weight < arg2.value*weight; //same logic, but faster
}

std::ostream& operator<<(std::ostream& os, Item const& item)
{
  os << "(" << item.weight << "," << item.value << ")";
  return os;
}

std::istream& operator>>(std::istream& os, Item& item)
{
  os >> item.weight >> item.value;
  return os;
}
