#include "BranchBound.h"
#include <limits>


void PrintBagOutput(std::vector<int> &bagSoFar, std::vector<int>& bestBagSoFar, std::vector<Item> const& items, int& weightSoFar, int& bestBagValueSoFar)
{
  static unsigned max_size_to_print = 12;

  if (items.size() < max_size_to_print)
    std::cout << "Base case: bag { ";

  int valueSoFar = 0;
  for (unsigned i = 0; i < bagSoFar.size(); ++i)
  {
    valueSoFar += items[bagSoFar[i]].value;

    if (items.size() < max_size_to_print)
    {
      std::cout << bagSoFar[i]; // would like to_string
      if ((i + 1) != bagSoFar.size())
        std::cout << ",";
    }
  }

  if (items.size() < max_size_to_print)
  {
    std::cout << " } weight=" << weightSoFar;
    std::cout << ", value=" << valueSoFar;
  }

  if (valueSoFar > bestBagValueSoFar)
  {
    if (items.size() < max_size_to_print)
      std::cout << " - is new best (old best=" << bestBagValueSoFar << ")\n";
    bestBagValueSoFar = valueSoFar;
    bestBagSoFar = bagSoFar;
  }
  else
  {
    if (items.size() < max_size_to_print)
      std::cout << " - is not better then previous best (" << bestBagValueSoFar << ")" << std::endl;
  }
}


void GenerateSubsets(std::vector<Item> const& items, int const& W,
                     unsigned depth,
                     std::vector<int>& bestBagSoFar,
                     std::vector<int>& bagSoFar,
                     int& weightSoFar,
                     int& bestBagValueSoFar)
{
  if (depth == items.size())
  {
    if (weightSoFar <= W)
      PrintBagOutput(bagSoFar, bestBagSoFar, items, weightSoFar, bestBagValueSoFar);
    return;
  }

  weightSoFar += items[depth].weight;

  bagSoFar.push_back(depth);

  GenerateSubsets(items, W, depth + 1,
    bestBagSoFar, bagSoFar,
    weightSoFar, bestBagValueSoFar);

  bagSoFar.pop_back();
  weightSoFar -= items[depth].weight;

  GenerateSubsets(items, W, depth + 1,
    bestBagSoFar, bagSoFar,
    weightSoFar, bestBagValueSoFar);
}


//plain vanilla backtracking
std::vector<int> knapsack_backtracking(std::vector<Item> const& items, int const& W)
{
  std::vector<int> bestBagSoFar;
  std::vector<int> bagSoFar;
  int weightSoFar = 0;
  int bestBagValueSoFar = std::numeric_limits<int>::min();

  GenerateSubsets(items, W, 0, bestBagSoFar, bagSoFar, weightSoFar, bestBagValueSoFar);

  return bestBagSoFar;
}


double CalculateUpperBound(std::vector<Item> const& items, int const& W, unsigned depth, 
                           int& curValue, int& curWeight)
{
  int remainingCapacity = W - curWeight;
  double upperBound = static_cast<double>(curValue);

  // assume items are in decreasing order of value/weight ratio, so that 
  // continuous knapsack prefers items starting in the beginning of the list 
  for (unsigned i = depth; i < items.size(); ++i)
  {
    remainingCapacity -= items[i].weight;
    if (remainingCapacity > 0)
      upperBound += static_cast<double>(items[i].value);
    else // choose partial amount of it and break
    {
      remainingCapacity += items[i].weight;
      double rcd = static_cast<double>(remainingCapacity);
      double wd  = static_cast<double>(items[i].weight);
      double vd = static_cast<double>(items[i].value);
      upperBound += vd * (rcd / wd);
      break; // found maximum value (i.e upper bound)
    }
  }
  return upperBound;
}


void knapsack_backtracking_branchbound_rec(
  std::vector<Item> const& items, int const& W, unsigned depth,//current depth of recursion
  std::vector<int>& bestBagSoFar, int& bestValueSoFar,
  std::vector<int>& curBag, int& curValue, int& curWeight)//current bag
{
  // recursion through all subsets, based on "take/not take" current item (take is first)

  static unsigned maxSizeToPrint = 12;
  //base case
  if (depth == items.size()) 
  {
    if (items.size() < maxSizeToPrint) {
      std::cout << "Base case: bag ";
      print(curBag);
      std::cout << "weight=" << curWeight << ", value=" << curValue;
    }
    if (curValue > bestValueSoFar) {
      if (items.size() < maxSizeToPrint) {
        std::cout << " - is new best (old best=" << bestValueSoFar << ")\n";
      }
      bestValueSoFar = curValue;
      bestBagSoFar = curBag;
    }
    else {
      if (items.size() < maxSizeToPrint) {
        std::cout << " - is not better then previous best (" << bestValueSoFar << ")\n";
      }
    }
    return;
  }

  // upper bound check
  double upper_bound = CalculateUpperBound(items, W, depth, curValue, curWeight);

  if (upper_bound <= bestValueSoFar)
  {
    if (items.size() < maxSizeToPrint)
    {
      std::cout << "Cancel branch" << std::endl;
    }
    return;
  }

  //general case
  curWeight += items[depth].weight;
  curValue += items[depth].value;

  curBag.push_back(depth);

  if (curWeight <= W)
    knapsack_backtracking_branchbound_rec(items, W, depth + 1,
      bestBagSoFar, bestValueSoFar, 
      curBag, curValue, curWeight);

  curBag.pop_back();
  curWeight -= items[depth].weight;
  curValue  -= items[depth].value;

  if (curWeight <= W)
    knapsack_backtracking_branchbound_rec(items, W, depth + 1,
      bestBagSoFar, bestValueSoFar, 
      curBag, curValue, curWeight);

}


//backtracking with upper bound optimization
std::vector<int> knapsack_backtracking_branchbound(std::vector<Item> const& items, int const& W)
{
  //data to keep track of the best so far
  std::vector<int> bestBagSoFar;
  int              bestValueSoFar = std::numeric_limits<int>::min();
  //data to keep track of the current bag
  std::vector<int> curBag;
  curBag.reserve(items.size());
  int curValue = 0;
  int curWeight = 0;
  //kick-start recursion
  knapsack_backtracking_branchbound_rec(
    items, W, 0,                  //problem data
    bestBagSoFar, bestValueSoFar, //best bag so far
    curBag, curValue, curWeight); //current bag


  return bestBagSoFar;
}
