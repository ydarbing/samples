#include "DynamicProgramming.h"
#include "BranchBound.h"
#include <iostream>
#include <fstream> //ostream, istream
#include <vector>
#include <algorithm>

#pragma warning( disable : 4996 )//sscanf_s
void readItems(char const* filename, std::vector<Item> & items, int& W) {
    std::ifstream in(filename,std::ifstream::in);
    in >> W;

    Item item;
    in >> item;
    while ( in.good() ) {
        items.push_back( item );
        in >> item;
    }
}

void print_bag( std::vector<int> const& bag, std::vector<Item> const& store, int const& W ) {
    int total_weight = 0, total_value = 0;
    std::vector<int>::const_iterator it = bag.begin(), it_e = bag.end();
    for ( ; it != it_e; ++it) {
        std::cout << store[ *it ] << " - ";
        total_value  += store[ *it ].value;
        total_weight += store[ *it ].weight;
    }
    std::cout << "\nTotal grab " << total_value << " weighing " << total_weight << " (max " << W << ")\n";
}






void test0() {
    int W = 0;
    std::vector<Item> store;
    readItems( "items0", store, W );
    std::vector<int> bag = knapsackDP( store, W );
    print_bag( bag, store, W );
}

void test1() {
    int W = 0;
    std::vector<Item> store;
    readItems( "items1", store, W );
    std::vector<int> bag = knapsackDP( store, W );
    print_bag( bag, store, W );
}

void test2() {
    int W = 0;
    std::vector<Item> store;
    readItems( "items2", store, W );
    std::vector<int> bag = knapsackDP( store, W );
    print_bag( bag, store, W );
}

void test3() {
    int W = 0;
    std::vector<Item> store;
    readItems( "items3", store, W );
    std::vector<int> bag = knapsackDP( store, W );
    print_bag( bag, store, W );
}

void test4() {
    int W = 0;
    std::vector<Item> store;
    readItems( "items4", store, W );
    std::vector<int> bag = knapsackDP( store, W );
    print_bag( bag, store, W );
}

void test5() {
    int W = 0;
    std::vector<Item> store;
    readItems( "items5", store, W );
    std::vector<int> bag = knapsackDP( store, W );
    print_bag( bag, store, W );
}

void test6() {
    int W = 0;
    std::vector<Item> store;
    readItems( "items0", store, W );
    std::vector<int> bag = knapsackRecMem( store, W );
    print_bag( bag, store, W );
}

void test7() {
    int W = 0;
    std::vector<Item> store;
    readItems( "items1", store, W );
    std::vector<int> bag = knapsackRecMem( store, W );
    print_bag( bag, store, W );
}

void test8() {
    int W = 0;
    std::vector<Item> store;
    readItems( "items2", store, W );
    std::vector<int> bag = knapsackRecMem( store, W );
    print_bag( bag, store, W );
}

void test9() {
    int W = 0;
    std::vector<Item> store;
    readItems( "items3", store, W );
    std::vector<int> bag = knapsackRecMem( store, W );
    print_bag( bag, store, W );
}

void test10() {
    int W = 0;
    std::vector<Item> store;
    readItems( "items4", store, W );
    std::vector<int> bag = knapsackRecMem( store, W );
    print_bag( bag, store, W );
}

void test11() {
    int W = 0;
    std::vector<Item> store;
    readItems( "items5", store, W );
    std::vector<int> bag = knapsackRecMem( store, W );
    print_bag( bag, store, W );
}


void (*pTests[])() = { 
    test0,test1,test2,test3,test4,test5,
    test6,test7,test8,test9,test10,test11
}; 




void readBBitems(char const* filename, std::vector<Item> & items, int& W) {
  std::ifstream in(filename, std::ifstream::in);
  if (in.is_open()) {
    in >> W;

    Item item;
    in >> item;
    while (in.good()) {
      items.push_back(item);
      in >> item;
    }
  }
  else {
    std::cout << "problem reading " << filename << std::endl;
  }
  std::sort(items.begin(), items.end());
  std::reverse(items.begin(), items.end());
}


void testBB0() {
  int W = 0;
  std::vector<Item> store;
  readBBitems("items0", store, W);
  std::vector<int> bag = knapsack_backtracking(store, W);
  print_bag(bag, store, W);
}

void testBB1() {
  int W = 0;
  std::vector<Item> store;
  readBBitems("items1", store, W);
  std::vector<int> bag = knapsack_backtracking(store, W);
  print_bag(bag, store, W);
}

void testBB2() {
  int W = 0;
  std::vector<Item> store;
  readBBitems("items2", store, W);
  std::vector<int> bag = knapsack_backtracking(store, W);
  print_bag(bag, store, W);
}

void testBB3() {
  int W = 0;
  std::vector<Item> store;
  readBBitems("items3", store, W);
  std::vector<int> bag = knapsack_backtracking(store, W);
  print_bag(bag, store, W);
}

void testBB4() {
  int W = 0;
  std::vector<Item> store;
  readBBitems("items4", store, W);
  std::vector<int> bag = knapsack_backtracking(store, W);
  print_bag(bag, store, W);
}

void testBB5() {
  int W = 0;
  std::vector<Item> store;
  readBBitems("items0", store, W);
  std::vector<int> bag = knapsack_backtracking_branchbound(store, W);
  print_bag(bag, store, W);
}

void testBB6() {
  int W = 0;
  std::vector<Item> store;
  readBBitems("items1", store, W);
  std::vector<int> bag = knapsack_backtracking_branchbound(store, W);
  print_bag(bag, store, W);
}

void testBB7() {
  int W = 0;
  std::vector<Item> store;
  readBBitems("items2", store, W);
  std::vector<int> bag = knapsack_backtracking_branchbound(store, W);
  print_bag(bag, store, W);
}

void testBB8() {
  int W = 0;
  std::vector<Item> store;
  readBBitems("items3", store, W);
  std::vector<int> bag = knapsack_backtracking_branchbound(store, W);
  print_bag(bag, store, W);
}

void testBB9() {
  int W = 0;
  std::vector<Item> store;
  readBBitems("items4", store, W);
  std::vector<int> bag = knapsack_backtracking_branchbound(store, W);
  print_bag(bag, store, W);
}


void(*ptestBBs[])() = {
  testBB0, testBB1, testBB2, testBB3, testBB4, testBB5, testBB6, testBB7, testBB8, testBB9
};






int main(int argc, char ** argv) try {
    std::vector<Item> store;
    std::vector<int> bag;
    int W = 0;

    if (argc == 3) {
        readItems( argv[2], store, W );
        int algorithm = 0;
        std::sscanf(argv[1],"%i",&algorithm);
        print ( store );
        std::cout << std::endl << "=========================" << std::endl;
        
        if (algorithm == 1)
        {
          std::cout << "Using backtracking algorithm with items in \"" << argv[2] << "\"\n";
          bag = knapsack_backtracking(store, W);
        }
        else if (algorithm == 2)
        {
          std::cout << "Using backtracking with upper bound algorithm with items in \"" << argv[2] << "\"\n";
          bag = knapsack_backtracking_branchbound(store, W);
        }
        else if ( algorithm == 3 ) 
        {
            std::cout << "Using recursive with memoization with items in \"" << argv[2] << "\"\n";
            bag = knapsackRecMem( store, W );
        } 
        else 
        {
            std::cout << "Using dynamic programming with items in \"" << argv[2] << "\"\n";
            bag = knapsackDP( store, W );
        }
        print_bag( bag, store, W );
    } 
    else 
    {
        std::cout << "Usage: " << argv[0] << " <algorithm (1 - BT, 2 - BT with UB, 3 - recursive with memoization, 4 - dynamic programming)> <input file>\n";
        return 1;
    }

    print_bag( bag, store, W );
} catch( char const* str) {
    std::cout << str << std::endl;
}
