cmake_minimum_required(VERSION 3.17)

project("Closest_Pair")

set(header_files
  include/ClosestPair.h
)
set(source_files
  src/ClosestPair.cpp
  src/main.cpp
)

ADD_EXECUTABLE("${PROJECT_NAME}" 
               ${source_files} 
               ${header_files}
               )

target_include_directories("${PROJECT_NAME}" PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")
