#include "ClosestPair.h"
#include <algorithm>
#include <limits>
#include <cmath>
#include <iostream>
#include <utility>

std::ostream& operator<< (std::ostream& os, Point const& p) {
	os << "(" << p.x << " , " << p.y << ") ";
	return os;
}

std::istream& operator>> (std::istream& os, Point & p) {
	os >> p.x >> p.y;
	return os;
}

////////////////////////////////////////////////////////////////////////////////
float closestPair_aux(std::vector<unsigned> indices, std::vector<Point> const& points);
float Distance(const Point& a, const Point& b);
float Distance(const float& a, const float& b);
float SeperatePoints(std::vector<unsigned>const& indices, std::vector<Point> const& points,
  std::vector<unsigned>& left, std::vector<unsigned>& right, bool& madeVertical);

////////////////////////////////////////////////////////////////////////////////
float closestPair ( std::vector< Point > const& points ) {
	int size = (int)points.size();
  std::vector<unsigned> indices;
  indices.reserve(size);

  for (int i = 0; i < size; ++i)
    indices.push_back(i);

	//std::cerr << "closestPair_aux " << size << " points:";
	//for(int i=0;i<size;++i) { std::cerr << points[i] << " "; } std::cerr << std::endl;

	if (size==0) 
    throw "zero size subset";
	if (size==1) 
    return std::numeric_limits<float>::max();

  float min_dist = closestPair_aux( indices, points );
  return std::sqrt(min_dist);
}

float SeperatePoints(std::vector<unsigned>const& indices, std::vector<Point> const& points,
                    std::vector<unsigned>& left, std::vector<unsigned>& right, bool& madeVertical)
{
  float median = 0.0f;
  for (unsigned i = 0; i < indices.size(); ++i)
    median += points[indices[i]].x;

  median /= indices.size();

  for (unsigned i = 0; i < indices.size(); ++i)
  {
    if (points[indices[i]].x < median)
      left.push_back(indices[i]);
    else
      right.push_back(indices[i]);
  }
  // vertical set of points found, split horizontally
  if (left.empty() || right.empty())
  {
    left.clear();
    right.clear();// remove any indices that were placed in left or right
    madeVertical = true;
    median = 0.0f;
    for (unsigned i = 0; i < indices.size(); ++i)
      median += points[indices[i]].y;
    median /= indices.size();

    for (unsigned i = 0; i < indices.size(); ++i)
    {
      if (points[indices[i]].y < median)
        left.push_back(indices[i]);
      else
        right.push_back(indices[i]);
    }
  }
  return median;
}

float Distance(const float& a, const float& b)
{
  return ((b - a) * (b - a));
}
// returns the squared distance between two points
float Distance(const Point& a, const Point& b)
{
  return ((b.x - a.x) * (b.x - a.x)) + ((b.y - a.y) * (b.y - a.y));
}

////////////////////////////////////////////////////////////////////////////////
float closestPair_aux(std::vector<unsigned> indices, std::vector<Point> const& points) {
	int size = (int)indices.size();

	//std::cerr << "closestPair_aux ";
	//for(int i=0;i<size;++i) { std::cerr << points[ indices[i] ] << " "; } std::cerr << std::endl;

	if (size==0) 
    throw "zero size subset";
	if (size==1) 
    return std::numeric_limits<float>::max();

  // divide points into parts (left and right) with respect to vertical line
  std::vector<unsigned> left;
  std::vector<unsigned> right;

  bool madeVertical = false;
  float splitPt = SeperatePoints(indices, points, left, right, madeVertical);

  float d1 = closestPair_aux(left, points);
  float d2 = closestPair_aux(right, points);

  float min_dist = std::min(d1, d2);

  for (unsigned i = 0; i < left.size(); ++i)
  {
    // split pt will always be lower
    float closeLeft = madeVertical ? points[left[i]].y : points[left[i]].x;
    if (Distance(closeLeft, splitPt) >= min_dist)
      continue;
    Point p1 = points[left[i]];
    for (unsigned j = 0; j < right.size(); ++j)
    {
      float closeRight = madeVertical ? points[right[j]].y : points[right[j]].x;
      if (Distance(closeRight, splitPt) >= min_dist)
        continue;
      Point p2 = points[right[j]];
      float dist = Distance(p1, p2);
      if (dist < min_dist)
        min_dist = dist;
    }
  }
	return min_dist;
}

