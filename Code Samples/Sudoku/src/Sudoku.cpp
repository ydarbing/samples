/*************************************************************************
Sudoku.cpp
Brady Reuter
2/8/2015
*/
#include "Sudoku.h"


/*********************************************************************/
/*!
\brief
Construct the sudoku board
\param basesize
how big the board will be   i.e 3 will be a 9x9 grid
\param stype
to use numbers or letters, based off how big the board is
\param callback
Implemented in the client and called during the search for a solution
*/
/***********************************************************************/
Sudoku::Sudoku(int basesize, SymbolType stype, M_CALLBACK callback) :
m_stats(), m_symbol(stype), m_NumToFill(0),
m_minusFactor(m_symbol == SYM_NUMBER ? '1' : 'A'),
m_abort(false), m_callback(callback)
{
  m_rowLength = basesize * basesize;
  m_size = basesize * basesize * basesize * basesize; // 3*3*3*3 = 81 ect
  m_board = new char[m_size];
  m_onlyFillBoard = new int[m_size];// allocate this much in the case of empty board
  m_dupIndexes = new int[3];// can only ever be 3
  ResetDuplicateIndexes();
  m_stats.basesize = basesize;
}


/*********************************************************************/
/*!
\brief
destroys sudoku board
*/
/***********************************************************************/
Sudoku::~Sudoku()
{
  delete[] m_dupIndexes;
  delete[] m_onlyFillBoard;
  delete[] m_board;
}

/*********************************************************************/
/*!
\brief
The client (driver) passed the board in the values parameter
\param values
one-dimensional array of symbols
\param size
base size of board
*/
/***********************************************************************/
void Sudoku::SetupBoard(const char *values, size_t size)
{
  m_size = size;
  for (int i = 0; i < static_cast<int>(size); ++i)
  {
    if (values[i] != '.')
      m_board[i] = values[i];
    else
    {
      m_onlyFillBoard[m_NumToFill++] = i;
      m_board[i] = EMPTY_CHAR;
    }
  }
}

/*********************************************************************/
/*!
\brief
Once the board is setup, this will start the search for the solution
\return
true if board was solved
false if board was unsolved
*/
/***********************************************************************/
bool Sudoku::Solve()
{
  // Return true if a solution was found, otherwise, return false.
  m_callback(*this, m_board, MSG_STARTING, m_stats.moves, m_stats.basesize, 0, m_minusFactor, m_dupIndexes);
  PlaceValue(0);
  if (m_stats.placed == m_NumToFill && !m_abort)
    m_callback(*this, m_board, MSG_FINISHED_OK, m_stats.moves, m_stats.basesize, 0, 0, 0);
  else
    m_callback(*this, m_board, MSG_FINISHED_FAIL, m_stats.moves, m_stats.basesize, 0, 0, 0);


  return true;
}

/*********************************************************************/
/*!
\brief
recursive function that completes the puzzle
\param index
where on the board value is
*/
/***********************************************************************/
void Sudoku::PlaceValue(int index)
{
  if (m_callback(*this, 0, MSG_ABORT_CHECK, 0, 0, 0, 0, 0) ||
    m_stats.placed == m_NumToFill)
    return;

  char val = m_minusFactor;
  for (int i = 0; i < m_rowLength; ++i, ++val)
  {
    bool valid = ValidMove(val, m_onlyFillBoard[index]);
    // place value and update stats
    m_board[m_onlyFillBoard[index]] = val;
    ++m_stats.placed;
    m_callback(*this, m_board, MSG_PLACING, ++m_stats.moves, m_stats.basesize,
      m_onlyFillBoard[index], val, m_dupIndexes);
    if (valid)
    {
      PlaceValue(index + 1);
      if (m_stats.placed == m_NumToFill)
        return;
    }
    else
    {
      // remove value and update stats
      m_board[m_onlyFillBoard[index]] = EMPTY_CHAR;
      --m_stats.placed;
      m_callback(*this, m_board, MSG_REMOVING, m_stats.moves, m_stats.basesize,
        m_onlyFillBoard[index], val, m_dupIndexes);
      // need to backtrack because we are at the last symbol
      if ((val - m_minusFactor) >= (m_rowLength - 1))
      {
        Backtrack(index);
        return;
      }
    }
  }
}

/*********************************************************************/
/*!
\brief
Recursive call that backtracks to appropriate number
\param index
where on the onlyFillBoard
*/
/***********************************************************************/
void Sudoku::Backtrack(int index)
{
  ++m_stats.backtracks;
  --m_stats.placed;
  // need to place the preivously set value to the next possible value
  char prevVal = m_board[m_onlyFillBoard[index - 1]];
  m_board[m_onlyFillBoard[index - 1]] = EMPTY_CHAR;

  m_callback(*this, m_board, MSG_REMOVING, m_stats.moves, m_stats.basesize,
    m_onlyFillBoard[index - 1], prevVal, m_dupIndexes);

  if ((prevVal + 1) - m_minusFactor > (m_rowLength - 1))
    Backtrack(--index);
}

/*********************************************************************/
/*!
\brief
Checks all possible ways the current value could be invalid
\param value
current value (number or letter) being checked
\param index
where on the board value is
\return
true if valid move
false if invalid move
*/
/***********************************************************************/
bool Sudoku::ValidMove(char value, int index)
{
  ResetDuplicateIndexes();

  bool v1 = ValidRow(value, index);
  bool v2 = ValidColumn(value, index);
  bool v3 = ValidQuadrant(value, index);

  return (v1 && v2 && v3);
}

/*********************************************************************/
/*!
\brief
Checks if value is valid in current row
\param value
current value (number or letter) being checked
\param index
where on the board value is
\return
true if valid move
false if invalid move
*/
/***********************************************************************/
bool Sudoku::ValidRow(char value, int index)
{
  unsigned temp = index - (index % m_rowLength);// start at beginning of row
  unsigned stop = temp + m_rowLength;
  for (; temp != stop; ++temp)
  {
    if (m_board[temp] == value)
    {
      AddDuplicateIndex(temp);
      return false;
    }
  }

  return true;
}

/*********************************************************************/
/*!
\brief
Checks if value is valid in current column
\param value
current value (number or letter) being checked
\param index
where on the board value is
\return
true if valid move
false if invalid move
*/
/***********************************************************************/
bool Sudoku::ValidColumn(char value, int index)
{
  // start value is first row of that column
  unsigned temp = (index % m_rowLength);
  unsigned stop = static_cast<unsigned>(m_size)-m_rowLength + temp + 1;
  for (; temp < stop; temp += m_rowLength)
  {
    if (m_board[temp] == value)
    {
      AddDuplicateIndex(temp);
      return false;
    }
  }

  return true;
}

/*********************************************************************/
/*!
\brief
Checks if value is valid in current subsquare
\param value
current value (number or letter) being checked
\param index
where on the board value is
\return
true if valid move
false if invalid move
*/
/***********************************************************************/
bool Sudoku::ValidQuadrant(char value, int index)
{
  // figure out how many rows need to move down to be in first row of subsquare
  int getToCorrectRow = (index / m_rowLength) % m_stats.basesize;
  index -= (m_rowLength * getToCorrectRow);
  // move to the first element in subsquare
  int start = index - (index % m_stats.basesize);
  int stop = start + (m_rowLength * (m_stats.basesize - 1)) + m_stats.basesize;

  // go through each row and column
  for (; start < stop; start += m_rowLength)
  {
    for (unsigned j = start; j < start + m_stats.basesize; ++j)
    {
      if (m_board[j] == value)
      {
        AddDuplicateIndex(j);
        return false;
      }
    }
  }
  return true;
}

/*********************************************************************/
/*!
\brief
Found a duplicate, so try and put it in the array
\param index
where on the board the duplicate was found
*/
/***********************************************************************/
void Sudoku::AddDuplicateIndex(unsigned index)
{
  // quickly check if this index was already found
  for (int i = 0; i < 3; ++i)
    if (m_dupIndexes[i] == static_cast<int>(index))
      return;

  if (m_dupIndexCount != 0)
  {
    if (m_dupIndexCount == 1)
    {
      if (static_cast<int>(index) < m_dupIndexes[0])
      {
        m_dupIndexes[1] = m_dupIndexes[0];
        m_dupIndexes[0] = index;
        m_dupIndexCount++;
      }
      else
        m_dupIndexes[m_dupIndexCount++] = index;
    }
    else // found all 3 duplicates
    {
      if (static_cast<int>(index) < m_dupIndexes[0])
      {
        m_dupIndexes[2] = m_dupIndexes[1];
        m_dupIndexes[1] = m_dupIndexes[0];
        m_dupIndexes[0] = index;
        m_dupIndexCount++;
      }
      else if (static_cast<int>(index) < m_dupIndexes[1])
      {
        m_dupIndexes[2] = m_dupIndexes[1];
        m_dupIndexes[1] = index;
        m_dupIndexCount++;
      }
      else
        m_dupIndexes[m_dupIndexCount++] = index;
    }
  }
  else
    m_dupIndexes[m_dupIndexCount++] = index;
}

void Sudoku::ResetDuplicateIndexes(void)
{
  for (int i = 0; i < 3; ++i)
    m_dupIndexes[i] = -1;

  m_dupIndexCount = 0;
}

/*********************************************************************/
/*!
\brief
get gameboard
\return
pointer to beginning of board array
*/
/***********************************************************************/
const char* Sudoku::GetBoard() const
{
  return m_board;
}
/*********************************************************************/
/*!
\brief
get current stats on solving the board
\return
stats
*/
/***********************************************************************/
Sudoku::SudokuStats Sudoku::GetStats() const
{
  return m_stats;
}
