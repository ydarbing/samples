cmake_minimum_required(VERSION 3.17)

set(CMAKE_TOOLCHAIN_FILE "C:/vcpkgscripts/buildsystems/vcpkg.cmake")

project("Sudoku")

find_package(unofficial-sqlite3 CONFIG REQUIRED)

set(header_files
  include/Sudoku.h
)

set(source_files
  src/main.cpp
  src/Sudoku.cpp
  )
  

add_executable("${PROJECT_NAME}" 
               ${source_files}
               ${header_files}
               )

target_include_directories("${PROJECT_NAME}" PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include" ../_libs/GetOpt)


# Lib includes 
target_link_directories("${PROJECT_NAME}" PRIVATE ${_VCPKG_INSTALLED_DIR}/${VCPKG_TARGET_TRIPLET}/lib)
target_link_libraries("${PROJECT_NAME}" PRIVATE getopt )
