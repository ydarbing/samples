
#include <string.h> // strlen
#include <cstdlib> // calloc
#include <iostream> // cout

#pragma warning (disable : 4996)

char AddAsciiInt(const char x, const char y, char& carry)
{
  // convert from ascii to int to do addition
  char overflow = (x - '0') + (y - '0') + carry;

  if (overflow < 10)
  {
    // no overflow, just convert back to ascii
    carry = 0;
    return overflow + '0';
  }
  else
  {
    // overflow, subtract base(10) and keep track of extra
    carry = 1;
    return overflow - 10 + '0';
  }
}

char *AddAsciiIntegers(const char *x, const char *y)
{
  if (x == nullptr || y == nullptr)
  {
    return nullptr;
  }
  unsigned xLength = (unsigned)strlen(x);
  unsigned yLength = (unsigned)strlen(y);

  unsigned newLength = xLength;
  if (xLength < yLength)
  {
    newLength = yLength;
  }
  // + 1 in case of overflow and + 2 for null terminated string
  char *ret = new char[newLength + 2]();
  ret[0] = '0'; // this is overflow spot

  int i = xLength - 1;
  int j = yLength - 1;
  int k = newLength;
  char carry = 0;
  // go through all matching lengths
  for (; i >= 0 && j >= 0; --i, --j, --k)
  {
    ret[k] = AddAsciiInt(x[i], y[j], carry);
  }

  for (; i >= 0; --i, --k)
  {
    // x was a longer number
    ret[k] = AddAsciiInt(x[i], '0', carry);
  }
  for (; j >= 0; --j, --k)
  {
    // y was a longer number
    ret[k] = AddAsciiInt(y[j], '0', carry);
  }

  // one last carry check to see if we should use overflow spot
  if (carry == 1)
  {
    ret[0] = '1';
  }

  return ret;
}




int main(void)
{
  //char* added = AddAsciiIntegers(nullptr, "1");
  char* added = AddAsciiIntegers("1", "123");
  std::cout << added << "!" << std::endl;

  delete added;
  return 0;
}
