/*
  ALGraph.cpp
  Brady Reuter
  3/30/2015
*/ 
#include "AL/ALGraph.h"

/*********************************************************************/
 /*!
   \brief
     Constructor 
   \param size
     how big the table is
 */
 /***********************************************************************/
ALGraph::ALGraph(unsigned size)
{
  for(unsigned i = 0; i < size; ++i)
  {
    m_graph.push_back(AdjInfo());
    m_graph[i].node->id = i+1;
    m_graph[i].weight = std::numeric_limits<unsigned>::max();
    m_graph[i].cost   = 0;
    m_graph[i].node->visited = false;
  }
}

/*********************************************************************/
 /*!
   \brief
     Destructor, frees all allocated nodes
 */
 /***********************************************************************/
ALGraph::~ALGraph(void)
{
  // free all allocated nodes
  for(unsigned i = 0; i < m_graph.size(); ++i)
    delete m_graph[i].node;
}

/*********************************************************************/
 /*!
   \brief
     Add a directed edge to the graph
   \param source
     starting node
   \param destination
     node where edge will be connected
   \param weight
     weight of edge
 */
 /***********************************************************************/
void ALGraph::AddDEdge(unsigned source, unsigned destination, unsigned weight)
{
  GEdge newEdge;
  newEdge.destination = destination;
  newEdge.weight = weight;
  m_graph[source-1].node->adjList.insert(newEdge);
}
/*********************************************************************/
 /*!
   \brief
     Add a undirected edge to the graph
   \param node1
     starting node
   \param node2
     node where edge will be connected
   \param weight
     weight of edge
 */
 /***********************************************************************/
void ALGraph::AddUEdge(unsigned node1, unsigned node2, unsigned weight)
{
  AddDEdge(node1, node2, weight);
  AddDEdge(node2, node1, weight);
}

class DistCompare
{
public:
  bool operator()(DijkstraInfo& di1, DijkstraInfo& di2)
  {
    if(di1.cost > di2.cost)
      return true;
    else
      return false;
  }
};

/*********************************************************************/
 /*!
   \brief
     Gets lowest cost to all nodes starting from given node
   \param start_node
     node that is going to get lowest cost to all other nodes
   \return
     vector of shortest path to all nodes from start_node
 */
 /***********************************************************************/
std::vector<DijkstraInfo> ALGraph::Dijkstra(unsigned start_node) const
{
  std::vector<DijkstraInfo> ret;
  ret.resize(m_graph.size());
  ALIST paths;

  // Get starting node (sNode)
  // Initialize sNode to 0 cost and mark as evaluated.
  // Initialize all nodes to infinite cost from the source.
  GNode* source = 0;
  for(unsigned i = 0; i < m_graph.size(); ++i)
  {
    if(i != start_node - 1)
    {
      m_graph[i].cost = std::numeric_limits<unsigned>::max();
    }
    else
    {
      source = m_graph[i].node;
      source->visited = true;
      m_graph[i].cost = 0;
    }
  }

  for(unsigned i = 0; i < m_graph.size(); ++i)
    ret[i].path.push_back(start_node);

  std::greater<AdjInfo> cmp;
  std::priority_queue<AdjInfo, std::vector<AdjInfo>, std::greater<AdjInfo> > lowCost(cmp);

  // For each node, y, adjacent to source
  //   1. Relax the node. That is, set y's cost to the cost of all edges from source to y.
  //   2. Place y into a priority queue based on its total cost. (Lower is better)
  //   3. Add source node as predecessor of y.
  std::multiset<GEdge>::iterator adjIt = source->adjList.begin();
  for( ; adjIt != source->adjList.end(); ++adjIt)
  {
    //GNode* y = m_graph[adjIt->destination-1].node;
    ret[adjIt->destination-1].path.push_back(adjIt->destination);
    m_graph[adjIt->destination-1].cost = adjIt->weight;
    lowCost.push(m_graph[adjIt->destination-1]);
  }

  //While there are nodes in the graph that haven't been evaluated
    //Remove a node, x, from the PQ (lowest total cost)
    //Mark x as evaluated.
    //For each neighbor, y, of x
      //Relax y
      //If new cost to reach y is less
        //Update list of nodes (path) to y from source.
        //Place y in the PQ.
      //End If
    //End For
  //End While
  while(lowCost.empty() == false)
  {
    AdjInfo x = lowCost.top();
    lowCost.pop();
    if(x.node->visited == true)
      continue;
    x.node->visited = true;

    adjIt = x.node->adjList.begin();
    for(; adjIt != x.node->adjList.end(); ++adjIt)
    {
      //GNode* y = m_graph[adjIt->destination-1].node;
      unsigned temp = x.cost + adjIt->weight;
      if(temp < m_graph[adjIt->destination-1].cost)
      {
        int dest = adjIt->destination;
        ret[adjIt->destination-1].path.push_back(x.node->id);
        m_graph[adjIt->destination-1].cost = temp;
        lowCost.push(m_graph[adjIt->destination-1]);
      }
    }
  }

  // set final cost
  for(unsigned i = 0; i < m_graph.size(); ++i)
    ret[i].cost = m_graph[i].cost;

  return ret;
}


/*********************************************************************/
 /*!
   \brief
     returns ALIST to communicate with driver so it can draw graph
   \return 
     vector of nodes starting with lowest
 */
 /***********************************************************************/
ALIST ALGraph::GetAList(void) const
{
  ALIST alist;
  alist.resize(m_graph.size());
  for(unsigned i = 0; i < m_graph.size(); ++i)
  {
    GNode* node = m_graph[i].node;
    std::multiset<GEdge>::iterator it = node->adjList.begin();
    for(; it != node->adjList.end(); ++it)
    {
      AdjacencyInfo info;
      info.id = it->destination;
      info.weight = it->weight;
      alist[i].push_back(info);
    }
  }
  return alist;
}

/*********************************************************************/
 /*!
   \brief
     Used for priority queue
   \param rhs
     value being compared
   \return 
     true if less than
 */
 /***********************************************************************/
bool ALGraph::AdjInfo::operator<(const AdjInfo& rhs) const
{
  return cost < rhs.cost; 
}

/*********************************************************************/
 /*!
   \brief
     Used for priority queue
   \param rhs
     value being compared
   \return 
     true if greater than
 */
 /***********************************************************************/
bool ALGraph::AdjInfo::operator>(const AdjInfo& rhs) const
{
  return cost > rhs.cost; 
}

/*********************************************************************/
 /*!
   \brief
     Constructor - holds the actual node and list of adjacent nodes
 */
 /***********************************************************************/
ALGraph::AdjInfo::AdjInfo()
{
  node = new GNode;
}

/*********************************************************************/
 /*!
   \brief
     Used for multiset
   \param rhs
     value being compared
   \return 
     true if less than
 */
 /***********************************************************************/
bool ALGraph::GEdge::operator<(const GEdge& rhs) const
{
  if(weight == rhs.weight)
    return destination < rhs.destination;
  else
    return weight < rhs.weight;
}
/*********************************************************************/
 /*!
   \brief
     Used for multiset
   \param rhs
     value being compared
   \return 
     true if greater than
 */
 /***********************************************************************/
bool ALGraph::GEdge::operator>(const GEdge& rhs) const
{
  if(weight == rhs.weight)
    return destination > rhs.destination;
  else
    return weight > rhs.weight;
}
