/*
  ALGraph.h
  Brady Reuter
  3/30/2015
*/ 
#pragma once

#include <vector>
#include <queue>
#include <set>
#include <limits>
#include <functional>// greater

struct DijkstraInfo
{
  unsigned cost;
  std::vector<unsigned> path;
};

struct AdjacencyInfo
{
  unsigned id;
  unsigned weight;
};

typedef std::vector<std::vector<AdjacencyInfo> > ALIST;

class ALGraph
{
  public:
    enum TRAVERSAL_METHOD {DEPTH_FIRST, BREADTH_FIRST};

    ALGraph(unsigned size);
    ~ALGraph(void);
    void AddDEdge(unsigned source, unsigned destination, unsigned weight);
    void AddUEdge(unsigned node1, unsigned node2, unsigned weight);

    std::vector<DijkstraInfo> Dijkstra(unsigned start_node) const;
    ALIST GetAList(void) const;
        
  private:
    
    class GNode; 
    class GEdge;
    struct AdjInfo
    {
      GNode* node;
      unsigned weight;
      mutable unsigned cost;
      AdjInfo();
      bool operator<(const AdjInfo& rhs) const;
      bool operator>(const AdjInfo& rhs) const;
    };
    class GEdge
    {
    public:
      bool operator<(const GEdge& rhs) const;
      bool operator>(const GEdge& rhs) const;

      unsigned destination;
      unsigned weight;
    };
    class GNode
    {
    public:
      unsigned id;
      bool  visited;    // Visited flag
      std::multiset<GEdge> adjList;
    };

    std::vector<AdjInfo> m_graph;
};

