cmake_minimum_required(VERSION 3.17)

project("Data_Structure__Graph")

set(header_files
  include/AL/ALGraph.h
)
set(source_files
  src/AL/ALGraph.cpp
  src/main.cpp
)

ADD_EXECUTABLE("${PROJECT_NAME}" 
               ${source_files} 
               ${header_files}
               )


target_include_directories("${PROJECT_NAME}" PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")
