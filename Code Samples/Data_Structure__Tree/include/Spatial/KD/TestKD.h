#pragma once
#include "Spatial/KD/KDTree.h"
#include <random>

namespace Tree
{	
	void TestKD(void);
	
	void TestPoint();
	
	void TestConstructor();
	void TestCopyConstructor();
	void TestAssignment();
	void TestConstructorGivenPointList();
	void TestConstructorGivenPointListWithDuplicates();
	void TestInsert();
  void TestRemove();
	void TestFind();
	
	void TestNearestNeighbor();
	void TestKNearestNeighbors();
	
	
	template <unsigned N, class ElemType>
	void GetVectorOfPoints(std::vector<typename KDTree<N,ElemType>::KDNode>& vec, unsigned numPoints)
	{
	  double minPos = -100.0;
	  double maxPos = 100.0;
	  std::random_device rd;
	  std::mt19937 gen(rd());
	  std::uniform_real_distribution<> dis(minPos, maxPos);
	  
    vec.clear();
    vec.reserve(numPoints);
	  KDTree<N, ElemType>::KDNode newNode;
	  while (numPoints--)
	  {
	    for (unsigned i = 0; i < N; ++i)
	      newNode.split[i] = dis(gen);
	
	    ++newNode.data;
	
	    vec.push_back(newNode);
	  }
	}
}
