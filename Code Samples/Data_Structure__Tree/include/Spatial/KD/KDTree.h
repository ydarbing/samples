#pragma once
#include "ObjectAllocator.h"
#include <vector>
#include <stdexcept> // std::exception
#include <limits>// max double
#include <algorithm> // max
#include <assert.h>// static_assert
#include <type_traits> // enable_if
#include "Spatial/KD/BoundedPriorityQueue.h" // for KNN

namespace Tree
{
  namespace KDUTILS
  {
    const double EPSILON = 1.0e-5f;

    inline bool DoubleEqual(double a, double b, double maxDiff = EPSILON)
    {
      return (a <= b + maxDiff && a >= b - maxDiff);
    }

  }

  class KDTException : public std::exception
  {
  public:
    KDTException(int ErrCode, const std::string& Message) : error_code_(ErrCode), message_(Message) {};

    virtual int code(void) const {
      return error_code_;
    }
    virtual const char *what(void) const throw() {
      return message_.c_str();
    }
    virtual ~KDTException() throw() {}
    enum KDT_EXCEPTION { E_DUPLICATE, E_NO_MEMORY, E_OUT_OF_RANGE };

  private:
    int error_code_;
    std::string message_;
  };


  template <unsigned dimension>//, typename T = float>
  class Point
  {
  public:
    Point() {};
    //template <typename... Args>
    //Point(Args... args)
    //  : pos{ args... }
    //{
    //  static_assert(sizeof...(Args) == dimension, "wrong number of arguments");
    //}

    template <typename... Tail>
    Point(typename std::enable_if<sizeof...(Tail)+1 == dimension, double>::type head, Tail... tail)
      : pos{ head, tail... } { };


  public:
    double pos[dimension];

    double SqDistance(const Point<dimension>& rhs)
    {
      double sqSum = 0;

      for (unsigned i = 0; i < dimension; ++i)
      {
        double distVec = pos[i] - rhs.pos[i];
        sqSum += distVec * distVec;
      }

      return sqSum;
    }

    double& operator[](int const i) { return pos[i]; }
    double const& operator[](int const i) const { return pos[i]; }

    bool operator==(const Point& rhs)const
    {
      for (unsigned i = 0; i < dimension; ++i)
        if (fabs(pos[i] - rhs.pos[i]) > KDUTILS::EPSILON)
          return false;

      return true;
    }
    bool operator!=(const Point& rhs) const { return !(*this == rhs); }
    bool operator<(const Point& rhs)  const { return memcmp(pos, rhs.pos, sizeof(double) * dimension) < 0; }
    bool operator<=(const Point& rhs) const { return memcmp(pos, rhs.pos, sizeof(double) * dimension) <= 0; }
    bool operator>(const Point& rhs)  const { return memcmp(pos, rhs.pos, sizeof(double) * dimension) > 0; }
    bool operator>=(const Point& rhs) const { return memcmp(pos, rhs.pos, sizeof(double) * dimension) >= 0; }


    friend std::ostream& operator<<(std::ostream &os, const Point& p)
    {

      os << "(";

      for (unsigned i = 0; i < dimension - 1; ++i)
        os << p[i] << ", ";

      os << p[dimension - 1] << ")";

      return os;
    }
  };


  //template <class Data, class Point = Point<2, double> >
  template <unsigned N, class ElemType>
  class KDTree
  {
  public:
    struct KDNode
    {
      Point<N> split;
      ElemType data;
      int axis;
      int count;// number of children
      KDNode* left;
      KDNode* right;

      KDNode() : axis(0), split(), data(), count(0), left(nullptr), right(nullptr) {};
      KDNode(const Point<N>& s, const ElemType& d, int _axis = 0, int _count = 0) :
        axis(_axis), split(s), data(d), count(_count), left(nullptr), right(nullptr) {};
    };
    typedef KDNode* NodePtr;
    typedef BoundedPQueue<NodePtr> NodeBPQ;

  private:
    typedef std::vector<KDNode> NodeVector;
    typedef std::vector<NodeVector> DimensionVectors;

  public:
    KDTree(ObjectAllocator* OA = nullptr, bool ShareOA = false);
    KDTree(std::vector<KDNode>& nodes, ObjectAllocator* OA = nullptr, bool ShareOA = false);
    virtual ~KDTree();
    KDTree(const KDTree& rhs);
    KDTree& operator= (const KDTree& other);

    ElemType& operator[] (const Point<N>& pt); // will insert if point is not in the tree
    ElemType& At(const int index)throw(KDTException);
    ElemType& At(const Point<N>& pt)throw(KDTException);
    const ElemType& At(const Point<N>& pt) const throw(KDTException);

    ElemType NNValue(const Point<N>& pt) const;
    // k is the number of closest neighbors to find
    NodeBPQ KNNValue(const Point<N>& pt, unsigned k) const;
    void KNNValue(const Point<N>& pt, NodeBPQ& result) const;


    virtual void Insert(const Point<N>& pt, const ElemType& value) throw(KDTException);
    virtual void Insert(const KDNode& node) throw(KDTException);
    virtual NodePtr Remove(const Point<N>& pt);

    void Clear(void);
    bool Find(const Point<N>& pt) const;
    bool Find(const Point<N>& pt, unsigned& compares) const;

    NodePtr GetRoot(void)const;
    bool Empty() const;
    unsigned GetSize() const;
    unsigned GetDimension() const;
    int GetHeight(void)const;
    // find minimum of root's dimension
    NodePtr FindMin(NodePtr root, int axis);

  private:

    void MakeOwnAllocator(void);
    NodePtr CopyTree(NodePtr other);
    bool FindKey(const NodePtr& curNode, const Point<N>& key, unsigned &compares)const;
    void InsertNode(NodePtr& curNode, const Point<N>& pt, const ElemType& value, int level)throw(KDTException);
    NodePtr MakeNode(const Point<N>& pt, const ElemType& value, int axis, int count = 0);
    NodePtr RemoveNode(NodePtr& curNode, const Point<N>& pt, int level);
    void DeleteNode(NodePtr node);
    void ClearTree(NodePtr& curNode);

    int TreeHeight(NodePtr curNode) const;
    void FindPredecessor(NodePtr curNode, NodePtr& predecessor) const;

    NodePtr SubscriptHelper(NodePtr tree, const int index);
    NodePtr SubscriptHelper(NodePtr& tree, const Point<N>& key, int level, bool insert);

    void NearestNeighbor(NodePtr curNode, const Point<N>& testPt, NodePtr& guess, double& bestDist) const;

    void KNearestNeighbor(NodePtr curNode, const Point<N>& testPt, NodeBPQ& result) const;

    double SqDistance(const Point<N>& a, const Point<N>& b) const;

    NodePtr BuildTree(DimensionVectors& dimensionsSorted, NodeVector& temp, long start, long end, long depth);
    void MergeSort(std::vector<KDNode>& toSort, std::vector<KDNode>& sorted, long low, long high, unsigned sortAxis);
    double CompareKeys(const Point<N>& a, const Point<N>& b, const unsigned startAxis);
    unsigned RemoveDuplicates(std::vector<KDNode>& nodes, const unsigned startAxis);
    void MakeFromList(std::vector<KDNode>& nodes);
    // copy p2 into p1
    //void CopyPoint(Point<N>& p1, Point<N> p2);
    // copy p2 into p1
    void CopyPoint(NodePtr& n1, NodePtr& n2);
    // find minimum of root's dimension
    NodePtr FindMin(NodePtr root, int axis, unsigned depth);

    NodePtr FindMinNode(NodePtr a, NodePtr b, NodePtr c, int axis);

  private:
    unsigned m_size;
    NodePtr  m_root;
    ObjectAllocator* m_allocator;
    bool     m_shareOA;
    bool     m_freeOA;
  };


}

#include "KDTree.hpp"
