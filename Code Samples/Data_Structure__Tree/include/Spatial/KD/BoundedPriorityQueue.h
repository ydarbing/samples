/**************************************************************
* BoundedPQueue<int> bpq(10); // Holds up to ten values
*
* After pushing the element, there is no guarantee
* that the value will actually be in the queue.  If the queue
* is full and the new element's priority exceeds the largest
* priority in the container, it will not be added.
*
* You can dequeue elements from a bounded priority queue using
* the popMin() function
*/
#pragma once

#include <algorithm>
#include <map>        // multimap
#include <limits>     // numeric_limits
#include <functional> // less

//Comparator will initally compare keys
template <typename T, typename Comparator = std::less<double> >
class BoundedPQueue 
{
public:
  explicit BoundedPQueue(unsigned maxSize, Comparator comp = Comparator());

  // If this overflows the maximum size of the queue, highest priority element 
  // will be deleted from queue 
  void push(const T& value, double priority);


  const T* top(void);
  void pop(void);
  // combines top and pop into one call 
  //T dequeue(void);

  unsigned size(void) const;
  bool empty(void) const;
  bool full(void)const;

  unsigned maxSize() const;

  // returns smallest priority of an element
  // i.e. priority of the element that will be popped first using popMin
  // returns numeric_limits<double>::infinity() if empty
  double best() const;
  // returns highest priority of an element
  // returns numeric_limits<double>::infinity() if empty
  double worst() const;

  // returns smallest priority element
  // i.e. priority of the element that will be popped first using popMin
  const T* best_data() const;
  // returns highest priority element
  const T* worst_data() const;


private: 
  typedef typename std::multimap<double, T, Comparator> MMap;
  typedef typename MMap::iterator Iter;
  MMap elems;
  unsigned maximumSize;
};

#include "Spatial/KD/BoundedPriorityQueue.hpp"

