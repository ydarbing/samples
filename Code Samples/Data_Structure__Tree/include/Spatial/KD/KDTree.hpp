#include "KDTree.h"
/*
KDTree.hpp
Brady Reuter
09/29/2016
*/
namespace Tree
{

  /*********************************************************************/
  /*!
  \brief
  Create a new tree and choose whether or not to make a allocator
  specifically for this one
  \param OA
  the allocator that will be used to create new nodes
  \param ShareOA
  tells copy and assignment operator if they should use the allocator
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  KDTree<N, ElemType>::KDTree(ObjectAllocator* OA /*= nullptr*/, bool ShareOA /*= false*/) :
    m_root(nullptr), m_allocator(OA), m_shareOA(ShareOA), m_freeOA(false), m_size(0)
  {
    if (!m_allocator)
      MakeOwnAllocator();
  }

  /*********************************************************************/
  /*!
  \brief
  Create a new tree and choose whether or not to make a allocator
  specifically for this one
  \param OA
  the allocator that will be used to create new nodes
  \param ShareOA
  tells copy and assignment operator if they should use the allocator
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  KDTree<N, ElemType>::KDTree(std::vector<KDNode>& nodes, ObjectAllocator* OA /*= nullptr*/, bool ShareOA /*= false*/)
    : m_root(nullptr), m_allocator(OA), m_shareOA(ShareOA), m_freeOA(false), m_size(0)
  {
    if (!m_allocator)
      MakeOwnAllocator();
    MakeFromList(nodes);
  }

  /*********************************************************************/
  /*!
  \brief
  creates a new allocator that uses the c++ new and delete
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  void KDTree<N, ElemType>::MakeOwnAllocator(void)
  {
    m_allocator = new ObjectAllocator(sizeof(KDTree<N, ElemType>::KDNode), OAConfig(true));
    m_freeOA = true; // we own the allocator so we will free it
    m_shareOA = false; // don't share this allocator with anything else
  }

  /*********************************************************************/
  /*!
  \brief
  destructor deletes all tree nodes and if it should delete allocator
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  KDTree<N, ElemType>::~KDTree()
  {
    Clear();

    if (m_freeOA)
    {
      delete m_allocator;
      m_allocator = nullptr;
    }
  }

  /*********************************************************************/
  /*!
  \brief
  Copy constructor
  \param rhs
  the tree that will be deep copied into this new one
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  KDTree<N, ElemType>::KDTree(const KDTree& rhs) : m_root(nullptr), m_size(0)
  {
    // If we are to use rhs' allocator
    if (rhs.m_shareOA)
    {
      m_allocator = rhs.m_allocator; // Use rhs' allocator
      m_shareOA = true; // If a copy of 'this object' is made
      m_freeOA = false; // don't own it (won't free it)
    }
    else // No sharing, create own personal allocator
      MakeOwnAllocator();

    m_root = CopyTree(rhs.GetRoot());
  }

  /*********************************************************************/
  /*!
  \brief
  Assignment operator
  \param rhs
  tree that will be deep copied into this new tree
  \return
  the new tree
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  KDTree<N, ElemType>& KDTree<N, ElemType>::operator=(const KDTree<N, ElemType>& rhs)
  {
    if (this != &rhs)
    {
      if (rhs.m_shareOA)
      {
        m_allocator = rhs.m_allocator;
        m_shareOA = true;
        m_freeOA = false;
      }
      else
      {
        if (!m_allocator)
          MakeOwnAllocator();
      }

      Clear();// clear this tree so rhs can be copied into it
      m_tree = CopyTree(rhs.root());
    }

    return *this;
  }


  /*********************************************************************/
  /*!
  \brief
  Recursively copies one tree and returns the root of the newly copied tree
  \param other
  the tree that is being copied
  \return
  the head of the newly copied tree
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  typename KDTree<N, ElemType>::KDNode* KDTree<N, ElemType>::CopyTree(NodePtr other)
  {
    if (other == nullptr)
      return nullptr;
    // make new node and copy all other data into it
    NodePtr newTree = MakeNode(other->split, other->data, other->axis);
    newTree->count = other->count;
    // recursive call to make rest of tree
    newTree->left = CopyTree(other->left);
    newTree->right = CopyTree(other->right);

    return newTree;
  }

  template <unsigned N, typename ElemType>
  void KDTree<N, ElemType>::Clear(void)
  {
    ClearTree(m_root);
    m_size = 0;
  }

  template <unsigned N, typename ElemType>
  void KDTree<N, ElemType>::ClearTree(NodePtr& tree)
  {
    if (tree != nullptr)
    {
      ClearTree(tree->left);
      ClearTree(tree->right);
      NodePtr del = tree;
      DeleteNode(del);
      tree = nullptr;
    }
  }

  template <unsigned N, typename ElemType>
  void KDTree<N, ElemType>::DeleteNode(NodePtr node)
  {
    // have to call destructor explicitly because we used placement new
    node->~KDNode();
    m_allocator->Free(node);
    --m_size;
  }

  /*********************************************************************/
  /*!
  \brief
  Makes a node from the space given back from the allocator
  \param pt
  the split point that will go into new node
  \param value
  the data that will go into new node
  \param axis
  split axis of the new node
  \return
  newly created node
  \exception KDTExcetion
  exception thrown if there is no more memory
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  typename KDTree<N, ElemType>::KDNode* KDTree<N, ElemType>::MakeNode(const Point<N>& pt, const ElemType& value, int axis, int count)
  {
    // call placement new to put new node in already allocated memory
    NodePtr node = nullptr;
    try {
      NodePtr memLocation = reinterpret_cast<NodePtr>(m_allocator->Allocate());
      node = new(memLocation)KDNode(pt, value, axis, count);
      ++m_size;
    }
    catch (const OAException& e)
    {
      throw (KDTException(KDTException::E_NO_MEMORY, e.what()));
    }

    return node;
  }

  /*********************************************************************/
  /*!
  \brief
  Insert an item into tree
  \param pt
  The point (key) being added to tree
  \param value
  The data that is being put into the tree
  \exception KDTExcetion
  exception thrown if there is no more memory or a duplicate key was
  inserted
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  void KDTree<N, ElemType>::Insert(const Point<N>& pt, const ElemType& value)throw(KDTException)
  {
    InsertNode(m_root, pt, value, 0);
  }

  /*********************************************************************/
  /*!
  \brief
  Insert an item into tree
  \param node
  The point (key) and data being added to tree
  \exception KDTExcetion
  exception thrown if there is no more memory or a duplicate key was
  inserted
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  void KDTree<N, ElemType>::Insert(const KDNode& node)throw(KDTException)
  {
    InsertNode(m_root, node.split, node.data, 0);
  }

  /*********************************************************************/
  /*!
  \brief
  Finds where to put data in tree (recursive)
  \param curNode
  \param pt
  The point (key) being added to tree
  current node on the tree we are comparing with value
  \param value
  data being put into tree
  \exception KDTExcetion
  exception thrown if there is no more memory
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  void KDTree<N, ElemType>::InsertNode(NodePtr& curNode, const Point<N>& pt, const ElemType& value, int level)  throw(KDTException)
  {
    int axis = level % N;
    if (curNode == nullptr)
      curNode = MakeNode(pt, value, axis);
    else if (pt[axis] < curNode->split[axis])
      InsertNode(curNode->left, pt, value, level + 1);
    else if (pt[axis] > curNode->split[axis])
      InsertNode(curNode->right, pt, value, level + 1);
    else
      throw (KDTException(KDTException::E_DUPLICATE, "Tried to insert key twice"));

    ++curNode->count;
  }

  template <unsigned N, typename ElemType>
  void KDTree<N, ElemType>::CopyPoint(NodePtr& n1, NodePtr& n2)
  {
    //for (unsigned i = 0; i < N; ++i)
    //  p1[i] = p2[i];
    n1->split = n2->split;
    n1->data = n2->data;
  }

  /*********************************************************************/
  /*!
  \brief
  Remove key from tree
  \param pt
  The point (key) and data being added to tree
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  typename KDTree<N, ElemType>::KDNode* KDTree<N, ElemType>::Remove(const Point<N>& pt)
  {
    return RemoveNode(m_root, pt, 0);
  }

  /*********************************************************************/
  /*!
  \brief
  Recursive function to find and remove key from this tree
  \param node
  The point (key) and data being added to tree
  \exception BSTExcetion
  exception thrown if there is no more memory or a duplicate key was
  inserted
  \return returns the root of modified tree
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  typename KDTree<N, ElemType>::KDNode* KDTree<N, ElemType>::RemoveNode(NodePtr& curNode, const Point<N>& pt, int depth)
  {
    int axis = depth % N;

    if (curNode == nullptr)
      return nullptr;

    if (curNode->split == pt)
    {
      if (curNode->right != nullptr)
      {
        NodePtr min = FindMin(curNode->right, axis, axis + 1);
        CopyPoint(curNode, min);
        curNode->right = RemoveNode(curNode->right, min->split, axis + 1);
        assert(curNode->count != 0);
        --curNode->count;
      }
      else if (curNode->left != nullptr)
      {
        NodePtr min = FindMin(curNode->left, axis, depth + 1);
        CopyPoint(curNode, min);
        curNode->right = RemoveNode(curNode->left, min->split, axis + 1);
        curNode->left = nullptr;
        assert(curNode->count != 0);
        --curNode->count;
      }
      else// node to be deleted is a leaf node
      {
        DeleteNode(curNode);
        curNode = nullptr;
      }
    }
    // if curNode does not contain point, search down
    else if (pt[axis] < curNode->split[axis])
    {
      curNode->left = RemoveNode(curNode->left, pt, axis + 1);
      assert(curNode->count != 0);
      --curNode->count;
    }
    else
    {
      curNode->right = RemoveNode(curNode->right, pt, axis + 1);
      assert(curNode->count != 0);
      --curNode->count;
    }

    return curNode;
  }

  /*********************************************************************/
  /*!
  \brief
  Public function to find the minimum node in tree from given axis
  \param root
    start at this node and find minimum in this tree
  \param axis
    what axis are we trying to find the minimum for
  \return returns the minimum node found
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  typename KDTree<N, ElemType>::KDNode* KDTree<N, ElemType>::FindMin(NodePtr root, int axis)
  {
    return FindMin(root, axis, 0);
  }

  /*********************************************************************/
  /*!
  \brief
    Public function to find the minimum node in tree from given axis
  \param root
    start at this node and find minimum in this tree
  \param axis
    what axis are we trying to find the minimum for
  \param depth
    used to find out the current axis
  \return returns the minimum node found
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  typename KDTree<N, ElemType>::KDNode* KDTree<N, ElemType>::FindMin(NodePtr curNode, int axis, unsigned depth)
  {
    if (curNode == nullptr)
      return nullptr;

    unsigned curAxis = depth % N;

    if (curAxis == axis)
    {
      if (curNode->left == nullptr)
        return curNode;
      return FindMin(curNode->left, axis, depth + 1);
    }

    return FindMinNode(curNode, 
                       FindMin(curNode->left, axis, depth + 1), 
                       FindMin(curNode->right, axis, depth +1), axis);
  }


  /*********************************************************************/
  /*!
  \brief
  Used to compare node and children to find minimum axis value
  \param a
    node to find min
  \param b
    node to find min
  \param c
    node to find min
  \param axis
    what axis are we trying to find the minimum for
  \return 
      returns the minimum node found
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  typename KDTree<N, ElemType>::KDNode* KDTree<N, ElemType>::FindMinNode(NodePtr a, NodePtr b, NodePtr c, int axis)
  {
    NodePtr ret = a;

    if (b != nullptr && b->split[axis] < ret->split[axis])
      ret = b;
    if (c != nullptr && c->split[axis] < ret->split[axis])
      ret = c;

    return ret;
  }

  /*********************************************************************/
  /*!
  \brief
  Test to see if a point (key) is in this tree
  \param key
  The point (key) and data being found to tree
  \return
  True if key was found, false otherwise
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  bool KDTree<N, ElemType>::Find(const Point<N>& key) const
  {
    unsigned unused = 0;
    return FindKey(m_root, key, unused);
  }


  /*********************************************************************/
  /*!
  \brief
  Test to see if a point (key) is in this tree
  \param key
  The point (key) and data being found to tree
  \param compares
  See how many compares were needed to find key
  \return
  True if key was found, false otherwise
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  bool KDTree<N, ElemType>::Find(const Point<N>& key, unsigned& compares) const
  {
    return FindKey(m_root, key, compares);
  }

  /*********************************************************************/
  /*!
  \brief
  Recursive function to find a key in this tree
  \param curNode
  Current node being used to check if it matches the key
  \param key
  The point (key) and data being found to tree
  \param compares
  See how many compares were needed to find key
  \return
  True if key was found, false otherwise
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  bool KDTree<N, ElemType>::FindKey(const NodePtr& curNode, const Point<N>& key, unsigned &compares) const
  {
    ++compares;
    if (curNode == nullptr)
      return false;
    else if (key[curNode->axis] == curNode->split[curNode->axis])
      return true;
    else if (key[curNode->axis] < curNode->split[curNode->axis])
      return FindKey(curNode->left, key, compares);
    else
      return FindKey(curNode->right, key, compares);
  }

  /*********************************************************************/
  /*!
  \brief
  Tells is this tree has no nodes in it.
  \return
  True if this tree is empty, false otherwise
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  bool KDTree<N, ElemType>::Empty() const
  {
    return m_size == 0;
  }

  /*********************************************************************/
  /*!
  \brief
  Get the size (number of nodes) of this tree
  \return
  Number of nodes in this tree
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  unsigned KDTree<N, ElemType>::GetSize() const
  {
    return m_size;
  }

  /*********************************************************************/
  /*!
  \brief
  Get the dimension of point used in this tree.
  \return
  Dimension
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  unsigned KDTree<N, ElemType>::GetDimension() const
  {
    return N;
  }

  /*********************************************************************/
  /*!
  \brief
  Get the maximum height of this tree.
  \return
  Height of tree.  Zero if empty.
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  int KDTree<N, ElemType>::GetHeight(void) const
  {
    return TreeHeight(m_root);
  }

  /*********************************************************************/
  /*!
  \brief
  Recursive function that finds the height of this tree.
  \return
  Height of tree.  Zero if empty.
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  int KDTree<N, ElemType>::TreeHeight(NodePtr curNode) const
  {
    if (curNode == nullptr)
      return 0;
    else
      return (1 + std::max(TreeHeight(curNode->left), TreeHeight(curNode->right)));
  }

  /*********************************************************************/
  /*!
  \brief
  Get the root of this tree.
  \return
  Node pointer of this tree's root.  Nullptr if empty.
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  typename KDTree<N, ElemType>::KDNode* KDTree<N, ElemType>::GetRoot(void) const
  {
    return m_root;
  }

  /*********************************************************************/
  /*!
  \brief
  Finds the tree's right most left child.
  \param tree
  Node in which to find the predecessor of.
  \param predecessor
  Reference to the "tree's" predecessor node
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  void KDTree<N, ElemType>::FindPredecessor(NodePtr tree, NodePtr& predecessor) const
  {
    predecessor = tree->left;
    while (predecessor->right != nullptr)
      predecessor = predecessor->right;
  }

  /*********************************************************************/
  /*!
  \brief
  Subscript operator will find the "pt" argument and if "pt" is not
  in tree, it will be inserted.
  \param pt
  Key to be found/inserted in the tree
  \return
  Reference to the data of the key being found/inserted.
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  ElemType& KDTree<N, ElemType>::operator[](const Point<N>& pt)
  {
    NodePtr node = SubscriptHelper(m_root, pt, 0, true);

    return node->data;
  }

  /*********************************************************************/
  /*!
  \brief
  Const version to find the "pt" argument
  \param pt
  Key to be found in the tree
  \return
  Reference to the data of the key being found
  \exception KDTExcetion
  exception thrown if "pt" is not in tree
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  const ElemType& KDTree<N, ElemType>::At(const Point<N>& pt) const throw(KDTException)
  {
    NodePtr guess = nullptr;
    double bestDist = std::numeric_limits<double>::max();
    NearestNeighbor(m_root, pt, guess, bestDist);

    if (KDUTILS::DoubleEqual(bestDist, 0.0))
      return guess->data;
    else
      throw (KDTException(KDTException::E_OUT_OF_RANGE, "Point not in tree."));
  }

  /*********************************************************************/
  /*!
  \brief
  Finds the "pt" argument
  \param pt
  Key to be found in the tree
  \return
  Reference to the data of the key being found
  \exception KDTExcetion
  exception thrown if "pt" is not in tree
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  ElemType& KDTree<N, ElemType>::At(const Point<N>& pt) throw(KDTException)
  {
    NodePtr guess = nullptr;
    double bestDist = std::numeric_limits<double>::max();
    NearestNeighbor(m_root, pt, guess, bestDist);

    if (KDUTILS::DoubleEqual(bestDist, 0.0))
      return guess->data;
    else
      throw (KDTException(KDTException::E_OUT_OF_RANGE, "Point not in tree."));
  }

  /*********************************************************************/
  /*!
  \brief
  Finds the index in tree
  \param index
  Index bieng found in tree
  \return
  Reference to the data of the key being found
  \exception KDTExcetion
  exception thrown if index is out of range
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  ElemType& KDTree<N, ElemType>::At(const int index) throw(KDTException)
  {
    if (index < 0 || m_size < index)
      throw (KDTException(KDTException::E_OUT_OF_RANGE, "Invalid Index.  Out of range."));

    NodePtr node = SubscriptHelper(m_root, index);

    return node->data;
  }
  /*********************************************************************/
  /*!
  \brief
  Helper function to find a key in the tree
  \param tree
  Current node being compared to key
  \param key
  Key trying to be found in this tree
  \param level
  Current level of tree parameter
  \param insert
  Boolean that if key is not found to insert key into tree
  \return
  Pointer to the found node
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  typename KDTree<N, ElemType>::KDNode* KDTree<N, ElemType>::SubscriptHelper(NodePtr& tree, const Point<N>& key, int level, bool insert)
  {
    if (tree == nullptr)
    {
      if (insert)
        tree = MakeNode(key, ElemType(), level % N);
      else
        return nullptr;
    }

    int axis = tree->axis;

    if (key[axis] < tree->split[axis])
      return SubscriptHelper(tree->left, key, level + 1, insert);
    else if (key[axis] > tree->split[axis])
      return SubscriptHelper(tree->right, key, level + 1, insert);
    else
      return tree;
  }


  /*********************************************************************/
  /*!
  \brief
  Helper function to find an index in the tree
  \param tree
  Current node being compared to key
  \param index
  Index trying to be found in this tree
  \return
  Pointer to the found node
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  typename KDTree<N, ElemType>::KDNode* KDTree<N, ElemType>::SubscriptHelper(NodePtr tree, const int index)
  {
    if (tree == nullptr)
      return nullptr;

    int check = 0;
    if (tree->left != nullptr)
      check = tree->left->count;

    if (check > index)
      return SubscriptHelper(tree->left, index);
    else if (check < index)
      return SubscriptHelper(tree->right, index - check - 1);
    else // this is the correct index;
      return tree;
  }


  /*********************************************************************/
  /*!
  \brief
  Find the nearest key to the given pt
  \param pt
  Point that is part or is not part of this tree that will be used
  to find the keys that are closest to it
  \return
  Data of nearest point
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  ElemType KDTree<N, ElemType>::NNValue(const Point<N>& pt) const
  {
    NodePtr guess = nullptr;
    double bestDist = std::numeric_limits<double>::max();

    NearestNeighbor(m_root, pt, guess, bestDist);

    return guess->data;
  }

  /*********************************************************************/
  /*!
  \brief
  Find the K nearest keys to the given pt
  \param pt
  Point that is part or is not part of this tree that will be used
  to find the keys that are closest to it
  \param k
  Number of keys (neighbors) in tree that are clostest to the given "pt"
  \return
  Bounded Priority Queue of k closest nodes
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  typename KDTree<N, ElemType>::NodeBPQ KDTree<N, ElemType>::KNNValue(const Point<N>& pt, unsigned k) const
  {
    NodeBPQ bpq(k);

    KNearestNeighbor(m_root, pt, bpq);

    return bpq;
  }

  /*********************************************************************/
  /*!
  \brief
  Find the K nearest keys to the given pt
  \param pt
  Point that is part or is not part of this tree that will be used
  to find the keys that are closest to it
  \param result
  Reference to resulting Bounded Priority Queue of k closest nodes
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  void KDTree<N, ElemType>::KNNValue(const Point<N>& pt, NodeBPQ& result) const
  {
    KNearestNeighbor(m_root, pt, result);
  }


  /*********************************************************************/
  /*!
  \brief
  Calculates the squared distance between the two given points
  \param a
  Start Point
  \param b
  End point
  \return
  Squared distance between the two keys (points)
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  double KDTree<N, ElemType>::SqDistance(const Point<N>& a, const Point<N>& b) const
  {
    double sqSum = 0;

    for (unsigned i = 0; i < N; ++i)
    {
      double distVec = a[i] - b[i];
      sqSum += distVec * distVec;
    }

    return sqSum;
  }


  /*********************************************************************/
  /*!
  \brief
  Finds the nearest key in tree to the given test point
  \param curNode
  Current node being searched and compared to the test point
  \param testPt
  Reference to the point in which we are finding closest key in tree
  \param guess
  Reference to the best guess (closest key to testPt so far)
  \param bestDist
  Reference to the shortest distance from test point to clostest key
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  void KDTree<N, ElemType>::NearestNeighbor(NodePtr curNode, const Point<N>& testPt, NodePtr& guess, double& bestDist) const
  {
    if (!curNode)
      return;

    double dist = SqDistance(curNode->split, testPt);
    double distAxis = curNode->split[curNode->axis] - testPt.pos[curNode->axis];
    double distAxisSq = distAxis * distAxis;

    if (!guess || dist < bestDist)
    {
      guess = curNode;
      bestDist = dist;
    }

    if (bestDist == 0)
      return; // exact match

    // search half of tree that contains test point
    NearestNeighbor(distAxis > 0 ? curNode->left : curNode->right, testPt, guess, bestDist);

    // test if candidate hypersphere crosses splitting plane
    if (distAxisSq >= bestDist)
      return;

    // look on other side of plane by seeing other subtree
    NearestNeighbor(distAxis > 0 ? curNode->right : curNode->left, testPt, guess, bestDist);
  }

  /*********************************************************************/
  /*!
  \brief
  Finds the K nearest kesy in tree to the given test point
  \param curNode
  Current node being searched and compared to the test point
  \param testPt
  Reference to the point in which we are finding closest keys in tree
  \param result
  Reference to Bounded priority queue of best guesses
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  void KDTree<N, ElemType>::KNearestNeighbor(NodePtr curNode, const Point<N>& testPt, NodeBPQ& result) const
  {
    if (!curNode)
      return;

    double dist = SqDistance(curNode->split, testPt);
    double distAxis = curNode->split[curNode->axis] - testPt.pos[curNode->axis];
    double distAxisSq = distAxis * distAxis;

    result.push(curNode, dist);

    // search half of tree that contains test point
    KNearestNeighbor(distAxis > 0 ? curNode->left : curNode->right, testPt, result);

    // test if candidate hypersphere crosses splitting plane
    if (!result.full() || distAxisSq <= result.worst())
    {
      // look on other side of plane by seeing other subtree
      KNearestNeighbor(distAxis > 0 ? curNode->right : curNode->left, testPt, result);
    }
  }

  /*********************************************************************/
  /*!
  \brief
  Specialized compare function for KDTree.  Compares all axes starting
  with a specific/important axis.
  \param a
  Key 1 to compare
  \param b
  Key 2 to compare with Key 1
  \param startAxis
  Highest priority axis of key to check with first
  \return
  Difference between the two given keys
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  double KDTree<N, ElemType>::CompareKeys(const Point<N>& a, const Point<N>& b, const unsigned startAxis)
  {
    double diff = 0;

    for (unsigned i = 0; i < N; ++i)
    {
      // fast alternative to the modulus operator for (i + startAxis) < 2 * N.
      unsigned r = i + startAxis;
      r = (r < N) ? r : r - N;
      diff = a[r] - b[r];
      if (KDUTILS::DoubleEqual(diff, 0.0) == false)
        break;
    }

    return diff;
  }


  /*********************************************************************/
  /*!
  \brief
  MergeSort nodes of the tree using the specialized compare
  \param toSort
  Vector of nodes to sort
  \param temp
  Vector of temporary nodes used for sorting process
  \param low
  Min index in sorting vector to sort
  \param high
  Max index in sorting vector to sort
  \param sortAxis
  Highest priority axis to use when sorting
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  void KDTree<N, ElemType>::MergeSort(std::vector<KDNode>& toSort, std::vector<KDNode>& temp, long low, long high, unsigned sortAxis)
  {
    if (high > low)
    {
      long i, j, k;
      // avoid overflow when calculating median
      long mid = low + ((high - low) >> 1);
      // sort lower half
      MergeSort(toSort, temp, low, mid, sortAxis);
      // sort upper half
      MergeSort(toSort, temp, mid + 1, high, sortAxis);

      // merge the results for this level of subdivision
      for (i = mid + 1; i > low; --i)
        temp[i - 1] = toSort[i - 1];
      for (j = mid; j < high; ++j)
        temp[mid + (high - j)] = toSort[j + 1];
      for (k = low; k <= high; ++k)
        toSort[k] = (CompareKeys(temp[i].split, temp[j].split, sortAxis) < 0) ? temp[i++] : temp[j--];
    }
  }


  /*********************************************************************/
  /*!
  \brief
  Removes duplicate nodes from given vector  (does not actually remove
  nodes from vector, but moves all duplicated to the back of vector)
  \param nodes
  Vector of nodes to be checked for duplicates
  \param startAxis
  Axis used as priority when finding duplicates
  \return
  New length of vector with all duplicates removed
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  unsigned KDTree<N, ElemType>::RemoveDuplicates(std::vector<KDNode>& nodes, const unsigned startAxis)
  {
    unsigned end = 0;

    for (unsigned i = 1; i < nodes.size(); ++i)
    {
      double compare = CompareKeys(nodes[i].split, nodes[i - 1].split, startAxis);

      if (compare > 0)
        nodes[++end] = nodes[i];
      else if (compare < 0)
        exit(1); // nodes is not sorted
    }

    //nodes.erase(nodes.begin() + end, nodes.end());
    return end;
  }

  /*********************************************************************/
  /*!
  \brief
  Makes a (median) balanced KDTree from a given list of Nodes (points and data)
  \param nodes
  Vector of nodes (points and data) to be inserted into
  */
  /***********************************************************************/
  template <unsigned N, typename ElemType>
  void KDTree<N, ElemType>::MakeFromList(std::vector<KDNode>& nodes)
  {
    Clear();

    DimensionVectors dimensionsSorted;
    NodeVector temp = nodes;

    dimensionsSorted.reserve(N);
    for (unsigned i = 0; i < N; ++i)
    {
      dimensionsSorted.push_back(nodes);
      // sort each dimension of given pointsList
      MergeSort(dimensionsSorted[i], temp, 0, static_cast<long>(nodes.size()) - 1, i);
    }

    unsigned newSize[N];
    for (unsigned i = 0; i < N; ++i)
      newSize[i] = RemoveDuplicates(dimensionsSorted[i], i);

    m_root = BuildTree(dimensionsSorted, temp, 0, newSize[0], 0);
  }

  /*********************************************************************/
  /*!
  \brief
  Makes a (median) balanced KD Tree from each sorted dimension vector
  \param dimensionsSorted
  Vector of vector of nodes (point and data) wheres each vector is a
  sorted point vector that is sorted based on the axis
  \param temp
  Holds a vector of a sorted dimesion as a temp
  \param start
  First index in sorted vector
  \param end
  Last index in sorted vector
  \param depth
  Current depth of recursion
  \return
  root of created tree
  */
  /***********************************************************************/
  template<unsigned N, typename ElemType>
  typename KDTree<N, ElemType>::KDNode* KDTree<N, ElemType>::BuildTree(DimensionVectors& dimensionsSorted, NodeVector& temp, long start, long end, long depth)
  {
    KDNode* node = nullptr;
    long axis = depth % N;

    if (start == end)
    {
      node = MakeNode(dimensionsSorted[0].at(end).split, dimensionsSorted[0].at(end).data, axis);
    }
    else if (start + 1 == end)
    {
      node = MakeNode(dimensionsSorted[0].at(start).split, dimensionsSorted[0].at(start).data, axis, 1);
      node->right = MakeNode(dimensionsSorted[0].at(end).split, dimensionsSorted[0].at(end).data, axis);
    }
    else if (start + 2 == end)
    {
      node = MakeNode(dimensionsSorted[0].at(start + 1).split, dimensionsSorted[0].at(start + 1).data, axis, 2);
      node->left = MakeNode(dimensionsSorted[0].at(start).split, dimensionsSorted[0].at(start).data, axis);
      node->right = MakeNode(dimensionsSorted[0].at(end).split, dimensionsSorted[0].at(end).data, axis);
    }
    else // (start + 2 < end)
    {
      long middle = start + ((end - start) / 2);
      node = MakeNode(dimensionsSorted[0].at(middle).split, dimensionsSorted[0].at(middle).data, axis, end - start);

      for (long i = start; i <= end; ++i)
        temp[i] = dimensionsSorted[0][i];

      long lower, higher;
      for (unsigned i = 1; i < N; ++i)
      {
        lower = start - 1;
        higher = middle;

        for (long j = start; j <= end; ++j)
        {
          double comp = CompareKeys(dimensionsSorted[i][j].split, node->split, axis);
          if (comp < 0)
            dimensionsSorted[i - 1][++lower] = dimensionsSorted[i][j];
          else if (comp > 0)
            dimensionsSorted[i - 1][++higher] = dimensionsSorted[i][j];
        }
      }

      for (long i = start; i <= end; ++i)
        dimensionsSorted[N - 1][i] = temp[i];

      node->left = BuildTree(dimensionsSorted, temp, start, lower, depth + 1);
      node->right = BuildTree(dimensionsSorted, temp, middle + 1, higher, depth + 1);
    }

    return node;
  }
}
