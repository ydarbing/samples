
/*
* Constructor accepts and stores the maximum size.
*/
template <typename T, typename Comparator>
BoundedPQueue<T, Comparator>::BoundedPQueue(unsigned maxSize, Comparator comp)
  : elems(comp), maximumSize(maxSize)
{
}

/*
* enqueue adds the element to the map, then deletes the last element of the
* map if there size exceeds the maximum size.
*/
template <typename T, typename Comparator>
void BoundedPQueue<T, Comparator>::push(const T& value, double priority)
{
  // don't insert if the priority being added is bigger than the worst
  if (size() == maxSize() && (elems.key_comp()(worst(), priority)))
    return;

  // Add the element to the collection.
  elems.insert(std::pair<double, T>(priority, value));

  // If there are too many elements in the queue, drop off the last one.
  if (size() > maximumSize) 
  {
    Iter last;
    last = elems.end();
    --last; // Now points to highest-priority element
    elems.erase(last);
  }
}

template <typename T, typename Comparator /*= std::less<double> */>
const T* BoundedPQueue<T, Comparator>::top(void)
{
  return &elems.begin()->second;
}

template <typename T, typename Comparator /*= std::less<double> */>
void BoundedPQueue<T, Comparator>::pop(void)
{
  elems.erase(elems.begin());
}

// returns the top element and then pops it
//template <typename T, typename Comparator>
//T BoundedPQueue<T, Comparator>::dequeue()
//{
//  // Copy the best value.
//  T result = elems.begin()->second;
//
//  // Remove it from the map.
//  elems.erase(elems.begin());
//
//  return result;
//}

template <typename T, typename Comparator>
bool BoundedPQueue<T, Comparator>::full(void) const
{
  return elems.size() == maximumSize;
}

template <typename T, typename Comparator>
unsigned BoundedPQueue<T, Comparator>::size() const
{
  return static_cast<unsigned>(elems.size());
}

template <typename T, typename Comparator>
bool BoundedPQueue<T, Comparator>::empty() const
{
  return elems.empty();
}

template <typename T, typename Comparator>
unsigned BoundedPQueue<T, Comparator>::maxSize() const
{
  return maximumSize;
}

/*
* The best() and worst() functions check if the queue is empty,
* and if so return infinity.
*/
template <typename T, typename Comparator>
double BoundedPQueue<T, Comparator>::best() const
{
  return empty() ? std::numeric_limits<double>::infinity() : elems.begin()->first;
}

template <typename T, typename Comparator>
double BoundedPQueue<T, Comparator>::worst() const
{
  return empty() ? std::numeric_limits<double>::infinity() : elems.rbegin()->first;
}

template <typename T, typename Comparator>
const T* BoundedPQueue<T, Comparator>::best_data() const
{
  return empty() ? nullptr : &elems.begin()->second;
}

template <typename T, typename Comparator>
const T* BoundedPQueue<T, Comparator>::worst_data() const
{
  return empty() ? nullptr : &elems.rbegin()->second;
}
