#include <assert.h>

namespace Tree
{
	inline void DynamicAABBTree::AddToFreeList(int index)
	{
	  for (int i = index; i < m_capacity; ++i)
	  {
	    m_nodes[i].next = i + 1;
	    m_nodes[i].height = Node::Null;
	  }
	
	  m_nodes[m_capacity - 1].next = Node::Null;
	  m_nodes[m_capacity - 1].height = Node::Null;
	  m_freeList = index;
	}
	
	inline void DynamicAABBTree::DestroyNode(int index)
	{
	  assert(index >= 0 && index < m_capacity);
	
	  m_nodes[index].next = m_freeList;
	  m_nodes[index].height = Node::Null;
	  m_freeList = index;
	
	  --m_size;
	}
	
	template <typename T>
	void DynamicAABBTree::Query(T* cb, const Maths::AABB& aabb) const
	{
	  const int stackCapacity = 256;
	  int stack[stackCapacity];
	  int sp = 1;
	
	  *stack = m_root;
	
	  while (sp)
	  {
	    assert(sp < stackCapacity);
	
	    int id = stack[--sp];
	
	    const Node* n = m_nodes + id;
	    if (aabb.TestIntersection(n->aabb))
	    {
	      if (n->IsLeaf(!cb->TreeCallBack(id)))
	        return;
	      else
	      {
	        stack[sp++] = n->left;
	        stack[sp++] = n->right;
	      }
	    }
	  }
	}
}

