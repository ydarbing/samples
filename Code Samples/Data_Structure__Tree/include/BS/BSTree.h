/*
  BSTree.h
  Brady Reuter
  2/25/2015

  Binary Search Tree and all protected functions needed to maintain
  an AVL tree
  */
#pragma once

#ifdef _MSC_VER
#pragma warning( disable : 4290 ) // suppress warning: C++ Exception Specification ignored
#endif

#include <queue> // breadth first
#include <string>    // std::string
#include <stdexcept> // std::exception
#include "ObjectAllocator.h"

namespace Tree
{

  class BSTException : public std::exception
  {
  public:
    BSTException(int ErrCode, const std::string& Message) :
      error_code_(ErrCode), message_(Message) {
    };

    virtual int code(void) const {
      return error_code_;
    }
    virtual const char *what(void) const throw() {
      return message_.c_str();
    }
    virtual ~BSTException() throw() {}
    enum BST_EXCEPTION{ E_DUPLICATE, E_NO_MEMORY };

  private:
    int error_code_;
    std::string message_;
  };

  template <typename T>
  class BSTree
  {
  public:
    struct BinTreeNode
    {
      BinTreeNode *left;
      BinTreeNode *right;
      T data;
      int balance_factor; // optional
      unsigned count;    // number of nodes in subtree

      BinTreeNode(void) : left(0), right(0), data(0), balance_factor(0), count(0) {};
      BinTreeNode(const T& value) : left(0), right(0), data(value), balance_factor(0), count(0) {};
    };
    typedef BinTreeNode* BinTree;

    BSTree(ObjectAllocator *OA = 0, bool ShareOA = false);
    BSTree(const BSTree& rhs);
    virtual ~BSTree();
    BSTree& operator=(const BSTree& rhs);
    const BinTreeNode* operator[](int index) const;
    virtual void insert(const T& value) throw(BSTException);
    virtual void remove(const T& value);
    void clear(void);
    bool find(const T& value, unsigned &compares) const;
    bool empty(void) const;
    unsigned int size(void) const;
    int height(void) const;
    BinTree root(void) const;


    typedef void(*FUNC_PTR)(const T& val);

    void TraversePreorder(FUNC_PTR func);
    void TraverseInorder(FUNC_PTR func);
    void TraversePostorder(FUNC_PTR func);
    void TraverseBreadthFirst(FUNC_PTR func);

  protected: //functions
    void TraverseTreePreorder(BinTree node, FUNC_PTR func);
    void TraverseTreeInorder(BinTree node, FUNC_PTR func);
    void TraverseTreePostorder(BinTree node, FUNC_PTR func);

    BinTree& get_root(void);
    BinTree make_node(const T& value);
    void free_node(BinTree node);
    int tree_height(BinTree tree) const;
    void find_predecessor(BinTree tree, BinTree &predecessor) const;
    void MakeOwnAllocator(void);
    void RotateRight(BinTree& tree);
    void RotateLeft(BinTree& tree);

  protected: //members
    BinTree m_tree;
    ObjectAllocator* m_allocator;
    bool             m_shareOA;
    bool             m_freeOA;

  private:
    const BinTreeNode* SubscriptHelper(BinTree tree, const int index)const;
    BinTree CopyTree(BinTree other);
    void InsertItem(BinTree& tree, T value)throw(BSTException);
    void ClearTree(BinTree& tree);
    bool FindItem(const BinTree& tree, T value, unsigned &compares)const;
    void RemoveItem(BinTree& tree, T value);
  };
}


#include "BSTree.hpp"
