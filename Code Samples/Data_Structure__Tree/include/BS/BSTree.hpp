/*
  BSTree.cpp
  Brady Reuter
  2/25/2015
  Binary Search Tree and all protected functions needed to maintain
   an AVL tree
*/ 

namespace Tree
{
	/*********************************************************************/
	 /*!
	   \brief
	     Create a new tree and choose whether or not to make a allocator 
	       specifically for this one
	   \param OA
	     the allocator that will be used to create new nodes
	   \param ShareOA 
	     tells copy and assignment operator if they should use the allocator
	 */
	 /***********************************************************************/
	template <typename T>
	BSTree<T>::BSTree(ObjectAllocator *OA, bool ShareOA): 
	  m_tree(0), m_allocator(OA), m_shareOA(ShareOA), m_freeOA(false)
	{
	  if(!m_allocator)
	    MakeOwnAllocator();
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Only made so 'new' is used in one spot, but creates a new
	       allocator that uses the c++ new and delete
	 */
	 /***********************************************************************/
	template <typename T>
	void BSTree<T>::MakeOwnAllocator(void)
	{                                        // Set UseCPPMemManager to true
	  m_allocator = new ObjectAllocator(sizeof(BinTreeNode), OAConfig(true));
	  m_freeOA = true;   // We own the allocator, we will have to free it
	  m_shareOA = false; // Do not share this allocator with any other list
	}
	
	/*********************************************************************/
	 /*!
	   \brief
	     destructor deletes all tree nodes and if it should delete allocator
	       it will
	 */
	 /***********************************************************************/
	template <typename T>
	BSTree<T>::~BSTree()
	{
	  clear();
	
	  if(m_freeOA)
	  {
	    delete m_allocator;
	    m_allocator = 0;
	  }
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Copy constructor
	   \param rhs
	     the tree that will be deep copied into this new one
	 */
	 /***********************************************************************/
	template <typename T>
	BSTree<T>::BSTree(const BSTree<T>& rhs): m_tree(0)
	{  // If we are to use rhs' allocator
	  if(rhs.m_shareOA)
	  {
	    m_allocator = rhs.m_allocator; // Use rhs' allocator
	    m_freeOA = false; // don't own it (won't free it)
	    m_shareOA = true; // If a copy of 'this object' is made
	  }
	  else // No sharing, create own personal allocator
	    MakeOwnAllocator();
	
	  m_tree = CopyTree(rhs.root());
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Assignment operator
	   \param rhs
	     the tree that will be deep copied into this new tree
	   \return
	     the new tree
	 */
	 /***********************************************************************/
	template <typename T>
	BSTree<T>& BSTree<T>::operator=(const BSTree<T>& rhs)
	{
	  if(this != &rhs)
	  {
	    if(rhs.m_shareOA)
	    {
	      m_allocator = rhs.m_allocator;
	      m_freeOA = false;
	      m_shareOA = true;
	    }
	    else
	    {
	      if(!m_allocator)
	        MakeOwnAllocator();
	    }
	
	    clear();
	    m_tree = CopyTree(rhs.root());
	  }
	
	  return *this;
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Recursively copies one tree and returns the root of the newly copied tree
	   \param other
	     the tree that is being copied
	  \return
	    the head of the newly copied tree
	 */
	 /***********************************************************************/
	template <typename T>
	typename BSTree<T>::BinTree BSTree<T>::CopyTree(BinTree other)
	{
	  if(other == 0)
	    return 0;
	  // make new node and copy all other data into it
	  BinTree newTree = make_node(other->data);
	  newTree->count          = other->count;
	  newTree->balance_factor = other->balance_factor;
	  // recursive call to make rest of tree
	  newTree->left = CopyTree(other->left);
	  newTree->right = CopyTree(other->right);
	
	  return newTree;
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Subscript operator to index into tree
	   \param index
	     what node on the tree we want
	   \return
	     the node at the given index
	     if not valid returns NULL
	 */
	 /***********************************************************************/
	template <typename T>
	const typename BSTree<T>::BinTreeNode* BSTree<T>::operator[](int index) const
	{
	  return SubscriptHelper(m_tree, index);
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Actually gets the node at the given index  (recursive)
	   \param tree
	     current node of tree, used to check index
	   \param index 
	     the index we are looking for
	    \return
	      the node with the given index
	 */
	 /***********************************************************************/
	template <typename T>
	const typename BSTree<T>::BinTreeNode* BSTree<T>::SubscriptHelper(BinTree tree, const int index) const
	{
	  if(tree == 0)
	    return 0;
	
	  int check = 0;
	  if(tree->left != 0)
	    check = tree->left->count;
	
	  if(check > index)
	    return SubscriptHelper(tree->left, index);
	  else if(check < index)
	    return SubscriptHelper(tree->right, index - check - 1);
	  else // this is the correct index;
	    return tree;
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Insert an item into tree
	   \param value
	     The data that is being put into the tree
	  \exception BSTExcetion
	    exception thrown if there is no more memory or a duplicate key was
	     inserted
	 */
	 /***********************************************************************/
	template <typename T>
	void BSTree<T>::insert(const T& value) throw(BSTException)
	{
	  InsertItem(m_tree, value);
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Finds where to put data in tree (recursive)
	   \param tree
	     current node on the tree we are comparing with value
	   \param value
	     data being put into tree
	  \exception BSTExcetion
	    exception thrown if there is no more memory or a duplicate key was
	     inserted
	 */
	 /***********************************************************************/
	template <typename T>
	void BSTree<T>::InsertItem(BinTree& tree, T value) throw(BSTException)
	{
	  if (tree == 0)
	    tree = make_node(value);
	  else if (value < tree->data)
	    InsertItem(tree->left, value);
	  else if (value > tree->data)
	    InsertItem(tree->right, value);
	  else
	    throw (BSTException(BSTException::E_DUPLICATE, "Tried to insert key twice"));
	    
	  ++tree->count;
	}
	
	
	template <typename T>
	void BSTree<T>::TraversePreorder(FUNC_PTR func)
	{
	  TraverseTreePreorder(m_tree, func);
	}
	
	template <typename T>
	void BSTree<T>::TraverseInorder(FUNC_PTR func)
	{
	  TraverseTreeInorder(m_tree, func);
	}
	template <typename T>
	void BSTree<T>::TraversePostorder(FUNC_PTR func)
	{
	  TraverseTreePostorder(m_tree, func);
	}
	
	
	
	template <typename T>
	void BSTree<T>::TraverseTreePreorder(BinTree node, FUNC_PTR func)
	{
	  if (node == nullptr)
	    return;
	
	  //std::cout << node->data;
	  func(node->data);// should check if func exists
	  TraverseTreePreorder(node->left,func);
	  TraverseTreePreorder(node->right,func);
	}
	
	template <typename T>
	void BSTree<T>::TraverseTreeInorder(BinTree node, FUNC_PTR func)
	{
	
	  if (node == nullptr)
	    return;
	
	  TraverseTreeInorder(node->left, func);
	  //std::cout << node->data;
	  func(node->data);// should check if func exists
	  TraverseTreeInorder(node->right, func);
	}
	
	template <typename T>
	void BSTree<T>::TraverseTreePostorder(BinTree node, FUNC_PTR func)
	{
	  if (node == nullptr)
	    return;
	
	  TraverseTreePostorder(node->left, func);
	  TraverseTreePostorder(node->right, func);
	  //std::cout << node->data;
	  func(node->data);// should check if func exists
	}
	
	
	template <typename T>
	void BSTree<T>::TraverseBreadthFirst(FUNC_PTR func)
	{
	  std::queue<BinTree> q;
	  q.push(m_tree);
	
	  while (!q.empty())
	  {
	    BinTree current = q.front();
	    q.pop();
	    //std::cout << current->data;
	    func(current->data);// should check if func exists
	
	    if (current->left != nullptr)
	      q.push(current->left);
	    if (current->right != nullptr)
	      q.push(current->right);
	  }
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Removes a given value from the tree
	   \param value
	     data being removed from tree
	 */
	 /***********************************************************************/
	template <typename T>
	void BSTree<T>::remove(const T& value)
	{
	  RemoveItem(m_tree, value);
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Removes a given value from the tree (recursive)
	   \param tree
	     current node of tree being check with value
	   \param value
	     data being removed from tree
	 */
	 /***********************************************************************/
	template <typename T>
	void BSTree<T>::RemoveItem(BinTree& tree, T value)
	{
	  if(tree == 0)
	    return;
	  else if(value < tree->data)
	  {
	    RemoveItem(tree->left, value);
	    --tree->count;
	  }
	  else if(value > tree->data)
	  {
	    RemoveItem(tree->right, value);
	    --tree->count;
	  }
	  else // (Data == tree->data)
	  {
	    if(tree->left == 0)
	    {
	      BinTree temp = tree;
	      tree = tree->right;
	      free_node(temp);
	    }
	    else if(tree->right == 0)
	    {
	      BinTree temp = tree;
	      tree = tree->left;
	      free_node(temp);
	    }
	    else
	    {
	      BinTree pred = 0;
	      --tree->count;
	      find_predecessor(tree, pred);
	      tree->data = pred->data;
	      RemoveItem(tree->left, tree->data);
	    }
	  }
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Finds a given value from the tree and tells how many comparisons were used
	   \param value
	     the data that will be checked if in tree
	   \param compares
	     number of comparisons used to find value in tree 
	   \return
	     true if found value
	 */
	 /***********************************************************************/
	template <typename T>
	bool BSTree<T>::find(const T& value, unsigned &compares) const
	{
	  return FindItem(m_tree, value, compares);
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Recursive functions to find given data in tree
	   \param tree
	     current node in tree being checked against value
	   \param value
	     the data that will be checked if in tree
	   \param compares
	     number of comparisons used to find value in tree 
	   \return
	     true if found value
	 */
	 /***********************************************************************/
	template <typename T>
	bool BSTree<T>::FindItem(const BinTree& tree, T value, unsigned &compares)const
	{
	  ++compares;
	  if (tree == 0)
	    return false;
	  else if (value == tree->data)
	    return true;
	  else if (value < tree->data)
	    return FindItem(tree->left, value, compares);
	  else
	    return FindItem(tree->right, value, compares);
	}
	
	/*********************************************************************/
	 /*!
	   \brief
	     Takes out all nodes in tree
	 */
	 /***********************************************************************/
	template <typename T>
	void BSTree<T>::clear(void)
	{
	  ClearTree(m_tree);
	}
	
	/*********************************************************************/
	 /*!
	   \brief
	     Recursively takes out all nodes in tree
	 */
	 /***********************************************************************/
	template <typename T>
	void BSTree<T>::ClearTree(BinTree& tree)
	{
	  if(tree != 0)
	  {
	    ClearTree(tree->left);
	    ClearTree(tree->right);
	    BinTree del = tree;
	    free_node(del);
	    tree = 0;
	  }
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Checks if the tree if empty
	   \return
	    true if no nodes in tree
	 */
	 /***********************************************************************/
	template <typename T>
	bool BSTree<T>::empty(void) const
	{
	  return (m_tree == 0);
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Gets the number of nodes in tree
	   \return
	    number of nodes in tree
	 */
	 /***********************************************************************/
	template <typename T>
	unsigned int BSTree<T>::size(void) const
	{
	  if(!m_tree)
	    return 0;
	
	  return m_tree->count;
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Gets the height of this tree
	   \return
	    height of tree
	 */
	 /***********************************************************************/
	template <typename T>
	int BSTree<T>::height(void) const
	{
	  return tree_height(m_tree);
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Gets the root of the tree
	   \return
	    root of tree
	 */
	 /***********************************************************************/
	template <typename T>
	typename BSTree<T>::BinTree BSTree<T>::root(void) const
	{
	  return m_tree;
	}
	
	/*********************************************************************/
	 /*!
	   \brief
	     Gets the root of the tree
	   \return
	    root of tree
	 */
	 /***********************************************************************/
	template <typename T>
	typename BSTree<T>::BinTree& BSTree<T>::get_root(void)
	{
	  return m_tree;
	}
	
	/*********************************************************************/
	 /*!
	   \brief
	     Makes a node from the space given back from the allocator
	   \param value
	     the data that will go into new node
	   \return
	     newly created node
	  \exception BSTExcetion
	    exception thrown if there is no more memory 
	 */
	 /***********************************************************************/
	template <typename T>
	typename BSTree<T>::BinTree BSTree<T>::make_node(const T& value)
	{
	  // call placement new to put new node in already allocated memory
	  BinTreeNode* node = 0;
	  try{
	    BinTreeNode* memLocation = reinterpret_cast<BinTreeNode*>(m_allocator->Allocate());
	    node = new(memLocation) BinTreeNode(value);
	  }
	  catch(const OAException& e)
	  {
	    throw (BSTException(BSTException::E_NO_MEMORY, e.what()));
	  }
	
	  return node;
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Deallocated given node
	   \param node
	     node to be deleted
	 */
	 /***********************************************************************/
	template <typename T>
	void BSTree<T>::free_node(BinTree node)
	{
	  // have to call destructor explicitly because we used placement new
	  node->~BinTreeNode();
	  m_allocator->Free(node);
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Finds the height of a given tree
	   \param tree
	     node whose height will be gotten
	   \return
	     the height of given tree
	 */
	 /***********************************************************************/
	template <typename T>
	int BSTree<T>::tree_height(BinTree tree) const
	{
	  if (tree == 0)
	    return -1;
	  else
	    return (1 + std::max(tree_height(tree->left), tree_height(tree->right)));
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Rotates the tree to the right (i.e promotes the left child)
	     also adjusted the number of nodesde
	   \param tree
	     node who will be rotated
	 */
	 /***********************************************************************/
	template <typename T>
	void BSTree<T>::RotateRight(BinTree& tree)
	{
	  unsigned numNodesToRight = (tree->right) ? tree->right->count : 0;
	  unsigned numNodesZigZag = 0;
	  if(tree->left) // check to see if the zig zag even exists
	    numNodesZigZag = (tree->left->right) ? tree->left->right->count : 0;
	
	  BinTree temp = tree;
	  tree = tree->left;
	  temp->left = tree->right;       
	  tree->right = temp;
	  // update counts of switched nodes
	  temp->count = numNodesToRight + numNodesZigZag + 1; // rotated's new count
	  tree->count = temp->count + ((tree->left) ? tree->left->count : 0) + 1; // promoted's new count
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Rotates the tree to the left (i.e promotes the right child)
	     also adjusted the number of nodes
	   \param tree
	     node who will be rotated
	 */
	 /***********************************************************************/
	template <typename T>
	void BSTree<T>::RotateLeft(BinTree& tree)
	{
	  unsigned numNodesToLeft = (tree->left) ? tree->left->count : 0;
	  unsigned numNodesZigZag = 0;
	  if(tree->left) // check to see if the zig zag even exists
	    numNodesZigZag = (tree->right->left) ? tree->right->left->count : 0;
	
	  BinTree temp = tree;
	  tree = tree->right;
	  temp->right = tree->left;
	  tree->left = temp;
	
	  temp->count = numNodesToLeft + numNodesZigZag + 1; // rotated's new count
	  tree->count = temp->count + ((tree->right) ? tree->right->count : 0) + 1; // promoted's new count
	}
	/*********************************************************************/
	 /*!
	   \brief
	     Finds the left child's right most node
	   \param tree
	     node's who predecessor will be found
	   \param predecessor
	     reference to the newly found predecessor
	 */
	 /***********************************************************************/
	template <typename T>
	void BSTree<T>::find_predecessor(BinTree tree, BinTree& predecessor) const
	{
	  predecessor = tree->left;
	  while (predecessor->right != 0)
	    predecessor = predecessor->right;
	}
	
}
