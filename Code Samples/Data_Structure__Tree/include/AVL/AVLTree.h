/*
  AVLTree.h
  Brady Reuter
  2/25/2015
   AVL Tree, inherits all of BSTree, and adds no new data, only functions

*/ 

#pragma once
#include <stack>
#include "BS/BSTree.h"
namespace Tree
{	
	template <typename T>
	class AVLTree : public BSTree<T>
	{
	  public:
	    AVLTree(ObjectAllocator *OA = 0, bool ShareOA = false);
	    virtual ~AVLTree();
	    virtual void insert(const T& value) throw(BSTException);
	    virtual void remove(const T& value);
	
	      // Returns true if efficiency implemented
	    static bool ImplementedBalanceFactor(void);
	    
	  private:
	    typedef typename BSTree<T>::BinTree BinTree;
	    typedef std::stack<BinTree*> BinStack;
	    void InsertItem(BinStack& touchedNodes, BinTree& curNode, const T& value)  throw(BSTException);
	    void RemoveItem(BinStack& touchedNodes, BinTree& curNode, const T& value);
	    void BalanceTree(BinStack& touchedNodes, bool removingNode = false);
	};
}

#include "AVLTree.hpp"
