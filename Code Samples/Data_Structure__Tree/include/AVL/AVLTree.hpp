/*
  AVLTree.hpp
  Brady Reuter
  2/25/2015
  */
namespace Tree
{
  /*********************************************************************/
  /*!
    \brief
    Create a new tree and choose whether or not to make a allocator
    specifically for this one
    \param OA
    the allocator that will be used to create new nodes
    \param ShareOA
    tells copy and assignment operator if they should use the allocator
    */
  /***********************************************************************/
  template <typename T>
  AVLTree<T>::AVLTree(ObjectAllocator* OA, bool ShareOA)
    : BSTree<T>(OA, ShareOA) // only need to initial data in base class since no new data is made
  {
  }
  /*********************************************************************/
  /*!
    \brief
    destructor
    */
  /***********************************************************************/
  template <typename T>
  AVLTree<T>::~AVLTree()
  {
  }
  /*********************************************************************/
  /*!
    \brief
    Adds item to tree using the stack method
    \param value
    the data that is going to be added to tree
    \exception BSTException
    throws if out of memory or tryed to put duplicate data in
    */
  /***********************************************************************/
  template <typename T>
  void AVLTree<T>::insert(const T& value) throw(BSTException)
  {
    std::stack<typename BSTree<T>::BinTree *> touchedNodes;
    InsertItem(touchedNodes, this->get_root(), value);
  }
  /*********************************************************************/
  /*!
    \brief
    Adds item to tree using the stack method  (recursive)
    \param touchedNodes
    the stack of nodes that have been touched in process of inserting
    \param curNode
    current node in the tree being compared to with value
    \param value
    the data that is going to be added to tree
    \exception BSTException
    throws if out of memory or tryed to put duplicate data in
    */
  /***********************************************************************/
  template <typename T>
  void AVLTree<T>::InsertItem(std::stack<typename BSTree<T>::BinTree *>& touchedNodes, BSTree<T>::BinTree& curNode, const T& value) throw(BSTException)
  {
    if (curNode == 0)
    {
      curNode = make_node(value);
      ++curNode->count;
      BalanceTree(touchedNodes);
    }
    else if (value < curNode->data)
    {
      touchedNodes.push(&curNode);
      ++curNode->count;
      InsertItem(touchedNodes, curNode->left, value);
    }
    else if (value > curNode->data)
    {
      touchedNodes.push(&curNode);
      ++curNode->count;
      InsertItem(touchedNodes, curNode->right, value);
    }
    else
      throw (BSTException(BSTException::E_DUPLICATE, "Tried to insert key twice"));
  }

  /*********************************************************************/
  /*!
    \brief
    Actual method that makes it an AVL tree, if there is an imbalance,
    perform rotations to balance tree, and go through whole stack if removing
    \param touchedNodes
    the stack of nodes that have been touched in process of inserting/deleting
    \param removingNode
    set to true if deleting a node so it can go through all nodes in the stack
    */
  /***********************************************************************/
  template <typename T>
  void AVLTree<T>::BalanceTree(std::stack<typename BSTree<T>::BinTree *>& touchedNodes, bool removingNode /*false*/)
  {
    while (!touchedNodes.empty())
    {
      BSTree<T>::BinTree* y = touchedNodes.top();
      touchedNodes.pop();

      int lHeight = this->tree_height((*y)->left);
      int rHeight = this->tree_height((*y)->right);
      // check if this "node" is the cause of the unbalance
      if (std::abs(lHeight - rHeight) > 1)
      {
        BinTree* u = (lHeight > rHeight) ? &(*y)->left : &(*y)->right;
        BinTree* v = &(*u)->left;
        BinTree* w = &(*u)->right;
        int vHeight = tree_height((*v));
        int wHeight = tree_height((*w));
        if (lHeight > rHeight)
        {
          if (vHeight < wHeight)
            this->RotateLeft((*u));

          this->RotateRight((*y));
        }
        else
        {
          if (vHeight > wHeight)
            this->RotateRight((*u));

          this->RotateLeft((*y));
        }
        // only need to find first if inserting 
        if (!removingNode)
          break;
      }
    }
  }

  /*********************************************************************/
  /*!
    \brief
    removes node from avl tree
    \param value
    data being removed from tree
    */
  /***********************************************************************/
  template <typename T>
  void AVLTree<T>::remove(const T& value)
  {
    std::stack<typename BSTree<T>::BinTree *> touchedNodes;
    RemoveItem(touchedNodes, this->get_root(), value);
  }

  /*********************************************************************/
  /*!
    \brief
    Removed item to tree using the stack method  (recursive)
    \param touchedNodes
    The stack of nodes that have been touched in process of removing
    \param curNode
    Current node in the tree being compared to with value
    \param value
    the data that is going to be removed from tree
    */
  /***********************************************************************/
  template <typename T>
  void AVLTree<T>::RemoveItem(std::stack<typename BSTree<T>::BinTree *>& touchedNodes, BSTree<T>::BinTree& curNode, const T& value)
  {
    if (curNode == 0) // item not found
      return;
    else if (value < curNode->data)
    {
      touchedNodes.push(&curNode);
      --curNode->count;
      RemoveItem(touchedNodes, curNode->left, value);
    }
    else if (value > curNode->data)
    {
      touchedNodes.push(&curNode);
      --curNode->count;
      RemoveItem(touchedNodes, curNode->right, value);
    }
    else // curNode->data == value
    {
      if (curNode->left == 0)
      {
        BinTree temp = curNode;
        curNode = curNode->right;
        this->free_node(temp);
        BalanceTree(touchedNodes, true);
      }
      else if (curNode->right == 0)
      {
        BinTree temp = curNode;
        curNode = curNode->left;
        this->free_node(temp);
        BalanceTree(touchedNodes, true);
      }
      else
      {
        BinTree pred = 0;
        --curNode->count;
        touchedNodes.push(&curNode);
        this->find_predecessor(curNode, pred);
        curNode->data = pred->data;
        RemoveItem(touchedNodes, curNode->left, curNode->data);
      }
    }
  }

  /*********************************************************************/
  /*!
    \brief
    static call to see if extra credit was done
    \return
    true if extra credit was implemented
    */
  /***********************************************************************/
  template <typename T>
  bool AVLTree<T>::ImplementedBalanceFactor(void)
  {
    return false;
  }

}
