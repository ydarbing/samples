#include "Spatial/KD/TestKD.h"
#include "Spatial/KD/BoundedPriorityQueue.h"

namespace Tree
{	
	typedef KDTree<3, unsigned>::KDNode TestNode;
	
	void TestPoint()
	{
	  Point<4> a(1.0, 2.0, 3.0, 4.0);// ok!
	  Point<4> a3; // ok!
	  //Point<4> a(1.0, 2.0, 3.0);// causes error
	  //Point<4> a3(1.0, 2.0, 3.0, 2.0, 3.0); // causes error
	}
	
	std::vector<TestNode> MakeKnownPointList(bool haveDuplicates = true)
	{
	  std::vector<TestNode> pointList;
	  pointList.reserve(haveDuplicates ? 26 : 16);
	  TestNode n;
	  unsigned d = 0;
	  n.split = Point<3>(2.0, 3.0, 3.0);  n.data = d; pointList.push_back(n);
	  n.split = Point<3>(5.0, 4.0, 2.0);  ++n.data;   pointList.push_back(n);
	  n.split = Point<3>(9.0, 6.0, 7.0);  ++n.data;   pointList.push_back(n);
	  n.split = Point<3>(4.0, 7.0, 9.0);  ++n.data;   pointList.push_back(n);
	  n.split = Point<3>(8.0, 1.0, 5.0);  ++n.data;   pointList.push_back(n);
	  n.split = Point<3>(7.0, 2.0, 6.0);  ++n.data;   pointList.push_back(n);
	  n.split = Point<3>(9.0, 4.0, 1.0);  ++n.data;   pointList.push_back(n);
	  n.split = Point<3>(8.0, 4.0, 2.0);  ++n.data;   pointList.push_back(n);
	  n.split = Point<3>(9.0, 7.0, 8.0);  ++n.data;   pointList.push_back(n);
	  n.split = Point<3>(6.0, 3.0, 1.0);  ++n.data;   pointList.push_back(n);
	  if (haveDuplicates) // insert into center for fun
	  {
	    n.split = Point<3>(6.0, 3.0, 1.0);  ++n.data;   pointList.push_back(n);
	    n.split = Point<3>(9.0, 6.0, 7.0);  ++n.data;   pointList.push_back(n);
	    n.split = Point<3>(2.0, 1.0, 3.0);  ++n.data;   pointList.push_back(n);
	    n.split = Point<3>(5.0, 4.0, 2.0);  ++n.data;   pointList.push_back(n);
	    n.split = Point<3>(4.0, 7.0, 9.0);  ++n.data;   pointList.push_back(n);
	    n.split = Point<3>(8.0, 7.0, 6.0);  ++n.data;   pointList.push_back(n);
	    n.split = Point<3>(9.0, 4.0, 1.0);  ++n.data;   pointList.push_back(n);
	    n.split = Point<3>(7.0, 2.0, 6.0);  ++n.data;   pointList.push_back(n);
	    n.split = Point<3>(1.0, 6.0, 8.0);  ++n.data;   pointList.push_back(n);
	    n.split = Point<3>(3.0, 4.0, 5.0);  ++n.data;   pointList.push_back(n);
	  }
	  n.split = Point<3>(3.0, 4.0, 5.0);  ++n.data;   pointList.push_back(n);
	  n.split = Point<3>(1.0, 6.0, 8.0);  ++n.data;   pointList.push_back(n);
	  n.split = Point<3>(9.0, 5.0, 3.0);  ++n.data;   pointList.push_back(n);
	  n.split = Point<3>(2.0, 1.0, 3.0);  ++n.data;   pointList.push_back(n);
	  n.split = Point<3>(8.0, 7.0, 6.0);  ++n.data;   pointList.push_back(n);
	  n.split = Point<3>(100.0, 100.0, 100.0);  ++n.data;   pointList.push_back(n);// out there for testing NN
	  return pointList;
	}
	
	void InsertElementsIntoTree(KDTree<3, unsigned>& kd, int numElements)
	{
	  std::vector<TestNode> pts;
	  GetVectorOfPoints<3, unsigned>(pts, numElements);
	  for (int i = 0; i < numElements; ++i)
	    kd.Insert(pts[i].split, pts[i].data);
	}
	
	void TestConstructor()
	{
	  const char *test = "Test1 - Default Constructor";
	  std::cout << "\n====================== " << test << " ======================\n";
	
	  try
	  {
	    KDTree<3, unsigned> kd;
	
	    assert(kd.GetSize() == 0);
	    assert(kd.GetRoot() == nullptr);
	    assert(kd.GetDimension() == 3);
	    assert(kd.Empty() == true);
	    assert(kd.GetHeight() == 0);
	  }
	  catch (const KDTException &e)
	  {
	    std::cout << "Caught BSTException in " << test;
	    int value = e.code();
	    if (value == KDTException::E_NO_MEMORY)
	      std::cout << "E_NO_MEMORY" << std::endl;
	    else
	      std::cout << "Unknown error code." << std::endl;
	  }
	  catch (...)
	  {
	    std::cout << "Caught unknown exception in " << test << std::endl;
	  }
	}
	
	void TestCopyConstructor()
	{
	  const char *test = "Test2 - Copy Constructor";
	  std::cout << "\n====================== " << test << " ======================\n";
	
	  int numElements = 12;
	  KDTree<3, unsigned> kd;
	  InsertElementsIntoTree(kd, numElements);
	
	  try
	  {
	    KDTree<3, unsigned> copy(kd);
	    assert(kd.GetSize() == copy.GetSize());
	    assert(kd.GetDimension() == copy.GetDimension());
	    assert(kd.Empty() == copy.Empty());
	    assert(kd.GetHeight() == copy.GetHeight());
	  }
	  catch (const KDTException &e)
	  {
	    std::cout << "Caught BSTException in " << test;
	    int value = e.code();
	    if (value == KDTException::E_NO_MEMORY)
	      std::cout << "E_NO_MEMORY" << std::endl;
	    else
	      std::cout << "Unknown error code." << std::endl;
	  }
	  catch (...)
	  {
	    std::cout << "Caught unknown exception in " << test << std::endl;
	  }
	
	
	}
	
	void TestAssignment()
	{
	  const char *test = "Test3 - Assignment Operator";
	  std::cout << "\n====================== " << test << " ======================\n";
	
	  int numElements = 12;
	  KDTree<3, unsigned> kd;
	  InsertElementsIntoTree(kd, numElements);
	
	  try
	  {
	    KDTree<3, unsigned> assign = kd;
	    assert(kd.GetSize() == assign.GetSize());
	    assert(kd.GetDimension() == assign.GetDimension());
	    assert(kd.Empty() == assign.Empty());
	    assert(kd.GetHeight() == assign.GetHeight());
	  }
	  catch (const KDTException &e)
	  {
	    std::cout << "Caught BSTException in " << test;
	    int value = e.code();
	    if (value == KDTException::E_NO_MEMORY)
	      std::cout << "E_NO_MEMORY" << std::endl;
	    else
	      std::cout << "Unknown error code." << std::endl;
	  }
	  catch (...)
	  {
	    std::cout << "Caught unknown exception in " << test << std::endl;
	  }
	}
	
	
	void TestConstructorGivenPointList()
	{
	  const char *test = "Test4 - Constructor Given Point List";
	  std::cout << "\n====================== " << test << " ======================\n";
	
	  unsigned numPoints = 25;
	  std::vector<TestNode> pointsList;
	  GetVectorOfPoints<3, unsigned>(pointsList, numPoints);
	
	  try
	  {
	    KDTree<3, unsigned> fromList(pointsList);
	  }
	  catch (const KDTException &e)
	  {
	    std::cout << "Caught BSTException in " << test;
	    int value = e.code();
	    if (value == KDTException::E_NO_MEMORY)
	      std::cout << "E_NO_MEMORY" << std::endl;
	    else
	      std::cout << "Unknown error code." << std::endl;
	  }
	  catch (...)
	  {
	    std::cout << "Caught unknown exception in " << test << std::endl;
	  }
	}
	
	
	void TestConstructorGivenPointListWithDuplicates()
	{
	  const char *test = "Test5 - Constructor Given Point List With Duplicates";
	  std::cout << "\n====================== " << test << " ======================\n";
	
	  std::vector<TestNode> pointList = MakeKnownPointList(true);
	
	  try {
	    KDTree<3, unsigned> fromList(pointList);
	  }
	  catch (const KDTException &e)
	  {
	    std::cout << "Caught BSTException in " << test;
	    int value = e.code();
	    if (value == KDTException::E_NO_MEMORY)
	      std::cout << "E_NO_MEMORY" << std::endl;
	    else
	      std::cout << "Unknown error code." << std::endl;
	  }
	  catch (...)
	  {
	    std::cout << "Caught unknown exception in " << test << std::endl;
	  }
	}
	
	void TestInsert()
	{
	  const char *test = "Test6 - Random Insert";
	
	  std::cout << "\n====================== " << test << " ======================\n";
	
	  int numPoints = 100;
	  int elemData = 0;
	  KDTree<3, unsigned> tree;
	  std::vector<TestNode> pts;
	
	  GetVectorOfPoints<3, unsigned>(pts, numPoints);
	  try
	  {
	    for (int i = 0; i < numPoints; i++)
	      tree.Insert(pts[i].split, elemData++);
	    std::cout << "height: " << tree.GetHeight() << std::endl;
	    std::cout << " nodes: " << tree.GetSize() << std::endl;
	    if (tree.Empty())
	      std::cout << "tree is empty\n\n";
	    else
	      std::cout << "tree is NOT empty\n\n";
	  }
	  catch (const KDTException &e)
	  {
	    std::cout << "Caught BSTException in " << test;
	    int value = e.code();
	    if (value == KDTException::E_NO_MEMORY)
	      std::cout << "E_NO_MEMORY" << std::endl;
	    else
	      std::cout << "Unknown error code." << std::endl;
	  }
	  catch (...)
	  {
	    std::cout << "Caught unknown exception in " << test << std::endl;
	  }
	}
	
	void TestFind()
	{
	  const char *test = "Test7 - Find";
	  std::cout << "\n====================== " << test << " ======================\n";
	
	  unsigned numPoints = 25;
	  std::vector<TestNode> pointsList;
	  GetVectorOfPoints<3, unsigned>(pointsList, numPoints);
	
	  try
	  {
	    KDTree<3, unsigned> tree(pointsList);
	
	    unsigned compares = 0;
	    int index = 0;
	    bool found = false;
	
	    found = tree.Find(pointsList[index].split, compares);
	    if (found)
	      std::cout << "Value " << pointsList[index].split << " found with " << compares << " compares\n";
	    else
	      std::cout << "Value " << pointsList[index].split << " NOT found with " << compares << " compares\n";
	
	    index = 3;
	    compares = 0;
	    found = tree.Find(pointsList[index].split, compares);
	    if (found)
	      std::cout << "Value " << pointsList[index].split << " found with " << compares << " compares\n";
	    else
	      std::cout << "Value " << pointsList[index].split << " NOT found with " << compares << " compares\n";
	
	    index = 5;
	    compares = 0;
	    found = tree.Find(pointsList[index].split, compares);
	    if (found)
	      std::cout << "Value " << pointsList[index].split << " found with " << compares << " compares\n";
	    else
	      std::cout << "Value " << pointsList[index].split << " NOT found with " << compares << " compares\n";
	
	    // test point that is not in tree
	    compares = 0;
	    Point<3> notInTree(0.0, 0.0, 0.0);
	    found = tree.Find(notInTree, compares);
	    if (found)
	      std::cout << "Value " << notInTree << " found with " << compares << " compares\n";
	    else
	      std::cout << "Value " << notInTree << " NOT found with " << compares << " compares\n";
	  }
	  catch (const KDTException &e)
	  {
	    std::cout << "Caught BSTException in " << test;
	    int value = e.code();
	    if (value == KDTException::E_NO_MEMORY)
	      std::cout << "E_NO_MEMORY" << std::endl;
	    else
	      std::cout << "Unknown error code." << std::endl;
	  }
	  catch (...)
	  {
	    std::cout << "Caught unknown exception in " << test << std::endl;
	  }
	}
	
	void TestNearestNeighbor()
	{
	  const char *test = "Test8 - Nearest Neighbor";
	  std::cout << "\n====================== " << test << " ======================\n";
	
	  std::vector<TestNode> pointList = MakeKnownPointList(false);// don't need duplicates (not testing constructor)
	
	  try {
	    KDTree<3, unsigned> kd(pointList);
	    unsigned toFind = kd.At(pointList.back().split);
	    Point<3> testPoint = pointList.back().split;
	
	    auto found = kd.NNValue(testPoint);
	    std::cout << "Test Nearest Neighbor with same point as in tree" << std::endl;
	    if (found == toFind)
	    {
	      std::cout << "Test Point Position: " << testPoint << "  Nearest Neighbor Position: " << pointList.back().split << std::endl;
	      std::cout << "Test Point Data: " << toFind << "  Nearest Neighbor Data: " << found << std::endl;
	    }
	    else
	      std::cout << "Did Not Get Correct Point!  Trying to find: " << toFind << " Found: " << found << std::endl;
	
	    testPoint = pointList.front().split;
	    toFind = kd.At(testPoint);//pointList.front().data;
	
	    found = kd.NNValue(testPoint);
	    std::cout << "Test Nearest Neighbor with same point as in tree" << std::endl;
	    if (found == toFind)
	    {
	      std::cout << "Test Point Position: " << testPoint << "  Nearest Neighbor Position: " << pointList.front().split << std::endl;
	      std::cout << "Test Point Data: " << toFind << "  Nearest Neighbor Data: " << found << std::endl;
	    }
	    else
	      std::cout << "Did Not Get Correct Point!  Trying to find: " << toFind << " Found: " << found << std::endl;
	
	
	    testPoint = pointList.back().split;
	    testPoint[0] += 1.0; // move it slightly so it's not the same point
	    toFind = kd.At(pointList.back().split);
	
	    found = kd.NNValue(testPoint);
	    std::cout << "Test Nearest Neighbor with point not in tree" << std::endl;
	    if (found == toFind)
	    {
	      std::cout << "Test Point Position: " << testPoint << "  Nearest Neighbor Position: " << pointList.back().split << std::endl;
	      std::cout << "Test Point Data: " << toFind << "  Nearest Neighbor Data: " << found << std::endl;
	    }
	    else
	      std::cout << "Did Not Get Correct Point!  Trying to find: " << toFind << " Found: " << found << std::endl;
	  }
	  catch (const KDTException &e)
	  {
	    std::cout << "Caught BSTException in " << test;
	    int value = e.code();
	    if (value == KDTException::E_NO_MEMORY)
	      std::cout << "E_NO_MEMORY" << std::endl;
	    else
	      std::cout << "Unknown error code." << std::endl;
	  }
	  catch (...)
	  {
	    std::cout << "Caught unknown exception in " << test << std::endl;
	  }
	
	}
	
	void TestKNearestNeighbors()
	{
	  const char *test = "Test9 - K Nearest Neighbors";
	  std::cout << "\n====================== " << test << " ======================\n";
	
	
	  std::vector<TestNode> pointList = MakeKnownPointList(false);// don't need duplicates (not testing constructor)
	
	  try {
	    KDTree<3, unsigned> kd(pointList);
	    unsigned k = 3;
	
	
	    unsigned toFind = kd.At(pointList.back().split);
	    Point<3> testPoint = pointList.back().split;
	
	    auto found = kd.KNNValue(testPoint, k);
	    std::cout << "Test Nearest Neighbor with same point as in tree" << std::endl;
	    if ((*found.best_data())->data == toFind)
	    {
	      std::cout << "Test Point Position: " << testPoint << " Test Point Data: " << toFind << std::endl;
	      int numClosest = 0;
	      while (!found.empty())
	      {
	        std::cout << ++numClosest << ". Nearest Neighbor Position: " << (*found.top())->split
	          << "  Nearest Neighbor Data: " << (*found.top())->data << std::endl;
	        found.pop();
	      }
	    }
	    else
	      std::cout << "Did Not Get Correct Point!  Trying to find: " << toFind << " Found: " << (*found.best_data())->data << std::endl;
	
	    testPoint = pointList.front().split;
	    toFind = kd.At(testPoint);
	
	    found = kd.KNNValue(testPoint, k);
	    std::cout << "Test Nearest Neighbor with same point as in tree" << std::endl;
	
	    if ((*found.best_data())->data == toFind)
	    {
	      std::cout << "Test Point Position: " << testPoint << " Test Point Data: " << toFind << std::endl;
	      int numClosest = 0;
	      while (!found.empty())
	      {
	        std::cout << ++numClosest << ". Nearest Neighbor Position: " << (*found.top())->split
	          << "  Nearest Neighbor Data: " << (*found.top())->data << std::endl;
	        found.pop();
	      }
	    }
	
	    else
	      std::cout << "Did Not Get Correct Point!  Trying to find: " << toFind << " Found: " << (*found.best_data())->data << std::endl;
	
	
	    testPoint = pointList.back().split;
	    testPoint[0] += 1.0; // move it slightly so it's not the same point
	    toFind = kd.At(pointList.back().split);
	
	    found = kd.KNNValue(testPoint, k);
	    std::cout << "Test Nearest Neighbor with point not in tree" << std::endl;
	    if ((*found.best_data())->data == toFind)
	    {
	      std::cout << "Test Point Position: " << testPoint << " Test Point Data: " << toFind << std::endl;
	      int numClosest = 0;
	      while (!found.empty())
	      {
	        std::cout << ++numClosest << ". Nearest Neighbor Position: " << (*found.top())->split
	          << "  Nearest Neighbor Data: " << (*found.top())->data << std::endl;
	        found.pop();
	      }
	    }
	    else
	      std::cout << "Did Not Get Correct Point!  Trying to find: " << toFind << " Found: " << (*found.best_data())->data << std::endl;
	  }
	  catch (const KDTException &e)
	  {
	    std::cout << "Caught BSTException in " << test;
	    int value = e.code();
	    if (value == KDTException::E_NO_MEMORY)
	      std::cout << "E_NO_MEMORY" << std::endl;
	    else
	      std::cout << "Unknown error code." << std::endl;
	  }
	  catch (...)
	  {
	    std::cout << "Caught unknown exception in " << test << std::endl;
	  }
	}

  void TestRemove()
  {
    const char *test = "Test6 - Remove";

    std::cout << "\n====================== " << test << " ======================\n";

    unsigned numPoints = 5;//250;
    std::vector<TestNode> pointsList;
    GetVectorOfPoints<3, unsigned>(pointsList, numPoints);

    try
    {
      KDTree<3, unsigned> tree(pointsList);

      std::cout << "Height: " << tree.GetHeight() << std::endl;
      std::cout << "Nodes: " << tree.GetSize() << std::endl;


      for (unsigned i = 0; i < pointsList.size(); ++i)
      {
        std::cout << "Removing Point: " << pointsList[i].split << std::endl;
        tree.Remove(pointsList[i].split);
        std::cout << "Height: " << tree.GetHeight() << std::endl;
        std::cout << "Nodes: " << tree.GetSize() << std::endl;
      }

      assert(tree.GetSize() == 0);
      assert(tree.GetRoot() == nullptr);
      assert(tree.GetDimension() == 3);
      assert(tree.Empty() == true);
      assert(tree.GetHeight() == 0);
    }
    catch (const KDTException &e)
    {
      std::cout << "Caught BSTException in " << test;
      int value = e.code();
      if (value == KDTException::E_NO_MEMORY)
        std::cout << "E_NO_MEMORY" << std::endl;
      else
        std::cout << "Unknown error code." << std::endl;
    }
    catch (...)
    {
      std::cout << "Caught unknown exception in " << test << std::endl;
    }
  }
	
	
	void TestKD(void)
	{
	  TestConstructor();
	  TestCopyConstructor();
	  TestAssignment();
	  TestConstructorGivenPointList();
	  TestConstructorGivenPointListWithDuplicates();
	  TestInsert();
    TestRemove();
	  TestFind();
	  TestNearestNeighbor();
	  TestKNearestNeighbors();

	}
	
}
