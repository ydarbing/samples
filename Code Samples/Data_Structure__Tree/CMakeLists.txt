cmake_minimum_required(VERSION 3.17)

project("Data_Structure__Tree")

set(header_files
   include/ObjectAllocator.h
   include/PRNG.h
   include/AVL/AVLTree.h
   include/AVL/AVLTree.hpp
   include/BS/BSTree.h
   include/BS/BSTree.hpp
   include/Spatial/DynamicAABB/DynamicAABBTree.h
   include/Spatial/DynamicAABB/DynamicAABBTree.hpp
   include/Spatial/KD/BoundedPriorityQueue.h
   include/Spatial/KD/BoundedPriorityQueue.hpp
   include/Spatial/KD/KDTree.h
   include/Spatial/KD/KDTree.hpp
   include/Spatial/KD/TestKD.h
)

set(source_files
   src/main.cpp
   src/ObjectAllocator.cpp
   src/PRNG.cpp
   src/Spatial/DynamicAABB/DynamicAABBTree.cpp
   src/Spatial/KD/TestKD.cpp
)


ADD_EXECUTABLE("${PROJECT_NAME}" 
               ${source_files} 
               ${header_files}
               )

target_include_directories("${PROJECT_NAME}" PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")

target_link_libraries("${PROJECT_NAME}" PRIVATE Maths)
