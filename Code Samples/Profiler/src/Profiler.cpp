// 
// File   Profiler.cpp
// Author Brady Reuter
//
// Brief - Instrumented profiler
// 
//  When using PROFILE or PROFILE_NAMED treat these like scope variables
//    meaning that profile time starts where declared and 
//    ends when goes out of scope
#include "Profiler.h"

#include <fstream> // log file
#include <iostream> // cout
#include <iomanip> // setw
#include <string> // to_string
#include <cassert>
#include <intrin.h>  //__rdtsc


namespace Profile
{
  // init profiler
  Profiler::ProfileData  Profiler::m_profiler;

  //////////////////////////////////////////////////////////////////////////
  //                     PROFILE SAMPLE
  //////////////////////////////////////////////////////////////////////////
  ProfileSample::ProfileSample(const char* name)
  {
    Profiler::StartProfile(name);
  }

  ProfileSample::~ProfileSample()
  {
    Profiler::StopProfile();
  }

  //////////////////////////////////////////////////////////////////////////
  //                     PROFILER
  //////////////////////////////////////////////////////////////////////////
  void Profiler::StartProfile(const char* name)
  {
    if (name != m_profiler.m_currentNode->GetName())
    {
      // check to see if there is already this child, if not make a new node
      m_profiler.m_currentNode = m_profiler.m_currentNode->GetSubNode(name);
    }
    m_profiler.m_currentNode->Call();
  }

  void Profiler::StopProfile()
  {
    // only go to parent if not recursive call
    if (m_profiler.m_currentNode->Return())
    {
      m_profiler.m_currentNode = m_profiler.m_currentNode->GetParent();
    }
  }

  void Profiler::Print()
  {
    // print the whole tree starting from the root
    m_profiler.m_root->Print(0);
  }

  // redirect std::cout to logfile
  void Profiler::Log()
  {
    // open new log file
    std::ofstream logFile;
    logFile.open("profile.log", std::ofstream::out | std::ofstream::trunc);

    // save the old cout buffer
    std::streambuf* coutBuf = std::cout.rdbuf();
    // redirect cout to the file
    std::cout.rdbuf(logFile.rdbuf());

    // print to new log file rather than console
    m_profiler.m_root->Print(0);

    // reset to standard output again
    std::cout.rdbuf(coutBuf);

    logFile.close();
  }


  //////////////////////////////////////////////////////////////////////////
  //                     PROFILE NODE
  //////////////////////////////////////////////////////////////////////////

  ProfileNode::ProfileNode(const char* name)
    : m_name(name), m_parent(nullptr), m_child(nullptr), m_sibling(nullptr),
    m_recusionCounter(0), m_totalCalls(0), m_totalTime(0),
    m_lastCallTime(0), m_lastCallStartTime(0),
    m_minTime(std::numeric_limits<proTime>::max()), m_maxTime(0)
  {
  }

  ProfileNode::~ProfileNode()
  {
    delete m_child;
    delete m_sibling;
  }

  ProfileNode* ProfileNode::CreateNode(const char* name)
  {
    ProfileNode* newNode = new ProfileNode(name);
    assert(newNode != nullptr);
    return newNode;
  }

  proTime ProfileNode::GetTime(void)
  {
    return __rdtsc();
  }

  void ProfileNode::AddChild(ProfileNode* newNode)
  {
    if (!newNode)
    {
      return;
    }
    // set parent to this 
    newNode->m_parent = this;

    if (m_child == nullptr)
    {
      // this node does not have a child, set newNode as child
      m_child = newNode;
      newNode->m_sibling = nullptr;
    }
    else
    {
      ProfileNode* lastChild = m_child;
      while (lastChild->m_sibling)
      {
        lastChild = lastChild->m_sibling;
      }
      lastChild->m_sibling = newNode;
      newNode->m_sibling = nullptr;
    }
  }

  void ProfileNode::Call()
  {
    ++m_totalCalls;

    proTime startTime = GetTime();
    if (m_recusionCounter == 0)
    {
      ++m_recusionCounter;
      m_startTime = startTime;
    }
    m_lastCallStartTime = startTime;
  }

  bool ProfileNode::Return()
  {
    --m_recusionCounter;
    if (m_recusionCounter == 0 && m_totalCalls != 0)
    {
      proTime returnTime = GetTime();
      m_lastCallTime = returnTime - m_lastCallStartTime;
      m_totalTime += returnTime - m_startTime;

      if (m_lastCallTime < m_minTime)
      {
        m_minTime = m_lastCallTime;
      }
      if (m_lastCallTime > m_maxTime)
      {
        m_maxTime = m_lastCallTime;
      }
    }
    return m_recusionCounter == 0;
  }


  void ProfileNode::Print(unsigned depth)
  {
    // TODO, create xml/html style for this instead of hard coded values
    const int funcWidth = 30;
    const int callsWidth = 6;
    const int cyclesWidth = 11;
    const int minMaxWitdth = 11;
    const int spacing = 5;

    if (depth == 0)
    {
      // root will not have any useful information, but good time to set column names
      std::string fn = "Function Name";
      std::cout << std::setw(funcWidth - fn.size() + spacing) << fn
        << std::setw(callsWidth + fn.size()) << "# Calls" << std::setw(spacing) << " "
        << std::setw(cyclesWidth) << "Total Cycles" << std::setw(spacing) << " "
        << std::setw(cyclesWidth) << "Last Call Cycles" << std::setw(spacing * 2) << " "
        << std::setw(cyclesWidth) << "Min Cycles" << std::setw(spacing * 2) << " "
        << std::setw(cyclesWidth) << "Max Cycles"
        << std::endl;
    }
    else
    {
      std::string indent((depth - 1) * 3, ' ');
      std::cout << indent
        << std::setw(funcWidth) << std::left << GetName()
        << std::setw(callsWidth) << std::right << std::to_string(m_totalCalls) << std::setw(spacing) << " "
        << std::setw(cyclesWidth) << std::right << std::to_string(m_totalTime) << std::setw(spacing * 2) << " "
        << std::setw(cyclesWidth) << std::right << std::to_string(m_lastCallTime) << std::setw(spacing * 2) << " "
        << std::setw(cyclesWidth) << std::right << std::to_string(m_minTime) << std::setw(spacing * 2) << " "
        << std::setw(cyclesWidth) << std::right << std::to_string(m_maxTime)
        << std::endl;
    }

    // recursively print out all children and siblings
    if (GetChild())
    {
      GetChild()->Print(depth + 1);
    }
    if (GetSibling())
    {
      GetSibling()->Print(depth);
    }
  }

  ProfileNode* ProfileNode::GetSubNode(const char* name)
  {
    ProfileNode* child = m_child;
    while (child)
    {
      if (child->m_name == name)
      {
        return child;
      }
      child = child->m_sibling;
    }

    // didn't find it, add it to end
    ProfileNode* newNode = CreateNode(name);
    AddChild(newNode);
    return newNode;
  }

  ProfileNode* ProfileNode::GetChild()
  {
    return m_child;
  }

  ProfileNode* ProfileNode::GetParent()
  {
    return m_parent;
  }

  ProfileNode* ProfileNode::GetSibling()
  {
    return m_sibling;
  }

  const char* ProfileNode::GetName()
  {
    return m_name;
  }

  //////////////////////////////////////////////////////////////////////////
  //                     PROFILE DATA
  //////////////////////////////////////////////////////////////////////////

  Profiler::ProfileData::ProfileData()
  {
    m_root = ProfileNode::CreateNode("root");
    m_currentNode = m_root;
  }

  // called when program is done
  Profiler::ProfileData::~ProfileData()
  {
    // if we profiled something make a log
    if (m_root->GetChild())
    {
      Profiler::Log();
    }

    delete m_root;
  }

}// namespace Profile

