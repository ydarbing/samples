// 
// File   Profiler.h
// Author Brady Reuter
//
// Brief - Instrumented profiler
//  User should only need to use macros
// 
//  When using PROFILE or PROFILE_NAMED treat these like scope variables
//    meaning that profile time starts where declared and 
//    ends when goes out of scope
#pragma once

namespace Profile
{
  typedef unsigned long long proTime;

  // Macros use this class as a simple way  
  // to start and end profiling of a function
  class ProfileSample
  {
  public:
    ProfileSample(const char* name);
    ~ProfileSample();
  };


  class ProfileNode
  {
  public:
    ProfileNode(const char* name);
    ~ProfileNode();

    static ProfileNode* CreateNode(const char* name);

    // function that is being profiled was called
    void Call();
    // function that was being profiled has finished, 
    // record stats and return true if the recursion counter is 0
    bool Return();

    // Print the 'depth' level of the call tree
    // recursively prints entire tree
    void Print(unsigned depth);

    const char*  GetName();
    ProfileNode* GetParent();
    ProfileNode* GetSibling();
    ProfileNode* GetChild();
    ProfileNode* GetSubNode(const char* nodeName);

  private:
    void AddChild(ProfileNode* child);
    inline proTime GetTime(void);

  protected:
    proTime m_startTime;
    proTime m_lastCallStartTime;
    proTime m_lastCallTime;
    proTime m_totalTime;
    proTime m_timePerFrame;
    proTime m_minTime;
    proTime m_maxTime;
    unsigned m_totalCalls;
    unsigned m_callsPerFrame;
    int      m_recusionCounter;

    const char* m_name;
    ProfileNode* m_parent;
    ProfileNode* m_child;
    ProfileNode* m_sibling;
  };


  class Profiler
  {
  public:
    static void StartProfile(const char* name);
    static void StopProfile();
    static void Print();
    static void Log();

    struct ProfileData
    {
      ProfileData();
      ~ProfileData();

      ProfileNode* m_root;
      ProfileNode* m_currentNode;
    };
  private:
    static ProfileData m_profiler;
  };
}// namespace Profile

// with no arguments just use function signature
#define PROFILE Profile::ProfileSample __profile(__FUNCTION__);
// allow custom naming for profiling
#define PROFILE_NAMED( name ) Profile::ProfileSample __profile( name );
// print out the current status of the profiler
#define PROFILE_PRINT Profile::Profiler::Print();
#define PROFILE_LOG   Profile::Profiler::Log();


