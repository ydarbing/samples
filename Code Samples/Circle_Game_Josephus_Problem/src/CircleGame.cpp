#include <iostream>
#include "CircleGame.h"


CircleGame::CircleGame(unsigned numChildren, unsigned stepsBeforeOut, unsigned survivors)
  : m_numPlayers(numChildren), m_steps(stepsBeforeOut), m_survivors(survivors)
{
}

CircleGame::~CircleGame()
{
}

void CircleGame::PlayGame()
{
  //for (unsigned i = 0; i < m_survivors; ++i)
  //  FindNextOut(m_numPlayers, m_steps, i);

  GetOutList(m_numPlayers, m_steps, m_survivors);
}

void CircleGame::SetNumPlayers(unsigned N)
{
  m_numPlayers = N;
}

// uses fast method described in "The Josephus Problem - Numberphile" youtube video
unsigned CircleGame::GetWinner()
{
  unsigned winner = m_numPlayers;
  // move last set bit to the beginning
  unsigned bit = MSB32(m_numPlayers) - 1;
  // set MSB to 0 
  winner &= ~(1 << bit);
  // left shift 1
  winner = winner << 1;
  // set first bit
  winner += 1;

  return winner;
}

//  n = num players
// k = steps before next player is out
// s = number of survivors at end of game
unsigned CircleGame::FindNextOut(unsigned n, unsigned k, unsigned s)
{
  unsigned i = s + 1;
  for (unsigned x = i; x <= n; ++x, ++i)
    s = (s + k) % i;

  return s;
}

//  n = num players
// k = steps before next player is out
// s = number of survivors at end of game
void CircleGame::GetOutList(unsigned n, unsigned k, unsigned s)
{
  //std::cout << "Out list: " << std::endl;

  m_circle.clear();
  m_circle.reserve(n);
  for (unsigned x = 1; x <= n; x++)
    m_circle.push_back(x);

  unsigned i = 0;
  while (m_circle.size() > s)
  {
    i += k;
    if (i >= m_circle.size()) 
      i %= m_circle.size();
    //std::cout << m_circle[i] << ", ";

    m_circle.erase(m_circle.begin() + i);
  }

  m_winner = *m_circle.begin();

  //std::cout << std::endl << "Winner(s): ";
  //while (s--)
  //  std::cout << *(m_circle.rbegin() + s);
}



void PlayGameVersion1()
{
  unsigned startCircleSize = 5;
  unsigned k = 2; // every k'th child loses

  std::vector<unsigned> children;
  children.resize(startCircleSize);

  unsigned curCircleSize = startCircleSize;
  unsigned curChild = 0;

  while (curCircleSize >= k)
  {
    for (unsigned j = 0; j < k; ++curChild)
    {
      if (curChild >= startCircleSize)
        curChild -= startCircleSize;
      if (children[curChild] == 0)
        ++j;
    }

    if (children[curChild - 1] == 0)
    {
      std::cout << curChild << " eliminated" << std::endl;
      children[curChild - 1] = 1;// child is knocked out of game
      --curCircleSize;
    }
  }

  // survivor list
  for (unsigned i = 0; i < startCircleSize; ++i)
    if (children[i] == 0)
      std::cout << i + 1 << std::endl;

}

