#include <iostream>
#include <assert.h>
#include "CircleGame.h"
#include "Profiler.h"


int main(int argc, char ** argv)
{
  CircleGame testWinner(1, 1);
  unsigned winner;
  unsigned numTests = 1000;
  unsigned i = 900;
  for(; i <= numTests; ++i)
  {
    testWinner.SetNumPlayers(i * i);
    
    {
      PROFILE_NAMED("FAST");
      winner = testWinner.GetWinner();
    }
    
    {
      PROFILE_NAMED("GAME");
      testWinner.PlayGame();
    }
    
    
    //std::cout << "Fast: " << winner << "  Played: " << testWinner.m_winner << std::endl;
    //assert(winner == testWinner.m_winner);

  }

  //CircleGame game(11, 3, 2);
  //
  //game.PlayGame();

  return 0;
}