#pragma once
#include <vector>
#include <unordered_set>

class CircleGame
{
public:
  unsigned m_winner; // for testing
  CircleGame(unsigned numPlayers, unsigned stepsBeforeOut = 1, unsigned numWinners = 1);
  ~CircleGame();

  void PlayGame();
  void SetNumPlayers(unsigned N);
  unsigned GetWinner();

protected:
  unsigned FindNextOut(unsigned n, unsigned k, unsigned s);
  void GetOutList(unsigned n, unsigned k, unsigned s);

  inline unsigned int MSB32(unsigned int x)
  {
    static const unsigned int bval[] =
    { 0,1,2,2,3,3,3,3,4,4,4,4,4,4,4,4 };

    unsigned int r = 0;
    if (x & 0xFFFF0000) { r += 16 / 1; x >>= 16 / 1; }
    if (x & 0x0000FF00) { r += 16 / 2; x >>= 16 / 2; }
    if (x & 0x000000F0) { r += 16 / 4; x >>= 16 / 4; }
    return r + bval[x];
  }

private:
  unsigned m_numPlayers;
  unsigned m_steps;
  unsigned m_survivors;
  std::vector<unsigned> m_circle;
  //std::unordered_set<unsigned> m_circle;
};
