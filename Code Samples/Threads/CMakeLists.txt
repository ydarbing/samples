cmake_minimum_required(VERSION 3.17)

project("Threads")

set(header_files
  include/ThreadPool.h
)
  
set(source_files
  src/main.cpp
  src/MutexExample.cpp
  src/ThreadPool.cpp
  )

add_executable("${PROJECT_NAME}" 
               ${source_files}
               ${header_files}
               )

target_include_directories("${PROJECT_NAME}" PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")
