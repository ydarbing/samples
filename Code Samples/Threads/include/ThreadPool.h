#pragma once
#include <assert.h>
#include <atomic>
#include <condition_variable>
#include <thread>
#include <mutex>

// N - 1 threads where N is the number of hardware threads available
#define MAX_THREAD_COUNT 15
// Determines the size of the thread pool itself in bytes, as a local array is used.
#define MAX_TASK_COUNT 1024
// Specify the number of bytes per cache-line
#define CACHE_LINE_SIZE 64
typedef __declspec(align(CACHE_LINE_SIZE)) std::thread Thread;

//#define YIELD_PROCESSOR _mm_pause


class Semaphore
{
public: 
  Semaphore(int size);
  // try to get the semaphore.  If can't it sleeps until possible
  void Wait();
  // Signal all sleeping threads.  Sets semaphore counter
  void SignalAll(int size);
  // used by main thread to decrement semaphore counter
  void Decrement();
  void SetSize(int size);

private:
  std::atomic<int> m_size;
  std::condition_variable m_condition;
  std::mutex m_mutex;
};


struct Task
{
  void(*kick)(void*);
  void* param;
};

class ThreadPool
{
  ThreadPool();
  ~ThreadPool();

  // queues a task.  Does not execute task
  void AddTask(Task task);
  // Releases all sleeping threads upon all queued tasks. Returns only
  // when all tasks are complete. Intended to be called by the main thread. 
  // The main thread executes tasks along with the pooled threads.
  void Kick();
  // Initializes threads within threadpool
  void Init();
  // Joins all threads in threadpool
  void Shutdown();
  
  inline int ThreadCount()const { return m_hintedThreadCount; }

private:
  bool TryPopTask(Task* task);
  void WaitForTask();

private:
  Task m_tasks[MAX_TASK_COUNT];
  Thread m_threads[MAX_THREAD_COUNT];
  Semaphore m_semaphore;
  std::atomic<int> m_taskCount;
  std::mutex m_mutex;
  int m_taskQueueCount;
  int m_hintedThreadCount;
  bool m_running;

};
