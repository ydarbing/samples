#include "ThreadPool.h"


Semaphore::Semaphore(int size) : m_size(size)
{
}

void Semaphore::Wait()
{
  std::unique_lock<std::mutex> lock(m_mutex);

  while (m_size == 0)
    m_condition.wait(lock);

  --m_size;
}

void Semaphore::SignalAll(int size)
{
  m_size = size;

  m_condition.notify_all();
}

void Semaphore::Decrement()
{
  --m_size;
}

void Semaphore::SetSize(int size)
{
  m_size = size;
}

ThreadPool::ThreadPool() : m_semaphore(0), m_running(false), m_taskQueueCount(0),
m_hintedThreadCount(std::thread::hardware_concurrency() - 1)
{
}

ThreadPool::~ThreadPool()
{
  Shutdown();
}

void ThreadPool::AddTask(Task task)
{
  m_mutex.lock();

  assert(m_taskQueueCount < MAX_TASK_COUNT);

  m_tasks[m_taskQueueCount++] = task;
  ++m_taskCount;

  m_mutex.unlock();
}

void ThreadPool::Kick()
{
  int size = m_taskCount.load();

  if (size)
    m_semaphore.SignalAll(size);

  while (m_taskCount != 0)
  {
    Task task;

    if (TryPopTask(&task))
    {
      task.kick(task.param);
      m_semaphore.Decrement();
      --m_taskCount;
    }

	std::this_thread::yield();
    //YIELD_PROCESSOR();
  }
}

void ThreadPool::Init()
{
  m_taskCount = 0;
  m_taskQueueCount = 0;
  m_semaphore.SetSize(0);
  m_running = true;

  // if this gets hit, raise MAX_THREAD_COUNT
  assert(m_hintedThreadCount <= MAX_THREAD_COUNT);

  for (int i = 0; i < m_hintedThreadCount; ++i)
    m_threads[i] = std::thread(&ThreadPool::WaitForTask, this);
}

void ThreadPool::Shutdown()
{
  if (m_running)
  {
    m_running = false;
    m_semaphore.SignalAll(m_hintedThreadCount);
    for (int i = 0; i < m_hintedThreadCount; ++i)
      m_threads[i].join();
  }
}

bool ThreadPool::TryPopTask(Task* task)
{
  m_mutex.lock();

  if (m_taskQueueCount)
  {
    *task = m_tasks[--m_taskQueueCount];
    m_mutex.unlock();
    return true;
  }

  m_mutex.unlock();

  return false;
}

void ThreadPool::WaitForTask()
{
  while (m_running)
  {
    Task task;
    if (TryPopTask(&task))
    {
      task.kick(task.param);
      --m_taskCount;
    }

    m_semaphore.Wait();
  }
}
