#include "Craps.h"
#include <iostream>
#include <stdlib.h>

int RollDice()
{
  int die1 = (rand() % 6) + 1;
  int die2 = (rand() % 6) + 1;
  return die1 + die2;
}

bool PlayGame()
{
  int roll = RollDice();
  std::cout << "You rolled: " << roll << std::endl;
  if (roll == 7 || roll == 11)
    return true;
  else if (roll == 2 || roll == 3 || roll == 12)
    return false;

  std::cout << "Your point is: " << roll << std::endl;
  int point = roll;

  while (roll != 7)
  {
    roll = RollDice();
    std::cout << "You rolled: " << roll << std::endl;
    if (roll == point)
    {
      return true;
    }
  }

  return false;
}


