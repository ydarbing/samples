#include "DealCards.h"

#include <iostream>
#include <stdlib.h>

#define NUM_SUITS 4
#define NUM_RANKS 13

void DealCards(int numCards)
{
  bool inHand[NUM_SUITS][NUM_RANKS] = { false };
  const int Ranks[] = { '2', '3', '4', '5', '6', '7', '8', '9', '10', 'j', 'q', 'k', 'a' };
  const int Suits[] = { 's', 'c', 'h', 'd' };
  int rank, suit;

  //std::cout << "Enter the number of cards in hand: ";
  //std::cin >> numCards;
  //numCards = 5;
  if (numCards > 52)
    numCards = 52;

  std::cout << "Your hand: ";

  while (numCards > 0)
  {
    suit = rand() % NUM_SUITS;
    rank = rand() % NUM_RANKS;

    if (inHand[suit][rank] == false)
    {
      inHand[suit][rank] = true;
      --numCards;
      std::cout << " " << Ranks[rank] << Suits[suit];
    }
  }
  std::cout << std::endl;
}

