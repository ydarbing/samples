#include <iostream>
#include <stdlib.h>//srand, rand
#include <ctime>

#include "Craps.h"
#include "DealCards.h"

int main(int argc, char** argv)
{
  srand((unsigned)time(nullptr));

  int numCards = (rand() % 52) + 1;
  std::cout << "Going to deal " << numCards << " cards." << std::endl;
  DealCards(numCards);

  int wins = 0;
  int losses = 0;
  bool play = true;
  while (play)
  {
    bool won = PlayGame();
    if (won)
    {
      std::cout << "You win!" << std::endl;
      ++wins;
    }
    else
    {
      std::cout << "You lose!" << std::endl;
      ++losses;
    }

    char again[10];
    std::cout << "Play again? ";
    std::cin >> again;

    if (toupper(again[0]) != 'Y')
    {
      play = false;
    }
    std::cout << std::endl;
  }

  std::cout << "Wins: " << wins << "  Losses: " << losses;

  return 0;
}