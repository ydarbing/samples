#include "SplitList.h"
#include "BList/BListTest.h"

int main(int argc, const char * argv[])
{

  // Split list
  Test_SimpleIncreasingNumbers();
  Test_NULL();
  Test_OnlyEven();
  Test_OnlyOdd();
  Test_Random();

  // BList
  Test_BList();

  return 0;
}