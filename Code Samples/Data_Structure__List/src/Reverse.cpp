#include "Reverse.h"
#include <iostream>

void PrintReverse(Node* list)
{
  if (list != nullptr)
  {
    PrintReverse(list->next);
    std::cout << list->data << std::endl;
  }
}

Node* ReverseInPlace(Node* list)
{
  Node* reversedListHead = nullptr;
  Node* restOfOriginalList = list;
  // build reversed like a stack while original remains
  while (restOfOriginalList != nullptr)
  {
    Node* originalListTail = restOfOriginalList->next;
    // head of remainder will be new head of the reversed list
    restOfOriginalList->next = reversedListHead;
    reversedListHead = restOfOriginalList;
    restOfOriginalList = originalListTail;
  }

  return reversedListHead;
}
