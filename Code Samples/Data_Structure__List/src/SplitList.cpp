#include "SplitList.h"

#include <stdio.h>
#include <iostream>
#include <cstdlib> // rand
//#include "Profiler.h"
//#include "vld.h"



Node* Split(Node** list)
{
  //PROFILE
    // This function does not allocate new memory, but DOES modify the input list.
    // The result of the split function should be:
    //		1. The input list should now point to the head of a list that contains only the even data values.
    //		2. Return the head pointer to a list containing only the odd data values from the input list.
    //		3. The new lists should preserve the order found in the original list.

    // Given a valid list
    if (list == NULL || *list == NULL)
    {
      return NULL;
    }
  Node* oddList = NULL;
  Node* evenList = NULL;
  // pointer to remember where the beginning is
  Node* beginningOfEven = NULL;
  Node* beginningOfOdd = NULL;

  while ((*list))
  {
    if ((*list)->data % 2 == 1) // odd value
    {
      if (oddList == NULL)
      {
        oddList = (*list);
        beginningOfOdd = oddList;
      }
      else
      {
        oddList->next = (*list);
        oddList = oddList->next;
      }
    }
    else // even value found
    {
      if (evenList == NULL)
      {
        evenList = (*list);
        beginningOfEven = evenList;
      }
      else
      {
        evenList->next = (*list);
        evenList = evenList->next;
      }
    }

    (*list) = (*list)->next;
  }
  // make sure the next nodes are not set to garbage
  if (evenList)
  {
    evenList->next = NULL;
  }
  if (oddList)
  {
    oddList->next = NULL;
  }
  // set input to the modified values and return odd list
  *list = beginningOfEven;
  return beginningOfOdd;
}

/******************************************
UTILITIES
/***************************************** */
static void AddNode(Node** headRef, int value)
{
  Node* newNode = new Node;
  newNode->data = value;

  // link the old list off the new node 
  newNode->next = (*headRef);

  // move the head to point to the new node
  (*headRef) = newNode;
}

static void CleanUp(Node** even, Node** odd)
{
  while (*even)
  {
    Node* temp = (*even)->next;
    delete (*even);
    *even = temp;
  }

  while (*odd)
  {
    Node* temp = (*odd)->next;
    delete (*odd);
    *odd = temp;
  }
}
static void PrePrint(Node* list)
{
  std::cout << "Given List: ";
  while (list)
  {
    std::cout << list->data << ", ";
    list = list->next;
  }
  std::cout << std::endl;
}
static void Print(Node* even, Node* odd)
{
  std::cout << "Even: ";
  while (even)
  {
    std::cout << even->data << ", ";
    even = even->next;
  }
  std::cout << std::endl;
  std::cout << "Odd: ";
  while (odd)
  {
    std::cout << odd->data << ", ";
    odd = odd->next;
  }
  std::cout << std::endl << std::endl;
}


/******************************************
TESTS
/***************************************** */
void Test_SimpleIncreasingNumbers()
{
  std::cout << "Test_SimpleIncreasingNumbers" << std::endl;
  Node* head = NULL;
  for (unsigned i = 0; i < 10; ++i)
  {
    AddNode(&head, 10 - i);
  }

  PrePrint(head);
  Node* oddList = Split(&head);

  Print(head, oddList);
  CleanUp(&head, &oddList);
}


void Test_NULL()
{
  std::cout << "Test_NULL" << std::endl;
  Node* head = NULL;
  PrePrint(head);
  Node* oddList = Split(&head);

  Print(head, oddList);
}


void Test_OnlyEven()
{
  std::cout << "Test_OnlyEven" << std::endl;
  Node* head = NULL;
  for (unsigned i = 0; i < 20; i += 2)
  {
    AddNode(&head, i);
  }

  PrePrint(head);
  Node* oddList = Split(&head);

  Print(head, oddList);
  CleanUp(&head, &oddList);
}

void Test_OnlyOdd()
{
  std::cout << "Test_OnlyOdd" << std::endl;
  Node* head = NULL;
  for (unsigned i = 1; i < 20; i += 2)
  {
    AddNode(&head, i);
  }

  PrePrint(head);
  Node* oddList = Split(&head);

  Print(head, oddList);
  CleanUp(&head, &oddList);
}

void Test_Random()
{
  std::cout << "Test_Random" << std::endl;
  Node* head = NULL;
  unsigned numElements = 15;
  for (unsigned i = 0; i < numElements; ++i)
  {
    AddNode(&head, std::rand() % numElements);
  }

  PrePrint(head);
  Node* oddList = Split(&head);

  Print(head, oddList);
  CleanUp(&head, &oddList);
}

