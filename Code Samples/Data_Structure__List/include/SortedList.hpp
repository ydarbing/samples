

/*********************************************************************/
/*!
\brief
get the size of the node

\return
the size of the node

*/
/***********************************************************************/
// static
template <typename T, typename Pred>
unsigned SortedList<T, Pred>::nodesize(void)
{
  return sizeof(Node);
}
//******************************************************************************
//******************************************************************************

/*********************************************************************/
/*!
\brief
Get the size of the list

\return
the size of the list
*/
/***********************************************************************/
template <typename T, typename Pred>
unsigned SortedList<T, Pred>::size(void) const throw()
{
  return size_;
}



/*********************************************************************/
/*!
\brief
Empty the list
*/
/***********************************************************************/
template <typename T, typename Pred>
void SortedList<T, Pred>::clear(void) throw()
{
  if (head_ != NULL)
  {
    Node* current = head_;

    while (current != NULL)
    {
      Node* temp = current->Next_;

      current->~Node();
      objAllocator_->Free(current);

      current = temp;
    }
  }
  size_ = 0;
}


/*********************************************************************/
/*!
\brief
Constructor
*/
/***********************************************************************/
template <typename T, typename Pred>
SortedList<T, Pred>::SortedList(Pred sorter, ObjectAllocator *Allocator, bool SharedAllocator)
{
  head_ = NULL;
  tail_ = NULL;
  size_ = 0;
  if (Allocator == NULL)
  {
    OAConfig config(true);
    objAllocator_ = new ObjectAllocator(sizeof(Node), config);
    SharedAllocatorCreator_ = true;
  }
  else
  {
    objAllocator_ = Allocator;
    SharedAllocatorCreator_ = false;
  }
  //dummyHead = reinterpret_cast<Node*>( objAllocator_->Allocate() );
  //dummyTail = reinterpret_cast<Node*>( objAllocator_->Allocate() );
  //dummyHead->Next_ = dummyTail;
  //dummyTail->Prev_ = dummyHead;

  typeOfSort_ = sorter;
  SharedAllocator_ = SharedAllocator;
}

/*********************************************************************/
/*!
\brief
Copy Constructor
*/
/***********************************************************************/
template <typename T, typename Pred>
SortedList<T, Pred>::SortedList(const SortedList &rhs) throw(SortedListException)
{
  if (rhs.SharedAllocator_ == true)
  {
    objAllocator_ = rhs.objAllocator_; // Use rhs' allocator
    SharedAllocatorCreator_ = false;  // We don't own it (won't free it)
    SharedAllocator_ = true;          // If a copy of 'this object' is made, share the allocator
  }
  else
  {
    try
    {
      OAConfig config(true); // Set UseCPPMemManager to true
      objAllocator_ = new ObjectAllocator(sizeof(Node), config);
    }
    catch (const OAException &e)
    {
      throw(SortedListException(SortedListException::E_NO_MEMORY, e.what()));
    }

    SharedAllocatorCreator_ = true;  // We own the allocator, we will have to free it
    SharedAllocator_ = false; // Do not share this allocator with any other list
  }

  //dummyHead = reinterpret_cast<Node*>( objAllocator_->Allocate() );
  //dummyTail = reinterpret_cast<Node*>( objAllocator_->Allocate() );
  //dummyHead->Next_ = dummyTail;
  //dummyTail->Prev_ = dummyHead;

  head_ = NULL;
  tail_ = NULL;
  size_ = 0;

  *this += rhs; // this will allocate new stuff and set the size correctly
}

/*********************************************************************/
/*!
\brief
Assignment operator

\param rhs
the sorted list that will be put into the new one

\return
reference to the new sorted list
*/
/***********************************************************************/
template <typename T, typename Pred>
SortedList<T, Pred>& SortedList<T, Pred>::operator=(const SortedList &rhs) throw(SortedListException)
{
  if (this == &rhs)
    return *this;
  else
  {
    clear(); // empty the current list

    if (rhs.SharedAllocator_ == true)
    {
      objAllocator_ = rhs.objAllocator_; // Use rhs' allocator
      SharedAllocatorCreator_ = false;  // We don't own it (won't free it)
      SharedAllocator_ = true;          // If a copy of 'this object' is made, share the allocator
    }
    else
    {
      try
      {
        OAConfig config(true); // Set UseCPPMemManager to true
        objAllocator_ = new ObjectAllocator(sizeof(Node), config);
      }
      catch (const OAException &e)
      {
        throw(SortedListException(SortedListException::E_NO_MEMORY, e.what()));
      }

      SharedAllocatorCreator_ = true;  // We own the allocator, we will have to free it
      SharedAllocator_ = false; // Do not share this allocator with any other list
    }

    //dummyHead = reinterpret_cast<Node*>( objAllocator_->Allocate() );
    //dummyTail = reinterpret_cast<Node*>( objAllocator_->Allocate() );
    //dummyHead->Next_ = dummyTail;
    //dummyTail->Prev_ = dummyHead;

    head_ = NULL;
    tail_ = NULL;
    *this += rhs; // this will allocate new stuff and set the size correctly

    return *this;
  }
}

template <typename T, typename Pred>
SortedList<T, Pred>& SortedList<T, Pred>::operator+=(const SortedList &rhs) throw(SortedListException)
{
  Node* CurrentNode = rhs.head_;

  while (CurrentNode)
  {
    push_back(CurrentNode->Data_);

    CurrentNode = CurrentNode->Next_;
  }
  return *this;
}

/*********************************************************************/
/*!
\brief
Destructor
*/
/***********************************************************************/
template <typename T, typename Pred>
SortedList<T, Pred>::~SortedList() throw()
{
  clear();

  if (SharedAllocatorCreator_ == true)
    delete objAllocator_;
}


/*********************************************************************/
/*!
\brief
Add a new node to the back of the list

\param value
the new data of the node

*/
/***********************************************************************/
template <typename T, typename Pred>
void SortedList<T, Pred>::push_back(const T& value) throw(SortedListException)
{
  // create a new node
  Node* pNode;
  try
  {
    Node* temp = New_Node(value, tail_);
    pNode = temp;
  }
  catch (const OAException &e)
  {
    throw(SortedListException(SortedListException::E_NO_MEMORY, e.what()));
  }

  ++size_; // increment size

  if (head_ == NULL || tail_ == NULL)// there is nothing in the list
  {
    head_ = pNode;
    tail_ = head_;
  }
  else // list not empty push new node to the back of list
  {
    // put the newly created node at the end of the list
    tail_->Next_ = pNode;
    // set the new tail to the newest node
    tail_ = pNode;
  }
}


/*********************************************************************/
/*!
\brief
Add a new node to the list where it will be in sorted order

\param value
the new data of the node

*/
/***********************************************************************/
template <typename T, typename Pred>
void SortedList<T, Pred>::insert(const T& value) throw(SortedListException)
{
  Node* tempNode;
  //  check if inserting into empty list
  if (head_ == NULL)
  {
    tempNode = New_Node(value, NULL);
    head_ = tempNode;
    tail_ = tempNode;
    ++size_;
  }
  else // non-empty list
  {
    Node* nextNode = head_->Next_;
    Node* actNode = head_;

    // loop through list until the correct position is found
    while (nextNode != NULL)
    {
      if (typeOfSort_(nextNode->Data_, value))
        break; // the next number is too large or too small insert now
      nextNode = nextNode->Next_;
      actNode = actNode->Next_;
    }
    // the last node was reached, insert at end of list
    if (nextNode == NULL)
    {
      push_back(value);
    }
    else
    {
      tempNode = New_Node(value, NULL);
      // Need to insert at front of list
      if (actNode->Data_ > value)
      {
        tempNode->Next_ = head_;
        head_ = tempNode;
        // set a new previous so reverse iter can reach the new head
        head_->Next_->Prev_ = head_;
      }
      else // insert not at front of list
      {
        nextNode->Prev_ = tempNode;
        actNode->Next_ = tempNode;

        tempNode->Prev_ = actNode;
        tempNode->Next_ = nextNode;
      }
      ++size_;
    }
  }
}

/*********************************************************************/
/*!
\brief
Creates a new node for the list

\param value
the new data of the node

\param prev
if known, the node that will come before the new one

\param next
if known, the node that will come after the new one

\return
a pointer to the new node created

*/
/***********************************************************************/
template <typename T, typename Pred>
typename SortedList<T, Pred>::Node* SortedList<T, Pred>::New_Node(const T& value, Node* prev, Node* next) const throw(SortedListException)
{
  Node* mem;
  try
  {
    Node* node = reinterpret_cast<Node*>(objAllocator_->Allocate()); // allocate the new node
    mem = new(node)Node(value);// Construct the object in the memory.
  }
  catch (const OAException &e)
  {
    throw(SortedListException(SortedListException::E_NO_MEMORY, e.what()));
  }

  mem->Prev_ = prev;
  mem->Next_ = next;

  return mem;
}


/*********************************************************************/
/*!
\brief
Sort the linked list based off a selection sort algorithm

\param fn
how it will be sorted.  ie low to high or high to low
*/
/***********************************************************************/
template <typename T, typename Pred>
template <typename Sorter>
void SortedList<T, Pred>::selection_sort(Sorter fn)
{
  Node* i, *j, *small;

  // loop though list
  for (i = head_->Next_; i->Next_ != NULL; i = i->Next_)
  {
    small = i;
    // loop though the list again
    for (j = i->Next_; j != NULL; j = j->Next_)
    {
      if (fn(j->Data_, small->Data_))
        small = j;
    }
    // swap the nodes
    //SwapNodes(i, small);
  }
}

/*********************************************************************/
/*!
\brief
Swap two nodes in the list

\param a
one of the nodes to be swapped

\param b
other node to be swapped
*/
/***********************************************************************/
template <typename T, typename Pred>
void SortedList<T, Pred>::SwapNodes(Node* a, Node* b)
{
  /*
  if( a->Next_ == b  || b->Next_ == a || a == head_ || b == head_ || a->Next_ == NULL || b->Next == NULL)
  {
    if(a == head_ || b == head_)
      head_ = b;
    else
      a->Prev_->Next_ = b;
  
    b->Prev_ = a->Prev_;
    a->Prev_ = b;
    a->Next_ = b->Next_;
    b->Next_->Prev_ = a;
    b->Next_ = a;
  }
  else
  {
    
  }
  */
}



/*********************************************************************/
/*!
\brief
Sort the linked list based off a merge sort algorithm

\param fn
how it will be sorted.  ie low to high or high to low
*/
/***********************************************************************/
template <typename T, typename Pred>
template <typename Sorter>
void SortedList<T, Pred>::merge_sort(Sorter fn)
{
  if (head_ == NULL)
    return;

  int chunkSize = 1;
  Node* left = head_;
  Node* right = head_;
  Node* dummy_H = head_;
  //Node* dummyTail = head_;

  while (right != NULL)
  {
    right = right->Next_ + chunkSize;

    if (fn(right->Data_, left->Data_))
      dummy_H->Next_ = right;
    break;
  }
}


/*!
\brief
Get the first node on the list

\return
a const iterator that is at the beginning of the list

*/
/***********************************************************************/
// iterator support
template <typename T, typename Pred>
typename SortedList<T, Pred>::const_iterator SortedList<T, Pred>::begin(void) const
{
  return const_iterator(head_);
}

/*********************************************************************/
/*!
\brief
Get the last node on the list

\return
a const iterator that is at the end of the list

*/
/***********************************************************************/
template <typename T, typename Pred>
typename SortedList<T, Pred>::const_iterator SortedList<T, Pred>::end(void) const
{
  return const_iterator(NULL);
}

/*********************************************************************/
/*!
\brief
Get the first node on the list

\return
a const reverse iterator that is at the beginning of the list

*/
/***********************************************************************/
template <typename T, typename Pred>
typename SortedList<T, Pred>::const_iterator SortedList<T, Pred>::rbegin(void) const
{
  return const_iterator(tail_);
}

/*********************************************************************/
/*!
\brief
Get the last node on the list

\return
a const reverse iterator that is at the end of the list

*/
/***********************************************************************/
template <typename T, typename Pred>
typename SortedList<T, Pred>::const_iterator SortedList<T, Pred>::rend(void) const
{
  return const_iterator(NULL);
}


//******************************************************************************
//******************************************************************************
// const_iterator
/*********************************************************************/
/*!
\brief
Conversion constructor

*/
/***********************************************************************/
template <typename T, typename Pred>
SortedList<T, Pred>::const_iterator::const_iterator(Node *p) : current_(p)
{
}

/*********************************************************************/
/*!
\brief
dereference operator

\return
the data type
*/
/***********************************************************************/
template <typename T, typename Pred>
const T& SortedList<T, Pred>::const_iterator::operator*(void) const
{
  return current_->Data_;
}

/*********************************************************************/
/*!
\brief
Comparison operator

\return
if the conparison was not equal return true
*/
/***********************************************************************/
template <typename T, typename Pred>
bool SortedList<T, Pred>::const_iterator::operator!=(const const_iterator &rhs) const
{
  return (current_ != rhs.current_);
}

/*********************************************************************/
/*!
\brief
pre-increment operator

\return
const iterator to the next element in the list
*/
/***********************************************************************/
template <typename T, typename Pred>
typename SortedList<T, Pred>::const_iterator& SortedList<T, Pred>::const_iterator::operator++(void)
{
  current_ = current_->Next_;
  return *this;
}


/*********************************************************************/
/*!
\brief
pre-decrement operator

\return
const iterator to the previous element in the list
*/
/***********************************************************************/
template <typename T, typename Pred>
typename SortedList<T, Pred>::const_iterator& SortedList<T, Pred>::const_iterator::operator--(void)
{
  current_ = current_->Prev_;
  return *this;
}



