/*
BList.cpp
Brady Reuter
1/26/2015
Doubly linked list where each node contains an array
inserting into list guarantees sorted
pushing front and back does not
*/

/*********************************************************************/
/*!
\brief
get the size of the node
\return
size of node
*/
/***********************************************************************/
template <typename T, unsigned Size>
unsigned BList<T, Size>::nodesize(void)
{
  return sizeof(BNode);
}

/*********************************************************************/
/*!
\brief
get the head of the BList
\return
head pointer
*/
/***********************************************************************/
template <typename T, unsigned Size>
const typename BList<T, Size>::BNode* BList<T, Size>::GetHead() const
{
  return head_;
}
/*********************************************************************/
/*!
\brief
get the stats of the BList
\return
list stats
*/
/***********************************************************************/
template <typename T, unsigned Size>
BListStats BList<T, Size>::GetStats() const
{
  return m_stats;
}

/*********************************************************************/
/*!
\brief
Default constructor for the BList
*/
/***********************************************************************/
template <typename T, unsigned Size>
BList<T, Size>::BList() : head_(NULL), tail_(NULL), m_stats()
{
  m_stats.ArraySize = Size;
}

/*********************************************************************/
/*!
\brief
Copy constructor for the BList
\param rhs
the list that is already constructed and all data will be put
into this new BList
*/
/***********************************************************************/
template <typename T, unsigned Size>
BList<T, Size>::BList(const BList &rhs) : head_(NULL), tail_(NULL)
{
  BNode* temp = rhs.head_;
  BNode* newNode = NULL;
  m_stats = rhs.m_stats;

  if (temp != NULL)
    AddAndConnct(newNode, temp->values[0], true);
  // have a head now just copy rest of rhs into this
  while (temp != NULL)
  {
    newNode->count = temp->count;
    // copy rhs array into this array
    for (unsigned i = 0; i < temp->count; ++i)
      newNode->values[i] = temp->values[i];

    // create the next node from rhs
    temp = temp->next;
    if (temp != NULL)
      AddAndConnct(newNode, temp->values[0], false);
  }
}

/*********************************************************************/
/*!
\brief
destructor for the BList
*/
/***********************************************************************/
template <typename T, unsigned Size>
BList<T, Size>::~BList()
{
  // delete all of the nodes
  clear();
}
/*********************************************************************/
/*!
\brief
Assignment operator for the BList
\param rhs
the list that is already constructed and all data will be put
into this new BList
*/
/***********************************************************************/
template <typename T, unsigned Size>
BList<T, Size>& BList<T, Size>::operator=(const BList &rhs) throw(BListException)
{
  if (this != &rhs)
  {
    m_stats.ArraySize = rhs.m_stats.ArraySize;
    // clear the current BList
    clear();
    BNode* temp = rhs.head_;
    BNode* newNode = NULL;

    m_stats = rhs.m_stats;
    if (temp != NULL)
      AddAndConnct(newNode, temp->values[0], true);
    // have a head now just copy rest of rhs into this
    while (temp != NULL)
    {
      newNode->count = temp->count;
      // copy rhs array into this array
      for (unsigned i = 0; i < temp->count; ++i)
        newNode->values[i] = temp->values[i];

      // create the next node from rhs
      temp = temp->next;
      if (temp != NULL)
        AddAndConnct(newNode, temp->values[0], false);
    }
  }
  return *this;
}

/*********************************************************************/
/*!
\brief
Allocates memory for a new node, going to be pushed to end of list
\param node
pointer to the newly created node
\param value
first value in new node's array
\param first
is it being inserted into an empty BList
\exception BListException
throws exception if no more memory
*/
/***********************************************************************/
template <typename T, unsigned Size>
void BList<T, Size>::AddAndConnct(BNode*& node, const T& value, const bool first) throw(BListException)
{
  try{// since we had to add a new node
    node = NewNode(value);
  }
  catch (const BListException &e){
    throw(BListException(BListException::E_NO_MEMORY, e.what()));
  }
  if (first)
  {
    head_ = node;
    tail_ = head_;
  }
  else
  {  // modify tail pointers so newnode is at end of list
    node->prev = tail_;
    tail_->next = node;
    tail_ = node;
  }
}

/*********************************************************************/
/*!
\brief
Push value onto the back of the BList
\param value
Newly added value to the end of BList
\exception BListException
throws exception if no more memory
*/
/***********************************************************************/
template <typename T, unsigned Size>
void BList<T, Size>::push_back(const T& value) throw(BListException)
{
  // create a new node
  BNode* newNode = NULL;
  bool addedNewNode = false;
  // the list is empty
  if (head_ == NULL || tail_ == NULL)
  {
    AddAndConnct(newNode, value, true);
    addedNewNode = true;
  }
  else// not an empty list
  {
    if (tail_->count == Size)
    {
      AddAndConnct(newNode, value, false);
      addedNewNode = true;
    }
    else
    {
      // don't need a new node so just push the new value into array
      tail_->values[tail_->count++] = value;
    }
  }
  // update BListStats
  IncrementStats(addedNewNode);
}

/*********************************************************************/
/*!
\brief
Push value onto the front of the BList
\param value
Newly added value to the front of BList
\exception BListException
throws exception if no more memory
*/
/***********************************************************************/
template <typename T, unsigned Size>
void BList<T, Size>::push_front(const T& value) throw(BListException)
{
  // create a new node
  BNode* newNode = NULL;
  bool addedNewNode = false;

  // check if empty list
  if (head_ == NULL || tail_ == NULL)
  {
    AddAndConnct(newNode, value, true);
    addedNewNode = true;
  }
  else
  {
    if (head_->count == Size)
    {
      try{
        newNode = NewNode(value);
      }
      catch (const BListException &e)
      {
        throw(BListException(BListException::E_NO_MEMORY, e.what()));
      }
      addedNewNode = true;
      // modify head pointers so newnode is in front of it
      newNode->next = head_;
      head_->prev = newNode;
      head_ = newNode;
    }
    else// don't need a new node so just push the new value into array
      PushIntoArray(head_, value, 0);
  }
  // update BListStats
  IncrementStats(addedNewNode);
}


/*********************************************************************/
/*!
\brief
Pushes a value to the front of an array  (not efficient)
\param node
the node who's array we will push front on
\param value
the value that is being pushed to the front of the array
\param startIndex
postion in array where new value will to placed
*/
/***********************************************************************/
template <typename T, unsigned Size>
void BList<T, Size>::PushIntoArray(BNode* node, const T& value, int startIndex)
{
  for (int i = node->count - 1; i >= startIndex; --i)
  {
    // swap i and i + 1
    const T temp = node->values[i];
    node->values[i] = node->values[i + 1];
    node->values[i + 1] = temp;
  }
  node->values[startIndex] = value;
  ++node->count;
}

/*********************************************************************/
/*!
\brief
Updates BList stats
\param addedNewNode
true if had to add a new node
*/
/***********************************************************************/
template <typename T, unsigned Size>
void BList<T, Size>::IncrementStats(bool addedNewNode)
{
  if (addedNewNode)
    ++m_stats.NodeCount;

  ++m_stats.ItemCount;
}

/*********************************************************************/
/*!
\brief
Allocates memory for a new node being placed on BList
\param Item
items value
\exception BListException
throws exception if no more memory
*/
/***********************************************************************/
template <typename T, unsigned Size>
typename BList<T, Size>::BNode* BList<T, Size>::NewNode(const T& Item) const throw(BListException)
{
  BNode* node = 0;
  try
  {
    node = new BNode; // might throw a std::bad_alloc or something else
    node->values[0] = Item;
    node->count = 1;
  }
  catch (const std::exception& e)
  {
    throw(BListException(BListException::E_NO_MEMORY, e.what()));
  }

  return node;
}

/*********************************************************************/
/*!
\brief
Inserts a value into the BList, may or may not have to create
a new node
\param value
this value determines where on the BList it will be placed
\exception BListException
throws exception if no more memory
*/
/***********************************************************************/
template <typename T, unsigned Size>
void BList<T, Size>::insert(const T& value) throw(BListException)
{
  // create a new node
  BNode* newNode = NULL;
  bool addedNewNode = false;

  // check if empty list
  if (head_ == NULL || tail_ == NULL)
  {
    AddAndConnct(newNode, value, true);
    addedNewNode = true;
  }
  else
  {
    BNode* rightNode = head_;
    FindBestNode(rightNode, addedNewNode, value);
    if (addedNewNode)
      SplitNode(rightNode, value);
  }

  IncrementStats(addedNewNode);
}

/*********************************************************************/
/*!
\brief
Find node with correct array of number to insert into (starting at head)
\param node
correct node to split if splitNode is true
\param splitNode
correct node is already full, if true node->count is not incremented here
\param value
determines where on the BList it will be inserted
*/
/***********************************************************************/
template <typename T, unsigned Size>
void BList<T, Size>::FindBestNode(BNode*& node, bool& splitNode, const T& value)
{
  splitNode = false;
  // quickly test to see if value is larger than largest value in BList
  if (tail_->values[tail_->count - 1] < value)
  {
    // make return node the one we want to split
    node = tail_;
    if (node->count == Size)
      splitNode = true;
    else// just have to push back
      node->values[node->count++] = value;

    return;
  }

  while (node != NULL)
  {
    // check if it can fit into this node
    if (value < node->values[node->count - 1])
    {
      // need to split the node
      if (node->count == Size)
      {// going to split this node in different function
        // spliting the node will handle node counts
        splitNode = true;
        return;
      }
      else // can just insert into this array, no splitting needed
      {
        for (unsigned i = 0; i < node->count; ++i)
        {
          if (value < node->values[i])
          {
            PushIntoArray(node, value, i);// function increments the node count
            return;
          }
        }
      }
    }
    // next node
    node = node->next;
    // check if value should actually be before this node
    if (node != NULL)
    {
      if (value < node->values[0])
      {// value belongs in beginning of node or end of prev node
        // check order
        // 1. check if left has room at end 
        // 2. check if right has room at beginning
        // 3. else split node
        node = node->prev;
        if (node->count != Size)
          node->values[node->count++] = value;
        else if (node->next->count != Size)
          PushIntoArray(node->next, value, 0);
        else
          splitNode = true;

        return;
      }
    }
  }
}

/*********************************************************************/
/*!
\brief
Splits one node into 2 because uder inserted into a full node
\param node
node that needs to be slit  i.e value should be in this node
\param value
the value that is being inserted
\exception BListException
throws exception if no more memory
*/
/***********************************************************************/
template <typename T, unsigned Size>
void BList<T, Size>::SplitNode(BNode* node, const T& value) throw(BListException)
{
  // find where in this nodes array value should be placed
  // since we know this node is full we can loop through the whole thing
  unsigned i = 0;
  while (node->values[i] < value)
  {
    if (++i == Size)// need to place node at the end of array
      break;
  }

  BNode* newNode = NULL;
  unsigned middle = Size / 2;

  try{
    // if count is 1 it is just a linked list
    // and splitting a linked list is not the same as splitting an array
    // thus a special case needed to be checked
    if (node->count == 1) // special case for splitting a node with size 1
    {
      if (i == 0)// this happens when inserting to front of BList
      {
        // the new split node should contain dublicated data
        // because the original node needs to have its value changed
        newNode = NewNode(node->values[middle]);
        --node->count;
      }
      else
      {
        newNode = NewNode(value);
        --newNode->count;
      }
    }
    else
    {
      newNode = NewNode(node->values[middle]);
      // update how we just took out of element
      --node->count;
    }
  }
  catch (const BListException &e){
    throw(BListException(BListException::E_NO_MEMORY, e.what()));
  }
  // move right half of node's array 
  // into left half of newNode's array
  for (unsigned k = 1, j = middle + 1; j < Size; ++j, ++k)
  {
    newNode->values[k] = node->values[j];
    --node->count;
    ++newNode->count;
  }

  // check which node the new value should go in
  if (value < newNode->values[0]) // value should not be in newNode
  {
    if (i == middle)
    {
      node->values[i] = value;
      ++node->count;
    }
    else
      PushIntoArray(node, value, i);
  }
  else// value should be somewhere in the new node
  {
    if (i == Size)// just need to push to end of array
    {
      newNode->values[middle] = value;
      ++newNode->count;
    }
    else // pushing updates node count  'i' is position in full array so take away half of it 
      PushIntoArray(newNode, value, i - middle);
  }

  // put the new node into the list
  newNode->next = node->next;
  newNode->prev = node;
  if (node != tail_)
    (node->next)->prev = newNode;
  else
    tail_ = newNode;
  node->next = newNode;
}

/*********************************************************************/
/*!
\brief
Deletes all of the nodes on this BList
*/
/***********************************************************************/
template <typename T, unsigned Size>
void BList<T, Size>::clear(void)
{
  while (head_ != NULL)
  {
    BNode* temp = head_;
    head_ = head_->next;
    delete temp;
  }
  m_stats.ItemCount = 0;
  m_stats.NodeCount = 0;
}


/*********************************************************************/
/*!
\brief
Remove an element by index
\param index
which element is going to be removed
\exception BListException
throws exception is given a non-valid index
*/
/***********************************************************************/
template <typename T, unsigned Size>
void BList<T, Size>::remove(int index) throw(BListException)
{
  if (static_cast<int>(m_stats.ItemCount) <= index || index < 0)
    throw(BListException(BListException::E_BAD_INDEX, "Bad Index"));

  BNode* node = head_;
  while (node != NULL)
  {
    index -= node->count;
    if (index < 0)// number is in this node
    {
      index = index + node->count;
      break;
    }
    node = node->next;
  }

  // updates node count and checks if node is empty
  RemoveNodeElement(node, index);
}

/*********************************************************************/
/*!
\brief
Goes through BList and finds given value, once found removes element
\param value
what value will be looked for on the list
*/
/***********************************************************************/
template <typename T, unsigned Size>
void BList<T, Size>::remove_by_value(const T& value)
{
  int index = find(value);
  remove(index);
}

/*********************************************************************/
/*!
\brief
Remove element from node's array
\param node
which node has the value that is going to be removed
\param index
what array index will have to be removed
*/
/***********************************************************************/
template <typename T, unsigned Size>
void BList<T, Size>::RemoveNodeElement(BNode* node, int index)
{
  // if it's the last element don't need to worry about it
  if (static_cast<unsigned>(index) != node->count - 1)
  {
    // overwrite what is being deleting
    for (unsigned i = index; i < node->count - 1; ++i)
      node->values[i] = node->values[i + 1];
  }

  if (--node->count == 0)// remove node
    RemoveNode(node);

  --m_stats.ItemCount;
}

/*********************************************************************/
/*!
\brief
Node is empty and will be removed from list
\param node
node that will be deleted
*/
/***********************************************************************/
template <typename T, unsigned Size>
void BList<T, Size>::RemoveNode(BNode* node)
{
  BNode* prevTemp = node->prev;
  BNode* nextTemp = node->next;

  if (prevTemp)
    prevTemp->next = nextTemp;
  else
    head_ = nextTemp;
  if (nextTemp)
    nextTemp->prev = prevTemp;
  else
    tail_ = prevTemp;

  // update stats and delete data
  --m_stats.NodeCount;
  delete node;
}
/*********************************************************************/
/*!
\brief
Finds the index of a specified value
\param value
the value being found on the BList
\return
index of found value, if value not found returns -1
*/
/***********************************************************************/
template <typename T, unsigned Size>
int BList<T, Size>::find(const T& value) const
{
  BNode* node = head_;
  int index = 0;
  while (node != NULL)
  {
    // when in here index should be at the beginning of array
    for (unsigned i = 0; i < node->count; ++i)
    {
      if (value == node->values[i])
        return index;

      ++index;
    }
    node = node->next;
  }

  return -1;
}

/*********************************************************************/
/*!
\brief
overloaded [] operator to get values by index (for l-values)
\param index
index for array to get value
\return
found value
\exception BListException
throws if given index is not valid
*/
/***********************************************************************/
template <typename T, unsigned Size>
T& BList<T, Size>::operator[](int index) throw(BListException)
{
  if (static_cast<int>(m_stats.ItemCount) <= index || index < 0)
    throw(BListException(BListException::E_BAD_INDEX, "Bad Index"));

  BNode* node = head_;
  while (node != NULL)
  {
    index -= node->count;
    if (index < 0)// number is in this node
    {
      index = index + node->count;
      break;
    }
    node = node->next;
  }
  return node->values[index];
}
/*********************************************************************/
/*!
\brief
overloaded [] operator to get values by index (for r-values)
\param index
index for array to get value
\return
found value
\exception BListException
throws if given index is not valid
*/
/***********************************************************************/
template <typename T, unsigned Size>
const T& BList<T, Size>::operator[](int index) const throw(BListException)
{
  if (static_cast<int>(m_stats.ItemCount) <= index || index < 0)
    throw(BListException(BListException::E_BAD_INDEX, "Bad Index"));

  BNode* node = head_;
  while (node != NULL)
  {
    index -= node->count;
    if (index < 0)// number is in this node
    {
      index = index + node->count;
      break;
    }
    node = node->next;
  }
  return node->values[index];
}

/*********************************************************************/
/*!
\brief
returns the total number of items in the BList (not counting any nodes)
\return
number of items in BList
*/
/***********************************************************************/
template <typename T, unsigned Size>
unsigned BList<T, Size>::size(void) const
{
  return m_stats.ItemCount;
}

