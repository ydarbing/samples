#pragma once
#include "Node.h"

void PrintReverse(Node* list);
Node* ReverseInPlace(Node* list);