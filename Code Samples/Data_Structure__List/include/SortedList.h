#pragma once

#include <string>     // error strings
#include <stdexcept>  // std::exception
#include <functional> // std::less

#include "ObjectAllocator.h"

class SortedListException : public std::exception
{
private:
  int m_ErrCode;
  std::string m_Description;

public:
  SortedListException(int ErrCode, const std::string& Description) :
    m_ErrCode(ErrCode), m_Description(Description) {};

  virtual int code(void) const {
    return m_ErrCode;
  }

  virtual const char *what(void) const throw() {
    return m_Description.c_str();
  }

  virtual ~SortedListException() throw() {
  }

  enum SORTEDLIST_EXCEPTION { E_NO_MEMORY, E_DATA_ERROR };
};

template <typename T, typename Pred = std::less<T> >
class SortedList
{
  struct Node;
public:
  // Constructor
  SortedList(Pred sorter = std::less<T>(), ObjectAllocator *Allocator = 0, bool SharedAllocator = false);

  // Copy constructor
  SortedList(const SortedList &rhs) throw(SortedListException);

  // Destructor
  ~SortedList() throw();

  // Assignment operator
  SortedList& operator=(const SortedList &rhs) throw(SortedListException);
  SortedList& operator+=(const SortedList &rhs) throw(SortedListException);


  void push_back(const T& value) throw(SortedListException);
  void insert(const T& value) throw(SortedListException);
  unsigned size(void) const throw();
  void clear(void) throw();

  template <typename Sorter>
  void selection_sort(Sorter sorter = std::less<T>());

  template <typename Sorter>
  void merge_sort(Sorter sorter = std::less<T>());

  static unsigned nodesize(void);

  //**************************************************
  // const_iterator
  class const_iterator
  {
  public:
    const_iterator(Node *p);                          // conversion ctor
    const T& operator*(void) const;                   // dereference op
    const_iterator& operator++(void);                 // pre-increment
    const_iterator& operator--(void);                 // pre-decrement
    bool operator!=(const const_iterator &rhs) const; // inequality op
  private:
    Node* current_; // pointer to the current node
  };
  //**************************************************

  // iteration
  const_iterator begin(void) const;  // the first node
  const_iterator end(void) const;    // one past the end
  const_iterator rbegin(void) const; // the last node
  const_iterator rend(void) const;   // one before the first

private: 
  struct Node
  {
    Node *Next_;
    Node *Prev_;
    T Data_;
    Node(const T& data) : Data_(data) {}
  };

  Node *head_;
  Node *tail_;
  // Node *dummyHead;
  // Node *dummyTail;
  unsigned size_;
  ObjectAllocator *objAllocator_;
  Pred  typeOfSort_;

  bool SharedAllocator_;
  bool SharedAllocatorCreator_;

  // Other private fields and methods ...
  Node* New_Node(const T& value, Node* prev, Node* next = NULL) const throw(SortedListException);
  void SwapNodes(Node* a, Node* b);
};

#include "SortedList.hpp"


