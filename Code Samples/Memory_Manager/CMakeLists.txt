cmake_minimum_required(VERSION 3.17)

project("Memory_Manager")

set(header_files
   include/ObjectAllocator.h
   include/PRNG.h
)

set(source_files
  src/main.cpp
  src/ObjectAllocator.cpp
  src/PRNG.cpp
  )

add_executable("${PROJECT_NAME}" 
               ${source_files} 
               ${header_files}
               )

target_include_directories("${PROJECT_NAME}" PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")
