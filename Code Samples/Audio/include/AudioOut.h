// AudioOut.h
// -- interface for low-level audio output
// cs245 2/15

#ifndef CS245_AUDIOOUT_H
#define CS245_AUDIOOUT_H

#include <windows.h>


class AudioOut {
  public:
    explicit AudioOut(int rate=44100, int nchannels=1, float latency=0.1f);
    ~AudioOut(void);
    virtual void GenerateData(short *out, int nsamples) = 0;
  private:
    int rate, nchannels;
    float latency;
    bool running;
    HANDLE thread;
    static DWORD WINAPI AudioThread(LPVOID);
};


#endif

