// MidiIn
// -- MIDI input framework
// cs245 1/14

#pragma once

#include <string>
#include <windows.h>


class MidiIn 
{
public:
  static std::string GetDeviceInfo(void);
  MidiIn(int devno);
  virtual ~MidiIn(void);
  virtual void NoteOn(int channel, int note, int velocity) { }
  virtual void NoteOff(int channel, int note) { }
  virtual void PitchWheelChange(int channel, float value) { }
  virtual void VolumeChange(int channel, int level) { }
  virtual void ModulationWheelChange(int channel, int value) { }
  virtual void ControlChange(int channel, int number, int value) { }
  
private:
  static void CALLBACK MidiEvent(HMIDIIN,UINT,DWORD_PTR,DWORD_PTR,DWORD_PTR);
  HMIDIIN midi_in;
};


