// Brady Reuter
// 2/13/2015

enum EnvelopeState
{
  Delay,
  Attack,
  Decay,
  Sustain, 
  Release
};

class ADSR 
{
public:
  ADSR(float delay = 0, float at = 0, float dt = 0,
    float sl = 1, float rt = 0, float rate = 44100);
  void SustainOff(void);
  float operator()(void);


private:
  void GotoState(const unsigned char nextState);
  
  float m_rate;
  float m_sustainLevel;

  unsigned m_delayCount;
  unsigned m_attackCount;
  unsigned m_stateCount; // used for staying in state for specified time

  float m_decayK;
  float m_releaseK;
  float m_curY; // used for exponential calculations
  float m_prevY;// used for exponential calculations
  unsigned char m_state;
};
