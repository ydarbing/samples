
#pragma once

class Resample 
{
public:
  Resample(short *samples = 0, unsigned count = 0, float factor = 1,
           unsigned loop_bgn = 0, unsigned loop_end = 0);
  ~Resample(void);
  float operator()(void);
  void OffsetPitch(float cents);

private:
  short* m_samples;
  unsigned m_count;
  unsigned m_loopBegin;
  unsigned m_loopEnd;
  float   m_factor;

  float m_offset;
  double m_rt;
};


