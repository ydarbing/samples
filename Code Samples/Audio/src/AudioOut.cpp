// AudioOut.cpp
// -- partial implementation of the AudioOut class
// cs250 2/15
//


// you may include other header files
#include <dsound.h>
#include "AudioOut.h"
using namespace std;

void SetupDirectSound(LPDIRECTSOUND8* ds, LPDIRECTSOUNDBUFFER8* dsb8, int channels, int rate);


AudioOut::AudioOut(int R, int n, float L)
    : rate(R), nchannels(n), latency(L),
      running(true), thread(0) {
  thread = CreateThread(0,0,AudioThread,this,0,0);
}


AudioOut::~AudioOut(void) {
  running = false;
  WaitForSingleObject(thread,INFINITE);
}


DWORD WINAPI AudioOut::AudioThread(LPVOID data) {

  AudioOut& audio = *reinterpret_cast<AudioOut*>(data);

  // setup direct sound
  LPDIRECTSOUND8 ds = nullptr;
  LPDIRECTSOUNDBUFFER8 dsb8 = nullptr;
  SetupDirectSound(&ds, &dsb8, audio.nchannels, audio.rate);
  if (!ds || !dsb8)
    return 1; // DirectSound did not setup correctly

  // initialize data in buffer
  void* vaddress;
  DWORD bufferSize;
  dsb8->Lock(0, 0, &vaddress, &bufferSize, 0, 0, DSBLOCK_ENTIREBUFFER);
  short *address = reinterpret_cast<short*>(vaddress);
  unsigned sampleCount = bufferSize / 2;
  audio.GenerateData(address, sampleCount);
  dsb8->Unlock(vaddress, bufferSize, 0, 0);


  dsb8->Play(0, 0, DSBPLAY_LOOPING);
  DWORD iLast = 0;
  DWORD maxAhead = DWORD(audio.rate * audio.latency);

  while (audio.running)
  {
    DWORD iPlay;
    dsb8->GetCurrentPosition(&iPlay, 0);
    DWORD iCanWriteTo = (iPlay + maxAhead) % bufferSize;

    DWORD bytesToWrite;

    if (iLast <= iCanWriteTo)
      bytesToWrite = iCanWriteTo - iLast;
    else
      bytesToWrite = bufferSize - iLast + iCanWriteTo;

    DWORD bytes1, bytes2;
    void *vaddress1, *vaddress2;

    // lock the portion of the buffer to write to:
    dsb8->Lock(iLast, bytesToWrite, &vaddress1, &bytes1, &vaddress2, &bytes2, 0);
    // fill in the 1st block
    address = reinterpret_cast<short*>(vaddress1);
    unsigned sampleCount = bytes1 / 2;
    audio.GenerateData(address, sampleCount);
    // fill in the 2nd block:
    address = reinterpret_cast<short*>(vaddress2);
    sampleCount = bytes2 / 2;
    audio.GenerateData(address, sampleCount);
    // unlock the buffer:
    dsb8->Unlock(vaddress1, bytes1, vaddress2, bytes2);

    iLast = (iLast + bytesToWrite) % bufferSize;
  }

  dsb8->Stop();
  dsb8->Release();
  ds->Release();

  return 0;
}


void SetupDirectSound(LPDIRECTSOUND8* ds, LPDIRECTSOUNDBUFFER8* dsb8, int channels, int rate)
{
  // get the window handle (to attach the sounds to):
  char *title = "Synthesizer";
  SetConsoleTitle(title);
  Sleep(40);
  HWND window = FindWindow(0, title);

  // specify the audio sample format
  WAVEFORMATEX waveformat;
  waveformat.wFormatTag = WAVE_FORMAT_PCM;
  waveformat.nChannels = channels;
  waveformat.nSamplesPerSec = rate;
  waveformat.wBitsPerSample = 16;
  waveformat.nBlockAlign = waveformat.nChannels * waveformat.wBitsPerSample / 8;
  waveformat.nAvgBytesPerSec = waveformat.nSamplesPerSec * waveformat.nBlockAlign;
  waveformat.cbSize = 0;

  // create a buffer
  DirectSoundCreate8(0, ds, 0); // get instance of directSound
  (*ds)->SetCooperativeLevel(window, DSSCL_NORMAL);
  DSBUFFERDESC desc;
  ZeroMemory(&desc, sizeof(desc)); // allocate secondary buffer to play sound
  desc.dwSize = sizeof(desc);
  desc.dwFlags = DSBCAPS_CTRLVOLUME | DSBCAPS_GLOBALFOCUS; // play when not in focus
  desc.lpwfxFormat = &waveformat;
  desc.dwBufferBytes = waveformat.nAvgBytesPerSec;// requested size of the buffer
  LPDIRECTSOUNDBUFFER dsb;
  (*ds)->CreateSoundBuffer(&desc, &dsb, 0);
  dsb->QueryInterface(IID_IDirectSoundBuffer8, (LPVOID*)dsb8);// create a directSound8 buffer
  dsb->Release();// release original buffer
}
