// Brady Reuter
// 2/13/2015

#include "ADSR.h"

#include <cmath> // log


ADSR::ADSR(float delay /*= 0*/, float at /*= 0*/, float dt /*= 0*/,
  float sl /*= 1*/, float rt /*= 0*/, float rate /*= 44100*/)
  : m_sustainLevel(sl), m_stateCount(0), m_rate(rate), m_state(Delay), m_curY(0), m_prevY(0)
{
  m_delayCount = unsigned(rate * delay);
  m_attackCount = unsigned(rate * at);
  m_decayK = 16.0f * float(std::log(2)) / dt;
  m_releaseK = 16.0f * float(std::log(2)) / rt;
}

void ADSR::SustainOff(void)
{
  GotoState(Release);
}

float ADSR::operator()(void)
{
  float retY = 0; // default to -96 dB

  switch (m_state)
  {
  case Delay: // just return 0
    if (++m_stateCount >= m_delayCount)
      GotoState(Attack);
    break;
  case Attack: // linear growth
    retY = m_stateCount / float(m_attackCount);

    if (++m_stateCount >= m_attackCount)
      GotoState(Decay);
    break;
  case Decay:
    retY = m_curY * m_prevY;
    m_prevY = retY;

    if (retY <= m_sustainLevel)
      GotoState(Sustain);
    break;
  case Sustain:
    retY = m_sustainLevel;
    break;
  case Release:
    retY = m_curY * m_prevY;
    m_prevY = retY;
    break;
  }

  return retY;
}

void ADSR::GotoState(const unsigned char nextState)
{
  m_state = nextState;
  m_stateCount = 0;

  switch (nextState)
  {
  case Decay:
    m_prevY = 1.0f;
    m_curY = m_rate / (m_rate + m_decayK);
    break;
  case Release:
    m_prevY = m_sustainLevel;
    m_curY = m_rate / (m_rate + m_releaseK);
    break;
  }
}
