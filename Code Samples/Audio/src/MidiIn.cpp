// MidiIn
// -- MIDI input framework
// cs245
#include "MidiIn.h"

#include <cassert>
#include <iostream>

//                                      (2^14) - 1
const float NEGONE_TO_ONE = (1.0f - (-1.0f)) / (16383);

enum EMessages
{
  NOTE_ON        = 0x90,
  NOTE_OFF       = 0x80,
  CONTROL_CHANGE = 0xB0,
  PITCH_SHIFT    = 0xE0
};

union ShortMessage 
{
  DWORD dword;
  unsigned char byte[4];
};

MidiIn::MidiIn(int devno)
{
  HMIDIIN midi_in{};
  MMRESULT result = midiInOpen(&midi_in, devno, (DWORD_PTR)MidiEvent,
    (DWORD_PTR)this, CALLBACK_FUNCTION);

  if (result != MMSYSERR_NOERROR) 
  {
    if (midi_in)
      midiInClose(midi_in);
    assert(midi_in && "failed to open input device");
  }
  midiInStart(midi_in);
}

MidiIn::~MidiIn(void)
{
  // stop sounds from being playing 
  midiInStop(midi_in);
  // close device
  midiInClose(midi_in);
}

std::string MidiIn::GetDeviceInfo(void)
{
  std::string ret;
  int size = midiInGetNumDevs();
  if (size == 0)
  {
    ret = "no devices present\n";
  }
  else 
  {
    MIDIINCAPS info;
    for (int i = 0; i < size; ++i)
    {
      midiInGetDevCaps(i, &info, sizeof(info));
      ret += std::to_string(i) + ": " + info.szPname + "\n";
    }
  }
    
  return ret;
}

void CALLBACK MidiIn::MidiEvent(HMIDIIN inDev, UINT msg, DWORD_PTR dwInstance, DWORD_PTR msgParam1, DWORD_PTR /*not used*/)
{
  if (msg == MIM_DATA)
  {
    MidiIn* thisGuy = reinterpret_cast<MidiIn*>(dwInstance);

    ShortMessage message;
    message.dword = static_cast<DWORD>(msgParam1);
    int channel = message.byte[0] & 0x0F;
    message.byte[0] -= channel; // get just what the message is in the first byte
  
    switch (message.byte[0])
    {
    case NOTE_ON:
      {
        if (message.byte[2] > 0)// if the velocity is 0 turn off
          thisGuy->NoteOn(channel, message.byte[1], message.byte[2]);
        else
          thisGuy->NoteOff(channel, message.byte[1]);
        break;
      }
    case NOTE_OFF:
      {
        thisGuy->NoteOff(channel, message.byte[1]);
        break;
      }
    case CONTROL_CHANGE:
      {
        if (message.byte[1] == 0x07) // specifically a volume change
          thisGuy->VolumeChange(channel, message.byte[2]);
        else if (message.byte[1] == 0x01) // specifically a modulation wheel change
          thisGuy->ModulationWheelChange(channel, message.byte[2]);
      
        thisGuy->ControlChange(channel, message.byte[1], message.byte[2]);
        break;
      }
    case PITCH_SHIFT:
      {
        // need to get a range from -1.0 to 1.0
        //                           (2^7)
        int int14 = message.byte[2] * 128 + message.byte[1];
        //                    (2^13) 
        float value = (int14 - 8192) * NEGONE_TO_ONE;

        thisGuy->PitchWheelChange(channel, value);
        break;
      }
    }// end switch
  }
}
