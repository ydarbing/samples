

#include "Resample.h"

#include <cassert>
#include <cmath>

Resample::Resample(short *samples /*= 0*/, unsigned count /*= 0*/, float factor /*= 1*/, unsigned loop_bgn /*= 0*/, unsigned loop_end /*= 0*/)
  : m_samples(samples), m_count(count), m_factor(factor), m_loopBegin(loop_bgn), m_loopEnd(loop_end),
  m_rt(-m_factor), m_offset(1.0f)
{
}

Resample::~Resample(void)
{
}

float Resample::operator()(void)
{
  m_rt += m_offset * m_factor;

  if (m_rt > m_loopEnd && m_loopEnd - m_loopBegin != 0)
    m_rt -= m_loopBegin;

  unsigned k = static_cast<unsigned>(std::floor(m_rt));

   if (k + 1 > m_count)
     return 0.0f; // this function has been called too many times
   else
     return static_cast<float>(m_samples[k] + (m_samples[k + 1] - m_samples[k]) * (m_rt - k));
}

void Resample::OffsetPitch(float cents)
{
  m_offset = std::powf(2.f, cents/1200.f);// std::exp2(cents / 1200.f);
}
