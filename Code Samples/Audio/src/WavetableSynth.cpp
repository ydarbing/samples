// Brady Reuter
// 2/25/2015

#include "Resample.h"
#include "MidiIn.h"
#include "AudioOut.h"
#include "ADSR.h"

#include <conio.h> // _kbhit
#include <exception>
#include <iostream>
#include <fstream>
#include <unordered_map>

static const float TWO_PI = 6.2831853f;

struct WaveInfo{
  char           riff_chunk[4];
  unsigned       chunk_size;
  char           wave_fmt[4];
  char           fmt_chunk[4];
  unsigned       fmt_chunk_size;
  unsigned short audio_format;
  unsigned short num_channels;
  unsigned       sampling_rate;
  unsigned       bytes_per_second;
  unsigned short block_align;
  unsigned short bits_per_sample;
  char           data_chunk[4];
  unsigned       data_chunk_size;
};
void GetWaveHeaderInfo(std::fstream& wav, WaveInfo& info);

class WavetableSynth : public MidiIn, AudioOut
{
public:
  WavetableSynth(int n, char *wavSamples[]= nullptr, int numSamples=0)
    : MidiIn(n), AudioOut(), m_globalVolume(1.0f), 
    m_oscillationDepth(0), m_oscillationRate(5.0f), m_currentIndex(0)
  {
    SetupPrerecordedAudioSamples();
  }
  ~WavetableSynth()
  {
    delete[] m_samples;
  }

protected:
  virtual void GenerateData(short *out, int nsamples)
  {
    if (m_notesOn.empty())
    {
      for (int i = 0; i < nsamples; ++i)
        out[i] = 0;
      return;
    }
  
    std::vector<int> doneNotes;
    for (int i = 0; i < nsamples; ++i)
    {
      out[i] = 0;
      NotesPlaying::iterator it = m_notesOn.begin();
     // combine sounds of all notes currently being played
      for (; it != m_notesOn.end(); ++it)
      {
        float adsrVal = it->second.adsr();
        if (it->second.sustainOff && adsrVal < 0.001f) // we can not hear this note anymore, done playing it
        {
          doneNotes.push_back(it->first); // don't want to erase notes while iterating through them
        }
        else
        {
          it->second.resample.OffsetPitch(m_oscillationDepth * std::sin(TWO_PI * m_oscillationRate * float(m_currentIndex) / m_rate));
          out[i] += short(m_globalVolume * it->second.velocity * adsrVal * it->second.resample());
        }
      }

      m_currentIndex++;
      // now safely remove the done notes
      for (unsigned i = 0; i < doneNotes.size(); ++i)
        m_notesOn.erase(doneNotes[i]);
    }
  }

  virtual void NoteOn(int channel, int note, int velocity)
  {
    // start attack phase
    float factor =  440.0f * std::pow(2.0f, (note - 69) / 120.0f) / 20.0f; //1.0f; 
    m_notesOn[note] = NoteModifier( 0.01f, 0.2f, 1.0f, 0.75f, 1.0f, // adsr info
                        Resample(m_samples, m_sampleSize, factor, m_loopBegin, m_loopEnd), 
                        velocity / 127.0f, // 0-1 velocity scale
                        false);// sustain has not been hit yet 

    int centOffsetFromA = (note - 69) * 100;
    m_notesOn[note].resample.OffsetPitch((float)centOffsetFromA);
  }

  virtual void NoteOff(int channel, int note)
  {
    // enter release phase
    m_notesOn[note].adsr.SustainOff();
    m_notesOn[note].sustainOff = true;
  }

  virtual void PitchWheelChange(int channel, float value)
  {
    std::cout << "PitchWheelChange: " << value << std::endl;
    m_pitchWheel = 200 * value;
    for (NotesPlaying::iterator it = m_notesOn.begin(); it != m_notesOn.end(); ++it)
    {
      it->second.resample.OffsetPitch(m_pitchWheel);
    }
  }

  virtual void VolumeChange(int channel, int level)
  {
    m_globalVolume = level / 127.0f;
    std::cout << "Global Volume Now: " << m_globalVolume << std::endl;
  }

  virtual void ModulationWheelChange(int channel, int value)
  {
    std::cout << "ModulationWheelChange: " << value << std::endl;
    m_oscillationDepth = (value * 200.0f / 127.0f);
  }

  virtual void ControlChange(int channel, int number, int value)
  {
    std::cout << "ControlChange: " << value << std::endl;
  }

  void SetupPrerecordedAudioSamples()
  {
    // read in wav files
    std::fstream wav("sample.wav", std::ios_base::in | std::ios_base::binary);

    if (wav.good() == false)
    {
      std::cout << "Could not find sample.wav" << std::endl;
      return;
    }
    WaveInfo info;
    GetWaveHeaderInfo(wav, info);
    m_sampleSize = info.data_chunk_size / (info.bits_per_sample / 8);

    m_rate = info.sampling_rate;
    m_samples = new short[m_sampleSize];
    wav.read(reinterpret_cast<char*>(m_samples), info.data_chunk_size);
    m_loopBegin = 31080;  // la = 34141;
    m_loopEnd   = 56345;  // la = 42979;
  }

private:
  // per channel
  short* m_samples;
  unsigned m_rate;
  unsigned m_sampleSize;
  unsigned m_loopBegin;
  unsigned m_loopEnd;
  unsigned m_currentIndex;
  float m_pitchWheel;
  float m_oscillationRate;
  float m_oscillationDepth;
  //
  float m_globalVolume;

  struct NoteModifier
  {
    NoteModifier() {};
    NoteModifier(float delay, float at, float dt, float sl, float rt, Resample _resample, float vel, bool _sustainOff)
      : adsr(ADSR(delay, at, dt, sl, rt)), resample(_resample), velocity(vel), sustainOff(_sustainOff) {};
    ADSR adsr;
    Resample resample;
    float velocity;
    bool sustainOff;
  };

  typedef std::unordered_map<int, NoteModifier> NotesPlaying;
  NotesPlaying m_notesOn;
};

int main(int argc, char *argv[])
{
  if (argc != 2)
  {
    std::cout << "WaveTableSynth" << std::endl;
    std::cout << MidiIn::GetDeviceInfo() << std::endl;
    _getch();
    return 0;
  }

  int inputPort = std::atoi(argv[1]);

  WavetableSynth synth(inputPort);
  bool done = false;
  while (!done)
  {
    if (_kbhit())
    {
      char c = char(_getch());
      done = (c == 'q');
    }
  }

  return 0;
}



void GetWaveHeaderInfo(std::fstream& wav, WaveInfo& info)
{
  char header[44];
  wav.read(header, 44);

  std::memmove(info.riff_chunk, header, 4);
  info.chunk_size = *reinterpret_cast<unsigned*>(header + 4);
  std::memmove(info.wave_fmt, header + 8, 4);
  std::memmove(info.fmt_chunk, header + 12, 4);
  info.fmt_chunk_size = *reinterpret_cast<unsigned*>(header + 16);
  info.audio_format = *reinterpret_cast<unsigned short*>(header + 20);
  info.num_channels = *reinterpret_cast<unsigned short*>(header + 22);
  info.sampling_rate = *reinterpret_cast<unsigned*>(header + 24);
  info.bytes_per_second = *reinterpret_cast<unsigned*>(header + 28);
  info.block_align = *reinterpret_cast<unsigned short*>(header + 32);
  info.bits_per_sample = *reinterpret_cast<unsigned short*>(header + 34);
  std::memmove(info.data_chunk, header + 36, 4);
  info.data_chunk_size = *reinterpret_cast<unsigned*>(header + 40);
}
