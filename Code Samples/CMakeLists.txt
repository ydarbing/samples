
cmake_minimum_required(VERSION 3.17)
project (CODE_SAMPLES)

set(CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/_modules/" ${CMAKE_MODULE_PATH})

#if(DEFINED ENV{VCPKG_ROOT} AND NOT DEFINED CMAKE_TOOLCHAIN_FILE)
#  set(CMAKE_TOOLCHAIN_FILE "$ENV{VCPKG_ROOT}/scripts/buildsystems/vcpkg.cmake"
#    CACHE STRING "")
#    message(" [INFO] VCPKG CMAKE_TOOLCHAIN_FILE = ${CMAKE_TOOLCHAIN_FILE}")
#endif()
set(CMAKE_TOOLCHAIN_FILE "C:/vcpkgscripts/buildsystems/vcpkg.cmake")

# Use folders for nice tree in Visual Studio and XCode
set_property(GLOBAL PROPERTY USE_FOLDERS ON)


add_subdirectory("Add_Ascii_Integers")
add_subdirectory("Audio")
add_subdirectory("Binary_Search")
add_subdirectory("Bit_Pack")
add_subdirectory("Calendar")
add_subdirectory("Casino")
add_subdirectory("Circle_Game_Josephus_Problem")
add_subdirectory("Closest_Pair")
add_subdirectory("Data_Structure__Graph")
add_subdirectory("Data_Structure__Hash")
add_subdirectory("Data_Structure__List")
add_subdirectory("Data_Structure__Tree")
add_subdirectory("Endianness")
add_subdirectory("FileIO")
add_subdirectory("Grid")
add_subdirectory("KMeans")
add_subdirectory("Knapsack")
add_subdirectory("Kruskal_Disjoint_Subsets")
add_subdirectory("Maths")

# uses external Magnum graphics 
#add_subdirectory("Maths_Demo")

add_subdirectory("Memory_Manager")
# need to change generator since ninja generator doesn't work nicely with CMAKE_MODULE_PATH# projects
#add_subdirectory("MySQL_Database")

add_subdirectory("Networking")
add_subdirectory("Networking_Test")

add_subdirectory("Number_To_Verbal_Phrase")
# In the middle of changing Maths libs and then
# plan on making physics its own lib CMAKE_TOOLCHAIN_FILE, curently broken exe
#add_subdirectory("Physics")

add_subdirectory("Profiler")
add_subdirectory("Random_Walk")
add_subdirectory("Real_Number_To_Phrase")
add_subdirectory("Reverse_Polish_Notation")
add_subdirectory("Small_Functions")
add_subdirectory("Sorting")
add_subdirectory("Strings")
add_subdirectory("Sudoku")
add_subdirectory("Test")
add_subdirectory("Threads")
add_subdirectory("Threads_Ball")
add_subdirectory("Verbal_Phrase_To_Number")