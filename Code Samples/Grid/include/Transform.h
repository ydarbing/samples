#pragma once

struct Coordinate{
  Coordinate(int _x, int _y) : x(_x), y(_y) {};
  int x;
  int y;
};

void TestTransform();

void Transform(int** grid, int width, int height);
void TransformBetter(int** grid, int width, int height);
bool HasZero(int** grid, int width, int height, int x, int y);

void PrintGrid(int** grid, int width, int height);

int RandomGridValue();