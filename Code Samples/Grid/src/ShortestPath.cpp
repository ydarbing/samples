#include <algorithm>
#include <string>
#include <queue>
#include <vector>
#include <iostream>
#include <fstream>
#include <set>

void print2D(std::vector<std::vector<int>> arr)
{
  for (unsigned i = 0; i < arr.size(); ++i)
  {
    for (unsigned j = 0; j < arr[0].size(); ++j)
    {
      std::cout << (arr[i][j] == (1 << 30) ? -9 : arr[i][j]) << "   ";
    }
    std::cout << std::endl;
  }
}

void solve(std::vector<std::vector<int>>& maze, int si, int sj, int ei, int ej)
{
  std::queue<std::pair<int, int>> queue;
  std::set<std::pair<int, int>> set;
  int M = (int)maze.size();
  int N = (int)maze[0].size();
  set.insert(std::make_pair(si, sj));
  queue.push(std::make_pair(si, sj));

  while (!queue.empty())
  {
    std::pair<int, int> p = queue.front();
    queue.pop();
    // go through all neighbors
    for (int x = -1; x <= 1; ++x)
    {
      for (int y = -1; y <= 1; ++y)
      {
        if (x == 0 && y == 0)
          continue;
        int i = p.first + x;
        int j = p.second + y;

        // if neighbor is out of bounds or a wall skip checking
        if (i < 0 || i >= M || j < 0 || j >= N || maze[i][j] == -1)
          continue;

        maze[i][j] = std::min(maze[i][j], maze[p.first][p.second] + 1);

        if (set.find(std::make_pair(i, j)) == set.end())
        {
          set.insert(std::make_pair(i, j));
          queue.push(std::make_pair(i, j));
        }
      }
    }
  }
}


void TestShortestPath(char* gridFile)
{
  std::ifstream infile(gridFile);
  std::string s;
  int v = 0, si = 0, sj = 0, ei = 0, ej = 0;
  std::vector<std::vector<int>> maze;
  int i = 0;
  while (infile >> s)
  {
    std::vector<int> row;
    for (unsigned j = 0; j < s.size(); ++j)
    {
      if (s[j] == 's')
      {
        si = i;
        sj = j;
        row.push_back(0);
      }
      else if (s[j] == 'e')
      {
        ei = i;
        ej = j;
        row.push_back((1 << 30));
      }
      else
      {
        row.push_back((s[j] == 'x' ? -1 : (1 << 30)));
      }
    }
    maze.push_back(row);
    ++i;
  }

  solve(maze, si, sj, ei, ej);
  print2D(maze);
  std::cout << "Shortest distance to end: " << maze[ei][ej] << std::endl;
}