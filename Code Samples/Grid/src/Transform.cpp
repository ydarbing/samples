#include "Transform.h"

#include "Profiler.h"

#include <cstdlib>// rand()
#include <vector>
#include <iostream>

void TransformBetter(int** grid, int width, int height)
{
  PROFILE
  std::vector<int> wholeRowOne(width, 1);
  std::vector<int> wholeColOne(height, 1);

  // check which cols have all 1's
  for (int y = 0; y < height; ++y)
  {
    if (y < width && wholeRowOne[y] == 0)
      continue;

    // check which rows have all 1's
    for (int x = 0; x < width; ++x)
    {
      if (x < height && wholeColOne[x] == 0)
        continue;
     
      if (grid[x][y] == 0)
      {
        wholeRowOne[y] = 0;
        wholeColOne[x] = 0;
        break;
      }
    }
  }

  for (int y = 0; y < height; ++y)
  {
    for (int x = 0; x < width; ++x)
    {
      if ((x < height && wholeColOne[x] == 0) ||
          (y < width && wholeRowOne[y] == 0))
      {
        grid[x][y] = 0;
      }
    }
  }
}

void Transform(int** grid, int width, int height)
{
  PROFILE
  std::vector<Coordinate> toChange;

  for (int y = 0; y < height; ++y)
  {
    for (int x = 0; x < width; ++x)
    {
      if (HasZero(grid, width, height, x, y))
      {
        toChange.push_back(Coordinate(x, y));
      }
    }
  }

  for (unsigned i = 0; i < toChange.size(); ++i)
  {
    grid[toChange[i].x][toChange[i].y] = 0;
  }
}


bool HasZero(int** grid, int width, int height, int x, int y)
{
  for (int i = 0; i < width; ++i)
  {
    if (grid[i][y] == 0)
      return true;
  }

  for (int j = 0; j < height; ++j)
  {
    if (grid[x][j] == 0)
      return true;
  }

  return false;
}

int** MakeGrid(int rows, int cols)
{

  // set up 2d array so it will work with [x,y]
  // row major
  int** grid = new int*[cols];
  for (int i = 0; i < cols; i++)
    grid[i] = new int[rows];

  // initialize grid
  for (int y = 0; y < rows; y++)
  {
    for (int x = 0; x < cols; x++)
    {
      grid[x][y] = RandomGridValue();
    }
  }

  return grid;
}
void TestTransform()
{
  // make a random grid
  const int cols = 80;
  const int rows = 40;
  int** grid1 = MakeGrid(rows, cols);
  int** grid2 = MakeGrid(rows, cols);


  //PrintGrid(grid1, cols, rows);

  Transform(grid1, cols, rows);

  TransformBetter(grid2, cols, rows);
  
  PROFILE_PRINT

  //PrintGrid(grid1, cols, rows);
  //PrintGrid(grid2, cols, rows);

  // clean up the grid
  for (int i = 0; i < cols; ++i)
  {
    delete[] grid1[i];
    delete[] grid2[i];
  }

  delete[] grid1;
  delete[] grid2;
}

int RandomGridValue()
{
  return rand() % 2;
}

void PrintGrid(int** grid, int width, int height)
{
  std::cout << std::endl;
  for (int y = 0; y < height; ++y)
  {
    for (int x = 0; x < width; ++x)
    {
      std::cout << grid[x][y] << " ";
    }
    std::cout << std::endl;
  }
}
