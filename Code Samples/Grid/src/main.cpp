#include "ShortestPath.h"
#include "Transform.h"


#include <cstdlib>
#include <ctime> // to seed rand
#include <iostream>


int main(int argc, char** argv)
{
  if (argc != 2)
    std::cout << "To test Shortest Path: Need a file with a grid on it.  ex \"grid.txt\"";
  else if (argc == 2)
  {
    ++argv;
    TestShortestPath(*argv);
  }
  
  //srand(time(nullptr));
  TestTransform();

  return 0;
}
