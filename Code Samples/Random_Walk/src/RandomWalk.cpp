#include <iostream>
#include <ctime>

#define GRID_SIZE 10


bool GetNextValidMove(const unsigned char board[][GRID_SIZE], unsigned& gridX, unsigned& gridY)
{
  bool tested[4] = { false };
  while (1)
  {
    unsigned nextMove = rand() % 4;
    if (tested[nextMove])
    {
      // check all directions if all have been tried return false
      int i = 0;
      for (; i < 4; ++i)
        if (tested[i] == false)
          break;

      if (i == 4) // all directions have been tested
        return false;
      else// there is another direction that can be tested
        continue;
    }
    unsigned testX = gridX;
    unsigned testY = gridY;

    switch (nextMove)
    {
    case 0: --testY; break; // up
    case 1: ++testX; break; // right
    case 2: ++testY; break; // down
    case 3: --testX; break; // left
    }
    tested[nextMove] = true;

    // since using unsigned it will wrap around 
    //  past grid size of it goes too low
    if (testX < GRID_SIZE && testY < GRID_SIZE
      && board[testX][testY] == '.')
    {
      // update index with new valid move
      gridX = testX;
      gridY = testY;
      return true;
    }
  }
}

void PrintBoard(unsigned char board[][GRID_SIZE])
{
  for (int r = 0; r < GRID_SIZE; ++r)
  {
    for (int c = 0; c < GRID_SIZE; ++c)
    {
      std::cout << board[r][c];
    }
    std::cout << std::endl;
  }
}

void RandomWalk()
{
  unsigned char board[GRID_SIZE][GRID_SIZE];

  for (int r = 0; r < GRID_SIZE; ++r)
    for (int c = 0; c < GRID_SIZE; ++c)
      board[r][c] = '.';

  srand((unsigned)time(nullptr));

  unsigned gridX = rand() % GRID_SIZE;
  unsigned gridY = rand() % GRID_SIZE;

  char letter = 'A';
  board[gridX][gridY] = letter++;

  while (letter <= 'Z')
  {
    if (!GetNextValidMove(board, gridX, gridY))
      break;

    board[gridX][gridY] = letter++;
  }

  PrintBoard(board); 
}


int main(int argc, char ** argv)
{
  RandomWalk();

  return 0;
}