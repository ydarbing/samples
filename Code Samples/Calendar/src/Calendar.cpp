#include "Calendar.h"
#include <iostream>
#include <iomanip>// std::setw
#include <sstream> // get int from command line
#include <cstring> // strlen
#include <stdio.h>

bool IsLeapYear(int year)
{
  if (year % 4 == 0 && year % 100 == 0 && year % 400 == 0)
    return true;
  else
    return false;
}

// taken from https://en.wikipedia.org/wiki/Determination_of_the_day_of_the_week
int DayOfWeek(int day, int month, int year)
{
  static int t[] = { 0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4 };
  year -= month < 3;
  return (year + (year / 4) - (year / 100) + (year / 400) + t[month - 1] + day) % 7;
}

void PrintMonthDetails(int month)
{
  int length = (int)strlen(Months[month]);
  int mid = (28 - length) / 2;
  for (int i = 0; i < mid; ++i)
    std::cout << " ";

  std::cout << Months[month] << std::endl;

  for (int i = 0; i < 7; ++i)
    std::cout << std::setw(CALENDAR_SPACE) << Days[i];

  std::cout << std::endl;
}

int main(int argc, char ** argv)
{
  int year = 0;

  if (argc == 2) // user gave year in command line
  {
    std::istringstream ss(argv[1]);
    if (!(ss >> year))
      std::cerr << "Invalid number " << argv[1] << std::endl;
  }
  else
  {
    std::cout << "Enter the calendar year you want to see: ";
    std::cin >> year;
  }


  if (IsLeapYear(year))
    DaysInMonth[1] = DaysInMonth[1] + 1;

  int dayOfWeek = DayOfWeek(1, 1, year);  // 0 for sunday, 1 for monday

  for (int month = 0; month < 12; ++month)
  {
    // begin outputting the calendar
    PrintMonthDetails(month);
    // print any leading "blank dates"
    for (int i = 0; i < dayOfWeek; ++i)
      std::cout << "    ";

    // print month
    int day = 1;
    for (; day <= DaysInMonth[month]; ++day)
    {
      if (day != 1 && (dayOfWeek + day - 1) % 7 == 0)
        std::cout << std::endl;
      std::cout << std::setw(CALENDAR_SPACE) << day;
    }
    // update the start day of the week
    dayOfWeek = (dayOfWeek + day - 1) % 7;
    std::cout << std::endl;
  }
  return 0;
}
