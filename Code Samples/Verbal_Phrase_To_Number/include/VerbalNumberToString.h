// Brady Reuter

#pragma once
#include <string>
#include <map>

std::string FindString(std::string inputString);

namespace VerbalToString
{
  static const std::map<const std::string, const char> onesNumberMap = {
    { "zero", '0' },
    { "one", '1' },
    { "two", '2' },
    { "three", '3' },
    { "four", '4' },
    { "five", '5' },
    { "six", '6' },
    { "seven", '7' },
    { "eight", '8' },
    { "nine", '9' }
  };

  static const std::map<const std::string, const std::string> tensNumberMap = {
    { "ten", "10" },
    { "eleven", "11" },
    { "twelve", "12" },
    { "thirteen", "13" },
    { "fourteen", "14" },
    { "fifteen", "15" },
    { "sixteen", "16" },
    { "seventeen", "17" },
    { "eighteen", "18" },
    { "nineteen", "19" },
    { "twenty", "20" },
    { "thirty", "30" },
    { "forty", "40" },
    { "fifty", "50" },
    { "sixty", "60" },
    { "seventy", "70" },
    { "eighty", "80" },
    { "ninety", "90" }
  };

  // numbers represent how many 0's there are  ex there are 6 zeros in one million
  static const std::map<const std::string, const unsigned> modifierNumberMap = {
    { "hundred", 2 },
    { "thousand", 3 },
    { "million", 6 },
    { "billion", 9 },
    { "trillion", 12 },
    { "quadrillion", 15 },
    { "quintillion", 18 },
    { "sextillion", 21 },
    { "septillion", 24 },
    { "octillion", 27 },
    { "nonillion", 30 },
    { "decillion", 33 },
    { "undecillion", 36 },
    { "duodecillion", 39 },
    { "tredecillion", 42 },
    { "quattuordecillion", 45 },
    { "quindecillion", 48 },
    { "sedecillion", 51 },
    { "septendecillion", 54 },
    { "octodecillion", 57 },
    { "novendecillion", 60 },
    { "vigintillion", 63 },
    { "unvigintillion", 66 },
    { "duovigintillion", 69 },
    { "tresvigintillion", 72 },
    { "quattuorvigintillion", 75 },
    { "quinquavigintillion", 78 },
    { "sexvigintillion", 81 },
    { "sesvigintillion", 81 },
    { "septemvigintillion", 84 },
    { "octovigintillion", 87 },
    { "novemvigintillion", 90 },
    { "trigintillion", 93 },
    { "untrigintillion", 96 },
    { "duotrigintillion", 99 },
    { "trestrigintillion", 102 },
    { "quattuortrigintillion", 105 },
    { "quinquatrigintillion", 108 },
    { "sestrigintillion", 111 },
    { "septentrigintillion", 114 },
    { "octotrigintillion", 117 },
    { "noventrigintillion", 120 },
    { "quadragintillion", 123 },

    { "quinquagintillion", 153 },

    { "sexagintillion", 183 },

    { "septuagintillion", 213 },

    { "octogintillion", 243 },

    { "nonagintillion", 273 },

    { "centillion", 303 },
    { "uncentillion", 306 },
    { "duocentillion", 309 },
    { "trescentillion", 312 },


    { "decicentillion", 333 },
    { "undecicentillion", 336 },

    { "viginticentillion", 363 },
    { "unviginticentillion", 366 },


    { "trigintacentillion", 393 },

    { "quadragintacentillion", 423 },

    { "quinquagintacentillion", 453 },

    { "sexagintacentillion", 483 },

    { "septuagintacentillion", 513 },

    { "octogintacentillion", 543 },

    { "nonagintacentillion", 573 },

    { "ducentillion", 603 },

    { "trecentillion", 903 },

    { "quadringentillion", 1203 },

    { "quingentillion", 1503 },

    { "sescentillion", 1803 },

    { "septingentillion", 2103 },

    { "octingentillion", 2403 },

    { "nongentillion", 2703 },


    { "millinillion", 3003 }

  };
}