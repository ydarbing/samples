// Brady Reuter

#pragma once
#include <string>
#include <map>

typedef unsigned long long U64;

U64 FindInteger(std::string inputString);

namespace VerbalToInt
{

  static const U64 QUINT = 1000000000000000000;

  static const std::map<const std::string, unsigned> onesNumberMap = {
    { "zero", 0 },
    { "one", 1 },
    { "two", 2 },
    { "three", 3 },
    { "four", 4 },
    { "five", 5 },
    { "six", 6 },
    { "seven", 7 },
    { "eight", 8 },
    { "nine", 9 }
  };

  static const std::map<const std::string, unsigned> tensNumberMap = {
    { "ten", 10 },
    { "eleven", 11 },
    { "twelve", 12 },
    { "thirteen", 13 },
    { "fourteen", 14 },
    { "fifteen", 15 },
    { "sixteen", 16 },
    { "seventeen", 17 },
    { "eighteen", 18 },
    { "nineteen", 19 },
    { "twenty", 20 },
    { "thirty", 30 },
    { "forty", 40 },
    { "fifty", 50 },
    { "sixty", 60 },
    { "seventy", 70 },
    { "eighty", 80 },
    { "ninety", 90 }
  };

  static const std::map<const std::string, U64> modifierNumberMap = {
    { "hundred", 100 },
    { "thousand", 1000 },
    { "million", 1000000 },
    { "billion", 1000000000 },
    { "trillion", 1000000000000 },
    { "quadrillion", 1000000000000000 },
    { "quintillion", 1000000000000000000 }
  };
  // max u64 number
  //18446744073709551615

} // namespace
