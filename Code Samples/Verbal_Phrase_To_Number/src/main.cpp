#include "VerbalNumberToInteger.h"
#include "VerbalNumberToString.h"

#include <iostream>

int main(int argc, char ** argv)
{
  std::string testStrings[] = {
    //"five hundred and three thousand",

    "negative five",
    "negative five hundred and three thousand",
    "negative fifty five",
    "negative fifty thousand",
    "negative five hundred",

    "one thousand and three",
    "eighteen quintillion",
    "thirty two hundred quintillion",
    "five hundred quintillion",
    "seventeen quintillion nine hundred ninety nine quadrillion nine hundred ninety nine trillion nine hundred ninety nine billion nine hundred ninety nine million nine hundred ninety nine thousand nine hundred ninety nine",
    "zero",
    "five",
    "ten",
    "five thousand",
    "five thousand six hundred thirty two",
    "one thousand one hundred ten",
    "five hundred three thousand",
    "five hundred thirty thousand",
    "five hundred thirty two thousand",
    "five hundred thousand",
    "thirty two thousand five hundred",
    "one thousand million",
    "one thousand thousand thousand",
    "ten million seven",
    "two millinillion six million seven thousand eighty three"
  };
  unsigned size = sizeof(testStrings) / sizeof(testStrings[0]);
  //int size2 = sizeof(testStrings) / sizeof(std::string);

  for (unsigned i = 0; i < size; ++i)
  {
    std::string finalStr = FindString(testStrings[i]);
    U64 finalInt = FindInteger(testStrings[i]);
    
    std::cout << testStrings[i] << std::endl
      << "TO STRING:  -> " << finalStr << std::endl
      << "TO INTEGER: -> ";
    
    if (finalInt == -1)
      std::cout << "Number Not Readable or Too Large for 64-bit int." << std::endl;
    else if (finalInt == -2)
      std::cout << "Unsigned Values Only!" << std::endl;
    else
      std::cout << finalInt << std::endl;

    std::cout << std::endl;

  }

  return 0;
}
