// Brady Reuter
/************************************************************************/
/*                                                                                   
       This program takes a string containing a verbally spoken number  
       string and turns it into a program usable integer.               

            Highest Number: 18 quintillion - 1 
                           (17,999,999,999,999,999,999)
            Lowest Number: 0 

      An empty input string will return 0

    Still missing a bunch of error checking: 
    - only works with correctly spelled words
    - no negatives
    - more test cases

/************************************************************************/
#include "VerbalNumberToInteger.h"
#include <sstream>
#include <vector>
#include <locale>

using namespace VerbalToInt;

typedef std::map<const std::string, unsigned>::const_iterator TensMapIter;
typedef std::map<const std::string, U64>::const_iterator ModifierMapIter;
typedef std::vector < std::string > Words;

static Words StringToWords(std::string inputString)
{
  Words words;
  std::stringstream ss(inputString);
  std::string word;
  while (std::getline(ss, word, ' '))
  {
    if (word != "and")
      words.push_back(word);
  }

  return words;
}

U64 FindInteger(std::string inputString)
{
  std::locale loc;
  for (unsigned i = 0; i < inputString.size(); ++i)
    inputString[i] = std::tolower(inputString[i], loc);

  Words words = StringToWords(inputString);

  if (words.empty())
    return 0;
  

  if (words[0] == "negative")
    return -2;

  TensMapIter numberMapIter;
  ModifierMapIter modifierMapIter;

  U64 finalNumber = 0;

  U64 loopSum = 0;
  unsigned i = 0;
  while (i < words.size())
  {
    loopSum = 0;

    // start with tens because nothing higher can have something after it
    numberMapIter = tensNumberMap.find(words[i]);
    if (numberMapIter != tensNumberMap.end())// found a valid corresponding number
    {
      ++i;
      // check to see if there is a ones number after.  i.e thirty two
      loopSum += numberMapIter->second;
    }
    if (i < words.size()) // if a tens was found, already did this step
    {
      numberMapIter = onesNumberMap.find(words[i]);
      if (numberMapIter != onesNumberMap.end())// found a valid corresponding number
      {
        ++i;
        loopSum += numberMapIter->second;
      }
    }

    if (i >= words.size())
      return finalNumber + loopSum;

    modifierMapIter = modifierNumberMap.find(words[i]);
    if (modifierMapIter == modifierNumberMap.end())
      return -1;
    
    while (modifierMapIter != modifierNumberMap.end())
    {
      U64 firstMod = modifierMapIter->second; // need to check if there is a same or higher modifier after this

      if (modifierMapIter->second == QUINT && loopSum >= 18) // hardcoded to my current max number
        return -1;
      
      //assert(modifierMapIter->second != QUINT && loopSum < 18); // number too large

      loopSum *= modifierMapIter->second; // 'five hundred thirty thousand'
      //                                                           ^  higher modifier that comes after a lower modifier 
      //                           at most, it will be three ahead, so check 1, 2 and 3 places ahead

      
      if ((i + 1) < words.size())
      {
        modifierMapIter = modifierNumberMap.find(words[i + 1]);
        if (modifierMapIter != modifierNumberMap.end() && modifierMapIter->second >= firstMod)
        {
          //assert(modifierMapIter->second == QUINT && loopSum >= 18); // number too large
          if (modifierMapIter->second == QUINT && loopSum >= 18)
            return -1;

          loopSum *= modifierMapIter->second;
          ++i;
        }
        else if ((i + 2) < words.size())
        {
          modifierMapIter = modifierNumberMap.find(words[i + 2]);
          if (modifierMapIter != modifierNumberMap.end() && modifierMapIter->second > firstMod)
          {
            // there is a number like 20 or 1
            TensMapIter num = tensNumberMap.find(words[i + 1]);
            if (num != tensNumberMap.end())
              loopSum += num->second;
            else
              loopSum += onesNumberMap.find(words[i + 1])->second;

            //assert(modifierMapIter->second == QUINT && loopSum >= 18); // number too large
            if (modifierMapIter->second == QUINT && loopSum >= 18)
              return -1;

            loopSum *= modifierMapIter->second;
            i += 2;
          }
          else if ((i + 3) < words.size())
          {
            modifierMapIter = modifierNumberMap.find(words[i + 3]);
            if (modifierMapIter != modifierNumberMap.end() && modifierMapIter->second > firstMod)
            {
              //assert(modifierMapIter->second == QUINT); // number too large
              if (modifierMapIter->second == QUINT && loopSum >= 18)
                return -1;

              // there is a number like 43 or 21 in between
              loopSum += tensNumberMap.find(words[i + 1])->second;
              loopSum += onesNumberMap.find(words[i + 2])->second;

              loopSum *= modifierMapIter->second;
              i += 3;
            }
          }
        }
      }


      ++i;
      if (i >= words.size())
        break;
      // need to check ahead to see if a higher modifier is coming
      modifierMapIter = modifierNumberMap.find(words[i]);
    }

    finalNumber += loopSum;
  }
  return finalNumber;
}



