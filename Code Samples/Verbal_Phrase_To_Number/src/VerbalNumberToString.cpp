// Brady Reuter
/************************************************************************/
/*
       This program takes a string containing a verbally spoken number
       string and turns it into string.

       USING SHORT SCALE REPRESENTATION

       ex.  one -> "1"
       two hundred -> "200"

       Still missing a bunch of error checking:
       - only works with correctly spelled words
       - more test cases

       /************************************************************************/
#include "VerbalNumberToString.h"
#include <sstream>
#include <vector>
#include <locale> // tolower

using namespace VerbalToString;

typedef std::map<const std::string, const char>::const_iterator DigitMapIter;
typedef std::map<const std::string, const std::string>::const_iterator TensMapIter;
typedef std::map<const std::string, const unsigned>::const_iterator ModifierMapIter;
typedef std::vector < std::string > Words;

// add all words except and (and is a useless addition to describing numbers)
static Words StringToWords(std::string inputString)
{

  std::locale loc;
  for (unsigned i = 0; i < inputString.size(); ++i)
    inputString[i] = std::tolower(inputString[i], loc);

  Words words;
  std::stringstream ss(inputString);
  std::string word;
  while (std::getline(ss, word, ' '))
  {
    if (word != "and")
      words.push_back(word);
  }

  return words;
}

// 
void AddZeros(std::string& str, unsigned numZeros)
{
  if (str.size() < numZeros)
  {
    str.reserve(numZeros);
    str.append(numZeros, '0');
  }
}


std::string FindString(std::string inputString)
{  
  Words words = StringToWords(inputString);

  if (words.empty())
    return "";

  DigitMapIter digitMapIter;
  TensMapIter tensMapIter;
  ModifierMapIter modifierMapIter;

  std::string ret;
  std::string tempHundreds;

  unsigned wordIndex = 0;
  unsigned replaceIndex = 0;
  unsigned isNeg = 0;
  bool hasTens = false;

  if (words[0] == "negative")
  {
    ret.push_back('-');
    isNeg = 1;
    ++wordIndex;
  }


  while (wordIndex < words.size())
  {
    replaceIndex = 0;
    tempHundreds = "";
    hasTens = false;
    // start with tens because nothing higher can have something after it
    tensMapIter = tensNumberMap.find(words[wordIndex]);
    if (tensMapIter != tensNumberMap.end())// found a valid corresponding number
    {
      // check to see if there is a ones number after.  i.e thirty two
      tempHundreds.append(tensMapIter->second);

      replaceIndex += 2;
      hasTens = true;
      ++wordIndex;
    }
    if (wordIndex < words.size()) // if a tens was found, already did this step
    {
      digitMapIter = onesNumberMap.find(words[wordIndex]);
      if (digitMapIter != onesNumberMap.end())// found a valid corresponding number
      {
        ++wordIndex;

        if (hasTens)
          tempHundreds[1] = digitMapIter->second; // guaranteed to have size == 2
        else
        {
          tempHundreds.push_back(digitMapIter->second);
          ++replaceIndex;
        }
      }
    }

    if (wordIndex >= words.size())
    {
      if (ret.size() - isNeg == 0)
        ret.append(tempHundreds);
      else
        ret.replace(ret.size() - replaceIndex, tempHundreds.size(), tempHundreds);
      return ret;
    }

    modifierMapIter = modifierNumberMap.find(words[wordIndex]);

    while (modifierMapIter != modifierNumberMap.end())
    {
      unsigned mod = modifierMapIter->second; // need to check if there is a same or higher modifier after this
      replaceIndex += modifierMapIter->second;

      if ((wordIndex + 1) < words.size())
      {
        modifierMapIter = modifierNumberMap.find(words[wordIndex + 1]);
        if (modifierMapIter != modifierNumberMap.end() && modifierMapIter->second >= mod)
        {
          replaceIndex += modifierMapIter->second;
          ++wordIndex;
        }
        else if ((wordIndex + 2) < words.size())
        {
          modifierMapIter = modifierNumberMap.find(words[wordIndex + 2]);
          if (modifierMapIter != modifierNumberMap.end() && modifierMapIter->second > mod)
          {
            // there is a number like 20 or 1
            TensMapIter num = tensNumberMap.find(words[wordIndex + 1]);
            if (num != tensNumberMap.end())
            {
              tempHundreds.append(num->second);
            }
            else
            {
              // for numbers like 503  need to add a zero before the 3
              if (mod == 2) // hundred
                tempHundreds.push_back('0');

              tempHundreds.push_back(onesNumberMap.find(words[wordIndex + 1])->second);
            }

            replaceIndex += modifierMapIter->second;
            wordIndex += 2;
          }
          else if ((wordIndex + 3) < words.size())
          {
            modifierMapIter = modifierNumberMap.find(words[wordIndex + 3]);
            if (modifierMapIter != modifierNumberMap.end() && modifierMapIter->second > mod)
            {
              // there is a number like 43 or 21 in between
              tempHundreds.append(tensNumberMap.find(words[wordIndex + 1])->second);
              tempHundreds[tempHundreds.size() - 1] = onesNumberMap.find(words[wordIndex + 2])->second;

              replaceIndex += modifierMapIter->second;
              wordIndex += 3;
            }
          }
        }
      }


      ++wordIndex;
      if (wordIndex >= words.size())
        break;
      // need to check ahead to see if a higher modifier is coming
      modifierMapIter = modifierNumberMap.find(words[wordIndex]);
    }
    AddZeros(ret, replaceIndex); // replace index the first time will be the total length of the string
    ret.replace(ret.size() - replaceIndex, tempHundreds.size(), tempHundreds);
  }
  return ret;
}
















/*
    First try:
       Did not work because numbers like one million six, the six would be placed like 1600000... which is bad
std::string FindString(std::string inputString)
{
  Words words = StringToWords(inputString);

  if (words.empty())
    return "";

  bool addedAllZeros = false; // if we call AddZeros, we only call it once per this function
  std::string ret;
  unsigned retIndex = 0;

  if (words[0] == "negative")
  {
    //ss << '-';
    ret.push_back('-');
    ++retIndex;
  }

  DigitMapIter digitMapIter;
  TensMapIter tensMapIter;
  ModifierMapIter modifierMapIter;

  std::string tempHundreds;
  bool hasTens = false;

  unsigned wordIndex = 0;
  while (wordIndex < words.size())
  {
    tempHundreds = "";
    hasTens = false;
    // start with tens because nothing higher can have something after it
    tensMapIter = tensNumberMap.find(words[wordIndex]);
    if (tensMapIter != tensNumberMap.end())// found a valid corresponding number
    {
      // check to see if there is a ones number after.  i.e thirty two
      if (retIndex >= ret.size())
        ret.insert(ret.size(), tensMapIter->second);
      else
        ret.replace(retIndex, 2, tensMapIter->second);

      hasTens = true;
      ++wordIndex;
      retIndex += 2;
    }
    if (wordIndex < words.size()) // if a tens was found, already did this step
    {
      digitMapIter = onesNumberMap.find(words[wordIndex]);
      if (digitMapIter != onesNumberMap.end())// found a valid corresponding number
      {
        ++wordIndex;
        if (hasTens)
        {
          ret[retIndex - 1] = digitMapIter->second;
        }
        else
        {
          if (retIndex >= ret.size())
            ret.insert(retIndex, 1, digitMapIter->second);
          else
            ret[retIndex] = digitMapIter->second;
          ++retIndex;
        }
      }
    }

    if (wordIndex >= words.size())
      return ret;

    modifierMapIter = modifierNumberMap.find(words[wordIndex]);

    unsigned modSum = 0;
    while (modifierMapIter != modifierNumberMap.end())
    {
      unsigned firstMod = modifierMapIter->second; // need to check if there is a same or higher modifier after this
      modSum += firstMod;


      if ((wordIndex + 1) < words.size())
      {
        modifierMapIter = modifierNumberMap.find(words[wordIndex + 1]);
        if (modifierMapIter != modifierNumberMap.end() && modifierMapIter->second >= firstMod)
        {
          modSum += modifierMapIter->second;
          ++wordIndex;
        }
        else if ((wordIndex + 2) < words.size())
        {
          modifierMapIter = modifierNumberMap.find(words[wordIndex + 2]);
          if (modifierMapIter != modifierNumberMap.end() && modifierMapIter->second > firstMod)
          {
            hasTens = false;
            // there is a number like 20 or 1
            TensMapIter num = tensNumberMap.find(words[wordIndex + 1]);
            if (num != tensNumberMap.end())
            {
              hasTens = true;
              ret.insert(retIndex, num->second);
              retIndex += 2;
            }
            else
            {
              if (hasTens)
              {
                ret[retIndex - 1] = onesNumberMap.find(words[wordIndex + 1])->second;
              }
              else
              {
                // for numbers like 503  need to add a zero before the 3
                if (modSum == 2) // hundred
                {
                  ret.insert(retIndex, 1, '0');
                  ++retIndex;
                }

                ret.insert(retIndex, 1, onesNumberMap.find(words[wordIndex + 1])->second);
                ++retIndex;
              }
            }

            modSum = modifierMapIter->second;
            AddZeros(ret, modSum, addedAllZeros);
            wordIndex += 2;
          }
          else if ((wordIndex + 3) < words.size())
          {
            modifierMapIter = modifierNumberMap.find(words[wordIndex + 3]);
            if (modifierMapIter != modifierNumberMap.end() && modifierMapIter->second > firstMod)
            {
              // there is a number like 43 or 21 in between
              ret.replace(retIndex, 2, tensNumberMap.find(words[wordIndex + 1])->second);
              retIndex += 2;
              ret[retIndex - 1] = onesNumberMap.find(words[wordIndex + 2])->second;

              modSum = modifierMapIter->second;
              AddZeros(ret, modSum, addedAllZeros);
              wordIndex += 3;
            }
          }
        }
      }


      ++wordIndex;
      if (wordIndex >= words.size())
        break;
      // need to check ahead to see if a higher modifier is coming
      modifierMapIter = modifierNumberMap.find(words[wordIndex]);
    }
    AddZeros(ret, modSum, addedAllZeros);
  }
  return ret;
}
*/

