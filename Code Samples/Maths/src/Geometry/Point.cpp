#include "Geometry/Point.h"

#include "Draw.h"
#include "Vector.h"
#include "Utils.h"


namespace Maths
{
  const Point Point::ORIGIN(0.0f, 0.0f, 0.0f, 1.0f);

  Point::Point()
    : x(0.0f), y(0.0f), z(0.0f), w(1.0f)
  {
  }

  Point::Point(float x_, float y_, float z_, float w_ /*= 1.0f*/)
    : x(x_), y(y_), z(z_), w(w_)
  {
  }

  Point Point::AddPoint(const Point& rhs) const
  {
    return Point(x + rhs.x,
                 y + rhs.y,
                 z + rhs.z,
                 w + rhs.w);
  }

  Point Point::GetMidpoint(const Point& rhs) const
  {
    return (AddPoint(rhs) * 0.5f);
  }


  Point Point::CrossPoint(const Point& rhs) const
  {
    return Point(y * rhs.z - rhs.y * z,
      rhs.x * z - x * rhs.z,
      x * rhs.y - rhs.x * y);
  }

  float Point::DotPoint(const Point& rhs) const
  {
    return (x * rhs.x + y * rhs.y + z * rhs.z + w * rhs.w);
  }

  Point Point::operator-(void) const
  {
    return Point(-x, -y, -z, -w);
  }

  Vector Point::operator-(const Point& rhs) const
  {
    return Vector(x - rhs.x,
      y - rhs.y,
      z - rhs.z,
      w - rhs.w);
  }

  Point Point::operator+(const Vector& rhs) const
  {
    return Point(x + rhs.x,
      y + rhs.y,
      z + rhs.z,
      w + rhs.w);
  }

  Point Point::operator-(const Vector& rhs) const
  {
    return Point(x - rhs.x,
      y - rhs.y,
      z - rhs.z,
      w - rhs.w);
  }

  Point Point::operator*(float scalar) const
  {
    return Point(x * scalar,
      y * scalar,
      z * scalar,
      w * scalar);
  }

  Point Point::operator/(float scalar) const
  {
    scalar = 1.0f / scalar;
    return Point(x * scalar,
      y * scalar,
      z * scalar,
      w * scalar);
  }

  Point& Point::operator+=(const Vector& rhs)
  {
    x += rhs.x;
    y += rhs.y;
    z += rhs.z;
    w += rhs.w;
    return (*this);
  }

  Point& Point::operator-=(const Vector& rhs)
  {
    x -= rhs.x;
    y -= rhs.y;
    z -= rhs.z;
    w -= rhs.w;
    return (*this);
  }

  Point& Point::operator*=(float scalar)
  {
    x *= scalar;
    y *= scalar;
    z *= scalar;
    w *= scalar;
    return (*this);
  }

  Point& Point::operator/=(float scalar)
  {
    scalar = 1.0f / scalar;
    x *= scalar;
    y *= scalar;
    z *= scalar;
    w *= scalar;
    return (*this);
  }

  bool Point::operator==(const Point& rhs) const
  {
    if (AbsVal(x - rhs.x) > EPSILON)
      return false;
    if (AbsVal(y - rhs.y) > EPSILON)
      return false;
    if (AbsVal(z - rhs.z) > EPSILON)
      return false;
    if (AbsVal(w - rhs.w) > EPSILON)
      return false;

    return true;
  }

  bool Point::operator!=(const Point& rhs) const
  {
    return !(*this == rhs);
  }

  bool Point::operator<(const Point& rhs) const
  {
    return memcmp(v, rhs.v, sizeof(float) * 4) < 0;
  }

  bool Point::operator<=(const Point& rhs) const
  {
    return memcmp(v, rhs.v, sizeof(float) * 4) <= 0;
  }

  bool Point::operator>(const Point& rhs) const
  {
    return memcmp(v, rhs.v, sizeof(float) * 4) > 0;
  }

  bool Point::operator>=(const Point& rhs) const
  {
    return memcmp(v, rhs.v, sizeof(float) * 4) >= 0;
  }

  Point Point::GetPosition(void) const
  {
    return *this;
  }

  void Point::SetPosition(const Point& pos)
  {
    *this = pos;
  }

  //void Point::Draw(ColorPtr rgba /*= nullptr*/) const
  //{
  //  DrawPoint((*this), rgba);
  //}

  std::ostream& operator<<(std::ostream& os, const Point& p)
  {
    os << "(" << p.x << ", " << p.y << ", " << p.z << ", " << p.w << ")";
    return os;
  }


  //////////////////////////////////////////////////////////////////////////
  //                            TESTS
  //////////////////////////////////////////////////////////////////////////


  static const float LOW = -100.0f;
  static const float HIGH = 100.0f;


  static bool TestPointVars(const Point& v, float x, float y, float z, float w)
  {
    if (AbsVal(v.x - x) > EPSILON ||
      AbsVal(v.y - y) > EPSILON ||
      AbsVal(v.z - z) > EPSILON ||
      AbsVal(v.w - w) > EPSILON)
      return false;

    return true;
  }


  static bool TestVectorVars(const Vector& v, float x, float y, float z, float w)
  {
    if (AbsVal(v.x - x) > EPSILON ||
      AbsVal(v.y - y) > EPSILON ||
      AbsVal(v.z - z) > EPSILON ||
      AbsVal(v.w - w) > EPSILON)
      return false;

    return true;
  }


  static bool TestDefaultConstructor(void)
  {
    Point a;

    return TestPointVars(a, 0.0f, 0.0f, 0.0f, 1.0f);
  }

  static bool TestNonDefaultConstructorWithoutDefaultInitializer(void)
  {
    float x = Random(LOW, HIGH);
    float y = Random(LOW, HIGH);
    float z = Random(LOW, HIGH);
    float w = Random(LOW, HIGH);
    Point a(x, y, z, w);

    return TestPointVars(a, x, y, z, w);
  }


  static bool TestNonDefaultConstructorWithDefaultInitializer(void)
  {
    float x = Random(LOW, HIGH);
    float y = Random(LOW, HIGH);
    float z = Random(LOW, HIGH);
    Point a(x, y, z);

    return TestPointVars(a, x, y, z, 1.0f);
  }
  

  static bool TestStaticOrigin(void)
  {
    Point o = Point::ORIGIN;
    return TestPointVars(o, 0.0f, 0.0f, 0.0f, 1.0f);
  }

  static bool TestDotPoint(void)
  {
    float x = Random(LOW, HIGH);
    float y = Random(LOW, HIGH);
    float z = Random(LOW, HIGH);
    float w = Random(LOW, HIGH);
    float X = Random(LOW, HIGH);
    float Y = Random(LOW, HIGH);
    float Z = Random(LOW, HIGH);
    float W = Random(LOW, HIGH);
    Point a(x, y, z, w);
    Point b(X, Y, Z, W);

    float d = a.DotPoint(b);

    return (d == (X * x + Y * y + Z * z + W * w));
  }

  static bool TestCrossPoint(void)
  {
    float x = Random(LOW, HIGH);
    float y = Random(LOW, HIGH);
    float z = Random(LOW, HIGH);
    float w = Random(LOW, HIGH);
    float X = Random(LOW, HIGH);
    float Y = Random(LOW, HIGH);
    float Z = Random(LOW, HIGH);
    float W = Random(LOW, HIGH);
    Point a(x, y, z, w);
    Point b(X, Y, Z, W);

    Point crossTest = a.CrossPoint(b);
    
    return TestPointVars(crossTest,
      Y * z - Z * y,
      Z * x - X * z,
      X * y - Y * x,
      1.0f);
  }

  static bool TestAddPoint(void)
  {
    float x = Random(LOW, HIGH);
    float y = Random(LOW, HIGH);
    float z = Random(LOW, HIGH);
    float w = Random(LOW, HIGH);
    float X = Random(LOW, HIGH);
    float Y = Random(LOW, HIGH);
    float Z = Random(LOW, HIGH);
    float W = Random(LOW, HIGH);
    Point a(x, y, z, w);
    Point b(X, Y, Z, W);

    a.AddPoint(b);
    
    return TestPointVars(a, x + X, y + Y, z + Z, w + W);
  }

  static bool TestMidpoint(void)
  {
    float x = Random(LOW, HIGH);
    float y = Random(LOW, HIGH);
    float z = Random(LOW, HIGH);
    float w = Random(LOW, HIGH);
    float X = Random(LOW, HIGH);
    float Y = Random(LOW, HIGH);
    float Z = Random(LOW, HIGH);
    float W = Random(LOW, HIGH);
    Point a(x, y, z, w);
    Point b(X, Y, Z, W);

    Point mid = a.GetMidpoint(b);

    return TestPointVars(mid, (x + X) * 0.5f, (y + Y) * 0.5f, (z + Z) * 0.5f, (w + W) * 0.5f);
  }

  static bool TestNegate(void)
  {
    float x = Random(LOW, HIGH);
    float y = Random(LOW, HIGH);
    float z = Random(LOW, HIGH);
    float w = Random(LOW, HIGH);
    Point a(x, y, z, w);
    Point b = -a;

    return TestPointVars(b, -x, -y, -z, -w);
  }


  static bool TestPointMinusPoint(void)
  {
    float x = Random(LOW, HIGH);
    float y = Random(LOW, HIGH);
    float z = Random(LOW, HIGH);
    float w = Random(LOW, HIGH);
    float X = Random(LOW, HIGH);
    float Y = Random(LOW, HIGH);
    float Z = Random(LOW, HIGH);
    float W = Random(LOW, HIGH);
    Point a(x, y, z, w);
    Point b(X, Y, Z, W);
    Vector c = a - b;

    return TestVectorVars(c, 
      a.x - b.x, 
      a.y - b.y, 
      a.z - b.z, 
      a.w - b.w);
  }

  static bool TestVectorAddition(void)
  {
    float x = Random(LOW, HIGH);
    float y = Random(LOW, HIGH);
    float z = Random(LOW, HIGH);
    float w = Random(LOW, HIGH);
    float X = Random(LOW, HIGH);
    float Y = Random(LOW, HIGH);
    float Z = Random(LOW, HIGH);
    float W = Random(LOW, HIGH);
    Point a(x, y, z, w);
    Vector b(X, Y, Z, W);
    Point c = a + b;

    return TestPointVars(c,
      x + X,
      y + Y,
      z + Z,
      w + W);
  }

  static bool TestVectorSubtraction(void)
  {
    float x = Random(LOW, HIGH);
    float y = Random(LOW, HIGH);
    float z = Random(LOW, HIGH);
    float w = Random(LOW, HIGH);
    float X = Random(LOW, HIGH);
    float Y = Random(LOW, HIGH);
    float Z = Random(LOW, HIGH);
    float W = Random(LOW, HIGH);
    Point a(x, y, z, w);
    Vector b(X, Y, Z, W);
    Point c = a - b;

    return TestPointVars(c,
      x - X,
      y - Y,
      z - Z,
      w - W);
  }

  static bool TestMultiplication(void)
  {
    float x = Random(LOW, HIGH);
    float y = Random(LOW, HIGH);
    float z = Random(LOW, HIGH);
    float w = Random(LOW, HIGH);
    float mult = Random(LOW, HIGH);
    Point a(x, y, z, w);
    Point b = a * mult;

    return TestPointVars(b,
      x * mult,
      y * mult,
      z * mult,
      w * mult);
  }

  static bool TestDivision(void)
  {
    float x = Random(LOW, HIGH);
    float y = Random(LOW, HIGH);
    float z = Random(LOW, HIGH);
    float w = Random(LOW, HIGH);
    float div = Random(LOW, HIGH);
    Point a(x, y, z, w);
    Point b = a / div;

    return TestPointVars(b,
      x / div,
      y / div,
      z / div,
      w / div);
  }

  static bool TestEquality(void)
  {
    float x = Random(LOW, HIGH);
    float y = Random(LOW, HIGH);
    float z = Random(LOW, HIGH);
    float w = Random(LOW, HIGH);
    Point a(x, y, z, w);
    Point b = a;

    if (a != b)
      return false;

    b.x = -a.x;

    if (a == b)
      return false;

    b.x = a.x;
    b.y = -a.y;

    if (a == b)
      return false;

    b.y = a.y;
    b.z = -a.z;

    if (a == b)
      return false;

    b.z = a.z;
    b.w = -a.w;

    if (a == b)
      return false;

    b.w = a.w;

    if (a != b)
      return false;

    return true;
  }

  bool Point::Test(void)
  {

    bool success = true;

    success = (success ? TestDefaultConstructor() : success);
    success = (success ? TestNonDefaultConstructorWithDefaultInitializer() : success);
    success = (success ? TestNonDefaultConstructorWithoutDefaultInitializer() : success);
    success = (success ? TestStaticOrigin() : success);
    success = (success ? TestMidpoint() : success);
    success = (success ? TestDotPoint() : success);
    success = (success ? TestCrossPoint() : success);
    success = (success ? TestNegate() : success);
    success = (success ? TestAddPoint() : success);
    success = (success ? TestPointMinusPoint() : success);
    success = (success ? TestVectorAddition() : success);
    success = (success ? TestVectorSubtraction() : success);
    success = (success ? TestMultiplication() : success);
    success = (success ? TestDivision() : success);
    success = (success ? TestEquality() : success);      

    return success;
  }

  std::string Point::GetName() const
  {
    return "Point";
  }

}// namespace Maths
