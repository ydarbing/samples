#include "Geometry/Geometry.h"
//#include "Serializer.h"

namespace Maths
{
  void Geometry::GetBoundingSphere(Vector& center, float& radius)const
  {
    Transform transform;
    transform.SetIdentity();
    Vector aabbMin, aabbMax;

    GetAabb(transform, aabbMin, aabbMax);

    radius = (aabbMax - aabbMin).Length() * float(0.5);
    center = (aabbMin + aabbMax) * float(0.5);
  }

  float Geometry::GetContactBreakingThreshold(float defaultContactThreshold)const
  {
    return GetAngularMotionDisc() * defaultContactThreshold;
  }

  float Geometry::GetAngularMotionDisc()const
  {
    Vector center;
    float disc;
    GetBoundingSphere(center, disc);
    disc += center.Length();
    return disc;
  }

  void Geometry::CalculateTemporalAabb(const Transform& curTrans, const Vector& linvel, const Vector& angvel, float timeStep, Vector& temporalAabbMin, Vector& temporalAabbMax) const
  {
    //start with static aabb
    GetAabb(curTrans, temporalAabbMin, temporalAabbMax);

    float temporalAabbMaxx = temporalAabbMax.x;
    float temporalAabbMaxy = temporalAabbMax.y;
    float temporalAabbMaxz = temporalAabbMax.z;
    float temporalAabbMinx = temporalAabbMin.x;
    float temporalAabbMiny = temporalAabbMin.y;
    float temporalAabbMinz = temporalAabbMin.z;

    // add linear motion
    Vector linearMotion = linvel * timeStep;

    if (linearMotion.x > float(0))
      temporalAabbMaxx += linearMotion.x;
    else
      temporalAabbMinx += linearMotion.x;

    if (linearMotion.y > float(0))
      temporalAabbMaxy += linearMotion.y;
    else
      temporalAabbMiny += linearMotion.y;

    if (linearMotion.z > float(0))
      temporalAabbMaxz += linearMotion.z;
    else
      temporalAabbMinz += linearMotion.z;

    // add conservative angular motion

    float angularMotion = angvel.Length() * GetAngularMotionDisc() * timeStep;
    Vector angularMotion3d(angularMotion, angularMotion, angularMotion);
    temporalAabbMin = Vector(temporalAabbMinx, temporalAabbMiny, temporalAabbMinz);
    temporalAabbMax = Vector(temporalAabbMaxx, temporalAabbMaxy, temporalAabbMaxz);

    temporalAabbMin -= angularMotion3d;
    temporalAabbMax += angularMotion3d;
  }


  const char* Geometry::Serialize(void* dataBuffer, Serializer* serializer) const
  {
    //CollisionShapeData* shapeData = (CollisionShapeData*)dataBuffer;
    //char* name = (char*)serializer->FindNameForPointer(this);
    //shapeData->m_name = (char*)serializer->GetUniquePointer(name);
    //if (shapeData->m_name)
    //  serializer->SerializeName(name);
    //
    //shapeData->m_shapeType = m_shapeType;
    //
    //memset(shapeData->m_padding, 0, sizeof(shapeData->m_padding));
    return "CollisionShapeData";

  }

  void Geometry::SerializeSingleShape(Serializer* serializer)const
  {
    //int len = CalculateSerializeBufferSize();
    //Chunk* chunk = serializer->allocate(len, 1);
    //const char* structType = Serialize(chunk->m_oldPtr, serializer);
    //serializer->FinalizeChunk(chunk, structType, E_SHAPE_CODE, (void*)this);
  }
}
