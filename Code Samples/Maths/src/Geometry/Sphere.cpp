#include "Geometry/Sphere.h"
#include "Draw.h"

namespace Maths
{
  Sphere::Sphere(void)
    :m_center(), m_radius(0), m_localScaling(1.0f, 1.0f, 1.0f)
  {
  }

  Sphere::Sphere(Point center, float radius)
    : m_center(center), m_radius(radius), m_localScaling(1.0f, 1.0f, 1.0f)
  {
  }

  Point Sphere::GetPosition(void) const
  {
    return m_center;
  }

  void Sphere::SetPosition(const Point& pos)
  {
    m_center = pos;
  }


  //void Sphere::Draw(ColorPtr rgba /*= nullptr*/) const
  //{
  //  DrawSphere(*this, rgba);
  //}

  const std::string Sphere::GetName() const
  {
    return "Sphere";
  }

  void Sphere::SetLocalScaling(const Vector& scaling)
  {
	  m_localScaling = scaling;
  }
  const Vector& Sphere::GetLocalScaling() const
  {
	  return m_localScaling;
  }

  void  Sphere::GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const
  {
	  (void)t;
	  (void)aabbMin;
	  (void)aabbMax;
  }
  void  Sphere::CalculateLocalInertia(float mass, Vector& inertia) const
  {
	  (void)mass;
	  (void)inertia;
  }
  void  Sphere::SetMargin(float margin) 
  {
	  (void)margin;
  }
  float Sphere::GetMargin() const
  {
	  return 0.0f;
  }

}// namespace Maths

