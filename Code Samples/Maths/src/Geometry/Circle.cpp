#include "Geometry/Circle.h"
#include "Draw.h"

namespace Maths
{


  Circle::Circle(void)
	:m_center(), m_normal(), m_radius(0), m_localScaling(1.0f, 1.0f, 1.0f)
  {
  }

  Circle::Circle(const Point& center, const Vector& norm, float rad)
    : m_center(center), m_normal(norm), m_radius(rad), m_localScaling(1.0f, 1.0f, 1.0f)
  {
    Vector::BuildOrthonormalBasis(m_dir0, m_dir1, norm);
  }

  Circle::Circle(const Point& center, const Vector& axis1, const Vector& axis2, float rad)
    : m_center(center), m_normal(axis1.Cross(axis2)), m_dir0(axis1), m_dir1(axis2), m_radius(rad), m_localScaling(1.0f, 1.0f, 1.0f)
  {

  }

  Point Circle::GetPosition(void) const
  {
    return m_center;
  }

  void Circle::SetPosition(const Point& pos)
  {
    m_center = pos;
  }

  //void Circle::Draw(ColorPtr rgba /*= nullptr*/) const
  //{
  //  DrawCircle(*this, rgba);
  //}

  const std::string Circle::GetName() const
  {
    return "Circle";
  }

  void Circle::SetLocalScaling(const Vector& scaling)
  {
	  m_localScaling = scaling;
  }

  const Vector& Circle::GetLocalScaling() const
  {
	  return m_localScaling;
  }

  void Circle::GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const
  {
	  (void)t;
	  (void)aabbMin;
	  (void)aabbMax;
  }

  void Circle::CalculateLocalInertia(float mass, Vector& inertia) const
  {
	  (void)mass;
	  (void)inertia;
  }

  void Circle::SetMargin(float margin)
  {
	  (void)margin;
  }

  float Circle::GetMargin() const
  {
	  return 0.0f;
  }

}// namespace Maths

