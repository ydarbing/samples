#include "Geometry/Box.h"

namespace Maths
{
  Box::Box(const Vector& boxHalfExtents)
    : PolyhedralConvexShape()
  {
    m_shapeType = E_BOX_SHAPE;

    Vector margin(GetMargin(), GetMargin(), GetMargin());

    m_implicitShapeDimensions = (boxHalfExtents * m_localScaling) - margin;

    SetSafeMargin(boxHalfExtents);
  }

  void Box::GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const
  {
    TransformAabb(GetHalfExtentsWithoutMargin(), GetHalfExtentsWithoutMargin(), GetMargin(), t, aabbMin, aabbMax);
  }

  void	Box::CalculateLocalInertia(float mass, Vector& inertia) const
  {
    Vector halfExtents = GetHalfExtentsWithMargin();

    float lx = float(2.)*(halfExtents.x);
    float ly = float(2.)*(halfExtents.y);
    float lz = float(2.)*(halfExtents.z);

    inertia.SetValue(mass / (float(12.0)) * (ly*ly + lz*lz),
                     mass / (float(12.0)) * (lx*lx + lz*lz),
                     mass / (float(12.0)) * (lx*lx + ly*ly));

  }

}// namespace Maths