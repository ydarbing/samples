#include "Geometry/Ray.h"
#include "Draw.h"

namespace Maths
{

  const Ray Ray::X_AXIS(Point::ORIGIN, Vector::UNIT_X);
  const Ray Ray::Y_AXIS(Point::ORIGIN, Vector::UNIT_Y);
  const Ray Ray::Z_AXIS(Point::ORIGIN, Vector::UNIT_Z);

  Ray::Ray(void)
    : m_origin(), m_dir()
  {
  }

  Ray::Ray(const Point& origin_, const Vector& dir_)
    :m_origin(origin_), m_dir(dir_)
  {
  }

  Point Ray::GetPosition(void) const
  {
    return m_origin;
  }

  void Ray::SetPosition(const Point& pos)
  {
    m_origin = pos;
  }

  void Ray::Draw(ColorPtr rgba /*= nullptr*/) const
  {
    DrawRay(*this, rgba);
  }

  const std::string Ray::GetName(void) const
  {
    return "Ray";
  }


}// namespace Maths
