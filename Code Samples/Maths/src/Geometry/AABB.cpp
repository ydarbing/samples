#include "Geometry/AABB.h"
#include "Draw.h"
#include "Vector.h"
#include "Utils.h"

namespace Maths
{
  AABB::AABB(void) 
    : m_min(), m_max(), m_localScaling(1.0f, 1.0f, 1.0f)
  {
  }

  AABB::AABB(float xMin, float xMax, float yMin, float yMax, float zMin, float zMax)
    : m_min(xMin, yMin, zMin), m_max(xMax, yMax, zMax), m_localScaling(1.0f, 1.0f, 1.0f)
  {
  }

  AABB::AABB(const Point& min_, const Point& max_)
    : m_min(min_), m_max(max_), m_localScaling(1.0f, 1.0f, 1.0f)
  {
  }

  AABB::AABB(const Point& center_, float xExtent, float yExtent, float zExtent)
	  : m_localScaling(1.0f, 1.0f, 1.0f)
  {
    m_min.x = center_.x - xExtent;
    m_min.y = center_.y - yExtent;
    m_min.z = center_.z - zExtent;

    m_max.x = center_.x + xExtent;
    m_max.y = center_.y + yExtent;
    m_max.z = center_.z + zExtent;
  }

  void AABB::GetCenterExtents(Point& center, float& xExtent, float& yExtent, float& zExtent) const
  {
    center = m_max.AddPoint(m_min) * 0.5f;
    xExtent = (m_max.x - m_min.x) * 0.5f;
    yExtent = (m_max.y - m_min.y) * 0.5f;
    zExtent = (m_max.z - m_min.z) * 0.5f;
  }

  bool AABB::HasXOverlap(const AABB& rhs) const
  {
    return (rhs.m_max.x <= m_max.x && m_max.x <= rhs.m_max.x);
  }

  bool AABB::HasYOverlap(const AABB& rhs) const
  {
    return (rhs.m_max.y <= m_max.y && m_max.y <= rhs.m_max.y);
  }

  bool AABB::HasZOverlap(const AABB& rhs) const
  {
    return (rhs.m_max.z <= m_max.z && m_max.z <= rhs.m_max.z);
  }

  bool AABB::TestIntersection(const AABB& rhs) const
  {
    return (HasXOverlap(rhs) && HasYOverlap(rhs) && HasZOverlap(rhs));
  }

  bool AABB::FindIntersection(const AABB& rhs, AABB& intersect) const
  {
    if (TestIntersection(rhs) == false)
      return false;

    intersect.m_min.x = (m_min.x <= rhs.m_min.x ? rhs.m_min.x : m_min.x);
    intersect.m_min.y = (m_min.y <= rhs.m_min.y ? rhs.m_min.y : m_min.y);
    intersect.m_min.z = (m_min.z <= rhs.m_min.z ? rhs.m_min.z : m_min.z);

    intersect.m_max.x = (m_max.x <= rhs.m_max.x ? m_max.x : rhs.m_max.x);
    intersect.m_max.y = (m_max.y <= rhs.m_max.y ? m_max.y : rhs.m_max.y);
    intersect.m_max.z = (m_max.z <= rhs.m_max.z ? m_max.z : rhs.m_max.z);
    return true;
  }

  Point AABB::GetPosition(void) const
  {
    return (m_min.AddPoint(m_max)) * 0.5f;
  }

  void AABB::SetPosition(const Point& pos)
  {
    float xExtent = (m_max.x - m_min.x) * 0.5f;
    float yExtent = (m_max.y - m_min.y) * 0.5f;
    float zExtent = (m_max.z - m_min.z) * 0.5f;

    m_min.x = pos.x - xExtent;
    m_min.y = pos.y - yExtent;
    m_min.z = pos.z - zExtent;

    m_max.x = pos.x + xExtent;
    m_max.y = pos.y + yExtent;
    m_max.z = pos.z + zExtent;
  }

  //void AABB::Draw(ColorPtr rgba /*= nullptr*/) const
  //{
  //  DrawAAB((*this), rgba);
  //}

  const std::string AABB::GetName() const
  {
    return "AABB";
  }

  void AABB::SetLocalScaling(const Vector& scaling)
  {
	  m_localScaling = scaling;
  }

  const Vector& AABB::GetLocalScaling() const
  {
	  return m_localScaling;
  }

  void AABB::GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const
  {
	  (void)t;
	  (void)aabbMin;
	  (void)aabbMax;
  }

  void AABB::CalculateLocalInertia(float mass, Vector& inertia) const
  {
	  (void)mass;
	  (void)inertia;
  }

  void AABB::SetMargin(float margin)
  {
	  (void)margin;
  }

  float AABB::GetMargin() const
  {
	  return 0.0f;
  }
}// namespace Maths
