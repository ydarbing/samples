#include "Geometry\ConvexShape.h"
#include "BroadphaseProxy.h"
#include "Utils.h"

#include "Geometry\Box.h"
#include "Geometry\Triangle.h"
#include "Geometry\Sphere.h"

namespace Maths
{

  ConvexShape::ConvexShape()
  {}

  void ConvexShape::Project(const Transform& trans, const Vector& dir, float& min, float& max, Vector& witnesPtMin, Vector& witnesPtMax) const
  {
    Vector localAxis = dir * trans.GetBasis();
    Vector vtx1 = trans(LocalGetSupportingVertex(localAxis));
    Vector vtx2 = trans(LocalGetSupportingVertex(-localAxis));

    min = vtx1.Dot(dir);
    max = vtx2.Dot(dir);
    witnesPtMin = vtx1;
    witnesPtMax = vtx2;

    if (min > max)
    {
      float temp = min;
      min = max;
      max = temp;
      witnesPtMin = vtx2;
      witnesPtMax = vtx1;
    }
  }

  static Vector ConvexHullSupport(Vector& localDirOrg, const Vector* points, int numPoints, const Vector& localScaling)
  {
    Vector vec = localDirOrg * localScaling;
    float maxDot;
    long ptIndex = vec.MaxDot(points, numPoints, maxDot);
    assert(ptIndex >= 0);
    Vector supportVec = points[ptIndex] * localScaling;
    return supportVec;
  }

  Vector ConvexShape::LocalGetSupportVertexWithoutMarginNonVirtual(const Vector& localDir) const
  {
    switch (m_shapeType)
    {
    case E_SPHERE_SHAPE:
      return Vector(0, 0, 0);
    case E_BOX_SHAPE:
    {
      Box* shape = (Box*)this;
      const Vector halfExtents = shape->GetImplicitShapeDimensions();


      return Vector(Fsels(localDir.x, halfExtents.x, -halfExtents.x),
        Fsels(localDir.y, halfExtents.y, -halfExtents.y),
        Fsels(localDir.z, halfExtents.z, -halfExtents.z));

    }
    case E_TRIANGLE_SHAPE:
    {
      Triangle* triangle = (Triangle*)this;
      Vector dir(localDir.x, localDir.y, localDir.z);
      Vector* vertices = &triangle->m_vertices[0];
      Vector dots = dir.Dot3(vertices[0], vertices[1], vertices[2]);
      Vector sup = vertices[dots.MaxAxis()];

      return Vector(sup.x, sup.y, sup.z);
    }
    case E_CYLINDER_SHAPE:
    {
      // TODO
      assert(0);
    }
    case E_CAPSULE_SHAPE:
    {
      // TODO
      assert(0);
    }
    case E_CONVEX_HULL_SHAPE:
    {
      // TODO
      assert(0);
    }
    case E_CONVEX_POINT_CLOUD_SHAPE:
    {
      // TODO
      assert(0);
    }
    default:
      return this->LocalGetSupportingVertexWithoutMargin(localDir);
    }
  }

  Vector ConvexShape::LocalGetSupportVertexNonVirtual(const Vector& localDir)const
  {
    Vector localDirNormal = localDir;
    if (localDirNormal.SquaredLength() < (EPSILON * EPSILON))
    {
      localDirNormal.SetValue(float(-1.), float(-1.), float(-1.));
    }
    localDirNormal.Normalize();

    return LocalGetSupportVertexWithoutMarginNonVirtual(localDirNormal) + GetMarginNonVirtual() * localDirNormal;
  }

  float ConvexShape::GetMarginNonVirtual() const
  {
    switch (m_shapeType)
    {
    case E_SPHERE_SHAPE:
    {
      // TODO
      assert(0);
      //Sphere* sphere = (Sphere*)this;
      //return sphere->GetRadius();
    }
    case E_BOX_SHAPE:
    {
      Box* box = (Box*)this;
      return box->GetMarginNonVirtual();
    }
    case E_TRIANGLE_SHAPE:
    {
      Triangle* triangle = (Triangle*)this;
      return triangle->GetMarginNonVirtual();
    }
    case E_CYLINDER_SHAPE:
    {
      ///TODO
      assert(0);
      //Cylinder* cylinder = (Cylinder*)this;
      //return cylinder->GetMarginNonVirtual();
    }
    case E_CONE_SHAPE:
    {
      assert(0);
      //Cone* coneShape = (Cone*)this;
      //return cone->GetMarginNonVirtual();
    }
    case E_CAPSULE_SHAPE:
    {
      assert(0);
      //Capsule* capsule = (Capsule*)this;
      //return capsule->GetMarginNonVirtual();
    }
    case E_CONVEX_POINT_CLOUD_SHAPE:
      /* fall through */
    case E_CONVEX_HULL_SHAPE:
    {
      PolyhedralConvexShape* convexHullShape = (PolyhedralConvexShape*)this;
      return convexHullShape->GetMarginNonVirtual();
    }
    default:
      return this->GetMargin();
    }
  }
}// namespace Maths

