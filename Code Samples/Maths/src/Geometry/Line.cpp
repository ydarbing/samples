#include "Geometry/Line.h"
#include "Draw.h"

namespace Maths
{


  Line::Line(void)
    : point(), direction(), localScaling(1.0f, 1.0f, 1.0f)
  {
  }

  Line::Line(const Point& point_, const Vector& dir)
	  : point(point_), direction(dir), localScaling(1.0f, 1.0f, 1.0f)
  {
  }

  Vector Line::GetDirection(void) const
  {
	  return direction;
  }

  void Line::SetDirection(Vector direction)
  {
	  this->direction = direction;
  }

  Point Line::GetPosition(void) const
  {
    return point;
  }

  void Line::SetPosition(const Point& pos)
  {
    point = pos;
  }

  //void Line::Draw(ColorPtr rgba /*= nullptr*/) const
  //{
  //  DrawLine((*this), rgba);
  //}

  const std::string Line::GetName() const
  {
    return "Line";
  }

  // since this is an infinite line getting an aabb is not applicable
  void Line::GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const
  {
	  (void)t;
	  (void)aabbMin;
	  (void)aabbMax;
  }

  void Line::SetLocalScaling(const Vector& scaling)
  {
	  localScaling = scaling;
  }

  const Vector& Line::GetLocalScaling() const
  {
	  return localScaling;
  }

  // not calculated
  void Line::CalculateLocalInertia(float mass, Vector& inertia) const
  {
	  (void)mass;
	  (void)inertia;
  }

  void Line::SetMargin(float margin)
  {
	  (void)margin;
  }

  float Line::GetMargin() const
  {
	  return 0.0f;
  }

}
