#include "Geometry/Segment.h"
#include "Draw.h"


namespace Maths
{
  Segment::Segment(void)
    : point0(), point1(), center(), direction(), extent(0.0f), m_localScaling(1.0f,1.0f,1.0f)
  {
  }

  Segment::Segment(const Point& p0, const Point& p1)
    : point0(p0), point1(p1), m_localScaling(1.0f, 1.0f, 1.0f)
  {
    ComputeCenterDirectionExtent();
  }

  Segment::Segment(const Point& center_, const Vector& dir, float extent_)
    : point0(), point1(), center(center_), direction(dir), extent(extent_), m_localScaling(1.0f, 1.0f, 1.0f)
  {
    ComputeEndPoints();
  }

  void Segment::ComputeCenterDirectionExtent(void)
  {
    center = point0.AddPoint(point1) * 0.5f;
    direction = (point1 - point0);
    extent = direction.Normalize() * 0.5f;
  }

  void Segment::ComputeEndPoints(void)
  {
    point0 = center - direction * extent;
    point1 = center + direction * extent;
  }

  Point Segment::GetPosition(void) const
  {
    return center;
  }

  void Segment::SetPosition(const Point& pos)
  {
    center = pos;
    ComputeEndPoints();
  }

  //void Segment::Draw(ColorPtr rgba /*= nullptr*/) const
  //{
  //  DrawSegment((*this), rgba);
  //}

  const std::string Segment::GetName() const
  {
    return "Segment";
  }

  void Segment::SetLocalScaling(const Vector& scaling)
  {
	  m_localScaling = scaling;
  }
  const Vector& Segment::GetLocalScaling() const
  {
	  return m_localScaling;
  }

  void  Segment::GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const
  {
	  (void)t;
	  (void)aabbMin;
	  (void)aabbMax;
  }
  void  Segment::CalculateLocalInertia(float mass, Vector& inertia) const
  {
	  (void)mass;
	  (void)inertia;
  }
  void  Segment::SetMargin(float margin)
  {
	  (void)margin;
  }
  float Segment::GetMargin() const
  {
	  return 0.0f;
  }
}// namespace Maths
