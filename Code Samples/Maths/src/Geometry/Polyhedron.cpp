#include "Geometry/Polyhedron.h"

#include "Draw.h"
#include "Vector.h"
#include "Geometry/Point.h"


namespace Maths
{

  Polyhedron::Polyhedron(std::vector<Point> verts, unsigned numTris, std::vector<unsigned> indices)
    : m_vertices(verts), m_numTriangles(numTris), m_indices(indices), m_localScaling(1.0f,1.0f,1.0f)
  {
  }

  Polyhedron::Polyhedron(const Polyhedron& other)
    : m_numTriangles(0), m_vertices(), m_indices(), m_localScaling(1.0f, 1.0f, 1.0f)
  {
    (*this) = other;
  }


  Polyhedron::~Polyhedron(void)
  {
    ClearVertices();
  }

  Polyhedron& Polyhedron::operator=(const Polyhedron& rhs)
  {
    m_vertices.clear();
    m_indices.clear();

    m_numTriangles = rhs.m_numTriangles;
    m_vertices.reserve(rhs.m_vertices.size());
    for (unsigned i = 0; i < rhs.m_vertices.size(); ++i)
      m_vertices.push_back(rhs.m_vertices[i]);

    m_indices.reserve(rhs.m_indices.size());
    for (unsigned i = 0; i < rhs.m_indices.size(); ++i)
      m_indices[i] = rhs.m_indices[i];

	m_localScaling = rhs.m_localScaling;
    return *this;
  }

  unsigned Polyhedron::GetVertexCount(void) const
  {
    return static_cast<unsigned>(m_vertices.size());
  }

  const std::vector<Point>&  Polyhedron::GetVertices(void) const
  {
    return m_vertices;
  }

  const Point& Polyhedron::GetVertex(unsigned i) const
  {
    // assert(0 <= i && i < m_numVertices)
    return m_vertices[i];
  }

  unsigned Polyhedron::GetTriangleCount(void) const
  {
    return m_numTriangles;
  }

  unsigned Polyhedron::GetIndexCount(void) const
  {
    return static_cast<unsigned>(m_indices.size());
  }

  const std::vector<unsigned>& Polyhedron::GetIndices(void) const
  {
    return m_indices;
  }

  const unsigned* Polyhedron::GetTriangle(unsigned i) const
  {
    //assert(i < m_numTriangles);
    return &(m_indices[3 * i]);
  }

  void Polyhedron::SetVertex(unsigned i, const Point& vertex)
  {
    //assert(i < m_vertices.size());
    m_vertices[i] = vertex;
  }

  Point Polyhedron::ComputeAverage(void) const
  {
    Point avg = m_vertices[0];

    for (unsigned i = 1; i < m_vertices.size(); ++i)
      avg = avg.AddPoint(m_vertices[i]);

    avg /= float(m_vertices.size());
    return avg;
  }

  float Polyhedron::ComputeSurfaceArea(void) const
  {
    float SA = 0.0f;
    const unsigned* ind = &m_indices[0];

    for (unsigned i = 0; i < m_numTriangles; ++i)
    {
      unsigned v0 = *ind++;
      unsigned v1 = *ind++;
      unsigned v2 = *ind++;

      Vector edge0 = m_vertices[v1] - m_vertices[v0];
      Vector edge1 = m_vertices[v2] - m_vertices[v0];
      SA += (edge0.Cross(edge1)).Length();
    }
    SA *= 0.5f;
    return SA;
  }

  float Polyhedron::ComputeVolume(void) const
  {
    float volume = 0.0f;
    const unsigned* ind = &m_indices[0];
    for (unsigned i = 0; i < m_numTriangles; ++i)
    {
      unsigned v0 = *ind++;
      unsigned v1 = *ind++;
      unsigned v2 = *ind++;

      Point cross = m_vertices[v1].CrossPoint(m_vertices[v2]);
      volume += m_vertices[v0].DotPoint(cross);
    }

    volume /= 6.0f;
    return volume;
  }

  Point Polyhedron::GetPosition(void) const
  {
    return ComputeAverage();
  }

  void Polyhedron::SetPosition(const Point& pos)
  {
    Vector* to = new Vector[m_vertices.size()];
    Point avg = ComputeAverage();

    for (unsigned i = 0; i < m_vertices.size(); ++i)
      to[i] = m_vertices[i] - avg;

    avg = pos;
    for (unsigned i = 0; i < m_vertices.size(); ++i)
      m_vertices[i] = avg + to[i];

    delete[] to;
  }

  void Polyhedron::SetLocalScaling(const Vector& scaling)
  {
	  m_localScaling = scaling;
  }
  const Vector& Polyhedron::GetLocalScaling() const
  {
	  return m_localScaling;
  }

  void  Polyhedron::GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const
  {
	  (void)t;
	  (void)aabbMin;
	  (void)aabbMax;
  }
  void  Polyhedron::CalculateLocalInertia(float mass, Vector& inertia) const
  {
	  (void)mass;
	  (void)inertia;
  }
  void  Polyhedron::SetMargin(float margin)
  {
	  (void)margin;
  }
  float Polyhedron::GetMargin() const
  {
	  return 0.0f;
  }

  //void Polyhedron::Draw(ColorPtr rgba /*= nullptr*/) const
  //{
  //  DrawPolyhedron((*this), rgba);
  //}

  void Polyhedron::ClearVertices(void)
  {
    m_vertices.clear();
  }

  const std::string Polyhedron::GetName() const
  {
    return "Polyhedron";
  }

}// namespace Maths