#include "Geometry/OOB.h"
#include "Draw.h"

namespace Maths
{
	OOB::OOB(void)
		: m_center(), m_localScaling(1.0f, 1.0f, 1.0f)
	{
		m_extent[0] = 0.0f;
		m_extent[1] = 0.0f;
		m_extent[2] = 0.0f;
	}

	OOB::OOB(const Point& cent, const Vector& ax0, const Vector& ax1, const Vector& ax2, float ext0, float ext1, float ext2)
		:m_center(cent), m_localScaling(1.0f, 1.0f, 1.0f)
	{
		m_axis[0] = ax0;
		m_axis[1] = ax1;
		m_axis[2] = ax2;
		m_extent[0] = ext0;
		m_extent[1] = ext1;
		m_extent[2] = ext2;
	}

	OOB::OOB(const Point& cent, const Vector axes[3], const float exts[3])
		:m_center(cent), m_localScaling(1.0f, 1.0f, 1.0f)
	{
		m_axis[0] = axes[0];
		m_axis[1] = axes[1];
		m_axis[2] = axes[2];
		m_extent[0] = exts[0];
		m_extent[1] = exts[1];
		m_extent[2] = exts[2];
	}

	void OOB::ComputeVertices(Point points[8]) const
	{
		Vector extAxis0 = m_axis[0] * m_extent[0];
		Vector extAxis1 = m_axis[1] * m_extent[1];
		Vector extAxis2 = m_axis[2] * m_extent[2];
		// BB = Back Bottom,  BT = Back Top, FB = Front Bottom, FT = Front Top
		points[0] = m_center - extAxis0 - extAxis1 - extAxis2; // BBL corner
		points[1] = m_center + extAxis0 - extAxis1 - extAxis2; // BBR corner
		points[2] = m_center + extAxis0 - extAxis1 + extAxis2; // BTR corner
		points[3] = m_center - extAxis0 - extAxis1 + extAxis2; // BTL corner
		points[4] = m_center - extAxis0 + extAxis1 - extAxis2; // FBL corner
		points[5] = m_center + extAxis0 + extAxis1 - extAxis2; // FBR corner
		points[6] = m_center + extAxis0 + extAxis1 + extAxis2; // FTR corner
		points[7] = m_center - extAxis0 + extAxis1 + extAxis2; // FTL corner
	}

	Point OOB::GetPosition(void) const
	{
		return m_center;
	}

	void OOB::SetPosition(const Point& pos)
	{
		m_center = pos;
	}

	//void OOB::Draw(ColorPtr rgba /*= nullptr*/) const
	//{
	//	DrawOOB(*this, rgba);
	//}

	const std::string OOB::GetName() const
	{
		return "OOB";
	}


	void OOB::GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const
	{
		(void)t;
		(void)aabbMin;
		(void)aabbMax;
	}

	void OOB::SetLocalScaling(const Vector& scaling)
	{
		m_localScaling = scaling;
	}

	const Vector& OOB::GetLocalScaling() const
	{
		return m_localScaling;
	}

	void OOB::CalculateLocalInertia(float mass, Vector& inertia) const
	{
		(void)mass;
		(void)inertia;
	}

	void OOB::SetMargin(float margin)
	{
		(void)margin;
	}

	float OOB::GetMargin() const
	{
		return 0.0f;
	}

}// namespace Maths
