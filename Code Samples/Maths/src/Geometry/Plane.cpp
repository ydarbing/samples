#include "Geometry/Plane.h"
#include "Geometry/Point.h"
#include "Vector.h"
#include "Draw.h"

namespace Maths
{
  const Plane Plane::XY_PLANE(0.0f, 0.0f, 1.0f, 0.0f);
  const Plane Plane::XZ_PLANE(0.0f, 1.0f, 0.0f, 0.0f);
  const Plane Plane::YZ_PLANE(1.0f, 0.0f, 0.0f, 0.0f);

  Plane::Plane()
    : a(0.0f), b(0.0f), c(0.0f), d(0.0f), m_localScaling(1.0f, 1.0f, 1.0f)
  {
  }

  Plane::Plane(float a_, float b_, float c_, float d_)
    : a(a_), b(b_), c(c_), d(d_), m_localScaling(1.0f, 1.0f, 1.0f)
  {
  }

  Plane::~Plane()
  {
  }

  Plane::Plane(const Vector& normal, const Point& point)
    : a(normal.x), b(normal.y), c(normal.z), d(-(normal.DotPoint(point))), m_localScaling(1.0f, 1.0f, 1.0f)
  {
  }

  Plane::Plane(const Point& p0, const Point& p1, const Point& p2)
	  : m_localScaling(1.0f, 1.0f, 1.0f)
  {
    Vector edge1 = p1 - p0;
    Vector edge2 = p2 - p0;
    Vector normal = edge1.Cross(edge2);
    normal.Normalize();

    a = normal.x;
    b = normal.y;
    c = normal.z;
    d = -(normal.DotPoint(p0));
  }


  float Plane::Normalize(float epsilon /*= EPSILON*/)
  {
    float length = sqrt(a * a + b * b + c * c);

    if (length > epsilon)
    {
      float invLength = 1.0f / length;
      a *= invLength;
      b *= invLength;
      c *= invLength;
      d *= invLength;
    }

    return length;
  }

  float Plane::DistanceTo(const Point& point) const
  {
    return (a * point.x + b * point.y + c * point.z + d);
  }

  Maths::Vector Plane::GetNormal(void) const
  {
    return Vector(a, b, c);
  }

  float Plane::GetConstant(void) const
  {
    return d;
  }

  Plane::PLANE_SIDE Plane::WhichSide(const Point& point) const
  {
    float distance = DistanceTo(point);

    if (distance < 0.0f)
      return BACK;
    else if (distance > 0.0f)
      return FRONT;
    else
      return ON;
  }

  bool Plane::operator==(const Plane& rhs) const
  {
    if (AbsVal(a - rhs.a) > EPSILON ||
        AbsVal(b - rhs.b) > EPSILON ||
        AbsVal(c - rhs.c) > EPSILON ||
        AbsVal(d - rhs.d) > EPSILON )
      return false;

    return true;
  }

  bool Plane::operator!=(const Plane& rhs) const
  {
    return !(*this == rhs);
  }

  Point Plane::GetPosition(void) const
  {
    return Point::ORIGIN;
  }

  void Plane::SetPosition(const Point& pos)
  {
    // nothing
  }


  void Plane::SetLocalScaling(const Vector& scaling)
  {
	  m_localScaling = scaling;
  }
  const Vector& Plane::GetLocalScaling() const
  {
	  return m_localScaling;
  }

  void  Plane::GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const
  {
	  (void)t;
	  (void)aabbMin;
	  (void)aabbMax;
  }
  void  Plane::CalculateLocalInertia(float mass, Vector& inertia) const
  {
	  (void)mass;
	  (void)inertia;
  }
  void  Plane::SetMargin(float margin)
  {
	  (void)margin;
  }
  float Plane::GetMargin() const
  {
	  return 0.0f;
  }

  //void Plane::Draw(ColorPtr rgba /*= nullptr*/) const
  //{
  //  unsigned unitsToDraw = 1000;
  //  DrawPlane((*this), &Point::ORIGIN, rgba, unitsToDraw);
  //}

  const std::string Plane::GetName() const
  {
    return "Plane";
  }

  std::ostream& operator<<(std::ostream& os, const Plane& p)
  {
    os << "(" << p.a << "x) + (" << p.b << "y) + (" << p.c << "c) + (" << p.d << ")";
    return os;
  }

}// namespace Maths

