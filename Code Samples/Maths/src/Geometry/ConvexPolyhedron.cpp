#include "Geometry/ConvexPolyhedron.h"

#include "Draw.h"
#include "Vector.h"
#include "Geometry/Point.h"
#include "Utils.h"

namespace Maths
{


  ConvexPolyhedron::ConvexPolyhedron(std::vector<Point> verts, unsigned numTris, std::vector<unsigned> indices, NCPlane *planes)
    : Polyhedron(verts, numTris, indices)
  {
    if (planes)
    {
      m_planes = planes;
    }
    else
    {
      m_planes = new NCPlane[numTris];
      UpdatePlanes();
    }
  }

  ConvexPolyhedron::ConvexPolyhedron(const ConvexPolyhedron &other)
    : Polyhedron(other), m_planes(nullptr)
  {
    *this = other;
  }

  ConvexPolyhedron::~ConvexPolyhedron(void)
  {
    delete[] m_planes;
  }

  ConvexPolyhedron& ConvexPolyhedron::operator=(const ConvexPolyhedron &rhs)
  {
    Polyhedron::operator=(rhs);

    m_planes = new NCPlane[m_numTriangles];

    for (unsigned i = 0; i < m_numTriangles; ++i)
      m_planes[i] = rhs.m_planes[i];

    m_sharingTriangles = rhs.m_sharingTriangles;
    return *this;
  }

  const ConvexPolyhedron::NCPlane* ConvexPolyhedron::GetPlanes(void) const
  {
    return m_planes;
  }

  const ConvexPolyhedron::NCPlane& ConvexPolyhedron::GetPlane(unsigned i) const
  {
    //assert(i < m_numTriangles);
    return m_planes[i];
  }

  void ConvexPolyhedron::SetVertex(unsigned i, const Point& vertex)
  {
    Polyhedron::SetVertex(i, vertex);
    const unsigned* ind = &m_indices[0];

    for (unsigned j = 0; j < m_numTriangles; ++i)
    {
      unsigned v0 = *ind++;
      unsigned v1 = *ind++;
      unsigned v2 = *ind++;
      
      if (i == v0 || i == v1 || i == v2)
        m_sharingTriangles.insert(j);
    }
  }

  void ConvexPolyhedron::UpdatePlanes(void)
  {
    if (!m_sharingTriangles.empty())
    {
      Point avg = ComputeAverage();

      for (unsigned i = 0; i < m_numTriangles; ++i)
        UpdatePlane(i, avg);
    }
  }

  bool ConvexPolyhedron::IsConvex(float threshold /*= 0.0f*/) const
  {
    float maxDist = -MAX_FLOAT;
    float minDist = MAX_FLOAT;

    for (unsigned i = 0; i < m_numTriangles; ++i)
    {
      const NCPlane& plane = m_planes[i];
      for (unsigned j = 0; j < m_vertices.size(); ++j)
      {
        float dist = plane.first.DotPoint(m_vertices[j]) - plane.second;
        if (dist < threshold)
          return false;
        if (dist < minDist)
          minDist = dist;
        if (dist > maxDist)
          maxDist = dist;
      }
    }
    return true;
  }

  bool ConvexPolyhedron::Contains(const Point& p, float threshold /*= 0.0f*/) const
  {
    for (unsigned i = 0; i < m_numTriangles; ++i)
    {
      const NCPlane& plane = m_planes[i];

      float dist = plane.first.DotPoint(p) - plane.second;
      if (dist < threshold)
        return false;
    }
    return true;
  }

  void ConvexPolyhedron::UpdatePlane(unsigned i, const Point &average)
  {
    unsigned base = i * 3;
    unsigned v0 = m_indices[base++];
    unsigned v1 = m_indices[base++];
    unsigned v2 = m_indices[base];

    Point vert0 = m_vertices[v0];
    Point vert1 = m_vertices[v1];
    Point vert2 = m_vertices[v2];

    Vector diff = average - vert0;
    Vector edge1 = vert1 - vert0;
    Vector edge2 = vert2 - vert0;
    Vector norm = edge2.Cross(edge1);

    float len = norm.Length();

    if (len > EPSILON)
    {
      norm /= len;
      float dot = norm.Dot(diff);
      if (dot < 0.0f)
        norm = -norm;
    } 
    else
    {
      norm = diff;
      norm.Normalize();
    }

    m_planes[i].first = norm;
    m_planes[i].second = norm.DotPoint(vert0);
  }

  //void ConvexPolyhedron::Draw(ColorPtr rgba /*= nullptr*/) const
  //{
  //  DrawConvexPolyhedron((*this), rgba);
  //}

  const std::string ConvexPolyhedron::GetName() const
  {
    return "ConvexPolyhedron";
  }

}// namespace Maths
