
#include "Quaternion.h"
#include "Vector.h"
#include "Matrix4.h"


 namespace Maths
{ 
  const Quaternion Quaternion::ZERO(0.0f, 0.0f, 0.0f, 0.0f);
  const Quaternion Quaternion::IDENTITY(1.0f, 0.0f, 0.0f, 0.0f);


  Quaternion::Quaternion(void)
    : w(1.0f), x(0.0f), y(0.0f), z(0.0f)
  {
  }

  Quaternion::Quaternion(float ww, float xx, float yy, float zz)
    : w(ww), x(xx), y(yy), z(zz)
  {
  }

  Quaternion::Quaternion(float x, float y, float z)
  {
    *this = Quaternion(Vector::UNIT_Z, z) * Quaternion(Vector::UNIT_Y, y) * Quaternion(Vector::UNIT_X, x);
  }

  Quaternion::Quaternion(const Matrix4& rotation)
  {
    FromRotationMatrix(rotation);
  }

  Quaternion::Quaternion(const Vector& axis, float angle)
  {
    FromAxisAngle(axis, angle);
  }

  float Quaternion::Length() const
  {
    return sqrt(w * w + x * x + y * y + z * z);
  }

  float Quaternion::SquaredLength() const
  {
    return (w * w + x * x + y * y + z * z);
  }

  float Quaternion::Dot(const Quaternion& rhs) const
  {
    return (w * rhs.w + x * rhs.x + y * rhs.y + z * rhs.z);
  }

  float Quaternion::Normalize()
  {
    float len = Length();

    if (len < Maths::EPSILON)
      len = w = x = y = z = 0.0f;
    else
    {
      float invLen = 1.0f / len;
      w *= invLen;
      x *= invLen;
      y *= invLen;
      z *= invLen;
    }
    return len;
  }

  Quaternion Quaternion::Inverse() const
  {
    float norm = Length();
    if (norm == 0.0f)
      return Quaternion(0.0f, 0.0f, 0.0f, 0.0f);

    norm = 1.0f / norm;
    return Quaternion(w * norm,
      -x * norm,
      -y * norm,
      -z * norm);
  }

  Quaternion Quaternion::Conjugate() const
  {
    return Quaternion(w, -x, -y, -z);
  }

  void Quaternion::AddScaledVector(const Vector& vec, float scale)
  {
    Quaternion q(0.0f,
      vec.x * scale,
      vec.y * scale,
      vec.z * scale);

    q = q * (*this);
    w += q.w * 0.5f;
    x += q.x * 0.5f;
    y += q.y * 0.5f;
    z += q.z * 0.5f;
  }

  Quaternion& Quaternion::operator=(const Quaternion &rhs)
  {
    w = rhs.w;
    x = rhs.x;
    y = rhs.y;
    z = rhs.z;
    return *this;
  }

  bool Quaternion::operator==(const Quaternion &rhs) const
  {
    using namespace Maths;

    if (AbsVal(w - rhs.w) > EPSILON)
      return false;
    if (AbsVal(x - rhs.x) > EPSILON)
      return false;
    if (AbsVal(y - rhs.y) > EPSILON)
      return false;
    if (AbsVal(z - rhs.z) > EPSILON)
      return false;

    return true;
  }

  bool Quaternion::operator!=(const Quaternion &rhs) const
  {
    return !(*this == rhs);
  }

  bool Quaternion::operator<(const Quaternion &rhs) const
  {
    return memcmp(v, rhs.v, sizeof(float) * 4) < 0;
  }

  bool Quaternion::operator<=(const Quaternion &rhs) const
  {
    return memcmp(v, rhs.v, sizeof(float) * 4) <= 0;
  }

  bool Quaternion::operator>(const Quaternion &rhs) const
  {
    return memcmp(v, rhs.v, sizeof(float) * 4) > 0;
  }

  bool Quaternion::operator>=(const Quaternion &rhs) const
  {
    return memcmp(v, rhs.v, sizeof(float) * 4) >= 0;
  }

  Quaternion Quaternion::operator+(const Quaternion &rhs) const
  {
    return Quaternion(w + rhs.w, x + rhs.x, y + rhs.y, z + rhs.y);
  }

  Quaternion Quaternion::operator-(const Quaternion &rhs) const
  {
    return Quaternion(w - rhs.w, x - rhs.x, y - rhs.y, z - rhs.z);
  }

  Quaternion Quaternion::operator*(const Quaternion &rhs) const
  {
    float ww = (v[0] * rhs.v[0]) - (v[1] * rhs.v[1]) - (v[2] * rhs.v[2]) - (v[3] * rhs.v[3]);
    float xx = (v[0] * rhs.v[1]) + (v[1] * rhs.v[0]) + (v[2] * rhs.v[3]) - (v[3] * rhs.v[2]);
    float yy = (v[0] * rhs.v[2]) - (v[1] * rhs.v[3]) + (v[2] * rhs.v[0]) + (v[3] * rhs.v[1]);
    float zz = (v[0] * rhs.v[3]) + (v[1] * rhs.v[2]) - (v[2] * rhs.v[1]) + (v[3] * rhs.v[0]);

    return Quaternion(ww, xx, yy, zz);
  }

  Vector Quaternion::operator*(const Vector& vec) const
  {
    Vector uv, uuv;
    Vector qvec(x, y, z);

    uv = qvec.Cross(vec);
    uuv = qvec.Cross(uuv);
    uv *= (2.0f * w);
    uuv *= 2.0f;

    return vec + uv + uuv;
  }

  Quaternion Quaternion::operator*(float scalar) const
  {
    return Quaternion(w * scalar,
      x * scalar,
      y * scalar,
      z * scalar);
  }

  Quaternion Quaternion::operator/(float scalar) const
  {
    using namespace Maths;

    if (scalar == 0.0f)
      return Quaternion(MAX_FLOAT, MAX_FLOAT, MAX_FLOAT, MAX_FLOAT);

    scalar = 1.0f / scalar;
    return Quaternion(w * scalar,
      x * scalar,
      y * scalar,
      z * scalar);
  }

  Quaternion Quaternion::operator-(void) const
  {
    return Quaternion(-w, -x, -y, -z);
  }

  Quaternion& Quaternion::operator+=(const Quaternion &rhs)
  {
    w += rhs.w;
    x += rhs.x;
    y += rhs.y;
    z += rhs.z;
    return (*this);
  }

  Quaternion& Quaternion::operator-=(const Quaternion &rhs)
  {
    w -= rhs.w;
    x -= rhs.x;
    y -= rhs.y;
    z -= rhs.z;
    return (*this);
  }

  Quaternion& Quaternion::operator*=(float scalar)
  {
    w *= scalar;
    x *= scalar;
    y *= scalar;
    z *= scalar;
    return (*this);
  }

  Quaternion& Quaternion::operator/=(float scalar)
  {
    if (scalar == 0.0f)
      w = x = y = z = Maths::MAX_FLOAT;
    else
    {
      scalar = 1.0f / scalar;
      w *= scalar;
      x *= scalar;
      y *= scalar;
      z *= scalar;
    }
    return (*this);
  }

  void Quaternion::SetValue(const float & _x, const float & _y, const float & _z)
  {
    x = _x;
    y = _y;
    z = _z;
    w = 0.f;
  }

  void Quaternion::SetValue(const float & _x, const float & _y, const float & _z, const float & _w)
  {
    x = _x;
    y = _y;
    z = _z;
    w = _w;
  }

  Quaternion& Quaternion::SetRotationX(float radians)
  {
    float sina, cosa;
    Maths::GetSinCos(radians * 0.5f, sina, cosa);
    w = cosa;
    x = sina;
    y = 0.0f;
    z = 0.0f;
    return (*this);
  }

  Quaternion& Quaternion::SetRotationY(float radians)
  {
    float sina, cosa;
    Maths::GetSinCos(radians * 0.5f, sina, cosa);
    w = cosa;
    x = 0.0f;
    y = sina;
    z = 0.0f;
    return (*this);
  }

  Quaternion& Quaternion::SetRotationZ(float radians)
  {
    float sina, cosa;
    Maths::GetSinCos(radians * 0.5f, sina, cosa);
    w = cosa;
    x = 0.0f;
    y = 0.0f;
    z = sina;
    return (*this);
  }

  Quaternion& Quaternion::SetRotationXY(float xRads, float yRads)
  {
    Quaternion xRot, yRot;
    xRot.SetRotationX(xRads);
    yRot.SetRotationY(yRads);
    (*this) = yRot * xRot;
    return (*this);
  }

  Quaternion& Quaternion::SetRotationXZ(float xRads, float zRads)
  {
    Quaternion xRot, zRot;
    xRot.SetRotationX(xRads);
    zRot.SetRotationZ(zRads);
    (*this) = zRot * xRot;
    return (*this);
  }

  Quaternion& Quaternion::SetRotationYZ(float yRads, float zRads)
  {
    Quaternion yRot, zRot;
    yRot.SetRotationY(yRads);
    zRot.SetRotationZ(zRads);
    (*this) = zRot * yRot;
    return (*this);
  }

  Quaternion& Quaternion::SetRotationXYZ(float xRads, float yRads, float zRads)
  {
    Quaternion xRot, yRot, zRot;
    xRot.SetRotationX(xRads);
    yRot.SetRotationY(yRads);
    zRot.SetRotationZ(zRads);
    (*this) = zRot * yRot * xRot;
    return (*this);
  }

  Quaternion& Quaternion::FromEulerAngles(float pitch, float yaw, float roll, bool radians /*= true*/)
  {
    Matrix4 mat(yaw, pitch, roll, radians);
    FromRotationMatrix(mat);
    return (*this);
  }

  Vector Quaternion::GetEulerAngles(bool asRadians /*= true*/) const
  {
    Matrix4 mat;
    ToRotationMatrix(mat);
    return mat.GetEulerAngles(asRadians);
  }

  Quaternion operator*(float scalar, const Quaternion& rhs)
  {
    return (rhs * scalar);
  }

  std::ostream& operator<<(std::ostream &os, const Quaternion& q)
  {
    os << "(" << q.w << ", " << q.x << ", " << q.y << ", " << q.z << ")";
    return os;
  }

  void Quaternion::FromRotationMatrix(const Matrix4& rotation)
  {
    const int next[3] = { 1, 2, 0 };

    float trace = rotation.m[0][0] + rotation.m[1][1] + rotation.m[2][2];
    float root;

    if (trace > 0.0f)
    {
      root = sqrt(trace + 1.0f);
      w = 0.5f * root;
      root = 0.5f / root;
      x = (rotation.m[2][1] - rotation.m[1][2]) * root;
      y = (rotation.m[0][2] - rotation.m[2][0]) * root;
      z = (rotation.m[1][0] - rotation.m[0][1]) * root;
    }
    else
    {
      int i = 0;
      if (rotation.m[1][1] > rotation.m[0][0])
        i = 1;

      if (rotation.m[2][2] > rotation.m[i][i])
        i = 2;

      int j = next[i];
      int k = next[j];

      root = sqrt(rotation.m[i][i] - rotation.m[j][j] - rotation.m[k][k] + 1.0f);
      float* quat[3] = { &x, &y, &z };
      *quat[i] = 0.5f * root;
      root = 0.5f / root;
      v[0] = (rotation.m[k][j] - rotation.m[j][k]) * root;
      *quat[j] = (rotation.m[j][i] + rotation.m[i][j]) * root;
      *quat[k] = (rotation.m[k][i] + rotation.m[i][k]) * root;
    }
  }

  void Quaternion::ToRotationMatrix(Matrix4& rotation) const
  {
    float twoX = 2.0f * v[1];
    float twoY = 2.0f * v[2];
    float twoZ = 2.0f * v[3];
    float twoWX = twoX * v[0];
    float twoWY = twoY * v[0];
    float twoWZ = twoZ * v[0];
    float twoXX = twoX * v[1];
    float twoXY = twoY * v[1];
    float twoXZ = twoZ * v[1];
    float twoYY = twoY * v[2];
    float twoYZ = twoZ * v[2];
    float twoZZ = twoZ * v[3];

    rotation.m[0][0] = 1.0f - (twoYY + twoZZ);
    rotation.m[0][1] = twoXY - twoWZ;
    rotation.m[0][2] = twoXZ + twoWY;
    rotation.m[0][3] = 0.0f;
    rotation.m[1][0] = twoXY + twoWZ;
    rotation.m[1][1] = 1.0f - (twoXX + twoZZ);
    rotation.m[1][2] = twoYZ - twoWX;
    rotation.m[1][3] = 0.0f;
    rotation.m[2][0] = twoXZ - twoWY;
    rotation.m[2][1] = twoYZ + twoWX;
    rotation.m[2][2] = 1.0f - (twoXX + twoYY);
    rotation.m[2][3] = 0.0f;
    rotation.m[3][0] = 0.0f;
    rotation.m[3][1] = 0.0f;
    rotation.m[3][2] = 0.0f;
    rotation.m[3][3] = 1.0f;
  }

  void Quaternion::FromAxisAngle(const Vector& axis, float angle)
  {
    float halfAngle = 0.5f * angle;
    float sina, cosa;
    Maths::GetSinCos(halfAngle, sina, cosa);

    v[0] = cosa;
    v[1] = sina * axis.x;
    v[2] = sina * axis.y;
    v[3] = sina * axis.z;
  }

  void Quaternion::ToAxisAngle(Vector& axis, float& angle) const
  {
    float sqrLen = x * x + y * y + z * z;

    if (sqrLen > 0.0f)
    {
      angle = 2.0f * acos(w);
      float invLength = 1.0f / sqrt(sqrLen);
      axis.x = x * invLength;
      axis.y = y * invLength;
      axis.z = z * invLength;
    }
    else
    {
      angle = 0.0f;
      axis.x = 1.0f;
      axis.y = 0.0f;
      axis.z = 0.0f;
    }

  }

  Vector Quaternion::Rotate(const Vector& vec) const
  {
    Matrix4 rotation;
    ToRotationMatrix(rotation);
    return (rotation * vec);
  }

  Quaternion& Quaternion::Slerp(float t, const Quaternion& start, const Quaternion& end)
  {
    float cs = start.Dot(end);
    float slerp_epsilon = 0.00001f;
    float coeff0, coeff1;

    bool negative = (cs < 0.0f);

    if (negative)
      cs = -cs;
    float angle = acos(cs);

    if ((1.0f - cs) > slerp_epsilon)
    {
      float invSin = 1.0f / sin(angle);
      float tAngle = t * angle;
      coeff0 = sin(angle - tAngle) * invSin;
      coeff1 = sin(tAngle) * invSin;
    }
    else
    {
      coeff0 = 1.0f - t;
      coeff1 = t;
    }

    if (negative)
      coeff1 = -coeff1;

    v[0] = coeff0 * start.v[0] + coeff1 * end.v[0];
    v[1] = coeff0 * start.v[1] + coeff1 * end.v[1];
    v[2] = coeff0 * start.v[2] + coeff1 * end.v[2];
    v[3] = coeff0 * start.v[3] + coeff1 * end.v[3];

    return (*this);
  }

  Quaternion& Quaternion::Squerp(float t, const Quaternion &q0, const Quaternion &a0, const Quaternion &a1, const Quaternion &q1)
  {
    float slerpT = 2.0f * t * (1.0f - t);
    Quaternion slerpP = Slerp(t, q0, q1);
    Quaternion slerpQ = Slerp(t, a0, a1);
    return Slerp(slerpT, slerpP, slerpQ);
  }

  Maths::Quaternion Quaternion::VectorToVector(const Vector& vec1, const Vector& vec2)
  {
    float mod = sqrt(vec1.SquaredLength() * vec2.SquaredLength());
    float angle = vec1.Dot(vec2) / mod;
    // Vectors Parallel
    if (angle >= 1.0f - EPSILON)
      return Quaternion::IDENTITY;
    // Vectors Opposite and Parallel 
    else if (angle <= -1.0f + EPSILON)
      return Quaternion(Vector::UNIT_Y, PI);
    else if (AbsVal(angle + 1.0f) < EPSILON)
    {
      Vector other = AbsVal(vec1.Dot(Vector::UNIT_X)) < 1.0 ? Vector::UNIT_X : Vector::UNIT_Y;
      Vector cross = vec1.Cross(other);
      cross.Normalize();
      return Quaternion(cross, PI);
    }
    Vector cross = vec1.Cross(vec2);
    Quaternion ret(vec1.Dot(vec2) + mod, cross.x, cross.y, cross.z);
    ret.Normalize();
    return ret;
  }


}