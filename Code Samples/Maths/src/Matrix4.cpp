#include "Matrix4.h"
#include "Vector.h"
#include "Geometry/Point.h"

#include <cassert>

namespace Maths
{

	const Matrix4 Matrix4::ZERO{ true };
	const Matrix4 Matrix4::IDENTITY{ false };
	const Matrix4 Matrix4::BIAS{ 0.5f, 0.0f, 0.0f, 0.0f,
							  0.0f, 0.5f, 0.0f, 0.0f,
							  0.0f, 0.0f, 0.5f, 0.0f,
							  0.5f, 0.5f, 0.5f, 1.0f };

	Matrix4::Matrix4(bool zero /*= true*/)
	{
		if (zero)
			MakeZero();
		else
			MakeIdentity();
	}

	Matrix4::Matrix4(float mm00, float mm01, float mm02, float mm03, float mm10, float mm11, float mm12, float mm13, float mm20, float mm21, float mm22, float mm23, float mm30, float mm31, float mm32, float mm33)
		: m00(mm00), m01(mm01), m02(mm02), m03(mm03),
		m10(mm10), m11(mm11), m12(mm12), m13(mm13),
		m20(mm20), m21(mm21), m22(mm22), m23(mm23),
		m30(mm30), m31(mm31), m32(mm32), m33(mm33)
	{
	}

	Matrix4::Matrix4(const Vector& u, const Vector& v)
		: m00(u.x* v.x), m01(u.x* v.y), m02(u.x* v.z), m03(0.0f),
		m10(u.y* v.x), m11(u.y* v.y), m12(u.y* v.z), m13(0.0f),
		m20(u.z* v.x), m21(u.z* v.y), m22(u.z* v.z), m23(0.0f),
		m30(0.0f), m31(0.0f), m32(0.0f), m33(0.0f)
	{
	}

	Matrix4::Matrix4(float pitch, float yaw, float roll, bool radians /*= true*/)
	{
		float sinYaw, cosYaw, sinPitch, cosPitch, sinRoll, cosRoll;
		if (radians)
		{
			GetSinCos(yaw, sinYaw, cosYaw);
			GetSinCos(pitch, sinPitch, cosPitch);
			GetSinCos(roll, sinRoll, cosRoll);
		}
		else
		{
			GetSinCos(yaw * DEG_TO_RAD, sinYaw, cosYaw);
			GetSinCos(pitch * DEG_TO_RAD, sinPitch, cosPitch);
			GetSinCos(roll * DEG_TO_RAD, sinRoll, cosRoll);
		}

		m00 = cosYaw * cosRoll;
		m01 = sinPitch * sinYaw * cosRoll - cosPitch * sinRoll;
		m02 = cosPitch * sinYaw * cosRoll + sinPitch * sinRoll;
		m03 = 0.0f;

		m10 = cosYaw * sinRoll;
		m11 = sinPitch * sinYaw * sinRoll + cosPitch * cosRoll;
		m12 = cosPitch * sinYaw * sinRoll - sinPitch * cosRoll;
		m13 = 0.0f;

		m20 = -sinYaw;
		m21 = sinPitch * cosYaw;
		m22 = cosPitch * cosYaw;
		m23 = 0.0f;

		m30 = 0.0f;
		m31 = 0.0f;
		m32 = 0.0f;
		m33 = 1.0f;
	}

	Matrix4::Matrix4(float mat[16])
		: m00(mat[0]), m01(mat[1]), m02(mat[2]), m03(mat[3]),
		m10(mat[4]), m11(mat[5]), m12(mat[6]), m13(mat[7]),
		m20(mat[8]), m21(mat[9]), m22(mat[10]), m23(mat[11]),
		m30(mat[12]), m31(mat[13]), m32(mat[14]), m33(mat[15])
	{
	}

	Matrix4::~Matrix4()
	{
	}

	Matrix4& Matrix4::MakeZero(void)
	{
		std::memset(v, 0, sizeof(float) * 16);
		return *this;
	}

	Matrix4& Matrix4::MakeIdentity(void)
	{
		MakeZero();
		m00 = m11 = m22 = m33 = 1.0f;
		return *this;
	}

	void Matrix4::SetRow(int row, const Vector& vec)
	{
		m[row][0] = vec.x;
		m[row][1] = vec.y;
		m[row][2] = vec.z;
		m[row][3] = vec.w;
	}

	void Matrix4::SetCol(int col, const Vector& vec)
	{
		m[0][col] = vec.x;
		m[1][col] = vec.y;
		m[2][col] = vec.z;
		m[3][col] = vec.w;
	}

	Maths::Vector Matrix4::GetRow(int row) const
	{
		return Vector(m[row][0],
			m[row][1],
			m[row][2],
			m[row][3]);
	}

	Maths::Vector Matrix4::GetCol(int col) const
	{
		return Vector(m[0][col],
			m[1][col],
			m[2][col],
			m[3][col]);
	}

	Matrix4& Matrix4::MakeSkew(const Vector& v)
	{
		MakeZero();
		m01 = -v.z;
		m02 = v.y;
		m10 = v.z;
		m12 = -v.x;
		m20 = -v.y;
		m21 = v.x;
		return (*this);
	}

	Matrix4& Matrix4::MakeTranslation(const Point& v)
	{
		MakeIdentity();
		m03 = v.x;
		m13 = v.y;
		m23 = v.z;
		return (*this);
	}

	Matrix4& Matrix4::MakeTransposeTranslation(const Point& v)
	{
		MakeIdentity();
		m30 = v.x;
		m31 = v.y;
		m32 = v.z;
		return (*this);
	}

	Matrix4& Matrix4::MakeTranslationInverse(const Point& v)
	{
		MakeIdentity();
		m03 = -v.x;
		m13 = -v.y;
		m23 = -v.z;
		return (*this);
	}

	Matrix4& Matrix4::MakeScale(const Vector& v)
	{
		MakeZero();
		m00 = v.x;
		m11 = v.y;
		m22 = v.z;
		m33 = 1.0f;
		return (*this);
	}

	Matrix4& Matrix4::MakeScaleInverse(const Vector& v)
	{
		MakeZero();
		m00 = 1.0f / v.x;
		m11 = 1.0f / v.y;
		m22 = 1.0f / v.z;
		m33 = 1.0f;
		return (*this);
	}

	Matrix4& Matrix4::MakeObliqueProjection(const Vector& norm, const Vector& origin, const Vector& dir)
	{
		MakeZero();
		float dotND = norm.Dot(dir);
		float dotNO = norm.Dot(origin);
		m00 = dir.v[0] * norm.v[0] - dotND;
		m01 = dir.v[0] * norm.v[1];
		m02 = dir.v[0] * norm.v[2];
		m03 = -dotNO * dir.v[0];
		m10 = dir.v[1] * norm.v[0];
		m11 = dir.v[1] * norm.v[1] - dotND;
		m12 = dir.v[1] * norm.v[2];
		m13 = -dotNO * dir.v[1];
		m20 = dir.v[2] * norm.v[0];
		m21 = dir.v[2] * norm.v[1];
		m22 = dir.v[2] * norm.v[2] - dotND;
		m23 = -dotNO * dir.v[2];
		m33 = -dotND;
		return (*this);
	}

	Matrix4& Matrix4::MakePerspectiveProjection(const Vector& norm, const Vector& origin, const Vector& eye)
	{
		float dotND = norm.Dot(eye - origin);
		m00 = dotND - eye.v[0] * norm.v[0];
		m01 = -eye.v[0] * norm.v[1];
		m02 = -eye.v[0] * norm.v[2];
		m03 = -(m00 * eye.v[0] + m01 * eye.v[1] + m02 * eye.v[2]);
		m10 = -eye.v[1] * norm.v[0];
		m11 = dotND - eye.v[1] * norm.v[1];
		m12 = -eye.v[1] * norm.v[2];
		m13 = -(m10 * eye.v[0] + m11 * eye.v[1] + m12 * eye.v[2]);
		m20 = -eye.v[2] * norm.v[0];
		m21 = -eye.v[2] * norm.v[1];
		m22 = dotND - eye.v[2] * norm.v[2];
		m23 = -(m20 * eye.v[0] + m21 * eye.v[1] + m22 * eye.v[2]);
		m30 = -norm.v[0];
		m31 = -norm.v[1];
		m32 = -norm.v[2];
		m33 = norm.Dot(eye);
		return (*this);
	}

	Matrix4& Matrix4::MakePerspectiveProjection(float fov, float aspect, float near, float far)
	{
		MakeZero();
		float f = 1.0f / Tan((fov / 360.0f) * PI);

		m00 = f / aspect;
		m11 = f;
		m22 = (far + near) / (near - far);
		m23 = -1.0f;
		m32 = (2.0f * far * near) / (near - far);

		return *this;
	}

	Matrix4& Matrix4::MakePerspectiveProjection(float left, float right, float top, float bottom, float near, float far)
	{
		MakeZero();
		m00 = 2.0f * near / (right - left);
		m20 = (right + left) / (right - left);

		m11 = 2.0f * near / (top - bottom);
		m21 = (top + bottom) / (top - bottom);

		m22 = -(far + near) / (far - near);
		m32 = -2.0f * far * near / (far - near);

		m23 = -1.0f;
		return *this;
	}

	Matrix4& Matrix4::MakeOrthographicProjection(float width, float height, float near, float far)
	{
		m[0][0] = 2.0f / width;
		m[0][1] = 0;
		m[0][2] = 0;
		m[0][3] = 0;

		m[1][0] = 0;
		m[1][1] = 2.0f / height;
		m[1][2] = 0;
		m[1][3] = 0;

		m[2][0] = 0;
		m[2][1] = 0;
		m[2][2] = -1 / (far - near);
		m[2][3] = 0;

		m[3][0] = -width / width;
		m[3][1] = -height / height;
		m[3][2] = -near / (far - near);
		m[3][3] = 1;


		return *this;
	}

	Matrix4& Matrix4::MakeLook(const Point& eye, const Point& look, const Vector& up)
	{
		Vector f(look - eye);
		f.Normalize();
		Vector u(up);
		u.Normalize();
		Vector s(f.Cross(u));
		s.Normalize();

		u = s.Cross(f);

		MakeIdentity();
		m[0][0] = s.x;
		m[1][0] = s.y;
		m[2][0] = s.z;
		m[0][1] = u.x;
		m[1][1] = u.y;
		m[2][1] = u.z;
		m[0][2] = -f.x;
		m[1][2] = -f.y;
		m[2][2] = -f.z;
		m[3][0] = -s.DotPoint(eye);
		m[3][1] = -u.DotPoint(eye);
		m[3][2] = f.DotPoint(eye);
		return *this;
	}

	Matrix4& Matrix4::MakeLookAt(const Point& eye, const Point& look, const Vector& up)
	{
		Vector Z(eye - look);
		Z.Normalize();
		Vector X(up.Cross(Z));
		X.Normalize();
		Vector Y(Z.Cross(X));
		Y.Normalize();

		m00 = X.x;
		m01 = Y.x;
		m02 = Z.x;
		m03 = 0.0f;

		m10 = X.y;
		m11 = Y.y;
		m12 = Z.y;
		m13 = 0.0f;

		m20 = X.z;
		m21 = Y.z;
		m22 = Z.z;
		m23 = 0.0f;

		m30 = -eye.x * X.x + -eye.y * X.y + -eye.z * X.z;
		m31 = -eye.x * Y.x + -eye.y * Y.y + -eye.z * Y.z;
		m32 = -eye.x * Z.x + -eye.y * Z.y + -eye.z * Z.z;
		m33 = 1.0f;

		return *this;
	}

	Matrix4& Matrix4::MakeReflection(const Vector& norm, const Vector& origin)
	{
		MakeZero();
		float twoDotNO = 2.0f * (norm.Dot(origin));
		m00 = 1.0f - 2.0f * norm.v[0] * norm.v[0];
		m01 = -2.0f * norm.v[0] * norm.v[1];
		m02 = -2.0f * norm.v[0] * norm.v[2];
		m03 = twoDotNO * norm.v[0];
		m10 = -2.0f * norm.v[1] * norm.v[0];
		m11 = 1.0f - 2.0f * norm.v[1] * norm.v[1];
		m12 = -2.0f * norm.v[1] * norm.v[2];
		m13 = twoDotNO * norm.v[1];
		m20 = -2.0f * norm.v[2] * norm.v[0];
		m21 = -2.0f * norm.v[2] * norm.v[1];
		m22 = 1.0f - 2.0f * norm.v[2] * norm.v[2];
		m23 = twoDotNO * norm.v[2];
		m33 = 1.0f;
		return (*this);
	}

	Matrix4& Matrix4::FromAxisAngle(const Vector& axis, float angle)
	{
		float sina, cosa;
		GetSinCos(angle, sina, cosa);

		Matrix4 cosScale(false),
			sinScale(false),
			negCosScale,
			outerProduct(axis, axis),
			skew,
			I(false);

		cosScale.m00 = cosScale.m11 = cosScale.m22 = cosa;
		sinScale.m00 = sinScale.m11 = sinScale.m22 = sina;

		negCosScale = I - cosScale;

		skew.MakeSkew(axis);

		(*this) = cosScale + negCosScale * outerProduct + sinScale * skew;
		return (*this);
	}

	Matrix4& Matrix4::FromTranslatedAxisAngle(const Point& position, const Vector& axis, float angle)
	{
		Matrix4 trans, rot, invTrans;

		trans.MakeTranslation(position);
		rot.FromAxisAngle(axis, angle);
		invTrans.MakeTranslationInverse(position);

		(*this) = trans * rot * invTrans;
		return (*this);
	}

	Point Matrix4::ToTranslation(void) const
	{
		return Point(m30, m31, m32);
	}

	Point Matrix4::ToTransposeTranslation(void) const
	{
		return Point(m03, m13, m23);
	}

	Matrix4 Matrix4::ToRotation(void) const
	{
		Matrix4 m = *this;
		Vector scale = ToScale();
		m.SetCol(0, m.GetCol(0) /= scale.x);
		m.SetCol(1, m.GetCol(1) /= scale.x);
		m.SetCol(2, m.GetCol(2) /= scale.x);
		m.SetRow(3, Vector::UNIT_W);
		return m;
	}

	Vector Matrix4::ToScale(void) const
	{
		return Vector(sqrtf(m00 * m00 + m01 * m01 + m02 * m02),
			sqrtf(m10 * m10 + m11 * m11 + m12 * m12),
			sqrtf(m20 * m20 + m21 * m21 + m22 * m22));
	}

	Vector Matrix4::GetEulerAngles(bool asRadians /*= true*/) const
	{
		float yaw, pitch, roll;

		pitch = atan2(m21, m22);
		float c2 = sqrt(m00 * m00 + m10 * m10);
		yaw = atan2(-m20, c2);
		float s1, c1;
		GetSinCos(pitch, s1, c1);
		roll = atan2(s1 * m02 - c1 * m01, c1 * m11 - s1 * m12);

		if (asRadians)
			return Vector(pitch, yaw, roll);
		else
			return Vector(pitch * RAD_TO_DEG, yaw * RAD_TO_DEG, roll * RAD_TO_DEG);
	}

	Matrix4 Matrix4::Inverse(float eps /*= EPSILON*/) const
	{
		float a0 = m00 * m11 - m01 * m10;
		float a1 = m00 * m12 - m02 * m10;
		float a2 = m00 * m13 - m03 * m10;
		float a3 = m01 * m12 - m02 * m11;
		float a4 = m01 * m13 - m03 * m11;
		float a5 = m02 * m13 - m03 * m12;
		float b0 = m20 * m31 - m21 * m30;
		float b1 = m20 * m32 - m22 * m30;
		float b2 = m20 * m33 - m23 * m30;
		float b3 = m21 * m32 - m22 * m31;
		float b4 = m21 * m33 - m23 * m31;
		float b5 = m22 * m33 - m23 * m32;

		float det = a0 * b5 - a1 * b4 + a2 * b3 + a3 * b2 - a4 * b1 + a5 * b0;
		if (AbsVal(det) > eps)
		{
			float invDet = 1.0f / det;
			return Matrix4((m11 * b5 - m12 * b4 + m13 * b3) * invDet,
				(-m01 * b5 + m02 * b4 - m03 * b3) * invDet,
				(m31 * a5 - m32 * a4 + m33 * a3) * invDet,
				(-m21 * a5 + m22 * a4 - m23 * a3) * invDet,
				(-m10 * b5 + m12 * b2 - m13 * b1) * invDet,
				(m00 * b5 - m02 * b2 + m03 * b1) * invDet,
				(-m30 * a5 + m32 * a2 - m33 * a1) * invDet,
				(m20 * a5 - m22 * a2 + m23 * a1) * invDet,
				(m10 * b4 - m11 * b2 + m13 * b0) * invDet,
				(-m00 * b4 + m01 * b2 - m03 * b0) * invDet,
				(m30 * a4 - m31 * a2 + m33 * a0) * invDet,
				(-m20 * a4 + m21 * a2 - m23 * a0) * invDet,
				(-m10 * b3 + m11 * b1 - m12 * b0) * invDet,
				(m00 * b3 - m01 * b1 + m02 * b0) * invDet,
				(-m30 * a3 + m31 * a1 - m32 * a0) * invDet,
				(m20 * a3 - m21 * a1 + m22 * a0) * invDet);
		}

		return ZERO;
	}

	Matrix4 Matrix4::Adjoint(void) const
	{
		float a0 = m00 * m11 - m01 * m10;
		float a1 = m00 * m12 - m02 * m10;
		float a2 = m00 * m13 - m03 * m10;
		float a3 = m01 * m12 - m02 * m11;
		float a4 = m01 * m13 - m03 * m11;
		float a5 = m02 * m13 - m03 * m12;
		float b0 = m20 * m31 - m21 * m30;
		float b1 = m20 * m32 - m22 * m30;
		float b2 = m20 * m33 - m23 * m30;
		float b3 = m21 * m32 - m22 * m31;
		float b4 = m21 * m33 - m23 * m31;
		float b5 = m22 * m33 - m23 * m32;

		return Matrix4(m11 * b5 - m12 * b4 + m13 * b3,
			-m01 * b5 + m02 * b4 - m03 * b3,
			m31 * a5 - m32 * a4 + m33 * a3,
			-m21 * a5 + m22 * a4 - m23 * a3,
			-m10 * b5 + m12 * b2 - m13 * b1,
			m00 * b5 - m02 * b2 + m03 * b1,
			-m30 * a5 + m32 * a2 - m33 * a1,
			m20 * a5 - m22 * a2 + m23 * a1,
			m10 * b4 - m11 * b2 + m13 * b0,
			-m00 * b4 + m01 * b2 - m03 * b0,
			m30 * a4 - m31 * a2 + m33 * a0,
			-m20 * a4 + m21 * a2 - m23 * a0,
			-m10 * b3 + m11 * b1 - m12 * b0,
			m00 * b3 - m01 * b1 + m02 * b0,
			-m30 * a3 + m31 * a1 - m32 * a0,
			m20 * a3 - m21 * a1 + m22 * a0);
	}

	float Matrix4::Determinant(void) const
	{
		return ((m00 * m11 - m01 * m10) * (m22 * m33 - m23 * m32)) -
			((m00 * m12 - m02 * m10) * (m21 * m33 - m23 * m31)) +
			((m00 * m13 - m03 * m10) * (m21 * m32 - m22 * m31)) +
			((m01 * m12 - m02 * m11) * (m20 * m33 - m23 * m30)) -
			((m01 * m13 - m03 * m11) * (m20 * m32 - m22 * m30)) +
			((m02 * m13 - m03 * m12) * (m20 * m31 - m21 * m30));
	}

	Matrix4 Matrix4::Transpose(void) const
	{
		return Matrix4(m00, m10, m20, m30,
			m01, m11, m21, m31,
			m02, m12, m22, m32,
			m03, m13, m23, m33);
	}

	Matrix4 Matrix4::TransposeTimes(const Matrix4& rhs) const
	{
		return Matrix4(m00 * rhs.m00 + m10 * rhs.m10 + m20 * rhs.m20 + m30 * rhs.m30,
			m00 * rhs.m01 + m10 * rhs.m11 + m20 * rhs.m21 + m30 * rhs.m31,
			m00 * rhs.m02 + m10 * rhs.m12 + m20 * rhs.m22 + m30 * rhs.m32,
			m00 * rhs.m03 + m10 * rhs.m13 + m20 * rhs.m23 + m30 * rhs.m33,
			m01 * rhs.m00 + m11 * rhs.m10 + m21 * rhs.m20 + m31 * rhs.m30,
			m01 * rhs.m01 + m11 * rhs.m11 + m21 * rhs.m21 + m31 * rhs.m31,
			m01 * rhs.m02 + m11 * rhs.m12 + m21 * rhs.m22 + m31 * rhs.m32,
			m01 * rhs.m03 + m11 * rhs.m13 + m21 * rhs.m23 + m31 * rhs.m33,
			m02 * rhs.m00 + m12 * rhs.m10 + m22 * rhs.m20 + m32 * rhs.m30,
			m02 * rhs.m01 + m12 * rhs.m11 + m22 * rhs.m21 + m32 * rhs.m31,
			m02 * rhs.m02 + m12 * rhs.m12 + m22 * rhs.m22 + m32 * rhs.m32,
			m02 * rhs.m03 + m12 * rhs.m13 + m22 * rhs.m23 + m32 * rhs.m33,
			m03 * rhs.m00 + m13 * rhs.m10 + m23 * rhs.m20 + m33 * rhs.m30,
			m03 * rhs.m01 + m13 * rhs.m11 + m23 * rhs.m21 + m33 * rhs.m31,
			m03 * rhs.m02 + m13 * rhs.m12 + m23 * rhs.m22 + m33 * rhs.m32,
			m03 * rhs.m03 + m13 * rhs.m13 + m23 * rhs.m23 + m33 * rhs.m33);
	}

	Matrix4 Matrix4::TimesTranspose(const Matrix4& rhs) const
	{
		return Matrix4(m00 * rhs.m00 + m01 * rhs.m01 + m02 * rhs.m02 + m03 * rhs.m03,
			m00 * rhs.m10 + m01 * rhs.m11 + m02 * rhs.m12 + m03 * rhs.m13,
			m00 * rhs.m20 + m01 * rhs.m21 + m02 * rhs.m22 + m03 * rhs.m23,
			m00 * rhs.m30 + m01 * rhs.m31 + m02 * rhs.m32 + m03 * rhs.m33,
			m10 * rhs.m00 + m11 * rhs.m01 + m12 * rhs.m02 + m13 * rhs.m03,
			m10 * rhs.m10 + m11 * rhs.m11 + m12 * rhs.m12 + m13 * rhs.m13,
			m10 * rhs.m20 + m11 * rhs.m21 + m12 * rhs.m22 + m13 * rhs.m23,
			m10 * rhs.m30 + m11 * rhs.m31 + m12 * rhs.m32 + m13 * rhs.m33,
			m20 * rhs.m00 + m21 * rhs.m01 + m22 * rhs.m02 + m23 * rhs.m03,
			m20 * rhs.m10 + m21 * rhs.m11 + m22 * rhs.m12 + m23 * rhs.m13,
			m20 * rhs.m20 + m21 * rhs.m21 + m22 * rhs.m22 + m23 * rhs.m23,
			m20 * rhs.m30 + m21 * rhs.m31 + m22 * rhs.m32 + m23 * rhs.m33,
			m30 * rhs.m00 + m31 * rhs.m01 + m32 * rhs.m02 + m33 * rhs.m03,
			m30 * rhs.m10 + m31 * rhs.m11 + m32 * rhs.m12 + m33 * rhs.m13,
			m30 * rhs.m20 + m31 * rhs.m21 + m32 * rhs.m22 + m33 * rhs.m23,
			m30 * rhs.m30 + m31 * rhs.m31 + m32 * rhs.m32 + m33 * rhs.m33);
	}

	Matrix4 Matrix4::TransposeTimesTranspose(const Matrix4& rhs) const
	{
		return Matrix4(m00 * rhs.m00 + m10 * rhs.m01 + m20 * rhs.m02 + m30 * rhs.m03,
			m00 * rhs.m10 + m10 * rhs.m11 + m20 * rhs.m12 + m30 * rhs.m13,
			m00 * rhs.m20 + m10 * rhs.m21 + m20 * rhs.m22 + m30 * rhs.m23,
			m00 * rhs.m30 + m10 * rhs.m31 + m20 * rhs.m32 + m30 * rhs.m33,
			m01 * rhs.m00 + m11 * rhs.m01 + m21 * rhs.m02 + m31 * rhs.m03,
			m01 * rhs.m10 + m11 * rhs.m11 + m21 * rhs.m12 + m31 * rhs.m13,
			m01 * rhs.m20 + m11 * rhs.m21 + m21 * rhs.m22 + m31 * rhs.m23,
			m01 * rhs.m30 + m11 * rhs.m31 + m21 * rhs.m32 + m31 * rhs.m33,
			m02 * rhs.m00 + m12 * rhs.m01 + m22 * rhs.m02 + m32 * rhs.m03,
			m02 * rhs.m10 + m12 * rhs.m11 + m22 * rhs.m12 + m32 * rhs.m13,
			m02 * rhs.m20 + m12 * rhs.m21 + m22 * rhs.m22 + m32 * rhs.m23,
			m02 * rhs.m30 + m12 * rhs.m31 + m22 * rhs.m32 + m32 * rhs.m33,
			m03 * rhs.m00 + m13 * rhs.m01 + m23 * rhs.m02 + m33 * rhs.m03,
			m03 * rhs.m10 + m13 * rhs.m11 + m23 * rhs.m12 + m33 * rhs.m13,
			m03 * rhs.m20 + m13 * rhs.m21 + m23 * rhs.m22 + m33 * rhs.m23,
			m03 * rhs.m30 + m13 * rhs.m31 + m23 * rhs.m32 + m33 * rhs.m33);
	}

	Point Matrix4::TransformInverse(const Point& point) const
	{
		Point temp(point.x - m03, point.y - m13, point.z - m23);
		return Point(temp.x * m00 + temp.y * m10 + temp.z * m20,
			temp.x * m01 + temp.y * m11 + temp.z * m21,
			temp.x * m02 + temp.y * m12 + temp.z * m22);
	}

	Maths::Vector Matrix4::TransformInverse(const Vector& vec) const
	{
		return Vector(vec.x * m00 + vec.y * m10 + vec.z * m20,
			vec.x * m01 + vec.y * m11 + vec.z * m21,
			vec.x * m02 + vec.y * m12 + vec.z * m22);
	}

	Maths::Vector Matrix4::operator*(const Vector& rhs) const
	{
		return Vector(rhs.x * m00 + rhs.y * m01 + rhs.z * m02 + rhs.w * m03,
			rhs.x * m10 + rhs.y * m11 + rhs.z * m12 + rhs.w * m13,
			rhs.x * m20 + rhs.y * m21 + rhs.z * m22 + rhs.w * m23,
			rhs.x * m30 + rhs.y * m31 + rhs.z * m32 + rhs.w * m33);
	}

	Point Matrix4::operator*(const Point& rhs) const
	{
		return Point(rhs.x * m00 + rhs.y * m01 + rhs.z * m02 + rhs.w * m03,
			rhs.x * m10 + rhs.y * m11 + rhs.z * m12 + rhs.w * m13,
			rhs.x * m20 + rhs.y * m21 + rhs.z * m22 + rhs.w * m23,
			rhs.x * m30 + rhs.y * m31 + rhs.z * m32 + rhs.w * m33);
	}

	Matrix4 Matrix4::operator*(const Matrix4& rhs) const
	{
		return Matrix4(m00 * rhs.m00 + m01 * rhs.m10 + m02 * rhs.m20 + m03 * rhs.m30,
			m00 * rhs.m01 + m01 * rhs.m11 + m02 * rhs.m21 + m03 * rhs.m31,
			m00 * rhs.m02 + m01 * rhs.m12 + m02 * rhs.m22 + m03 * rhs.m32,
			m00 * rhs.m03 + m01 * rhs.m13 + m02 * rhs.m23 + m03 * rhs.m33,
			m10 * rhs.m00 + m11 * rhs.m10 + m12 * rhs.m20 + m13 * rhs.m30,
			m10 * rhs.m01 + m11 * rhs.m11 + m12 * rhs.m21 + m13 * rhs.m31,
			m10 * rhs.m02 + m11 * rhs.m12 + m12 * rhs.m22 + m13 * rhs.m32,
			m10 * rhs.m03 + m11 * rhs.m13 + m12 * rhs.m23 + m13 * rhs.m33,
			m20 * rhs.m00 + m21 * rhs.m10 + m22 * rhs.m20 + m23 * rhs.m30,
			m20 * rhs.m01 + m21 * rhs.m11 + m22 * rhs.m21 + m23 * rhs.m31,
			m20 * rhs.m02 + m21 * rhs.m12 + m22 * rhs.m22 + m23 * rhs.m32,
			m20 * rhs.m03 + m21 * rhs.m13 + m22 * rhs.m23 + m23 * rhs.m33,
			m30 * rhs.m00 + m31 * rhs.m10 + m32 * rhs.m20 + m33 * rhs.m30,
			m30 * rhs.m01 + m31 * rhs.m11 + m32 * rhs.m21 + m33 * rhs.m31,
			m30 * rhs.m02 + m31 * rhs.m12 + m32 * rhs.m22 + m33 * rhs.m32,
			m30 * rhs.m03 + m31 * rhs.m13 + m32 * rhs.m23 + m33 * rhs.m33);
	}

	Matrix4 Matrix4::operator*(float mult) const
	{
		return Matrix4(m00 * mult, m01 * mult, m02 * mult, m03 * mult,
			m10 * mult, m11 * mult, m12 * mult, m13 * mult,
			m20 * mult, m21 * mult, m22 * mult, m23 * mult,
			m30 * mult, m31 * mult, m32 * mult, m33 * mult);
	}

	Matrix4 Matrix4::operator/(float div) const
	{
		assert(div != 0);
		div = 1.0f / div;
		return Matrix4(m00 * div, m01 * div, m02 * div, m03 * div,
			m10 * div, m11 * div, m12 * div, m13 * div,
			m20 * div, m21 * div, m22 * div, m23 * div,
			m30 * div, m31 * div, m32 * div, m33 * div);

	}

	Matrix4 Matrix4::operator+(const Matrix4& rhs) const
	{
		return Matrix4(m00 + rhs.m00, m01 + rhs.m01, m02 + rhs.m02, m03 + rhs.m03,
			m10 + rhs.m10, m11 + rhs.m11, m12 + rhs.m12, m13 + rhs.m13,
			m20 + rhs.m20, m21 + rhs.m21, m22 + rhs.m22, m23 + rhs.m23,
			m30 + rhs.m30, m31 + rhs.m31, m32 + rhs.m32, m33 + rhs.m33);
	}

	Matrix4 Matrix4::operator-(const Matrix4& rhs) const
	{
		return Matrix4(m00 - rhs.m00, m01 - rhs.m01, m02 - rhs.m02, m03 - rhs.m03,
			m10 - rhs.m10, m11 - rhs.m11, m12 - rhs.m12, m13 - rhs.m13,
			m20 - rhs.m20, m21 - rhs.m21, m22 - rhs.m22, m23 - rhs.m23,
			m30 - rhs.m30, m31 - rhs.m31, m32 - rhs.m32, m33 - rhs.m33);
	}


	Matrix4& Matrix4::operator+=(const Matrix4& rhs)
	{
		m00 += rhs.m00;
		m01 += rhs.m01;
		m02 += rhs.m02;
		m03 += rhs.m03;
		m10 += rhs.m10;
		m11 += rhs.m11;
		m12 += rhs.m12;
		m13 += rhs.m13;
		m20 += rhs.m20;
		m21 += rhs.m21;
		m22 += rhs.m22;
		m23 += rhs.m23;
		m30 += rhs.m30;
		m31 += rhs.m31;
		m32 += rhs.m32;
		m33 += rhs.m33;
		return (*this);
	}

	Matrix4& Matrix4::operator-=(const Matrix4& rhs)
	{
		m00 -= rhs.m00;
		m01 -= rhs.m01;
		m02 -= rhs.m02;
		m03 -= rhs.m03;
		m10 -= rhs.m10;
		m11 -= rhs.m11;
		m12 -= rhs.m12;
		m13 -= rhs.m13;
		m20 -= rhs.m20;
		m21 -= rhs.m21;
		m22 -= rhs.m22;
		m23 -= rhs.m23;
		m30 -= rhs.m30;
		m31 -= rhs.m31;
		m32 -= rhs.m32;
		m33 -= rhs.m33;
		return (*this);
	}

	Matrix4& Matrix4::operator*=(float mult)
	{
		m00 *= mult; m01 *= mult; m02 *= mult; m03 *= mult;
		m10 *= mult; m11 *= mult; m12 *= mult; m13 *= mult;
		m20 *= mult; m21 *= mult; m22 *= mult; m23 *= mult;
		m30 *= mult; m31 *= mult; m32 *= mult; m33 *= mult;
		return (*this);
	}
	Matrix4& Matrix4::operator/=(float div)
	{
		assert(div != 0);
		div = 1.0f / div;
		m00 *= div; m01 *= div; m02 *= div; m03 *= div;
		m10 *= div; m11 *= div; m12 *= div; m13 *= div;
		m20 *= div; m21 *= div; m22 *= div; m23 *= div;
		m30 *= div; m31 *= div; m32 *= div; m33 *= div;
		return (*this);
	}


	bool Matrix4::operator==(const Matrix4& rhs) const
	{
		if (AbsVal(m00 - rhs.m00) > EPSILON ||
			AbsVal(m01 - rhs.m01) > EPSILON ||
			AbsVal(m01 - rhs.m01) > EPSILON ||
			AbsVal(m02 - rhs.m02) > EPSILON ||
			AbsVal(m03 - rhs.m03) > EPSILON ||
			AbsVal(m10 - rhs.m10) > EPSILON ||
			AbsVal(m11 - rhs.m11) > EPSILON ||
			AbsVal(m12 - rhs.m12) > EPSILON ||
			AbsVal(m13 - rhs.m13) > EPSILON ||
			AbsVal(m20 - rhs.m20) > EPSILON ||
			AbsVal(m21 - rhs.m21) > EPSILON ||
			AbsVal(m22 - rhs.m22) > EPSILON ||
			AbsVal(m23 - rhs.m23) > EPSILON ||
			AbsVal(m30 - rhs.m30) > EPSILON ||
			AbsVal(m31 - rhs.m31) > EPSILON ||
			AbsVal(m32 - rhs.m32) > EPSILON ||
			AbsVal(m33 - rhs.m33) > EPSILON)
			return false;

		return true;
	}

	bool Matrix4::operator!=(const Matrix4& rhs) const
	{
		return !(*this == rhs);
	}

	bool Matrix4::operator<(const Matrix4& rhs) const
	{
		return memcmp(v, rhs.v, sizeof(float) * 16) < 0;
	}

	bool Matrix4::operator<=(const Matrix4& rhs) const
	{
		return memcmp(v, rhs.v, sizeof(float) * 16) <= 0;
	}

	bool Matrix4::operator>(const Matrix4& rhs) const
	{
		return memcmp(v, rhs.v, sizeof(float) * 16) > 0;
	}

	bool Matrix4::operator>=(const Matrix4& rhs) const
	{
		return memcmp(v, rhs.v, sizeof(float) * 16) >= 0;
	}

	std::ostream& operator<<(std::ostream& os, const Matrix4& mat)
	{
		os << "[" << mat.m00 << " " << mat.m01 << " " << mat.m02 << " " << mat.m03 << "]\n";
		os << "[" << mat.m10 << " " << mat.m11 << " " << mat.m12 << " " << mat.m13 << "]\n";
		os << "[" << mat.m20 << " " << mat.m21 << " " << mat.m22 << " " << mat.m23 << "]\n";
		os << "[" << mat.m30 << " " << mat.m31 << " " << mat.m32 << " " << mat.m33 << "]";
		return os;
	}





	//////////////////////////////////////////////////////////////////////////
	//                TESTS ONLY BELOW THIS POINT
	//////////////////////////////////////////////////////////////////////////
	static const float LOW = -10000.0f;
	static const float HIGH = 10000.0f;

	static bool TestMatrix4Vars(const Matrix4& m, float m00, float m01, float m02, float m03,
		float m10, float m11, float m12, float m13,
		float m20, float m21, float m22, float m23,
		float m30, float m31, float m32, float m33)
	{
		if (!Equal(m.m00, m00) || !Equal(m.m01, m01) || !Equal(m.m02, m02) || !Equal(m.m03, m03) ||
			!Equal(m.m10, m10) || !Equal(m.m11, m11) || !Equal(m.m12, m12) || !Equal(m.m13, m13) ||
			!Equal(m.m20, m20) || !Equal(m.m21, m21) || !Equal(m.m22, m22) || !Equal(m.m23, m23) ||
			!Equal(m.m30, m30) || !Equal(m.m31, m31) || !Equal(m.m32, m32) || !Equal(m.m33, m33))
			return false;

		return true;
	}

	static bool TestMatrix4Vars(const Matrix4& m, float mm[16])
	{
		if (!Equal(m.m00, mm[0]) || !Equal(m.m01, mm[1]) || !Equal(m.m02, mm[2]) || !Equal(m.m03, mm[3]) ||
			!Equal(m.m10, mm[4]) || !Equal(m.m11, mm[5]) || !Equal(m.m12, mm[6]) || !Equal(m.m13, mm[7]) ||
			!Equal(m.m20, mm[8]) || !Equal(m.m21, mm[9]) || !Equal(m.m22, mm[10]) || !Equal(m.m23, mm[11]) ||
			!Equal(m.m30, mm[12]) || !Equal(m.m31, mm[13]) || !Equal(m.m32, mm[14]) || !Equal(m.m33, mm[15]))
			return false;

		return true;
	}

	static bool TestVectorVars(const Vector& v, float x, float y, float z, float w)
	{
		if (AbsVal(v.x - x) > EPSILON || AbsVal(v.y - y) > EPSILON ||
			AbsVal(v.z - z) > EPSILON || AbsVal(v.w - w) > EPSILON)
			return false;

		return true;
	}

	static bool TestDefaultConstructor(void)
	{
		Matrix4 z(true);
		Matrix4 i(false);

		if (!TestMatrix4Vars(z, 0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f))
			return false;

		if (!TestMatrix4Vars(i, 1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f))
			return false;

		return true;
	}


	static bool TestNonDefaultConstructor(void)
	{
		float a = Random(LOW, HIGH), b = Random(LOW, HIGH), c = Random(LOW, HIGH), d = Random(LOW, HIGH),
			e = Random(LOW, HIGH), f = Random(LOW, HIGH), g = Random(LOW, HIGH), h = Random(LOW, HIGH),
			i = Random(LOW, HIGH), j = Random(LOW, HIGH), k = Random(LOW, HIGH), l = Random(LOW, HIGH),
			m = Random(LOW, HIGH), n = Random(LOW, HIGH), o = Random(LOW, HIGH), p = Random(LOW, HIGH);
		Matrix4 t(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p);

		if (!TestMatrix4Vars(t, a, b, c, d,
			e, f, g, h,
			i, j, k, l,
			m, n, o, p))
			return false;


		float test[16];
		for (unsigned i = 0; i < 16; ++i)
			test[i] = Random(LOW, HIGH);

		Matrix4 t2(test);
		if (!TestMatrix4Vars(t2, test))
			return false;

		return true;
	}

	static bool TestSetRow(void)
	{
		Matrix4 t(true);
		Vector v;

		v.x = Random(LOW, HIGH);
		v.y = Random(LOW, HIGH);
		v.z = Random(LOW, HIGH);
		v.w = Random(LOW, HIGH);

		t.SetRow(0, v);

		if (!TestMatrix4Vars(t, v.x, v.y, v.z, v.w,
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f))
			return false;

		t.MakeZero();
		t.SetRow(1, v);

		if (!TestMatrix4Vars(t, 0.0f, 0.0f, 0.0f, 0.0f,
			v.x, v.y, v.z, v.w,
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f))
			return false;

		t.MakeZero();
		t.SetRow(2, v);

		if (!TestMatrix4Vars(t, 0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,
			v.x, v.y, v.z, v.w,
			0.0f, 0.0f, 0.0f, 0.0f))
			return false;

		t.MakeZero();
		t.SetRow(3, v);

		if (!TestMatrix4Vars(t, 0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,
			v.x, v.y, v.z, v.w))
			return false;

		return true;
	}

	static bool TestSetCol(void)
	{
		Matrix4 t(true);
		Vector v;

		v.x = Random(LOW, HIGH);
		v.y = Random(LOW, HIGH);
		v.z = Random(LOW, HIGH);
		v.w = Random(LOW, HIGH);

		t.SetCol(0, v);

		if (!TestMatrix4Vars(t, v.x, 0.0f, 0.0f, 0.0f,
			v.y, 0.0f, 0.0f, 0.0f,
			v.z, 0.0f, 0.0f, 0.0f,
			v.w, 0.0f, 0.0f, 0.0f))
			return false;

		t.MakeZero();
		t.SetCol(1, v);

		if (!TestMatrix4Vars(t, 0.0f, v.x, 0.0f, 0.0f,
			0.0f, v.y, 0.0f, 0.0f,
			0.0f, v.z, 0.0f, 0.0f,
			0.0f, v.w, 0.0f, 0.0f))
			return false;

		t.MakeZero();
		t.SetCol(2, v);

		if (!TestMatrix4Vars(t, 0.0f, 0.0f, v.x, 0.0f,
			0.0f, 0.0f, v.y, 0.0f,
			0.0f, 0.0f, v.z, 0.0f,
			0.0f, 0.0f, v.w, 0.0f))
			return false;

		t.MakeZero();
		t.SetCol(3, v);

		if (!TestMatrix4Vars(t, 0.0f, 0.0f, 0.0f, v.x,
			0.0f, 0.0f, 0.0f, v.y,
			0.0f, 0.0f, 0.0f, v.z,
			0.0f, 0.0f, 0.0f, v.w))
			return false;

		return true;
	}

	static bool TestGetRow(void)
	{
		float test[16];
		for (unsigned i = 0; i < 16; ++i)
			test[i] = Random(LOW, HIGH);
		Matrix4 t(test);
		Vector v = t.GetRow(0);

		if (!TestVectorVars(v, test[0], test[1], test[2], test[3]))
			return false;

		v = t.GetRow(1);

		if (!TestVectorVars(v, test[4], test[5], test[6], test[7]))
			return false;

		v = t.GetRow(2);

		if (!TestVectorVars(v, test[8], test[9], test[10], test[11]))
			return false;

		v = t.GetRow(3);

		if (!TestVectorVars(v, test[12], test[13], test[14], test[15]))
			return false;

		return true;
	}

	static bool TestGetCol(void)
	{
		float a = Random(LOW, HIGH), b = Random(LOW, HIGH), c = Random(LOW, HIGH), d = Random(LOW, HIGH),
			e = Random(LOW, HIGH), f = Random(LOW, HIGH), g = Random(LOW, HIGH), h = Random(LOW, HIGH),
			i = Random(LOW, HIGH), j = Random(LOW, HIGH), k = Random(LOW, HIGH), l = Random(LOW, HIGH),
			m = Random(LOW, HIGH), n = Random(LOW, HIGH), o = Random(LOW, HIGH), p = Random(LOW, HIGH);
		Matrix4 t(a, b, c, d,
			e, f, g, h,
			i, j, k, l,
			m, n, o, p);
		Vector v = t.GetCol(0);

		if (!TestVectorVars(v, a, e, i, m))
			return false;

		v = t.GetCol(1);

		if (!TestVectorVars(v, b, f, j, n))
			return false;

		v = t.GetCol(2);

		if (!TestVectorVars(v, c, g, k, o))
			return false;

		v = t.GetCol(3);

		if (!TestVectorVars(v, d, h, l, p))
			return false;

		return true;
	}

	static bool TestInverse(void)
	{
		Matrix4 m(-4.0f, 0.0f, 0.0f, 0.0f,
			-1.0f, 2.0f, 0.0f, 0.0f,
			-4.0f, 4.0f, 4.0f, 0.0f,
			-1.0f, -9.0f, -1.0f, 1.0f);

		Matrix4 invM = m.Inverse();

		if (!TestMatrix4Vars(invM, -0.250f, 0, 0, 0,
			-0.125f, 0.5f, 0, 0,
			-0.125f, -0.5f, 0.25f, 0,
			-1.500f, 4.0f, 0.25f, 1.0f))
			return false;

		return true;
	}

	static bool TestAdjoint(void)
	{
		Matrix4 m(5.0f, -2.0f, 2.0f, 7.0f,
			1.0f, 0.0f, 0.0f, 3.0f,
			-3.0f, 1.0f, 5.0f, 0.0f,
			3.0f, -1.0f, -9.0f, 4.0f);

		Matrix4 adjM = m.Adjoint();

		if (!TestMatrix4Vars(adjM, -12.0f, 76.0f, -60.0f, -36.0f,
			-56.0f, 208.0f, -82.0f, -58.0f,
			4.0f, 4.0f, -2.0f, -10.0f,
			4.0f, 4.0f, 20.0f, 12.0f))
			return false;

		return true;
	}

	static bool TestDeterminant(void)
	{
		Matrix4 m(3.0f, 0.0f, 2.0f, -1.0f,
			1.0f, 2.0f, 0.0f, -2.0f,
			4.0f, 0.0f, 6.0f, -3.0f,
			5.0f, 0.0f, 2.0f, 0.0f);

		float det = m.Determinant();

		if (det != 20.0f)
			return false;

		return true;
	}


	static bool TestTranspose(void)
	{
		float a = Random(LOW, HIGH), b = Random(LOW, HIGH), c = Random(LOW, HIGH), d = Random(LOW, HIGH),
			e = Random(LOW, HIGH), f = Random(LOW, HIGH), g = Random(LOW, HIGH), h = Random(LOW, HIGH),
			i = Random(LOW, HIGH), j = Random(LOW, HIGH), k = Random(LOW, HIGH), l = Random(LOW, HIGH),
			m = Random(LOW, HIGH), n = Random(LOW, HIGH), o = Random(LOW, HIGH), p = Random(LOW, HIGH);
		Matrix4 t(a, b, c, d,
			e, f, g, h,
			i, j, k, l,
			m, n, o, p);
		Matrix4 transT = t.Transpose();

		return TestMatrix4Vars(transT, a, e, i, m,
			b, f, j, n,
			c, g, k, o,
			d, h, l, p);
	}

	static bool TestTransposeTimes(void)
	{
		float t0_vars[16];
		float t1_vars[16];
		for (unsigned i = 0; i < 16; ++i)
		{
			t0_vars[i] = Random(LOW, HIGH);
			t1_vars[i] = Random(LOW, HIGH);
		}
		Matrix4 t0(t0_vars);
		Matrix4 t1(t1_vars);
		Matrix4 t2 = t0.TransposeTimes(t1);
		Matrix4 t3 = t0.Transpose() * t1;

		return (t2 == t3);
	}

	static bool TestTimesTranspose(void)
	{
		float t0_vars[16];
		float t1_vars[16];
		for (unsigned i = 0; i < 16; ++i)
		{
			t0_vars[i] = Random(LOW, HIGH);
			t1_vars[i] = Random(LOW, HIGH);
		}
		Matrix4 t0(t0_vars);
		Matrix4 t1(t1_vars);
		Matrix4 t2 = t0.TimesTranspose(t1);
		Matrix4 t3 = t0 * t1.Transpose();

		return (t2 == t3);
	}

	static bool TestTransposeTimesTranspose(void)
	{
		float t0_vars[16];
		float t1_vars[16];
		for (unsigned i = 0; i < 16; ++i)
		{
			t0_vars[i] = Random(LOW, HIGH);
			t1_vars[i] = Random(LOW, HIGH);
		}
		Matrix4 t0(t0_vars);
		Matrix4 t1(t1_vars);
		Matrix4 t2 = t0.TransposeTimesTranspose(t1);
		Matrix4 t3 = t0.Transpose() * t1.Transpose();

		return (t2 == t3);
	}


	static bool TestVectorMultiplication(void)
	{
		float test[16];
		for (unsigned i = 0; i < 16; ++i)
			test[i] = Random(LOW, HIGH);

		Matrix4 t0(test);
		Vector t1(Random(LOW, HIGH),
			Random(LOW, HIGH),
			Random(LOW, HIGH),
			Random(LOW, HIGH));

		Vector r0 = t0.GetRow(0);
		Vector r1 = t0.GetRow(1);
		Vector r2 = t0.GetRow(2);
		Vector r3 = t0.GetRow(3);

		Vector t2 = t0 * t1;

		if (!TestVectorVars(t2, r0.Dot(t1),
			r1.Dot(t1),
			r2.Dot(t1),
			r3.Dot(t1)))
			return false;

		return true;
	}

	static bool TestPointMultiplication(void)
	{

		float test[16];
		for (unsigned i = 0; i < 16; ++i)
			test[i] = Random(LOW, HIGH);
		Matrix4 t0(test);
		Point t1(Random(LOW, HIGH),
			Random(LOW, HIGH),
			Random(LOW, HIGH),
			Random(LOW, HIGH));

		Vector r0 = t0.GetRow(0);
		Vector r1 = t0.GetRow(1);
		Vector r2 = t0.GetRow(2);
		Vector r3 = t0.GetRow(3);

		Point t2 = t0 * t1;

		float x = r0.DotPoint(t1);
		float y = r1.DotPoint(t1);
		float z = r2.DotPoint(t1);
		float w = r3.DotPoint(t1);

		Vector t3(x, y, z, w);

		if (!TestVectorVars(t3, t2.x, t2.y, t2.z, t2.w))
			return false;

		return true;
	}

	static bool TestMatrix4Multiplication(void)
	{
		float v0[16];
		float v1[16];
		for (unsigned i = 0; i < 16; ++i)
		{
			v0[i] = Random(LOW, HIGH);
			v1[i] = Random(LOW, HIGH);
		}

		Matrix4 t0(v0);
		Matrix4 t1(v1);

		Vector t0r0 = t0.GetRow(0);
		Vector t0r1 = t0.GetRow(1);
		Vector t0r2 = t0.GetRow(2);
		Vector t0r3 = t0.GetRow(3);

		Vector t1c0 = t1.GetCol(0);
		Vector t1c1 = t1.GetCol(1);
		Vector t1c2 = t1.GetCol(2);
		Vector t1c3 = t1.GetCol(3);

		Matrix4 t2 = t0 * t1;

		float aa = t0r0.Dot(t1c0), bb = t0r0.Dot(t1c1), cc = t0r0.Dot(t1c2), dd = t0r0.Dot(t1c3),
			ee = t0r1.Dot(t1c0), ff = t0r1.Dot(t1c1), gg = t0r1.Dot(t1c2), hh = t0r1.Dot(t1c3),
			ii = t0r2.Dot(t1c0), jj = t0r2.Dot(t1c1), kk = t0r2.Dot(t1c2), ll = t0r2.Dot(t1c3),
			mm = t0r3.Dot(t1c0), nn = t0r3.Dot(t1c1), oo = t0r3.Dot(t1c2), pp = t0r3.Dot(t1c3);

		if (!TestMatrix4Vars(t2, aa, bb, cc, dd,
			ee, ff, gg, hh,
			ii, jj, kk, ll,
			mm, nn, oo, pp))
			return false;

		return true;
	}

	static bool TestMatrix4Addition(void)
	{
		float v0[16];
		float v1[16];
		for (unsigned i = 0; i < 16; ++i)
		{
			v0[i] = Random(LOW, HIGH);
			v1[i] = Random(LOW, HIGH);
		}

		Matrix4 t0(v0);
		Matrix4 t1(v1);
		Matrix4 t2 = t0 + t1;

		return TestMatrix4Vars(t2, v0[0] + v1[0], v0[1] + v1[1], v0[2] + v1[2], v0[3] + v1[3],
			v0[4] + v1[4], v0[5] + v1[5], v0[6] + v1[6], v0[7] + v1[7],
			v0[8] + v1[8], v0[9] + v1[9], v0[10] + v1[10], v0[11] + v1[11],
			v0[12] + v1[12], v0[13] + v1[13], v0[14] + v1[14], v0[15] + v1[15]);
	}

	static bool TestMatrix4Subtraction(void)
	{
		float v0[16];
		float v1[16];
		for (unsigned i = 0; i < 16; ++i)
		{
			v0[i] = Random(LOW, HIGH);
			v1[i] = Random(LOW, HIGH);
		}

		Matrix4 t0(v0);
		Matrix4 t1(v1);
		Matrix4 t2 = t1 - t0;

		return TestMatrix4Vars(t2, v1[0] - v0[0], v1[1] - v0[1], v1[2] - v0[2], v1[3] - v0[3],
			v1[4] - v0[4], v1[5] - v0[5], v1[6] - v0[6], v1[7] - v0[7],
			v1[8] - v0[8], v1[9] - v0[9], v1[10] - v0[10], v1[11] - v0[11],
			v1[12] - v0[12], v1[13] - v0[13], v1[14] - v0[14], v1[15] - v0[15]);
	}


	static bool TestMultiplication(void)
	{

		float v[16];
		for (unsigned i = 0; i < 16; ++i)
			v[i] = Random(LOW, HIGH);

		Matrix4 t0(v);
		float mult = Random(LOW, HIGH);
		Matrix4 t1 = t0 * mult;

		if (!TestMatrix4Vars(t1, v[0] * mult, v[1] * mult, v[2] * mult, v[3] * mult,
			v[4] * mult, v[5] * mult, v[6] * mult, v[7] * mult,
			v[8] * mult, v[9] * mult, v[10] * mult, v[11] * mult,
			v[12] * mult, v[13] * mult, v[14] * mult, v[15] * mult))
			return false;

		return true;
	}

	static bool TestDivision(void)
	{
		float v[16];
		for (unsigned i = 0; i < 16; ++i)
			v[i] = Random(LOW, HIGH);

		Matrix4 t0(v);
		float div = Random(LOW, HIGH);
		Matrix4 t1 = t0 / div;

		return TestMatrix4Vars(t1, v[0] / div, v[1] / div, v[2] / div, v[3] / div,
			v[4] / div, v[5] / div, v[6] / div, v[7] / div,
			v[8] / div, v[9] / div, v[10] / div, v[11] / div,
			v[12] / div, v[13] / div, v[14] / div, v[15] / div);
	}
	static bool TestMakeZero(void)
	{
		float t0_vars[16];
		for (unsigned i = 0; i < 16; ++i)
			t0_vars[i] = Random(LOW, HIGH);

		Matrix4 t0(t0_vars);
		t0.MakeZero();

		return TestMatrix4Vars(t0, 0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f);
	}

	static bool TestMakeIdentity(void)
	{
		float v[16];
		for (unsigned i = 0; i < 16; ++i)
			v[i] = Random(LOW, HIGH);

		Matrix4 t0(v);
		t0.MakeIdentity();

		return TestMatrix4Vars(t0, 1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f);
	}


	static bool TestStaticZero(void)
	{
		return TestMatrix4Vars(Matrix4::ZERO, 0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f);
	}

	static bool TestStaticIdentity(void)
	{
		return TestMatrix4Vars(Matrix4::IDENTITY, 1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f);
	}
	static bool TestEquality(void)
	{
		float v[16];
		for (unsigned i = 0; i < 16; ++i)
			v[i] = Random(LOW, HIGH);

		Matrix4 t0(v);
		Matrix4 t1 = t0;

		if (t1 != t0)
			return false;

		t1.m00 = LOW - 1.0f;

		if (t1 == t0)
			return false;

		t1.m00 = t0.m00;
		t1.m01 = HIGH + 1.0f;

		if (t1 == t0)
			return false;

		t1.m01 = t0.m01;
		t1.m02 = LOW - 1.0f;

		if (t1 == t0)
			return false;

		t1.m02 = t0.m02;
		t1.m03 = HIGH + 1.0f;

		if (t1 == t0)
			return false;

		t1.m03 = t0.m03;
		t1.m10 = HIGH + 1.0f;

		if (t1 == t0)
			return false;

		t1.m10 = t0.m10;
		t1.m11 = LOW - 1.0f;

		if (t1 == t0)
			return false;

		t1.m11 = t0.m11;
		t1.m12 = HIGH + 1.0f;

		if (t1 == t0)
			return false;

		t1.m12 = t0.m12;
		t1.m13 = LOW - 1.0f;

		if (t1 == t0)
			return false;

		t1.m13 = t0.m13;
		t1.m20 = HIGH + 1.0f;

		if (t1 == t0)
			return false;

		t1.m20 = t0.m20;
		t1.m21 = LOW - 1.0f;

		if (t1 == t0)
			return false;

		t1.m21 = t0.m21;
		t1.m22 = HIGH + 1.0f;

		if (t1 == t0)
			return false;

		t1.m22 = t0.m22;
		t1.m23 = LOW - 1.0f;

		if (t1 == t0)
			return false;

		t1.m23 = t0.m23;

		if (t1 != t0)
			return false;

		return true;
	}

	static bool TestEulerAngles(void)
	{
		float yaw1 = Random(-90.0f, 0.0f),
			pitch1 = Random(0.0f, 180.0f),
			roll1 = Random(-180.0f, 180.0f);

		Matrix4 fromEuler1(yaw1, pitch1, roll1, false);
		Vector toEuler11 = fromEuler1.GetEulerAngles(false);

		Matrix4 fromEuler2(toEuler11.x, toEuler11.y, toEuler11.z, false);
		Vector toEuler21 = fromEuler2.GetEulerAngles(false);

		//(toEuler21 == toEuler11);
		return true;
	}

	bool Matrix4::Test(void)
	{
		bool fullSuccess = true;

		fullSuccess = (fullSuccess ? TestDefaultConstructor() : fullSuccess);
		fullSuccess = (fullSuccess ? TestNonDefaultConstructor() : fullSuccess);
		fullSuccess = (fullSuccess ? TestMakeZero() : fullSuccess);
		fullSuccess = (fullSuccess ? TestMakeIdentity() : fullSuccess);
		fullSuccess = (fullSuccess ? TestSetRow() : fullSuccess);
		fullSuccess = (fullSuccess ? TestSetCol() : fullSuccess);
		fullSuccess = (fullSuccess ? TestGetRow() : fullSuccess);
		fullSuccess = (fullSuccess ? TestGetCol() : fullSuccess);
		fullSuccess = (fullSuccess ? TestInverse() : fullSuccess);
		fullSuccess = (fullSuccess ? TestAdjoint() : fullSuccess);
		fullSuccess = (fullSuccess ? TestDeterminant() : fullSuccess);
		fullSuccess = (fullSuccess ? TestTranspose() : fullSuccess);
		fullSuccess = (fullSuccess ? TestTransposeTimes() : fullSuccess);
		fullSuccess = (fullSuccess ? TestTimesTranspose() : fullSuccess);
		fullSuccess = (fullSuccess ? TestTransposeTimesTranspose() : fullSuccess);
		fullSuccess = (fullSuccess ? TestVectorMultiplication() : fullSuccess);
		fullSuccess = (fullSuccess ? TestPointMultiplication() : fullSuccess);
		fullSuccess = (fullSuccess ? TestMatrix4Multiplication() : fullSuccess);
		fullSuccess = (fullSuccess ? TestMatrix4Addition() : fullSuccess);
		fullSuccess = (fullSuccess ? TestMatrix4Subtraction() : fullSuccess);
		fullSuccess = (fullSuccess ? TestMultiplication() : fullSuccess);
		fullSuccess = (fullSuccess ? TestDivision() : fullSuccess);
		fullSuccess = (fullSuccess ? TestEquality() : fullSuccess);
		fullSuccess = (fullSuccess ? TestStaticZero() : fullSuccess);
		fullSuccess = (fullSuccess ? TestStaticIdentity() : fullSuccess);
		fullSuccess = (fullSuccess ? TestEulerAngles() : fullSuccess);

		return fullSuccess;
	}
}
