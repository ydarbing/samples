/*
  ObjectAllocator.cpp
  Brady Reuter
  1/11/2015

  Custom memory manager with the use of a singly linked list
*/
#include "ObjectAllocator.h"
#include <cstring> //memset

/*********************************************************************/
 /*!
   \brief
     allocated fixed size of data based on the parameters given
   \param ObjectSize
     size of object that Allocator will be able to handle
   \param config
     parameters that will tell how the client wants the data to be handled
 */
 /***********************************************************************/
ObjectAllocator::ObjectAllocator(size_t ObjectSize, const OAConfig& config) throw(OAException):
m_freeList(NULL), m_pageList(NULL), m_Config(config), m_OAStats()
{
  m_OAStats.ObjectSize_ = ObjectSize;

  if(m_Config.Alignment_ > 0)
  {
    // need to compare the sizes of these to get correct alignment
    size_t align = m_Config.Alignment_;
    size_t left  = sizeof(void*) + m_Config.HBlockInfo_.size_ + m_Config.PadBytes_;
    size_t inner = (ObjectSize + (2 * m_Config.PadBytes_) + m_Config.HBlockInfo_.size_);
    size_t i = 1, k = 1;

    while(i*align <= left)
      ++i;
    while(k*align <= inner)
      ++k;
    m_Config.LeftAlignSize_  = static_cast<unsigned>((i*align) % left);
    m_Config.InterAlignSize_ = static_cast<unsigned>((k*align) % inner); 
  }
  // calculate the page size
  m_OAStats.PageSize_ = sizeof(void*) + m_Config.LeftAlignSize_ 
                                      + (m_Config.HBlockInfo_.size_ *  m_Config.ObjectsPerPage_)
                                      + (2 * m_Config.PadBytes_     *  m_Config.ObjectsPerPage_) 
                                      + (m_Config.InterAlignSize_   * (m_Config.ObjectsPerPage_ - 1)) 
                                      + (m_OAStats.ObjectSize_      * m_Config.ObjectsPerPage_);
  // left and inner will be used for pointer movement when allocating new pages and writing signatures 
  m_left  = sizeof(void*) + m_Config.LeftAlignSize_ + config.HBlockInfo_.size_ + m_Config.PadBytes_;
  m_inner = m_OAStats.ObjectSize_ + m_Config.InterAlignSize_ + config.HBlockInfo_.size_ + (2 * m_Config.PadBytes_);
  
  try
  {
    if(m_Config.UseCPPMemManager_)
      return;
    else
      ActuallyAllocatePage();
  }
  catch(OAException const& e)
  {
    throw e;
  }
}


/*********************************************************************/
 /*!
   \brief
     Object allocator destructor, destroys the object allocator
 */
 /***********************************************************************/
ObjectAllocator::~ObjectAllocator() throw()
{
  // free all pages
  while(m_pageList != NULL)
  {
    GenericObject* temp = m_pageList;
    m_pageList = m_pageList->Next;
    delete[] temp;
  }
}


/*********************************************************************/
 /*!
   \brief
      allocate data
    \return
      Pointer to the allocated data
 */
 /***********************************************************************/
void* ObjectAllocator::Allocate(const char* label /*= 0*/) throw(OAException)
{
  if(m_Config.UseCPPMemManager_)
  {
    ++m_OAStats.Allocations_;
    ++m_OAStats.ObjectsInUse_;
    // self explanitory, if there is more than the previous most object, increment it 
    if(m_OAStats.ObjectsInUse_ > m_OAStats.MostObjects_)
      ++m_OAStats.MostObjects_;

    return new char[m_OAStats.ObjectSize_];
  }
  else
  {
    // if freelist empty, try to make new page
    if(m_freeList == NULL)
      ActuallyAllocatePage();

    // get the head of freeList and give to client
    void* temp = reinterpret_cast<void*>(m_freeList);
    m_freeList = m_freeList->Next;

    ++m_OAStats.Allocations_;
    ++m_OAStats.ObjectsInUse_;
    --m_OAStats.FreeObjects_;

    // sign new block with allocated pattern
    if(m_Config.DebugOn_)
    {
      ToggleHeaderSignatures(temp);
      std::memset(temp, ALLOCATED_PATTERN, m_OAStats.ObjectSize_);
    }

    if(m_Config.HBlockInfo_.type_ == OAConfig::hbExternal)
      AllocateExternalHeader(temp, label);

    if(m_OAStats.ObjectsInUse_ > m_OAStats.MostObjects_)
      ++m_OAStats.MostObjects_;
    //return the former head of the freelist
    return temp;
  }
}

/*********************************************************************/
 /*!
   \brief
     Sets up the external header the user requested
    \param  objStart
      pointer to the object so we can move into the header
    \param  label
      string that will be allocated for identification
 */
 /***********************************************************************/
void ObjectAllocator::AllocateExternalHeader(void* objStart, const char* label)
{
  // dynamically allocate external header
  MemBlockInfo* hBlock = new MemBlockInfo;
  hBlock->alloc_num = m_OAStats.Allocations_;
  hBlock->in_use = true;
  // now dynamically allocate the label
  if(label == NULL)
  {
    hBlock->label = new char[1];
    hBlock->label[0] = 0;
  }
  else
  {
    hBlock->label = new char[std::strlen(label) + 1];
    std::strcpy(hBlock->label, label);
  }
  // now put this in the header location
  char* move = reinterpret_cast<char*>(objStart);
  move -= OAConfig::EXTERNAL_HEADER_SIZE + m_Config.PadBytes_;
  std::memcpy(move, &hBlock, sizeof(void*));
}
/*********************************************************************/
 /*!
   \brief
     free the memory that was given to the object
    \param  Object
      pointer to section of memory that is going to be freed
 */
 /***********************************************************************/
void ObjectAllocator::Free(void* Object) throw(OAException)
{
  ++m_OAStats.Deallocations_;
  if(m_Config.UseCPPMemManager_)
  {
    --m_OAStats.ObjectsInUse_;
    delete[] reinterpret_cast<char*>(Object);
    return;
  }
  else
  {
    if( m_Config.DebugOn_ )
    {
      // check for: 
      // 1 - trying to free an address not on any of the pages
      // 2 - freeing an object more than once
      // 3 - freeing object on appropriate boundry
      // 4 - checking padding
      GenericObject* objPage = GetPageOfObject(Object);
      if(objPage == NULL)
        throw OAException(OAException::E_BAD_BOUNDARY, "Free : Object address was not valid");
      if(OnBoundry(objPage, Object) == false)
        throw OAException(OAException::E_BAD_BOUNDARY, "Free : Not a valid boundry");
      if(DoubleFreeCheck(Object) == false)
        throw OAException(OAException::E_MULTIPLE_FREE, "Free : Tryed to free twice");
      int padCheck = PaddingCheck(Object);
      if(padCheck == 1)
        throw OAException(OAException::E_CORRUPTED_BLOCK, "Free : Padding currupted before object");
      else if(padCheck == 2)
        throw OAException(OAException::E_CORRUPTED_BLOCK, "Free : Padding currupted after object");
      // set headers back to 0 and sign the free pattern on the blocks
      ToggleHeaderSignatures(Object);
      std::memset(Object, FREED_PATTERN, m_OAStats.ObjectSize_);
    }

    if(m_Config.HBlockInfo_.type_ == OAConfig::hbExternal)
      FreeExternalHeader(Object);

    // push returned block onto top of freelist
    reinterpret_cast<GenericObject*>(Object)->Next = m_freeList;
    m_freeList = reinterpret_cast<GenericObject*>(Object);
    // update stats
    --m_OAStats.ObjectsInUse_;
    ++m_OAStats.FreeObjects_;
  }
}
/*********************************************************************/
 /*!
   \brief
      free allocated memory due to external header, 
      also sets header bytes back to 0
   \param obj
     pointer to start of the object memory
 */
 /***********************************************************************/
void ObjectAllocator::FreeExternalHeader(void* obj)
{
  // put this in the header location
  unsigned char* move = reinterpret_cast<unsigned char*>(obj);
  move -= OAConfig::EXTERNAL_HEADER_SIZE + m_Config.PadBytes_;
  MemBlockInfo* test = *reinterpret_cast<MemBlockInfo**>(move);
  delete [] test->label;
  delete test;
  std::memset(move, 0, sizeof(void*));
}
/*********************************************************************/
 /*!
   \brief
      gets page that contains the given obj 
    \param obj
      the object that is going to be found 
    \return
      pointer to page that has given obj
      if not found returns NULL
 */
 /***********************************************************************/
GenericObject* ObjectAllocator::GetPageOfObject(void* obj)
{
  GenericObject* temp = m_pageList;
  // go through pageList until we find page that contain the obj
  while(temp != NULL)
  {
    if( temp < obj && obj < (reinterpret_cast<unsigned char*>(temp) + m_OAStats.PageSize_ - 1))
      break;
    temp = temp->Next;
  }
  return temp;
}

/*********************************************************************/
 /*!
   \brief
      Calls the callback fn for each block still in use
    \param fn
      callback function
    \return
      Number of blocks in use
 */
 /***********************************************************************/
unsigned ObjectAllocator::DumpMemoryInUse(DUMPCALLBACK fn) const
{
  // no need to search if there are no objects in use
  if(m_OAStats.ObjectsInUse_ == 0)
    return 0;
  // go though pages and put all objects in callback function
  GenericObject* temp = m_pageList;
  while(temp != NULL)
  {
    UsedObjectsOnPage(temp, fn);
    temp = temp->Next;
  }
  return m_OAStats.ObjectsInUse_;
}
/******************************************************************************/
/*!
  \brief
    passes each block of memory in use on page to callback function
  \param page
    pointer to start of page whose used blocks should be dumped
  \param fn
    clients callback function for used blocks
*/
/******************************************************************************/
void ObjectAllocator::UsedObjectsOnPage(GenericObject* page, DUMPCALLBACK fn)const
{
  // go to first object on page
  char* temp = reinterpret_cast<char*>(page);
  temp += m_left;
  if(OnFreeList(temp))
    fn(temp, m_OAStats.ObjectSize_);
  // loop through rest of objects on page 
  for(unsigned i = 1; i < m_Config.ObjectsPerPage_; ++i)
  {
    temp += m_inner;
    if(OnFreeList(temp))
      fn(temp, m_OAStats.ObjectSize_);
  }
}

/*********************************************************************/
 /*!
   \brief
     tests if pointer is being used by user or on the freelist
    \param obj
      pointer being checked against freelist
    \return
      true if it is a legitimate free
      false if it is on the freelist
 */
 /***********************************************************************/
bool ObjectAllocator::OnFreeList(void* obj)const
{
  // header blocks will allow for faster checking
  // TODO should external header be in this check?
  if(m_Config.HBlockInfo_.type_ == OAConfig::hbBasic || 
     m_Config.HBlockInfo_.type_ == OAConfig::hbExtended)
  {
    char* header = reinterpret_cast<char*>(obj) - (m_Config.PadBytes_ + 1);
    // test flag bit on header to see if correctly set
    return IsHeaderFlagOn(header);
  }
  else
  {
    GenericObject* temp = m_freeList;
    while(temp != NULL)
    {
      if(temp == reinterpret_cast<GenericObject*>(obj))
        return false;
      temp = temp->Next;
    }
    return true;
  }
}
/*********************************************************************/
 /*!
   \brief
     calls the callback fn for each block that is potentially corrupted
    \param fn
      callback function
    \return
      number of corrupted blocks
 */
 /***********************************************************************/
unsigned ObjectAllocator::ValidatePages(VALIDATECALLBACK fn) const
{
  GenericObject* temp = m_pageList;
  unsigned numCorrupted = 0;
  // loop through all pages calling function that will
  // call a function on all parts of the page
  while(temp != NULL)
  {
    numCorrupted += ValidateObjectsOnPage(temp, fn);
    temp = temp->Next;
  }
  return numCorrupted;
}
/*********************************************************************/
 /*!
   \brief
      returns the statistics for the allocator
    \param page
      pointer to start of page
    \param fn
      callback function for testing corrupted blocks
    \return
      number of corruptions found on page
 */
 /***********************************************************************/
unsigned ObjectAllocator::ValidateObjectsOnPage(GenericObject* page, VALIDATECALLBACK fn)const
{
  unsigned  corruptions = 0;
  // go to first object on page
  char* temp = reinterpret_cast<char*>(page);
  temp += m_left;
  if(PaddingCheck(temp))
  {
    fn(temp, m_OAStats.ObjectSize_);
    ++corruptions;
  }
  
  // go through rest of objects on page
  for(unsigned i = 1; i < m_Config.ObjectsPerPage_; ++i)
  {
    temp += m_inner;
    if(PaddingCheck(temp))
    {
      fn(temp, m_OAStats.ObjectSize_);
      ++corruptions;
    }
  }
  return corruptions;
}
/*********************************************************************/
 /*!
   \brief
      Will free all empty pages
    \return
      number of pages freed
 */
 /***********************************************************************/
unsigned ObjectAllocator::FreeEmptyPages(void)
{
  return 0;
}
/*********************************************************************/
 /*!
   \brief
        Tells the driver if the extra credit was implemented
    \return
        a bool where true is yes, the extra credit was implemented
        and returning false means the exrea credit was not implemented
 */
 /***********************************************************************/
bool ObjectAllocator::ImplementedExtraCredit(void)
{
  // only alignment implemented
  return false;
}
/*********************************************************************/
 /*!
   \brief
     Turns on or off debugging
    \param State
      Bool for turning on and off the debug
 */
 /***********************************************************************/
void ObjectAllocator::SetDebugState(bool State)
{
  m_Config.DebugOn_ = State;
}
/*********************************************************************/
 /*!
   \brief
       returns a pointer to the internal free list
    \return
        const void* to the m_freeList
 */
 /***********************************************************************/
const void* ObjectAllocator::GetFreeList(void) const
{
  return m_freeList;
}
/*********************************************************************/
 /*!
   \brief
      returns a pointer to the internal page list
    \return
       const void* to the m_pageList
 */
 /***********************************************************************/
const void* ObjectAllocator::GetPageList(void) const
{
  return m_pageList;
}
/*********************************************************************/
 /*!
   \brief
      returns the configuration parameters
    \return
      copy of the configuration parameters
 */
 /***********************************************************************/
OAConfig ObjectAllocator::GetConfig(void) const
{
  return m_Config;
}
/*********************************************************************/
 /*!
   \brief
      returns the statistics for the allocator
    \return
      copy of the statistics for the allocator
 */
 /***********************************************************************/
OAStats ObjectAllocator::GetStats(void) const
{
  return m_OAStats;
}
/*********************************************************************/
 /*!
   \brief
     allocate new page and set up pointers
  \exception OAException
		 throws if max pages are allocated or not enough free memory
 */
 /***********************************************************************/
void ObjectAllocator::ActuallyAllocatePage(void) throw(OAException)
{
  // trying to make more pages than client requested
  if(m_OAStats.PagesInUse_ == m_Config.MaxPages_)
    throw OAException(OAException::E_NO_PAGES, "Allocate : Maximum number of pages has been allocated");

  char* newPage = NULL;
  try
  {
    // allocate the whole page 
    newPage = new char[m_OAStats.PageSize_];
  }
  catch(std::bad_alloc&)
  {
    throw OAException(OAException::E_NO_MEMORY, "Allocate : Not enough system memory available.");
  }

  // apply debugging to the memory 
  if(m_Config.DebugOn_)
    WriteNewPageSignatures(newPage);

  // push page onto top of pageList 
  reinterpret_cast<GenericObject*>(newPage)->Next = m_pageList;
  m_pageList = reinterpret_cast<GenericObject*>(newPage);
  // move newPage to the where first object actually is
  newPage += m_left;
  reinterpret_cast<GenericObject*>(newPage)->Next = NULL;
  m_freeList = reinterpret_cast<GenericObject*>(newPage);
  // Set up the m_freeList start at 1 because first obj was already set up
  for(unsigned i = 1; i <= m_Config.ObjectsPerPage_ - 1; ++i)
  {
    newPage += m_inner;
    reinterpret_cast<GenericObject*>(newPage)->Next = m_freeList;
    m_freeList = reinterpret_cast<GenericObject*>(newPage);
  }
  // update stat tracking
  ++m_OAStats.PagesInUse_;
  m_OAStats.FreeObjects_ += m_Config.ObjectsPerPage_;
}

/******************************************************************************/
/*!
  \brief
    write signatures to each section of new page
  \param page
    pointer to beginning of page that will be signed
*/
/******************************************************************************/
void ObjectAllocator::WriteNewPageSignatures(void* page)
{
  unsigned char* temp = reinterpret_cast<unsigned char*>(page);
  // sign full page and jump past next pointer
  std::memset(temp, UNALLOCATED_PATTERN, m_OAStats.PageSize_);
  temp +=  sizeof(void*);
  // sign left alignment and move past is
  std::memset(temp, ALIGN_PATTERN, m_Config.LeftAlignSize_);
  temp += m_Config.LeftAlignSize_;
  // sign header block and move past it
  std::memset(temp, 0x00, m_Config.HBlockInfo_.size_);
  temp += m_Config.HBlockInfo_.size_;
  // sign front padding and jump to beginning of first object
  std::memset(temp, PAD_PATTERN, m_Config.PadBytes_);
  temp += m_Config.PadBytes_;

  // now go through the rest of page
  for(unsigned i = 1; i < m_Config.ObjectsPerPage_; ++i)
  {
    // move past object
    temp += m_OAStats.ObjectSize_;
    // sign back padding and move past it
    std::memset(temp, PAD_PATTERN, m_Config.PadBytes_);
    temp += m_Config.PadBytes_;
    // sign inner alignment and move past it
    std::memset(temp, ALIGN_PATTERN, m_Config.InterAlignSize_);
    temp += m_Config.InterAlignSize_;
    // sign header block and move past it
    std::memset(temp, 0x00, m_Config.HBlockInfo_.size_);
    temp += m_Config.HBlockInfo_.size_;
    // sign front padding and move past it
    std::memset(temp, PAD_PATTERN, m_Config.PadBytes_);
    temp += m_Config.PadBytes_;
  }
  // move past last object and sign last pad bytes
  temp += m_OAStats.ObjectSize_;
  std::memset(temp, PAD_PATTERN, m_Config.PadBytes_);
}


/******************************************************************************/
/*!
  \brief
    this is only called twice, once when allocing to set the bit
     and once when freeing to set it back to 0
  \param obj
    pointer to start of object
*/
/******************************************************************************/
void ObjectAllocator::ToggleHeaderSignatures(void* obj)
{
  if(m_Config.HBlockInfo_.type_ == OAConfig::hbBasic ||
     m_Config.HBlockInfo_.type_ == OAConfig::hbExtended)
  {
    char* flag = reinterpret_cast<char*>(obj);
    flag -= m_Config.PadBytes_ + 1;

    char* temp = reinterpret_cast<char*>(obj);
    temp -=  OAConfig::BASIC_HEADER_SIZE + m_Config.PadBytes_;
    // since we are writing a unsigned number, need an unsigned pointer
    unsigned* allocs = reinterpret_cast<unsigned*>(temp);

    if(*flag)
    {
      *flag = 0;
      *allocs = 0;
    }
    else
    {
      *flag = 1;
      *allocs = m_OAStats.Allocations_;
      if(m_Config.HBlockInfo_.type_ == OAConfig::hbExtended)
      {
        // temp is currently at the allocation counter, so move the pointer
        //  the size of the useCounter which is an unsigned short
        temp -= sizeof(unsigned short);
        unsigned short* useCount = reinterpret_cast<unsigned short*>(temp);
        ++(*useCount); 
      }
    }
  }
}

/******************************************************************************/
/*!
  \brief
    determines memory is valid address on a page
  \param page
    pointer to start of page that contains block
  \param obj
    pointer to start of block whose address should be tested
  \return
    true if block on appropriate boundary
*/
/******************************************************************************/
bool ObjectAllocator::OnBoundry(GenericObject* page, void* obj)
{
  // go to the first obj on page
  char* temp = reinterpret_cast<char*>(page);
  temp += m_left;
  // see if that obj is within the boundry
  long unsigned check = static_cast<long unsigned>((reinterpret_cast<char*>(obj) - temp));
  if((check % m_inner) == 0)
    return true;
  else
    return false;
}

/******************************************************************************/
/*!
  \brief
    checks for double freeing of memory
  \param obj
    pointer of to be checked memory
  \return
    false if already freed (ie already on freelist)
*/
/******************************************************************************/
bool ObjectAllocator::DoubleFreeCheck(void* obj)
{
  if(m_Config.HBlockInfo_.type_ == OAConfig::hbBasic ||
     m_Config.HBlockInfo_.type_ == OAConfig::hbExtended)
  {
    //TODO OAConfig::external should be checked in need to be checked here?
    char* check = reinterpret_cast<char*>(obj) - (m_Config.PadBytes_ + 1);
    return IsHeaderFlagOn(check);
  }
  else
  {
    GenericObject* temp = m_freeList;
    while(temp != NULL)
    {
      if(temp == obj)
        return false;
      temp = temp->Next;
    }
    return true;
  }
}

/******************************************************************************/
/*!
  \brief
    bitwise operation to testing if specified bit of a byte is set
  \param temp
    pointer to header byte of memory
  \return
    true if the bit is set
*/
/******************************************************************************/
bool ObjectAllocator::IsHeaderFlagOn(char* temp)const
{
  return *temp & 1;
}
/******************************************************************************/
/*!
  \brief
    checks pad bytes and makes sure they have correct signature
  \param obj
    pointer of to be checked memory
  \return
    1 if left side is corrupted
    2 if right side is corrupted
    0 if pad bytes are valid
*/
/******************************************************************************/
int ObjectAllocator::PaddingCheck(void* obj)const
{
  // need to move pointer to the pad bytes
  unsigned char* pPad = reinterpret_cast<unsigned char*>(obj);  
  pPad -= m_Config.PadBytes_;

  for(unsigned i = 1; i < m_Config.PadBytes_; ++i)
  {
    if(*pPad != PAD_PATTERN)
      return 1;
    ++pPad;
  }
  // now need to check the back padding
  pPad = reinterpret_cast<unsigned char*>(obj); 
  pPad += m_OAStats.ObjectSize_;
  for(unsigned j = 1; j < m_Config.PadBytes_; ++j)
  {
    if(*pPad != PAD_PATTERN)
      return 2;
    ++pPad;
  }
  return 0;
}