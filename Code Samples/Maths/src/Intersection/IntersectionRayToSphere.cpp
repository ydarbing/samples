#include "Intersection/IntersectionRayToSphere.h"

#include "Geometry/Segment.h"
#include "Geometry/Ray.h"
#include "Geometry/Sphere.h"
#include <cassert>

namespace Maths
{


  IntersectionRayToSphere::IntersectionRayToSphere(const Ray& ray, const Sphere& sphere)
    : m_ray(&ray), m_sphere(&sphere), m_quantity(0)
  {
  }

  const Ray& IntersectionRayToSphere::GetRay(void) const
  {
    return *m_ray;
  }

  const Sphere& IntersectionRayToSphere::GetSphere(void) const
  {
    return *m_sphere;
  }

  int IntersectionRayToSphere::GetQuantity(void) const
  {
    return m_quantity;
  }

  const Point& IntersectionRayToSphere::GetIntersectionPoint(int index) const
  {
    assert(0 <= index && index < 2);
    return m_point[index];
  }

  float IntersectionRayToSphere::GetRayParameter(int index) const
  {
    assert(0 <= index && index < 2);
    return m_rayParameter[index];
  }


  float IntersectionRayToSphere::GetDiscriminant(float& a, float& b, float& c)
  {
    a = m_ray->m_dir.Dot(m_ray->m_dir);
    Vector diff = m_ray->m_origin - m_sphere->m_center;
    b = 2.0f * m_ray->m_dir.Dot(diff);
    c = diff.Dot(diff) - m_sphere->m_radius * m_sphere->m_radius;

    return ((b * b) - (4 * a * c));
  }


  bool IntersectionRayToSphere::Test(void)
  {
    float a, b, c;
    float discriminant = GetDiscriminant(a, b, c);

    if (discriminant < 0)
      return false;
    else
      return true;
  }

  bool IntersectionRayToSphere::Find(void)
  {
    float a, b, c;
    float discriminant = GetDiscriminant(a, b, c);

    if (discriminant < 0)
    {
      m_intersectType = INONE;
      return false;
    }

    discriminant = sqrt(discriminant);

    float den = 1.0f / (2.0f * a);
    float t0 = (-b + discriminant) * den;
    float t1 = (-b - discriminant) * den;
    m_rayParameter[0] = t0;
    m_rayParameter[1] = t1;

    m_intersectType = ISEGMENT;
    m_point[0] = m_ray->m_origin + m_ray->m_dir * t0;
    m_point[1] = m_ray->m_origin + m_ray->m_dir * t1;
    
    return true;
  }

  //void IntersectionRayToSphere::Draw(bool calc /*= false*/, ColorPtr intersectionColor /*= nullptr*/)
  //{
  //  if (calc)
  //    Find();
  //
  //  switch (m_intersectType)
  //  {
  //  case INONE:
  //    return;
  //  case IPOINT:
  //    m_point[0].Draw(intersectionColor);
  //    break;
  //  case ISEGMENT:
  //    if (intersectionColor)
  //    {
  //      Color ptColor = -(*intersectionColor);
  //      m_point[0].Draw(&ptColor);
  //      m_point[1].Draw(&ptColor);
  //    }
  //    Segment(m_point[0], m_point[1]).Draw(intersectionColor);
  //    break;
  //  }
  //}

}// namespace Maths
