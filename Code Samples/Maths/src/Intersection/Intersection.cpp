#include "Intersection/Intersection.h"

namespace Maths
{


  Intersection::Intersection()
    : m_intersectType(INONE), m_contactTime(0.0f)
  {
  }

  Intersection::~Intersection()
  {
  }

  float Intersection::GetContactTime(void) const
  {
    return m_contactTime;
  }

  int Intersection::GetIntersectionType(void) const
  {
    return m_intersectType;
  }

  bool Intersection::Test(void)
  {
    return false;
  }

  bool Intersection::Test(float t, const Vector& vel0, const Vector& vel1)
  {
    return false;
  }

  bool Intersection::Find(void)
  {
    return false;
  }

  bool Intersection::Find(float t, const Vector& vel0, const Vector& vel1)
  {
    return false;
  }

}// namespace Maths

