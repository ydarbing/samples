#include "Intersection/IntersectionRayToAABB.h"

#include "Geometry/Ray.h"
#include "Geometry/AABB.h"
#include "Geometry/Segment.h"
#include "Utils.h"

namespace Maths
{


  IntersectionRayToAAB::IntersectionRayToAAB(const Ray& ray, const AABB& aab)
    : m_ray(&ray), m_aab(&aab), m_quantity(0)
  {
  }

  const Ray& IntersectionRayToAAB::GetRay(void) const
  {
    return *m_ray;
  }

  const AABB& IntersectionRayToAAB::GetAAB(void) const
  {
    return *m_aab;
  }

  int IntersectionRayToAAB::GetQuantity(void) const
  {
    return m_quantity;
  }

  const Point& IntersectionRayToAAB::GetIntersectionPoint(int index) const
  {
    return m_point[index];
  }

  bool IntersectionRayToAAB::Test(void)
  {
    return false;
  }

  bool IntersectionRayToAAB::Find(void)
  {
    m_intersectType = INONE;
    m_quantity = 0;
    float t;
    Vector invDir;
    invDir.x = 1.0f / m_ray->m_dir.x;
    invDir.y = 1.0f / m_ray->m_dir.y;
    invDir.z = 1.0f / m_ray->m_dir.z;
    
    float t1 = (m_aab->m_min.x - m_ray->m_origin.x) * invDir.x;
    float t2 = (m_aab->m_max.x - m_ray->m_origin.x) * invDir.x;
    float t3 = (m_aab->m_min.y - m_ray->m_origin.y) * invDir.y;
    float t4 = (m_aab->m_max.y - m_ray->m_origin.y) * invDir.y;
    float t5 = (m_aab->m_min.z - m_ray->m_origin.z) * invDir.z;
    float t6 = (m_aab->m_max.z - m_ray->m_origin.z) * invDir.z;

    float tmin = Max(Max(Min(t1, t2), Min(t3, t4)), Min(t5, t6));
    float tmax = Min(Min(Max(t1, t2), Max(t3, t4)), Max(t5, t6));

    // if tmax < 0, ray is intersecting AAB, but whole AAB is behing us
    if (tmax < 0)
    {
      t = tmax;
      return false;
    }

    // if tmin > tmax, ray doesn't intersect AAB
    if (tmin > tmax)
    {
      t = tmax;
      return false;
    }

    t = tmin;
    m_point[0] = m_ray->m_origin + m_ray->m_dir * t;
    m_intersectType = IPOINT;
    m_quantity = 1;
    return true;
  }

  //void IntersectionRayToAAB::Draw(bool calc /*= false*/, ColorPtr intersectionColor /*= nullptr*/)
  //{
  //  if (calc)
  //    Find();
  //
  //  switch (m_intersectType)
  //  {
  //  case INONE:
  //    return;
  //  case IPOINT:
  //    m_point[0].Draw(intersectionColor);
  //    break;
  //  case ISEGMENT:
  //    if (intersectionColor)
  //    {
  //      Color ptColor = -(*intersectionColor);
  //      m_point[0].Draw(&ptColor);
  //      m_point[1].Draw(&ptColor);
  //    }
  //    Segment(m_point[0], m_point[1]).Draw(intersectionColor);
  //    break;
  //  }
  //}


}// namespace Maths
