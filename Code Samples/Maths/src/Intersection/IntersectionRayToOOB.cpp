#include "Intersection/IntersectionRayToOOB.h"

#include "Geometry/Ray.h"
#include "Geometry/OOB.h"
#include "Geometry/Segment.h"
#include "Utils.h"
namespace Maths
{


  IntersectionRayToOOB::IntersectionRayToOOB(const Ray& ray, const OOB& oob)
    : m_ray(&ray), m_oob(&oob), m_quantity(0)
  {
  }

  const Ray& IntersectionRayToOOB::GetRay(void) const
  {
    return *m_ray;
  }

  const OOB& IntersectionRayToOOB::GetOOB(void) const
  {
    return *m_oob;
  }

  int IntersectionRayToOOB::GetQuantity(void) const
  {
    return m_quantity;
  }

  const Point& IntersectionRayToOOB::GetIntersectionPoint(int index) const
  {
    return m_point[index];
  }

  bool IntersectionRayToOOB::Test(void)
  {
    float dirDotAxis[3], absDirDotAxis[3], diffDotAxis[3], AbsDiffDotAxis[3];
    Vector diff = m_ray->m_origin - m_oob->m_center;

    dirDotAxis[0] = m_ray->m_dir.Dot(m_oob->m_axis[0]);
    absDirDotAxis[0] = AbsVal(dirDotAxis[0]);
    diffDotAxis[0] = diff.Dot(m_oob->m_axis[0]);
    AbsDiffDotAxis[0] = AbsVal(diffDotAxis[0]);
    if (AbsDiffDotAxis[0] > m_oob->m_extent[0] && diffDotAxis[0] * dirDotAxis[0] >= 0.0f)
      return false;

    dirDotAxis[1] = m_ray->m_dir.Dot(m_oob->m_axis[1]);
    absDirDotAxis[1] = AbsVal(dirDotAxis[1]);
    diffDotAxis[1] = diff.Dot(m_oob->m_axis[1]);
    AbsDiffDotAxis[1] = AbsVal(diffDotAxis[1]);
    if (AbsDiffDotAxis[1] > m_oob->m_extent[1] && diffDotAxis[1] * dirDotAxis[1] >= 0.0f)
      return false;

    dirDotAxis[2] = m_ray->m_dir.Dot(m_oob->m_axis[2]);
    absDirDotAxis[2] = AbsVal(dirDotAxis[2]);
    diffDotAxis[2] = diff.Dot(m_oob->m_axis[2]);
    AbsDiffDotAxis[2] = AbsVal(diffDotAxis[2]);
    if (AbsDiffDotAxis[2] > m_oob->m_extent[2] && diffDotAxis[2] * dirDotAxis[2] >= 0.0f)
      return false;

    float dcdDotAxis[3];
    Vector dirCrossDiff = m_ray->m_dir.Cross(diff);

    dcdDotAxis[0] = AbsVal(dirCrossDiff.Dot(m_oob->m_axis[0]));
    float rhs = m_oob->m_extent[1] * absDirDotAxis[2] + m_oob->m_extent[2] * absDirDotAxis[1];
    if (dcdDotAxis[0] > rhs)
      return false;

    dcdDotAxis[1] = AbsVal(dirCrossDiff.Dot(m_oob->m_axis[1]));
    rhs = m_oob->m_extent[0] * absDirDotAxis[2] + m_oob->m_extent[2] * absDirDotAxis[0];
    if (dcdDotAxis[1] > rhs)
      return false;

    dcdDotAxis[2] = AbsVal(dirCrossDiff.Dot(m_oob->m_axis[2]));
    rhs = m_oob->m_extent[0] * absDirDotAxis[1] + m_oob->m_extent[1] * absDirDotAxis[0];
    if (dcdDotAxis[2] > rhs)
      return false;

    return true;
  }

  bool IntersectionRayToOOB::Find(void)
  {
    return false;
  }

  //void IntersectionRayToOOB::Draw(bool calc /*= false*/, ColorPtr intersectionColor /*= nullptr*/)
  //{
  //  if (calc)
  //    Find();
  //
  //  switch (m_intersectType)
  //  {
  //  case INONE:
  //    return;
  //  case IPOINT:
  //    m_point[0].Draw(intersectionColor);
  //    break;
  //  case ISEGMENT:
  //    if (intersectionColor)
  //    {
  //      Color ptColor = -(*intersectionColor);
  //      m_point[0].Draw(&ptColor);
  //      m_point[1].Draw(&ptColor);
  //    }
  //    Segment(m_point[0], m_point[1]).Draw(intersectionColor);
  //    break;
  //  }
  //}

}// namespace Maths
