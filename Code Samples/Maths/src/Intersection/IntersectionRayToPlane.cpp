#include "Intersection/IntersectionRayToPlane.h"

#include "Geometry/Ray.h"
#include "Geometry/Plane.h"

namespace Maths
{


  IntersectionRayToPlane::IntersectionRayToPlane(const Ray& ray, const Plane& plane)
    : m_plane(&plane), m_ray(&ray), m_rayParameter(-1.0f)
  {
  }

  const Ray& IntersectionRayToPlane::GetRay(void) const
  {
    return *m_ray;
  }

  const Plane& IntersectionRayToPlane::GetPlane(void) const
  {
    return *m_plane;
  }

  float IntersectionRayToPlane::GetRayParameter(void) const
  {
    return m_rayParameter;
  }

  Point IntersectionRayToPlane::GetIntersectionPoint(void) const
  {
    return Point(m_ray->m_origin + m_ray->m_dir * m_rayParameter);
  }

  bool IntersectionRayToPlane::Test(void)
  {
    return Find(); // both test and find are roughly the same
  }

  bool IntersectionRayToPlane::Find(void)
  {
    float dirDotNorm = m_ray->m_dir.Dot(m_plane->GetNormal());
    float signDist = m_plane->DistanceTo(m_ray->m_origin);

    if (AbsVal(dirDotNorm) > EPSILON)
    {
      m_rayParameter = -signDist / dirDotNorm;
      if (m_rayParameter >= 0.0f)
      {
        m_intersectType = IPOINT; // ray is not parallel to plane
        return true;
      }
    }
    if (m_rayParameter < 0.0f)
    {
      m_intersectType = INONE;
      return false;
    }
    else if (AbsVal(signDist) <= EPSILON)
    {
      m_intersectType = IRAY; // ray is within the plane
      return true;
    }

    m_intersectType = INONE;
    return false;
  }

  //void IntersectionRayToPlane::Draw(bool calc /*= false*/, ColorPtr intersectionColor /*= nullptr*/)
  //{
  //  if (calc)
  //    Find();
  //
  //  switch (m_intersectType)
  //  {
  //  case INONE:
  //    return;
  //  case IPOINT:
  //    GetIntersectionPoint().Draw(intersectionColor);
  //    break;
  //  case IRAY:
  //    m_ray->Draw(intersectionColor);
  //    break;
  //  }
  //}

}// namespace Maths
