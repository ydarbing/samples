#include "Intersection/SegmentToSegmentCommonEndPoint.h"
#include "Geometry/Segment.h"

namespace Maths
{

  bool FindCommonEndPoint(Point p0, float length_0, Point p1, float length_1, Vector hintDirection, Point *commonEndPos)
  {
    if (hintDirection.IsZero())
      hintDirection = Vector::UNIT_X;

    // two line segments with unknown directions are essentially two spheres
    Vector pos0ToPos1 = p1 - p0;

    if (pos0ToPos1.IsZero() && length_0 == length_1) // same segments
    {
      hintDirection.Normalize();
      *commonEndPos = p0 + hintDirection * length_0;
      return true;
    }

    float sqPtpLen = pos0ToPos1.SquaredLength();
    float sumLen = length_0 + length_1;
    float sqLen = sumLen * sumLen;

    if (sqLen < sqPtpLen)
    {
      return false; // the two line segments are not close enough to touch 
    }
    else if (sqLen == sqPtpLen) // single point of intersection possible
    {
      // will be a straight line so only need to normalize the vector from
      pos0ToPos1.Normalize();
      *commonEndPos = p0 + pos0ToPos1 * length_0;
      return true;
    }

    float lengthDiff = length_0 - length_1;
    float sqLengthDiff = lengthDiff * lengthDiff;

    if (sqPtpLen < sqLengthDiff) // length of one segment is too large to connect to one common endpoint
    {
      return false;
    }
    else if (sqPtpLen == sqLengthDiff) // segments are within each other but can touch a common endpoint
    {
      pos0ToPos1.Normalize();
      if (lengthDiff < 0.0f)
        *commonEndPos = p1 - pos0ToPos1 * length_1;
      else
        *commonEndPos = p0 + pos0ToPos1 * length_0;

      return true;
    }

    float d = sqPtpLen;
    float t = (1.0f + lengthDiff * sumLen / sqPtpLen) * 0.5f;
    // intersection center
    Point ci = p0 + pos0ToPos1 * t;
    // intersection radius
    float ri = sqrt(abs(length_0 * length_0 - t * t * d));
    // change name for easier reading
    Vector normal = pos0ToPos1;
    // project the hint onto the plane (separating axis) 
    // then find point on circle along the hint direction by using ri (radius of circle)
    Vector proj_normal_hint = normal * (hintDirection.Dot(normal) / normal.SquaredLength());
    Vector proj_hint_onto_plane = hintDirection - proj_normal_hint;

    if (proj_hint_onto_plane.IsZero())// if hint is parallel to normal, can pick either unit x or unit y
    {
      hintDirection.Normalize();// normalize to compare with unit vector
      hintDirection.x = abs(hintDirection.x);
      hintDirection.y = abs(hintDirection.y);
      hintDirection.z = abs(hintDirection.z);

      if (hintDirection == Vector::UNIT_X)
        hintDirection = Vector::UNIT_Y; // make it the Y unit vector, because hint is already along x
      else
        hintDirection = Vector::UNIT_X;

      proj_hint_onto_plane = hintDirection - proj_normal_hint;
    }
    proj_hint_onto_plane.Normalize();
    *commonEndPos = ci + proj_hint_onto_plane * ri;

    return true;
  }

  /*
  SegmentToSegmentCommonEndPoint::SegmentToSegmentCommonEndPoint(Segment& seg0, Segment& seg1, Vector hintDir)
  {
    m_pos0 = &seg0.center;
    m_length0 = seg0.extent;

    m_pos1 = &seg1.center;
    m_length1 = seg1.extent;

    m_hintDir = &hintDir;
  }
  */

  SegmentToSegmentCommonEndPoint::SegmentToSegmentCommonEndPoint(Point p0, float p0Length, Point p1, float p1Length, Vector hintDir)
    : m_hasCommonPoint(false)
  {
    m_pos0 = p0.GetPosition();
    m_length0 = p0Length;
    m_pos1 = p1.GetPosition();
    m_length1 = p1Length;

    m_hintDir = hintDir;
  }



  void SegmentToSegmentCommonEndPoint::SetSegment0(Point point, float len)
  {
    m_pos0 = point;
    m_length0 = len;
  }

  void SegmentToSegmentCommonEndPoint::SetSegment1(Point point, float len)
  {
    m_pos1 = point;
    m_length1 = len;
  }

  void SegmentToSegmentCommonEndPoint::SetHintDir(Vector dir)
  {
    m_hintDir = dir;
  }

  bool SegmentToSegmentCommonEndPoint::Find()
  {
    m_hasCommonPoint = FindCommonEndPoint(m_pos0, m_length0, m_pos1, m_length1, m_hintDir, &m_commonEndPoint);
    return m_hasCommonPoint;
  }

  //void SegmentToSegmentCommonEndPoint::Draw(bool calc, ColorPtr one, ColorPtr two, ColorPtr ptColor)
  //{
  //  if (calc)
  //    Find();
  //
  //  if (m_hasCommonPoint)
  //  {
  //    Segment(m_pos0, m_commonEndPoint).Draw(one);
  //    Segment(m_pos1, m_commonEndPoint).Draw(two);
  //    m_commonEndPoint.Draw(ptColor);
  //  }
  //}
}