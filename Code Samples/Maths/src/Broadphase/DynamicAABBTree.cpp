#include "Broadphase/DynamicAABBTree.h"
#include "Utils.h" // Max

namespace Maths
{
  DynamicAABBTree::DynamicAABBTree()
    : m_root(Node::Null), m_capacity(64), m_size(0)
  {
    m_nodes = new Node[m_capacity];
    AddToFreeList(0);
  }

  DynamicAABBTree::~DynamicAABBTree()
  {
    delete[] m_nodes;
  }

  int DynamicAABBTree::Insert(const AABB& aabb, void* userData)
  {
    int id = MakeNode();

    m_nodes[id].aabb = aabb;
    m_nodes[id].aabb.Expand();
    m_nodes[id].userData = userData;
    m_nodes[id].height = 0;

    InsertLeaf(id);

    return id;
  }

  void DynamicAABBTree::Remove(int id)
  {
    assert(id >= 0 && id < m_capacity);
    assert(m_nodes[id].IsLeaf());

    RemoveLeaf(id);
    DestroyNode(id);
  }

  bool DynamicAABBTree::Update(int id, const AABB& aabb, const Vector& displacement)
  {
    assert(id >= 0 && id < m_capacity);
    assert(m_nodes[id].IsLeaf());

    if (m_nodes[id].aabb.Contains(aabb))
      return false;

    RemoveLeaf(id);

    m_nodes[id].aabb = aabb;
    m_nodes[id].aabb.Expand();
    m_nodes[id].aabb.Stretch(displacement);

    InsertLeaf(id);

    return true;
  }

  void* DynamicAABBTree::GetUserData(int id) const
  {
    assert(id >= 0 && id < m_capacity);

    return m_nodes[id].userData;
  }

  const AABB& DynamicAABBTree::GetFatAABB(int id) const
  {
    assert(id >= 0 && id < m_capacity);

    return m_nodes[id].aabb;
  }

  void DynamicAABBTree::Validate() const
  {
    int freeNodes = 0;
    int index = m_freeList;

    while (index != Node::Null)
    {
      assert(index >= 0 && index < m_capacity);
      index = m_nodes[index].next;
      ++freeNodes;
    }

    assert(m_size + freeNodes == m_capacity);

    if (m_root != Node::Null)
    {
      assert(m_nodes[m_root].parent == Node::Null);

#ifdef _DEBUG
      ValidateTree(m_root);
#endif
    }
  }

  void DynamicAABBTree::ValidateTree(int index) const
  {
    Node* node = m_nodes + index;

    int leftIndex = node->left;
    int rightIndex = node->right;

    if (node->IsLeaf())
    {
      assert(rightIndex == Node::Null);
      assert(node->height == 0);
      return;
    }

    assert(leftIndex >= 0 && leftIndex < m_capacity);
    assert(rightIndex >= 0 && rightIndex < m_capacity);

    Node* leftChild = m_nodes + leftIndex;
    Node* rightChild = m_nodes + rightIndex;

    assert(leftChild->parent == index);
    assert(rightChild->parent == index);

    ValidateTree(leftIndex);
    ValidateTree(rightIndex);
  }

  int DynamicAABBTree::MakeNode()
  {
    if (m_freeList == Node::Null)
    {
      m_capacity *= 2;
      Node* newNodes = new Node[m_capacity];
      memcpy(newNodes, m_nodes, sizeof(Node) * m_size);
      delete[] m_nodes;
      m_nodes = newNodes;

      AddToFreeList(m_size);
    }

    int freeNode = m_freeList;

    m_freeList = m_nodes[m_freeList].next;
    m_nodes[freeNode].height = 0;
    m_nodes[freeNode].left = Node::Null;
    m_nodes[freeNode].right = Node::Null;
    m_nodes[freeNode].parent = Node::Null;
    m_nodes[freeNode].userData = nullptr;
    ++m_size;
    return freeNode;
  }

  int DynamicAABBTree::Balance(int aIndex)
  {
    Node* A = m_nodes + aIndex;
    if (A->IsLeaf() || A->height == 1)
      return aIndex;
    //       A (aIndex)
    //    /     \
    //   B       C
    //  / \     / \
    // D   E   F   G

    int bIndex = A->left;
    int cIndex = A->right;
    Node* B = m_nodes + bIndex;
    Node* C = m_nodes + cIndex;

    int balance = C->height - B->height;
    // C is heigher, promote C
    if (balance > 1)
      return PromoteChild(A, aIndex, C, cIndex, B);
    // B is higher, promote B
    else if (balance < -1)
      return PromoteChild(A, aIndex, B, bIndex, C);

    return aIndex;
  }


  int DynamicAABBTree::PromoteChild(Node* parent, int parentIndex, Node* promote, int promoteIndex, Node* promoteSibling)
  {
    //        /
    //       A   (parent)
    //    /     \
    //   B       C (promote is either B or C with the sibling being either C or B respectively)
    //  / \     / \
    // D   E   F   G

    int iLeftChild = promote->left;
    int iRightChild = promote->right;
    Node* leftChild = m_nodes + iLeftChild;
    Node* rightChild = m_nodes + iRightChild;

    // point grandparent to the node we are promoting
    if (parent->parent != Node::Null)
    {
      if (m_nodes[parent->parent].left == parentIndex)
        m_nodes[parent->parent].left = promoteIndex;
      else
        m_nodes[parent->parent].right = promoteIndex;
    }
    else
      m_root = promoteIndex;

    // swap parent and toPromote
    promote->right = parentIndex;
    promote->parent = parent->parent;
    parent->parent = promoteIndex;
    // finish rotating
    if (leftChild->height > rightChild->height)
    {
      promote->left = iLeftChild;
      parent->left = iRightChild;
      rightChild->parent = parentIndex;

      parent->aabb = promoteSibling->aabb.Combine(rightChild->aabb);
      promote->aabb = parent->aabb.Combine(leftChild->aabb);

      parent->height = 1 + Max(promoteSibling->height, rightChild->height);
      promote->height = 1 + Max(parent->height, leftChild->height);
    }
    else
    {
      promote->left = iRightChild;
      parent->left = iLeftChild;
      leftChild->parent = parentIndex;
      parent->aabb = promoteSibling->aabb.Combine(leftChild->aabb);
      promote->aabb = parent->aabb.Combine(rightChild->aabb);
      parent->height = 1 + Max(promoteSibling->height, leftChild->height);
      promote->height = 1 + Max(parent->height, rightChild->height);
    }

    return promoteIndex;
  }




  void DynamicAABBTree::InsertLeaf(int id)
  {
    if (m_root == Node::Null)
    {
      m_root = id;
      m_nodes[m_root].parent = Node::Null;
      return;
    }

    // search for sibling
    int index = m_root;
    AABB leafAabb = m_nodes[m_root].aabb;

    while (!m_nodes[index].IsLeaf())
    {
      // Cost for insertion at index (branch node), involves creation
      // of new branch to contain this index and the new leaf
      AABB combined = leafAabb.Combine(m_nodes[index].aabb);
      float combinedArea = combined.SurfaceArea();
      float branchCost = 2.0f * combinedArea;

      // Inherited cost (surface area growth from hierarchy update after descent)
      float inheritedCost = 2.0f * (combinedArea - m_nodes[index].aabb.SurfaceArea());

      int left = m_nodes[index].left;
      int right = m_nodes[index].right;

      float leftCost;
      float rightCost;
      float leftInflated = leafAabb.Combine(m_nodes[left].aabb).SurfaceArea();
      float rightInflated = leafAabb.Combine(m_nodes[right].aabb).SurfaceArea();

      if (m_nodes[left].IsLeaf())
        leftCost = leftInflated + inheritedCost;
      else
      {
        float branchArea = m_nodes[left].aabb.SurfaceArea();
        leftCost = leftInflated - branchArea + inheritedCost;
      }

      if (m_nodes[right].IsLeaf())
        rightCost = rightInflated + inheritedCost;
      else
      {
        float branchArea = m_nodes[right].aabb.SurfaceArea();
        rightCost = rightInflated - branchArea + inheritedCost;
      }

      // early out on branch index
      if (branchCost < leftCost && branchCost < rightCost)
        break;

      index = (leftCost < rightCost) ? left : right;
    }

    int sibling = index;
    // make new parent
    int oldParent = m_nodes[sibling].parent;
    int newParent = MakeNode();
    m_nodes[newParent].parent = oldParent;
    m_nodes[newParent].userData = nullptr;
    m_nodes[newParent].aabb = leafAabb.Combine(m_nodes[sibling].aabb);
    m_nodes[newParent].height = m_nodes[sibling].height + 1;

    // sibling was root
    if (oldParent == Node::Null)
    {
      m_nodes[newParent].left = sibling;
      m_nodes[newParent].right = id;
      m_nodes[sibling].parent = newParent;
      m_nodes[id].parent = newParent;
      m_root = newParent;
    }
    else
    {
      if (m_nodes[oldParent].left == sibling)
        m_nodes[oldParent].left = newParent;
      else
        m_nodes[oldParent].right = newParent;

      m_nodes[newParent].left = sibling;
      m_nodes[newParent].right = id;
      m_nodes[sibling].parent = newParent;
      m_nodes[id].parent = newParent;
    }

    SyncHeirarchy(m_nodes[id].parent);
  }

  void DynamicAABBTree::RemoveLeaf(int index)
  {
    if (index == m_root)
    {
      m_root = Node::Null;
      return;
    }

    int parent = m_nodes[index].parent;
    int grandparent = m_nodes[parent].parent;
    int sibling;

    if (m_nodes[parent].left == index)
      sibling = m_nodes[parent].right;
    else
      sibling = m_nodes[parent].left;
    // parent was root
    if (grandparent == Node::Null)
    {
      m_root = sibling;
      m_nodes[sibling].parent = Node::Null;
    }
    else
    {
      // connect grandparent to sibling
      if (m_nodes[grandparent].left == parent)
        m_nodes[grandparent].left = sibling;
      else
        m_nodes[grandparent].right = sibling;
      //connect sibling to grandparent
      m_nodes[sibling].parent = grandparent;
    }
    DestroyNode(parent);
    SyncHeirarchy(grandparent);
  }


  void DynamicAABBTree::SyncHeirarchy(int index)
  {
    while (index != Node::Null)
    {
      index = Balance(index);

      int left = m_nodes[index].left;
      int right = m_nodes[index].right;

      m_nodes[index].height = 1 + Max(m_nodes[left].height, m_nodes[right].height);
      m_nodes[index].aabb = m_nodes[left].aabb.Combine(m_nodes[right].aabb);

      index = m_nodes[index].parent;
    }
  }

}// namespace Maths

