
#include "Vector.h"
#include "Geometry/Point.h"
#include "Utils.h"


namespace Maths
{
  const Vector Vector::ZERO(0.0f, 0.0f, 0.0f, 0.0f);
  const Vector Vector::UNIT_X(1.0f, 0.0f, 0.0f, 0.0f);
  const Vector Vector::UNIT_Y(0.0f, 1.0f, 0.0f, 0.0f);
  const Vector Vector::UNIT_Z(0.0f, 0.0f, 1.0f, 0.0f);
  const Vector Vector::UNIT_W(0.0f, 0.0f, 0.0f, 1.0f);

  Vector::Vector()
    : x(0.0f), y(0.0f), z(0.0f), w(0.0f)
  {
  }

  Vector::Vector(float x_, float y_, float z_, float w_ /*= 0.0f*/)
    : x(x_), y(y_), z(z_), w(w_)
  {
  }

  float Vector::Dot(const Vector &rhs) const
  {
    return (x * rhs.x + y * rhs.y + z * rhs.z + w * rhs.w);
  }

  float Vector::DotPoint(const Point& rhs) const
  {
    return (x * rhs.x + y * rhs.y + z * rhs.z + w * rhs.w);
  }

  Vector Vector::Cross(const Vector &rhs) const
  {
    return Vector(y * rhs.z - rhs.y * z,
      rhs.x * z - x * rhs.z,
      x * rhs.y - rhs.x * y);
  }

  float Vector::Triple(const Vector & v1, const Vector & v2) const
  {
    return
      v[0] * (v1.v[1] * v2.v[2] - v1.v[2] * v2.v[1]) +
      v[1] * (v1.v[2] * v2.v[0] - v1.v[0] * v2.v[2]) +
      v[2] * (v1.v[0] * v2.v[1] - v1.v[1] * v2.v[0]);
  }

  float Vector::Length(void) const
  {
    return sqrt(SquaredLength());
  }

  float Vector::SquaredLength(void) const
  {
    return (x * x + y * y + z * z + w * w);
  }

  float Vector::Normalize(void)
  {
    float len = Length();

    if (len < Maths::EPSILON)
      len = x = y = z = w = 0.0f;
    else
    {
      float invLen = 1.0f / len;
      x *= invLen;
      y *= invLen;
      z *= invLen;
      w *= invLen;
    }
    return len;
  }

  void Vector::Clear(void)
  {
    memset(v, 0, sizeof(float) * 4);
  }

  bool Vector::IsZero(void) const
  {
    return (x == 0.0f &&
      y == 0.0f &&
      z == 0.0f);
  }

  Vector Vector::operator-(void) const
  {
    return Vector(-x, -y, -z, -w);
  }

  Vector Vector::operator+(const Vector &rhs) const
  {
    return Vector(x + rhs.x,
      y + rhs.y,
      z + rhs.z,
      w + rhs.w);
  }

  Vector Vector::operator-(const Vector &rhs) const
  {
    return Vector(x - rhs.x,
      y - rhs.y,
      z - rhs.z,
      w - rhs.w);
  }

  Vector Vector::operator*(float scalar) const
  {
    return Vector(x * scalar,
      y * scalar,
      z * scalar,
      w * scalar);
  }

  Vector Vector::operator*(const Vector& rhs)const
  {
	  return Vector(
		  v[0] * rhs.v[0],
		  v[1] * rhs.v[1],
		  v[2] * rhs.v[2]);
  }

  //Vector Vector::operator/(float scalar) const
  //{
  //  scalar = 1.0f / scalar;
  //  return Vector(x * scalar,
  //    y * scalar,
  //    z * scalar,
  //    w * scalar);
  //}

  Vector& Vector::operator+=(const Vector &rhs)
  {
    x += rhs.x;
    y += rhs.y;
    z += rhs.z;
    w += rhs.w;
    return (*this);
  }

  Vector& Vector::operator-=(const Vector &rhs)
  {
    x -= rhs.x;
    y -= rhs.y;
    z -= rhs.z;
    w -= rhs.w;
    return (*this);
  }


  Vector& Vector::operator*=(const Vector &rhs)
  {
    v[0] *= rhs.v[0];
    v[1] *= rhs.v[1];
    v[2] *= rhs.v[2];

    return (*this);
  }


  Vector& Vector::operator*=(float scalar)
  {
    x *= scalar;
    y *= scalar;
    z *= scalar;
    w *= scalar;
    return (*this);
  }

  Vector& Vector::operator/=(float scalar)
  {
    scalar = 1.0f / scalar;
    x *= scalar;
    y *= scalar;
    z *= scalar;
    w *= scalar;
    return (*this);
  }

  bool Vector::operator==(const Vector &rhs) const
  {
    if (Maths::AbsVal(x - rhs.x) > Maths::EPSILON)
      return false;
    if (Maths::AbsVal(y - rhs.y) > Maths::EPSILON)
      return false;
    if (Maths::AbsVal(z - rhs.z) > Maths::EPSILON)
      return false;
    if (Maths::AbsVal(w - rhs.w) > Maths::EPSILON)
      return false;

    return true;
  }

  bool Vector::operator!=(const Vector &rhs) const
  {
    return !(*this == rhs);
  }

  bool Vector::operator<(const Vector &rhs) const
  {
    return memcmp(v, rhs.v, sizeof(float) * 4) < 0;
  }

  bool Vector::operator<=(const Vector &rhs) const
  {
    return memcmp(v, rhs.v, sizeof(float) * 4) <= 0;
  }

  bool Vector::operator>(const Vector &rhs) const
  {
    return memcmp(v, rhs.v, sizeof(float) * 4) > 0;
  }

  bool Vector::operator>=(const Vector &rhs) const
  {
    return memcmp(v, rhs.v, sizeof(float) * 4) >= 0;
  }

  bool Vector::IsParallel(const Vector& v1, const Vector& v2)
  {
    Vector c(v1.Cross(v2));
    return Maths::Equal(c.x, 0.0f) && Maths::Equal(c.y, 0.0f) && Maths::Equal(c.z, 0.0f) && Maths::Equal(c.w, 0.0f);
  }



  std::ostream& operator<<(std::ostream &os, const Vector &v)
  {
    os << "(" << v.x << ", " << v.y << ", " << v.z << ", " << v.w << ")";
    return os;
  }


  // only does small array's (max of 10 elements) if larger, returns immediately 0, 0
  long Vector::MaxDot(const Vector *array, long array_count, float &retDot) const
  {
    const long cutoff = 10;
    
    if (array_count < cutoff)
    {
      float maxDot1 = -Maths::MAX_FLOAT;
      int ptIndex = -1;
      for (int i = 0; i < array_count; ++i)
      {
        float dot = array[i].Dot(*this);
        if (dot > maxDot1)
        {
          maxDot1 = dot;
          ptIndex = i;
        }
      }

      retDot = maxDot1;
      return ptIndex;
    }
    else
    {
      retDot = 0;
      return 0;
    }
  }

  // only does small array's (max of 10 elements) if larger, returns immediately 0, 0
  long Vector::MinDot(const Vector *array, long array_count, float &retDot) const
  {
    const long cutoff = 10;

    if (array_count < cutoff)
    {
      float minDot1 = Maths::MAX_FLOAT;
      int ptIndex = -1;
      for (int i = 0; i < array_count; ++i)
      {
        float dot = array[i].Dot(*this);
        if (dot < minDot1)
        {
          minDot1 = dot;
          ptIndex = i;
        }
      }

      retDot = minDot1;
      return ptIndex;
    }
    else
    {
      retDot = 0;
      return 0;
    }
  }






  //////////////////////////////////////////////////////////////////////////
  //                         TESTS
  //////////////////////////////////////////////////////////////////////////

  static const float LOW = -100.0f;
  static const float HIGH = 100.0f;


  static bool TestVectorVars(const Vector& v, float x, float y, float z, float w)
  {
    if (Maths::AbsVal(v.x - x) > Maths::EPSILON ||
      Maths::AbsVal(v.y - y) > Maths::EPSILON ||
      Maths::AbsVal(v.z - z) > Maths::EPSILON ||
      Maths::AbsVal(v.w - w) > Maths::EPSILON)
      return false;

    return true;
  }


  static bool TestDefaultConstructor(void)
  {
    Vector a;

    return TestVectorVars(a, 0.0f, 0.0f, 0.0f, 0.0f);
  }

  static bool TestStaticZero(void)
  {
    return TestVectorVars(Vector::ZERO, 0.0f, 0.0f, 0.0f, 0.0f);
  }

  static bool TestNonDefaultConstructor(void)
  {
    float x = Maths::Random(LOW, HIGH);
    float y = Maths::Random(LOW, HIGH);
    float z = Maths::Random(LOW, HIGH);
    float w = Maths::Random(LOW, HIGH);
    Vector a(x, y, z, w);

    return TestVectorVars(a, x, y, z, w);
  }



  static bool TestDot(void)
  {
    float X = Maths::Random(LOW, HIGH);
    float Y = Maths::Random(LOW, HIGH);
    float Z = Maths::Random(LOW, HIGH);
    float W = Maths::Random(LOW, HIGH);
    float x = Maths::Random(LOW, HIGH);
    float y = Maths::Random(LOW, HIGH);
    float z = Maths::Random(LOW, HIGH);
    float w = Maths::Random(LOW, HIGH);
    Vector a(X, Y, Z, W);
    Vector b(x, y, z, w);

    float d = a.Dot(b);

    return d == (X * x + Y * y + Z * z + W * w);
  }

  static bool TestDotPoint(void)
  {
    float X = Maths::Random(LOW, HIGH);
    float Y = Maths::Random(LOW, HIGH);
    float Z = Maths::Random(LOW, HIGH);
    float W = Maths::Random(LOW, HIGH);
    float x = Maths::Random(LOW, HIGH);
    float y = Maths::Random(LOW, HIGH);
    float z = Maths::Random(LOW, HIGH);
    float w = Maths::Random(LOW, HIGH);
    Vector a(X, Y, Z, W);
    Point b(x, y, z, w);

    float d = a.DotPoint(b);

    if (d != X * x + Y * y + Z * z + W * w)
      return false;

    return true;
  }

  static bool TestCross(void)
  {
    float X = Maths::Random(LOW, HIGH);
    float Y = Maths::Random(LOW, HIGH);
    float Z = Maths::Random(LOW, HIGH);
    float W = Maths::Random(LOW, HIGH);
    float x = Maths::Random(LOW, HIGH);
    float y = Maths::Random(LOW, HIGH);
    float z = Maths::Random(LOW, HIGH);
    float w = Maths::Random(LOW, HIGH);
    Vector a(X, Y, Z, W);
    Vector b(x, y, z, w);

    Vector crossTest = a.Cross(b);

    return TestVectorVars(crossTest,
      Y * z - Z * y,
      Z * x - X * z,
      X * y - Y * x,
      0.0f);
  }

  static bool TestLength(void)
  {
    float x = Maths::Random(LOW, HIGH);
    float y = Maths::Random(LOW, HIGH);
    float z = Maths::Random(LOW, HIGH);
    float w = Maths::Random(LOW, HIGH);
    Vector a(x, y, z, w);

    float len = a.Length();

    return (len == sqrt(x * x + y * y + z * z + w * w));
  }

  static bool TestSquaredLength(void)
  {
    float x = Maths::Random(LOW, HIGH);
    float y = Maths::Random(LOW, HIGH);
    float z = Maths::Random(LOW, HIGH);
    float w = Maths::Random(LOW, HIGH);
    Vector a(x, y, z, w);

    float len = a.SquaredLength();

    return (len == (x * x + y * y + z * z + w * w));
  }


  static bool TestNormalize(void)
  {
    float x = Maths::Random(LOW, HIGH);
    float y = Maths::Random(LOW, HIGH);
    float z = Maths::Random(LOW, HIGH);
    float w = Maths::Random(LOW, HIGH);
    Vector a(x, y, z, w);

    float len = a.Length();
    float n = a.Normalize();

    if (Maths::AbsVal(a.SquaredLength() - 1.0f) > Maths::EPSILON)
      return false;

    if (n != len)
      return false;

    return true;
  }

  static bool TestNegate(void)
  {
    float x = Maths::Random(LOW, HIGH);
    float y = Maths::Random(LOW, HIGH);
    float z = Maths::Random(LOW, HIGH);
    float w = Maths::Random(LOW, HIGH);
    Vector a(x, y, z, w);
    Vector b = -a;

    return TestVectorVars(b, -x, -y, -z, -w);
  }

  static bool TestVectorAddition(void)
  {
    float X = Maths::Random(LOW, HIGH);
    float Y = Maths::Random(LOW, HIGH);
    float Z = Maths::Random(LOW, HIGH);
    float W = Maths::Random(LOW, HIGH);
    float x = Maths::Random(LOW, HIGH);
    float y = Maths::Random(LOW, HIGH);
    float z = Maths::Random(LOW, HIGH);
    float w = Maths::Random(LOW, HIGH);
    Vector a(X, Y, Z, W);
    Vector b(x, y, z, w);
    Vector c = a + b;

    return TestVectorVars(c,
      X + x,
      Y + y,
      Z + z,
      W + w);
  }

  static bool TestVectorSubtraction(void)
  {
    float X = Maths::Random(LOW, HIGH);
    float Y = Maths::Random(LOW, HIGH);
    float Z = Maths::Random(LOW, HIGH);
    float W = Maths::Random(LOW, HIGH);
    float x = Maths::Random(LOW, HIGH);
    float y = Maths::Random(LOW, HIGH);
    float z = Maths::Random(LOW, HIGH);
    float w = Maths::Random(LOW, HIGH);
    Vector a(X, Y, Z, W);
    Vector b(x, y, z, w);
    Vector c = a - b;

    return TestVectorVars(c,
      X - x,
      Y - y,
      Z - z,
      W - w);
  }

  static bool TestMultiplication(void)
  {
    float x = Maths::Random(LOW, HIGH);
    float y = Maths::Random(LOW, HIGH);
    float z = Maths::Random(LOW, HIGH);
    float w = Maths::Random(LOW, HIGH);
    float mult = Maths::Random(LOW, HIGH);
    Vector a(x, y, z, w);
    Vector b = a * mult;

    return TestVectorVars(b,
      x * mult,
      y * mult,
      z * mult,
      w * mult);
  }

  static bool TestDivision(void)
  {
    float x = Maths::Random(LOW, HIGH);
    float y = Maths::Random(LOW, HIGH);
    float z = Maths::Random(LOW, HIGH);
    float w = Maths::Random(LOW, HIGH);
    float div = Maths::Random(LOW, HIGH);
    Vector a(x, y, z, w);
    Vector b = a / div;

    return TestVectorVars(b,
      x / div,
      y / div,
      z / div,
      w / div);
  }

  static bool TestEquality(void)
  {
    float x = Maths::Random(LOW, HIGH);
    float y = Maths::Random(LOW, HIGH);
    float z = Maths::Random(LOW, HIGH);
    float w = Maths::Random(LOW, HIGH);
    Vector a(x, y, z, w);
    Vector b = a;

    if (a != b)
      return false;

    b.x = -a.x;

    if (a == b)
      return false;

    b.x = a.x;
    b.y = -a.y;

    if (a == b)
      return false;

    b.y = a.y;
    b.z = -a.z;

    if (a == b)
      return false;

    b.z = a.z;
    b.w = -a.w;

    if (a == b)
      return false;

    b.w = a.w;

    if (a != b)
      return false;

    return true;
  }

  static bool TestUnit_X(void)
  {
    return TestVectorVars(Vector::UNIT_X, 1.0f, 0.0f, 0.0f, 0.0f);
  }

  static bool TestUnit_Y(void)
  {
    return TestVectorVars(Vector::UNIT_Y, 0.0f, 1.0f, 0.0f, 0.0f);
  }

  static bool TestUnit_Z(void)
  {
    return TestVectorVars(Vector::UNIT_Z, 0.0f, 0.0f, 1.0f, 0.0f);
  }

  static bool TestUnit_W(void)
  {
    return TestVectorVars(Vector::UNIT_W, 0.0f, 0.0f, 0.0f, 1.0f);
  }

  bool Vector::Test(void)
  {
    bool success = true;

    success = (success ? TestDefaultConstructor() : success);
    success = (success ? TestNonDefaultConstructor() : success);
    success = (success ? TestDot() : success);
    success = (success ? TestDotPoint() : success);
    success = (success ? TestCross() : success);
    success = (success ? TestLength() : success);
    success = (success ? TestSquaredLength() : success);
    success = (success ? TestNormalize() : success);
    success = (success ? TestNegate() : success);
    success = (success ? TestVectorAddition() : success);
    success = (success ? TestVectorSubtraction() : success);
    success = (success ? TestMultiplication() : success);
    success = (success ? TestDivision() : success);
    success = (success ? TestEquality() : success);
    success = (success ? TestStaticZero() : success);
    success = (success ? TestUnit_X() : success);
    success = (success ? TestUnit_Y() : success);
    success = (success ? TestUnit_Z() : success);
    success = (success ? TestUnit_W() : success);

    return success;
  }

  void Vector::BuildOrthonormalBasis(Vector& u, Vector& v, const Vector& w)
  {
    if (Maths::AbsVal(w.x) >= Maths::AbsVal(w.y))
    {
      float invLen = 1.0f / sqrt(w.x * w.x + w.z * w.z);
      u.x = -w.z * invLen;
      u.y = 0.0f;
      u.z = w.x * invLen;
      v.x = w.y * u.z;
      v.y = w.z * u.x - w.x * u.z;
      v.z = -w.y * u.x;
    }
    else
    {
      float invLen = 1.0f / sqrt(w.y * w.y + w.z * w.z);
      u.x = 0.0f;
      u.y = w.z * invLen;
      u.z = -w.y * invLen;
      v.x = w.y * u.z - w.z * u.y;
      v.y = -w.x * u.z;
      v.z = w.x * u.y;
    }
  }

  Vector Vector::BuildRandomUnitXYZ(void)
  {
    Vector temp(Maths::Random(-1.0f, 1.0f),
      Maths::Random(-1.0f, 1.0f),
      Maths::Random(-1.0f, 1.0f));
    temp.Normalize();
    return temp;
  }

  Vector Vector::BuildRandomUnitXY(void)
  {
    Vector temp(Maths::Random(-1.0f, 1.0f),
      Maths::Random(-1.0f, 1.0f),
      0.0f);
    temp.Normalize();
    return temp;
  }

  Vector Vector::BuildRandomUnitXZ(void)
  {
    Vector temp(Maths::Random(-1.0f, 1.0f),
      0.0f,
      Maths::Random(-1.0f, 1.0f));
    temp.Normalize();
    return temp;
  }

  Vector Vector::BuildRandomUnitYZ(void)
  {
    Vector temp(0.0f,
      Maths::Random(-1.0f, 1.0f),
      Maths::Random(-1.0f, 1.0f));
    temp.Normalize();
    return temp;
  }


}