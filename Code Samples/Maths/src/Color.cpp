
#include "Color.h"

#include "Utils.h"
#include "Vector.h"
#include "Geometry/Point.h"

namespace Maths
{

  const Color Color::RED(255, 0, 0);
  const Color Color::ORANGE(255, 165, 0);
  const Color Color::YELLOW(255, 255, 0);
  const Color Color::GREEN(0, 255, 0);
  const Color Color::BLUE(0, 0, 255);
  const Color Color::VIOLET(205, 57, 170);
  const Color Color::PINK(255, 182, 193);
  const Color Color::BROWN(210, 105, 30);
  const Color Color::GRAY(128, 128, 128);
  const Color Color::BLACK(0, 0, 0);
  const Color Color::WHITE(255, 255, 255);


  Color::Color(void)
    : r(0.0f), g(0.0f), b(0.0f), a(1.0f)
  {
  }

  Color::Color(float r_, float g_, float b_, float a_ /*= 1.0f*/)
    : r(r_), g(g_), b(b_), a(a_)
  {
  }

  Color::Color(int r_, int g_, int b_, int a_ /*= 255*/)
    : r(r_ / 255.0f), g(g_ / 255.0f), b(b_ / 255.0f), a(a_ / 255.0f)
  {
  }

  Color::Color(const Vector& v)
    : r(v.x), g(v.y), b(v.z), a(1.0f)
  {
  }

  Color::Color(const Point& p)
    : r(p.x), g(p.y), b(p.z), a(1.0f)
  {
  }

  void Color::Clear(bool toWhite /*= true*/)
  {
    a = 1.0f;
    if (toWhite)
      r = g = b = 1.0f;
    else
      r = g = b = 0.0f;
  }

  Color& Color::NormalizeColor(void)
  {
    r /= 255.0f;
    g /= 255.0f;
    b /= 255.0f;
    a /= 255.0f;
    return *this;
  }

  Color Color::GetNormalizedColor(void) const
  {
    float normal = 1.0f / 255.0f;
    return Color(r*normal, g*normal, b*normal, a*normal);
  }

  Color& Color::StandardizeColor(void)
  {
    r *= 255.0f;
    g *= 255.0f;
    b *= 255.0f;
    a *= 255.0f;
    return *this;
  }

  Color Color::GetStandardizedColor(void) const
  {
    return Color(r*255.0f, g*255.0f, b*255.0f, a*255.0f);
  }

  Color& Color::Clamp(void)
  {
    r = Maths::Clamp(r, 0.0f, 1.0f);
    g = Maths::Clamp(g, 0.0f, 1.0f);
    b = Maths::Clamp(b, 0.0f, 1.0f);
    a = Maths::Clamp(a, 0.0f, 1.0f);
    return *this;
  }

  Color Color::CreateVariance(const Color& var) const
  {
    return Color(Maths::Clamp(r + Maths::Random(-var.r, var.r), 0.0f, 1.0f),
      Maths::Clamp(g + Maths::Random(-var.g, var.g), 0.0f, 1.0f),
      Maths::Clamp(b + Maths::Random(-var.b, var.b), 0.0f, 1.0f),
      Maths::Clamp(a + Maths::Random(-var.a, var.a), 0.0f, 1.0f));
  }

  Color Color::operator-(void) const
  {
    return Color(1.0f - r,
      1.0f - g,
      1.0f - b,
      a);
  }

  Color Color::operator-(const Color& rhs) const
  {
    return Color(r - rhs.a * rhs.r,
      g - rhs.a * rhs.g,
      b - rhs.a * rhs.b,
      a);
  }

  Color Color::operator+(const Color& rhs) const
  {
    return Color(r + rhs.a * rhs.r,
      g + rhs.a * rhs.g,
      b + rhs.a * rhs.b,
      a);
  }

  Color Color::operator*(float scalar) const
  {
    return Color(r, g, b, a * scalar);
  }

  Color Color::operator/(float scalar) const
  {
    //assert(scalar != 0.0f)
    return Color(r, g, b, a / scalar);
  }

  Color& Color::operator-=(const Color& rhs)
  {
    r -= rhs.a * rhs.r;
    g -= rhs.a * rhs.g;
    b -= rhs.a * rhs.b;
    return (*this);
  }

  Color& Color::operator+=(const Color& rhs)
  {
    r += rhs.a * rhs.r;
    g += rhs.a * rhs.g;
    b += rhs.a * rhs.b;
    return (*this);
  }

  Color& Color::operator*=(float scalar)
  {
    a *= scalar;
    return (*this);
  }

  Color& Color::operator/=(float scalar)
  {
    //assert(scalar != 0.0f);
    a /= scalar;
    return (*this);
  }

  bool Color::operator==(const Color& rhs) const
  {
    return (r == rhs.r &&
      g == rhs.g &&
      b == rhs.b &&
      a == rhs.a);
  }

  bool Color::operator!=(const Color& rhs) const
  {
    return (*this == rhs);
  }

  Color Color::BuildRandomColor(float a /*= 1.0f*/)
  {
    return Color(Random(0.0f, 1.0f),
      Random(0.0f, 1.0f),
      Random(0.0f, 1.0f),
      a);
  }

  Color Color::BuildRandomRed(float a /*= 1.0f*/)
  {
    return Color(Random(0.8f, 1.0f),
      Random(0.0f, 0.4f),
      Random(0.0f, 0.4f),
      a);
  }

  Color Color::BuildRandomGreen(float a /*= 1.0f*/)
  {
    return Color(Random(0.0f, 0.4f),
      Random(0.8f, 1.0f),
      Random(0.0f, 0.4f),
      a);
  }

  Color Color::BuildRandomBlue(float a /*= 1.0f*/)
  {
    return Color(Random(0.0f, 0.4f),
      Random(0.0f, 0.4f),
      Random(0.8f, 1.0f),
      a);
  }

  std::ostream& operator<<(std::ostream& os, const Color& col)
  {
    os << "|red: " << col.r << "|green: " << col.g << "|blue: " << col.b << "|alpha: " << col.a << "|";
    return os;
  }

} // namespace Maths
