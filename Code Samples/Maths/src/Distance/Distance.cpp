#include "Distance/Distance.h"
#include "Utils.h"

namespace Maths
{

  Distance::Distance()
    : maximumIterations(0), zeroThreshold(EPSILON), m_contactTime(MAX_FLOAT),
    m_closestPoint0(), m_closestPoint1(), m_hasMultipleClosestPoints0(false),
    m_hasMultipleClosestPoints1(false), m_diffStep(0.0f), m_invTwoDiffStep(0.0f)
  {
    SetDifferenceStep(1e-3f);
  }

  Distance::~Distance()
  {
  }

  float Distance::GetDistance(float min, float max, const Vector& vel0, const Vector& vel1)
  {
    float t0 = min;
    float f0 = GetDistance(t0, vel0, vel1);
    if (f0 <= zeroThreshold)
    {
      m_contactTime = t0;  // distance is effectively zero
      return 0.0f;        // objects are in contact initially
    }
    float df0 = GetDerivative(t0, vel0, vel1);
    if (df0 >= 0.0f)
    {
      m_contactTime = t0;  // distance is increasing on [0, max]
      return f0;
    }
    float t1 = max;
    float f1 = GetDistance(t1, vel0, vel1);
    if (f1 <= zeroThreshold)
    {
      m_contactTime = t1;  // distance is effectively zero
      return 0.0f;
    }
    float df1 = GetDerivative(t1, vel0, vel1);
    if (df1 <= 0.0f)
    {
      m_contactTime = t1;  // distance is decreasing on [0, max]
      return f1;
    }
    int i = 0;
    for (; i < maximumIterations; ++i)
    {
      float t = t0 - (f0 / df0);  // compute the next Newtonion iterate
      if (t >= max)
        break;  // convexity guarantees distance is positive, switch to numerical minimizer
      
      float f = GetDistance(t, vel0, vel1);
      if (f <= zeroThreshold)
      {
        m_contactTime = t; // distance is effectively zero
        return 0.0f;
      }
      float df = GetDerivative(t, vel0, vel1);
      if (df >= 0.0f)
        break;  // convexity guarantees distance is positive, switch to numerical minimizer
      
      t0 = t;
      f0 = f;
      df0 = df;
    }
    if (i == maximumIterations)
    {
      m_contactTime = t0;  // no convergence within iteration counts
      return f0;          // distance at the last time
    }
    float tm = t0;  // distance is positive, use bisection to find roots of derivative
    for (i = 0; i < maximumIterations; ++i)
    {
      tm = 0.5f * (t0 + t1);
      float dfm = GetDerivative(tm, vel0, vel1);
      float product = dfm * df0;
      if (product < -zeroThreshold)
      {
        t1 = tm;
        df1 = dfm;
      }
      else if (product > zeroThreshold)
      {
        t0 = tm;
        df0 = dfm;
      }
      else
      {
        break;
      }
    }
    m_contactTime = tm;  // time when the minimum occurs, not contact time, store for debugging
    float fm = GetDistance(tm, vel0, vel1);
    return fm;
  }


  float Distance::GetDistanceSquared(float min, float max, const Vector& vel0, const Vector& vel1)
  {
    float t0 = min;
    float f0 = GetDistanceSquared(t0, vel0, vel1);
    if (f0 <= zeroThreshold)
    {
      m_contactTime = t0;  // distance is effectively zero
      return 0.0f;        // objects are in contact initially
    }
    float df0 = GetDerivativeSquared(t0, vel0, vel1);
    if (df0 >= 0.0f)
    {
      m_contactTime = t0;  // distance is increasing on [0, max]
      return f0;
    }
    float t1 = max;
    float f1 = GetDistanceSquared(t1, vel0, vel1);
    if (f1 <= zeroThreshold)
    {
      m_contactTime = t1;  // distance is effectively zero
      return 0.0f;
    }
    float df1 = GetDerivativeSquared(t1, vel0, vel1);
    if (df1 <= 0.0f)
    {
      m_contactTime = t1;  // distance is decreasing on [0, max]
      return f1;
    }
    int i = 0;
    for (; i < maximumIterations; ++i)
    {
      float t = t0 - (f0 / df0);  // compute the next Newtonion iterate
      if (t >= max)
        break;  // convexity guarantees distance is positive, switch to numerical minimizer

      float f = GetDistanceSquared(t, vel0, vel1);
      if (f <= zeroThreshold)
      {
        m_contactTime = t; // distance is effectively zero
        return 0.0f;
      }
      float df = GetDerivativeSquared(t, vel0, vel1);
      if (df >= 0.0f)
        break;  // convexity guarantees distance is positive, switch to numerical minimizer

      t0 = t;
      f0 = f;
      df0 = df;
    }
    if (i == maximumIterations)
    {
      m_contactTime = t0;  // no convergence within iteration counts
      return f0;          // distance at the last time
    }
    float tm = t0;  // distance is positive, use bisection to find roots of derivative
    for (i = 0; i < maximumIterations; ++i)
    {
      tm = 0.5f * (t0 + t1);
      float dfm = GetDerivativeSquared(tm, vel0, vel1);
      float product = dfm * df0;
      if (product < -zeroThreshold)
      {
        t1 = tm;
        df1 = dfm;
      }
      else if (product > zeroThreshold)
      {
        t0 = tm;
        df0 = dfm;
      }
      else
      {
        break;
      }
    }
    m_contactTime = tm;  // time when the minimum occurs, not contact time, store for debugging
    float fm = GetDistanceSquared(tm, vel0, vel1);
    return fm;
  }

  float Distance::GetDerivative(float t, const Vector& vel0, const Vector& vel1)
  {
    float funcp = GetDistance(t + m_diffStep, vel0, vel1);
    float funcm = GetDistance(t - m_diffStep, vel0, vel1);
    float deriv = m_invTwoDiffStep * (funcp - funcm);
    return deriv;
  }

  float Distance::GetDerivativeSquared(float t, const Vector& vel0, const Vector& vel1)
  {
    float dist = GetDistance(t, vel0, vel1);
    float deriv = GetDerivative(t, vel0, vel1);
    float derivSq = 2.0f * dist * deriv;
    return derivSq;
  }

  void Distance::SetDifferenceStep(float step)
  {
    if (step > 0.0f)
      m_diffStep = step;
    else
      m_diffStep = 1e-3f;

    m_invTwoDiffStep = 0.5f / m_diffStep;
  }

  float Distance::GetDifferenceStep(void) const
  {
    return m_diffStep;
  }

  float Distance::GetContactTime(void) const
  {
    return m_contactTime;
  }

  const Point& Distance::GetClosestPoint0(void) const
  {
    return m_closestPoint0;
  }

  const Point& Distance::GetClosestPoint1(void) const
  {
    return m_closestPoint1;
  }

  bool Distance::HasMultipleClosestPoints0(void) const
  {
    return m_hasMultipleClosestPoints0;
  }

  bool Distance::HasMultipleClosestPoints1(void) const
  {
    return m_hasMultipleClosestPoints1;
  }


}// namespace Maths
