#include "Distance/DistancePointToSegment.h"
#include "Utils.h"

namespace Maths
{


  DistancePointToSegment::DistancePointToSegment(const Point& point, const Segment& segment)
    : m_point(&point), m_segment(&segment)
  {

  }

  const Point& DistancePointToSegment::GetPoint(void) const
  {
    return *m_point;
  }

  const Segment& DistancePointToSegment::GetSegment(void) const
  {
    return *m_segment;
  }

  float DistancePointToSegment::GetSegmentParameter(void) const
  {
    return m_segmentParam;
  }

  //void DistancePointToSegment::Draw(bool calc /*= false*/, ColorPtr one /*= nullptr*/, ColorPtr two /*= nullptr*/, ColorPtr seg /*= nullptr*/)
  //{
  //  if (calc)
  //    GetDistanceSquared();
  //
  //  m_point->Draw(one);
  //  m_segment->Draw(two);
  //  m_closestPoint0.Draw(two);
  //  m_closestPoint1.Draw(one);
  //  Segment(m_closestPoint0, m_closestPoint1).Draw(seg);
  //}

  float DistancePointToSegment::GetDistance(void)
  {
    return sqrt(GetDistanceSquared());
  }

  float DistancePointToSegment::GetDistanceSquared(void)
  {
    Vector diff = (*m_point) - m_segment->center;
    m_segmentParam = m_segment->direction.Dot(diff);

    if (-m_segment->extent < m_segmentParam)
    {
      if (m_segmentParam < m_segment->extent)
        m_closestPoint1 = m_segment->center + m_segment->direction * m_segmentParam;
      else
        m_closestPoint1 = m_segment->point1;
    }
    else
    {
      m_closestPoint1 = m_segment->point0;
    }

    m_segmentParam = Clamp(m_segmentParam, -m_segment->extent, m_segment->extent);
    m_closestPoint0 = (*m_point);
    return (m_closestPoint0 - m_closestPoint1).SquaredLength();
  }

  float DistancePointToSegment::GetDistance(float t, const Vector& vel0, const Vector& vel1)
  {
    Point stepPoint = *m_point + vel0 * t;
    Point stepCenter = m_segment->center + vel1 * t;
    Segment stepSegment(stepCenter, m_segment->direction, m_segment->extent);
    return DistancePointToSegment(stepPoint, stepSegment).GetDistance();
  }

  float DistancePointToSegment::GetDistanceSquared(float t, const Vector& vel0, const Vector& vel1)
  {
    Point stepPoint = *m_point + vel0 * t;
    Point stepCenter = m_segment->center + vel1 * t;
    Segment stepSegment(stepCenter, m_segment->direction, m_segment->extent);
    return DistancePointToSegment(stepPoint, stepSegment).GetDistanceSquared();
  }

} //namespace Maths

