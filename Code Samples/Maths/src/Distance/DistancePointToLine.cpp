#include "Distance/DistancePointToLine.h"
#include "Geometry/Segment.h"

namespace Maths
{

  DistancePointToLine::DistancePointToLine(const Point& point, const Line& line)
    : m_point(&point), m_line(&line)
  {
  }

  const Point& DistancePointToLine::GetPoint(void) const
  {
    return *m_point;
  }

  const Line& DistancePointToLine::GetLine(void) const
  {
    return *m_line;
  }

  float DistancePointToLine::GetLineParameter(void) const
  {
    return m_lineParam;
  }


  //void DistancePointToLine::Draw(bool calc /* = false */, ColorPtr one /* = nullptr */, ColorPtr two /* = nullptr */, ColorPtr seg /* = nullptr */)
  //{
  //  if (calc)
  //    GetDistanceSquared();
  //
  //  m_point->Draw(one);
  //  m_line->Draw(two);
  //  m_closestPoint0.Draw(two);
  //  m_closestPoint1.Draw(one);
  //  Segment(m_closestPoint0, m_closestPoint1).Draw(seg);
  //}

  float DistancePointToLine::GetDistance(void)
  {
    return sqrt(GetDistanceSquared());
  }

  float DistancePointToLine::GetDistanceSquared(void)
  {
    Vector diff = *m_point - m_line->point;
    m_lineParam = m_line->direction.Dot(diff);

    m_closestPoint0 = *m_point;
    m_closestPoint1 = m_line->point + m_line->direction * m_lineParam;

    return (m_closestPoint0 - m_closestPoint1).SquaredLength();
  }

  float DistancePointToLine::GetDistance(float t, const Vector& vel0, const Vector& vel1)
  {
    Point point0 = *m_point + vel0 * t;
    Point point1 = m_line->point + vel1 * t;
    Line stepLine(point1, m_line->direction);
    return DistancePointToLine(point0, stepLine).GetDistance();
  }

  float DistancePointToLine::GetDistanceSquared(float t, const Vector& vel0, const Vector& vel1)
  {
    Point point0 = (*m_point) + vel0 * t;
    Point point1 = m_line->point + vel1 * t;
    Line stepLine(point1, m_line->direction);
    return (DistancePointToLine(point0, stepLine).GetDistanceSquared());
  }

}
