#include "Distance/DistancePointToOOB.h"

#include "Geometry/Segment.h"
#include "Geometry/OOB.h"
#include "Vector.h"

namespace Maths
{
  DistancePointToOOB::DistancePointToOOB(const Point& point, const OOB& oob)
    : m_point(&point), m_oob(&oob)
  {

  }

  const Point& DistancePointToOOB::GetPoint(void) const
  {
    return *m_point;
  }

  const OOB& DistancePointToOOB::GetOOB(void) const
  {
    return *m_oob;
  }

  //void DistancePointToOOB::Draw(bool calc /*= false*/, ColorPtr one /*= nullptr*/, ColorPtr two /*= nullptr*/, ColorPtr seg /*= nullptr*/)
  //{
  //  if (calc)
  //    GetDistanceSquared();
  //
  //  m_point->Draw(one);
  //  m_oob->Draw(two);
  //  m_closestPoint0.Draw(two);
  //  m_closestPoint1.Draw(one);
  //  Segment(m_closestPoint0, m_closestPoint1).Draw(seg);
  //}

  float DistancePointToOOB::GetDistance(void)
  {
    return sqrt(GetDistanceSquared());
  }

  float DistancePointToOOB::GetDistanceSquared(void)
  {
    Vector diff = *m_point - m_oob->m_center;
    Vector closest;
    float distSq = 0;
    float delta = 0;

    for (unsigned i = 0; i < 3; ++i)
    {
      closest.v[i] = diff.Dot(m_oob->m_axis[i]);
      if (closest.v[i] < -m_oob->m_extent[i])
      {
        delta = closest.v[i] + m_oob->m_extent[i];
        distSq += delta * delta;
        closest.v[i] = -m_oob->m_extent[i];
      }
      else if (closest.v[i] > m_oob->m_extent[i])
      {
        delta = closest.v[i] - m_oob->m_extent[i];
        distSq += delta * delta;
        closest.v[i] = m_oob->m_extent[i];
      }
    }

    m_closestPoint0 = *m_point;
    m_closestPoint1 = m_oob->m_center
                      + m_oob->m_axis[0] * closest.x
                      + m_oob->m_axis[1] * closest.y
                      + m_oob->m_axis[2] * closest.z;
    return distSq;
  }

  float DistancePointToOOB::GetDistance(float t, const Vector& vel0, const Vector& vel1)
  {
    Point movedPoint = (*m_point) + vel0 * t;
    Point center = m_oob->m_center + vel1 * t;
    OOB movedOOB(center, m_oob->m_axis, m_oob->m_extent);
    return DistancePointToOOB(movedPoint, movedOOB).GetDistance();
  }


  float DistancePointToOOB::GetDistanceSquared(float t, const Vector& vel0, const Vector& vel1)
  {
    Point movedPoint = (*m_point) + vel0 * t;
    Point center = m_oob->m_center + vel1 * t;
    OOB movedOOB(center, m_oob->m_axis, m_oob->m_extent);
    return DistancePointToOOB(movedPoint, movedOOB).GetDistanceSquared();
  }

}// namespace Maths
