#include "Distance/DistancePointToPlane.h"
#include "Geometry/Segment.h"

namespace Maths
{


  DistancePointToPlane::DistancePointToPlane(const Point& point, const Plane& plane)
    :m_point(&point), m_plane(&plane)
  {
  }

  const Point& DistancePointToPlane::GetPoint(void) const
  {
    return *m_point;
  }

  const Plane& DistancePointToPlane::GetPlane(void) const
  {
    return *m_plane;
  }

  //void DistancePointToPlane::Draw(bool calc /*= false*/, ColorPtr one /*= nullptr*/, ColorPtr two /*= nullptr*/, ColorPtr seg /*= nullptr*/)
  //{
  //  if (calc)
  //    GetDistanceSquared();
  //
  //  m_point->Draw(one);
  //  m_plane->Draw(two);
  //  m_closestPoint0.Draw(two);
  //  m_closestPoint1.Draw(one);
  //  Segment(m_closestPoint0, m_closestPoint1).Draw(seg);
  //}

  float DistancePointToPlane::GetDistance(void)
  {
    float dist = m_plane->DistanceTo(*m_point);
    m_closestPoint0 = *m_point;
    m_closestPoint1 = *m_point - m_plane->GetNormal() * dist;
    return AbsVal(dist);
  }
  float DistancePointToPlane::GetDistanceSquared(void)
  {
    float dist = m_plane->DistanceTo(*m_point);
    m_closestPoint0 = *m_point;
    m_closestPoint1 = *m_point - m_plane->GetNormal() * dist;
    return dist*dist;
  }


  float DistancePointToPlane::GetDistance(float t, const Vector& vel0, const Vector& vel1)
  {
    Point stepPoint = *m_point + vel0 * t;
    float stepConst = m_plane->GetConstant() + m_plane->GetNormal().Dot(vel1);
    Plane stepPlane(m_plane->a, m_plane->b, m_plane->c, stepConst);
    return DistancePointToPlane(stepPoint, stepPlane).GetDistance();
  }

  float DistancePointToPlane::GetDistanceSquared(float t, const Vector& vel0, const Vector& vel1)
  {
    Point stepPoint = *m_point + vel0 * t;
    float stepConst = m_plane->GetConstant() + m_plane->GetNormal().Dot(vel1);
    Plane stepPlane(m_plane->a, m_plane->b, m_plane->c, stepConst);
    return DistancePointToPlane(stepPoint, stepPlane).GetDistanceSquared();
  }

} //namespace Maths
