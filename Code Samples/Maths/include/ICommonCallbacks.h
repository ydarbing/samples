#pragma once


typedef void(*WheelCallback)(float deltax, float deltay);
typedef void(*ResizeCallback)(float width, float height);
typedef void(*MouseMoveCallback)(float x, float y);
typedef void(*MouseButtonCallback)(int button, int state, float x, float y);
typedef void(*KeyboardCallback)(int keycode, int state);
typedef void(*RenderCallback) ();

enum {
  KEY_ESCAPE = 27,
  KEY_F1 = 0xff00,
  KEY_F2,
  KEY_F3,
  KEY_F4,
  KEY_F5,
  KEY_F6,
  KEY_F7,
  KEY_F8,
  KEY_F9,
  KEY_F10,
  KEY_F11,
  KEY_F12,
  KEY_F13,
  KEY_F14,
  KEY_F15,
  KEY_LEFT_ARROW,
  KEY_RIGHT_ARROW,
  KEY_UP_ARROW,
  KEY_DOWN_ARROW,
  KEY_PAGE_UP,
  KEY_PAGE_DOWN,
  KEY_END,
  KEY_HOME,
  KEY_INSERT,
  KEY_DELETE,
  KEY_BACKSPACE,
  KEY_SHIFT,
  KEY_CONTROL,
  KEY_ALT,
  KEY_RETURN
};