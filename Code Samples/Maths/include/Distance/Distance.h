#pragma once
#include "Color.h"
#include "Geometry/Point.h"

namespace Maths
{
  class Distance
  {
  public:
    virtual ~Distance();

    //virtual void Draw(bool calc = false, ColorPtr one = nullptr, ColorPtr two = nullptr, ColorPtr seg = nullptr) = 0;

    virtual float GetDistance(void) = 0;
    virtual float GetDistanceSquared(void) = 0;

    // distances for dynamics
    virtual float GetDistance(float t, const Vector& vel0, const Vector& vel1) = 0;
    virtual float GetDistanceSquared(float t, const Vector& vel0, const Vector& vel1) = 0;

    virtual float GetDerivative(float t, const Vector& vel0, const Vector& vel1);
    virtual float GetDerivativeSquared(float t, const Vector& vel0, const Vector& vel1);

    virtual float GetDistance(float min, float max, const Vector& vel0, const Vector& vel1);
    virtual float GetDistanceSquared(float min, float max, const Vector& vel0, const Vector& vel1);

    void SetDifferenceStep(float step);
    float GetDifferenceStep(void) const;

    float GetContactTime(void) const;

    const Point& GetClosestPoint0(void) const;
    const Point& GetClosestPoint1(void) const;
    bool HasMultipleClosestPoints0(void) const;
    bool HasMultipleClosestPoints1(void) const;

  public:
    int maximumIterations;
    float zeroThreshold;

  protected:
    Distance();

    float m_contactTime;
    Point m_closestPoint0;
    Point m_closestPoint1;
    bool m_hasMultipleClosestPoints0;
    bool m_hasMultipleClosestPoints1;
    float m_diffStep;
    float m_invTwoDiffStep;

  };

} // namespace Maths
