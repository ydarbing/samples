#pragma once
#include "Distance/Distance.h"

namespace Maths
{
  class OOB;

  class DistancePointToOOB : public Distance
  {
  public:
    DistancePointToOOB(const Point& point, const OOB& oob);

    const Point& GetPoint(void) const;
    const OOB& GetOOB(void) const;

    //void Draw(bool calc = false, ColorPtr one = nullptr, ColorPtr two = nullptr, ColorPtr seg = nullptr) override;

    virtual float GetDistance(void) override;
    virtual float GetDistanceSquared(void) override;
    virtual float GetDistance(float t, const Vector& vel0, const Vector& vel1) override;
    virtual float GetDistanceSquared(float t, const Vector& vel0, const Vector& vel1) override;

  private:
    const Point* m_point;
    const OOB* m_oob;
  };

}// namespace Maths
