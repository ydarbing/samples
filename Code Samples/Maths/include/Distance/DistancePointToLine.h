#pragma once
#include "Distance/Distance.h"
#include "Geometry/Line.h"

namespace Maths
{
  class DistancePointToLine : public Distance
  {
  public:
    DistancePointToLine(const Point& point, const Line& line);

    const Point& GetPoint(void) const;
    const Line& GetLine(void) const;

    //void Draw(bool calc = false, ColorPtr one = nullptr, ColorPtr two = nullptr, ColorPtr seg = nullptr) override;

    float GetLineParameter(void) const;
    virtual float GetDistance(void) override;
    virtual float GetDistanceSquared(void) override;
    virtual float GetDistance(float t, const Vector& vel0, const Vector& vel1) override;
    virtual float GetDistanceSquared(float t, const Vector& vel0, const Vector& vel1) override;

  private:
    const Point* m_point;
    const Line* m_line;
    float m_lineParam;
  };
}// namespace Maths
