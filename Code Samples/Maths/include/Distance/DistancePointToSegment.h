#pragma once
#include "Distance/Distance.h"
#include "Geometry/Segment.h"

namespace Maths
{
  class DistancePointToSegment :public Distance
  {
  public:
    DistancePointToSegment(const Point& point, const Segment& segment);

    const Point& GetPoint(void) const;
    const Segment& GetSegment(void) const;
    float GetSegmentParameter(void) const;

    //void Draw(bool calc = false, ColorPtr one = nullptr, ColorPtr two = nullptr, ColorPtr seg = nullptr) override;

    virtual float GetDistance(void) override;
    virtual float GetDistanceSquared(void) override;
    virtual float GetDistance(float t, const Vector& vel0, const Vector& vel1) override;
    virtual float GetDistanceSquared(float t, const Vector& vel0, const Vector& vel1) override;

  private:
    const Point* m_point;
    const Segment* m_segment;
    float m_segmentParam;
  };
}// namespace Maths
