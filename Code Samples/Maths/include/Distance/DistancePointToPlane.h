#pragma once
#include "Distance/Distance.h"
#include "Geometry/Plane.h"

namespace Maths
{
  class DistancePointToPlane :public Distance
  {
  public:
    DistancePointToPlane(const Point& point, const Plane& plane);

    const Point& GetPoint(void) const;
    const Plane& GetPlane(void) const;

    //void Draw(bool calc = false, ColorPtr one = nullptr, ColorPtr two = nullptr, ColorPtr seg = nullptr) override;

    virtual float GetDistance(void) override;
    virtual float GetDistanceSquared(void) override;
    virtual float GetDistance(float t, const Vector& vel0, const Vector& vel1) override;
    virtual float GetDistanceSquared(float t, const Vector& vel0, const Vector& vel1) override;

  private:
    const Point* m_point;
    const Plane* m_plane;
  };
}// namespace Maths
