#pragma once

class ICommonCamera
{
public:
  virtual ~ICommonCamera() {}
  virtual void GetCameraProjectionMatrix(float m[16])const = 0;
  virtual void GetCameraViewMatrix(float m[16])const = 0;

  virtual void SetVRCamera() = 0;
  virtual void DisableVRCamera() = 0;
  virtual bool IsVRCamera() const = 0;
  virtual void SetVRCameraOffsetTransform(const float offset[16]) = 0;

  virtual void GetCameraTargetPosition(float pos[3]) const = 0;
  virtual void GetCameraPosition(float pos[3]) const = 0;

  virtual void GetCameraTargetPosition(double pos[3]) const = 0;
  virtual void GetCameraPosition(double pos[3]) const = 0;

  virtual void	SetCameraTargetPosition(float x, float y, float z) = 0;
  virtual void	SetCameraDistance(float dist) = 0;
  virtual float	GetCameraDistance() const = 0;

  virtual void	SetCameraUpVector(float x, float y, float z) = 0;
  virtual	void	GetCameraUpVector(float up[3]) const = 0;
  virtual void	GetCameraForwardVector(float fwd[3]) const = 0;

  ///the setCameraUpAxis will call the 'setCameraUpVector' and 'setCameraForwardVector'
  virtual void	SetCameraUpAxis(int axis) = 0;
  virtual int		GetCameraUpAxis() const = 0;

  virtual void	SetCameraYaw(float yaw) = 0;
  virtual float	GetCameraYaw() const = 0;

  virtual void	SetCameraPitch(float pitch) = 0;
  virtual float	GetCameraPitch() const = 0;

  virtual void	SetAspectRatio(float ratio) = 0;
  virtual float	GetAspectRatio() const = 0;

  virtual float GetCameraFrustumNear() const = 0;
  virtual float GetCameraFrustumFar() const = 0;
};
