#pragma once

#include "Matrix3.h"

namespace Maths
{
  class Transform
  {
  private:
    Matrix3 m_basis;
    Vector m_origin;

  public:
    Transform() {}

    explicit inline Transform(const Quaternion& q, const Vector& o = Vector(float(0), float(0), float(0))) :
      m_basis(q), m_origin(o)
    {}
    
    explicit inline Transform(const Matrix3& m, const Vector& o = Vector(float(0), float(0), float(0))) :
      m_basis(m), m_origin(o)
    {}


    inline Transform(const Transform& other)
      : m_basis(other.m_basis),
      m_origin(other.m_origin)
    {}
    
    inline Transform& operator=(const Transform& other)
    {
      m_basis = other.m_basis;
      m_origin = other.m_origin;
      return *this;
    }

    inline void Mult(const Transform& t1, const Transform& t2)
    {
      m_basis = t1.m_basis * t2.m_basis;
      m_origin = t1(t2.m_origin);
    }

    inline Vector operator()(const Vector& x) const
    {
      return x.Dot3(m_basis[0], m_basis[1], m_basis[2]) + m_origin;
    }

    // Return the transform of the vector
    inline Vector operator*(const Vector& x) const
    {
      return (*this)(x);
    }

    // Return the transform of the btQuaternion 
    inline Quaternion operator*(const Quaternion& q) const
    {
      return GetRotation() * q;
    }

    // Return the basis matrix for the rotation
    inline Matrix3& GetBasis()             { return m_basis; }
    inline const Matrix3& GetBasis() const { return m_basis; }
    // Return the origin vector translation
    inline Vector& GetOrigin()             { return m_origin; }
    inline const Vector& GetOrigin() const { return m_origin; }

    Quaternion GetRotation() const 
    {
      Quaternion q;
      m_basis.GetRotation(q);
      return q;
    }

    void SetFromOpenGLMatrix(const float* m)
    {
      m_basis.SetFromOpenGLSubMatrix(m);
      m_origin.SetValue(m[12], m[13], m[14]);
    }

    void GetOpenGLMatrix(float* m) const
    {
      m_basis.GetOpenGLSubMatrix(m);
      m[12] = m_origin.x;
      m[13] = m_origin.y;
      m[14] = m_origin.z;
      m[15] = float(1.0);
    }

    inline void SetOrigin(const Vector& origin)
    {
      m_origin = origin;
    }

    inline Vector InvXform(const Vector& inVec) const;

    inline void SetBasis(const Matrix3& basis)
    {
      m_basis = basis;
    }
    inline void SetRotation(const Quaternion& q)
    {
      m_basis.SetRotation(q);
    }
    
    void SetIdentity()
    {
      m_basis.SetIdentity();
      m_origin.SetValue(float(0.0), float(0.0), float(0.0));
    }

    Transform& operator*=(const Transform& t)
    {
      m_origin += m_basis * t.m_origin;
      m_basis *= t.m_basis;
      return *this;
    }
    Transform Inverse() const
    {
      Matrix3 inv = m_basis.Transpose();
      return Transform(inv, inv * -m_origin);
    }

    // Return the inverse of this transform times the other transform
    Transform InverseTimes(const Transform& t) const;
    Transform operator*(const Transform& t) const;

    static const Transform&	GetIdentity()
    {
      static const Transform identityTransform(Matrix3::GetIdentity());
      return identityTransform;
    }
  };


  inline Vector Transform::InvXform(const Vector& inVec) const
  {
    Vector v = inVec - m_origin;
    return (m_basis.Transpose() * v);
  }

  inline Transform Transform::InverseTimes(const Transform& t) const
  {
    Vector v = t.GetOrigin() - m_origin;
    return Transform(m_basis.TransposeTimes(t.m_basis), v * m_basis);
  }

  inline Transform Transform::operator*(const Transform& t) const
  {
    return Transform(m_basis * t.m_basis, (*this)(t.m_origin));
  }

  // Test if two transforms have all elements equal 
  inline bool operator==(const Transform& t1, const Transform& t2)
  {
    return (t1.GetBasis() == t2.GetBasis() && t1.GetOrigin() == t2.GetOrigin());
  }

}// namespace Maths
