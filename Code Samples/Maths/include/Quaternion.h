#pragma once
#include <ostream>// friend ostream

namespace Maths
{
  class Matrix4;
  class Vector;

  class Quaternion
  {
  public:
    Quaternion();
    Quaternion(float ww, float xx, float yy, float zz);
    // creat quaternion from rotations on the three cardinal axes (X,Y,Z)
    Quaternion(float x, float y, float z);
    // creat quaternion from a rotation matrix
    Quaternion(const Matrix4& rotation);
    // creat quaternion from an axis and angle
    Quaternion(const Vector& axis, float angle);

    float Length()const;
    float SquaredLength()const;
    float Dot(const Quaternion& rhs)const;
    // normalizes this quaternion
    float Normalize();
    // returns the inverse of this quaternion
    // takes the form (w-x-y-z) / SquaredLength()
    Quaternion Inverse()const;
    // returns the conjugate of this quaternion
    // takes the form (w-x-y-z)
    Quaternion Conjugate()const;
    void AddScaledVector(const Vector& vec, float scale);
    // assignmen operator
    Quaternion& operator=(const Quaternion &rhs);
    //Tests whether this quaternion is equal to another quaternion
    bool operator==(const Quaternion &rhs) const;
    //Tests whether this quaternion is not equal to another quaternion
    bool operator!=(const Quaternion &rhs) const;
    //Tests whether this quaternion is less than another quaternion
    bool operator<(const Quaternion &rhs) const;
    //Tests whether this quaternion is less than or equal to another quaternion
    bool operator<=(const Quaternion &rhs) const;
    //Tests whether this quaternion is greater than another quaternion
    bool operator>(const Quaternion &rhs) const;
    //Tests whether this quaternion is greater than or equal to another quaternion
    bool operator>=(const Quaternion &rhs) const;
    //Returns the result of adding a quaternion to this quaternion
    Quaternion operator+(const Quaternion &rhs) const;
    //Returns the result of subtracting a quaternion from this quaternion
    Quaternion operator-(const Quaternion &rhs) const;
    //Returns a quaternion that is equivalent to a rotation by rhs followed by a
    //rotation by *this
    Quaternion operator*(const Quaternion &rhs) const;
    //Returns the result of rotating a vector by this quaternion
    Vector operator*(const Vector& vec) const;
    //Returns the result of multiplying a scalar by this quaternion
    Quaternion operator*(float scalar) const;
    //Returns the result of dividing this quaternion by a scalar
    Quaternion operator/(float scalar) const;
    //Returns the result of negating this quaternion
    Quaternion operator-(void) const;
    //Adds a quaternion to this quaternion
    Quaternion& operator+=(const Quaternion &rhs);
    //Subtracts a quaternion from this quaternion
    Quaternion& operator-=(const Quaternion &rhs);
    //Multiplies this quaternion by a scalar
    Quaternion& operator*=(float scalar);
    //Divides this quaternion by a scalar
    Quaternion& operator/=(float scalar);
    // Set values with w set to 0
    void SetValue(const float& _x, const float& _y, const float& _z);
    // Set values
    void SetValue(const float& _x, const float& _y, const float& _z, const float& _w);
    Quaternion& SetRotationX(float radians);
    //Sets this quaternion to match a rotation about Y-axis by the specified radians
    Quaternion& SetRotationY(float radians);
    //Sets this quaternion to match a rotation about Z-axis by the specified radians
    Quaternion& SetRotationZ(float radians);
    // Sets this quaternion to match a rotation about X-axis by the specified radians
    // and the Y-axis by the specified radians
    Quaternion& SetRotationXY(float xRads, float yRads);
    //Sets this quaternion to match a rotation about X-axis by the specified radians
    //and the Z-axis by the specified radians
    Quaternion& SetRotationXZ(float xRads, float zRads);
    //Sets this quaternion to match a rotation about Y-axis by the specified radians
    //and the Z-axis by the specified radians
    Quaternion& SetRotationYZ(float yRads, float zRads);
    //Sets this quaternion to match a rotation about X-axis by the specified radians
    //then the Y-axis by the specified radians, then Z-axis by the specified radians
    Quaternion& SetRotationXYZ(float xRads, float yRads, float zRads);
    //Sets this quaternion to the equivalent of the specified Euler angles
    // pitch is rotation about x, yaw is rotation about y, roll is rotation about z
    Quaternion& FromEulerAngles(float pitch, float yaw, float roll, bool radians = true);
    //Extracts the Euler angles from the quaternion
    //Vector is returned as Vector(pitch, yaw, roll, 0)
    Vector GetEulerAngles(bool asRadians = true) const;
    //Sets the components of this quaternion to match the rotation represented
    //by a matrix
    void FromRotationMatrix(const Matrix4& rotation);
    //Sets a matrix to match the rotation represented by this quaternion
    void ToRotationMatrix(Matrix4& rotation) const;
    //Sets the components of this quaternion to match the rotation represented
    //by an axis and angle
    void FromAxisAngle(const Vector& axis, float angle);
    //Sets an axis and angle to match the rotation represented by this quaternion
    void ToAxisAngle(Vector& axis, float& angle) const;
    //Returns the result of rotating a vector by this quaternion
    Vector Rotate(const Vector& vec) const;
    //Spherical linear interpolation from start to end quaternions by t
    Quaternion& Slerp(float t, const Quaternion& start, const Quaternion& end);
    //Spherical quadratic interpolation of quaternions
    Quaternion& Squerp(float t, const Quaternion &q0, const Quaternion &a0, const Quaternion &a1, const Quaternion &q1);

    friend Quaternion operator*(float scalar, const Quaternion& rhs);
    //Prints in the quaternion in the form:
    //(w, x, y, z)
    friend std::ostream& operator<<(std::ostream &os, const Quaternion& vec);
    //Returns the quaternion that rotates one vector to another
    static Quaternion VectorToVector(const Vector& vec1, const Vector& vec2);

    static const Quaternion ZERO;
    static const Quaternion IDENTITY;

  public:
    union
    {
      // x,y,z are vector, w is a scalar
      struct { float w, x, y, z; };
      float v[4];
    };
  };

}
