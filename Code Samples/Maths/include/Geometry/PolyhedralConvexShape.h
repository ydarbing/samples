#pragma once
#include "ConvexShape.h"
#include "AabbUtil.h"


namespace Maths
{

  class ConvexPolyhedron;

  class PolyhedralConvexShape : public ConvexShape
  {
  protected:
    ConvexPolyhedron* m_polyhedron;

  public:
    PolyhedralConvexShape();
    virtual ~PolyhedralConvexShape();

    const ConvexPolyhedron* GetConvexPolyhedron() const {
      return m_polyhedron;
    }


    virtual Vector LocalGetSupportingVertexWithoutMargin(const Vector& vec)const;
    virtual void	BatchedUnitVectorGetSupportingVertexWithoutMargin(const Vector* vectors, Vector* supportVerticesOut, int numVectors) const;

    virtual void	CalculateLocalInertia(float mass, Vector& inertia) const;


    virtual int	GetNumVertices() const = 0;
    virtual int GetNumEdges() const = 0;
    virtual void GetEdge(int i, Vector& pa, Vector& pb) const = 0;
    virtual void GetVertex(int i, Vector& vtx) const = 0;
    virtual int	GetNumPlanes() const = 0;
    virtual void GetPlane(Vector& planeNormal, Vector& planeSupport, int i) const = 0;
    //	virtual int getIndex(int i) const = 0 ; 

    virtual	bool IsInside(const Vector& pt, float tolerance) const = 0;
  };

  class PolyhedralConvexAabbCachingShape : PolyhedralConvexShape
  {
    Vector m_localAabbMin;
    Vector m_localAabbMax;
    bool   m_isLocalAabbValid;

  protected:

    void SetCachedLocalAabb(const Vector& aabbMin, const Vector& aabbMax)
    {
      m_isLocalAabbValid = true;
      m_localAabbMin = aabbMin;
      m_localAabbMax = aabbMax;
    }

    inline void GetCachedLocalAabb(Vector& aabbMin, Vector& aabbMax) const
    {
      assert(m_isLocalAabbValid);
      aabbMin = m_localAabbMin;
      aabbMax = m_localAabbMax;
    }

  protected:
    PolyhedralConvexAabbCachingShape();

  public:
    inline void GetNonVirtualAabb(const Transform& trans, Vector& aabbMin, Vector& aabbMax, float margin) const
    {
      assert(m_isLocalAabbValid);
      TransformAabb(m_localAabbMin, m_localAabbMax, margin, trans, aabbMin, aabbMax);
    }

    virtual void	SetLocalScaling(const Vector& scaling);

    virtual void GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const;

    void	RecalcLocalAabb();
  };

}// namespace Maths
