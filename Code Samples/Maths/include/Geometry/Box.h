#pragma once

#include "AabbUtil.h"

#include "Geometry/PolyhedralConvexShape.h"
#include "BroadphaseProxy.h"
#include "Geometry/Point.h"
#include "Vector.h"
#include "Utils.h" // Min/Max functions

namespace Maths
{
  // Axis Aligned Bounding Box
  class Box : public PolyhedralConvexShape
  {
    // using Vector m_implicitShapeDimensions for shape

  public:


    Vector GetHalfExtentsWithMargin()const
    {
      Vector halfExtents = GetHalfExtentsWithoutMargin();
      Vector margin(GetMargin(), GetMargin(), GetMargin());
      halfExtents += margin;
      return halfExtents;
    }


    const Vector& GetHalfExtentsWithoutMargin() const
    {
      return m_implicitShapeDimensions;//scaling is included, margin is not
    }

    virtual Vector LocalGetSupportingVertex(const Vector& vec) const
    {
      Vector halfExtents = GetHalfExtentsWithoutMargin();
      Vector margin(GetMargin(), GetMargin(), GetMargin());
      halfExtents += margin;

      return Vector(Fsels(vec.x, halfExtents.x, -halfExtents.x),
                    Fsels(vec.y, halfExtents.y, -halfExtents.y),
                    Fsels(vec.z, halfExtents.z, -halfExtents.z));
    }


    virtual void BatchedUnitVectorGetSupportingVertexWithoutMargin(const Vector* vectors, Vector* supportVerticesOut, int numVectors) const
    {
      const Vector& halfExtents = GetHalfExtentsWithoutMargin();

      for (int i = 0; i < numVectors; ++i)
      {
        const Vector& vec = vectors[1];
        supportVerticesOut[i].SetValue(Fsels(vec.x, halfExtents.x, -halfExtents.x),
                                       Fsels(vec.y, halfExtents.y, -halfExtents.y),
                                       Fsels(vec.z, halfExtents.z, -halfExtents.z));
      }
    }





    Box(const Vector& boxHalfExtents);

    virtual void SetMargin(float collisionMargin)
    {
      Vector oldMargin(GetMargin(), GetMargin(), GetMargin());
      Vector implicitShapeDimensionsWithMargin = m_implicitShapeDimensions + oldMargin;

      ConvexShape::SetMargin(collisionMargin);
      Vector newMargin(GetMargin(), GetMargin(), GetMargin());
      m_implicitShapeDimensions = implicitShapeDimensionsWithMargin - newMargin;
    }


    virtual void SetLocalScaling(const Vector& scaling)
    {
      Vector oldMargin(GetMargin(), GetMargin(), GetMargin());
      Vector implicitShapeDimensionsWithMargin = m_implicitShapeDimensions + oldMargin;
      Vector unscaledImplicitShapeDimensionsWithMargin = implicitShapeDimensionsWithMargin / m_localScaling;

      ConvexShape::SetLocalScaling(scaling);

      m_implicitShapeDimensions = (unscaledImplicitShapeDimensionsWithMargin * m_localScaling) - oldMargin;
    }


    virtual void GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const;

    virtual void CalculateLocalInertia(float mass, Vector& inertia)const;

    virtual void GetPlane(Vector& planeNormal, Vector& planeSupport, int i) const
    {
      Vector plane;
      GetPlaneEquation(plane, i);
      planeNormal = Vector(plane.x, plane.y, plane.z);
      planeSupport = LocalGetSupportingVertex(-planeNormal);
    }

    virtual int GetNumPlanes() const
    {
      return 6;
    }

    virtual int GetNumVertices() const
    {
      return 8;
    }

    virtual int GetNumEdges() const
    {
      return 12;
    }

    virtual void GetVertex(int i, Vector& vtx) const
    {
      Vector halfExtents = GetHalfExtentsWithMargin();

      vtx = Vector(halfExtents.x * (1 - (i & 1)) - halfExtents.x * (i & 1),
                   halfExtents.y * (1 - ((i & 2) >> 1)) - halfExtents.y * ((i & 2) >> 1),
                   halfExtents.z * (1 - ((i & 4) >> 2)) - halfExtents.z * ((i & 4) >> 2));
    }


    virtual void GetPlaneEquation(Vector& plane, int i) const
    {
      Vector halfExtents = GetHalfExtentsWithoutMargin();

      switch (i)
      {
      case 0:
        plane.SetValue(float(1.), float(0.), float(0.), -halfExtents.x);
        break;
      case 1:
        plane.SetValue(float(-1.), float(0.), float(0.), -halfExtents.x);
        break;
      case 2:
        plane.SetValue(float(0.), float(1.), float(0.), -halfExtents.y);
        break;
      case 3:
        plane.SetValue(float(0.), float(-1.), float(0.), -halfExtents.y);
        break;
      case 4:
        plane.SetValue(float(0.), float(0.), float(1.), -halfExtents.z);
        break;
      case 5:
        plane.SetValue(float(0.), float(0.), float(-1.), -halfExtents.z);
        break;
      default:
        assert(0);
      }
    }

    virtual void GetEdge(int i, Vector& pa, Vector& pb)const
    {
      int edgeVert0 = 0;
      int edgeVert1 = 0;

      switch (i)
      {
      case 0:
        edgeVert0 = 0; edgeVert1 = 1;
        break;
      case 1:
        edgeVert0 = 0; edgeVert1 = 2;
        break;
      case 2:
        edgeVert0 = 1; edgeVert1 = 3;
        break;
      case 3:
        edgeVert0 = 2; edgeVert1 = 3;
        break;
      case 4:
        edgeVert0 = 0; edgeVert1 = 4;
        break;
      case 5:
        edgeVert0 = 1; edgeVert1 = 5;
        break;
      case 6:
        edgeVert0 = 2; edgeVert1 = 6;
        break;
      case 7:
        edgeVert0 = 3; edgeVert1 = 7;
        break;
      case 8:
        edgeVert0 = 4; edgeVert1 = 5;
        break;
      case 9:
        edgeVert0 = 4; edgeVert1 = 6;
        break;
      case 10:
        edgeVert0 = 5; edgeVert1 = 7;
        break;
      case 11:
        edgeVert0 = 6; edgeVert1 = 7;
        break;
      default:
        assert(0);

      }

      GetVertex(edgeVert0, pa);
      GetVertex(edgeVert1, pb);
    }


    virtual bool IsInside(const Vector& pt, float tolerance) const
    {
      Vector halfExtents = GetHalfExtentsWithoutMargin();

      bool result = (pt.x <= ( halfExtents.x + tolerance)) &&
                    (pt.x >= (-halfExtents.x - tolerance)) &&
                    (pt.y <= ( halfExtents.y + tolerance)) &&
                    (pt.y >= (-halfExtents.y - tolerance)) &&
                    (pt.z <= ( halfExtents.z + tolerance)) &&
                    (pt.z >= (-halfExtents.z - tolerance));

      return result;
    }

    virtual const std::string GetName() const
    {
      return "Box";
    }

    virtual int getNumPreferredPenetrationDirections() const
    {
      return 6;
    }

    virtual void	getPreferredPenetrationDirection(int index, Vector& penetrationVector) const
    {
      switch (index)
      {
      case 0:
        penetrationVector.SetValue(float(1.), float(0.), float(0.));
        break;
      case 1:
        penetrationVector.SetValue(float(-1.), float(0.), float(0.));
        break;
      case 2:
        penetrationVector.SetValue(float(0.), float(1.), float(0.));
        break;
      case 3:
        penetrationVector.SetValue(float(0.), float(-1.), float(0.));
        break;
      case 4:
        penetrationVector.SetValue(float(0.), float(0.), float(1.));
        break;
      case 5:
        penetrationVector.SetValue(float(0.), float(0.), float(-1.));
        break;
      default:
        assert(0);
      }
    }
  };

}

