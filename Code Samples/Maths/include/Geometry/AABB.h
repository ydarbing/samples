#pragma once

#include "Geometry/Geometry.h"
#include "Geometry/Point.h"
#include "Vector.h"
#include "Utils.h" // Min/Max functions

namespace Maths
{
  // Axis Aligned Bounding Box
  class AABB : public Geometry
  {
  public:
    AABB(void);
    AABB(float xMin, float xMax, float yMin, float yMax, float zMin, float zMax);
    AABB(const Point& min_, const Point& max_);
    AABB(const Point& center_, float xExtent, float yExtent, float zExtent);

    void GetCenterExtents(Point& center, float& xExtent, float& yExtent, float& zExtent) const;
    bool HasXOverlap(const AABB& rhs) const;
    bool HasYOverlap(const AABB& rhs) const;
    bool HasZOverlap(const AABB& rhs) const;
    bool TestIntersection(const AABB& rhs) const;
    bool FindIntersection(const AABB& rhs, AABB& intersect) const;


    inline float SurfaceArea()const;
    inline bool Contains(const AABB& other)const;
    inline bool Contains(const Point& other)const;
    inline const AABB Combine(const AABB& other);
    inline void Stretch(const Vector& displacement, const float stretchBy = 2.0f);
    inline void Expand(const float expandBy = 0.5f);
    inline void Shrink(const float shrinkBy = 0.5f);

	virtual void SetLocalScaling(const Vector& scaling) override;
	virtual const Vector& GetLocalScaling() const override;
	virtual const std::string GetName() const override;
	////virtual void Draw(ColorPtr rgba = nullptr) const override;
	virtual Point GetPosition(void) const override;
	virtual void SetPosition(const Point& pos) override;
  public:
    Point m_min;
    Point m_max;

  private: 
	Vector m_localScaling;

	private: // not implemented base functions
	// Inherited via Geometry
	virtual void GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const override;
	virtual void CalculateLocalInertia(float mass, Vector& inertia) const override;
	virtual void SetMargin(float margin) override;
	virtual float GetMargin() const override;
  };

}// Maths namespace


#include "Geometry/AABB.hpp"

