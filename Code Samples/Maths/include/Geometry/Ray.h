#pragma once

#include "Geometry.h"

#include "Point.h"
#include "Vector.h"

namespace Maths
{
  class Point;
  class Vector;

  class Ray //: public Geometry
  {
  public:
    Ray(void);
    Ray(const Point& origin_, const Vector& dir_);

    Point GetPosition(void)const;
    void SetPosition(const Point& pos);
    void Draw(ColorPtr rgba = nullptr)const;
    const std::string GetName(void)const;

    static const Ray X_AXIS;
    static const Ray Y_AXIS;
    static const Ray Z_AXIS;

  public:
    Point m_origin;
    Vector m_dir;
  };
}// namespace Maths

