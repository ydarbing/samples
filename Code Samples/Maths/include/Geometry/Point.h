#pragma once

#include "Color.h"

#include <ostream>

namespace Maths
{
  class Vector;// forward declaration 

  class Point
  {
  public:
    Point();
    Point(float x_, float y_, float z_, float w_ = 1.0f);
    Point AddPoint(const Point& rhs) const;
    Point GetMidpoint(const Point& rhs) const;

    // Returns the cross product of two points, no in - place modification
    // Because the cross product of two points is mathematically meaningless, this must be
    // an explicit function call
    Point CrossPoint(const Point& rhs) const;

    // Returns dot product of two points, no in - place modification
    // Because dot product of two points is mathematically meaningless, this must be
    // an explicit function call
    float DotPoint(const Point& rhs) const;

    Point operator-(void) const;
    Vector operator-(const Point& rhs) const;
    Point operator+(const Vector& rhs) const;
    Point operator-(const Vector& rhs) const;
    Point operator*(float scalar) const;
    Point operator/(float scalar) const;
    Point& operator+=(const Vector& rhs);
    Point& operator-=(const Vector& rhs);
    Point& operator*=(float scalar);
    Point& operator/=(float scalar);
    bool operator==(const Point& rhs) const;
    bool operator!=(const Point& rhs) const;
    bool operator<(const Point& rhs) const;
    bool operator<=(const Point& rhs) const;
    bool operator>(const Point& rhs) const;
    bool operator>=(const Point& rhs) const;
    friend std::ostream& operator<<(std::ostream &os, const Point& p);

    virtual Point GetPosition(void) const;
    virtual void SetPosition(const Point& pos);
    //virtual void Draw(ColorPtr rgba = nullptr) const;
    virtual std::string GetName() const;

    static bool Test(void);

    static const Point ORIGIN;

  public:
    union
    {
      struct { float x, y, z, w; };
      float v[4];
    };
  };
}// namespace Maths
