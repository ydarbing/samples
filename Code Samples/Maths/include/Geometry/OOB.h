#pragma once

#include "Geometry/Geometry.h"
#include "Geometry/Point.h"
#include "Vector.h"

namespace Maths
{
  // object oriented box
  class OOB : public Geometry
  {
  public:
    OOB(void);
    OOB(const Point& cent, const Vector& ax0, const Vector& ax1, const Vector& ax2, float ext0, float ext1, float ext2);
    OOB(const Point& cent, const Vector axes[3], const float exts[3]);

    void ComputeVertices(Point points[8]) const;

    virtual Point GetPosition(void) const;
    virtual void SetPosition(const Point& pos);
    //virtual void Draw(ColorPtr rgba = nullptr) const;
    virtual const std::string GetName() const;

  public:
    Point m_center;
    Vector m_axis[3];
    float m_extent[3];

  private:
	Vector m_localScaling;

  private: // not implemented base functions
	// Inherited via Geometry
	virtual void GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const override;
	virtual void SetLocalScaling(const Vector& scaling) override;
	virtual const Vector& GetLocalScaling() const override;
	virtual void CalculateLocalInertia(float mass, Vector& inertia) const override;
	virtual void SetMargin(float margin) override;
	virtual float GetMargin() const override;
  };

}// Maths namespace
