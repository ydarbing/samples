#pragma once

#include "Geometry/Geometry.h"
#include "Geometry/Point.h"
#include "Vector.h"

namespace Maths
{
  class Circle : public Geometry
  {
  public:
    Circle(void);
    Circle(const Point& center, const Vector& norm, float rad);
    Circle(const Point& center, const Vector& axis1, const Vector& axis2, float rad);

    virtual Point GetPosition(void) const;
    virtual void SetPosition(const Point& pos);
    //virtual void Draw(ColorPtr rgba = nullptr) const;
    virtual const std::string GetName() const;

  public:
    Point m_center;
    Vector m_normal;
    Vector m_dir0;
    Vector m_dir1;
    float m_radius;

  private: // not implemented base functions
	  Vector m_localScaling;

	// Inherited via Geometry
	virtual void GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const override;
	virtual void SetLocalScaling(const Vector& scaling) override;
	virtual const Vector& GetLocalScaling() const override;
	virtual void CalculateLocalInertia(float mass, Vector& inertia) const override;
	virtual void SetMargin(float margin) override;
	virtual float GetMargin() const override;
  };

}// Maths namespace
