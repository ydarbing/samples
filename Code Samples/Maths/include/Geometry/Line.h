#pragma once

#include "Geometry.h"
#include "Color.h"
#include "Vector.h"
#include "Point.h"

namespace Maths
{
	// infinite line that passes though point with a given vector direction (as opposed to a segment)
  class Line : public Geometry
  {
  public:
    Line(void);
    Line(const Point& point_, const Vector& dir);

	Vector GetDirection(void)const;
	void SetDirection(Vector direction);

    virtual Point GetPosition(void) const override;
    virtual void SetPosition(const Point& pos)override;
    ////virtual void Draw(ColorPtr rgba = nullptr) const override;
    virtual const std::string GetName() const override;

	virtual void SetLocalScaling(const Vector& scaling) override;
	virtual const Vector& GetLocalScaling() const override;
  public:
    Point point;
    Vector direction;
	Vector localScaling;


	// Inherited via Geometry and not used
  private:
	virtual void GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const override;
	virtual void CalculateLocalInertia(float mass, Vector& inertia) const override;
	virtual void SetMargin(float margin) override;
	virtual float GetMargin() const override;
  };
}// namespace Maths
