#pragma once

#include "Geometry/Geometry.h"
#include "Geometry/Point.h"

namespace Maths
{

  class ConcaveShape : public Geometry
  {
  public:
    ConcaveShape(void);

    virtual Point GetPosition(void) const;
    virtual void SetPosition(const Point& pos);
    virtual const std::string GetName() const;
    virtual void GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const;
    virtual void GetBoundingSphere(Vector& center, float& radius) const;
    virtual void	SetLocalScaling(const Vector& scaling);
    virtual const Vector& getLocalScaling() const;
  };

}// namespace Maths
