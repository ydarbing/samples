#pragma once
#include "Polyhedron.h"
#include "Vector.h"
#include <set>

namespace Maths
{
  class Vector;
  class Point;

  class ConvexPolyhedron : public Polyhedron
  {
  public:
    typedef std::pair<Vector, float> NCPlane;

    ConvexPolyhedron(std::vector<Point> verts, unsigned numTris, std::vector<unsigned> indices, NCPlane *planes);
    ConvexPolyhedron(const ConvexPolyhedron &other);
    virtual ~ConvexPolyhedron(void);

    ConvexPolyhedron& operator=(const ConvexPolyhedron &rhs);

    const NCPlane* GetPlanes(void) const;
    const NCPlane& GetPlane(unsigned i) const;
    virtual void SetVertex(unsigned i, const Point& vertex);
    void UpdatePlanes(void);
    bool IsConvex(float threshold = 0.0f) const;
    bool Contains(const Point& p, float threshold = 0.0f) const;

    //virtual void Draw(ColorPtr rgba = nullptr) const;
    virtual const std::string GetName() const override;

  protected:
    NCPlane*           m_planes;
    std::set<unsigned> m_sharingTriangles;

    void UpdatePlane(unsigned i, const Point &average);
  };

}// Math namespace
