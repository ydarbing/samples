#include "AABB.h"


namespace Maths
{
  inline void AABB::Stretch(const Vector& displacement, const float stretchBy /*= 2.0f*/)
  {
    Vector d = displacement * stretchBy;

    if (d.x < 0.0f)
      m_min.x += d.x;
    else
      m_max.x += d.x;

    if (d.y < 0.0f)
      m_min.y += d.y;
    else
      m_max.y += d.y;

    if (d.z < 0.0f)
      m_min.z += d.z;
    else
      m_max.z += d.z;
  }

  inline void AABB::Expand(const float expandBy /*= 0.5*/)
  {
    Vector vec(expandBy, expandBy, expandBy);

    m_min -= vec;
    m_max += vec;
  }

  inline void AABB::Shrink(const float shrinkBy /*= 0.5f*/)
  {
    Vector vec(shrinkBy, shrinkBy, shrinkBy);

    m_min += vec;
    m_max -= vec;
  }

  inline const AABB AABB::Combine(const AABB& other)
  {
    AABB ret;

    ret.m_min = Min(m_min, other.m_min);
    ret.m_max = Max(m_max, other.m_max);

    return ret;
  }

  inline bool AABB::Contains(const AABB& other)const
  {
    return
      m_min.x <= other.m_min.x &&
      m_min.y <= other.m_min.y &&
      m_min.z <= other.m_min.z &&
      m_max.x >= other.m_max.x &&
      m_max.y >= other.m_max.y &&
      m_max.z >= other.m_max.z;
  }

  inline bool AABB::Contains(const Point& other) const
  {
    return
      m_min.x <= other.x &&
      m_min.y <= other.y &&
      m_min.z <= other.z &&
      m_max.x >= other.x &&
      m_max.y >= other.y &&
      m_max.z >= other.z;
  }

  inline float AABB::SurfaceArea() const
  {
    float x = m_max.x - m_min.x;
    float y = m_max.y - m_min.y;
    float z = m_max.z - m_min.z;

    return 2.0f * (x*y + x*z + y*z);
  }
}// namespace Maths