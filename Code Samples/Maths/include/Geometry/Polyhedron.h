#pragma once
#include "Geometry.h"
#include <vector>

#include <set>

namespace Maths
{
  class Vector;
  class Point;

  class Polyhedron : public Geometry
  {
  public:
    Polyhedron(std::vector<Point> verts, unsigned numTris, std::vector<unsigned> indices);
    Polyhedron(const Polyhedron& other);
    virtual ~Polyhedron(void);

    Polyhedron& operator=(const Polyhedron& rhs);

    unsigned GetVertexCount(void) const;
    const std::vector<Point>& GetVertices(void) const;
    const Point& GetVertex(unsigned i) const;
    unsigned GetTriangleCount(void) const;
    unsigned GetIndexCount(void) const;
    const std::vector<unsigned>& GetIndices(void) const;
    const unsigned* GetTriangle(unsigned i) const;

    virtual void SetVertex(unsigned i, const Point& vertex);

    Point ComputeAverage(void) const;
    float ComputeSurfaceArea(void) const;
    float ComputeVolume(void) const;

	virtual Point GetPosition(void) const override;
	virtual void SetPosition(const Point& pos) override;
	//virtual void Draw(ColorPtr rgba = nullptr) const override;
	virtual const std::string GetName() const override;
	virtual void SetLocalScaling(const Vector& scaling) override;
	virtual const Vector& GetLocalScaling() const override;

  private:
    void ClearVertices(void);
  protected:
    //unsigned  m_numVertices;
    unsigned  m_numTriangles;
    //unsigned  m_numIndices;
    //Point*    m_vertices;
    //unsigned* m_indices;
    std::vector<Point> m_vertices;
    std::vector<unsigned> m_indices;



  private:
	  Vector m_localScaling;

  private: // not implemented base functions
  // Inherited via Geometry
	  virtual void GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const override;
	  virtual void CalculateLocalInertia(float mass, Vector& inertia) const override;
	  virtual void SetMargin(float margin) override;
	  virtual float GetMargin() const override;
  };

}// Math namespace