#pragma once

#include "Geometry/Geometry.h"
#include "Transform.h"
#include "Geometry/Point.h"

namespace Maths
{

  class ConvexShape : public Geometry
  {
  protected:
    Vector m_localScaling;
    Vector m_implicitShapeDimensions;
    float m_collisionMargin; // not scaled
    float m_padding;

    ConvexShape();

  public:
    virtual ~ConvexShape() { }

    const Vector& GetImplicitShapeDimensions()const
    {
      return m_implicitShapeDimensions;
    }

    void SetImplicitShapeDimensions(const Vector& dimens)
    {
      m_implicitShapeDimensions = dimens;
    }

    void SetSafeMargin(float minDim, float defaultMarginMult = 0.1f)
    {
      float safeMargin = defaultMarginMult * minDim;
      if (safeMargin < GetMargin())
      {
        SetMargin(safeMargin);
      }
    }

    void SetSafeMargin(const Vector& halfExtents, float defaultMarginMult = 0.1f)
    {
      float minDim = halfExtents[halfExtents.MinAxis()];
      SetSafeMargin(minDim, defaultMarginMult);
    }
    
    
    void GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const
    {
      GetAabbSlow(t, aabbMin, aabbMax);
    }

    virtual void GetAabbSlow(const Transform& t, Vector& aabbMin, Vector& aabbMax) const;


    virtual void SetLocalScaling(const Vector& scaling);
    virtual const Vector& GetLocalScaling() const
    {
      return m_localScaling;
    }
    const Vector& GetLocalScalingNonVirtual() const
    {
      return m_localScaling;
    }
    
    virtual void  SetMargin(float margin)
    {
      m_collisionMargin = margin;
    }
    virtual float GetMargin() const
    {
      return m_collisionMargin;
    }
	float GetMarginNonVirtual()const;


    virtual int  GetNumPreferredPenetrationDirections() const
    {
      return 0;
    }
    virtual void GetPreferredPenetrationDirection(int index, Vector& penetrationVector) const
    {
      assert(0);
    }


    virtual Vector LocalGetSupportingVertex(const Vector& vec) const;
    virtual Vector LocalGetSupportingVertexWithoutMargin(const Vector& vec) const;

    Vector LocalGetSupportVertexWithoutMarginNonVirtual(const Vector& vec) const;
    Vector LocalGetSupportVertexNonVirtual(const Vector& vec) const;
    void GetAabbNonVirtual(const Transform& t, Vector& aabbMin, Vector& aabbMax) const;

    // the vectors should be unit length
    virtual void BatchedUnitVectorGetSupportingVertexWithoutMargin(const Vector* vectors, Vector* supportVerticesOut, int numVectors) const;
    virtual void Project(const Transform& trans, const Vector& dir, float& minProj, float& maxProj, Vector& witnesPtMin, Vector& witnesPtMax) const;
    
    

    // Inherited via Geometry
    virtual Point GetPosition(void) const override;
    virtual void SetPosition(const Point & pos) override;
    virtual const std::string GetName() const override;
  };

}// namespace Maths
