#pragma once
#include "Color.h"
#include "BroadphaseProxy.h"
#include "Transform.h"

class Serializer;

namespace Maths
{
  class Point;
  // base class that provides interface for collision geometry
  class Geometry
  {
  protected:
    int m_shapeType;
    void* m_userPointer;
    int m_userIndex;

  public:
    Geometry(void) : m_shapeType(E_INVALID_SHAPE), m_userPointer(nullptr), m_userIndex(-1)
    {
    }

    virtual ~Geometry(void)
    {
    }

    virtual void GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const = 0;
    virtual void GetBoundingSphere(Vector& center, float& radius)const;

    // GetAngularMotionDisc returns maximum radius needed for conservative advancement to handle time-of-impact with rotations
    virtual float GetAngularMotionDisc() const;
    virtual float GetContactBreakingThreshold(float defaultContactThresholdFactor) const;
    // Calculate the enclosing aabb for the moving object over interval [0..timeStep)
    // result is conservative
    void CalculateTemporalAabb(const Transform& curTrans, const Vector& linvel, const Vector& angvel, float timeStep, Vector& temporalAabbMin, Vector& temporalAabbMax) const;

    inline bool	IsPolyhedral() const
    {
      return BroadphaseProxy::IsPolyhedral(GetShapeType());
    }

    inline bool	IsConvex2d() const
    {
      return BroadphaseProxy::IsConvex2d(GetShapeType());
    }

    inline bool	IsConvex() const
    {
      return BroadphaseProxy::IsConvex(GetShapeType());
    }
    inline bool	IsNonMoving() const
    {
      return BroadphaseProxy::IsNonMoving(GetShapeType());
    }
    inline bool	IsConcave() const
    {
      return BroadphaseProxy::IsConcave(GetShapeType());
    }
    inline bool	IsCompound() const
    {
      return BroadphaseProxy::IsCompound(GetShapeType());
    }

    inline bool	IsSoftBody() const
    {
      return BroadphaseProxy::IsSoftBody(GetShapeType());
    }

    // iIInfinite used to catch simulation error (aabb check)
    inline bool IsInfinite() const
    {
      return BroadphaseProxy::IsInfinite(GetShapeType());
    }


    virtual void	SetLocalScaling(const Vector& scaling) = 0;
    virtual const Vector& GetLocalScaling() const = 0;
    virtual void	CalculateLocalInertia(float mass, Vector& inertia) const = 0;

    virtual const std::string GetName() const = 0;

    int GetShapeType()const { return m_shapeType; }

    virtual Vector GetAnisotropicRollingFrictionDirection() const
    {
      return Vector(1, 1, 1);
    }
    virtual void	SetMargin(float margin) = 0;
    virtual float GetMargin() const = 0;


    void SetUserPointer(void* userPtr)
    {
      m_userPointer = userPtr;
    }

    void*	GetUserPointer() const
    {
      return m_userPointer;
    }
    void SetUserIndex(int index)
    {
      m_userIndex = index;
    }

    int GetUserIndex() const
    {
      return m_userIndex;
    }

    virtual	int	CalculateSerializeBufferSize() const;
    // Fills the dataBuffer and returns the struct name (and 0 on failure)
    virtual	const char*	Serialize(void* dataBuffer, Serializer* serializer) const;

    virtual void SerializeSingleShape(Serializer* serializer) const;

    virtual Point GetPosition(void) const = 0;
    virtual void SetPosition(const Point& pos) = 0;
    //virtual void Draw(ColorPtr rgba = nullptr) const = 0;
  };

  struct CollisionShapeData
  {
    char* m_name;
    int   m_shapeType;
    char  m_padding[4];
  };

  inline	int	Geometry::CalculateSerializeBufferSize() const
  {
    return sizeof(CollisionShapeData);
  }

}// Math namespace
