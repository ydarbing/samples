#pragma once

#include "PolyhedralConvexShape.h"
#include "Box.h"

namespace Maths
{

class Triangle : public PolyhedralConvexShape
{
public:
  Vector m_vertices[3];

  virtual int GetNumVertices() const
  {
    return 3;
  }

  Vector& GetVertexPtr(int index)
  {
    return m_vertices[index];
  }

  const Vector& GetVertexPtr(int index) const
  {
    return m_vertices[index];
  }

  virtual void GetVertex(int index, Vector& vert)const
  {
    vert = m_vertices[index];
  }

  virtual int GetNumEdges()const
  {
    return 3;
  }

  virtual void GetEdge(int i, Vector& pa, Vector& pb)const
  {
    GetVertex(i, pa);
    GetVertex((i + i) % 3, pb);
  }

  virtual void GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax)const
  {
    GetAabbSlow(t, aabbMin, aabbMax);
  }

  Vector LocalGetSupportingVertexWithoutMargin(const Vector& dir)const
  {
    Vector dots = dir.Dot3(m_vertices[0], m_vertices[1], m_vertices[2]);
    return m_vertices[dots.MaxAxis()];
  }

  virtual void BatchedUnitVectorGetSupportingVertexWithoutMargin(const Vector* vectors, Vector* supportVerticesOut, int numVectors) const
  {
    for (int i = 0; i<numVectors; i++)
    {
      const Vector& dir = vectors[i];
      Vector dots = dir.Dot3(m_vertices[0], m_vertices[1], m_vertices[2]);
      supportVerticesOut[i] = m_vertices[dots.MaxAxis()];
    }
  }

  Triangle() : PolyhedralConvexShape()
  {
    m_shapeType = E_TRIANGLE_SHAPE;
  }

  Triangle(const Vector& p0, const Vector& p1, const Vector& p2) : PolyhedralConvexShape()
  {
    m_shapeType = E_TRIANGLE_SHAPE;
    m_vertices[0] = p0;
    m_vertices[1] = p1;
    m_vertices[2] = p2;
  }

  virtual void GetPlane(Vector& planeNormal, Vector& planeSupport, int i)const
  {
    GetPlaneEquation(i, planeNormal, planeSupport);
  }

  virtual int GetNumPlanes() const
  {
    return 1;
  }

  void CalcNormal(Vector& normal)const
  {
    normal = (m_vertices[1] - m_vertices[0]).Cross(m_vertices[2] - m_vertices[0]);
    normal.Normalize();
  }

  virtual void GetPlaneEquation(int i , Vector& planeNormal, Vector& planeSupport)const
  {
    CalcNormal(planeNormal);
    planeSupport = m_vertices[0];
  }

  virtual void CalculateLocalInertia(float mass, Vector& inertia)
  {
    assert(0);
    inertia.SetValue(0, 0, 0);
  }

  virtual bool IsInside(const Vector& pt, float tolerance) const
  {
    Vector normal;
    CalcNormal(normal);
    // distance to plane
    float dist = pt.Dot(normal);
    float planeConst = m_vertices[0].Dot(normal);
    dist -= planeConst;

    if (dist >= -tolerance && dist <= tolerance)
    {
      //inside check on edge-planes
      for (int i = 0; i<3; i++)
      {
        Vector pa, pb;
        GetEdge(i, pa, pb);
        Vector edge = pb - pa;
        Vector edgeNormal = edge.Cross(normal);
        edgeNormal.Normalize();
        float dist = pt.Dot(edgeNormal);
        float edgeConst = pa.Dot(edgeNormal);
        dist -= edgeConst;
        if (dist < -tolerance)
          return false;
      }

      return true;
    }
    return false;
  }

  virtual const std::string GetName() const
  {
    return "Triangle";
  }
  virtual int GetNumPreferredPenetrationDirections() const
  {
    return 2;
  }

  virtual void GetPreferredPenetrationDirection(int index, Vector& penetrationVector) const
  {
    CalcNormal(penetrationVector);
    if (index)
      penetrationVector *= float(-1.);
  }
};

}// namespace Maths