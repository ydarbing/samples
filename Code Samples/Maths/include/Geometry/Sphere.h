#pragma once

#include "Geometry/Geometry.h"
#include "Geometry/Point.h"

namespace Maths
{
  class Sphere : public Geometry
  {
  public:
    Sphere(void);
    Sphere(Point center, float radius);

    virtual Point GetPosition(void) const override;
    virtual void SetPosition(const Point& pos) override;

  public:
    Point m_center;
    float m_radius;

	virtual const std::string GetName() const override;
	//virtual void Draw(ColorPtr rgba = nullptr) const override;
	virtual void SetLocalScaling(const Vector& scaling) override;
	virtual const Vector& GetLocalScaling() const override;

  private:
	  Vector m_localScaling;

  private: // not implemented base functions
  // Inherited via Geometry
	  virtual void GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const override;
	  virtual void CalculateLocalInertia(float mass, Vector& inertia) const override;
	  virtual void SetMargin(float margin) override;
	  virtual float GetMargin() const override;
  };

}// Maths namespace
