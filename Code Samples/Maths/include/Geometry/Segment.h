#pragma once

#include "Geometry\Geometry.h"
#include "Vector.h"
#include "Point.h"

namespace Maths
{
  class Segment : public Geometry
  {
  public:
    Segment(void);
    Segment(const Point& p0, const Point& p1);
    Segment(const Point& center_, const Vector& dir, float extent_);

    void ComputeCenterDirectionExtent(void);
    void ComputeEndPoints(void);

    virtual Point GetPosition(void) const override;
    virtual void SetPosition(const Point& pos) override;
    //virtual void Draw(ColorPtr rgba = nullptr) const override;
    virtual const std::string GetName() const override;
	virtual void SetLocalScaling(const Vector& scaling) override;
	virtual const Vector& GetLocalScaling() const override;

  public:
    Point point0;
    Point point1;
    Point center;
    Vector direction;
    float extent;



  private:
	  Vector m_localScaling;

  private: // not implemented base functions
  // Inherited via Geometry
	  virtual void GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const override;
	  virtual void CalculateLocalInertia(float mass, Vector& inertia) const override;
	  virtual void SetMargin(float margin) override;
	  virtual float GetMargin() const override;
  };

}// Maths namespace