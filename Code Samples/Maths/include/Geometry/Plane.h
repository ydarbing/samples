#pragma once
#include "Geometry\Geometry.h"
#include "Utils.h"

namespace Maths
{
  class Point;
  class Vector;

  class Plane : public Geometry
  {
  public:
    Plane();
    // use plane equation a_*x + b_*y + c_*z - d_ = 0
    Plane(float a_, float b_, float c_, float d_);
    // create plane that contains 'point' with given normal
    Plane(const Vector& normal, const Point& point);
    // create plane that contains all 3 points
    Plane(const Point& p0, const Point& p1, const Point& p2);
    ~Plane();

    float Normalize(float epsilon = EPSILON);
    float DistanceTo(const Point& point) const;
    Vector GetNormal(void) const;
    float GetConstant(void) const;

    enum PLANE_SIDE
    {
      FRONT = 0,
      BACK,
      ON
    };

    PLANE_SIDE WhichSide(const Point& point) const;

    bool operator==(const Plane &rhs) const;
    bool operator!=(const Plane &rhs) const;

    friend std::ostream& operator<<(std::ostream &os, const Plane &p);

    virtual Point GetPosition(void) const override;
    virtual void SetPosition(const Point& pos) override;
    ////virtual void Draw(ColorPtr rgba = nullptr) const override;
    virtual const std::string GetName() const override;
	virtual void SetLocalScaling(const Vector& scaling) override;
	virtual const Vector& GetLocalScaling() const override;

    static const Plane XY_PLANE;
    static const Plane XZ_PLANE;
    static const Plane YZ_PLANE;

  public:
    union
    {
      struct { float a, b, c, d; };
      float v[4];
    };


  private:
	  Vector m_localScaling;

  private: // not implemented base functions
  // Inherited via Geometry
	  virtual void GetAabb(const Transform& t, Vector& aabbMin, Vector& aabbMax) const override;
	  virtual void CalculateLocalInertia(float mass, Vector& inertia) const override;
	  virtual void SetMargin(float margin) override;
	  virtual float GetMargin() const override;
  };

} // namespace Maths
