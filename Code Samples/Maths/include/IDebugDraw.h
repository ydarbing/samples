#pragma once

#include "Vector.h"
#include "Transform.h"
#include "Utils.h" // HALF_PI

using namespace Maths;

class IDebugDraw
{
public:
  struct DefaultColors
  {
    Vector	m_activeObject;
    Vector	m_deactivatedObject;
    Vector	m_wantsDeactivationObject;
    Vector	m_disabledDeactivationObject;
    Vector	m_disabledSimulationObject;
    Vector	m_aabb;
    Vector m_contactPoint;
    
    DefaultColors()
      : m_activeObject(1,1,1),
      m_deactivatedObject(0,1,0),
      m_wantsDeactivationObject(0,1,1),
      m_disabledDeactivationObject(1,0,0),
      m_disabledSimulationObject(1,1,0),
      m_aabb(1,0,0),
      m_contactPoint(1,1,0)
    {
    }
  };

  enum	DebugDrawModes
  {
    DBG_NoDebug = 0,
    DBG_DrawWireframe = 1,
    DBG_DrawAabb = 2,
    DBG_DrawFeaturesText = 4,
    DBG_DrawContactPoints = 8,
    DBG_NoDeactivation = 16,
    DBG_NoHelpText = 32,
    DBG_DrawText = 64,
    DBG_ProfileTimings = 128,
    DBG_EnableSatComparison = 256,
    DBG_DisableBulletLCP = 512,
    DBG_EnableCCD = 1024,
    DBG_DrawConstraints = (1 << 11),
    DBG_DrawConstraintLimits = (1 << 12),
    DBG_FastWireframe = (1 << 13),
    DBG_DrawNormals = (1 << 14),
    DBG_DrawFrames = (1 << 15),
    DBG_MAX_DEBUG_DRAW_MODE
  };


  virtual ~IDebugDraw() {}
  
  virtual DefaultColors	GetDefaultColors() const { DefaultColors colors;	return colors; }
  ///the default implementation for setDefaultColors has no effect. A derived class can implement it and store the colors.
  virtual void SetDefaultColors(const DefaultColors& /*colors*/) {}

  virtual void DrawLine(const Vector& from, const Vector& to, const Vector& color) = 0;

  virtual void DrawLine(const Vector& from, const Vector& to, const Vector& fromColor, const Vector& toColor)
  {
    (void)toColor;
    DrawLine(from, to, fromColor);
  }

  virtual void DrawSphere(float radius, const Transform& transform, const Vector& color)
  {

    Vector center = transform.GetOrigin();
    Vector up = transform.GetBasis().GetColumn(1);
    Vector axis = transform.GetBasis().GetColumn(0);
    float minTh = -Maths::HALF_PI;
    float maxTh =  Maths::HALF_PI;
    float minPs = -Maths::HALF_PI;
    float maxPs =  Maths::HALF_PI;
    float stepDegrees = 30.f;
    DrawSpherePatch(center, up, axis, radius, minTh, maxTh, minPs, maxPs, color, stepDegrees, false);
    DrawSpherePatch(center, up, -axis, radius, minTh, maxTh, minPs, maxPs, color, stepDegrees, false);
  }

  virtual void DrawSphere(const Vector& p, float radius, const Vector& color)
  {
    Transform tr;
    tr.SetIdentity();
    tr.SetOrigin(p);
    DrawSphere(radius, tr, color);
  }

  virtual	void	DrawTriangle(const Vector& v0, const Vector& v1, const Vector& v2, const Vector& /*n0*/, const Vector& /*n1*/, const Vector& /*n2*/, const Vector& color, float alpha)
  {
    DrawTriangle(v0, v1, v2, color, alpha);
  }
  virtual	void	DrawTriangle(const Vector& v0, const Vector& v1, const Vector& v2, const Vector& color, float /*alpha*/)
  {
    DrawLine(v0, v1, color);
    DrawLine(v1, v2, color);
    DrawLine(v2, v0, color);
  }

  virtual void	DrawContactPoint(const Vector& PointOnB, const Vector& normalOnB, float distance, int lifeTime, const Vector& color) = 0;
  virtual void	ReportErrorWarning(const char* warningString) = 0;
  virtual void	Draw3dText(const Vector& location, const char* textString) = 0;
  virtual void	SetDebugMode(int debugMode) = 0;
  virtual int		GetDebugMode() const = 0;

  virtual void DrawAABB(const Vector& from, const Vector& to, const Vector& color)
  {
    Vector halfExtents = (to - from)* 0.5f;
    Vector center = (to + from) *0.5f;
    int i, j;

    Vector edgecoord(1.f, 1.f, 1.f), pa, pb;
    for (i = 0; i<4; i++)
    {
      for (j = 0; j<3; j++)
      {
        pa = Vector(edgecoord[0] * halfExtents[0], edgecoord[1] * halfExtents[1],
          edgecoord[2] * halfExtents[2]);
        pa += center;

        int othercoord = j % 3;
        edgecoord[othercoord] *= -1.f;
        pb = Vector(edgecoord[0] * halfExtents[0], edgecoord[1] * halfExtents[1],
          edgecoord[2] * halfExtents[2]);
        pb += center;

        DrawLine(pa, pb, color);
      }
      edgecoord = Vector(-1.f, -1.f, -1.f);
      if (i<3)
        edgecoord[i] *= -1.f;
    }
  }
  virtual void DrawTransform(const Transform& transform, float orthoLen)
  {
    Vector start = transform.GetOrigin();
    DrawLine(start, start + transform.GetBasis() * Vector(orthoLen, 0, 0), Vector(1.f, 0.3, 0.3));
    DrawLine(start, start + transform.GetBasis() * Vector(0, orthoLen, 0), Vector(0.3, 1.f, 0.3));
    DrawLine(start, start + transform.GetBasis() * Vector(0, 0, orthoLen), Vector(0.3, 0.3, 1.f));
  }

  virtual void DrawArc(const Vector& center, const Vector& normal, const Vector& axis, float radiusA, float radiusB, float minAngle, float maxAngle,
    const Vector& color, bool drawSect, float stepDegrees = float(10.f))
  {
    const Vector& vx = axis;
    Vector vy = normal.Cross(axis);
    float step = stepDegrees * RADS_PER_DEG;
    int nSteps = (int)Maths::AbsVal((maxAngle - minAngle) / step);
    if (!nSteps) nSteps = 1;
    Vector prev = center + radiusA * vx * Maths::Cos(minAngle) + radiusB * vy * Maths::Sin(minAngle);
    if (drawSect)
      DrawLine(center, prev, color);
    
    for (int i = 1; i <= nSteps; i++)
    {
      float angle = minAngle + (maxAngle - minAngle) * float(i) / float(nSteps);
      Vector next = center + radiusA * vx * Maths::Cos(angle) + radiusB * vy * Maths::Sin(angle);
      DrawLine(prev, next, color);
      prev = next;
    }
    if (drawSect)
      DrawLine(center, prev, color);
  }


  virtual void DrawSpherePatch(const Vector& center, const Vector& up, const Vector& axis, float radius,
    float minTh, float maxTh, float minPs, float maxPs, const Vector& color, float stepDegrees = float(10.f), bool drawCenter = true)
  {
    Vector vA[74];
    Vector vB[74];
    Vector *pvA = vA, *pvB = vB, *pT;
    Vector npole = center + up * radius;
    Vector spole = center - up * radius;
    Vector arcStart;
    float step = stepDegrees * RADS_PER_DEG;
    const Vector& kv = up;
    const Vector& iv = axis;
    Vector jv = kv.Cross(iv);
    bool drawN = false;
    bool drawS = false;
    if (minTh <= -Maths::HALF_PI)
    {
      minTh = -Maths::HALF_PI + step;
      drawN = true;
    }
    if (maxTh >= Maths::HALF_PI)
    {
      maxTh = Maths::HALF_PI - step;
      drawS = true;
    }
    if (minTh > maxTh)
    {
      minTh = -Maths::HALF_PI + step;
      maxTh = Maths::HALF_PI - step;
      drawN = drawS = true;
    }
    int n_hor = (int)((maxTh - minTh) / step) + 1;
    if (n_hor < 2) n_hor = 2;
    float step_h = (maxTh - minTh) / float(n_hor - 1);
    bool isClosed = false;
    if (minPs > maxPs)
    {
      minPs = -Maths::PI + step;
      maxPs = Maths::PI;
      isClosed = true;
    }
    else if ((maxPs - minPs) >= Maths::PI * float(2.f))
    {
      isClosed = true;
    }
    else
    {
      isClosed = false;
    }

    int n_vert = (int)((maxPs - minPs) / step) + 1;
    if (n_vert < 2) n_vert = 2;
    float step_v = (maxPs - minPs) / float(n_vert - 1);
    for (int i = 0; i < n_hor; i++)
    {
      float th = minTh + float(i) * step_h;
      float sth = radius * Maths::Sin(th);
      float cth = radius * Maths::Cos(th);
      for (int j = 0; j < n_vert; j++)
      {
        float psi = minPs + float(j) * step_v;
        float sps = Maths::Sin(psi);
        float cps = Maths::Cos(psi);
        pvB[j] = center + cth * cps * iv + cth * sps * jv + sth * kv;
        if (i)
          DrawLine(pvA[j], pvB[j], color);
        else if (drawS)
          DrawLine(spole, pvB[j], color);

        if (j)
          DrawLine(pvB[j - 1], pvB[j], color);
        else
          arcStart = pvB[j];
        
        if ((i == (n_hor - 1)) && drawN)
          DrawLine(npole, pvB[j], color);

        if (drawCenter)
        {
          if (isClosed)
          {
            if (j == (n_vert - 1))
            {
              DrawLine(arcStart, pvB[j], color);
            }
          }
          else
          {
            if (((!i) || (i == (n_hor - 1))) && ((!j) || (j == (n_vert - 1))))
            {
              DrawLine(center, pvB[j], color);
            }
          }
        }
      }
      pT = pvA; pvA = pvB; pvB = pT;
    }
  }


  virtual void DrawBox(const Vector& bbMin, const Vector& bbMax, const Vector& color)
  {
    DrawLine(Vector(bbMin[0], bbMin[1], bbMin[2]), Vector(bbMax[0], bbMin[1], bbMin[2]), color);
    DrawLine(Vector(bbMax[0], bbMin[1], bbMin[2]), Vector(bbMax[0], bbMax[1], bbMin[2]), color);
    DrawLine(Vector(bbMax[0], bbMax[1], bbMin[2]), Vector(bbMin[0], bbMax[1], bbMin[2]), color);
    DrawLine(Vector(bbMin[0], bbMax[1], bbMin[2]), Vector(bbMin[0], bbMin[1], bbMin[2]), color);
    DrawLine(Vector(bbMin[0], bbMin[1], bbMin[2]), Vector(bbMin[0], bbMin[1], bbMax[2]), color);
    DrawLine(Vector(bbMax[0], bbMin[1], bbMin[2]), Vector(bbMax[0], bbMin[1], bbMax[2]), color);
    DrawLine(Vector(bbMax[0], bbMax[1], bbMin[2]), Vector(bbMax[0], bbMax[1], bbMax[2]), color);
    DrawLine(Vector(bbMin[0], bbMax[1], bbMin[2]), Vector(bbMin[0], bbMax[1], bbMax[2]), color);
    DrawLine(Vector(bbMin[0], bbMin[1], bbMax[2]), Vector(bbMax[0], bbMin[1], bbMax[2]), color);
    DrawLine(Vector(bbMax[0], bbMin[1], bbMax[2]), Vector(bbMax[0], bbMax[1], bbMax[2]), color);
    DrawLine(Vector(bbMax[0], bbMax[1], bbMax[2]), Vector(bbMin[0], bbMax[1], bbMax[2]), color);
    DrawLine(Vector(bbMin[0], bbMax[1], bbMax[2]), Vector(bbMin[0], bbMin[1], bbMax[2]), color);
  }
  virtual void DrawBox(const Vector& bbMin, const Vector& bbMax, const Transform& trans, const Vector& color)
  {
    DrawLine(trans * Vector(bbMin[0], bbMin[1], bbMin[2]), trans * Vector(bbMax[0], bbMin[1], bbMin[2]), color);
    DrawLine(trans * Vector(bbMax[0], bbMin[1], bbMin[2]), trans * Vector(bbMax[0], bbMax[1], bbMin[2]), color);
    DrawLine(trans * Vector(bbMax[0], bbMax[1], bbMin[2]), trans * Vector(bbMin[0], bbMax[1], bbMin[2]), color);
    DrawLine(trans * Vector(bbMin[0], bbMax[1], bbMin[2]), trans * Vector(bbMin[0], bbMin[1], bbMin[2]), color);
    DrawLine(trans * Vector(bbMin[0], bbMin[1], bbMin[2]), trans * Vector(bbMin[0], bbMin[1], bbMax[2]), color);
    DrawLine(trans * Vector(bbMax[0], bbMin[1], bbMin[2]), trans * Vector(bbMax[0], bbMin[1], bbMax[2]), color);
    DrawLine(trans * Vector(bbMax[0], bbMax[1], bbMin[2]), trans * Vector(bbMax[0], bbMax[1], bbMax[2]), color);
    DrawLine(trans * Vector(bbMin[0], bbMax[1], bbMin[2]), trans * Vector(bbMin[0], bbMax[1], bbMax[2]), color);
    DrawLine(trans * Vector(bbMin[0], bbMin[1], bbMax[2]), trans * Vector(bbMax[0], bbMin[1], bbMax[2]), color);
    DrawLine(trans * Vector(bbMax[0], bbMin[1], bbMax[2]), trans * Vector(bbMax[0], bbMax[1], bbMax[2]), color);
    DrawLine(trans * Vector(bbMax[0], bbMax[1], bbMax[2]), trans * Vector(bbMin[0], bbMax[1], bbMax[2]), color);
    DrawLine(trans * Vector(bbMin[0], bbMax[1], bbMax[2]), trans * Vector(bbMin[0], bbMin[1], bbMax[2]), color);
  }

  virtual void DrawPlane(const Vector& planeNormal, float planeConst, const Transform& transform, const Vector& color)
  {
    Vector planeOrigin = planeNormal * planeConst;
    Vector vec0, vec1;
    PlaneSpace(planeNormal, vec0, vec1);
    float vecLen = 100.f;
    Vector pt0 = planeOrigin + vec0*vecLen;
    Vector pt1 = planeOrigin - vec0*vecLen;
    Vector pt2 = planeOrigin + vec1*vecLen;
    Vector pt3 = planeOrigin - vec1*vecLen;
    DrawLine(transform*pt0, transform*pt1, color);
    DrawLine(transform*pt2, transform*pt3, color);
  }

  virtual void clearLines()
  {
  }

  virtual void flushLines()
  {
  }
};
