#pragma once
#include "Vector.h"


enum EGeometryShapes
{
  // polyhedral convex shapes
  E_BOX_SHAPE,
  E_TRIANGLE_SHAPE,
  E_TETRAHEDRAL_SHAPE,
  E_CONVEX_TRIANGLEMESH_SHAPE,
  E_CONVEX_HULL_SHAPE,
  E_CONVEX_POINT_CLOUD_SHAPE,
  E_CUSTOM_POLYHEDRAL_SHAPE,
  // implicit convex Shapes
IMPLICIT_CONVEX_SHAPES_START_HERE,
  E_SPHERE_SHAPE,
  E_CAPSULE_SHAPE,
  E_CONE_SHAPE,
  E_CONVEX_SHAPE,
  E_CYLINDER_SHAPE,
  E_UNIFORM_SCALING_SHAPE,
  E_BOX_2D_SHAPE,
  E_CONVEX_2D_SHAPE,
  E_CUSTOM_CONVEX_SHAPE,
  //concave shapes
CONCAVE_SHAPES_START_HERE,
  E_TRIANGLE_MESH_SHAPE,
  E_SCALED_TRIANGLE_MESH_SHAPE,
  E_TERRAIN_SHAPE,
  ///Used for GIMPACT Trimesh integration
  E_GIMPACT_SHAPE,

  E_EMPTY_SHAPE,
  E_STATIC_PLANE_SHAPE,
  E_CUSTOM_CONCAVE_SHAPE,
CONCAVE_SHAPES_END_HERE,

  E_COMPOUND_SHAPE,
  E_SOFTBODY_SHAPE,

  E_INVALID_SHAPE,
  E_MAX_SHAPES
};

namespace Maths
{
  struct BroadphaseProxy
  {
    enum CollisionFilterGroups
    {

      DefaultFilter = 1,
      StaticFilter = 2,
      KinematicFilter = 4,
      DebrisFilter = 8,
      SensorTrigger = 16,
      CharacterFilter = 32,
      AllFilter = -1 //all bits sets: DefaultFilter | StaticFilter | KinematicFilter | DebrisFilter | SensorTrigger
    };

    void* m_clientObject;
    int m_collisionFilterGroup;
    int m_collisionFilterMask;
    int m_uniqueID;

    Vector m_aabbMin;
    Vector m_aabbMax;

    inline int GetUID()const
    {
      return m_uniqueID;
    }

    BroadphaseProxy() : m_clientObject(nullptr), m_collisionFilterGroup(0), m_collisionFilterMask(0),
		m_aabbMin(), m_aabbMax(), m_uniqueID(-1)
    {
    }

    BroadphaseProxy(const Vector& aabbMin, const Vector& aabbMax, void* userPtr, int collisionFilterGroup, int collisionFilterMask)
      : m_clientObject(userPtr), m_collisionFilterGroup(collisionFilterGroup), m_collisionFilterMask(collisionFilterMask),
      m_aabbMin(aabbMin), m_aabbMax(aabbMax), m_uniqueID(-1)
    {
    }

    static inline bool IsPolyhedral(int proxyType)
    {
      return (proxyType  < IMPLICIT_CONVEX_SHAPES_START_HERE);
    }

    static inline bool IsConvex(int proxyType)
    {
      return (proxyType < CONCAVE_SHAPES_START_HERE);
    }

    static inline bool IsNonMoving(int proxyType)
    {
      return (IsConcave(proxyType) && !(proxyType == E_GIMPACT_SHAPE));
    }

    static inline bool IsConcave(int proxyType)
    {
      return ((proxyType > CONCAVE_SHAPES_START_HERE) &&
        (proxyType < CONCAVE_SHAPES_END_HERE));
    }
    static inline bool IsCompound(int proxyType)
    {
      return (proxyType == E_COMPOUND_SHAPE);
    }

    static inline bool IsSoftBody(int proxyType)
    {
      return (proxyType == E_SOFTBODY_SHAPE);
    }

    static inline bool IsInfinite(int proxyType)
    {
      return (proxyType == E_STATIC_PLANE_SHAPE);
    }

    static inline bool IsConvex2d(int proxyType)
    {
      return (proxyType == E_BOX_2D_SHAPE) || (proxyType == E_CONVEX_2D_SHAPE);
    }

  };

  class CollisionAlgorithm;
  struct BroadphaseProxy;

  // BroadphasePair class contains a pair of aabb-overlapping objects
  // A Dispatcher class can search a CollisionAlgorithm that performs exact/narrowphase collision detection on the actual collision shapes
  struct BroadphasePair
  {
    BroadphaseProxy* m_pProxy0;
    BroadphaseProxy* m_pProxy1;
    mutable CollisionAlgorithm* m_algorithm;

    BroadphasePair() :m_pProxy0(nullptr), m_pProxy1(nullptr), m_algorithm(nullptr)
    {
    }

    BroadphasePair(const BroadphasePair& other)
      : m_pProxy0(other.m_pProxy0), m_pProxy1(other.m_pProxy1), m_algorithm(other.m_algorithm)
    {
    }

    BroadphasePair(BroadphaseProxy& proxy0, BroadphaseProxy& proxy1)
    { // keep them sorted so std::set operations work
      if (proxy0.m_uniqueID < proxy1.m_uniqueID)
      {
        m_pProxy0 = &proxy0;
        m_pProxy1 = &proxy1;
      }
      else
      {
        m_pProxy0 = &proxy1;
        m_pProxy1 = &proxy0;
      }

      m_algorithm = nullptr;
    }
  };

  class BroadphasePairSortPredicate
  {
  public:
    bool operator() (const BroadphasePair& a, const BroadphasePair& b)const
    {
      const int uidA0 = a.m_pProxy0 ? a.m_pProxy0->m_uniqueID : -1;
      const int uidB0 = b.m_pProxy0 ? b.m_pProxy0->m_uniqueID : -1;
      const int uidA1 = a.m_pProxy1 ? a.m_pProxy1->m_uniqueID : -1;
      const int uidB1 = b.m_pProxy1 ? b.m_pProxy1->m_uniqueID : -1;

      return uidA0 > uidB0 ||
        (a.m_pProxy0 == b.m_pProxy0 && uidA1 > uidB1) ||
        (a.m_pProxy0 == b.m_pProxy0 && a.m_pProxy1 == b.m_pProxy1 && a.m_algorithm > b.m_algorithm);
    }
  };

  inline bool operator==(const BroadphasePair& a, const BroadphasePair& b)
  {
    return (a.m_pProxy0 == b.m_pProxy0) && (a.m_pProxy1 == b.m_pProxy1);
  }

}// namespace Maths