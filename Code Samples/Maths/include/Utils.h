#pragma once
#include "MathsAssert.h"
#include "Color.h"
#include <cassert>
#include <math.h>


namespace Maths
{

#define RecipSqrt(x) ((float)(float(1.0) / Sqrt(float(x)))) /* reciprocal square root */

  const float EPSILON = 1.0e-5f;
  const int LOMONT_EPSILON = *(int*)&EPSILON;
  const float MAX_FLOAT = 3.402823466e+38f;
  const float PI = float(4.0 * atan(1.0));
  const float TWO_PI = float(2.0) * PI;
  const float HALF_PI = float(0.5) * PI;
  const float QUARTER_PI = float(0.25) * PI;
  const float THREE_HALF_PI = float(3.0) * HALF_PI;
  const float INV_PI = 1.0f / PI;
  const float INV_TWO_PI = 1.0f / TWO_PI;
  const float DEG_TO_RAD = PI / 180.0f;
  const float RAD_TO_DEG = 180.0f / PI;
  const float RADS_PER_DEG = (TWO_PI / float(360.0));
  const float DEGS_PER_RAD = (float(360.0) / TWO_PI);
  const float SQRT12 = float(0.7071067811865475244008443621048490);
  const float LN_2 = float(log(2.0));
  const float LN_10 = float(log(10.0));
  const float INV_LN_2 = 1.0f / LN_2;
  const float INV_LN_10 = 1.0f / LN_10;
  const float ROOT_TWO = float(sqrt(2.0));
  const float INV_ROOT_2 = 1.0f / ROOT_TWO;
  const float ROOT_THREE = float(sqrt(3.0));
  const float INV_ROOT_THREE = 1.0f / ROOT_THREE;



  inline float Cos(float x) { return cosf(x); }
  inline float Sin(float x) { return sinf(x); }
  inline float Tan(float x) { return tanf(x); }
  inline float Asin(float x)
  {
    if (x < float(-1))
      x = float(-1);
    if (x > float(1))
      x = float(1);
    return asinf(x);
  }

#ifndef Fsels
  inline float Fsel(float a, float b, float c)
  {
    return a >= 0 ? b : c;
  }
#endif
#define Fsels(a, b, c) (float) Fsel(a, b, c)


  template <typename T>
  inline T AbsVal(const T& val)
  {
    return (val < T(0) ? -val : val);
  }


  inline float Sqrt(float y)
  {
    return sqrtf(y);
  }

  inline bool Equal(float a, float b, int maxDiff = LOMONT_EPSILON)
  {
    // solid, fast routine across all platforms with constant time behavior
    int ai = *reinterpret_cast<int*>(&a);
    int bi = *reinterpret_cast<int*>(&b);
    int blah = -42, blahdi = -100;
    int test = (((unsigned)(ai^bi)) >> 31) - 1;
    int diff = (((0x80000000 - ai)&(~test)) | (ai & test)) - bi;
    int v1 = maxDiff + diff;
    int v2 = maxDiff - diff;
    int res = (v1 | v2);
    return (v1 | v2) >= 0;
  }

  inline bool FloatEqual(float a, float b, float maxDiff = EPSILON)
  {
    return (a <= b + maxDiff && a >= b - maxDiff);
  }

  template <typename T>
  inline T Min(const T& a, const T& b)
  {
    return (b < a ? b : a);
  }

  template <typename T>
  inline T Max(const T& a, const T& b)
  {
    return (a < b ? b : a);
  }

  template <typename T>
  inline T Clamp(const T& val, const T& min, const T& max)
  {
    return Max(min, Min(val, max));
  }


  template <typename T>
  inline int Round(const T& val)
  {
    if (val < T(0))
      return (T(-0.5f) < val - int(val) ? int(val) : int(val - 1));
    else
      return (val - int(val) < T(0.5f) ? int(val) : int(val + 1));
  }

  inline float Random(void)
  {
    return (float(rand()) / float(RAND_MAX));
  }

  template <typename T>
  inline T Random(const T& min, const T& max)
  {
    return T(min + ((max - min) * Random()));
  }

  template <>
  inline int Random(const int& min, const int& max)
  {
    return Round(min + ((max - min) * Random()));
  }

  template <>
  inline unsigned Random(const unsigned& min, const unsigned& max)
  {
    return Round(min + ((max - min) * Random()));
  }

  template <>
  inline Color Random(const Color& min, const Color& max)
  {
    return Color(Random(min.r, max.r),
      Random(min.g, max.g),
      Random(min.b, max.b),
      Random(min.a, max.a));
  }

  inline void ZeroIfZero(float& val)
  {
    if (Equal(val, 0.0f))
      val = 0.0f;
  }

  inline void GetSinCos(float angle, float& sina, float& cosa)
  {
    sina = sin(angle);
    cosa = cos(angle);
    ZeroIfZero(sina);
    ZeroIfZero(cosa);
  }


  // generate random int between a and b
  inline int RandomInt(int a, int b)
  {
    maths_assert(a < b);
    int result = a + rand() % (b - a + 1);
    maths_assert(result >= a);
    maths_assert(result <= b);
    return result;
  }
  // generate random float between a and b
  inline float RandomFloat(float a, float b)
  {
    maths_assert(a < b);
    float random = Random();
    float diff = b - a;
    float r = random * diff;
    return a + r;
  }

  // Calculates the population count of an unsigned 32 bit integer at compile time.
  // Population count is the number of bits in the integer that set to 1.
  //   See "Hacker's Delight" and http://www.hackersdelight.org/hdcodetxt/popArrayHS.c.txt
  template <uint32_t x> struct PopCount
  {
    enum {
      a = x - ((x >> 1) & 0x55555555),
      b = (((a >> 2) & 0x33333333) + (a & 0x33333333)),
      c = (((b >> 4) + b) & 0x0f0f0f0f),
      d = c + (c >> 8),
      e = d + (d >> 16),

      result = e & 0x0000003f
    };
  };

  // Calculates the log 2 of an unsigned 32 bit integer at compile time.
  template <uint32_t x> struct Log2
  {
    enum {
      a = x | (x >> 1),
      b = a | (a >> 2),
      c = b | (b >> 4),
      d = c | (c >> 8),
      e = d | (d >> 16),
      f = e >> 1,

      result = PopCount<f>::result
    };
  };

  //Calculates the number of bits required to serialize an integer value in [min,max] at compile time.
  template <int64_t min, int64_t max> 
  struct BitsRequired
  {
    static const uint32_t result = (min == max) ? 0 : (Log2<uint32_t(max - min)>::result + 1);
  };

  //Calculates the population count of an unsigned 32 bit integer.
  //The population count is the number of bits in the integer set to 1.
  inline uint32_t popcount(uint32_t x)
  {
#ifdef __GNUC__
    return __builtin_popcount(x);
#else // #ifdef __GNUC__
    const uint32_t a = x - ((x >> 1) & 0x55555555);
    const uint32_t b = (((a >> 2) & 0x33333333) + (a & 0x33333333));
    const uint32_t c = (((b >> 4) + b) & 0x0f0f0f0f);
    const uint32_t d = c + (c >> 8);
    const uint32_t e = d + (d >> 16);
    const uint32_t result = e & 0x0000003f;
    return result;
#endif // #ifdef __GNUC__
  }

  //Calculates the log base 2 of an unsigned 32 bit integer.
  inline uint32_t log2(uint32_t x)
  {
    const uint32_t a = x | (x >> 1);
    const uint32_t b = a | (a >> 2);
    const uint32_t c = b | (b >> 4);
    const uint32_t d = c | (c >> 8);
    const uint32_t e = d | (d >> 16);
    const uint32_t f = e >> 1;
    return popcount(f);
  }


  //Calculates the number of bits required to serialize an integer in range [min,max].
  inline int BitsRequired(uint32_t min, uint32_t max)
  {
#ifdef __GNUC__
    return 32 - __builtin_clz(max - min);
#else // #ifdef __GNUC__
    return (min == max) ? 0 : log2(max - min) + 1;
#endif // #ifdef __GNUC__
  }

  //Reverse the order of bytes in a 64 bit integer.
  inline uint64_t BitSwap(uint64_t value)
  {
#ifdef __GNUC__
    return __builtin_bswap64(value);
#else // #ifdef __GNUC__
    value = (value & 0x00000000FFFFFFFF) << 32 | (value & 0xFFFFFFFF00000000) >> 32;
    value = (value & 0x0000FFFF0000FFFF) << 16 | (value & 0xFFFF0000FFFF0000) >> 16;
    value = (value & 0x00FF00FF00FF00FF) << 8 | (value & 0xFF00FF00FF00FF00) >> 8;
    return value;
#endif // #ifdef __GNUC__
  }

  //Reverse the order of bytes in a 32 bit integer.
  inline uint32_t BitSwap(uint32_t value)
  {
#ifdef __GNUC__
    return __builtin_bswap32(value);
#else // #ifdef __GNUC__
    return (value & 0x000000ff) << 24 | (value & 0x0000ff00) << 8 | (value & 0x00ff0000) >> 8 | (value & 0xff000000) >> 24;
#endif // #ifdef __GNUC__
  }

  // Reverse the order of bytes in a 16 bit integer
  inline uint16_t BitSwap(uint16_t value)
  {
    return (value & 0x00ff) << 8 | (value & 0xff00) >> 8;
  }


  template <typename T>
  T HostToNetwork(T value)
  {
#if MATHS_BIG_ENDIAN
    return BitSwap(value);
#else // #if MATHS_BIG_ENDIAN
    return value;
#endif // #if MATHS_BIG_ENDIAN
  }

  template <typename T>
  T NetworkToHost(T value)
  {
#if MATHS_BIG_ENDIAN
    return BitSwap(value);
#else // #if MATHS_BIG_ENDIAN
    return value;
#endif // #if MATHS_BIG_ENDIAN
  }

  // Compares two 16 bit sequence numbers and returns true if the first one is greater than the second (considering wrapping)
  // IMPORTANT: This is not the same as s1 > s2!
  // Greater than is defined specially to handle wrapping sequence numbers.
  // If the two sequence numbers are close together, it is as normal, but they are far apart, it is assumed that they have wrapped around.
  // Thus, sequence_greater_than( 1, 0 ) returns true, and so does sequence_greater_than( 0, 65535 )!
  // returns - true if the s1 is greater than s2, with sequence number wrapping considered.
  inline bool SequenceGreaterThan(uint16_t s1, uint16_t s2)
  {
    return ((s1 > s2) && (s1 - s2 <= 32768)) ||
      ((s1 < s2) && (s2 - s1 > 32768));
  }

  // Compares two 16 bit sequence numbers and returns true if the first one is less than the second (considering wrapping)
  // IMPORTANT: This is not the same as s1 < s2!
  // Greater than is defined specially to handle wrapping sequence numbers.
  // If the two sequence numbers are close together, it is as normal, but they are far apart, it is assumed that they have wrapped around.
  // Thus, sequence_less_than( 0, 1 ) returns true, and so does sequence_greater_than( 65535, 0 )!
  // returns - true if the s1 is less than s2, with sequence number wrapping considered.
  inline bool SequenceLessThan(uint16_t s1, uint16_t s2)
  {
    return SequenceGreaterThan(s2, s1);
  }

  //Convert a signed integer to an unsigned integer with zig-zag encoding.
  // 0,-1,+1,-2,+2... becomes 0,1,2,3,4 ...
  // @returns The input value converted from signed to unsigned with zig-zag encoding.
  inline int SignedToUnsigned(int n)
  {
    return (n << 1) ^ (n >> 31);
  }

  // Convert an unsigned integer to as signed integer with zig-zag encoding.
  // 0,1,2,3,4... becomes 0,-1,+1,-2,+2...
  // returns - input value converted from unsigned to signed with zig-zag encoding.
  inline int UnsignedToSigned(uint32_t n)
  {
    return (n >> 1) ^ (-int32_t(n & 1));
  }
}// namespace Maths
