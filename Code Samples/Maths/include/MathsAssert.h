#pragma once

#include <cstdlib> // exit()
#include <cstring> // memcpy
#include <cstdint> // fixed length integers

namespace Maths
{

const uint32_t SerializeCheckValue = 0x12345678; // The value written to the stream for serialize checks. See WriteStream::SerializeCheck and ReadStream::SerializeCheck

#define MATHS_SERIALIZE_CHECKS 1


#define MATHS_PLATFORM_WINDOWS   1
#define MATHS_PLATFORM_MAC       2
#define MATHS_PLATFORM_UNIX      3

#if defined(_WIN32)
#define MATHS_PLATFORM MATHS_PLATFORM_WINDOWS
#elif defined(__APPLE__)
#define MATHS_PLATFORM MATHS_PLATFORM_MAC
#else
#define MATHS_PLATFORM MATHS_PLATFORM_UNIX
#endif







#if !defined (MATHS_LITTLE_ENDIAN ) && !defined( MATHS_BIG_ENDIAN )

#ifdef __BYTE_ORDER__
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#define MATHS_LITTLE_ENDIAN 1
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#define MATHS_BIG_ENDIAN 1
#else
#error Unknown machine endianess detected. User needs to define MATHS_LITTLE_ENDIAN or MATHS_BIG_ENDIAN.
#endif // __BYTE_ORDER__

  // Detect with GLIBC's endian.h
#elif defined(__GLIBC__)
#include <endian.h>
#if (__BYTE_ORDER == __LITTLE_ENDIAN)
#define MATHS_LITTLE_ENDIAN 1
#elif (__BYTE_ORDER == __BIG_ENDIAN)
#define MATHS_BIG_ENDIAN 1
#else
#error Unknown machine endianess detected. User needs to define MATHS_LITTLE_ENDIAN or MATHS_BIG_ENDIAN.
#endif // __BYTE_ORDER

  // Detect with _LITTLE_ENDIAN and _BIG_ENDIAN macro
#elif defined(_LITTLE_ENDIAN) && !defined(_BIG_ENDIAN)
#define MATHS_LITTLE_ENDIAN 1
#elif defined(_BIG_ENDIAN) && !defined(_LITTLE_ENDIAN)
#define MATHS_BIG_ENDIAN 1

  // Detect with architecture macros
#elif    defined(__sparc)     || defined(__sparc__)                           \
        || defined(_POWER)      || defined(__powerpc__)                         \
        || defined(__ppc__)     || defined(__hpux)      || defined(__hppa)      \
        || defined(_MIPSEB)     || defined(_POWER)      || defined(__s390__)
#define MATHS_BIG_ENDIAN 1
#elif    defined(__i386__)    || defined(__alpha__)   || defined(__ia64)      \
        || defined(__ia64__)    || defined(_M_IX86)     || defined(_M_IA64)     \
        || defined(_M_ALPHA)    || defined(__amd64)     || defined(__amd64__)   \
        || defined(_M_AMD64)    || defined(__x86_64)    || defined(__x86_64__)  \
        || defined(_M_X64)      || defined(__bfin__)
#define MATHS_LITTLE_ENDIAN 1
#elif defined(_MSC_VER) && defined(_M_ARM)
#define MATHS_LITTLE_ENDIAN 1
#else
#error Unknown machine endianess detected. User needs to define MATHS_LITTLE_ENDIAN or MATHS_BIG_ENDIAN. 
#endif
#endif

#ifndef MATHS_LITTLE_ENDIAN
#define MATHS_LITTLE_ENDIAN 0
#endif

#ifndef MATHS_BIG_ENDIAN
#define MATHS_BIG_ENDIAN 0
#endif

#define MATHS_LOG_LEVEL_NONE      0
#define MATHS_LOG_LEVEL_ERROR     1
#define MATHS_LOG_LEVEL_INFO      2
#define MATHS_LOG_LEVEL_DEBUG     3

  /**
  Set the maths log level.
  Valid log levels are: MATHS_LOG_LEVEL_NONE, MATHS_LOG_LEVEL_ERROR, MATHS_LOG_LEVEL_INFO and MATHS_LOG_LEVEL_DEBUG
  @param level The log level to set. Initially set to MATHS_LOG_LEVEL_NONE.
  */

  void MathsLogLevel(int level);

  /**
  Printf function used by maths to emit logs.
  This function internally calls the printf callback set by the user.
  */

  void MathsPrintf(int level, const char * format, ...);

  extern void(*maths_assert_function)(const char *, const char *, const char * file, int line);

#ifndef NDEBUG
#define maths_assert( condition )                                                         \
do                                                                                          \
{                                                                                           \
    if ( !(condition) )                                                                     \
    {                                                                                       \
        maths_assert_function( #condition, __FUNCTION__, __FILE__, __LINE__ );            \
        exit(1);                                                                            \
    }                                                                                       \
} while(0)
#else
#define maths_assert( ignore ) ((void)0)
#endif


}// namespace Maths