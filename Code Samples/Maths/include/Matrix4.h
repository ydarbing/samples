#pragma once
#include "Utils.h"
#include <ostream>

namespace Maths
{
  class Vector;
  class Point;

  class Matrix4
  {
  public:
    Matrix4(bool zero = true);
    Matrix4(float mat[16]);
    Matrix4(float mm00, float mm01, float mm02, float mm03,
      float mm10, float mm11, float mm12, float mm13,
      float mm20, float mm21, float mm22, float mm23,
      float mm30, float mm31, float mm32, float mm33);
    Matrix4(const Vector& u, const Vector& v);
    Matrix4(float pitch, float yaw, float roll, bool radians = true);
    ~Matrix4();


    Matrix4& MakeZero(void);
    Matrix4& MakeIdentity(void);
    // row indices are 0-3
    void SetRow(int row, const Vector& vec);
    // column indices are 0-3
    void SetCol(int col, const Vector& vec);
    // row indices are 0-3
    Vector GetRow(int row) const;
    // column indices are 0-3
    Vector GetCol(int col) const;


    Matrix4& MakeSkew(const Vector& v);
    Matrix4& MakeTranslation(const Point& v);
    Matrix4& MakeTransposeTranslation(const Point& v);
    Matrix4& MakeTranslationInverse(const Point& v);
    //Matrix& MakeRotation(const Quaternion& r);
    Matrix4& MakeScale(const Vector& v);
    Matrix4& MakeScaleInverse(const Vector& v);
    Matrix4& MakeObliqueProjection(const Vector& norm, const Vector& origin, const Vector& dir);
    Matrix4& MakePerspectiveProjection(const Vector& norm, const Vector& origin, const Vector& eye);
    Matrix4& MakeOrthographicProjection(float width, float height, float near, float far);
    Matrix4& MakePerspectiveProjection(float fov, float aspect, float near, float far);
    Matrix4& MakePerspectiveProjection(float left, float right, float top, float bottom, float near, float far);
    Matrix4& MakeLook(const Point& eye, const Point& look, const Vector& up);
    // create view matrix looking at point(look)
    Matrix4& MakeLookAt(const Point& eye, const Point& look, const Vector& up);
    // create matrix for reflection across norm and through an origin
    Matrix4& MakeReflection(const Vector& norm, const Vector& origin);
    // create matrix that rotates about 'axis' by 'angle'
    Matrix4& FromAxisAngle(const Vector& axis, float angle);
    // create matrix that rotates about 'axis' by 'angle' at a specific point 'position'
    Matrix4& FromTranslatedAxisAngle(const Point& position, const Vector& axis, float angle);

    //Matrix& Build(const Point& translation, const Quaternion& orientation, const Vector& scale);

    // Decomposes translation vector from a model matrix (matrix in model space)
    Point ToTranslation(void) const;
    // Decomposes translation vector from a transposed model matrix (matrix in model space)
    Point ToTransposeTranslation(void) const;

    //Quaternion ToOrientation(void) const;

    Matrix4 ToRotation(void) const;
    Vector ToScale(void) const;
    Vector GetEulerAngles(bool asRadians = true) const;
    Matrix4 Inverse(float eps = Maths::EPSILON) const;
    Matrix4 Adjoint(void) const;
    float Determinant(void) const;
    Matrix4 Transpose(void) const;
    //Returns the result of Transpose * rhs
    Matrix4 TransposeTimes(const Matrix4& rhs) const;
    //Returns the result of this * rhs.Transpose
    Matrix4 TimesTranspose(const Matrix4& rhs) const;
    //Returns the result of Transpose * rhs.Transpose
    Matrix4 TransposeTimesTranspose(const Matrix4& rhs) const;
    //Returns the result of transforming the point in reverse
    //If matrix represents world from model, this would calculate the point as
    //though it was doing a model from world transform
    //assuming matrix does not include scale or shear
    Point TransformInverse(const Point& point) const;
    //Returns the result of transforming the vector in reverse
    //If matrix represents world from model, this would calculate the vector as
    //though it was doing a model from world transform
    //assuming matrix does not include scale or shear
    Vector TransformInverse(const Vector& vec) const;

    Vector operator*(const Vector& rhs) const;
    Point operator*(const Point& rhs) const;
    //Returns result of concatenating two matrices
    Matrix4 operator*(const Matrix4& rhs) const;
    Matrix4 operator+(const Matrix4& rhs) const;
    Matrix4 operator-(const Matrix4& rhs) const;
    Matrix4 operator*(float mult) const;
    Matrix4 operator/(float div) const;
    Matrix4& operator+=(const Matrix4& rhs);
    Matrix4& operator-=(const Matrix4& rhs);
    Matrix4& operator/=(float div);
    Matrix4& operator*=(float mult);
    bool operator==(const Matrix4& rhs) const;
    bool operator!=(const Matrix4& rhs) const;
    bool operator<(const Matrix4& rhs) const;
    bool operator<=(const Matrix4& rhs) const;
    bool operator>(const Matrix4& rhs) const;
    bool operator>=(const Matrix4& rhs) const;


    friend std::ostream& operator<<(std::ostream& os, const Matrix4& mat);
    static const Matrix4 ZERO;
    static const Matrix4 IDENTITY;
    static const Matrix4 BIAS;

    static bool Test(void);

  public:
    union
    {
      struct {
        float m00, m01, m02, m03,
          m10, m11, m12, m13,
          m20, m21, m22, m23,
          m30, m31, m32, m33;
      };
      float m[4][4];
      float v[16];
    };
  };

}