#pragma once
#include <ostream> 

namespace Maths
{
  class Vector;
  class Point;

  class Color
  {
  public:
    // Alpha channel is set to 1.0f (full opacity) all others are set to 0.0f (black)
    Color(void);
    // r, g, b, a are all values in the range [0.0, 1.0]
    Color(float r, float g, float b, float a = 1.0f);
    // r, g, b, a are all values in the range [0, 255]
    Color(int r, int g, int b, int a = 255);
    // Constructs using ONLY x, y, z, alpha channel will be 1.0 regardless of w
    explicit Color(const Vector& v);
    //Constructs using ONLY x, y, z, alpha channel will be 1.0 regardless of w
    explicit Color(const Point& p);
    // Clears the color values to either white or black based on the bool parameter
    void Clear(bool toWhite = true);
    // Normalizes color to be used for graphics pipeline. [0,255] -> [0,1]
    Color& NormalizeColor(void);
    // Returns a normalized color to be used for graphics pipeline. [0,255] -> [0,1]
    Color GetNormalizedColor(void) const;
    // Standardizes color to be used for user. [0,1] -> [0,255]
    Color& StandardizeColor(void);
    // Returns a standardized color to be used for user. [0,1] -> [0,255]
    Color GetStandardizedColor(void) const;
    // Clamps color between the range [0,1]
    Color& Clamp(void);
    // Creates a random variation on a color based on the passed in values
    Color CreateVariance(const Color& var) const;
    // Negates the color and returns the result
    Color operator-(void) const;
    // Subtracts one color from another and returns the result
    // The subtracted red, green, blue values of rhs are scaled by its alpha channel
    Color operator-(const Color& rhs) const;
    // Adds one color to another and returns the result
    // The added red, green, blue values of rhs are scaled by its alpha channel
    Color operator+(const Color& rhs) const;
    // Multiplies the alpha channel by a scalar value and returns the result
    // Only affects the alpha channel; red, green, blue channels remain unaffected
    Color operator*(float scalar) const;
    // Divides the alpha channel by a scalar value and returns the result
    // Only affects alpha channel; red, green, blue channels remain unaffected
    Color operator/(float scalar) const;
    // Subtracts one color from another in place
    // The subtracted red, green, blue values of rhs is scaled by its alpha channel
    Color& operator-=(const Color& rhs);
    // Adds one color to another in place
    // The added red, green, blue values of rhs is scaled by its alpha channel
    Color& operator+=(const Color& rhs);
    // Multiplies the alpha channel by a scalar value in place
    // Only affects the alpha channel; red, green, blue channels remain unaffected
    Color& operator*=(float scalar);
    // Divides the alpha channel by a scalar value in place
    // Only affects the alpha channel; red, green, blue channels remain unaffected
    Color& operator/=(float scalar);
    bool operator==(const Color& rhs) const;
    bool operator!=(const Color& rhs) const;
    static Color BuildRandomColor(float a = 1.0f);
    // Builds a random color that is mostly red
    static Color BuildRandomRed(float a = 1.0f);
    //Builds a random color that is mostly green
    static Color BuildRandomGreen(float a = 1.0f);
    // Builds a random color that is mostly blue
    static Color BuildRandomBlue(float a = 1.0f);
    // Prints the color to the specified output stream
    // Prints the color in the form:
    // |red: r|green: g|blue: b|alpha: a|
    friend std::ostream& operator<<(std::ostream &os, const Color &c);

    static const Color RED;
    static const Color ORANGE;
    static const Color YELLOW;
    static const Color GREEN;
    static const Color BLUE;
    static const Color VIOLET;
    static const Color PINK;
    static const Color BROWN;
    static const Color GRAY;
    static const Color BLACK;
    static const Color WHITE;

  public:
    union
    {
      struct { float r, g, b, a; };
      float v[4];
    };
  };

  typedef const Color* ColorPtr;
}// namespace Maths
