#pragma once

#include "Utils.h"
#include "Vector.h"
#include "ICommonRenderer.h"
#include "ICommonWindow.h"
#include "ICommonCamera.h"

struct DrawGridData
{
  int gridSize;
  float upOffset;
  int upAxis;
  float gridColor[4];

  DrawGridData(int upAxis = 1)
    :gridSize(10),
    upOffset(0.001f),
    upAxis(upAxis)
  {
    gridColor[0] = 0.6f;
    gridColor[1] = 0.6f;
    gridColor[2] = 0.6f;
    gridColor[3] = 1.f;
  }
};

enum EnumSphereLevelOfDetail
{
  SPHERE_LOD_POINT_SPRITE = 0,
  SPHERE_LOD_LOW,
  SPHERE_LOD_MEDIUM,
  SPHERE_LOD_HIGH,

};


class CommonGraphicsApp
{
public: 

  enum drawText3DOption
  {
    eDrawText3D_OrtogonalFaceCamera = 1,
    eDrawText3D_TrueType = 2,
    eDrawText3D_TrackObject = 4,
  };
  class ICommonWindow*	m_window;
  class ICommonRenderer*	m_renderer;
  class ICommonParameter*	m_parameterInterface;
  class ICommon2dCanvas*	m_2dCanvasInterface;

  bool	m_mouseInitialized;
  bool	m_leftMouseButton;
  bool	m_middleMouseButton;
  bool	m_rightMouseButton;
  float m_wheelMultiplier;
  float m_mouseMoveMultiplier;
  float	m_mouseXpos;
  float	m_mouseYpos;
  float	m_backgroundColorRGB[3];

  CommonGraphicsApp()
    :m_window(nullptr),
    m_renderer(nullptr),
    m_parameterInterface(nullptr),
    m_2dCanvasInterface(nullptr),
    m_leftMouseButton(false),
    m_middleMouseButton(false),
    m_rightMouseButton(false),
    m_wheelMultiplier(0.01f),
    m_mouseMoveMultiplier(0.4f),
    m_mouseXpos(0.f),
    m_mouseYpos(0.f),
    m_mouseInitialized(false)
  {
    m_backgroundColorRGB[0] = 0.7;
    m_backgroundColorRGB[1] = 0.7;
    m_backgroundColorRGB[2] = 0.8;
  }
  virtual ~CommonGraphicsApp()
  {
  }

  virtual void DumpNextFrameToPng(const char* pngFilename) {}
  virtual void DumpFramesToVideo(const char* mp4Filename) {}

  virtual void GetScreenPixels(unsigned char* rgbaBuffer, int bufferSizeInBytes, float* depthBuffer, int depthBufferSizeInBytes) {}

  virtual void GetBackgroundColor(float* red, float* green, float* blue) const
  {
    if (red)
      *red = m_backgroundColorRGB[0];
    if (green)
      *green = m_backgroundColorRGB[1];
    if (blue)
      *blue = m_backgroundColorRGB[2];
  }
  virtual void SetBackgroundColor(float red, float green, float blue)
  {
    m_backgroundColorRGB[0] = red;
    m_backgroundColorRGB[1] = green;
    m_backgroundColorRGB[2] = blue;
  }
  virtual void SetMouseWheelMultiplier(float mult)
  {
    m_wheelMultiplier = mult;
  }
  virtual float GetMouseWheelMultiplier() const
  {
    return m_wheelMultiplier;
  }

  virtual void SetMouseMoveMultiplier(float mult)
  {
    m_mouseMoveMultiplier = mult;
  }

  virtual float GetMouseMoveMultiplier() const
  {
    return m_mouseMoveMultiplier;
  }



  virtual void DrawGrid(DrawGridData data = DrawGridData()) = 0;
  virtual void SetUpAxis(int axis) = 0;
  virtual int GetUpAxis() const = 0;

  virtual void SwapBuffer() = 0;
  virtual void DrawText(const char* txt, int posX, int posY)
  {
    float size = 1;
    float colorRGBA[4] = { 0,0,0,1 };
    DrawText(txt, posX, posY, size, colorRGBA);
  }

  virtual void DrawText(const char* txt, int posX, int posY, float size)
  {
    float colorRGBA[4] = { 0,0,0,1 };
    DrawText(txt, posX, posY, size, colorRGBA);
  }
  virtual void DrawText(const char* txt, int posX, int posY, float size, float colorRGBA[4]) = 0;
  virtual void DrawText3D(const char* txt, float posX, float posY, float posZ, float size) = 0;
  virtual void DrawText3D(const char* txt, float position[3], float orientation[4], float color[4], float size, int optionFlag) = 0;
  virtual void DrawTexturedRect(float x0, float y0, float x1, float y1, float color[4], float u0, float v0, float u1, float v1, int useRGBA) = 0;
  virtual int	RegisterCubeShape(float halfExtentsX, float halfExtentsY, float halfExtentsZ, int textureIndex = -1, float textureScaling = 1) = 0;
  virtual int	RegisterGraphicsUnitSphereShape(EnumSphereLevelOfDetail lod, int textureId = -1) = 0;


  virtual void RegisterGrid(int xres, int yres, float color0[4], float color1[4]) = 0;

  void DefaultMouseButtonCallback(int button, int state, float x, float y)
  {
    if (button == 0)
      m_leftMouseButton = (state == 1);
    if (button == 1)
      m_middleMouseButton = (state == 1);

    if (button == 2)
      m_rightMouseButton = (state == 1);

    m_mouseXpos = x;
    m_mouseYpos = y;
    m_mouseInitialized = true;
  }
  void DefaultMouseMoveCallback(float x, float y)
  {

    if (m_window && m_renderer)
    {
      ICommonCamera* camera = m_renderer->GetActiveCamera();

      bool isAltPressed = m_window->IsModifierKeyPressed(KEY_ALT);
      bool isControlPressed = m_window->IsModifierKeyPressed(KEY_CONTROL);


      if (isAltPressed || isControlPressed)
      {
        float xDelta = x - m_mouseXpos;
        float yDelta = y - m_mouseYpos;
        float cameraDistance = camera->GetCameraDistance();
        float pitch = camera->GetCameraPitch();
        float yaw = camera->GetCameraYaw();

        float targPos[3];
        float	camPos[3];

        camera->GetCameraTargetPosition(targPos);
        camera->GetCameraPosition(camPos);

        Maths::Vector cameraPosition = Maths::Vector(float(camPos[0]),
          float(camPos[1]),
          float(camPos[2]));

        Maths::Vector cameraTargetPosition = Maths::Vector(float(targPos[0]),
          float(targPos[1]),
          float(targPos[2]));
        Maths::Vector cameraUp = Maths::Vector::ZERO;
        cameraUp[camera->GetCameraUpAxis()] = 1.f;

        if (m_leftMouseButton)
        {
          pitch -= yDelta*m_mouseMoveMultiplier;
          yaw -= xDelta*m_mouseMoveMultiplier;
        }

        if (m_middleMouseButton)
        {
          cameraTargetPosition += cameraUp * yDelta*0.01;


          Maths::Vector fwd = cameraTargetPosition - cameraPosition;
          Maths::Vector side = cameraUp.Cross(fwd);
          side.Normalize();
          cameraTargetPosition += side * xDelta*0.01;

        }
        if (m_rightMouseButton)
        {
          cameraDistance -= xDelta*0.01f;
          cameraDistance -= yDelta*0.01f;
          if (cameraDistance<1)
            cameraDistance = 1;
          if (cameraDistance>1000)
            cameraDistance = 1000;
        }
        camera->SetCameraDistance(cameraDistance);
        camera->SetCameraPitch(pitch);
        camera->SetCameraYaw(yaw);
        camera->SetCameraTargetPosition(cameraTargetPosition[0], cameraTargetPosition[1], cameraTargetPosition[2]);

      }

    }//m_window &&  m_renderer

    m_mouseXpos = x;
    m_mouseYpos = y;
    m_mouseInitialized = true;
  }

  void DefaultWheelCallback(float deltax, float deltay)
  {

    if (m_renderer)
    {
      Maths::Vector cameraTargetPosition, cameraPosition, cameraUp = Maths::Vector::ZERO;
      cameraUp[GetUpAxis()] = 1;
      ICommonCamera* camera = m_renderer->GetActiveCamera();

      camera->GetCameraPosition(cameraPosition);
      camera->GetCameraTargetPosition(cameraTargetPosition);

      if (!m_leftMouseButton)
      {

        float cameraDistance = camera->GetCameraDistance();
        if (deltay<0 || cameraDistance>1)
        {
          cameraDistance -= deltay*0.01f;
          if (cameraDistance<1)
            cameraDistance = 1;
          camera->SetCameraDistance(cameraDistance);

        }
        else
        {

          Maths::Vector fwd = cameraTargetPosition - cameraPosition;
          fwd.Normalize();
          cameraTargetPosition += fwd * deltay * m_wheelMultiplier;// todo: expose it in the GUI?
        }
      }
      else
      {
        if (Maths::AbsVal(deltax) > Maths::AbsVal(deltay))
        {
          Maths::Vector fwd = cameraTargetPosition - cameraPosition;
          Maths::Vector side = cameraUp.Cross(fwd);
          side.Normalize();
          cameraTargetPosition += side * deltax * m_wheelMultiplier;

        }
        else
        {
          cameraTargetPosition -= cameraUp * deltay * m_wheelMultiplier;

        }
      }

      camera->SetCameraTargetPosition(cameraTargetPosition[0], cameraTargetPosition[1], cameraTargetPosition[2]);
    }

  }


};
