#pragma once
#include "Vector.h"
#include "Geometry/Point.h"
#include "Matrix4.h"

namespace Maths
{

  class Camera
  {
  public:
    Camera();

    void Update(float dt);

    Point WorldFromScreen(int screenWidth, int screenHeight, int screenX, int screenY);

    void Rotate(float yaw, float pitch, float roll);
    void Translate(float x, float y);

    void SetSpin(float spin);
    void SetTilt(float tilt);
    void SetZoom(double zoom);
    void AddZoom(float amount);

    void AddSpin(float spin);
    void AddTilt(float tilt);

    Point& GetPos();
    Point& GetFocusPos();
    Vector& GetLookDir();
    Vector& GetUpDir();

    float GetFocalLength()const;
    float GetAperture()const;
    float GetAspectRatio()const;
    float GetNear()const;
    float GetFar()const;
    float GetEyeSeparation()const;
    double GetZoom()const;
    float GetFOV()const;


    void SetAspect(float ratio);
    void SetAspect(float width, float height);
    void SetAperture(float ap);
    void SetNear(float near);
    void SetFar(float far);
    void SetFocalLength(float fl);
    void SetEyeSeparation(float es);
    void SetPos(Point pos);
    void SetFocusPos(Point fp);
    void SetLookDir(Vector ld);
    void SetUpDir(Vector ud);
    void SetFOV(float fov);

  protected:
    void UpdateMatrixProjection(void);
    void UpdateMatrixView(void);
    void UpdateProjectionFromCamera(void);
    void UpdateCameraFromWorld(void);

  private:
    Matrix4 m_matrixProjection;
    Matrix4 m_matrixView;

    Matrix4 m_cameraFromWorld;
    Matrix4 m_projectionFromCamera;

    Vector m_lookDir;
    Vector m_upDir;
    Point m_focusPos; // this is where everything will rotate about
    Point m_pos;

    float m_aspectRatio;
    float m_near;
    float m_far;

    float m_fov;
    float m_focalLength;
    float m_aperture;
    float m_eyeSeparation; // not used, would be if doing stereo views

    float m_spin;
    float m_tilt;
    double m_zoom;
  };  


} // namespace Maths
