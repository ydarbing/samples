#pragma once

#include"Vector.h"
#include"Transform.h"
#include"Matrix3.h"

namespace Maths 
{

  inline void TransformAabb(const Vector& localAabbMin, const Vector& localAabbMax, float margin, const Transform& trans, Vector& aabbMinOut, Vector& aabbMaxOut)
  {
    assert(localAabbMin.x <= localAabbMax.x);
    assert(localAabbMin.y <= localAabbMax.y);
    assert(localAabbMin.z <= localAabbMax.z);
    Vector localHalfExtents = float(0.5)*(localAabbMax - localAabbMin);
    localHalfExtents += Vector(margin, margin, margin);

    Vector localCenter = float(0.5)*(localAabbMax + localAabbMin);
    Matrix3 abs_b = trans.GetBasis().Absolute();
    Vector center = trans(localCenter);
    Vector extent = localHalfExtents.Dot3(abs_b[0], abs_b[1], abs_b[2]);
    aabbMinOut = center - extent;
    aabbMaxOut = center + extent;
  }
}// namespace Maths