#pragma once
#include "Utils.h"
#include <ostream>
#include <assert.h>

#include "Vector.h"
#include "Quaternion.h"

namespace Maths
{

  class Matrix3
  {
  private:
    ///Data storage for the matrix, each vector is a row of the matrix
    Vector m_el[3];

  public:
    Matrix3() {}

    explicit Matrix3(const Quaternion& q) { SetRotation(q); }

    Matrix3(const float& xx, const float& xy, const float& xz,
      const float& yx, const float& yy, const float& yz,
      const float& zx, const float& zy, const float& zz)
    {
      SetValue(xx, xy, xz,
        yx, yy, yz,
        zx, zy, zz);
    }

    Matrix3(const Matrix3& other)
    {
      m_el[0] = other.m_el[0];
      m_el[1] = other.m_el[1];
      m_el[2] = other.m_el[2];
    }

    Matrix3& operator=(const Matrix3& other)
    {
      m_el[0] = other.m_el[0];
      m_el[1] = other.m_el[1];
      m_el[2] = other.m_el[2];
      return *this;
    }
    Vector GetColumn(int i) const
    {
      return Vector(m_el[0][i], m_el[1][i], m_el[2][i]);
    }


    // Get a row of the matrix as a vector
    const Vector& GetRow(int i) const
    {
      assert(0 <= i && i < 4);
      return m_el[i];
    }

    // Get a mutable reference to a row of the matrix as a vector
    Vector&  operator[](int i)
    {
      assert(0 <= i && i < 4);
      return m_el[i];
    }

    // Get a const reference to a row of the matrix as a vector
    const Vector& operator[](int i) const
    {
      assert(0 <= i && i < 4);
      return m_el[i];
    }

    // Multiply by the target matrix on the right
    Matrix3& operator*=(const Matrix3& m);

    // Adds by the target matrix on the right
    Matrix3& operator+=(const Matrix3& m);

    // Substractss by the target matrix on the right
    Matrix3& operator-=(const Matrix3& m);

    // Set from the rotational part of a 4x4 OpenGL matrix
    void SetFromOpenGLSubMatrix(const float *m)
    {
      m_el[0].SetValue(m[0], m[4], m[8]);
      m_el[1].SetValue(m[1], m[5], m[9]);
      m_el[2].SetValue(m[2], m[6], m[10]);

    }
    /** Set the values of the matrix explicitly (row major)
          xx Top left
          xy Top Middle
          xz Top Right
          yx Middle Left
          yy Middle Middle
          yz Middle Right
          zx Bottom Left
          zy Bottom Middle
          zz Bottom Right
    */
    void SetValue(const float& xx, const float& xy, const float& xz,
      const float& yx, const float& yy, const float& yz,
      const float& zx, const float& zy, const float& zz)
    {
      m_el[0].SetValue(xx, xy, xz);
      m_el[1].SetValue(yx, yy, yz);
      m_el[2].SetValue(zx, zy, zz);
    }

    // Set the matrix from a quaternion 
    void SetRotation(const Quaternion& q)
    {
      float d = q.SquaredLength();
      assert(d != float(0.0));
      float s = float(2.0) / d;

      float xs = q.x * s, ys = q.y * s, zs = q.z * s;
      float wx = q.w * xs, wy = q.w * ys, wz = q.w * zs;
      float xx = q.x * xs, xy = q.x * ys, xz = q.x * zs;
      float yy = q.y * ys, yz = q.y * zs, zz = q.z * zs;
      SetValue(
        float(1.0) - (yy + zz), xy - wz, xz + wy,
        xy + wz, float(1.0) - (xx + zz), yz - wx,
        xz - wy, yz + wx, float(1.0) - (xx + yy));
    }

    /* Set matrix from euler angles using YPR around YXZ respectively
    *  yaw - Yaw about Y axis
    *  pitch - Pitch about X axis
    *  roll - Roll about Z axis
    */
    void SetEulerYPR(const float& yaw, const float& pitch, const float& roll)
    {
      SetEulerZYX(roll, pitch, yaw);
    }

    /*Set matrix from euler angles YPR around ZYX axes
    * eulerX - Roll about X axis
    * eulerY - Pitch around Y axis
    * eulerZ - Yaw about Z axis
    *
    * These angles are used to produce a rotation matrix. The euler
    * angles are applied in ZYX order. I.e a vector is first rotated
    * about X then Y and then Z
    **/
    void SetEulerZYX(float eulerX, float eulerY, float eulerZ) {

      float ci(Maths::Cos(eulerX));
      float cj(Maths::Cos(eulerY));
      float ch(Maths::Cos(eulerZ));
      float si(Maths::Sin(eulerX));
      float sj(Maths::Sin(eulerY));
      float sh(Maths::Sin(eulerZ));
      float cc = ci * ch;
      float cs = ci * sh;
      float sc = si * ch;
      float ss = si * sh;

      SetValue(cj * ch, sj * sc - cs, sj * cc + ss,
        cj * sh, sj * ss + cc, sj * cs - sc,
        -sj, cj * si, cj * ci);
    }

    void SetIdentity()
    {
      SetValue(float(1.0), float(0.0), float(0.0),
        float(0.0), float(1.0), float(0.0),
        float(0.0), float(0.0), float(1.0));
    }

    static const Matrix3&	GetIdentity()
    {
      static const Matrix3
        identityMatrix(
          float(1.0), float(0.0), float(0.0),
          float(0.0), float(1.0), float(0.0),
          float(0.0), float(0.0), float(1.0));

      return identityMatrix;
    }

    /* Fill the rotational part of an OpenGL matrix and clear the shear/perspective
    * m - The array to be filled */
    void GetOpenGLSubMatrix(float *m) const
    {
      m[0] = float(m_el[0].x);
      m[1] = float(m_el[1].x);
      m[2] = float(m_el[2].x);
      m[3] = float(0.0);
      m[4] = float(m_el[0].y);
      m[5] = float(m_el[1].y);
      m[6] = float(m_el[2].y);
      m[7] = float(0.0);
      m[8] = float(m_el[0].z);
      m[9] = float(m_el[1].z);
      m[10] = float(m_el[2].z);
      m[11] = float(0.0);
    }

    /* Get the matrix represented as a quaternion
    * q - The quaternion which will be set */
    void GetRotation(Quaternion& q) const
    {
      float trace = m_el[0].x + m_el[1].y + m_el[2].z;

      float temp[4];

      if (trace > float(0.0))
      {
        float s = sqrtf(trace + float(1.0));
        temp[3] = (s * float(0.5));
        s = float(0.5) / s;

        temp[0] = ((m_el[2].y - m_el[1].z) * s);
        temp[1] = ((m_el[0].z - m_el[2].x) * s);
        temp[2] = ((m_el[1].x - m_el[0].y) * s);
      }
      else
      {
        int i = m_el[0].x < m_el[1].y ?
          (m_el[1].y < m_el[2].z ? 2 : 1) :
          (m_el[0].x < m_el[2].z ? 2 : 0);
        int j = (i + 1) % 3;
        int k = (i + 2) % 3;

        float s = sqrtf(m_el[i][i] - m_el[j][j] - m_el[k][k] + float(1.0));
        temp[i] = s * float(0.5);
        s = float(0.5) / s;

        temp[3] = (m_el[k][j] - m_el[j][k]) * s;
        temp[j] = (m_el[j][i] + m_el[i][j]) * s;
        temp[k] = (m_el[k][i] + m_el[i][k]) * s;
      }
      q.SetValue(temp[0], temp[1], temp[2], temp[3]);
    }
    /* Get the matrix represented as euler angles around YXZ, roundtrip with setEulerYPR
    * yaw - Yaw around Y axis
    * pitch - Pitch around X axis
    * roll - Roll around Z axis */
    void GetEulerYPR(float& yaw, float& pitch, float& roll) const
    {

      // first use the normal calculus
      yaw = float(atan2f(m_el[1].x, m_el[0].x));
      pitch = float(Maths::Asin(-m_el[2].x));
      roll = float(atan2f(m_el[2].y, m_el[2].z));

      // on pitch = +/-HalfPI
      if (fabsf(pitch) == Maths::HALF_PI)
      {
        if (yaw > 0)
          yaw -= Maths::PI;
        else
          yaw += Maths::PI;

        if (roll > 0)
          roll -= Maths::PI;
        else
          roll += Maths::PI;
      }
    }


    /* Get the matrix represented as euler angles around ZYX
    * yaw Yaw around X axis
    * pitch Pitch around Y axis
    * roll around X axis
    * solution_number Which solution of two possible solutions ( 1 or 2) are possible values*/
    void GetEulerZYX(float& yaw, float& pitch, float& roll, unsigned int solution_number = 1) const
    {
      struct Euler
      {
        float yaw;
        float pitch;
        float roll;
      };

      Euler euler_out;
      Euler euler_out2; //second solution - get the pointer to the raw data

                        // Check that pitch is not at a singularity
      if (fabsf(m_el[2].x) >= 1)
      {
        euler_out.yaw = 0;
        euler_out2.yaw = 0;

        // From difference of angles formula
        float delta = atan2f(m_el[0].x, m_el[0].z);
        if (m_el[2].x > 0)  //gimbal locked up
        {
          euler_out.pitch = Maths::PI / float(2.0);
          euler_out2.pitch = Maths::PI / float(2.0);
          euler_out.roll = euler_out.pitch + delta;
          euler_out2.roll = euler_out.pitch + delta;
        }
        else // gimbal locked down
        {
          euler_out.pitch = -Maths::PI / float(2.0);
          euler_out2.pitch = -Maths::PI / float(2.0);
          euler_out.roll = -euler_out.pitch + delta;
          euler_out2.roll = -euler_out.pitch + delta;
        }
      }
      else
      {
        euler_out.pitch = -Maths::Asin(m_el[2].x);
        euler_out2.pitch = Maths::PI - euler_out.pitch;

        euler_out.roll = atan2f(m_el[2].y / Cos(euler_out.pitch),
          m_el[2].z / Cos(euler_out.pitch));
        euler_out2.roll = atan2f(m_el[2].y / Cos(euler_out2.pitch),
          m_el[2].z / Cos(euler_out2.pitch));

        euler_out.yaw = atan2f(m_el[1].x / Cos(euler_out.pitch),
          m_el[0].x / Cos(euler_out.pitch));
        euler_out2.yaw = atan2f(m_el[1].x / Cos(euler_out2.pitch),
          m_el[0].x / Cos(euler_out2.pitch));
      }

      if (solution_number == 1)
      {
        yaw = euler_out.yaw;
        pitch = euler_out.pitch;
        roll = euler_out.roll;
      }
      else
      {
        yaw = euler_out2.yaw;
        pitch = euler_out2.pitch;
        roll = euler_out2.roll;
      }
    }

    /* Create a scaled copy of the matrix
    * s - Scaling vector The elements of the vector will scale each column */
    Matrix3 Scaled(const Vector& s) const
    {
      return Matrix3(
        m_el[0].x * s.x, m_el[0].y * s.y, m_el[0].z * s.z,
        m_el[1].x * s.x, m_el[1].y * s.y, m_el[1].z * s.z,
        m_el[2].x * s.x, m_el[2].y * s.y, m_el[2].z * s.z);
    }

    float Determinant() const;
    Matrix3 Adjoint() const;
    /* Return the matrix with all values non negative */
    Matrix3 Absolute() const;
    /* Return the transpose of the matrix */
    Matrix3 Transpose() const;
    /* Return the inverse of the matrix */
    Matrix3 Inverse() const;

    /// Solve A * x = b, where b is a column vector. This is more efficient
    /// than computing the inverse in one-shot cases.
    ///Solve33 is from Box2d, thanks to Erin Catto,
    Vector Solve33(const Vector& b) const
    {
      Vector col1 = GetColumn(0);
      Vector col2 = GetColumn(1);
      Vector col3 = GetColumn(2);

      float det = Dot(col1, Cross(col2, col3));
      if (fabsf(det) > Maths::EPSILON)
      {
        det = 1.0f / det;
      }
      Vector x;
      x[0] = det * Dot(b, Cross(col2, col3));
      x[1] = det * Dot(col1, Cross(b, col3));
      x[2] = det * Dot(col1, Cross(col2, b));
      return x;
    }

    Matrix3 TransposeTimes(const Matrix3& m) const;
    Matrix3 TimesTranspose(const Matrix3& m) const;

    inline float Tdotx(const Vector& v) const
    {
      return m_el[0].x * v.x + m_el[1].x * v.y + m_el[2].x * v.z;
    }
    inline float Tdoty(const Vector& v) const
    {
      return m_el[0].y * v.x + m_el[1].y * v.y + m_el[2].y * v.z;
    }
    inline float Tdotz(const Vector& v) const
    {
      return m_el[0].z * v.x + m_el[1].z * v.y + m_el[2].z * v.z;
    }

    ///extractRotation is from "A robust method to extract the rotational part of deformations"
    ///See http://dl.acm.org/citation.cfm?doid=2994258.2994269
    inline void ExtractRotation(Quaternion &q, float tolerance = 1.0e-9, int maxIter = 100)
    {
      int iter = 0;
      float w;
      const Matrix3& A = *this;
      for (iter = 0; iter < maxIter; iter++)
      {
        Matrix3 R(q);
        Vector omega = (R.GetColumn(0).Cross(A.GetColumn(0)) + R.GetColumn(1).Cross(A.GetColumn(1))
          + R.GetColumn(2).Cross(A.GetColumn(2))
          ) * (float(1.0) / fabsf(R.GetColumn(0).Dot(A.GetColumn(0)) + R.GetColumn
          (1).Dot(A.GetColumn(1)) + R.GetColumn(2).Dot(A.GetColumn(2))) +
            tolerance);
        w = omega.Normalize();
        if (w < tolerance)
          break;
        q = Quaternion(Vector((float(1.0) / w) * omega), w) *
          q;
        q.Normalize();
      }
    }



    /* diagonalizes this matrix
    * rot stores the rotation from the coordinate system in which the matrix is diagonal to the original
    * coordinate system, i.e., old_this = rot * new_this * rot^T.
    * threshold - See iteration
    * maxIter - The iteration stops when we hit the given tolerance or when maxIter have been executed.
    */
    void Diagonalize(Matrix3& rot, float tolerance = 1.0e-9, int maxIter = 100)
    {
      Quaternion r;
      r = Quaternion::IDENTITY;
      ExtractRotation(r, tolerance, maxIter);
      rot.SetRotation(r);
      Matrix3 rotInv = Matrix3(r.Inverse());
      Matrix3 old = *this;
      SetValue(old.Tdotx(rotInv[0]), old.Tdoty(rotInv[0]), old.Tdotz(rotInv[0]),
        old.Tdotx(rotInv[1]), old.Tdoty(rotInv[1]), old.Tdotz(rotInv[1]),
        old.Tdotx(rotInv[2]), old.Tdoty(rotInv[2]), old.Tdotz(rotInv[2]));
    }




    /* Calculate the matrix cofactor
    * r1 The first row to use for calculating the cofactor
    * c1 The first column to use for calculating the cofactor
    * r1 The second row to use for calculating the cofactor
    * c1 The second column to use for calculating the cofactor
    * See http://en.wikipedia.org/wiki/Cofactor_(linear_algebra) for more details
    */
    float Cofac(int r1, int c1, int r2, int c2) const
    {
      return m_el[r1][c1] * m_el[r2][c2] - m_el[r1][c2] * m_el[r2][c1];
    }
  };



  inline Matrix3& Matrix3::operator*=(const Matrix3& m)
  {
    SetValue(
      m.Tdotx(m_el[0]), m.Tdoty(m_el[0]), m.Tdotz(m_el[0]),
      m.Tdotx(m_el[1]), m.Tdoty(m_el[1]), m.Tdotz(m_el[1]),
      m.Tdotx(m_el[2]), m.Tdoty(m_el[2]), m.Tdotz(m_el[2]));

    return *this;
  }

  inline Matrix3& Matrix3::operator+=(const Matrix3& m)
  {
    SetValue(
      m_el[0][0] + m.m_el[0][0],
      m_el[0][1] + m.m_el[0][1],
      m_el[0][2] + m.m_el[0][2],
      m_el[1][0] + m.m_el[1][0],
      m_el[1][1] + m.m_el[1][1],
      m_el[1][2] + m.m_el[1][2],
      m_el[2][0] + m.m_el[2][0],
      m_el[2][1] + m.m_el[2][1],
      m_el[2][2] + m.m_el[2][2]);

    return *this;
  }

  inline Matrix3 operator*(const Matrix3& m, const float& k)
  {
    return Matrix3(
      m[0].x * k, m[0].y * k, m[0].z * k,
      m[1].x * k, m[1].y * k, m[1].z * k,
      m[2].x * k, m[2].y * k, m[2].z * k);
  }

  inline Matrix3 operator+(const Matrix3& m1, const Matrix3& m2)
  {
    return Matrix3(
      m1[0][0] + m2[0][0],
      m1[0][1] + m2[0][1],
      m1[0][2] + m2[0][2],

      m1[1][0] + m2[1][0],
      m1[1][1] + m2[1][1],
      m1[1][2] + m2[1][2],

      m1[2][0] + m2[2][0],
      m1[2][1] + m2[2][1],
      m1[2][2] + m2[2][2]);
  }

  inline Matrix3 operator-(const Matrix3& m1, const Matrix3& m2)
  {
    return Matrix3(
      m1[0][0] - m2[0][0],
      m1[0][1] - m2[0][1],
      m1[0][2] - m2[0][2],

      m1[1][0] - m2[1][0],
      m1[1][1] - m2[1][1],
      m1[1][2] - m2[1][2],

      m1[2][0] - m2[2][0],
      m1[2][1] - m2[2][1],
      m1[2][2] - m2[2][2]);
  }


  inline Matrix3& Matrix3::operator-=(const Matrix3& m)
  {
    SetValue(
      m_el[0][0] - m.m_el[0][0],
      m_el[0][1] - m.m_el[0][1],
      m_el[0][2] - m.m_el[0][2],
      m_el[1][0] - m.m_el[1][0],
      m_el[1][1] - m.m_el[1][1],
      m_el[1][2] - m.m_el[1][2],
      m_el[2][0] - m.m_el[2][0],
      m_el[2][1] - m.m_el[2][1],
      m_el[2][2] - m.m_el[2][2]);

    return *this;
  }


  inline float Matrix3::Determinant() const
  {
    return Triple((*this)[0], (*this)[1], (*this)[2]);
  }


  inline Matrix3 Matrix3::Absolute() const
  {
    return Matrix3(
      fabsf(m_el[0].x), fabsf(m_el[0].y), fabsf(m_el[0].z),
      fabsf(m_el[1].x), fabsf(m_el[1].y), fabsf(m_el[1].z),
      fabsf(m_el[2].x), fabsf(m_el[2].y), fabsf(m_el[2].z));

  }

  inline Matrix3 Matrix3::Transpose() const
  {
    return Matrix3(m_el[0].x, m_el[1].x, m_el[2].x,
      m_el[0].y, m_el[1].y, m_el[2].y,
      m_el[0].z, m_el[1].z, m_el[2].z);
  }

  inline Matrix3 Matrix3::Adjoint() const
  {
    return Matrix3(Cofac(1, 1, 2, 2), Cofac(0, 2, 2, 1), Cofac(0, 1, 1, 2),
      Cofac(1, 2, 2, 0), Cofac(0, 0, 2, 2), Cofac(0, 2, 1, 0),
      Cofac(1, 0, 2, 1), Cofac(0, 1, 2, 0), Cofac(0, 0, 1, 1));
  }

  inline Matrix3 Matrix3::Inverse() const
  {
    Vector co(Cofac(1, 1, 2, 2), Cofac(1, 2, 2, 0), Cofac(1, 0, 2, 1));
    float det = (*this)[0].Dot(co);
    assert(det != float(0.0));
    float s = float(1.0) / det;
    return Matrix3(co.x * s, Cofac(0, 2, 2, 1) * s, Cofac(0, 1, 1, 2) * s,
      co.y * s, Cofac(0, 0, 2, 2) * s, Cofac(0, 2, 1, 0) * s,
      co.z * s, Cofac(0, 1, 2, 0) * s, Cofac(0, 0, 1, 1) * s);
  }

  inline Matrix3 Matrix3::TransposeTimes(const Matrix3& m) const
  {
    return Matrix3(
      m_el[0].x * m[0].x + m_el[1].x * m[1].x + m_el[2].x * m[2].x,
      m_el[0].x * m[0].y + m_el[1].x * m[1].y + m_el[2].x * m[2].y,
      m_el[0].x * m[0].z + m_el[1].x * m[1].z + m_el[2].x * m[2].z,
      m_el[0].y * m[0].x + m_el[1].y * m[1].x + m_el[2].y * m[2].x,
      m_el[0].y * m[0].y + m_el[1].y * m[1].y + m_el[2].y * m[2].y,
      m_el[0].y * m[0].z + m_el[1].y * m[1].z + m_el[2].y * m[2].z,
      m_el[0].z * m[0].x + m_el[1].z * m[1].x + m_el[2].z * m[2].x,
      m_el[0].z * m[0].y + m_el[1].z * m[1].y + m_el[2].z * m[2].y,
      m_el[0].z * m[0].z + m_el[1].z * m[1].z + m_el[2].z * m[2].z);
  }

  inline Matrix3 Matrix3::TimesTranspose(const Matrix3& m) const
  {
    return Matrix3(
      m_el[0].Dot(m[0]), m_el[0].Dot(m[1]), m_el[0].Dot(m[2]),
      m_el[1].Dot(m[0]), m_el[1].Dot(m[1]), m_el[1].Dot(m[2]),
      m_el[2].Dot(m[0]), m_el[2].Dot(m[1]), m_el[2].Dot(m[2]));
  }

  inline Vector operator*(const Matrix3& m, const Vector& v)
  {
    return Vector(m[0].Dot(v), m[1].Dot(v), m[2].Dot(v));
  }
  
  
  inline Vector operator*(const Vector& v, const Matrix3& m)
  {
    return Vector(m.Tdotx(v), m.Tdoty(v), m.Tdotz(v));
  }
  
  inline Matrix3 operator*(const Matrix3& m1, const Matrix3& m2)
  {
    return Matrix3(
      m2.Tdotx(m1[0]), m2.Tdoty(m1[0]), m2.Tdotz(m1[0]),
      m2.Tdotx(m1[1]), m2.Tdoty(m1[1]), m2.Tdotz(m1[1]),
      m2.Tdotx(m1[2]), m2.Tdoty(m1[2]), m2.Tdotz(m1[2]));
  }
  
  inline bool operator==(const Matrix3& m1, const Matrix3& m2)
  {
    return
      (m1[0][0] == m2[0][0] && m1[1][0] == m2[1][0] && m1[2][0] == m2[2][0] &&
        m1[0][1] == m2[0][1] && m1[1][1] == m2[1][1] && m1[2][1] == m2[2][1] &&
        m1[0][2] == m2[0][2] && m1[1][2] == m2[1][2] && m1[2][2] == m2[2][2]);
  }

}// namespace Maths
