#pragma once

#include "ICommonCallbacks.h"

struct WindowInfo
{
  int m_width;
  int m_height;
  bool m_fullscreen;
  int m_colorBitsPerPixel;
  void* m_winowHandle;
  const char* m_title;
  int m_glVersion;

  WindowInfo(int width = 1024, int height = 768)
    :m_width(width),
    m_height(height),
    m_fullscreen(false),
    m_colorBitsPerPixel(32),
    m_winowHandle(nullptr),
    m_title("title"),
    m_glVersion(3)
  {
  }
};


class ICommonWindow
{
public:
  ICommonWindow() {}

  virtual void CreateDefaultWindow(int width, int height, const char* title)
  {
    WindowInfo wi(width, height);
    wi.m_title = title;
    CreateWindow(wi);
  }
  virtual void  CreateWindow(const WindowInfo& wi) = 0;
  virtual int   GetWidth() const = 0;
  virtual int   GetHeight() const = 0;

  virtual void	CloseWindow() = 0;

  virtual void	RunMainLoop() = 0;
  virtual	float	GetTimeInSeconds() = 0;

  virtual bool	RequestedExit() const = 0;
  virtual	void	SetRequestExit() = 0;

  virtual	void	StartRendering() = 0;

  virtual	void	EndRendering() = 0;

  virtual bool  IsModifierKeyPressed(int key) = 0;

  virtual void SetMouseMoveCallback(MouseMoveCallback	mouseCallback) = 0;
  virtual MouseMoveCallback GetMouseMoveCallback() = 0;

  virtual void SetMouseButtonCallback(MouseButtonCallback	mouseCallback) = 0;
  virtual MouseButtonCallback GetMouseButtonCallback() = 0;

  virtual void SetResizeCallback(ResizeCallback	resizeCallback) = 0;
  virtual ResizeCallback GetResizeCallback() = 0;

  virtual void SetWheelCallback(WheelCallback wheelCallback) = 0;
  virtual WheelCallback GetWheelCallback() = 0;

  virtual void SetKeyboardCallback(KeyboardCallback	keyboardCallback) = 0;
  virtual KeyboardCallback SetKeyboardCallback() = 0;

  virtual void SetRenderCallback(RenderCallback renderCallback) = 0;

  virtual void SetWindowTitle(const char* title) = 0;

  virtual	float	GetRetinaScale() const = 0;
  virtual	void	SetAllowRetina(bool allow) = 0;
  virtual int FileOpenDialog(char* fileName, int maxFileNameLength) = 0;
};