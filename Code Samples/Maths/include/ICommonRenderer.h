#pragma once

struct ICommonCamera;

enum
{
  GL_TRIANGLES = 1,
  GL_POINTS
};

enum
{
  DEFAULT_RENDERMODE = 1,
  WIREFRAME_RENDERMODE
};



struct GfxVertexFormat0
{
  float x, y, z, w;
  float unused0, unused1, unused2, unused3;
  float u, v;
};

struct GfxVertexFormat1
{
  float x, y, z, w;
  float nx, ny, nz;
  float u, v;
};


struct ICommonRenderer
{
  virtual ~ICommonRenderer() {}
  virtual void Init() = 0;
  virtual void UpdateCamera(int upAxis) = 0;
  virtual void RemoveAllInstances() = 0;
  virtual void RemoveGraphicsInstance(int instanceUid) = 0;

  virtual const ICommonCamera* GetActiveCamera() const = 0;
  virtual ICommonCamera* GetActiveCamera() = 0;
  virtual void SetActiveCamera(ICommonCamera* cam) = 0;

  virtual void SetLightPosition(const float lightPos[3]) = 0;
  virtual void SetLightPosition(const double lightPos[3]) = 0;

  virtual void RenderScene() = 0;
  virtual void RenderSceneInternal(int renderMode = DEFAULT_RENDERMODE) {};
  virtual int GetScreenWidth() = 0;
  virtual int GetScreenHeight() = 0;

  virtual void Resize(int width, int height) = 0;

  virtual int RegisterGraphicsInstance(int shapeIndex, const float* position, const float* quaternion, const float* color, const float* scaling) = 0;
  virtual int RegisterGraphicsInstance(int shapeIndex, const double* position, const double* quaternion, const double* color, const double* scaling) = 0;
  virtual void DrawLines(const float* positions, const float color[4], int numPoints, int pointStrideInBytes, const unsigned int* indices, int numIndices, float pointDrawSize) = 0;
  virtual void DrawLine(const float from[4], const float to[4], const float color[4], float lineWidth) = 0;
  virtual void DrawLine(const double from[4], const double to[4], const double color[4], double lineWidth) = 0;
  virtual void DrawPoint(const float* position, const float color[4], float pointDrawSize) = 0;
  virtual void DrawPoint(const double* position, const double color[4], double pointDrawSize) = 0;
  virtual void DrawTexturedTriangleMesh(float worldPosition[3], float worldOrientation[4], const float* vertices, int numvertices, const unsigned int* indices, int numIndices, float color[4], int textureIndex = -1, int vertexLayout = 0) = 0;

  virtual int RegisterShape(const float* vertices, int numvertices, const int* indices, int numIndices, int primitiveType = B3_GL_TRIANGLES, int textureIndex = -1) = 0;
  virtual void UpdateShape(int shapeIndex, const float* vertices) = 0;

  virtual int RegisterTexture(const unsigned char* texels, int width, int height, bool flipPixelsY = true) = 0;
  virtual void UpdateTexture(int textureIndex, const unsigned char* texels, bool flipPixelsY = true) = 0;
  virtual void ActivateTexture(int textureIndex) = 0;
  virtual void ReplaceTexture(int shapeIndex, int textureIndex) {};

  virtual int GetShapeIndexFromInstance(int srcIndex) { return -1; }

  virtual bool ReadSingleInstanceTransformToCPU(float* position, float* orientation, int srcIndex) = 0;

  virtual void WriteSingleInstanceTransformToCPU(const float* position, const float* orientation, int srcIndex) = 0;
  virtual void WriteSingleInstanceTransformToCPU(const double* position, const double* orientation, int srcIndex) = 0;
  virtual void WriteSingleInstanceColorToCPU(const float* color, int srcIndex) = 0;
  virtual void WriteSingleInstanceColorToCPU(const double* color, int srcIndex) = 0;
  virtual void WriteSingleInstanceScaleToCPU(const float* scale, int srcIndex) = 0;
  virtual void WriteSingleInstanceScaleToCPU(const double* scale, int srcIndex) = 0;
  virtual void WriteSingleInstanceSpecularColorToCPU(const double* specular, int srcIndex) = 0;
  virtual void WriteSingleInstanceSpecularColorToCPU(const float* specular, int srcIndex) = 0;


  virtual int GetTotalNumInstances() const = 0;

  virtual void WriteTransforms() = 0;

  virtual void ClearZBuffer() = 0;

  //This is internal access to OpenGL3+ features, mainly used for OpenCL-OpenGL interop
  //Only the GLInstancingRenderer supports it, just return 0 otherwise.
  virtual struct	GLInstanceRendererInternalData* GetInternalData() = 0;
};

template <typename T>
inline int ProjectWorldCoordToScreen(T objx, T objy, T objz,
  const T modelMatrix[16],
  const T projMatrix[16],
  const int viewport[4],
  T *winx, T *winy, T *winz)
{
  int i;
  T in2[4];
  T tmp[4];

  in2[0] = objx;
  in2[1] = objy;
  in2[2] = objz;
  in2[3] = T(1.0);

  for (i = 0; i<4; i++)
  {
    tmp[i] = in2[0] * modelMatrix[0 * 4 + i] + in2[1] * modelMatrix[1 * 4 + i] +
      in2[2] * modelMatrix[2 * 4 + i] + in2[3] * modelMatrix[3 * 4 + i];
  }

  T out[4];
  for (i = 0; i<4; i++)
  {
    out[i] = tmp[0] * projMatrix[0 * 4 + i] + tmp[1] * projMatrix[1 * 4 + i] + tmp[2] * projMatrix[2 * 4 + i] + tmp[3] * projMatrix[3 * 4 + i];
  }

  if (out[3] == T(0.0))
    return 0;
  out[0] /= out[3];
  out[1] /= out[3];
  out[2] /= out[3];
  /* Map x, y and z to range 0-1 */
  out[0] = out[0] * T(0.5) + T(0.5);
  out[1] = out[1] * T(0.5) + T(0.5);
  out[2] = out[2] * T(0.5) + T(0.5);

  /* Map x,y to viewport */
  out[0] = out[0] * viewport[2] + viewport[0];
  out[1] = out[1] * viewport[3] + viewport[1];

  *winx = out[0];
  *winy = out[1];
  *winz = out[2];
  return 1;
}

