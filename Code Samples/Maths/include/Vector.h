#pragma once
#include <ostream>
#include "Utils.h"

namespace Maths
{
  class Point; // forward declare

  class Vector
  {
  public:
    Vector();
    Vector(float x_, float y_, float z_, float w_ = 0.0f);

    float Dot(const Vector &rhs) const;
    // create a vector as  Vector( this->dot(Vector v0), this->dot(Vector v1), this->dot(Vector v2))
    inline Vector Dot3(const Vector& v0, const Vector& v1, const Vector& v2) const
    {
      return Vector(Dot(v0), Dot(v1), Dot(v2));
    }
    inline Vector Absolute() const
    {
      return Vector( AbsVal(v[0]), AbsVal(v[1]), AbsVal(v[2]));
    }


    // return values are 0 = x, 1 = y, 2 = z
    inline int MinAxis() const
    {
      return v[0] < v[1] ? (v[0] < v[2] ? 0 : 2) : (v[1] < v[2] ? 1 : 2);
    }
    
    inline int MaxAxis() const
    {
      return v[0] < v[1] ? (v[1] < v[2] ? 2 : 1) : (v[0] < v[2] ? 2 : 0);
    }

    inline int FurthestAxis() const
    {
      return Absolute().MinAxis();
    }

    inline int ClosestAxis() const
    {
      return Absolute().MaxAxis();
    }


    void SetValue(const float& x_, const float& y_, const float& z_)
    {
      v[0] = x_;
      v[1] = y_;
      v[2] = z_;
      v[3] = float(0.f);
    }


    void SetValue(const float& x_, const float& y_, const float& z_, const float& w_)
    {
      v[0] = x_;
      v[1] = y_;
      v[2] = z_;
      v[3] = w_;
    }
    //Returns the result of the dot product between a vector and a point
    //Not technically a valid dot product, but useful for plane computations
    float DotPoint(const Point &rhs) const;
    Vector Cross(const Vector &rhs) const;
    float Triple(const Vector& v1, const Vector& v2) const;
    //Returns the length of the vector
    float Length(void) const;
    // Returns the squared length of the vector
    float SquaredLength(void) const;
    float Normalize(void);
    void Clear(void);
    bool IsZero(void) const;
    Vector operator-(void) const;
    Vector operator+(const Vector &rhs) const;
    Vector operator-(const Vector &rhs) const;
    Vector operator*(float scalar) const;

	// Return the elementwise product of two vectors
	Vector operator*(const Vector& rhs)const;
    //Vector operator/(float scalar) const;

    Vector& operator+=(const Vector &rhs);
    Vector& operator-=(const Vector &rhs);
    Vector& operator*=(const Vector &v);
    Vector& operator*=(float scalar);
    Vector& operator/=(float scalar);
    bool operator==(const Vector &rhs) const;
    bool operator!=(const Vector &rhs) const;
    bool operator<(const Vector &rhs) const;
    bool operator<=(const Vector &rhs) const;
    bool operator>(const Vector &rhs) const;
    bool operator>=(const Vector &rhs) const;
    static bool IsParallel(const Vector& v1, const Vector& v2);
    friend std::ostream& operator<<(std::ostream &os, const Vector &v);

    long MaxDot(const Vector *array, long array_count, float &dotOut) const;
    long MinDot(const Vector *array, long array_count, float &dotOut) const;

    static void BuildOrthonormalBasis(Vector& u, Vector& v, const Vector& w);
    static Vector BuildRandomUnitXYZ(void);
    static Vector BuildRandomUnitXY(void);
    static Vector BuildRandomUnitXZ(void);
    static Vector BuildRandomUnitYZ(void);
    static bool Test(void);


    static const Vector ZERO;
    static const Vector UNIT_X;
    static const Vector UNIT_Y;
    static const Vector UNIT_Z;
    static const Vector UNIT_W;

    //inline float&       operator[](int i)       { return (&v[0])[i];	}
    //inline const float& operator[](int i) const { return (&v[0])[i]; }
    /// operator float*() replaces operator[], using implicit conversion
    inline operator float *() { return &v[0]; }
    inline operator const float *() const { return &v[0]; }

    template <class T>
    inline void PlaneSpace(const T& n, T& p, T& q)
    {
      if (fabsf(n[2]) > SIMDSQRT12) {
        // choose p in y-z plane
        btScalar a = n[1] * n[1] + n[2] * n[2];
        btScalar k = btRecipSqrt(a);
        p[0] = 0;
        p[1] = -n[2] * k;
        p[2] = n[1] * k;
        // set q = n x p
        q[0] = a*k;
        q[1] = -n[0] * p[2];
        q[2] = n[0] * p[1];
      }
      else {
        // choose p in x-y plane
        btScalar a = n[0] * n[0] + n[1] * n[1];
        btScalar k = btRecipSqrt(a);
        p[0] = -n[1] * k;
        p[1] = n[0] * k;
        p[2] = 0;
        // set q = n x p
        q[0] = -n[2] * p[1];
        q[1] = n[2] * p[0];
        q[2] = a*k;
      }
    }

  public:
    union
    {
      struct { float x, y, z, w; };
      struct { float r, g, b, a; };
      float v[4];
    };
  };

  inline Vector operator*(const float& s, const Vector& v)
  {
    return v * s;
  }

  // returns the vector inversely scaled by s
  inline Vector operator/(const Vector& v, const float& s)
  {
    assert(s != float(s));

    return v * (float(1.0) / s);
  }

  // returns the vector inversely scaled by v2
  inline Vector operator/(const Vector& v1, const Vector& v2)
  {
    return Vector(v1.v[0] / v2.v[0],
                  v1.v[1] / v2.v[1],
                  v1.v[2] / v2.v[2]);
  }

  inline float Dot(const Vector& v1, const Vector& v2)
  {
    return v1.Dot(v2);
  }
  
  inline Vector Cross(const Vector& v1, const Vector& v2)
  {
    return v1.Cross(v2);
  }
  
  inline float Triple(const Vector& v1, const Vector& v2, const Vector& v3)
  {
    return v1.Triple(v2, v3);
  }

  template <class T>
  inline void PlaneSpace(const T& n, T& p, T& q)
  {
    if (btFabs(n[2]) > SQRT12) 
    {
      // choose p in y-z plane
      float a = n[1] * n[1] + n[2] * n[2];
      float k = RecipSqrt(a);
      p[0] = 0;
      p[1] = -n[2] * k;
      p[2] = n[1] * k;
      // set q = n x p
      q[0] = a*k;
      q[1] = -n[0] * p[2];
      q[2] = n[0] * p[1];
    }
    else 
    {
      // choose p in x-y plane
      float a = n[0] * n[0] + n[1] * n[1];
      float k = RecipSqrt(a);
      p[0] = -n[1] * k;
      p[1] = n[0] * k;
      p[2] = 0;
      // set q = n x p
      q[0] = -n[2] * p[1];
      q[1] = n[2] * p[0];
      q[2] = a*k;
    }
  }
}