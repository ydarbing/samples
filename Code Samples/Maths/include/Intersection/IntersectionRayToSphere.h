#pragma once

#include "Intersection/Intersection.h"
#include "Geometry/Point.h"

namespace Maths
{
  class Ray;
  class Sphere;

  class IntersectionRayToSphere : public Intersection
  {
  public:
    IntersectionRayToSphere(const Ray& ray, const Sphere& sphere);

    const Ray& GetRay(void) const;
    const Sphere& GetSphere(void) const;

    int GetQuantity(void) const;
    const Point& GetIntersectionPoint(int index) const;
    float GetRayParameter(int index) const;

    virtual bool Test(void);
    virtual bool Find(void);
    //void Draw(bool calc = false, ColorPtr intersectionColor = nullptr)override;

  private:
    float GetDiscriminant(float& a, float& b, float& c);

  private:
    const Ray* m_ray;
    const Sphere* m_sphere;

    int m_quantity;
    Point m_point[2];
    float m_rayParameter[2];
  };

}// namespace Maths