#pragma once

#include "Intersection/Intersection.h"
#include "Geometry/Point.h"
#include "Vector.h"


namespace Maths
{
  class Segment;

  bool FindCommonEndPoint(Point p0, float p0Length, Point p1, float p1Length, Vector hintDir, Point* commonEndpoint);


  // will check if two segments could have a connected common endpoint
  class SegmentToSegmentCommonEndPoint
  {
  public:
    //SegmentToSegmentCommonEndPoint(Segment& seg0, Segment& seg1, Vector hint_dir);
    SegmentToSegmentCommonEndPoint(Point p0, float p0Length, Point p1, float p1Length, Vector hintDir);

    void SetSegment0(Point point, float len);
    void SetSegment1(Point point, float len);
    void SetHintDir(Vector dir);

    bool Find();

    //void Draw(bool calc = false, ColorPtr one = nullptr, ColorPtr two = nullptr, ColorPtr ptColor = nullptr);

  private:
    Point m_commonEndPoint;
    Point m_pos0;
    Point m_pos1;
    Vector m_hintDir;
    float m_length0;
    float m_length1;
    bool m_hasCommonPoint;
  };

}// namespace Maths