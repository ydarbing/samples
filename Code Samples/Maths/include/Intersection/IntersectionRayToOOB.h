#pragma once

#include "Intersection/Intersection.h"
#include "Geometry/Point.h"

namespace Maths
{
  class Ray;
  class OOB;

  class IntersectionRayToOOB : public Intersection
  {
  public:
    IntersectionRayToOOB(const Ray& ray, const OOB& oob);

    const Ray& GetRay(void) const;
    const OOB& GetOOB(void) const;

    int GetQuantity(void) const;
    const Point& GetIntersectionPoint(int index) const;

    virtual bool Test(void);
    virtual bool Find(void);
    //void Draw(bool calc = false, ColorPtr intersectionColor = nullptr) override;

  private:
    const Ray* m_ray;
    const OOB* m_oob;

    int m_quantity;
    Point m_point[2];
  };

}// namespace Maths