#pragma once

#include "Intersection/Intersection.h"

namespace Maths
{
  class Ray;
  class Plane;

  class IntersectionRayToPlane : public Intersection
  {
  public:
    IntersectionRayToPlane(const Ray& ray, const Plane& plane);

    const Ray& GetRay(void) const;
    const Plane& GetPlane(void) const;

    Point GetIntersectionPoint(void) const;

    float GetRayParameter(void) const;

    virtual bool Test(void);
    virtual bool Find(void);
    //void Draw(bool calc = false, ColorPtr intersectionColor = nullptr)override;

  private:
    const Ray* m_ray;
    const Plane* m_plane;
    float m_rayParameter;
  };

}// namespace Maths