#pragma once

#include "Intersection/Intersection.h"

#include "Geometry/Point.h"

namespace Maths
{
  class Ray;
  class AABB;
 
  class IntersectionRayToAAB : public Intersection
  {
  public:
    IntersectionRayToAAB(const Ray& ray, const AABB& aab);

    const Ray& GetRay(void) const;
    const AABB& GetAAB(void) const;

    int GetQuantity(void) const;
    const Point& GetIntersectionPoint(int index) const;

    virtual bool Test(void);
    virtual bool Find(void);
    //void Draw(bool calc = false, ColorPtr intersectionColor = nullptr)override;

  private:
    const Ray* m_ray;
    const AABB* m_aab;

    int m_quantity;
    Point m_point[2];
  };

}// namespace Maths