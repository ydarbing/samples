#pragma once

#include "Color.h"

namespace Maths
{
  class Vector;

  class Intersection
  {
  protected:
    Intersection();
  public:
    virtual ~Intersection();


    float GetContactTime(void) const;
    int GetIntersectionType(void) const;

    virtual bool Test(void);
    virtual bool Find(void);
    virtual bool Test(float t, const Vector& vel0, const Vector& vel1);
    virtual bool Find(float t, const Vector& vel0, const Vector& vel1);

    //void Draw(bool calc = false, ColorPtr intersection = nullptr);

    enum
    {
      INONE = 0,
      IPOINT,
      ISEGMENT,
      IRAY,
      ILINE,
      IPLANE,
      ICIRCLE,
      IOTHER,
    };

  protected:
    int m_intersectType; //from enum
    float m_contactTime;
  };
}// namespace Maths

