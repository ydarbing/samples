#pragma once

#include "Geometry/AABB.h"

namespace Maths
{
  class DynamicAABBTree
  {
  public:
    DynamicAABBTree();
    ~DynamicAABBTree();

    int Insert(const AABB& aabb, void* userData);
    void Remove(int id);
    bool Update(int id, const AABB& aabb, const Vector& displacement);

    template <typename T>
    void Query(T* cb, const AABB& aabb)const;


    void* GetUserData(int id) const;
    const AABB& GetFatAABB(int id)const;
    //void Render()const;
    void Validate()const;

  private:
    struct Node
    {
      static const int Null = -1;

      AABB aabb;

      union
      {
        int parent;
        int next; // free list
      };

      union 
      {
        struct 
        {
          int left;
          int right;
        };
        // only leaf nodes hold user data, 
        // so use the same memory for left/right indices to store user data void pointer
        void* userData;
      };

      // leaf = 0, free node = -1
      int height;

      bool IsLeaf()const { return right == Null; }
    };


  private:
    int MakeNode();
    inline void DestroyNode(int index);
    int Balance(int index);
    int PromoteChild(Node* parent, int parentIndex, Node* toPromote, int toPromoteIndex, Node* toPromoteSibling);
    void InsertLeaf(int id);
    void RemoveLeaf(int index);
    void ValidateTree(int index)const;
    //void RenderNode(int index)const;

    // Correct AABB hierarchy heights and AABBs starting at supplied
    // index traversing up hierarchy
    void SyncHeirarchy(int index);
    // Insert nodes at a given index until m_capacity into the free list
    inline void AddToFreeList(int index);


    int m_root;
    int m_size;
    int m_capacity;
    int m_freeList;
    Node* m_nodes;
  };
}

#include "Broadphase/DynamicAABBTree.hpp"
