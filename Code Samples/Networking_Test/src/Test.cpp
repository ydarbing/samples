#include "GG_Networking.h"
#include "Test.h"

#include "Allocator\DefaultAllocator.h"

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>

using namespace GG;


static void CheckHandler(const char* condition,
                         const char* function,
                         const char* file,
                         int line)
{
  printf("Check failed: (%s), function %s, file %s, line %d", condition, function, file, line);

#ifndef NDEBUG
#if defined( __GNUC__ )
  __builtin_trap();
#elif defined( _MSC_VER )
  __debugbreak();
#endif
#endif
  exit(1);
}

#define check(condition)                                                      \
do                                                                             \
{                                                                              \
    if ( !(condition) )                                                        \
    {                                                                          \
        CheckHandler( #condition, __FUNCTION__, __FILE__, __LINE__ );          \
    }                                                                          \
} while(0)

void test_endian()
{
  uint32_t value = 0x11223344;
  const char* bytes = (const char*)&value;

#if GG_LITTLE_ENDIAN

  check(bytes[0] == 0x44);
  check(bytes[1] == 0x33);
  check(bytes[2] == 0x22);
  check(bytes[3] == 0x11);
#else
  check(bytes[3] == 0x44);
  check(bytes[2] == 0x33);
  check(bytes[1] == 0x22);
  check(bytes[0] == 0x11);
#endif

}


#include "Queue.h"
void test_queue()
{
  const int queueSize = 1024;

  Queue<int> queue(GetDefaultAllocator(), queueSize);

  check(queue.IsEmpty());
  check(!queue.IsFull());
  check(queue.GetNumEntries() == 0);
  check(queue.GetSize() == queueSize);

  int numEntries = 100;

  for (int i = 0; i < numEntries; ++i)
  {
    queue.Push(i);
  }

  check(!queue.IsEmpty());
  check(!queue.IsFull());
  check(queue.GetNumEntries() == numEntries);
  check(queue.GetSize() == queueSize);

  for (int i = 0; i < numEntries; ++i)
    check(queue[i] == i);

  for (int i = 0; i < numEntries; ++i)
    check(queue.Pop() == i);


  check(queue.IsEmpty());
  check(!queue.IsFull());
  check(queue.GetNumEntries() == 0);
  check(queue.GetSize() == queueSize);

  queue.Clear();


  check(queue.IsEmpty());
  check(!queue.IsFull());
  check(queue.GetNumEntries() == 0);
  check(queue.GetSize() == queueSize);
}


#if GG_WITH_MBEDTLS

#include "Utils.h" // Base64 functions and RandomBytes

void test_base64()
{
  const int bufferSize = 256;

  char input[bufferSize];
  char encoded[bufferSize * 2];
  char decoded[bufferSize];

  strcpy(input, "[2001:4860:4860::8888]:50000");

  const int encodedBytes = Base64EncodeString(input, encoded, sizeof(encoded));

  check(encodedBytes == (int)strlen(encoded) + 1);

  char encodedExpected[] = "WzIwMDE6NDg2MDo0ODYwOjo4ODg4XTo1MDAwMAA=";

  check(strcmp(encoded, encodedExpected) == 0);

  const int decodedBytes = Base64DecodeString(encoded, decoded, sizeof(decoded));

  check(decodedBytes == (int)strlen(decoded) + 1);

  check(strcmp(input, decoded) == 0);

  uint8_t key[KEY_BYTES];
  RandomBytes(key, KEY_BYTES);


  char base64_key[KEY_BYTES * 2];
  Base64EncodeData(key, KEY_BYTES, base64_key, (s32) sizeof(base64_key));

  uint8_t decoded_key[KEY_BYTES];
  Base64DecodeData(base64_key, decoded_key, KEY_BYTES);

  check(memcmp(key, decoded_key, KEY_BYTES) == 0);
}

#endif // #if GG_WITH_MBEDTLS


#include "BitWriter.h"
#include "BitReader.h"

void test_bitpacker()
{
  const int bufferSize = 256;

  uint8_t buffer[bufferSize];

  BitWriter writer(buffer, bufferSize);

  check(writer.GetData() == buffer);
  check(writer.GetBitsWritten() == 0);
  check(writer.GetBytesWritten() == 0);
  check(writer.GetBitsAvailable() == bufferSize * 8);

  writer.WriteBits(0, 1);
  writer.WriteBits(1, 1);
  writer.WriteBits(10, 8);
  writer.WriteBits(255, 8);
  writer.WriteBits(1000, 10);
  writer.WriteBits(50000, 16);
  writer.WriteBits(9999999, 32);
  writer.FlushBits();

  const int bitsWritten = 1 + 1 + 8 + 8 + 10 + 16 + 32;

  check(writer.GetBytesWritten() == 10);
  check(writer.GetBitsWritten() == bitsWritten);
  check(writer.GetBitsAvailable() == bufferSize * 8 - bitsWritten);

  const int bytesWritten = writer.GetBytesWritten();

  check(bytesWritten == 10);

  memset(buffer + bytesWritten, 0, bufferSize - bytesWritten);

  BitReader reader(buffer, bytesWritten);

  check(reader.GetBitsRead() == 0);
  check(reader.GetBitsRemaining() == bytesWritten * 8);

  uint32_t a = reader.ReadBits(1);
  uint32_t b = reader.ReadBits(1);
  uint32_t c = reader.ReadBits(8);
  uint32_t d = reader.ReadBits(8);
  uint32_t e = reader.ReadBits(10);
  uint32_t f = reader.ReadBits(16);
  uint32_t g = reader.ReadBits(32);

  check(a == 0);
  check(b == 1);
  check(c == 10);
  check(d == 255);
  check(e == 1000);
  check(f == 50000);
  check(g == 9999999);

  check(reader.GetBitsRead() == bitsWritten);
  check(reader.GetBitsRemaining() == bytesWritten * 8 - bitsWritten);
}

#include "Utils.h" // BitsRequired()
void test_bits_required()
{
  check(BitsRequired(0, 0) == 0);
  check(BitsRequired(0, 1) == 1);
  check(BitsRequired(0, 2) == 2);
  check(BitsRequired(0, 3) == 2);
  check(BitsRequired(0, 4) == 3);
  check(BitsRequired(0, 5) == 3);
  check(BitsRequired(0, 6) == 3);
  check(BitsRequired(0, 7) == 3);
  check(BitsRequired(0, 8) == 4);
  check(BitsRequired(0, 255) == 8);
  check(BitsRequired(0, 65535) == 16);
  check(BitsRequired(0, 4294967295) == 32);
}

const int MaxItems = 11;
#include "Address.h" // MAX_ADDRESS_LENGTH
#include"ISerializeable.h"

struct TestData
{
  TestData()
  {
    memset(this, 0, sizeof(TestData));
  }

  int a, b, c;
  uint32_t d : 8;
  uint32_t e : 8;
  uint32_t f : 8;
  bool g;
  int numItems;
  int items[MaxItems];
  float floatValue;
  double doubleValue;
  uint64_t uint64Value;
  uint8_t bytes[17];
  char string[MAX_ADDRESS_LENGTH];
};

struct TestContext
{
  int min;
  int max;
};

struct TestObject : public ISerializeable
{
  TestData data;

  void Init()
  {
    data.a = 1;
    data.b = -2;
    data.c = 150;
    data.d = 55;
    data.e = 255;
    data.f = 127;
    data.g = true;

    data.numItems = MaxItems / 2;
    for (int i = 0; i < data.numItems; ++i)
      data.items[i] = i + 10;

    data.floatValue = 3.1415926f;
    data.doubleValue = 1 / 3.0;
    data.uint64Value = 0x1234567898765432L;

    for (int i = 0; i < (int)sizeof(data.bytes); ++i)
      data.bytes[i] = rand() % 255;

    strcpy(data.string, "hello world!");
  }

  template <typename Stream>
  bool Serialize(Stream& stream)
  {
    const TestContext& context = *(const TestContext*)stream.GetContext();

    SERIALIZE_INT(stream, data.a, context.min, context.max);
    SERIALIZE_INT(stream, data.b, context.min, context.max);

    SERIALIZE_INT(stream, data.c, -100, 10000);

    SERIALIZE_BITS(stream, data.d, 6);
    SERIALIZE_BITS(stream, data.e, 8);
    SERIALIZE_BITS(stream, data.f, 7);

    SERIALIZE_ALIGN(stream);

    SERIALIZE_BOOL(stream, data.g);

    SERIALIZE_CHECK(stream);

    SERIALIZE_INT(stream, data.numItems, 0, MaxItems - 1);
    for (int i = 0; i < data.numItems; ++i)
      SERIALIZE_BITS(stream, data.items[i], 8);

    SERIALIZE_FLOAT(stream, data.floatValue);

    SERIALIZE_DOUBLE(stream, data.doubleValue);

    SERIALIZE_UINT64(stream, data.uint64Value);

    SERIALIZE_BYTES(stream, data.bytes, sizeof(data.bytes));

    SERIALIZE_STRING(stream, data.string, sizeof(data.string));

    SERIALIZE_CHECK(stream);

    return true;
  }

  GG_VIRTUAL_SERIALIZE_FUNCTIONS();

  bool operator ==(const TestObject& other)const
  {
    return memcmp(&data, &other.data, sizeof(TestData)) == 0;
  }
  bool operator !=(const TestObject& other)const
  {
    return !(*this == other);
  }
};

#include "Stream\StreamWrite.h"
#include "Stream\StreamRead.h"

void test_stream()
{
  const int bufferSize = 1024;
  uint8_t buffer[bufferSize];

  TestContext context;
  context.min = -10;
  context.max = 10;

  StreamWrite writeStream(GetDefaultAllocator(), buffer, bufferSize);

  TestObject writeObject;
  writeObject.Init();
  writeStream.SetContext(&context);
  writeObject.Serialize(writeStream);
  writeStream.Flush();

  const int bytesWritten = writeStream.GetBytesProcessed();

  memset(buffer + bytesWritten, 0, bufferSize - bytesWritten);

  TestObject readObject;
  StreamRead readStream(GetDefaultAllocator(), buffer, bytesWritten);
  readStream.SetContext(&context);
  readObject.Serialize(readStream);

  check(readObject == writeObject);
}

#include "Address.h"
bool parse_address(const char string[])
{
  Address address(string);
  return address.IsValid();
}

void test_address()
{
  check(parse_address("") == false);
  check(parse_address("[") == false);
  check(parse_address("[]") == false);
  check(parse_address("[]:") == false);
  check(parse_address(":") == false);
  check(parse_address("1") == false);
  check(parse_address("12") == false);
  check(parse_address("123") == false);
  check(parse_address("1234") == false);
  check(parse_address("1234.0.12313.0000") == false);
  check(parse_address("1234.0.12313.0000.0.0.0.0.0") == false);
  check(parse_address("1312313:123131:1312313:123131:1312313:123131:1312313:123131:1312313:123131:1312313:123131") == false);
  check(parse_address(".") == false);
  check(parse_address("..") == false);
  check(parse_address("...") == false);
  check(parse_address("....") == false);
  check(parse_address(".....") == false);


  // IPV4 non loopback with no port
  {
    Address address("107.77.207.77");
    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV4);
    check(address.GetPort() == 0);
    check(address.GetAddress4()[0] == 107);
    check(address.GetAddress4()[1] == 77);
    check(address.GetAddress4()[2] == 207);
    check(address.GetAddress4()[3] == 77);
    check(!address.IsLoopback());
  }


  // IPV4 loopback with no port
  {
    Address address("127.0.0.1");
    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV4);
    check(address.GetPort() == 0);
    check(address.GetAddress4()[0] == 127);
    check(address.GetAddress4()[1] == 0);
    check(address.GetAddress4()[2] == 0);
    check(address.GetAddress4()[3] == 1);
    check(address.IsLoopback());
  }

  // IPV4 non loopback with port
  {
    Address address("107.77.207.77:40000");
    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV4);
    check(address.GetPort() == 40000);
    check(address.GetAddress4()[0] == 107);
    check(address.GetAddress4()[1] == 77);
    check(address.GetAddress4()[2] == 207);
    check(address.GetAddress4()[3] == 77);
    check(!address.IsLoopback());
  }

  // IPV4 loopback with port
  {
    Address address("127.0.0.1:40000");
    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV4);
    check(address.GetPort() == 40000);
    check(address.GetAddress4()[0] == 127);
    check(address.GetAddress4()[1] == 0);
    check(address.GetAddress4()[2] == 0);
    check(address.GetAddress4()[3] == 1);
    check(address.IsLoopback());
  }


  // valid IPV6 non loopback with no port
  {
    Address address("fe80::202:b3ff:fe1e:8329");
    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV6);
    check(address.GetPort() == 0);
    check(address.GetAddress6()[0] == 0xfe80);
    check(address.GetAddress6()[1] == 0x0000);
    check(address.GetAddress6()[2] == 0x0000);
    check(address.GetAddress6()[3] == 0x0000);
    check(address.GetAddress6()[4] == 0x0202);
    check(address.GetAddress6()[5] == 0xb3ff);
    check(address.GetAddress6()[6] == 0xfe1e);
    check(address.GetAddress6()[7] == 0x8329);
    check(!address.IsLoopback());
  }


  // valid IPV6 non loopback with no port
  {
    Address address("::");
    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV6);
    check(address.GetPort() == 0);
    check(address.GetAddress6()[0] == 0x0000);
    check(address.GetAddress6()[1] == 0x0000);
    check(address.GetAddress6()[2] == 0x0000);
    check(address.GetAddress6()[3] == 0x0000);
    check(address.GetAddress6()[4] == 0x0000);
    check(address.GetAddress6()[5] == 0x0000);
    check(address.GetAddress6()[6] == 0x0000);
    check(address.GetAddress6()[7] == 0x0000);
    check(!address.IsLoopback());
  }

  // valid IPV6  loopback with no port
  {
    Address address("::1");
    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV6);
    check(address.GetPort() == 0);
    check(address.GetAddress6()[0] == 0x0000);
    check(address.GetAddress6()[1] == 0x0000);
    check(address.GetAddress6()[2] == 0x0000);
    check(address.GetAddress6()[3] == 0x0000);
    check(address.GetAddress6()[4] == 0x0000);
    check(address.GetAddress6()[5] == 0x0000);
    check(address.GetAddress6()[6] == 0x0000);
    check(address.GetAddress6()[7] == 0x0001);
    check(address.IsLoopback());
  }



  // valid IPV6 non loopback with port
  {
    Address address("[fe80::202:b3ff:fe1e:8329]:40000");
    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV6);
    check(address.GetPort() == 40000);
    check(address.GetAddress6()[0] == 0xfe80);
    check(address.GetAddress6()[1] == 0x0000);
    check(address.GetAddress6()[2] == 0x0000);
    check(address.GetAddress6()[3] == 0x0000);
    check(address.GetAddress6()[4] == 0x0202);
    check(address.GetAddress6()[5] == 0xb3ff);
    check(address.GetAddress6()[6] == 0xfe1e);
    check(address.GetAddress6()[7] == 0x8329);
    check(!address.IsLoopback());
  }

  // valid IPV6 non loopback with port
  {
    Address address("[::]:40000");
    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV6);
    check(address.GetPort() == 40000);
    check(address.GetAddress6()[0] == 0x0000);
    check(address.GetAddress6()[1] == 0x0000);
    check(address.GetAddress6()[2] == 0x0000);
    check(address.GetAddress6()[3] == 0x0000);
    check(address.GetAddress6()[4] == 0x0000);
    check(address.GetAddress6()[5] == 0x0000);
    check(address.GetAddress6()[6] == 0x0000);
    check(address.GetAddress6()[7] == 0x0000);
    check(!address.IsLoopback());
  }

  // valid IPV6 loopback with port
  {
    Address address("[::1]:40000");
    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV6);
    check(address.GetPort() == 40000);
    check(address.GetAddress6()[0] == 0x0000);
    check(address.GetAddress6()[1] == 0x0000);
    check(address.GetAddress6()[2] == 0x0000);
    check(address.GetAddress6()[3] == 0x0000);
    check(address.GetAddress6()[4] == 0x0000);
    check(address.GetAddress6()[5] == 0x0000);
    check(address.GetAddress6()[6] == 0x0000);
    check(address.GetAddress6()[7] == 0x0001);
    check(address.IsLoopback());
  }

  char buffer[MAX_ADDRESS_LENGTH];
  {
    const uint16_t address6[] = { 0xFE80, 0x0000, 0x0000, 0x0000, 0x0202, 0xB3FF, 0xFE1E, 0x8329 };

    Address address(address6[0], address6[1], address6[2], address6[2],
                    address6[4], address6[5], address6[6], address6[7]);

    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV6);
    check(address.GetPort() == 0);

    for (int i = 0; i < 8; ++i)
      check(address6[i] == address.GetAddress6()[i]);

    check(strcmp(address.ToString(buffer, MAX_ADDRESS_LENGTH), "fe80::202:b3ff:fe1e:8329") == 0);
  }

  {
    const uint16_t address6[] = { 0xFE80, 0x0000, 0x0000, 0x0000, 0x0202, 0xB3FF, 0xFE1E, 0x8329 };

    Address address(address6);

    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV6);
    check(address.GetPort() == 0);

    for (int i = 0; i < 8; ++i)
      check(address6[i] == address.GetAddress6()[i]);

    check(strcmp(address.ToString(buffer, MAX_ADDRESS_LENGTH), "fe80::202:b3ff:fe1e:8329") == 0);
  }

  {
    const uint16_t address6[] = { 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0001 };

    Address address(address6);

    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV6);
    check(address.GetPort() == 0);

    for (int i = 0; i < 8; ++i)
      check(address6[i] == address.GetAddress6()[i]);

    check(strcmp(address.ToString(buffer, MAX_ADDRESS_LENGTH), "::1") == 0);
  }

  {
    const uint16_t address6[] = { 0xFE80, 0x0000, 0x0000, 0x0000, 0x0202, 0xB3FF, 0xFE1E, 0x8329 };

    Address address(address6[0], address6[1], address6[2], address6[2],
                    address6[4], address6[5], address6[6], address6[7], 65535);

    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV6);
    check(address.GetPort() == 65535);

    for (int i = 0; i < 8; ++i)
      check(address6[i] == address.GetAddress6()[i]);

    check(strcmp(address.ToString(buffer, MAX_ADDRESS_LENGTH), "[fe80::202:b3ff:fe1e:8329]:65535") == 0);
  }

  {
    const uint16_t address6[] = { 0xFE80, 0x0000, 0x0000, 0x0000, 0x0202, 0xB3FF, 0xFE1E, 0x8329 };

    Address address(address6, 65535);

    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV6);
    check(address.GetPort() == 65535);

    for (int i = 0; i < 8; ++i)
      check(address6[i] == address.GetAddress6()[i]);

    check(strcmp(address.ToString(buffer, MAX_ADDRESS_LENGTH), "[fe80::202:b3ff:fe1e:8329]:65535") == 0);
  }

  {
    const uint16_t address6[] = { 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0001 };

    Address address(address6, 65535);

    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV6);
    check(address.GetPort() == 65535);

    for (int i = 0; i < 8; ++i)
      check(address6[i] == address.GetAddress6()[i]);

    check(strcmp(address.ToString(buffer, MAX_ADDRESS_LENGTH), "[::1]:65535") == 0);
  }


  {
    Address address("fe80::202:b3ff:fe1e:8329");
    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV6);
    check(address.GetPort() == 0);
    check(strcmp(address.ToString(buffer, MAX_ADDRESS_LENGTH), "fe80::202:b3ff:fe1e:8329") == 0);
  }

  {
    Address address("::1");
    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV6);
    check(address.GetPort() == 0);
    check(strcmp(address.ToString(buffer, MAX_ADDRESS_LENGTH), "::1") == 0);
  }

  {
    Address address("[fe80::202:b3ff:fe1e:8329]:65535");
    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV6);
    check(address.GetPort() == 65535);
    check(strcmp(address.ToString(buffer, MAX_ADDRESS_LENGTH), "[fe80::202:b3ff:fe1e:8329]:65535") == 0);
  }

  {
    Address address("[::1]:65535");
    check(address.IsValid());
    check(address.GetType() == E_ADDRESS_IPV6);
    check(address.GetPort() == 65535);
    check(strcmp(address.ToString(buffer, MAX_ADDRESS_LENGTH), "[::1]:65535") == 0);
  }
}

#include "BitArray.h"

void test_bit_array()
{
  const int size = 300;

  BitArray bitArray(GetDefaultAllocator(), size);

  check(bitArray.GetSize() == size);

  for (int i = 0; i < size; ++i)
    check(bitArray.GetBit(i) == 0);

  // set every third bit and verify correct bits are set on read
  for (int i = 0; i < size; ++i)
  {
    if ((i % 3) == 0)
      bitArray.SetBit(i);
  }

  for (int i = 0; i < size; ++i)
  {
    if ((i % 3) == 0)
      check(bitArray.GetBit(i) == 1);
    else
      check(bitArray.GetBit(i) == 0);
  }


  // clear every third bit to zero and verify all bits are zero again

  for (int i = 0; i < size; ++i)
  {
    if ((i % 3) == 0)
      bitArray.ClearBit(i);
  }
  for (int i = 0; i < size; ++i)
  {
    check(bitArray.GetBit(i) == 0);
  }

  // set some bits 
  for (int i = 0; i < size; ++i)
  {
    if ((i % 10) == 0)
      bitArray.SetBit(i);
  }

  for (int i = 0; i < size; ++i)
  {
    if ((i % 10) == 0)
      check(bitArray.GetBit(i) == 1);
    else
      check(bitArray.GetBit(i) == 0);
  }

  // clear all bits to zero and verify
  bitArray.Clear();


  for (int i = 0; i < size; ++i)
  {
    check(bitArray.GetBit(i) == 0);
  }
}


#include "SequenceBuffer.h"
struct TestSequenceData
{
  TestSequenceData() : sequence(0xFFFF) {}
  explicit TestSequenceData(uint16_t sequence_) : sequence(sequence_) {}
  uint16_t sequence;
};

void test_sequence_buffer()
{
  const int size = 256;

  SequenceBuffer<TestSequenceData> sequenceBuffer(GetDefaultAllocator(), size);

  for (int i = 0; i < size; ++i)
    check(sequenceBuffer.Find(i) == nullptr);

  for (int i = 0; i <= size * 4; ++i)
  {
    TestSequenceData* entry = sequenceBuffer.Insert(i);
    entry->sequence = i;
    check(sequenceBuffer.GetSequence() == i + 1);
  }

  for (int i = 0; i <= size; ++i)
  {
    TestSequenceData* entry = sequenceBuffer.Find(i);
    check(!entry);
  }

  int index = size * 4;

  for (int i = 0; i < size; ++i)
  {
    TestSequenceData* entry = sequenceBuffer.Find(index);
    check(entry);
    check(entry->sequence == (uint32_t)index);
    index--;
  }
  sequenceBuffer.Reset();

  check(sequenceBuffer.GetSequence() == 0);

  for (int i = 0; i < size; ++i)
    check(sequenceBuffer.Find(i) == nullptr);
}



#include "Allocator\TLSF_Allocator.h"
void test_allocator_tlsf()
{
  const int numBlocks = 256;
  const int blockSize = 1024;
  const int memorySize = numBlocks * blockSize;

  uint8_t* memory = (uint8_t*)malloc(memorySize);

  TLSF_Allocator allocator(memory, memorySize);

  uint8_t* blockData[numBlocks];
  memset(blockData, 0, sizeof(blockData));

  int stopIndex = 0;

  for (int i = 0; i < numBlocks; ++i)
  {
    blockData[i] = (uint8_t*)GG_ALLOCATE(allocator, blockSize);

    if (!blockData[i])
    {
      check(allocator.GetErrorLevel() == E_ALLOCATOR_ERROR_OUT_OF_MEMORY);
      allocator.ClearError();
      check(allocator.GetErrorLevel() == E_ALLOCATOR_ERROR_NONE);
      stopIndex = i;
      break;
    }

    check(blockData[i]);
    check(allocator.GetErrorLevel() == E_ALLOCATOR_ERROR_NONE);

    memset(blockData[i], i + 10, blockSize);
  }

  check(stopIndex > numBlocks / 2);

  for (int i = 0; i < numBlocks - 1; ++i)
  {
    if (blockData[i])
    {
      for (int j = 0; j < blockSize; ++j)
      {
        check(blockData[i][j] == uint8_t(i + 10));
      }
    }

    GG_FREE(allocator, blockData[i]);
  }

  free(memory);
}

#include "Connection.h"

void pump_connection_update(ConnectionConfig& connectionConfig, double& time, Connection& sender, Connection& receiver, uint16_t& senderSequence, uint16_t& receiverSequence, float deltaTime = 0.1f, int packetLossPercent = 90)
{
  uint8_t* packetData = (uint8_t*)alloca(connectionConfig.maxPacketSize);

  int packetBytes = 0;
  if (sender.GeneratePacket(nullptr, senderSequence, packetData, connectionConfig.maxPacketSize, packetBytes))
  {
    if (RandomInt(0, 100) >= packetLossPercent)
    {
      receiver.ProcessPacket(nullptr, senderSequence, packetData, packetBytes);
      sender.ProcessAcks(&senderSequence, 1);
    }
  }
  if (receiver.GeneratePacket(nullptr, receiverSequence, packetData, connectionConfig.maxPacketSize, packetBytes))
  {
    if (RandomInt(0, 100) >= packetLossPercent)
    {
      sender.ProcessPacket(nullptr, receiverSequence, packetData, packetBytes);
      receiver.ProcessAcks(&receiverSequence, 1);
    }
  }

  time += deltaTime;

  sender.AdvanceTime(time);
  receiver.AdvanceTime(time);

  senderSequence++;
  receiverSequence++;
}

void test_connection_reliable_ordered_messages()
{
  TestMessageFactory messageFactory(GetDefaultAllocator());

  double time = 100.0;

  ConnectionConfig connectionConfig;

  Connection sender(GetDefaultAllocator(), messageFactory, connectionConfig, time);
  Connection receiver(GetDefaultAllocator(), messageFactory, connectionConfig, time);

  const int numMessagesSent = 64;

  for (int i = 0; i < numMessagesSent; ++i)
  {
    TestMessage* message = (TestMessage*)messageFactory.CreateMessage(E_TEST_MESSAGE);
    check(message);
    message->sequence = i;
    sender.SendMessage(0, message);
  }

  const int senderPort = 10000;
  const int receiverPort = 10001;

  Address senderAddress("::1", senderPort);
  Address receiverAddress("::1", receiverPort);

  int numMessagesReceived = 0;

  const int numIterations = 1000;

  uint16_t senderSequence = 0;
  uint16_t receiverSequence = 0;

  for (int i = 0; i < numIterations; ++i)
  {
    pump_connection_update(connectionConfig, time, sender, receiver, senderSequence, receiverSequence);

    while (true)
    {
      Message* message = receiver.ReceiveMessage(0);
      if (!message)
        break;

      check(message->GetId() == (int)numMessagesReceived);
      check(message->GetType() == E_TEST_MESSAGE);

      TestMessage* testMessage = (TestMessage*)message;

      check(testMessage->sequence == numMessagesReceived);

      ++numMessagesReceived;

      messageFactory.ReleaseMessage(message);
    }
    if (numMessagesReceived == numMessagesSent)
      break;
  }
  check(numMessagesReceived == numMessagesSent);
}

void test_connection_reliable_ordered_blocks()
{
  TestMessageFactory messageFactory(GetDefaultAllocator());

  double time = 100.0;

  ConnectionConfig connectionConfig;

  Connection sender(GetDefaultAllocator(), messageFactory, connectionConfig, time);
  Connection receiver(GetDefaultAllocator(), messageFactory, connectionConfig, time);

  const int numMessagesSent = 32;

  for (int i = 0; i < numMessagesSent; ++i)
  {
    TestBlockMessage* message = (TestBlockMessage*)messageFactory.CreateMessage(E_TEST_BLOCK_MESSAGE);
    check(message);
    message->sequence = i;

    const int blockSize = 1 + ((i * 901) % 3333);
    uint8_t* blockData = (uint8_t*)GG_ALLOCATE(messageFactory.GetAllocator(), blockSize);

    for (int j = 0; j < blockSize; ++j)
      blockData[j] = i + j;

    message->AttachBlock(messageFactory.GetAllocator(), blockData, blockSize);
    sender.SendMessage(0, message);
  }

  const int senderPort = 10000;
  const int receiverPort = 10001;

  Address senderAddress("::1", senderPort);
  Address receiverAddress("::1", receiverPort);

  int numMessagesReceived = 0;

  uint16_t senderSequence = 0;
  uint16_t receiverSequence = 0;

  const int numIterations = 10000;

  for (int i = 0; i < numIterations; ++i)
  {
    pump_connection_update(connectionConfig, time, sender, receiver, senderSequence, receiverSequence);

    while (true)
    {
      Message* message = receiver.ReceiveMessage(0);
      if (!message)
        break;

      check(message->GetId() == (int)numMessagesReceived);
      check(message->GetType() == E_TEST_BLOCK_MESSAGE);

      TestBlockMessage* blockMessage = (TestBlockMessage*)message;
      check(blockMessage->sequence == uint16_t(numMessagesReceived));

      const int blockSize = blockMessage->GetBlockSize();

      check(blockSize == 1 + ((numMessagesReceived * 901) % 3333));

      uint8_t* blockData = blockMessage->GetBlockData();
      check(blockData);

      for (int j = 0; j < blockSize; ++j)
      {
        check(blockData[j] == uint8_t(numMessagesReceived + j));
      }
      ++numMessagesReceived;
      messageFactory.ReleaseMessage(message);
    }

    if (numMessagesReceived == numMessagesSent)
      break;
  }

  check(numMessagesReceived == numMessagesSent);
}

void test_connection_reliable_ordered_messages_and_blocks()
{
  TestMessageFactory messageFactory(GetDefaultAllocator());

  double time = 100.0;

  ConnectionConfig connectionConfig;

  Connection sender(GetDefaultAllocator(), messageFactory, connectionConfig, time);
  Connection receiver(GetDefaultAllocator(), messageFactory, connectionConfig, time);

  const int numMessagesSent = 32;

  for (int i = 0; i < numMessagesSent; ++i)
  {
    if (rand() % 2)
    {
      TestMessage* message = (TestMessage*)messageFactory.CreateMessage(E_TEST_MESSAGE);
      check(message);
      message->sequence = i;
      sender.SendMessage(0, message);
    }
    else
    {
      TestBlockMessage* message = (TestBlockMessage*)messageFactory.CreateMessage(E_TEST_BLOCK_MESSAGE);
      check(message);
      message->sequence = i;

      const int blockSize = 1 + ((i * 901) % 3333);
      uint8_t* blockData = (uint8_t*)GG_ALLOCATE(messageFactory.GetAllocator(), blockSize);

      for (int j = 0; j < blockSize; ++j)
        blockData[j] = i + j;

      message->AttachBlock(messageFactory.GetAllocator(), blockData, blockSize);
      sender.SendMessage(0, message);
    }
  }

  const int senderPort = 10000;
  const int receiverPort = 10001;

  Address senderAddress("::1", senderPort);
  Address receiverAddress("::1", receiverPort);

  int numMessagesReceived = 0;

  uint16_t senderSequence = 0;
  uint16_t receiverSequence = 0;

  const int numIterations = 10000;

  for (int i = 0; i < numIterations; ++i)
  {
    pump_connection_update(connectionConfig, time, sender, receiver, senderSequence, receiverSequence);

    while (true)
    {
      Message* message = receiver.ReceiveMessage(0);

      if (!message)
        break;

      check(message->GetId() == (int)numMessagesReceived);

      switch (message->GetType())
      {
        case E_TEST_MESSAGE:
        {
          TestMessage* testMessage = (TestMessage*)message;
          check(testMessage->sequence == uint16_t(numMessagesReceived));
          ++numMessagesReceived;
        }
        break;
        case E_TEST_BLOCK_MESSAGE:
        {
          TestBlockMessage* blockMessage = (TestBlockMessage*)message;
          check(blockMessage->sequence == uint16_t(numMessagesReceived));

          const int blockSize = blockMessage->GetBlockSize();

          check(blockSize == 1 + ((numMessagesReceived * 901) % 3333));

          const uint8_t* blockData = blockMessage->GetBlockData();
          check(blockData);

          for (int j = 0; j < blockSize; ++j)
          {
            check(blockData[j] == uint8_t(numMessagesReceived + j));
          }

          ++numMessagesReceived;
        }
        break;
      }
      messageFactory.ReleaseMessage(message);
    }
    if (numMessagesReceived == numMessagesSent)
      break;
  }
  check(numMessagesReceived == numMessagesSent);
}

void test_connection_reliable_ordered_messages_and_blocks_multiple_channels()
{
  const int numChannels = 2;
  double time = 100.0;

  TestMessageFactory messageFactory(GetDefaultAllocator());

  ConnectionConfig connectionConfig;
  connectionConfig.numChannels = numChannels;
  connectionConfig.channel[0].type = E_CHANNEL_TYPE_RELIABLE_ORDERED;
  connectionConfig.channel[0].maxMessagesPerPacket = 8;
  connectionConfig.channel[1].type = E_CHANNEL_TYPE_RELIABLE_ORDERED;
  connectionConfig.channel[1].maxMessagesPerPacket = 8;

  Connection sender(GetDefaultAllocator(), messageFactory, connectionConfig, time);
  Connection receiver(GetDefaultAllocator(), messageFactory, connectionConfig, time);

  const int numMessagesSent = 32;

  for (int channelIndex = 0; channelIndex < numChannels; ++channelIndex)
  {
    for (int i = 0; i < numMessagesSent; ++i)
    {
      if (rand() % 2)
      {
        TestMessage* message = (TestMessage*)messageFactory.CreateMessage(E_TEST_MESSAGE);
        check(message);
        message->sequence = i;
        sender.SendMessage(channelIndex, message);
      }
      else
      {
        TestBlockMessage* message = (TestBlockMessage*)messageFactory.CreateMessage(E_TEST_BLOCK_MESSAGE);
        check(message);
        message->sequence = i;
        const int blockSize = 1 + ((i * 901) % 3333);
        uint8_t* blockData = (uint8_t*)GG_ALLOCATE(messageFactory.GetAllocator(), blockSize);

        for (int j = 0; j < blockSize; ++j)
          blockData[j] = i + j;

        message->AttachBlock(messageFactory.GetAllocator(), blockData, blockSize);
        sender.SendMessage(channelIndex, message);
      }
    }
  }

  const int senderPort = 10000;
  const int receiverPort = 10001;

  Address senderAddress("::1", senderPort);
  Address receiverAddress("::1", receiverPort);

  const int numIterations = 10000;

  int numMessagesReceived[numChannels];
  memset(numMessagesReceived, 0, sizeof(numMessagesReceived));

  uint16_t senderSequence = 0;
  uint16_t receiverSequence = 0;

  for (int i = 0; i < numIterations; ++i)
  {
    pump_connection_update(connectionConfig, time, sender, receiver, senderSequence, receiverSequence);

    for (int channelIndex = 0; channelIndex < numChannels; ++channelIndex)
    {
      while (true)
      {
        Message* message = receiver.ReceiveMessage(channelIndex);
        if (!message)
          break;

        check(message->GetId() == (int)numMessagesReceived[channelIndex]);

        switch (message->GetType())
        {
          case E_TEST_MESSAGE:
          {
            TestMessage* testMessage = (TestMessage*)message;
            check(testMessage->sequence == uint16_t(numMessagesReceived[channelIndex]));

            ++numMessagesReceived[channelIndex];
          }
          break;

          case E_TEST_BLOCK_MESSAGE:
          {
            TestBlockMessage* blockMessage = (TestBlockMessage*)message;
            check(blockMessage->sequence == uint16_t(numMessagesReceived[channelIndex]));

            const int blockSize = blockMessage->GetBlockSize();

            check(blockSize == 1 + ((numMessagesReceived[channelIndex] * 901) % 3333));

            const uint8_t* blockData = blockMessage->GetBlockData();
            check(blockData);

            for (int j = 0; j < blockSize; ++j)
            {
              check(blockData[j] == uint8_t(numMessagesReceived[channelIndex] + j));
            }
            ++numMessagesReceived[channelIndex];
          }
          break;
        }
        messageFactory.ReleaseMessage(message);
      }
    }

    bool receivedAllMessages = true;

    for (int channelIndex = 0; channelIndex < numChannels; ++channelIndex)
    {
      if (numMessagesReceived[channelIndex] != numMessagesSent)
      {
        receivedAllMessages = false;
        break;
      }
    }

    if (receivedAllMessages)
      break;
  }

  for (int channelIndex = 0; channelIndex < numChannels; ++channelIndex)
  {
    check(numMessagesReceived[channelIndex] == numMessagesSent);
  }
}


void test_connection_unreliable_unordered_messages()
{
  TestMessageFactory messageFactory(GetDefaultAllocator());

  double time = 100.0;

  ConnectionConfig connectionConfig;
  connectionConfig.numChannels = 1;
  connectionConfig.channel[0].type = E_CHANNEL_TYPE_UNRELIABLE_UNORDERED;

  Connection sender(GetDefaultAllocator(), messageFactory, connectionConfig, time);
  Connection receiver(GetDefaultAllocator(), messageFactory, connectionConfig, time);

  const int senderPort = 10000;
  const int receiverPort = 10001;

  Address senderAddress("::1", senderPort);
  Address receiverAddress("::1", receiverPort);

  const int numIterations = 256;
  const int numMessagesSent = 16;

  for (int j = 0; j < numMessagesSent; ++j)
  {
    TestMessage* message = (TestMessage*)messageFactory.CreateMessage(E_TEST_MESSAGE);
    check(message);
    message->sequence = j;
    sender.SendMessage(0, message);
  }

  int numMessagesReceived = 0;
  uint16_t senderSequence = 0;
  uint16_t receiverSequence = 0;

  for (int i = 0; i < numIterations; ++i)
  {
    pump_connection_update(connectionConfig, time, sender, receiver, senderSequence, receiverSequence, 0.1f, 0);

    while (true)
    {
      Message* message = receiver.ReceiveMessage(0);
      if (!message)
        break;

      check(message->GetType() == E_TEST_MESSAGE);

      TestMessage* testMessage = (TestMessage*)message;

      check(testMessage->sequence == uint16_t(numMessagesReceived));

      ++numMessagesReceived;

      messageFactory.ReleaseMessage(message);
    }

    if (numMessagesReceived == numMessagesSent)
      break;
  }

  check(numMessagesReceived == numMessagesSent);
}

void test_connection_unreliable_unordered_blocks()
{
  TestMessageFactory messageFactory(GetDefaultAllocator());

  double time = 100.0;

  ConnectionConfig connectionConfig;
  connectionConfig.numChannels = 1;
  connectionConfig.channel[0].type = E_CHANNEL_TYPE_UNRELIABLE_UNORDERED;

  Connection sender(GetDefaultAllocator(), messageFactory, connectionConfig, time);
  Connection receiver(GetDefaultAllocator(), messageFactory, connectionConfig, time);

  const int senderPort = 10000;
  const int receiverPort = 10001;

  Address senderAddress("::1", senderPort);
  Address receiverAddress("::1", receiverPort);

  const int numIterations = 256;
  const int numMessagesSent = 8;

  for (int j = 0; j < numMessagesSent; ++j)
  {
    TestBlockMessage* message = (TestBlockMessage*)messageFactory.CreateMessage(E_TEST_BLOCK_MESSAGE);
    check(message);

    message->sequence = j;
    const int blockSize = 1 + (j * 7);

    uint8_t* blockData = (uint8_t*)GG_ALLOCATE(messageFactory.GetAllocator(), blockSize);

    for (int k = 0; k < blockSize; ++k)
    {
      blockData[k] = j + k;
    }
    message->AttachBlock(messageFactory.GetAllocator(), blockData, blockSize);
    sender.SendMessage(0, message);
  }

  int numMessagesReceived = 0;

  uint16_t senderSequence = 0;
  uint16_t receiverSequence = 0;

  for (int i = 0; i < numIterations; ++i)
  {
    pump_connection_update(connectionConfig, time, sender, receiver, senderSequence, receiverSequence, 0.1f, 0);

    while (true)
    {
      Message * message = receiver.ReceiveMessage(0);
      if (!message)
        break;

      check(message->GetType() == E_TEST_BLOCK_MESSAGE);

      TestBlockMessage * blockMessage = (TestBlockMessage*)message;

      check(blockMessage->sequence == uint16_t(numMessagesReceived));

      const int blockSize = blockMessage->GetBlockSize();

      check(blockSize == 1 + (numMessagesReceived * 7));

      const uint8_t* blockData = blockMessage->GetBlockData();

      check(blockData);

      for (int j = 0; j < blockSize; ++j)
      {
        check(blockData[j] == uint8_t(numMessagesReceived + j));
      }

      ++numMessagesReceived;

      messageFactory.ReleaseMessage(message);
    }

    if (numMessagesReceived == numMessagesSent)
      break;
  }

  check(numMessagesReceived == numMessagesSent);
}

#include "Client\Client.h"
#include "Server\Server.h"

void PumpClientServerUpdate(double& time, Client** client, int numClients, Server** server, int numServers, float deltaTime = 0.1f)
{
  for (int i = 0; i < numClients; ++i)
    client[i]->SendPackets();

  for (int i = 0; i < numServers; ++i)
    server[i]->SendPackets();

  for (int i = 0; i < numClients; ++i)
    client[i]->ReceivePackets();

  for (int i = 0; i < numServers; ++i)
    server[i]->ReceivePackets();

  time += deltaTime;

  for (int i = 0; i < numClients; ++i)
    client[i]->AdvanceTime(time);

  for (int i = 0; i < numServers; ++i)
    server[i]->AdvanceTime(time);

  gg_sleep(0.0f);
}

void SendClientToServerMessages(Client& client, int numMessagesToSend, int channelIndex = 0)
{
  for (int i = 0; i < numMessagesToSend; ++i)
  {
    if (!client.CanSendMessage(channelIndex))
      break;

    if (rand() % 10)
    {
      TestMessage* message = (TestMessage*)client.CreateMessage(E_TEST_MESSAGE);
      check(message);
      message->sequence = i;
      client.SendMessage(channelIndex, message);
    }
    else
    {
      TestBlockMessage* message = (TestBlockMessage*)client.CreateMessage(E_TEST_BLOCK_MESSAGE);
      check(message);
      message->sequence = i;
      const int blockSize = 1 + ((i * 901) % 1001);
      uint8_t* blockData = client.AllocateBlock(blockSize);
      check(blockData);

      for (int j = 0; j < blockSize; ++j)
        blockData[j] = i + j;

      client.AttachBlockToMessage(message, blockData, blockSize);
      client.SendMessage(channelIndex, message);
    }
  }
}

void SendServerToClientMessages(Server& server, int clientIndex, int numMessagesToSend, int channelIndex = 0)
{
  for (int i = 0; i < numMessagesToSend; ++i)
  {
    if (!server.CanSendMessage(clientIndex, channelIndex))
      break;

    if (rand() % 10)
    {
      TestMessage* message = (TestMessage*)server.CreateMessage(clientIndex, E_TEST_MESSAGE);
      check(message);
      message->sequence = i;
      server.SendMessage(clientIndex, channelIndex, message);
    }
    else
    {
      TestBlockMessage* message = (TestBlockMessage*)server.CreateMessage(clientIndex, E_TEST_BLOCK_MESSAGE);
      check(message);
      message->sequence = i;
      const int blockSize = 1 + ((i * 901) % 1001);
      uint8_t* blockData = server.AllocateBlock(clientIndex, blockSize);
      check(blockData);

      for (int j = 0; j < blockSize; ++j)
        blockData[j] = i + j;

      server.AttachBlockToMessage(clientIndex, message, blockData, blockSize);
      server.SendMessage(clientIndex, channelIndex, message);
    }
  }
}

void ProcessServerToClientMessages(Client& client, int& numMessagesReceivedFromServer)
{
  while (true)
  {
    Message* message = client.ReceiveMessage(0);
    if (!message)
      break;

    check(message->GetId() == numMessagesReceivedFromServer);

    switch (message->GetType())
    {
      case E_TEST_MESSAGE:
      {
        check(!message->IsBlockMessage());
        TestMessage* testMessage = (TestMessage*)message;
        check(testMessage->sequence == uint16_t(numMessagesReceivedFromServer));
        ++numMessagesReceivedFromServer;
      }
      break;

      case E_TEST_BLOCK_MESSAGE:
      {
        check(message->IsBlockMessage());
        TestBlockMessage* blockMessage = (TestBlockMessage*)message;
        check(blockMessage->sequence == uint16_t(numMessagesReceivedFromServer));
        const int blockSize = 1 + ((numMessagesReceivedFromServer * 901) % 1001);
        const uint8_t* blockData = blockMessage->GetBlockData();
        check(blockData);
        for (int j = 0; j < blockSize; ++j)
          check(blockData[j] == uint8_t(numMessagesReceivedFromServer + j));

        ++numMessagesReceivedFromServer;
      }
      break;
    }

    client.ReleaseMessage(message);
  }
}

void ProcessClientToServerMessages(Server& server, int clientIndex, int& numMessagesReceivedFromClient)
{
  while (true)
  {
    Message* message = server.ReceiveMessage(clientIndex, 0);
    if (!message)
      break;

    check(message->GetId() == numMessagesReceivedFromClient);

    switch (message->GetType())
    {
      case E_TEST_MESSAGE:
      {
        check(!message->IsBlockMessage());
        TestMessage* testMessage = (TestMessage*)message;
        check(testMessage->sequence == uint16_t(numMessagesReceivedFromClient));
        ++numMessagesReceivedFromClient;
      }
      break;

      case E_TEST_BLOCK_MESSAGE:
      {
        check(message->IsBlockMessage());
        TestBlockMessage* blockMessage = (TestBlockMessage*)message;
        check(blockMessage->sequence == uint16_t(numMessagesReceivedFromClient));
        const int blockSize = 1 + ((numMessagesReceivedFromClient * 901) % 1001);
        const uint8_t* blockData = blockMessage->GetBlockData();
        check(blockData);
        for (int j = 0; j < blockSize; ++j)
          check(blockData[j] == uint8_t(numMessagesReceivedFromClient + j));

        ++numMessagesReceivedFromClient;
      }
      break;
    }

    server.ReleaseMessage(clientIndex, message);
  }
}

void test_client_server_messages()
{
  const uint64_t clientId = 1;

  Address clientAddress("0.0.0.0", ClientPort);
  Address serverAddress("127.0.0.1", ServerPort);

  double time = 100.0;

  ClientServerConfig config;
  config.channel[0].messageSendQueueSize = 32;
  config.channel[0].maxMessagesPerPacket = 8;
  config.channel[0].maxBlockSize = 1024;
  config.channel[0].blockFragmentSize = 200;

  Client client(GetDefaultAllocator(), clientAddress, config, adapter, time);

  uint8_t privateKey[KEY_BYTES];
  memset(privateKey, 0, KEY_BYTES);

  Server server(GetDefaultAllocator(), privateKey, serverAddress, config, adapter, time);

  server.Start(MAX_CLIENTS);

  client.SetLatency(250);
  client.SetJitter(100);
  client.SetPacketLoss(25);
  client.SetDuplicates(25);

  server.SetLatency(250);
  server.SetJitter(100);
  server.SetPacketLoss(25);
  server.SetDuplicates(25);

  for (int iteration = 0; iteration < 2; ++iteration)
  {
    client.InsecureConnect(privateKey, clientId, serverAddress);

    const int numIterations = 10000;

    for (int i = 0; i < numIterations; ++i)
    {
      Client* clients[] = { &client };
      Server* servers[] = { &server };

      PumpClientServerUpdate(time, clients, 1, servers, 1);

      if (client.ConnectionFailed())
        break;
      if (!client.IsConnecting() && client.IsConnected() && server.GetNumConnectedClients() == 1)
        break;
    }

    check(!client.IsConnecting());
    check(client.IsConnected());
    check(server.GetNumConnectedClients() == 1);
    check(client.GetClientIndex() == 0);
    check(server.IsClientConnected(0));

    const int numMessagesSent = config.channel[0].messageSendQueueSize;

    SendClientToServerMessages(client, numMessagesSent);

    SendServerToClientMessages(server, client.GetClientIndex(), numMessagesSent);

    int numMessagesReceivedFromClient = 0;
    int numMessagesReceivedFromServer = 0;

    for (int i = 0; i < numIterations; ++i)
    {
      if (!client.IsConnected())
        break;

      Client* clients[] = { &client };
      Server* servers[] = { &server };

      PumpClientServerUpdate(time, clients, 1, servers, 1);

      ProcessServerToClientMessages(client, numMessagesReceivedFromServer);

      ProcessClientToServerMessages(server, client.GetClientIndex(), numMessagesReceivedFromClient);

      if (numMessagesReceivedFromClient == numMessagesSent && numMessagesReceivedFromServer == numMessagesSent)
        break;
    }

    check(client.IsConnected());
    check(server.IsClientConnected(client.GetClientIndex()));
    check(numMessagesReceivedFromClient == numMessagesSent);
    check(numMessagesReceivedFromServer == numMessagesSent);

    client.Disconnect();

    for (int i = 0; i < numIterations; ++i)
    {
      Client* clients[] = { &client };
      Server* servers[] = { &server };

      PumpClientServerUpdate(time, clients, 1, servers, 1);

      if (!client.IsConnected() && server.GetNumConnectedClients() == 0)
        break;
    }
    check(!client.IsConnected() && server.GetNumConnectedClients() == 0);
  }
  server.Stop();
}


void CreateClients(int numClients, Client** clients, const Address& address, const ClientServerConfig& config, Adapter& _adapter, double time)
{
  for (int i = 0; i < numClients; ++i)
  {
    clients[i] = GG_NEW(GetDefaultAllocator(), Client, GetDefaultAllocator(), address, config, _adapter, time);
    clients[i]->SetLatency(250);
    clients[i]->SetJitter(100);
    clients[i]->SetPacketLoss(25);
    clients[i]->SetDuplicates(25);
  }
}

void ConnectClients(int numClients, Client** clients, const uint8_t privateKey[], const Address& serverAddress)
{
  for (int i = 0; i < numClients; ++i)
    clients[i]->InsecureConnect(privateKey, i + 1, serverAddress);
}

void DestroyClients(int numClients, Client** clients)
{
  for (int i = 0; i < numClients; ++i)
  {
    clients[i]->Disconnect();

    GG_DELETE(GetDefaultAllocator(), Client, clients[i]);
  }
}

bool AllClientsConnected(int numClients, Server& server, Client** clients)
{
  if (server.GetNumConnectedClients() != numClients)
    return false;

  for (int i = 0; i < numClients; ++i)
  {
    if (!clients[i]->IsConnected())
      return false;
  }

  return true;
}

bool AnyClientDisconnected(int numClients, Client** clients)
{
  for (int i = 0; i < numClients; ++i)
  {
    if (clients[i]->IsDisconnected())
      return true;
  }

  return false;
}

void test_client_server_start_stop_restart()
{
  Address clientAddress("0.0.0.0", 0);
  Address serverAddress("127.0.0.1", ServerPort);

  double time = 100.0;

  ClientServerConfig config;
  config.channel[0].messageSendQueueSize = 32;
  config.channel[0].maxMessagesPerPacket = 8;
  config.channel[0].maxBlockSize = 1024;
  config.channel[0].blockFragmentSize = 200;

  uint8_t privateKey[KEY_BYTES];
  memset(privateKey, 0, KEY_BYTES);

  Server server(GetDefaultAllocator(), privateKey, serverAddress, config, adapter, time);

  server.Start(MAX_CLIENTS);

  server.SetLatency(250);
  server.SetJitter(100);
  server.SetPacketLoss(25);
  server.SetDuplicates(25);

  int numClients[] = { 3, 5, 1 };

  const int numIterations = sizeof(numClients) / sizeof(int);

  for (int iteration = 0; iteration < numIterations; ++iteration)
  {
    server.Start(numClients[iteration]);

    Client* clients[MAX_CLIENTS];

    CreateClients(numClients[iteration], clients, clientAddress, config, adapter, time);

    ConnectClients(numClients[iteration], clients, privateKey, serverAddress);

    while (true)
    {
      Server* servers[] = { &server };

      PumpClientServerUpdate(time, (Client**)clients, numClients[iteration], servers, 1);

      if (AnyClientDisconnected(numClients[iteration], clients))
        break;

      if (AllClientsConnected(numClients[iteration], server, clients))
        break;
    }

    check(AllClientsConnected(numClients[iteration], server, clients));

    const int numMessagesSent = config.channel[0].messageSendQueueSize;

    for (int clientIndex = 0; clientIndex < numClients[iteration]; ++clientIndex)
    {
      SendClientToServerMessages(*clients[clientIndex], numMessagesSent);
      SendServerToClientMessages(server, clientIndex, numMessagesSent);
    }

    int numMessagesReceivedFromClient[MAX_CLIENTS];
    int numMessagesReceivedFromServer[MAX_CLIENTS];

    memset(numMessagesReceivedFromClient, 0, sizeof(numMessagesReceivedFromClient));
    memset(numMessagesReceivedFromServer, 0, sizeof(numMessagesReceivedFromServer));

    const int numInternalIterations = 10000;

    for (int i = 0; i < numInternalIterations; ++i)
    {
      Server* servers[] = { &server };

      PumpClientServerUpdate(time, clients, numClients[iteration], servers, 1);

      bool allMessagesReceived = true;

      for (int j = 0; j < numClients[iteration]; ++j)
      {
        ProcessServerToClientMessages(*clients[j], numMessagesReceivedFromServer[j]);

        if (numMessagesReceivedFromServer[j] != numMessagesSent)
          allMessagesReceived = false;

        int clientIndex = clients[j]->GetClientIndex();

        ProcessClientToServerMessages(server, clientIndex, numMessagesReceivedFromClient[clientIndex]);

        if (numMessagesReceivedFromClient[clientIndex] != numMessagesSent)
          allMessagesReceived = false;
      }

      if (allMessagesReceived)
        break;
    }

    for (int clientIndex = 0; clientIndex < numClients[iteration]; ++clientIndex)
    {
      check(numMessagesReceivedFromClient[clientIndex] == numMessagesSent);
      check(numMessagesReceivedFromServer[clientIndex] == numMessagesSent);
    }

    DestroyClients(numClients[iteration], clients);

    server.Stop();
  }
}


void test_client_server_message_failed_to_serialize_reliable_ordered()
{
  const uint64_t clientId = 1;

  Address clientAddress("0.0.0.0", ClientPort);
  Address serverAddress("127.0.0.1", ServerPort);

  double time = 100.0;

  ClientServerConfig config;
  config.maxPacketSize = 1100;
  config.numChannels = 1;
  config.channel[0].type = E_CHANNEL_TYPE_RELIABLE_ORDERED;
  config.channel[0].maxBlockSize = 1024;
  config.channel[0].blockFragmentSize = 200;

  uint8_t privateKey[KEY_BYTES];
  memset(privateKey, 0, KEY_BYTES);

  Server server(GetDefaultAllocator(), privateKey, serverAddress, config, adapter, time);

  server.Start(MAX_CLIENTS);

  Client client(GetDefaultAllocator(), clientAddress, config, adapter, time);

  client.InsecureConnect(privateKey, clientId, serverAddress);

  const int numIterations = 10000;

  for (int i = 0; i < numIterations; ++i)
  {
    Client* clients[] = { &client };
    Server* servers[] = { &server };
    PumpClientServerUpdate(time, clients, 1, servers, 1);

    if (client.ConnectionFailed())
      break;

    if (!client.IsConnecting() && client.IsConnected() && server.GetNumConnectedClients() == 1)
      break;
  }

  check(!client.IsConnecting());
  check(client.IsConnected());
  check(server.GetNumConnectedClients() == 1);
  check(client.GetClientIndex() == 0);
  check(server.IsClientConnected(0));

  // send a message from client to server that fails to serialize on read, this should disconnect the client from the server
  Message* message = client.CreateMessage(E_TEST_SERIALIZE_FAIL_ON_READ_MESSAGE);
  check(message);
  client.SendMessage(0, message);

  for (int i = 0; i < 256; ++i)
  {
    Client* clients[] = { &client };
    Server* servers[] = { &server };

    PumpClientServerUpdate(time, clients, 1, servers, 1);

    if (!client.IsConnected() && server.GetNumConnectedClients() == 0)
      break;
  }

  check(!client.IsConnected() && server.GetNumConnectedClients() == 0);

  client.Disconnect();

  server.Stop();
}

void test_client_server_message_failed_to_serialize_unreliable_unordered()
{
  const uint64_t clientId = 1;

  Address clientAddress("0.0.0.0", ClientPort);
  Address serverAddress("127.0.0.1", ServerPort);

  double time = 100.0;

  ClientServerConfig config;
  config.maxPacketSize = 1100;
  config.numChannels = 1;
  config.channel[0].type = E_CHANNEL_TYPE_UNRELIABLE_UNORDERED;
  config.channel[0].maxBlockSize = 1024;
  config.channel[0].blockFragmentSize = 200;

  uint8_t privateKey[KEY_BYTES];
  memset(privateKey, 0, KEY_BYTES);

  Server server(GetDefaultAllocator(), privateKey, serverAddress, config, adapter, time);

  server.Start(MAX_CLIENTS);

  Client client(GetDefaultAllocator(), clientAddress, config, adapter, time);

  client.InsecureConnect(privateKey, clientId, serverAddress);

  const int numIterations = 10000;

  for (int i = 0; i < numIterations; ++i)
  {
    Client* clients[] = { &client };
    Server* servers[] = { &server };
    PumpClientServerUpdate(time, clients, 1, servers, 1);

    if (client.ConnectionFailed())
      break;

    if (!client.IsConnecting() && client.IsConnected() && server.GetNumConnectedClients() == 1)
      break;
  }

  check(!client.IsConnecting());
  check(client.IsConnected());
  check(server.GetNumConnectedClients() == 1);
  check(client.GetClientIndex() == 0);
  check(server.IsClientConnected(0));

  // send a message from client to server that fails to serialize on read, this should disconnect the client from the server
  for (int i = 0; i < 256; ++i)
  {
    Client* clients[] = { &client };
    Server* servers[] = { &server };

    Message* message = client.CreateMessage(E_TEST_SERIALIZE_FAIL_ON_READ_MESSAGE);
    check(message);
    client.SendMessage(0, message);
    PumpClientServerUpdate(time, clients, 1, servers, 1);

    if (!client.IsConnected() && server.GetNumConnectedClients() == 0)
      break;
  }

  check(!client.IsConnected() && server.GetNumConnectedClients() == 0);

  client.Disconnect();

  server.Stop();
}


void test_client_server_message_exhaust_stream_allocator()
{
  const u64 clientId = 1;

  Address clientAddress("0.0.0.0", ClientPort);
  Address serverAddress("127.0.0.1", ServerPort);

  f64 time = 100.0;

  ClientServerConfig config;
  config.maxPacketSize = 1100;
  config.numChannels = 1;
  config.channel[0].type = E_CHANNEL_TYPE_RELIABLE_ORDERED;
  config.channel[0].maxBlockSize = 1024;
  config.channel[0].blockFragmentSize = 200;


  u08 privateKey[KEY_BYTES];
  memset(privateKey, 0, KEY_BYTES);

  Server server(GetDefaultAllocator(), privateKey, serverAddress, config, adapter, time);

  server.Start(MAX_CLIENTS);

  Client client(GetDefaultAllocator(), clientAddress, config, adapter, time);

  client.InsecureConnect(privateKey, clientId, serverAddress);

  const s32 numIterations = 10000;

  for (s32 i = 0; i < numIterations; ++i)
  {
    Client* clients[] = { &client };
    Server* servers[] = { &server };

    PumpClientServerUpdate(time, clients, 1, servers, 1);

    if (client.ConnectionFailed())
      break;

    if (!client.IsConnecting() && client.IsConnected() && server.GetNumConnectedClients() == 1)
      break;
  }

  check(!client.IsConnecting());
  check(client.IsConnected());
  check(server.GetNumConnectedClients() == 1);
  check(client.GetClientIndex() == 0);
  check(server.IsClientConnected(0));

  // send message from client to server that exhausts the stream allocator on read, this should disconnect the client from the server
  Message* message = client.CreateMessage(E_TEST_EXHAUST_STREAM_ALLOCATOR_ON_READ_MESSAGE);
  check(message);
  client.SendMessage(0, message);

  for (s32 i = 0; i < 256; ++i)
  {
    Client* clients[] = { &client };
    Server* servers[] = { &server };

    PumpClientServerUpdate(time, clients, 1, servers, 1);

    if (!client.IsConnected() && server.GetNumConnectedClients() == 0)
      break;
  }

  check(!client.IsConnected() && server.GetNumConnectedClients() == 0);

  client.Disconnect();

  server.Stop();
}

void test_client_server_message_receive_queue_overflow()
{
  const u64 clientId = 1;

  Address clientAddress("0.0.0.0", ClientPort);
  Address serverAddress("127.0.0.1", ServerPort);

  f64 time = 100.0;

  ClientServerConfig config;
  config.maxPacketSize = 1100;
  config.numChannels = 1;
  config.channel[0].type = E_CHANNEL_TYPE_RELIABLE_ORDERED;
  config.channel[0].maxBlockSize = 1024;
  config.channel[0].blockFragmentSize = 200;
  config.channel[0].messageSendQueueSize = 1024;
  config.channel[0].messageReceiveQueueSize = 256;


  u08 privateKey[KEY_BYTES];
  memset(privateKey, 0, KEY_BYTES);

  Server server(GetDefaultAllocator(), privateKey, serverAddress, config, adapter, time);

  server.Start(MAX_CLIENTS);

  Client client(GetDefaultAllocator(), clientAddress, config, adapter, time);

  client.InsecureConnect(privateKey, clientId, serverAddress);


  while (true)
  {
    Client* clients[] = { &client };
    Server* servers[] = { &server };

    PumpClientServerUpdate(time, clients, 1, servers, 1);

    if (client.ConnectionFailed())
      break;

    if (!client.IsConnecting() && client.IsConnected() && server.GetNumConnectedClients() == 1)
      break;
  }

  check(!client.IsConnecting());
  check(client.IsConnected());
  check(server.GetNumConnectedClients() == 1);
  check(client.GetClientIndex() == 0);
  check(server.IsClientConnected(0));

  // send a lot of messages, but don't dequeue them, this tests that the receive queue is able to handle overflow
  // ex.  the receiver should detect an error and disconnect the client, because the message is out of bounds
  const s32 numMessagesSent = config.channel[0].messageSendQueueSize;

  SendClientToServerMessages(client, numMessagesSent);

  for (s32 i = 0; i < numMessagesSent * 4; ++i)
  {
    Client* clients[] = { &client };
    Server* servers[] = { &server };

    PumpClientServerUpdate(time, clients, 1, servers, 1);
  }

  check(!client.IsConnected());
  check(server.GetNumConnectedClients() == 0);

  client.Disconnect();

  server.Stop();
}

void test_reliable_fragment_overflow_bug()
{
  double time = 100.0;

  ClientServerConfig config;
  config.numChannels = 2;
  config.channel[0].type = E_CHANNEL_TYPE_UNRELIABLE_UNORDERED;
  config.channel[0].packetBudget = 8000;
  config.channel[1].type = E_CHANNEL_TYPE_RELIABLE_ORDERED;
  config.channel[1].packetBudget = -1;

  u08 privateKey[KEY_BYTES];
  memset(privateKey, 0, KEY_BYTES);

  Server server(GetDefaultAllocator(), privateKey, Address("127.0.0.1", ServerPort), config, adapter, time);

  server.Start(MAX_CLIENTS);
  check(server.IsRunning());

  u64 clientId = 0;
  RandomBytes((uint8_t*)&clientId, 8);

  Client client(GetDefaultAllocator(), Address("0.0.0.0"), config, adapter, time);

  Address serverAddress("127.0.0.1", ServerPort);

  client.InsecureConnect(privateKey, clientId, serverAddress);

  Client* clients[] = { &client };
  Server* servers[] = { &server };

  while (true)
  {
    PumpClientServerUpdate(time, clients, 1, servers, 1);

    if (client.ConnectionFailed())
      break;

    if (!client.IsConnecting() && client.IsConnected() && server.GetNumConnectedClients() == 1)
      break;
  }

  check(!client.IsConnecting());
  check(client.IsConnected());
  check(client.GetClientIndex() == 0);
  check(server.GetNumConnectedClients() == 1);
  check(server.IsClientConnected(0));

  PumpClientServerUpdate(time, clients, 1, servers, 1);
  check(!client.IsDisconnected());

  TestBlockMessage* testBlockMessage = (TestBlockMessage*)client.CreateMessage(E_TEST_BLOCK_MESSAGE);
  uint8_t* blockData = client.AllocateBlock(7169);
  memset(blockData, 0, 7169);
  client.AttachBlockToMessage(testBlockMessage, blockData, 7169);
  client.SendMessage(0, testBlockMessage);

  testBlockMessage = (TestBlockMessage*)client.CreateMessage(E_TEST_BLOCK_MESSAGE);
  blockData = client.AllocateBlock(1024);
  memset(blockData, 0, 1024);
  client.AttachBlockToMessage(testBlockMessage, blockData, 1024);
  client.SendMessage(1, testBlockMessage);

  PumpClientServerUpdate(time, clients, 1, servers, 1);
  PumpClientServerUpdate(time, clients, 1, servers, 1);
  PumpClientServerUpdate(time, clients, 1, servers, 1);
  check(!client.IsDisconnected());


  Message* message = server.ReceiveMessage(0, 0);
  check(message);
  check(message->GetType() == E_TEST_BLOCK_MESSAGE);
  server.ReleaseMessage(0, message);

  message = server.ReceiveMessage(0, 1);
  check(message);
  check(message->GetType() == E_TEST_BLOCK_MESSAGE);
  server.ReleaseMessage(0, message);

  client.Disconnect();

  server.Stop();
}

void test_single_message_type_reliable()
{
  SingleTestMessageFactory messageFactory(GetDefaultAllocator());
  double time = 100.0;

  ConnectionConfig connectionConfig;

  Connection sender(GetDefaultAllocator(), messageFactory, connectionConfig, time);
  Connection receiver(GetDefaultAllocator(), messageFactory, connectionConfig, time);

  const int numMessagesSent = 64;

  for (int i = 0; i < numMessagesSent; ++i)
  {
    TestMessage* message = (TestMessage*)messageFactory.CreateMessage(E_SINGLE_TEST_MESSAGE);
    check(message);
    message->sequence = i;
    sender.SendMessage(0, message);
  }

  const int SenderPort = 10000;
  const int ReceiverPort = 10001;

  Address senderAddress("::1", SenderPort);
  Address receiverAddress("::1", ReceiverPort);

  int numMessagesReceived = 0;

  const int NumIterations = 1000;

  uint16_t senderSequence = 0;
  uint16_t receiverSequence = 0;

  for (int i = 0; i < NumIterations; ++i)
  {
    pump_connection_update(connectionConfig, time, sender, receiver, senderSequence, receiverSequence);

    while (true)
    {
      Message* message = receiver.ReceiveMessage(0);
      if (!message)
        break;

      check(message->GetId() == (int)numMessagesReceived);
      check(message->GetType() == E_SINGLE_TEST_MESSAGE);

      TestMessage* testMessage = (TestMessage*)message;
      check(testMessage->sequence == numMessagesReceived);

      ++numMessagesReceived;

      messageFactory.ReleaseMessage(message);
    }
    if (numMessagesReceived == numMessagesSent)
      break;
  }
  check(numMessagesReceived == numMessagesSent);
}

void test_single_message_type_reliable_blocks()
{
  SingleBlockTestMessageFactory messageFactory(GetDefaultAllocator());
  double time = 100.0;

  ConnectionConfig connectionConfig;

  Connection sender(GetDefaultAllocator(), messageFactory, connectionConfig, time);
  Connection receiver(GetDefaultAllocator(), messageFactory, connectionConfig, time);

  const int numMessagesSent = 32;

  for (int i = 0; i < numMessagesSent; ++i)
  {
    TestBlockMessage* message = (TestBlockMessage*)messageFactory.CreateMessage(E_SINGLE_BLOCK_TEST_MESSAGE);
    check(message);
    message->sequence = i;
    const int blockSize = 1 + ((i * 901) % 3333);
    uint8_t* blockData = (uint8_t*)GG_ALLOCATE(messageFactory.GetAllocator(), blockSize);
    for (int j = 0; j < blockSize; ++j)
      blockData[j] = i + j;

    message->AttachBlock(messageFactory.GetAllocator(), blockData, blockSize);
    sender.SendMessage(0, message);
  }

  const int SenderPort = 10000;
  const int ReceiverPort = 10001;
  Address senderAddress("::1", SenderPort);
  Address receiverAddress("::1", ReceiverPort);

  int numMessagesReceived = 0;

  const int NumIterations = 10000;

  uint16_t senderSequence = 0;
  uint16_t receiverSequence = 0;

  for (int i = 0; i < NumIterations; ++i)
  {
    pump_connection_update(connectionConfig, time, sender, receiver, senderSequence, receiverSequence);

    while (true)
    {
      Message* message = receiver.ReceiveMessage(0);
      if (!message)
        break;

      check(message->GetId() == (int)numMessagesReceived);
      check(message->GetType() == E_SINGLE_BLOCK_TEST_MESSAGE);

      TestBlockMessage* blockMessage = (TestBlockMessage*)message;
      check(blockMessage->sequence == uint16_t(numMessagesReceived));

      const int blockSize = blockMessage->GetBlockSize();
      check(blockSize == 1 + ((numMessagesReceived * 901) % 3333));
      const uint8_t* blockData = blockMessage->GetBlockData();
      check(blockData);

      for (int j = 0; j < blockSize; ++j)
        check(blockData[j] == uint8_t(numMessagesReceived + j));

      ++numMessagesReceived;
      messageFactory.ReleaseMessage(message);
    }
    if (numMessagesReceived == numMessagesSent)
      break;
  }
  check(numMessagesReceived == numMessagesSent);

}

void test_single_message_type_unreliable()
{
  SingleTestMessageFactory messageFactory(GetDefaultAllocator());
  double time = 100.0;

  ConnectionConfig connectionConfig;
  connectionConfig.numChannels = 1;
  connectionConfig.channel[0].type = E_CHANNEL_TYPE_UNRELIABLE_UNORDERED;

  Connection sender(GetDefaultAllocator(), messageFactory, connectionConfig, time);
  Connection receiver(GetDefaultAllocator(), messageFactory, connectionConfig, time);

  const int SenderPort = 10000;
  const int ReceiverPort = 10001;
  Address senderAddress("::1", SenderPort);
  Address receiverAddress("::1", ReceiverPort);

  const int NumIterations = 256;
  const int numMessagesSent = 16;

  for (int i = 0; i < numMessagesSent; ++i)
  {
    TestMessage* message = (TestMessage*)messageFactory.CreateMessage(E_SINGLE_TEST_MESSAGE);
    check(message);
    message->sequence = i;
    sender.SendMessage(0, message);
  }


  int numMessagesReceived = 0;
  uint16_t senderSequence = 0;
  uint16_t receiverSequence = 0;

  for (int i = 0; i < NumIterations; ++i)
  {
    pump_connection_update(connectionConfig, time, sender, receiver, senderSequence, receiverSequence, 0.1f, 0);

    while (true)
    {
      Message* message = receiver.ReceiveMessage(0);
      if (!message)
        break;

      check(message->GetType() == E_SINGLE_TEST_MESSAGE);
      TestMessage* testMessage = (TestMessage*)message;
      check(testMessage->sequence == uint16_t(numMessagesReceived));

      ++numMessagesReceived;

      messageFactory.ReleaseMessage(message);
    }
    if (numMessagesReceived == numMessagesSent)
      break;
  }
  check(numMessagesReceived == numMessagesSent);
}

#define RUN_TEST(test_function)                   \
do                                                \
{                                                 \
  printf("\n" #test_function "\n");               \
  if (!InitializeGG())                            \
  {                                               \
    printf("Error: failed to initialize GG\n");   \
    exit(1);                                      \
  }                                               \
  test_function();                                \
  ShutdownGG();                                   \
}                                                 \
while(0)


#include "Netcode\Netcode.h" // netcode_test()
#include "Reliable\Reliable.h" // set reliable log level and reliable_test()

#ifndef SOAK
#define SOAK 1
#endif

#if SOAK
#include <signal.h>
static volatile int quit = 0;

void interrupt_handler(int /*dummy*/)
{
  quit = 1;
}
#endif


int main()
{
  srand(time(nullptr));
  GG::gg_log_level(GG_LOG_LEVEL_DEBUG);
  GG::reliable_log_level(GG_LOG_LEVEL_DEBUG);

#if SOAK
  signal(SIGINT, interrupt_handler);
  s32 iter = 0;
  while (true)
#endif
  {
     // /*
    {
      printf("[netcode]\n\n");

      check(InitializeGG());
      netcode_test();
      ShutdownGG();
    }

    {
      printf("\n[reliable]\n\n");
      check(InitializeGG());
      reliable_test();
      ShutdownGG();
    }

    printf("\n[GG]\n\n");

    RUN_TEST(test_endian);
    RUN_TEST(test_queue);
#if GG_WITH_MBEDTLS
    RUN_TEST(test_base64);
#endif

    RUN_TEST(test_bitpacker);
    RUN_TEST(test_bits_required);
    RUN_TEST(test_stream);
    RUN_TEST(test_address);
    RUN_TEST(test_bit_array);
    RUN_TEST(test_sequence_buffer);
    RUN_TEST(test_allocator_tlsf);
    
    RUN_TEST(test_connection_reliable_ordered_messages);
    RUN_TEST(test_connection_reliable_ordered_blocks);
    RUN_TEST(test_connection_reliable_ordered_messages_and_blocks);
    RUN_TEST(test_connection_reliable_ordered_messages_and_blocks_multiple_channels);
    RUN_TEST(test_connection_unreliable_unordered_messages);
    RUN_TEST(test_connection_unreliable_unordered_blocks);
    
    RUN_TEST(test_client_server_messages);
    RUN_TEST(test_client_server_start_stop_restart);
    RUN_TEST(test_client_server_message_failed_to_serialize_reliable_ordered);
    RUN_TEST(test_client_server_message_failed_to_serialize_unreliable_unordered);
    RUN_TEST(test_client_server_message_exhaust_stream_allocator);
    // */
	RUN_TEST(test_client_server_message_receive_queue_overflow);
   RUN_TEST(test_reliable_fragment_overflow_bug);
   RUN_TEST(test_single_message_type_reliable);
   RUN_TEST(test_single_message_type_reliable_blocks);
   RUN_TEST(test_single_message_type_unreliable);

#if SOAK
    if (quit)
      break;
    ++iter;
    for (int j = 0; j < iter % 10; ++j)
      printf(".");

    printf("\n");
#endif // SOAK
  }


#if SOAK
  if (quit)
    printf("\n");
  else
#else
  printf("\n*** ALL TESTS PASS ***\n\n");
#endif // SOAK

  return 0;
}

