#pragma once

using namespace GG;

const uint64_t ProtocolId = 0x11223344556677ULL;

const int ClientPort = 30000;
const int ServerPort = 40000;

inline int GetNumBitsForMessage(uint16_t sequence)
{
  static int messageBitsArray[] = { 1, 320, 120, 4, 256, 45, 11, 13, 101, 100, 84, 95, 203, 2, 3, 8, 512, 5, 3, 7, 50 };

  const int modulus = sizeof(messageBitsArray) / sizeof(int);
  const int index = sequence % modulus;
  return messageBitsArray[index];
}

#include "Message.h"
#include "ISerializeable.h"// virtual serialize funtions
#include "Serializer.h" // SERIALZE_BITS, ect

struct TestMessage : public Message
{
  uint16_t sequence;

  TestMessage() { sequence = 0; }

  template <typename Stream>
  bool Serialize(Stream& stream)
  {
    SERIALIZE_BITS(stream, sequence, 16);

    int numBits = GetNumBitsForMessage(sequence);
    int numWords = numBits / 32;
    uint32_t dummy = 0;

    for (int i = 0; i < numWords; ++i)
      SERIALIZE_BITS(stream, dummy, 32);

    int numRemainderBits = numBits - numWords * 32;

    if (numRemainderBits > 0)
      SERIALIZE_BITS(stream, dummy, numRemainderBits);

    return true;
  }

  GG_VIRTUAL_SERIALIZE_FUNCTIONS();
};

#include "BlockMessage.h"

struct TestBlockMessage : public BlockMessage
{
  uint16_t sequence;

  TestBlockMessage() { sequence = 0; }

  template <typename Stream>
  bool Serialize(Stream& stream)
  {
    SERIALIZE_BITS(stream, sequence, 16);
    return true;
  }
  GG_VIRTUAL_SERIALIZE_FUNCTIONS();
};

struct TestSerializeFailOnReadMessage : public Message
{
  template <typename Stream>
  bool Serialize(Stream& /*stream*/)
  {
    return !Stream::IsReading;
  }
  GG_VIRTUAL_SERIALIZE_FUNCTIONS();
};

struct TestExhaustStreamAllocatorOnReadMessage : public Message
{
  template <typename Stream>
  bool Serialize(Stream& stream)
  {
    if (Stream::IsReading)
    {
      const int numBuffers = 100;

      void* buffers[numBuffers];

      memset(buffers, 0, sizeof(buffers));

      for (int i = 0; i < numBuffers; ++i)
      {
        buffers[i] = GG_ALLOCATE(stream.GetAllocator(), 1024*1024);
      }

      for (int i = 0; i < numBuffers; ++i)
      {
        GG_FREE(stream.GetAllocator(), buffers[i]);
      }
    }
    return true;
  }

  GG_VIRTUAL_SERIALIZE_FUNCTIONS();
};


enum TestMessageType
{
  E_TEST_MESSAGE,
  E_TEST_BLOCK_MESSAGE,
  E_TEST_SERIALIZE_FAIL_ON_READ_MESSAGE,
  E_TEST_EXHAUST_STREAM_ALLOCATOR_ON_READ_MESSAGE,
  E_NUM_TEST_MESSAGE_TYPES
};

#include "Serializer.h"

GG_MESSAGE_FACTORY_START(TestMessageFactory, E_NUM_TEST_MESSAGE_TYPES);
  GG_DECLARE_MESSAGE_TYPE(E_TEST_MESSAGE, TestMessage);
  GG_DECLARE_MESSAGE_TYPE(E_TEST_BLOCK_MESSAGE, TestBlockMessage);
  GG_DECLARE_MESSAGE_TYPE(E_TEST_SERIALIZE_FAIL_ON_READ_MESSAGE, TestSerializeFailOnReadMessage);
  GG_DECLARE_MESSAGE_TYPE(E_TEST_EXHAUST_STREAM_ALLOCATOR_ON_READ_MESSAGE, TestExhaustStreamAllocatorOnReadMessage);
GG_MESSAGE_FACTORY_FINISH();

enum SingleTestMessageType
{
  E_SINGLE_TEST_MESSAGE,
  E_NUM_SINGLE_TEST_MESSAGE_TYPES
};

GG_MESSAGE_FACTORY_START(SingleTestMessageFactory, E_NUM_SINGLE_TEST_MESSAGE_TYPES);
  GG_DECLARE_MESSAGE_TYPE(E_SINGLE_TEST_MESSAGE, TestMessage);
GG_MESSAGE_FACTORY_FINISH();

enum SingleBlockTestMessageType
{
  E_SINGLE_BLOCK_TEST_MESSAGE,
  E_NUM_SINGLE_BLOCK_TEST_MESSAGE_TYPES
};

GG_MESSAGE_FACTORY_START(SingleBlockTestMessageFactory, E_NUM_SINGLE_BLOCK_TEST_MESSAGE_TYPES);
  GG_DECLARE_MESSAGE_TYPE(E_SINGLE_BLOCK_TEST_MESSAGE, TestBlockMessage);
GG_MESSAGE_FACTORY_FINISH();

#include "Adapter.h"

class TestAdapter : public Adapter
{
public:
  MessageFactory* CreateMessageFactory(Allocator& allocator)
  {
    return GG_NEW(allocator, TestMessageFactory, allocator);
  }
};

static TestAdapter adapter;
