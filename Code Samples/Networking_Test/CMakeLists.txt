cmake_minimum_required(VERSION 3.17)

project("Networking_Test")


set(header_files
   include/Test.h
)

set(source_files
   src/test.cpp
)

ADD_EXECUTABLE("${PROJECT_NAME}" 
               ${source_files} 
               ${header_files}
               )

target_include_directories("${PROJECT_NAME}" PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")

# Libs 
target_link_directories("${PROJECT_NAME}" PRIVATE ${_VCPKG_INSTALLED_DIR}/${VCPKG_TARGET_TRIPLET}/lib)
target_link_libraries("${PROJECT_NAME}" PRIVATE Networking)
