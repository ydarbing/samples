// Brady Reuter
// 11/07/2016
// KMP string search algorithm 
#pragma once

#include <string>
#include <vector>

// S = textToSearch
// W = the string sought to be found in S
//returns index in S where W is found
size_t KMP_Search(std::string & S, std::string& W);

size_t KMP_Search(size_t index, std::string & S, std::string& W);
