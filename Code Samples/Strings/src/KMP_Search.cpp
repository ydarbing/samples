#include "KMP_Search.h"

// W = word to be analyzed
// return a filled table
std::vector<int> MakeKMPTable(std::string& W);


// S = textToSearch
// W = the string sought to be found in S
//returns index in S where W is found
size_t KMP_Search(std::string & S, std::string& W)
{
  return KMP_Search(0, S, W);
}

// index = index to start at in S
// S = textToSearch
// W = the string sought to be found in S
//returns index in S where W is found
size_t KMP_Search(size_t index, std::string & S, std::string& W)
{
  if (W.empty() || index >= S.size() || S.size() - index < W.size())
    return S.size();

  size_t m = index; // beginning of current match in S
  size_t i = 0; // pos of current character in W
  std::vector<int> failureTable = MakeKMPTable(W);

  while (m + i < S.size())
  {
    if (W[i] == S[m + i])
    {
      if (i == W.size() - 1)
        return m;

      ++i;
    }
    else
    {
      if (failureTable[i] > -1)
      {
        m = m + i - failureTable[i];
        i = failureTable[i];
      }
      else
      {
        ++m;
        i = 0;
      }
    }
  }

  // searched all of S unsuccessfully
  return S.size();
}

// W = word to be analyzed
// return a filled table 
std::vector<int> MakeKMPTable(std::string& W)
{
  unsigned pos = 2;// current pos we are computing in T
  int cnd = 0;// index in W of the next character of the current canidate substring
  
  std::vector<int> T(W.size());

  if (W.size() == 1)
  {
    T[0] = -1;
    return T;
  }
  if(W.size() == 2)
  {
    T[0] = -1;
    T[1] = 0;
    return T;
  }
  T[0] = -1;
  T[1] = 0;

  while (pos < W.size())
  {
    if (W[pos - 1] == W[cnd]) // substring continues
    {
      T[pos] = cnd + 1;
      ++cnd;
      ++pos;
    }
    else if (cnd > 0)// substring doesn't continue but we can fall back
    {
      cnd = T[cnd];
      T[pos] = 0;
    }
    else// ran out of canidates  (cnd == 0)
    {
      T[pos] = 0;
      ++pos;
    }
  }
  return T;
}


