#include "Basic.h"
#include "LineCount.h"
#include "FileLength.h"


int main(int argc, char* argv[])
{
  //Basic();

  if (argc == 2)
  {
    LineCount(argv[1]);
    LineCount2(argv[1]);
    LineCountSTD(argv[1]);
    FileLength(argv[1]);
  }
  
  return 0;
}