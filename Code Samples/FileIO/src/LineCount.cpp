#include "LineCount.h"

#include <iostream>
#include <fstream>
#include <string>

#include <algorithm> // for std method

static void Print(long count)
{
  std::cout << "Total lines in file: " << count << std::endl;
}


long LineCount(char* txtFile)
{
  std::ifstream inFile(txtFile);
  if (!inFile)
  {
    std::cout << "File " << txtFile << " does not exist";
    return 0;
  }
  char c;
  long count = 1;
  while (inFile.get(c))
  {
    if (c == '\n')
    {
      count++;
    }
  }

  Print(count);

  return count;
}

long LineCount2(char* txtFile)
{
  std::ifstream inFile(txtFile);
  if (!inFile)
  {
    std::cout << "File " << txtFile << " does not exist";
    return 0;
  }

  std::string line;
  long count = 0;

  while (std::getline(inFile, line))
    ++count;

  Print(count);

  return count;
}


long LineCountSTD(char* txtFile)
{
  std::ifstream inFile(txtFile);
  if (!inFile)
  {
    std::cout << "File " << txtFile << " does not exist";
    return 0;
  }
  
  long count = (long)std::count(std::istreambuf_iterator<char>(inFile),
                                std::istreambuf_iterator<char>(), '\n');
  Print(++count);

  return count;
}

