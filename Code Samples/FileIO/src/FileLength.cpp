#include "FileLength.h"

#include <iostream>
#include <fstream>

static void Print(long bytes)
{
  std::cout << "The Size of the File is: " << bytes << " Bytes." << std::endl;
}

long FileLength(char* file)
{
  std::ifstream inFile(file);
  if (!inFile.is_open()) 
  {
    std::cout << "Couldn't open File " << file << std::endl;
    return 0;
  }

  long begin, end;

  begin = (long)inFile.tellg();
  inFile.seekg(0, std::ios::end);
  end = (long)inFile.tellg();

  inFile.close();

  Print(end - begin);

  return end - begin;
}

