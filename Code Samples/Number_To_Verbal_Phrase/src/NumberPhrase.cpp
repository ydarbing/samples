// Brady Reuter
#include "NumberPhrase.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

int StringToInt(char asciiNumber)
{
  return asciiNumber - '0';
}

std::string LastTwoNumbers(const std::string& inNumber)
{
  if (inNumber.length() == 1)
  {
    int ones = StringToInt(inNumber[0]);
    return OnesPlace[ones];
  }
  // length == 2
  int tens = StringToInt(inNumber[0]);
  if (tens == 1)
  {
    tens = StringToInt(inNumber[1]);
    return TensPlaceSpecial[tens];
  }
  else
  {
    int ones = StringToInt(inNumber[1]);

    if (tens == 0 && ones != 0)
      return OnesPlace[ones];
    else if (ones == 0)
      return TensPlace[tens];
    else
      return TensPlace[tens] + " " + OnesPlace[ones];
  }
}

// takes away all commas and any leading 0's
// returns false if the only number is 0 or no numbers in string
bool CleanUpNumberString(std::string& inNumber, std::string& ret)
{
  // remove commas if there are any
  inNumber.erase(std::remove(inNumber.begin(), inNumber.end(), ','), inNumber.end());
  // find the first number
  std::size_t pos = inNumber.find_first_of("0123456789");
  if (pos == std::string::npos)
    return false;
  else if (pos != 0)
  {
    if (inNumber[pos - 1] == '-')
    {
      ret += "negative ";
    }
    inNumber = inNumber.substr(pos);
  }

  return true;
}

std::string NumberToReadableString(std::string inNumber)
{
  std::string ret = "";

  if (CleanUpNumberString(inNumber, ret) == false)
    return OnesPlace[0]; // return "zero"

  size_t length = inNumber.length();
  // index of inNumber we are on
  int numProgress = 0;
  // are we dealing with millions, billions, ect..
  int thousands = static_cast<int>(ceil(length / 3.0)) - 1;
  // before we get into the while loop take care of occurrences 
  // like 10,000 which does not start with a beginning hundreds digit
  int noFirstHundreds = length % 3;

  if (noFirstHundreds != 0)
  {
    ret += LastTwoNumbers(inNumber.substr(0, noFirstHundreds)) + " ";
    if (thousands > 0)
    {
      ret += ThousandsPlace[thousands] + " ";
      --thousands;
    }
    numProgress += noFirstHundreds;
  }

  // go through the rest of the number 
  while (numProgress < length)
  {
    bool addDefaultHundredsLabel = false;
    int hundredNum = StringToInt(inNumber[numProgress]);
    if (hundredNum != 0)
    {
      ret += OnesPlace[hundredNum] + " hundred ";
      addDefaultHundredsLabel = true;
    }
    ++numProgress;

    std::string shouldAdd = LastTwoNumbers(inNumber.substr(numProgress, 2));
    if (shouldAdd != "")
    {
      // this is added purely for 
      if (addDefaultHundredsLabel)
      {
        ret += "and ";
      }
      ret += shouldAdd + " ";
      addDefaultHundredsLabel = true;
    }
    // as long as there was a non-zero number in this group of 3 
    // we want to add the thousands label
    if (addDefaultHundredsLabel && thousands > 0)
    {
      ret += ThousandsPlace[thousands] + " ";
    }

    --thousands;
    numProgress += 2;
  }
  // remove the last space
  ret.pop_back();
  return ret;
}

void Test(const std::string& testNumber)
{
  std::cout << testNumber << ": " << NumberToReadableString(testNumber) << std::endl;
}

void Test(const std::vector<std::string>& testNumbers)
{
  for (unsigned i = 0; i < testNumbers.size(); ++i)
  {
    Test(testNumbers[i]);
  }
}


int main(int argc, char ** argv)
{
  const char* numbers[] = {
    "000000002002",
    "987,654,321",
    //"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
    //"10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
    //"20", "21", "22", "23",
    //"30", "33",
    //"40", "43",
    //"50", "53",
    //"60", "63",
    //"70", "73",
    //"80", "83",
    //"90", "93",
    //"222,222",
    //"6,880,612,905",
    //"100",
    //"1000",
    //"10000",
    //"100000",
    //"1000000",
    //"10000000",
    //"100000000",
    //"1000000000",
    //"10000000000",
    //"100000000000",
    //"1000000000000",
    //"10000000000000",
    //"100000000000000",
    //"1000000000000000",
    //"10000000000000000",
    //"100000000000000000",
    //"1000000000000000000",
    //"10000000000000000000",
    //"100000000000000000000",
    //"1000000000000000000000",
    //"10000000000000000000000",
    //"100000000000000000000000",
    //"1000000000000000000000000",
    //"10000000000000000000000000",
    //"100000000000000000000000000",
    //"1000000000000000000000000000",
    //"10000000000000000000000000000",
    //"100000000000000000000000000000",
    //"1000000000000000000000000000000",
    //"10000000000000000000000000000000",
    //"100000000000000000000000000000000",
    //"1000000000000000000000000000000000",
    //"10000000000000000000000000000000000",
    //"100000000000000000000000000000000000",
    //"1000000000000000000000000000000000000",
    //"10000000000000000000000000000000000000",
    //"100000000000000000000000000000000000000",
    //"1000000000000000000000000000000000000000",
    //"10000000000000000000000000000000000000000",
    //"100000000000000000000000000000000000000000",
    "1000000000000000000000000000000000000000000"
  };

  std::vector<std::string> testStrings(numbers, std::end(numbers));
  Test(testStrings);
  return 0;
}
