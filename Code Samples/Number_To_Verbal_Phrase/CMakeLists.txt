cmake_minimum_required(VERSION 3.17)

project("Number_To_Verbal_Phrase")


set(header_files
   include/NumberPhrase.h
)

set(source_files
   src/NumberPhrase.cpp
)

ADD_EXECUTABLE("${PROJECT_NAME}" 
               ${source_files} 
               ${header_files}
               )

target_include_directories("${PROJECT_NAME}" PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")

# Make the executable a default target to build & run in Visual Studio
set_property(DIRECTORY ${PROJECT_SOURCE_DIR} PROPERTY VS_STARTUP_PROJECT "${PROJECT_NAME}")