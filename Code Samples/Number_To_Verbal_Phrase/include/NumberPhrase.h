// Brady Reuter

#pragma once
#include <string>

std::string GetNumberPhrase(std::string &inNumber);

static const std::string OnesPlace[] = {
  "zero",
  "one",
  "two",
  "three",
  "four",
  "five",
  "six",
  "seven",
  "eight",
  "nine"
};

static const std::string TensPlaceSpecial[] = {
  "ten",
  "eleven",
  "twelve",
  "thirteen",
  "fourteen",
  "fifteen",
  "sixteen",
  "seventeen",
  "eighteen",
  "nineteen"
};

static const std::string TensPlace[] = {
  "", // blank for easy conversion
  "ten",
  "twenty",
  "thirty",
  "forty",
  "fifty",
  "sixty",
  "seventy",
  "eighty",
  "ninety"
};


static const std::string ThousandsPlace[] = {
  "",
  "thousand",
  "million",
  "billion",
  "trillion",
  "quadrillion",
  "quintillion",
  "sextillion",
  "septillion",
  "octillion",
  "nonillion",
  "decillion",
  "undecillion",
  "duodecillion",
  "tredecillion",
  "quattuordecillion",
  "quindecillion",
  "sedecillion",
  "septendecillion",
  "octodecillion",
  "novendecillion",
  "vigintillion",
  "unvigintillion",
  "duovigintillion",
  "tresvigintillion",
  "quattuorvigintillion",
  "quinquavigintillion",
  "sesvigintillion",
  "septemvigintillion",
  "octovigintillion",
  "novemvigintillion"
};

static const int MAX_NUMBER_LENGTH = sizeof(ThousandsPlace) / sizeof(*ThousandsPlace);
