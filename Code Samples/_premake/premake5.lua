local solutionName = "Code Samples"
-- Directory vars
local dirWorking = "../"
local dirObjectFiles = "../_tmp/"
local dirProjectFiles = "../_settings"


--local dxInclude = os.getenv("DXSDK_DIR") .. "/Include/"
--local dxLib = os.getenv("DXSDK_DIR") .. "/Lib/x86/"
--
-- Want to add a project to another project?  
--  Look at Grid project for adding in another project
local mathsLib = "../Maths/bin/Win64/Debug/"
local mathsHeader = "../Maths/include/"
--
-- Want to add Profiler to a project for testing?  
--  Look at Grid project
-- dont forget to add Profiler to dep_debug.txt/dep_release.txt 
local profilerLib = "../Profiler/bin/"
local profilerHeader = "../Profiler/include/"


--local networkingLibRelease = "../Networking/bin/Win64/Release"
local networkingLibDebug = "../Networking/bin/Win64/Debug/"
local networkingHeader = "../Networking/include/"

local glewLib = "../_libs/GL/"

local getOptLib = "../_libs/GetOpt/"

local mbedtlsLib = "../_libs/mbedtls/"

local sodiumLib = "../_libs/sodium/"



-----------------------------------  FUNCTIONS -----------------------------------------------

-- copies all dlls from given 'dir' to the projects executable location in either debug or release
-- using the postbuildcommands
function CopyDLLToExe(cmd, toDebug)
  if toDebug == 1 then
    postbuildcommands { cmd .. "Debug\\\""}
  else
    postbuildcommands { cmd .. "Release\\\""}
  end
  
end

function glewPBC(projectDir, toDebug)
  local dest = "\"$(ProjectDir)bin\\"
  local cmd = "for /R " .. glewLib .. " %%f in (*.dll) do copy \"%%f\" " .. dest

  CopyDLLToExe(cmd, toDebug)
end

function getOptPBC(projectDir, toDebug)
  local dest = "\"$(ProjectDir)bin\\Win32\\"
  local cmd = "for /R " .. getOptLib .. " %%f in (*.dll) do copy \"%%f\" " .. dest

  CopyDLLToExe(cmd, toDebug)
end


function useNetworkingPCH(projectDir) 
  pchheader ( "NetworkPrecompiled.h" )
  --print(projectDir.."src/NetworkPrecompiled.cpp")
  pchsource (projectDir .. "src/NetworkPrecompiled.cpp" )
end

function useProfilerLib(projectDir)
  libdirs {profilerLib}
  --links Profiler
  
  includedirs {profilerHeader}
end

function useMathsLib(projectDir)
  libdirs {mathsLib}
  --links Maths
  
  includedirs {mathsHeader}
end

function useNetworkingLib(projectDir)
  libdirs {networkingLibDebug}
  --links Networking
  
  includedirs {networkingHeader}
end

function useGlewLib(projectDir)
  libdirs {glewLib, glewLib .. "**"}
  --links glew, freeglut
  
  includedirs {glewLib, glewLib .. "**"}
end


function useGetOptLib(projectDir)
  libdirs {getOptLib}
  --links GetOpt
  
  includedirs {getOptLib}
end

function useSodiumLib(projectDir)
  libdirs {sodiumLib}
  --links libsodium
  
  includedirs {sodiumLib}
end

function useMbedtlsLib(projectDir)
  libdirs {mbedtlsLib}
  
  includedirs {mbedtlsLib}
end




local function makeProject(projectPlatform, projectName, projDependencies, debugDependencies, realeaseDependencies, debugLinks, releaseLinks)
  return {platform = projectPlatform, name = projectName, pDep = projDependencies, dDep = debugDependencies, rDep = realeaseDependencies,dLinks = debugLinks, rLinks = releaseLinks}
end


local LinkMaths = {"Maths"}
local LinkGlew = {"glew32"}
local LinkGetOpt = {"getopt"}
local LinkProfiler = {"Profiler"}
local LinkNetworking = {"Networking"}
local LinkAudio = {"winmm", "dsound", "dxguid"}
local LinkSodium  = {"libsodium"}
local LinkMbedtls = {"mbedtls", "mbedcrypto", "mbedx509"}

----------------------------------------------------- START PROJECTS  ----------------------------------------------------------------------
local windowedApps = {
}


local consoleApps = {
-- makeProject takes 
-- 1. Name of project (string)
-- 2. Functions that add dependencies (libDirs, includeDirs, ect) (Table of functions)
-- 3. DEBUG CONFIGURATION SPECIFIC Functions (could add postbuildcommands, ect), function must have 1st argument of an int
-- 4. RELEASE CONFIGURATION SPECIFIC Functions (could add postbuildcommands, ect)
-- 5. Debug Links   (table of strings)
-- 6. Release Links (table of strings)

  makeProject("", "Add Ascii Integers",              {}, {}, {}, {}, {}),
  makeProject("", "Audio",                           {}, {}, {}, {LinkAudio}, {LinkAudio}),
  makeProject("", "Binary Search",                   {}, {}, {}, {}, {}),
  makeProject("", "Bit Pack",                        {}, {}, {}, {}, {}),
  makeProject("", "Calendar",                        {}, {}, {}, {}, {}),
  makeProject("", "Casino",                          {}, {}, {}, {}, {}),
  makeProject("", "Circle Game (Josephus problem)",  {useProfilerLib}, {}, {}, LinkProfiler, LinkProfiler),
  makeProject("", "Closest Pair",                    {}, {}, {}, {}, {}),
  makeProject("", "Data Structure - Graph",          {}, {}, {}, {}, {}),
  makeProject("", "Data Structure - Hash",           {}, {}, {}, {}, {}),
  makeProject("", "Data Structure - List",           {}, {}, {}, {}, {}),
  makeProject("", "Data Structure - Tree",           {useMathsLib}, {useMathsLib}, {}, {LinkMaths}, {LinkMaths}),
  makeProject("", "Endianness",                      {}, {}, {}, {}, {}),
  makeProject("", "FileIO",                          {}, {}, {}, {}, {}),
  makeProject("", "Grid",                            {useProfilerLib}, {}, {}, LinkProfiler, LinkProfiler),
  makeProject("", "K Means",                         {}, {}, {}, {}, {}),
  makeProject("", "Knapsack",                        {}, {}, {}, {}, {}),
  makeProject("", "Kruskal Disjoint Subsets",        {}, {}, {}, {}, {}),
  makeProject("", "Maths Demo",                      {useMathsLib}, {}, {}, LinkMaths, LinkMaths),
  makeProject("", "Memory Manager",                  {}, {}, {}, {}, {}),
  makeProject("", "Networking - Test",               {useNetworkingLib}, {}, {}, {LinkNetworking}, {LinkNetworking}),
  makeProject("", "Number To Verbal Phrase",         {}, {}, {}, {}, {}),
  --makeProject("", "Physics",                         {useMathsLib}, {}, {}, LinkMaths, LinkMaths),
  --makeProject("", "Physics Demo",                    {useMathsLib}, {}, {}, LinkMaths, LinkMaths),
  makeProject("", "RandomWalk",                      {}, {}, {}, {}, {}),
  makeProject("", "Real Number To Phrase",           {}, {}, {}, {}, {}),
  makeProject("", "Reverse Polish Notation",         {}, {}, {}, {}, {}),
  makeProject("", "Small Functions",                 {}, {}, {}, {}, {}),
  makeProject("", "Sorting",                         {}, {}, {}, {}, {}),
  makeProject("", "Strings",                         {}, {}, {}, {}, {}),
  makeProject("Win64", "Sudoku",                          {useGetOptLib}, {getOptPBC}, {getOptPBC}, LinkGetOpt, LinkGetOpt),
  makeProject("", "Test",                            {}, {}, {}, {}, {}),
  makeProject("", "Threads",                         {}, {}, {}, {}, {}),
  makeProject("", "ThreadsBall",                     {}, {}, {}, {}, {}),
  makeProject("", "Verbal Phrase To Number",         {}, {}, {}, {}, {}),
}


local staticLibraries = {
  makeProject("", "Maths",    {}, {}, {}, {}, {}),
  makeProject("", "Networking", {useNetworkingPCH, useMbedtlsLib, useSodiumLib }, {}, {}, {LinkSodium, LinkMbedtls}, {LinkSodium, LinkMbedtls}),
  makeProject("", "Profiler", {}, {}, {}, {}, {}),
}

local dynamicLibraries = {
}
------------------------  END PROJECTS  --------------------------------------------------------------------------



local flagsRelease = {
  --"WinMain", 
  --"FatalWarnings",
  --"ExtraWarnings"
}

local flagsDebug = {
  --"WinMain", 
  --"FatalWarnings",
  --"ExtraWarnings"
}
-- Preprocessor defines
local definesRelease = {
  "NDEBUG",
  "_CRT_SECURE_NO_WARNINGS",
}

local definesDebug = {
  "DEBUG",
  "_CRT_SECURE_NO_WARNINGS",
}





function makeGenericCppProject(toRemoveProjectPlatform, projectName, projDependencies, debugDependencies, realeaseDependencies, debugLinks, realeaseLinks)
  local projectDir = "../" .. projectName .. "/"
  local dirDependencies = projectDir .. "dep/"
  
  language( "C++" )
  removeplatforms { toRemoveProjectPlatform }
  kind( projKind )  
  location( projectDir )    
  targetdir( projOutDir )
  includedirs { projectDir, projectDir .. "include/", dirDependencies .. "**" }
  libdirs { dirDependencies .. "**"}
  debugdir ( dirBinary )
  

  files  { projectDir .. "include/**.h", projectDir .. "include/**.inl", 
           projectDir .. "include/**.hpp", projectDir .. "src/**.cpp", projectDir .. "**.txt"}
  
  vpaths { ["Header Files/*"] = {projectDir .. "include/**.h", projectDir .. "include/**.hpp", projectDir .. "include/**.inl"},
           ["Source Files/*"] = {projectDir .. "src/**.c", projectDir .. "src/**.cpp"},
           ["Docs*"] = projectDir .. "**.txt" }           
  
  --buildoptions { "/wd4201" }
  buildoptions { "/wd4127", "/wd4100", "/wd4481", "/wd4201", "/wd4189" }
  
  
  -- project dependent includes and libdirs
  for i, func in ipairs(projDependencies) do
    func(projectDir)
  end
  
  
  -- Debug Configuration Settings
  configuration "Debug"
    -- ADD for loop for project's debug specific things
    defines { definesDebug }
    flags { flagsDebug }  
	  links {debugLinks}
    rtti "Off"
    symbols "On"
    optimize "Off"
    floatingpoint "Fast"
    -- debug dependent postbuildcommands, ect
    for j, func2 in ipairs(debugDependencies) do
      func2(projectDir, 1)
    end
  
  
  -- Release Configuration Settings
  configuration "Release"
    -- ADD for loop for project's release specific things
    defines { definesRelease }
    flags { flagsRelease } 
	  links {realeaseLinks}
    rtti  "Off"
    symbols "On"
    optimize "On"
    floatingpoint "Fast"
    -- release dependent postbuildcommands, ect
    for k, func3 in ipairs(realeaseDependencies) do
      func3(projectDir, 0);
    end

end


       

      
      
      
if _ACTION ~= nil then
  solution( solutionName )
  
  location( dirWorking )
  
  objdir( dirObjectFiles )
  
  characterset ("MBCS")
  
  startproject "Real Number To Phrase"
  
  configurations { "Debug", "Release" }
  platforms {"Win32", "Win64"}
  
  filter "platforms:Win32"
      architecture "x86"
  
  filter "platforms:Win64"
      architecture "x64"
      
    
  for i = 1,4 do
    if i == 1 then
      local projKind = "WindowedApp"

      for j, proj in ipairs( windowedApps ) do
        project( proj.name )
        kind( projKind ) 
        makeGenericCppProject( proj.platform, proj.name, proj.pDep, proj.dDep, proj.rDep, proj.dLinks, proj.rLinks)
      end

    elseif i == 2 then
      local projKind = "ConsoleApp"

      for j, proj in ipairs( consoleApps ) do
        project( proj.name )
        kind( projKind ) 
        makeGenericCppProject( proj.platform, proj.name, proj.pDep, proj.dDep, proj.rDep, proj.dLinks, proj.rLinks)
      end
      
    elseif i == 3 then
      local projKind = "StaticLib"

      for j, proj in ipairs( staticLibraries ) do
        project( proj.name )
        kind( projKind ) 
        makeGenericCppProject( proj.platform, proj.name, proj.pDep, proj.dDep, proj.rDep, proj.dLinks, proj.rLinks)
      end
      
    elseif i == 4 then
      local projKind = "SharedLib"

      for j, proj in ipairs( dynamicLibraries ) do
        project( proj.name )
        kind( projKind ) 
        makeGenericCppProject( proj.platform, proj.name, proj.pDep, proj.dDep, proj.rDep, proj.dLinks, proj.rLinks)
      end
    end
  end
  
-----
--   MySQL Database Project
-----
  project "MySQL Database"
    language( "C#" )
    platforms { "Win64" }
    kind( "ConsoleApp" )
    location( "../" .. project().name )
    
    local projectDir = "../" .. project().name .. "/"
    
    files  { projectDir .. "src/**.cs" }
    
    vpaths { ["Source Files"] = {projectDir .. "**.cs"},
             ["Docs*"] = projectDir .. "**.txt" }
    
    links  { "System", "System.Configuration", "System.Data", "System.XML" }

    configuration { "Debug" }
      defines { "DEBUG" }
      optimize "Off"
      --Symbols "On"
      
    configuration { "Release" }
      defines { "NDEBUG" }
      optimize "On"
end
