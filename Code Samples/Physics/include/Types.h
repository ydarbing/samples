#pragma once
#include <cstdint>



typedef char      c08;
typedef int8_t    s08;
typedef uint8_t   u08;
typedef int16_t   s16;
typedef uint16_t  u16;
typedef int32_t   s32;
typedef uint32_t  u32;
typedef int64_t   s64;
typedef uint64_t  u64;
typedef float     f32;
typedef double    f64;


typedef float real;

const real FPS30 = real(1.0 / 30.0);
const real FPS60 = real(1.0 / 60.0);



