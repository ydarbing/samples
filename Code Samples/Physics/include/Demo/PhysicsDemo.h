#pragma once
#include <vector>
#include "Camera.h"
#include "Geometry/Segment.h"
#include "Geometry/Geometry.h"

class GLModel;

namespace PhysicsDemo
{
  class Scene
  {
  public:
    Scene(int screenX, int screenY, int windowX, int windowY);
    Scene(int screenX, int screenY);
    ~Scene();


    void Keyboard(unsigned char key, int x, int y);
    void MouseButton(int button, int state, int x, int y);
    void MouseMotion(int x, int y);
    void HandleResize(int w, int h);
	void HandleDisplay(void);
	void HandleIdle(void);
    void PassiveMouseMotion(int x, int y);
    void MouseWheel(int wheel, int dir, int x, int y);
    void SpecialKeys(int key, int x, int y);
    void KeyboardUp(unsigned char key, int x, int y);

    // just get verts and faces of obj file
    bool GetBasicObj(const std::string file, std::vector<Maths::Point>& verts, std::vector<unsigned>& indices);

  private:
    void CastRay(int rayStartX, int rayStartY);
    void MakeWorld();
    void RenderString(int x, int y, const std::string s);
    void InitScene();
    void CameraToOrigin();
    void MakeRoom();

  protected:
    struct ShapeInfo
    {
      ShapeInfo(Maths::Geometry* shape_, const Maths::Color& color_, float thickness_)
        : shape(shape_), color(color_), thickness(thickness_)
      { }
      Maths::Geometry* shape;
      Maths::Color color;
      float thickness;
    };

  private:
    Maths::Camera m_camera;
    Maths::Segment m_viewToPlane;
    std::vector<ShapeInfo> m_shapes;

    std::string m_camPosString;
    std::string m_intersectPosString;
    std::string m_rayStartPosString;

    int m_windowWidth;
    int m_windowHeight;
    int m_screenWidth;
    int m_screenHeight;

    float rotatespeed; // Each object can autorotate
                       // frustum
    float m_near;
    float m_far;
    // mouse buttons
    bool m_leftDown;
    bool m_rightDown;
    // mouse pos
    int mouseX;
    int mouseY;


  };
}// namespace PhysicsDemo

extern PhysicsDemo::Scene* g_Scene;