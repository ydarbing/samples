#pragma once

#include <iostream>

#define SLEEPMILLI(X) std::this_thread::sleep_for(std::chrono::milliseconds(X))

#define SCAST(want, have) static_cast<want>(have)

#define RECAST(want, have) reinterpret_cast<want>(have)


/////////////////
// HELPFUL FOR VS TODO OUTPUT
#define STRINGIZE_(X) #X
#define STRINGIZE(X) STRINGIZE_(X)
#define TODO(X) \
  __pragma(message(__FILE__ "(" STRINGIZE(__LINE__) "): TODO_" X))

#define DEV_NOTE(X) \
  __pragma(message(__FILE__ "(" STRINGIZE(__LINE__) "): NOTE_" X))


// assertion includes
#include <cassert>
// assertions
#define _USE_CONTROL_ADV_ASSERT_
#ifdef _USE_CONTROL_ADV_ASSERT_
#ifdef _DEBUG
#define assertion(x) \
{ \
  if(!(x)) \
  { \
    std::cout << "Assert " << __FILE__ << ":" << __LINE__ << "(" << #x << ")\n"; \
    __debugbreak(); \
  } \
}
#else
#define assertion(x) {}
#endif//_DEBUG
#else
#define assertion(x) assert(x)
#endif //_USE_CONTROL_ADV_ASSERT_
