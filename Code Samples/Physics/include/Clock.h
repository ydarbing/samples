#pragma once

#include "Types.h"
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>


class Clock
{
  Clock();
  ~Clock();

  void Start(void);
  void Stop(void);
  
  real Elapsed(void);

  real Difference(void);
  
  s64 Current(void);

private:
  LARGE_INTEGER m_frequency;
  LARGE_INTEGER m_start;
  LARGE_INTEGER m_stop;
};
