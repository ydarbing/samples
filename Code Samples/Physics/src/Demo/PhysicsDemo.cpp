#include "Demo/PhysicsDemo.h"

#include "Geometry/Polyhedron.h"
#include "Geometry/Plane.h"
#include "Geometry/Segment.h"
#include "Geometry/AABB.h"
#include "Geometry/OOB.h"
#include "Geometry/Sphere.h"

#include "Utils.h"
#include "Vector.h"

#include "Draw.h"


#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "Geometry/Line.h"
#include "Distance/DistancePointToSegment.h"
#include "Distance/DistancePointToOOB.h"
#include "Intersection/IntersectionRayToPlane.h"
#include "Geometry/Ray.h"

#include "Geometry/Circle.h"

#define ESC_KEY 27
#define DTOR    0.0174532925

PhysicsDemo::Scene* g_Scene = nullptr;

namespace PhysicsDemo
{
	void ZoomOutCamera(Maths::Camera& cam)
	{
		double mult = 1.02;
		cam.SetZoom(cam.GetZoom() * mult);
	}

	void ZoomInCamera(Maths::Camera& cam)
	{
		if (cam.GetZoom() < 0.004)
			return;// max zoom
		double mult = 0.98;
		cam.SetZoom(cam.GetZoom() * mult);
	}

	Scene::Scene(int screenX, int screenY, int windowX, int windowY)
		: m_screenWidth(screenX), m_screenHeight(screenY), m_windowWidth(windowX), m_windowHeight(windowY),
		rotatespeed(0.5f), m_leftDown(false), m_rightDown(false), mouseX(0), mouseY(0),
		m_near(0.1f), m_far(10000.0f)
	{
		InitScene();
	}
	Scene::Scene(int screenX, int screenY)
		: m_screenWidth(screenX), m_screenHeight(screenY), m_windowWidth(400), m_windowHeight(300),
		rotatespeed(0.5f), m_leftDown(false), m_rightDown(false), mouseX(0), mouseY(0),
		m_near(0.1f), m_far(10000.0f)
	{
		InitScene();
	}

	Scene::~Scene()
	{
		for (unsigned i = 0; i < m_shapes.size(); ++i)
			delete m_shapes[i].shape;
	}

	void Scene::InitScene()
	{
		using namespace Maths;
		Plane* plane = new Plane(0, 1, 0, 0);
		Segment* s1 = new Segment(Point(-2.0f, 0.0f, -5.0f), Point(2.0f, 0.0f, -5.0f));
		Segment* s2 = new Segment(Point(-2.0f, 1.0f, -5.0f), Point(2.0f, -1.0f, -5.0f));
		Line* l1 = new Line(Point::ORIGIN, Vector::UNIT_Y);
		AABB* aab = new AABB(Point(2.0f, 0.0f, 2.0f), Point(5.0f, 3.0f, 5.0f));
		Sphere* sphere = new Sphere(Point(-2.0f, 1.0f, 2.0f), 1.0f);

		Vector axes[3] = { Vector(1.0f, 0, 0), Vector::BuildRandomUnitYZ(), Vector::BuildRandomUnitYZ() };
		float ext[3] = { 2.0f, 2.0f, 2.0f };
		OOB* oob = new OOB(Point(3.5f, 1.0f, -3.5f), axes, ext);

		m_shapes.push_back(ShapeInfo(plane, Color(0.9f, 0.1f, 0.1f, 0.1f), 1.0f));
		m_shapes.push_back(ShapeInfo(s1, Color::BuildRandomBlue(), 1.0f));
		m_shapes.push_back(ShapeInfo(s2, Color::BuildRandomBlue(), 1.0f));
		m_shapes.push_back(ShapeInfo(l1, Color::BuildRandomGreen(), 1.0f));
		m_shapes.push_back(ShapeInfo(aab, Color::BuildRandomBlue(), 1.0f));
		m_shapes.push_back(ShapeInfo(oob, Color::BuildRandomBlue(), 1.0f));

		//std::vector<Point> verts;
		//std::vector<unsigned> indices;
		//unsigned numTris = 0;
		//if (GetBasicObj("Objects/dodecahedron.obj", verts, indices))
		//{
		//  numTris = indices.size() / 3;
		//  Polyhedron* p1 = new Polyhedron(verts, numTris, indices);
		//  m_shapes.push_back(ShapeInfo(p1, Color::BuildRandomGreen(), 1.0f));
		//}

		m_shapes.push_back(ShapeInfo(sphere, Color::BuildRandomGreen(), 1.0f));

		//MakeRoom();
		CameraToOrigin();
	}

	void Scene::CameraToOrigin()
	{
		m_camera.SetAspect((float)m_windowWidth, (float)m_windowHeight);//(float)m_screenWidth, (float)m_screenHeight);
		m_camera.SetNear(m_near);
		m_camera.SetFar(m_far);
		m_camera.SetAperture(60.0f);
		m_camera.SetFOV(45.0f);
		m_camera.SetFocusPos(Maths::Point());
		m_camera.SetUpDir(Maths::Vector(0, 1.0f, 0));
		m_camera.SetPos(Maths::Point(-10.0f, 5.0f, 0));
		m_camera.SetFocalLength(10.0f);
		m_camera.SetEyeSeparation(m_camera.GetFocalLength() / 30.0f);
		m_camera.SetZoom(1.0);
		m_camera.SetLookDir(m_camera.GetFocusPos() - m_camera.GetPos());
	}

	void Scene::MakeRoom()
	{
		using namespace Maths;
		std::vector<Point> verts;
		std::vector<unsigned> indices;
		indices.push_back(0);
		indices.push_back(1);
		indices.push_back(3);

		indices.push_back(1);
		indices.push_back(2);
		indices.push_back(3);

		indices.push_back(0);
		indices.push_back(3);
		indices.push_back(4);

		indices.push_back(0);
		indices.push_back(4);
		indices.push_back(5);

		verts.push_back(Point(0, 0, 0));
		verts.push_back(Point(13.0833f, 0, 0));
		verts.push_back(Point(13.0833f, 0, 3.0833f));
		verts.push_back(Point(12.1666f, 0, 3.0833f));
		verts.push_back(Point(7.09295f, 0, 11.2134f));
		verts.push_back(Point(0, 0, 7.0f));
		Polyhedron* room = new Polyhedron(verts, 4, indices);
		float sa = room->ComputeSurfaceArea();
		m_shapes.push_back(ShapeInfo(room, Color::BuildRandomBlue(), 1.0f));

		verts.clear();
		verts.push_back(Point(0, 0, 0));
		verts.push_back(Point(13.0833f, 0, 0));
		verts.push_back(Point(13.0833f, 0, 3.0833f));
		verts.push_back(Point(12.1666f, 0, 3.0833f));
		indices.clear();
		indices.push_back(0);
		indices.push_back(1);
		indices.push_back(2);

		indices.push_back(0);
		indices.push_back(2);
		indices.push_back(3);
		Polyhedron* closet = new Polyhedron(verts, 4, indices);
		float csa = closet->ComputeSurfaceArea();
		m_shapes.push_back(ShapeInfo(closet, Color::BuildRandomBlue(), 1.0f));
		float closetWallStart = (84.0f / 12.0f);

		float unknownX = 87.6810379f / 12.0f;// these were found using circle circle intersection based on wall points
		float unknownZ = 136.115585f / 12.0f;// these were found using circle circle intersection based on wall points

		float closetWallLength = 102.0f / 12.0f;
		float closestStartOnWallLength = 17.0f / 12.0f;
		float closetEndOnWallLength = 91.0f / 12.0f;
		float closetDoorDepth = 3.0f / 12.0f;
		float closetInsideLeftWallLength = 14.0f / 12.0f;
		float closetInsideRightWallLength = 11.0f / 12.0f;
		float closetLeftWallLength = (28.0f / 12.0f);
		float closetRightWallLength = (25.0f / 12.0f);
		Segment* closetWall = new Segment(Point(0, 0, closetWallStart), Point(unknownX, 0, unknownZ));

		float wallLength = closetWall->extent * 2.0f;
		Vector wallVector = closetWall->direction;
		wallVector.Normalize();
		Point wallEnd = closetWall->point0 + wallVector * closetWallLength;
		Point closetBegin = closetWall->point0 + wallVector * closestStartOnWallLength;
		Point closetEnd = closetWall->point0 + wallVector * closetEndOnWallLength;

		Segment* closetSeg = new Segment(closetBegin, closetEnd);

		float closetDoor = closetSeg->extent * 2.0f;

		Vector closetVector = closetEnd - closetBegin;
		closetVector.Normalize();
		Vector closetNormal(-closetVector.z, 0, closetVector.x);

		Point closetInsideBegin = closetBegin + closetNormal * closetDoorDepth;
		Point closetInsideEnd = closetEnd + closetNormal * closetDoorDepth;


		Segment* closetLeft = new Segment(closetBegin, closetInsideBegin);
		Segment* closetRight = new Segment(closetEnd, closetInsideEnd);


		m_shapes.push_back(ShapeInfo(closetLeft, Color::BuildRandomBlue(), 1.0f));
		m_shapes.push_back(ShapeInfo(closetRight, Color::BuildRandomBlue(), 1.0f));

		Vector closetInsideVector = closetEnd - closetInsideEnd;
		Vector closetInsideNormal(-closetInsideVector.z, 0, closetInsideVector.x);
		closetInsideNormal.Normalize();

		Segment* closetInsideWallLeft = new Segment(closetInsideBegin, closetInsideBegin + closetInsideNormal * -closetInsideLeftWallLength);
		Segment* closetInsideWallRight = new Segment(closetInsideEnd, closetInsideEnd + closetInsideNormal * closetInsideRightWallLength);

		Segment* closetInside = new Segment(closetInsideWallLeft->point1, closetInsideWallRight->point1);

		float closetInsideLength = closetInside->extent * 2.0f;


		m_shapes.push_back(ShapeInfo(closetInsideWallLeft, Color::BuildRandomBlue(), 1.0f));
		m_shapes.push_back(ShapeInfo(closetInsideWallRight, Color::BuildRandomBlue(), 1.0f));


		// rotate the left and right closetInsideNormal by degrees
		float radLeft = -62.797f * DEG_TO_RAD; //-63.199f * DEG_TO_RAD;
		float newX = closetInsideNormal.x * cos(radLeft) + closetInsideNormal.z * sin(radLeft);
		float newZ = -closetInsideNormal.x * sin(radLeft) + closetInsideNormal.z * cos(radLeft);
		Vector closetLeftAngleVector(newX, 0, newZ);
		// rotate the left and right closetInsideNormal by degrees
		float radRight = (91.433f - 180.0f) * DEG_TO_RAD; //(84.951f - 180.0f) * DEG_TO_RAD;
		newX = closetInsideNormal.x * cos(radRight) + closetInsideNormal.z * sin(radRight);
		newZ = -closetInsideNormal.x * sin(radRight) + closetInsideNormal.z * cos(radRight);
		Vector closetRightAngleVector(newX, 0, newZ);

		closetLeftAngleVector.Normalize();
		closetRightAngleVector.Normalize();


		Segment* closetLeftWall = new Segment(closetInsideWallLeft->point1, closetInsideWallLeft->point1 + closetLeftAngleVector * closetLeftWallLength);
		Segment* closetRightWall = new Segment(closetInsideWallRight->point1, closetInsideWallRight->point1 + closetRightAngleVector * closetRightWallLength);

		m_shapes.push_back(ShapeInfo(closetLeftWall, Color::BuildRandomGreen(), 1.0f));
		m_shapes.push_back(ShapeInfo(closetRightWall, Color::BuildRandomRed(), 1.0f));


		Segment* closetBackWall = new Segment(closetLeftWall->point1, closetRightWall->point1);

		float closetLeftLength = closetLeftWall->extent * 2.0f;
		float closetRightLength = closetRightWall->extent * 2.0f;
		float closetBackLength = closetBackWall->extent * 2.0f;
		m_shapes.push_back(ShapeInfo(closetBackWall, Color::BuildRandomBlue(), 1.0f));
	}


	void Scene::Keyboard(unsigned char key, int x, int y)
	{
		switch (key)
		{
		case ESC_KEY:
			exit(0);
			break;
		case 'w': // Translate camera up 
		case 'W':
		case '8':
			m_camera.Translate(0, 1);
			break;
		case 'z': // unzoom camera
		case 'Z':
			ZoomOutCamera(m_camera);
			break;
		case 'x': // Zoom camera
		case 'X':
			ZoomInCamera(m_camera);
			break;
		case 's': // Translate camera down 
		case 'S':
		case '2':
			m_camera.Translate(0, -1);
			break;
		case 'a': // Translate camera left 
		case 'A':
		case '4':
			m_camera.Translate(-1, 0);
			break;
		case 'd': // Translate camera right
		case 'D':
		case '6':
			m_camera.Translate(1, 0);
			break;
		case '3':
			m_camera.Translate(1, -1);
			break;
		case '7':
			m_camera.Translate(-1, 1);
			break;
		case '9':
			m_camera.Translate(1, 1);
			break;
		case '1':
			m_camera.Translate(-1, -1);
			break;
		}
		glutPostRedisplay();
	}

	void Scene::RenderString(int x, int y, const std::string s)
	{
		glColor3f(0.5f, 0.5f, 0.9f);
		glRasterPos2i(x, y);
		for (unsigned i = 0; i < s.size(); ++i)
			glutBitmapCharacter(GLUT_BITMAP_9_BY_15, s[i]);
	}

	void Scene::MouseButton(int button, int state, int x, int y)
	{
		mouseX = x;
		mouseY = y;

		if (button == GLUT_LEFT_BUTTON)
		{
			m_leftDown = (state == GLUT_DOWN);
			if (m_leftDown)
				CastRay(mouseX, mouseY);

		}
		else if (button == GLUT_RIGHT_BUTTON)
			m_rightDown = (state == GLUT_DOWN);
		else if (button == 3 && state == GLUT_DOWN) // scroll down
			ZoomInCamera(m_camera);
		else if (button == 4 && state == GLUT_DOWN) // scroll up
			ZoomOutCamera(m_camera);


		glutPostRedisplay();
	}

	void Scene::MouseMotion(int x, int y)
	{
		int dx = x - mouseX;
		int dy = y - mouseY;

		if (m_leftDown)
			m_camera.Rotate(dx / 2.0f, dy / 2.0f, 0);

		if (m_rightDown)
			m_camera.Rotate(0, 0, dx / 2.0f);


		mouseX = x;
		mouseY = y;

		glutPostRedisplay();
	}

	void Scene::HandleResize(int w, int h)
	{
		//Tell OpenGL how to convert from coordinates to pixel values
		glViewport(0, 0, w, h);
		m_screenWidth = w;
		m_screenHeight = h;

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity(); //Reset camera
		float aspectRatio = (float)w / (float)h;
		gluPerspective(m_camera.GetAperture() * m_camera.GetZoom(),
			aspectRatio, m_near, m_far);

		m_camera.SetAspect(aspectRatio);
	}

	void Scene::HandleIdle(void)
	{
		HandleDisplay();
	}

	void Scene::HandleDisplay(void)
	{
		using namespace Maths;
		std::stringstream ss;
		ss << "Cam Pos: ";
		ss << m_camera.GetPos().x << ", " << m_camera.GetPos().y << ", " << m_camera.GetPos().z;
		m_camPosString = ss.str();

		Point focus;
		//find focal point
		m_camera.GetLookDir().Normalize();
		focus = m_camera.GetPos() + (m_camera.GetLookDir() * m_camera.GetFocalLength());
		Vector right = m_camera.GetLookDir().Cross(m_camera.GetUpDir());
		right.Normalize();
		//right *= m_camera.GetEyeSeperation() / 2.0f;

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		if (m_camPosString.empty() == false)
			RenderString(1, 1, m_camPosString);
		if (m_intersectPosString.empty() == false)
			RenderString(1, 2, m_intersectPosString);
		if (m_rayStartPosString.empty() == false)
			RenderString(1, 3, m_rayStartPosString);

		// set projection
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(m_camera.GetAperture() * m_camera.GetZoom(),
			m_screenWidth / (double)m_screenHeight,
			m_near, m_far);
		//glViewport(0, 0, m_screenWidth, m_screenHeight);

		// create model
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		Point lookAt = m_camera.GetPos() - right;
		Vector up = m_camera.GetUpDir();
		gluLookAt(lookAt.x, lookAt.y, lookAt.z,
			focus.x, focus.y, focus.z,
			up.x, up.y, up.z);

		MakeWorld();


		glutSwapBuffers();
	}


	void Scene::MakeWorld()
	{
		//static float rotateangle = 0.0f;
		glPushMatrix();
		//glRotatef(rotateangle, 0.0, 1.0, 0.0);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		using namespace Maths;


		for (unsigned i = 0; i < m_shapes.size(); ++i)
		{
			glLineWidth(m_shapes[i].thickness);
			m_shapes[i].shape->Draw(&m_shapes[i].color);
		}
		glLineWidth(1.0f);


		//Point out;
		//Point  line1(0.0f, 1.0f, 0.0f);
		//Point  line2(0.0f, 0.0f, 0.0f);
		//Vector hint(0.0f, 0.0f, 0.0f);
		//float len1 = 2.0f;
		//float len2 = 1.0f;
		//
		//if (intersect_line_segments(line1, len1, line2, len2, hint, &out))
		//{
		//  Circle c1(line1, Vector::UNIT_Z, len1);
		//  Circle c2(line2, Vector::UNIT_Z, len2);
		//  c1.Draw(&Color::BROWN);
		//  c2.Draw(&Color::GRAY);
		//  Segment s1(line1, out);
		//  Segment s2(line2, out);
		//  s1.Draw(&Color::YELLOW);
		//  s2.Draw(&Color::BLUE);
		//  line1.Draw(&Color::ORANGE);
		//  line2.Draw(&Color::ORANGE);
		//  out.Draw(&Color::GREEN);
		//}
		//Sphere sphere1(line1, len1);
		//Sphere sphere2(line2, len2);
		//sphere1.Draw(&Color::BROWN);
		//sphere2.Draw(&Color::GRAY);


		Maths::DrawPoint(m_camera.GetFocusPos(), &Maths::Color::RED);

		Maths::DistancePointToSegment(m_camera.GetFocusPos(), *static_cast<Maths::Segment*>(m_shapes[1].shape))
			.Draw(true, &Maths::Color::BuildRandomColor(), &Maths::Color::BuildRandomColor(), &Maths::Color::BuildRandomColor());
		glPopMatrix();

		Maths::DistancePointToOOB(m_camera.GetFocusPos(), *static_cast<Maths::OOB*>(m_shapes[5].shape))
			.Draw(true, &Maths::Color::BuildRandomColor(), &Maths::Color::BuildRandomColor(), &Maths::Color::BuildRandomColor());

		m_viewToPlane.Draw(&Maths::Color::BuildRandomBlue());
		//rotateangle += rotatespeed;
	}

	bool Scene::GetBasicObj(const std::string file, std::vector<Maths::Point>& verts, std::vector<unsigned>& indices)
	{
		std::ifstream obj(file);
		std::string line;
		if (obj.is_open())
		{
			verts.clear();
			indices.clear();

			while (getline(obj, line))
			{
				std::istringstream lineSS(line);
				std::string lineType;
				lineSS >> lineType;

				if (lineType == "v")
				{
					float x = 0, y = 0, z = 0, w = 1.0f;
					lineSS >> x >> y >> z >> w;
					verts.push_back(Maths::Point(x, y, z, w));
				}
				else if (lineType == "f")
				{
					// only assumes there are vertices associated with faces
					unsigned i1 = 0, i2 = 0, i3 = 0;
					lineSS >> i1 >> i2 >> i3;
					indices.push_back(i1); indices.push_back(i2);
					indices.push_back(i1); indices.push_back(i3);
					indices.push_back(i2); indices.push_back(i3);
				}
			}
			return true;
		}
		return false;
	}

	void Scene::CastRay(int rayStartX, int rayStartY)
	{
		m_camera.Update(0);

		Maths::Point rayStartWorld = m_camera.WorldFromScreen(m_windowWidth, m_windowHeight, rayStartX, rayStartY);
		Maths::Vector dir = rayStartWorld - m_camera.GetPos();
		dir.Normalize();
		Maths::IntersectionRayToPlane iRayPlane(Maths::Ray(rayStartWorld, dir), *static_cast<Maths::Plane*>(m_shapes[0].shape));

		iRayPlane.Draw(true, &Maths::Color::BuildRandomColor());
		Maths::Ray r = iRayPlane.GetRay();
		Maths::Point p = r.GetPosition();
		m_viewToPlane = Maths::Segment(p, iRayPlane.GetIntersectionPoint());


		std::stringstream ss;
		ss << "Ray pos: ";
		ss << p.x << ", " << p.y << ", " << p.z;
		m_rayStartPosString = ss.str();

		std::stringstream ss1;
		ss1 << "Int pos: ";
		ss1 << m_viewToPlane.point1.x << ", " << m_viewToPlane.point1.y << ", " << m_viewToPlane.point1.z;
		m_intersectPosString = ss1.str();

		//m_shapes.back().shape->SetPosition(iRayPlane.GetIntersectionPoint());
	}

	void Scene::PassiveMouseMotion(int x, int y)
	{
		throw std::logic_error("The method or operation is not implemented.");
	}

	void Scene::MouseWheel(int wheel, int dir, int x, int y)
	{
		throw std::logic_error("The method or operation is not implemented.");
	}

	void Scene::SpecialKeys(int key, int x, int y)
	{
		throw std::logic_error("The method or operation is not implemented.");
	}

	void Scene::KeyboardUp(unsigned char key, int x, int y)
	{
		throw std::logic_error("The method or operation is not implemented.");
	}


}// namespace Demo
