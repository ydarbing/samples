#define GLEW_STATIC
#include "freeglut.h"

#include <cstring>
#include <vector>

#include "Maths.h"

#define ESC_KEY 27

struct RopeVert
{
  Vec2 velocity;
  Vec2 position;
  Vec2 oldPosition;
  f32 invMass;
};

std::vector<RopeVert> g_verts;
const f32 kMaxDist = f32(2.0);
const f32 kRadius = f32(5.0);
const u32 kRopeVerts = 20;
bool circle = false;
f32 xf;
f32 yf;

void BuildRope(void);

void Keyboard(unsigned char key, s32 x, s32 y)
{
  switch (key)
  {
  case ESC_KEY:
    exit(0);
    break;
  case 'r':
    BuildRope();
    break;
  }
}

void RenderString(s32 x, s32 y, const char *s)
{
  glColor3f(0.5f, 0.5f, 0.9f);
  glRasterPos2i(x, y);
  u32 l = (u32)std::strlen(s);
  for (u32 i = 0; i < l; ++i)
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *(s + i));
}

void Mouse(s32 button, s32 state, s32 x, s32 y)
{
  xf = f32(x) * f32(1.0f / 10.0f);
  yf = f32(y) * f32(1.0f / 10.0f);

  if (state == GLUT_DOWN)
  {
    switch (button)
    {
    case GLUT_LEFT_BUTTON:
    {
      circle = true;
    } break;
    case GLUT_RIGHT_BUTTON:
    {
    } break;
    }
  }

  else if (state == GLUT_UP)
  {
    switch (button)
    {
    case GLUT_LEFT_BUTTON:
    {
      circle = false;
    } break;
    case GLUT_RIGHT_BUTTON:
    {
    } break;
    }
  }
}

void MouseMotion(s32 x, s32 y)
{
  xf = f32(x);
  yf = f32(y);

  xf -= f32(400);
  yf -= f32(300);
  xf /= f32(800);
  yf /= f32(600);
  xf *= f32(80);
  yf *= f32(60);
  xf += f32(40);
  yf += f32(30);
}

void Solve(f32 dt)
{
  for (u32 i = 0; i < g_verts.size() - 1; ++i)
  {
    RopeVert& a = g_verts[i];
    RopeVert& b = g_verts[i + 1];

    Vec2 v = b.position - a.position;
    f32 length = v.Len();

    if (length != f32(0.0))
    {
      f32 error = kMaxDist / length - f32(1.0);
      Vec2 correction = v * error;

      if (i != 0)
      {
        f32 invMass = a.invMass + b.invMass;
        a.position -= correction * (a.invMass / invMass);
        b.position += correction * (b.invMass / invMass);
      }
      else
      {
        b.position += correction;
      }
    }
  }
}

void Integrate(f32 dt)
{
  for (u32 i = 1; i < g_verts.size(); ++i)
  {
    RopeVert& a = g_verts[i];

    a.velocity += Vec2(0.0, 9.8) * dt;
    a.position += a.velocity * dt;
  }
}

void VelocityFixup(f32 inv_dt)
{
  for (u32 i = 1; i < g_verts.size(); ++i)
  {
    RopeVert& a = g_verts[i];

    a.velocity = (a.position - a.oldPosition) * inv_dt;
    a.oldPosition = a.position;
  }
}

void Collide(void)
{
  if (circle)
  {
    Vec2 mouse(xf, yf);

    for (u32 i = 1; i < g_verts.size(); ++i)
    {
      RopeVert& a = g_verts[i];

      f32 d = DistSqr(a.position, mouse);

      if (d < kRadius * kRadius)
      {
        Vec2 dir = a.position - mouse;

        dir.Normalize();
        dir *= kRadius - std::sqrt(d);
        a.position += dir;
      }
    }
  }
}

void SolveRope(f32 dt)
{
  Integrate(dt);

  Collide();

  for (u32 i = 0; i < 8; ++i)
    Solve(dt);

  VelocityFixup(f32(1.0) / dt);
}

void BuildRope(void)
{
  g_verts.clear();

  f32 x = f32(40.0);
  f32 y = f32(10.0);

  for (u32 i = 0; i < kRopeVerts; ++i)
  {
    RopeVert v;

    v.position.Set(x + f32(i) * kMaxDist, y);
    v.oldPosition = v.position;
    v.velocity.Set(0.0, 0.0);
    v.invMass = f32(1.0) / f32(kRopeVerts * 3 - i * 2);

    g_verts.push_back(v);
  }
}

void RenderRope(void)
{
  glBegin(GL_LINES);

  glColor3f(1.0f, 1.0f, 1.0f);
  for (u32 i = 0; i < g_verts.size() - 1; ++i)
  {
    RopeVert& a = g_verts[i];
    RopeVert& b = g_verts[i + 1];

    glVertex2f((float)a.position.x, (float)a.position.y);
    glVertex2f((float)b.position.x, (float)b.position.y);
  }

  glEnd();
}

void MainLoop(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  srand(1);

  RenderString(1, 2, "Press and hold left click to use a circle");
  RenderString(1, 4, "Press r to reset");

  SolveRope((f32(1.0) / f32(60.0)));
  RenderRope();

  if (circle)
  {
    const u32 kSegments = 20;

    // Render a circle with a bunch of lines
    glBegin(GL_LINE_LOOP);
    Vec2 pos(xf, yf);
    f32 theta = 0;
    f32 inc = PI * f32(2.0) / (f32)kSegments;
    for (u32 i = 0; i < kSegments; ++i)
    {
      theta += inc;
      Vec2 p(std::cos(theta), std::sin(theta));
      p *= kRadius;
      p += pos;
      glVertex2f((float)p.x, (float)p.y);
    }
    glEnd();
  }

  glutSwapBuffers();
}

int main(s32 argc, char **argv)
{
  const s32 WindowWidth = 800;
  const s32 WindowHeight = 600;
  glutInit(&argc, argv);
  s32 screenWidth = glutGet(GLUT_SCREEN_WIDTH);
  s32 screenHeight = glutGet(GLUT_SCREEN_HEIGHT);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
  glutInitWindowSize(WindowWidth, WindowHeight);
  glutInitWindowPosition((screenWidth - WindowWidth) / 2, (screenHeight - WindowHeight) / 2);
  glutCreateWindow("2D Demo \"Ropes\"");
  //glewInit();

  glutDisplayFunc(MainLoop);
  glutKeyboardFunc(Keyboard);
  glutIdleFunc(MainLoop);
  glutMouseFunc(Mouse);
  glutMotionFunc(MouseMotion);

  glMatrixMode(GL_PROJECTION);
  gluOrtho2D(0, 80, 60, 0);

  glutShowWindow();

  BuildRope();

  glutMainLoop();
}
