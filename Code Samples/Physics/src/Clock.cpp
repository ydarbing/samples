#include "Clock.h"

Clock::Clock()
{
  SetThreadAffinityMask(GetCurrentThread(), 1);
  QueryPerformanceFrequency(&m_frequency);

  Start();
  Stop();
}

Clock::~Clock()
{
}

void Clock::Start(void)
{
  QueryPerformanceCounter(&m_start);
}

void Clock::Stop(void)
{
  QueryPerformanceCounter(&m_stop);
}

real Clock::Elapsed(void)
{
  LARGE_INTEGER current;

  QueryPerformanceCounter(&current);

  return real(current.QuadPart - m_start.QuadPart) / real(m_frequency.QuadPart);
}

real Clock::Difference(void)
{
  return real(m_stop.QuadPart - m_start.QuadPart) / real(m_frequency.QuadPart);
}

s64 Clock::Current(void)
{
  LARGE_INTEGER current;

  QueryPerformanceCounter(&current);

  return current.QuadPart;
}
