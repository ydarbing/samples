#pragma once


#include "Matrix3.h"
#include "Matrix4.h"
#include "Quaternion.h"
#include "Geometry\Point.h"

#include <Magnum/Math/RectangularMatrix.h>
#include <Magnum/Math/Quaternion.h>


namespace Magnum
{
  namespace Math
  {
    namespace Implementation
    {
      template<> struct VectorConverter<3, float, Maths::Point>
      {
        static Vector<3, Float> from(const Maths::Point& other)
        {
          return { other.x, other.y, other.z };
        }

        static Maths::Point to(const Vector<3, Float>& other)
        {
          return { other[0], other[1], other[2]};
        }
      };
      template<> struct VectorConverter<4, float, Maths::Point>
      {
        static Vector<4, Float> from(const Maths::Point& other)
        {
          return { other.x, other.y, other.z, other.w };
        }

        static Maths::Point to(const Vector<4, Float>& other)
        {
          return { other[0], other[1], other[2], other[3] };
        }
      };

      template<> struct VectorConverter<3, float, Maths::Vector>
      {
        static Vector<3, Float> from(const Maths::Vector& other)
        {
          return { other.x, other.y, other.z };
        }

        static Maths::Vector to(const Vector<3, Float>& other)
        {
          return { other[0], other[1], other[2] };
        }
      };

      template<> struct VectorConverter<4, float, Maths::Vector>
      {
        static Vector<4, Float> from(const Maths::Vector& other)
        {
          return { other.x, other.y, other.z, other.w };
        }

        static Maths::Vector to(const Vector<4, Float>& other)
        {
          return { other[0], other[1], other[2], other[3] };
        }
      };

      template<> struct RectangularMatrixConverter<3, 3, float, Maths::Matrix3>
      {
        static RectangularMatrix<3, 3, Float> from(const Maths::Matrix3& other)
        {
          return { Vector<3, Float>(other.GetColumn(0)),
                  Vector<3, Float>(other.GetColumn(1)),
                  Vector<3, Float>(other.GetColumn(2)) };
        }

        static Maths::Matrix3 to(const RectangularMatrix<3, 3, Float>& other)
        {
          return { other[0][0], other[1][0], other[2][0],
                  other[0][1], other[1][1], other[2][1],
                  other[0][2], other[1][2], other[2][2] };
        }
      };

      template<> struct RectangularMatrixConverter<4, 4, float, Maths::Matrix4>
      {
        static RectangularMatrix<4, 4, Float> from(const Maths::Matrix4& other)
        {
          return { Vector<4, Float>(other.GetCol(0)),
                   Vector<4, Float>(other.GetCol(1)),
                   Vector<4, Float>(other.GetCol(2)),
                   Vector<4, Float>(other.GetCol(3)) };
        }

        static Maths::Matrix4 to(const RectangularMatrix<4, 4, Float>& other)
        {
          return { other[0][0], other[1][0], other[2][0], other[3][0],
                   other[0][1], other[1][1], other[2][1], other[3][1],
                   other[0][2], other[1][2], other[2][2], other[3][2],
                   other[0][3], other[1][3], other[2][3], other[3][3] };
        }
      };

      template<> struct QuaternionConverter<float, Maths::Quaternion>
      {
        static Quaternion<Float> from(const Maths::Quaternion& other)
        {
          return { {other.x, other.y, other.z}, other.w };
        }

        static Maths::Quaternion to(const Quaternion<Float>& other)
        {
          return { other.vector().x(), other.vector().y(), other.vector().z(), other.scalar() };
        }
      };
    }
  }
}
