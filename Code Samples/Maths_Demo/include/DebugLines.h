#pragma once

/*
    This file is part of Magnum.

    Original authors � credit is appreciated but not required:

        2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019 �
            Vladim�r Vondru� <mosra@centrum.cz>
        2016 � Bill Robinson <airbaggins@gmail.com>

    This is free and unencumbered software released into the public domain.

    Anyone is free to copy, modify, publish, use, compile, sell, or distribute
    this software, either in source code form or as a compiled binary, for any
    purpose, commercial or non-commercial, and by any means.

    In jurisdictions that recognize copyright laws, the author or authors of
    this software dedicate any and all copyright interest in the software to
    the public domain. We make this dedication for the benefit of the public
    at large and to the detriment of our heirs and successors. We intend this
    dedication to be an overt act of relinquishment in perpetuity of all
    present and future rights to this software under copyright law.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
    IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
    CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <vector>
#include <Magnum/GL/Buffer.h>
#include <Magnum/GL/Mesh.h>
#include <Magnum/SceneGraph/SceneGraph.h>
#include <Magnum/Shaders/VertexColor.h>
#include <Magnum/Shaders/MeshVisualizer.h>

#include "Geometry\Circle.h"
#include "Geometry\Sphere.h"
#include "Geometry\Segment.h"
#include "Geometry\Ray.h"
#include "Geometry\Line.h"
#include "Geometry\Polyhedron.h"
#include "Geometry\ConvexPolyhedron.h"

using namespace Magnum;
using namespace Magnum::Math::Literals;

class DebugLines
{

public:
  //struct Point
  //{
  //  Vector3 position;
  //  Color3 color;
  //};

  explicit DebugLines(const std::size_t initialBufferCapacity);
  ~DebugLines();
  // move constructor
  DebugLines(DebugLines&&) noexcept;
  // Move assignment
  DebugLines& operator=(DebugLines&&) noexcept;

  // Copying is not allowed
  DebugLines(const DebugLines&) = delete;
  DebugLines& operator=(const DebugLines&) = delete;

  void FlushLines();
  void reset();

  void AddLine(const Vector3& from, const Vector3& to, const Color3& color)
  {
     AddLine(from, to, color, color);
  }
  
  void AddLine(const Vector3& from, const Vector3& to, const Color3& fromColor, const Color3& toColor)
  {
     _bufferData.emplace_back(from);
     _bufferData.push_back(fromColor);
     _bufferData.emplace_back(to);
     _bufferData.push_back(toColor);
  }

  void draw();
  void AddSegment(const Maths::Segment& s, const Color3& color);
  void AddRay(const Maths::Ray& r, const Color3& color);
  void AddLine(const Maths::Line& l, const Color3& color );
  void AddCircle(const Maths::Circle& c, const Color3& color);
  void AddSphere(const Maths::Sphere& s, const Color3& color);
  void AddPolyhedron(const Maths::Polyhedron& p, const Color3& color);
  void AddConvexPolyhedron(const Maths::ConvexPolyhedron& cp, const Color3& color);

  void AddXAxis(const Color3& xrgba);
  void AddYAxis(const Color3& yrgba);
  void AddZAxis(const Color3& zrgba);
  void AddXYAxis(const Color3& xrgba, const Color3& yrgba);
  void AddXZAxis(const Color3& xrgba, const Color3& zrgba);
  void AddYZAxis(const Color3& yrgba, const Color3& zrgba);
  void AddXYZAxes(void);

  void AddGrid(Maths::Point center, Maths::Vector normal, unsigned widthMeters, unsigned heightMeters, float metersPerLine, const Color3& rgba);

  DebugLines& setTransformationProjectionMatrix(const Matrix4& matrix)
  {
     _transformationProjectionMatrix = matrix;
     return *this;
  }

private:
   void AddAxisTick(Maths::Segment& tick, const Maths::Vector& step, unsigned count, const Color3& color);
   void AddAxis(const Maths::Vector& axis, const Color3& color);
protected:
  Matrix4 _transformationProjectionMatrix;
  GL::Buffer _buffer;
  GL::Mesh _mesh;
  Shaders::VertexColor3D _shader;
  std::vector<Vector3> _bufferData;
};



class DebugLinesGeo
{

public:

  explicit DebugLinesGeo();

  void reset();

  void addLine(const Vector3& p0, const Vector3& p1)
  {
    _lines.push_back(p0);
    _lines.push_back(p1);
  }

  void SetBuffer(std::vector<Vector3>& lines);

  void draw(const Matrix4& transformationProjectionMatrix);

protected:
  std::vector<Vector3> _lines;
  Color3 _color;
  GL::Buffer _buffer;
  GL::Mesh _mesh;
  Shaders::MeshVisualizer _shader;
};
