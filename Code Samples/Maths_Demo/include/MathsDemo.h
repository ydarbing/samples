#pragma once

#include <vector>
#include "Geometry/Geometry.h"


#include <Corrade/Containers/Reference.h>
#include <Corrade/Utility/Resource.h>
#include <Magnum/Image.h>
#include <Magnum/PixelFormat.h>
#include <Magnum/GL/AbstractShaderProgram.h>
#include <Magnum/GL/Context.h>
#include <Magnum/GL/DefaultFramebuffer.h>
#include <Magnum/GL/Framebuffer.h>
#include <Magnum/GL/Mesh.h>
#include <Magnum/GL/Renderbuffer.h>
#include <Magnum/GL/RenderbufferFormat.h>
#include <Magnum/GL/Renderer.h>
#include <Magnum/GL/Shader.h>
#include <Magnum/GL/Texture.h>
#include <Magnum/GL/TextureFormat.h>
#include <Magnum/GL/Version.h>
#include <Magnum/Math/Color.h>
#include <Magnum/MeshTools/Compile.h>
#include <Magnum/Platform/Sdl2Application.h>
#include <Magnum/Primitives/Cube.h>
#include <Magnum/Primitives/Plane.h>
#include <Magnum/Primitives/UVSphere.h>
#include <Magnum/Trade/MeshData3D.h>
#include <Magnum/SceneGraph/Scene.h>
#include <Magnum/SceneGraph/MatrixTransformation3D.h>
#include <Magnum/SceneGraph/Camera.h>
#include <Magnum/SceneGraph/Drawable.h>
#include <Magnum/Shaders/Generic.h>

#include <Magnum\Shaders\MeshVisualizer.h>
#include <Magnum\Shaders\Phong.h>
#include <Magnum\Shaders\Flat.h>

#include "DebugLines.h"

//namespace Maths
//{
using namespace Magnum;
using namespace Magnum::Math::Literals;

typedef SceneGraph::Object<SceneGraph::MatrixTransformation3D> Object3D;
typedef SceneGraph::Scene<SceneGraph::MatrixTransformation3D> Scene3D;

class WireframeDrawable : public SceneGraph::Drawable3D
{
public:
  explicit WireframeDrawable(Object3D& object, const Color4& color, Shaders::Flat3D& shader, GL::Mesh&& mesh, SceneGraph::DrawableGroup3D* drawables) : SceneGraph::Drawable3D{ object, drawables }, _color{ color }, _shader(shader), _mesh{ std::move(mesh) } {}

private:
  void draw(const Matrix4& transformation, SceneGraph::Camera3D& camera) override
  {
    _shader.setColor(_color)
      .setTransformationProjectionMatrix(camera.projectionMatrix() * transformation);
    _mesh.draw(_shader);
  }

  Color4 _color;
  Shaders::Flat3D& _shader;
  GL::Mesh _mesh;
};


class ColoredDrawable : public Object3D, SceneGraph::Drawable3D
{
public:
  explicit ColoredDrawable(UnsignedByte id, Shaders::MeshVisualizer& shader, const Color4& color, GL::Mesh& mesh, Object3D& parent, SceneGraph::DrawableGroup3D& drawables)
    : Object3D{ &parent }, SceneGraph::Drawable3D{ *this, &drawables }, _id{ id }, _shader(shader), _mesh(mesh), _color{ color }
  {}

  void setColor(Color4& color) { _color = color; }

private:
  void draw(const Matrix4& transformation, SceneGraph::Camera3D& camera) override
  {
    _shader.setTransformationProjectionMatrix(camera.projectionMatrix() * transformation)
      .setColor(_color)
      .setWireframeColor(Color3{ 1,1,1 })
      .setViewportSize(Vector2{ GL::defaultFramebuffer.viewport().size() });
    _mesh.draw(_shader);
  }

  Color4 _color;
  Shaders::MeshVisualizer& _shader;
  GL::Mesh& _mesh;
  UnsignedByte _id;
};

class PickableObject : public Object3D, SceneGraph::Drawable3D
{
public:
  explicit PickableObject(UnsignedByte id, Shaders::Phong& shader, Shaders::Flat3D& wireframeShader, const Color4& color, GL::Mesh& mesh, Object3D& parent, SceneGraph::DrawableGroup3D& drawables)
    : Object3D{ &parent }, SceneGraph::Drawable3D{ *this, &drawables }, _id{ id }, _selected{ false },
    _shaderWireframe(wireframeShader), _shader(shader),
    _color{ color }, _mesh(mesh) {}

  void setSelected(bool selected) { _selected = selected; }
  void setColor(Color4& color) { _color = color; }

private:
  virtual void draw(const Matrix4& transformationMatrix, SceneGraph::Camera3D& camera)
  {
    if (!_selected)
    {
      _shader.setTransformationMatrix(transformationMatrix)
        .setNormalMatrix(transformationMatrix.rotationScaling())
        .setProjectionMatrix(camera.projectionMatrix())
        .setAmbientColor(_selected ? _color * 0.3f : Color4{})
        .setDiffuseColor(_color * (_selected ? 2.0f : 1.0f))
        /* relative to the camera */
        .setLightPosition({ 13.0f, 2.0f, 5.0f })
        .setObjectId(_id);
      _mesh.draw(_shader);
    }
    else
    {
      _shaderWireframe.setTransformationProjectionMatrix(camera.projectionMatrix() * transformationMatrix)
        .setColor(_color * (_selected ? 2.0f : 1.0f))
        .setObjectId(_id);
      _mesh.draw(_shaderWireframe);
    }
  }

  UnsignedByte _id;
  bool _selected;
  Shaders::Flat3D& _shaderWireframe;
  Shaders::Phong& _shader;
  Color4 _color;
  GL::Mesh& _mesh;
  //GL::Mesh& _meshWireframe;
};

class MathsDemo : public Platform::Application
{
public:
  explicit MathsDemo(const Arguments& arguments);

private:

  struct Model
  {
    GL::Buffer indexBuffer, vertexBuffer;
    GL::Mesh mesh;
    Float radius;
  };

  void InitModels();
  void InitScene();
  void addModel(const Trade::MeshData3D& meshData3D);

  Float depthAt(const Vector2i& windowPosition);
  Vector3 unproject(const Vector2i& windowPosition, Float depth) const;

  void renderDebugLines();

  void drawEvent() override;
  void keyPressEvent(KeyEvent& event) override;
  void mousePressEvent(MouseEvent& event) override;
  void mouseMoveEvent(MouseMoveEvent& event) override;
  void mouseReleaseEvent(MouseEvent& event) override;
  void mouseScrollEvent(MouseScrollEvent& event) override;

  Scene3D _scene;
  Object3D* _cameraObject;
  SceneGraph::Camera3D* _camera;
  SceneGraph::DrawableGroup3D _drawables;

  DebugLines _debugLines;
  //DebugLinesGeo _debugLinesGeo;

  Shaders::Phong _shader;
  Shaders::Flat3D _shaderWireframe;
  std::vector<Model> m_models;

  std::vector<std::unique_ptr<PickableObject> > m_objects;
  //std::vector<std::unique_ptr<ColoredDrawable> > m_objects;
  std::vector<std::unique_ptr<WireframeDrawable> > m_wireFrames;

  bool _drawDebug{ false };
  bool _drawObjects{ true };
  bool _drawWireframe{ true };
  GL::Framebuffer _framebuffer;
  GL::Renderbuffer _color, _objectId, _depth;

  Vector2i _previousMousePosition, _mousePressPosition;
  Float _lastDepth;
  Vector2i _lastPosition{ -1 };
  Vector3 _rotationPoint, _translationPoint;
};

//}// namespace Maths

  //#include "Camera.h"
//#include "Geometry/Segment.h"

/*
namespace MathsDemo
{
  class Scene
  {
  public:
    Scene(int screenX, int screenY, int windowX, int windowY);
    Scene(int screenX, int screenY);
    ~Scene();

    // just get verts and faces of obj file
    bool GetBasicObj(const std::string file, std::vector<Maths::Point>& verts, std::vector<unsigned>& indices);

  private:
    void CastRay(int rayStartX, int rayStartY);
    void MakeWorld();
    void RenderString(int x, int y, const std::string s);
    void InitScene();
    void CameraToOrigin();
    void MakeRoom();

  protected:
    struct ShapeInfo
    {
      ShapeInfo(Maths::Geometry* shape_, const Maths::Color& color_, float thickness_ = 1.0f)
        : shape(shape_), color(color_), thickness(thickness_)
      { }
      Maths::Geometry* shape;
      Maths::Color color;
      float thickness;
    };

  private:
    Maths::Camera m_camera;
    Maths::Segment m_viewToPlane;
    std::vector<ShapeInfo> m_shapes;

    std::string m_camPosString;
    std::string m_intersectPosString;
    std::string m_rayStartPosString;

    int m_windowWidth;
    int m_windowHeight;
    int m_screenWidth;
    int m_screenHeight;

    float rotatespeed; // Each object can autorotate
    // frustum
    float m_near;
    float m_far;
    // mouse buttons
    bool m_leftDown;
    bool m_rightDown;
    // mouse pos
    int mouseX;
    int mouseY;


  };
}// namespace MathsDemo
*/
