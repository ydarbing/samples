#pragma once
#include "Color.h"

namespace Maths
{
  class Plane;

  void DrawPoint(const class Point& p, ColorPtr rgba = nullptr);
  void DrawSegment(const class Segment& s, ColorPtr rgba = nullptr);
  void DrawRay(const class Ray& r, ColorPtr rgba = nullptr);
  void DrawLine(const class Line& l, ColorPtr rgba = nullptr);
  static void DrawPlaneStatic(const Plane& p, const Point& c, unsigned units);
  void DrawPlane(const Plane& p, const Point* center = nullptr, ColorPtr rgba = nullptr, unsigned units = 1000);
  void DrawAAB(const class AABB& aab, ColorPtr rgba = nullptr);
  void DrawOOB(const class OOB& oob, ColorPtr rgba = nullptr);
  void DrawCircle(const class Circle& c, ColorPtr rgba = nullptr);
  void DrawSphere(const class Sphere& s, ColorPtr rgba = nullptr);
  void DrawPolyhedron(const class  Polyhedron& p, ColorPtr rgba = nullptr);
  void DrawConvexPolyhedron(const class ConvexPolyhedron& c, ColorPtr rgba = nullptr);

  void DrawXAxis(ColorPtr xrgba = nullptr);
  void DrawYAxis(ColorPtr yrgba = nullptr);
  void DrawZAxis(ColorPtr zrgba = nullptr);
  void DrawXYAxis(ColorPtr xrgba = nullptr, ColorPtr yrgba = nullptr);
  void DrawXZAxis(ColorPtr xrgba = nullptr, ColorPtr zrgba = nullptr);
  void DrawYZAxis(ColorPtr yrgba = nullptr, ColorPtr zrgba = nullptr);
  void DrawXYZAxes(void);

  void DrawOriginCircles(ColorPtr rgba = nullptr);

  void DrawGrid(Point center, Vector normal, unsigned widthMeters, unsigned heightMeters, float metersPerLine, ColorPtr rgba = nullptr);
  void ColorGridCoordinate(int x, int y, bool gridOnTop = true, ColorPtr rgba = nullptr);
  
}// namespace Maths
