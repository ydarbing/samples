#pragma once

#include <vector>
#include "Geometry/Geometry.h"


#include <Corrade/Containers/Reference.h>
#include <Corrade/Utility/Resource.h>
#include <Magnum/Image.h>
#include <Magnum/PixelFormat.h>
#include <Magnum/GL/AbstractShaderProgram.h>
#include <Magnum/GL/Context.h>
#include <Magnum/GL/DefaultFramebuffer.h>
#include <Magnum/GL/Framebuffer.h>
#include <Magnum/GL/Mesh.h>
#include <Magnum/GL/Renderbuffer.h>
#include <Magnum/GL/RenderbufferFormat.h>
#include <Magnum/GL/Renderer.h>
#include <Magnum/GL/Shader.h>
#include <Magnum/GL/Texture.h>
#include <Magnum/GL/TextureFormat.h>
#include <Magnum/GL/Version.h>
#include <Magnum/Math/Color.h>
#include <Magnum/MeshTools/Compile.h>
#include <Magnum/Platform/Sdl2Application.h>
#include <Magnum/Primitives/Cube.h>
#include <Magnum/Primitives/Plane.h>
#include <Magnum/Primitives/UVSphere.h>
#include <Magnum/Trade/MeshData3D.h>
#include <Magnum/SceneGraph/Scene.h>
#include <Magnum/SceneGraph/MatrixTransformation3D.h>
#include <Magnum/SceneGraph/Camera.h>
#include <Magnum/SceneGraph/Drawable.h>
#include <Magnum/Shaders/Generic.h>

#include <Magnum\Shaders\MeshVisualizer.h>

using namespace Magnum;
using namespace Magnum::Math::Literals;

typedef SceneGraph::Object<SceneGraph::MatrixTransformation3D> Object3D;
typedef SceneGraph::Scene<SceneGraph::MatrixTransformation3D> Scene3D;

class IdMeshVisualizer : public GL::AbstractShaderProgram //public Shaders::MeshVisualizer 
{
public:
  /* Not actually used, just for documentation purposes --- but attribute
     locations in the vertex shader source have to match these in order
     to make MeshTools::compile() work */
  typedef Shaders::Generic3D::Position Position;
  typedef Shaders::Generic3D::Normal Normal;

  enum : UnsignedInt
  {
    ColorOutput = Shaders::Generic3D::ColorOutput,
    ObjectIdOutput = Shaders::Generic3D::ObjectIdOutput
  };

  explicit IdMeshVisualizer(/*Flags flags = {}*/);

  IdMeshVisualizer& setObjectId(UnsignedInt id)
  {
    setUniform(m_objectIdUniform, id);
    return *this;
  }

  IdMeshVisualizer& setLightPosition(const Vector3& position)
  {
    setUniform(m_lightPositionUniform, position);
    return *this;
  }

  IdMeshVisualizer& setAmbientColor(const Color3& color)
  {
    setUniform(m_ambientColorUniform, color);
    return *this;
  }

  IdMeshVisualizer& setColor(const Color3& color)
  {
    setUniform(m_colorUniform, color);
    return *this;
  }

  IdMeshVisualizer& setTransformationMatrix(const Matrix4& matrix)
  {
    setUniform(m_transformationMatrixUniform, matrix);
    return *this;
  }

  IdMeshVisualizer& setNormalMatrix(const Matrix3x3& matrix)
  {
    setUniform(m_normalMatrixUniform, matrix);
    return *this;
  }

  IdMeshVisualizer& setProjectionMatrix(const Matrix4& matrix)
  {
    setUniform(m_projectionMatrixUniform, matrix);
    return *this;
  }

private:
  Int m_objectIdUniform,
    m_lightPositionUniform,
    m_ambientColorUniform,
    m_colorUniform,
    m_transformationMatrixUniform,
    m_normalMatrixUniform,
    m_projectionMatrixUniform;
};

IdMeshVisualizer::IdMeshVisualizer(/*Flags flags /*= {}*/)
  //: Shaders::MeshVisualizer(flags)
{
  //Utility::Resource rs("picking-data");

  GL::Shader vert{ GL::Version::GL330, GL::Shader::Type::Vertex },
    frag{ GL::Version::GL330, GL::Shader::Type::Fragment };
  //vert.addSource(rs.get("PhongId.vert"));
  //frag.addSource(rs.get("PhongId.frag"));
  vert.addFile("PhongId.vert");
  frag.addFile("PhongId.frag");
  CORRADE_INTERNAL_ASSERT(GL::Shader::compile({ vert, frag }));
  attachShaders({ vert, frag });
  CORRADE_INTERNAL_ASSERT(link());

  m_objectIdUniform = uniformLocation("objectId");
  m_lightPositionUniform = uniformLocation("light");
  m_ambientColorUniform = uniformLocation("ambientColor");
  m_colorUniform = uniformLocation("color");
  m_transformationMatrixUniform = uniformLocation("transformationMatrix");
  m_projectionMatrixUniform = uniformLocation("projectionMatrix");
  m_normalMatrixUniform = uniformLocation("normalMatrix");
}

