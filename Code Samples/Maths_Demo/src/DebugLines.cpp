/*
   This file is part of Magnum.

   Original authors � credit is appreciated but not required:

      2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019 �
         Vladim�r Vondru� <mosra@centrum.cz>
      2016 � Bill Robinson <airbaggins@gmail.com>

   This is free and unencumbered software released into the public domain.

   Anyone is free to copy, modify, publish, use, compile, sell, or distribute
   this software, either in source code form or as a compiled binary, for any
   purpose, commercial or non-commercial, and by any means.

   In jurisdictions that recognize copyright laws, the author or authors of
   this software dedicate any and all copyright interest in the software to
   the public domain. We make this dedication for the benefit of the public
   at large and to the detriment of our heirs and successors. We intend this
   dedication to be an overt act of relinquishment in perpetuity of all
   present and future rights to this software under copyright law.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
   THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "DebugLines.h"

#include <Corrade/Containers/ArrayViewStl.h>
#include <Magnum/GL/Renderer.h>
#include <Magnum/SceneGraph/Camera.h>
#include <Magnum\GL\DefaultFramebuffer.h>


#include "Utils.h"
#include "Matrix4.h"
#include "Geometry\Circle.h"
#include "Integration.h"

DebugLines::DebugLines(const std::size_t initialBufferCapacity)
   : _mesh{ GL::MeshPrimitive::Lines }
{
   _mesh.addVertexBuffer(_buffer, 0,
      Shaders::VertexColor3D::Position{},
      Shaders::VertexColor3D::Color3{});
   _bufferData.reserve(initialBufferCapacity * 4);
}

DebugLines::~DebugLines() = default;
DebugLines::DebugLines(DebugLines&&) noexcept = default;
DebugLines& DebugLines::operator=(DebugLines&&) noexcept = default;

void DebugLines::FlushLines()
{
   if (!_bufferData.empty())
   {
      /* Update buffer with new data */
      _buffer.setData(Containers::ArrayView<Vector3>(_bufferData.data(), _bufferData.size()),
         GL::BufferUsage::DynamicDraw);

      /* Update shader and draw */
      _shader.setTransformationProjectionMatrix(_transformationProjectionMatrix);
      _mesh.setCount(_bufferData.size() / 2)
         .draw(_shader);

      /* Clear buffer to receive new data */
      _bufferData.clear();
   }
}

void DebugLines::reset()
{
   _bufferData.clear();
   _buffer.invalidateData();
}

void DebugLines::draw()
{
   GL::Renderer::disable(GL::Renderer::Feature::DepthTest);
   //_buffer.setData(_bufferData, GL::BufferUsage::StreamDraw);
   //_mesh.setCount(_bufferData.size());
   _shader.setTransformationProjectionMatrix(_transformationProjectionMatrix);
   _mesh.draw(_shader);
   GL::Renderer::enable(GL::Renderer::Feature::DepthTest);
}


void DebugLines::AddSegment(const Maths::Segment& s, const Color3& color)
{
   AddLine(Vector3{ s.point0 }, Vector3{ s.point1 }, color);
}

void DebugLines::AddRay(const Maths::Ray& r, const Color3& color)
{
   Maths::Point to{ r.m_origin + r.m_dir * 50000.0f };

   AddLine(Vector3{ r.m_origin }, Vector3{ to }, color);
}

void DebugLines::AddLine(const Maths::Line& l, const Color3& color)
{
   //_bufferData.push_back(Vector3{l.point}, color });
   //_bufferData.push_back(Vector3{l.direction}, color });
   //_bufferData.push_back(Vector3{l.point}, color });
   //_bufferData.push_back(Vector3{-l.direction}, color });
}

void DebugLines::AddPolyhedron(const Maths::Polyhedron& p, const Color3& color)
{
   const std::vector<Maths::Point> verts = p.GetVertices();
   unsigned count = p.GetVertexCount();
   for (unsigned i = 0; i < count; ++i)
   {
      unsigned inext = ((i + 1) < count ? (i + 1) : 0);
      AddSegment(Maths::Segment(verts[i], verts[inext]), color);
   }
}

void DebugLines::AddCircle(const Maths::Circle& c, const Color3& color)
{
   using namespace Maths;
   unsigned segCount = Round(PI * 7.0f * c.m_radius);
   float angle = 0.0f;
   float delta = TWO_PI / float(segCount);
   float sina, cosa;

   for (unsigned i = 0; i <= segCount; ++i)
   {
      angle += delta;
      GetSinCos(angle, sina, cosa);

      Maths::Point p = c.m_center + (c.m_dir1 * c.m_radius * cosa + c.m_dir0 * c.m_radius * sina);

      _bufferData.emplace_back(Vector3{ p.GetPosition() });
      _bufferData.push_back(color);
   }
}

void DebugLines::AddSphere(const Maths::Sphere& s, const Color3& rgba)
{
   using namespace Maths;
   Circle xNorm(s.m_center, Vector::UNIT_X, s.m_radius);
   Circle yNorm(s.m_center, Vector::UNIT_Y, s.m_radius);
   Circle zNorm(s.m_center, Vector::UNIT_Z, s.m_radius);

   unsigned numCircles = Round(TWO_PI * s.m_radius);
   float angleDelta = PI / float(numCircles);

   Maths::Matrix4 rotAboutX;
   Maths::Matrix4 rotAboutY;
   Maths::Matrix4 rotAboutZ;

   rotAboutX.FromAxisAngle(Vector::UNIT_X, angleDelta);
   rotAboutY.FromAxisAngle(Vector::UNIT_Y, angleDelta);
   rotAboutZ.FromAxisAngle(Vector::UNIT_Z, angleDelta);

   for (unsigned i = 0; i < numCircles; ++i)
   {
      xNorm.m_normal = rotAboutZ * xNorm.m_normal;
      xNorm.m_dir0 = rotAboutZ * xNorm.m_dir0;
      xNorm.m_dir1 = rotAboutZ * xNorm.m_dir1;
      AddCircle(xNorm, 0xee0000_rgbf);

      yNorm.m_normal = rotAboutX * yNorm.m_normal;
      yNorm.m_dir0 = rotAboutX * yNorm.m_dir0;
      yNorm.m_dir1 = rotAboutX * yNorm.m_dir1;
      AddCircle(yNorm, 0x00ee00_rgbf);

      zNorm.m_normal = rotAboutY * zNorm.m_normal;
      zNorm.m_dir0 = rotAboutY * zNorm.m_dir0;
      zNorm.m_dir1 = rotAboutY * zNorm.m_dir1;
      AddCircle(zNorm, 0x0000ee_rgbf);
   }
}


void DebugLines::AddConvexPolyhedron(const Maths::ConvexPolyhedron& cp, const Color3& color)
{
   const std::vector<Maths::Point> verts = cp.GetVertices();
   unsigned count = cp.GetVertexCount();
   for (unsigned i = 0; i < count; ++i)
   {
      unsigned inext = ((i + 1) < count ? (i + 1) : 0);
      AddSegment(Maths::Segment(verts[i], verts[inext]), color);
   }
}

void DebugLines::AddAxisTick(Maths::Segment& tick, const Maths::Vector& step, unsigned count, const Color3& color)
{
   for (unsigned i = 0; i < count; ++i)
   {
      tick.center += step;
      tick.ComputeEndPoints();
      AddSegment(tick, color);
   }
}

void DebugLines::AddAxis(const Maths::Vector& axis, const Color3& color)
{
   Maths::Line axisLine(Maths::Point::ORIGIN, axis);
   AddLine(axisLine, color);

   Maths::Segment tick(Maths::Point::ORIGIN, Maths::Vector::UNIT_Z, 0.5f);
   AddAxisTick(tick, axis, 100, color);

   tick.center = Maths::Point::ORIGIN;
   AddAxisTick(tick, -axis, 100, color);

   tick.extent = 0.25f;
   tick.center = Maths::Point::ORIGIN - (axis * 0.5f);
   AddAxisTick(tick, axis, 100, color);
}

void DebugLines::AddXAxis(const Color3& xrgba)
{
   AddAxis(Maths::Vector{ Maths::Vector::UNIT_X }, xrgba);
}

void DebugLines::AddYAxis(const Color3& yrgba)
{
   AddAxis(Maths::Vector{ Maths::Vector::UNIT_Y }, yrgba);
}
void DebugLines::AddZAxis(const Color3& zrgba)
{
   AddAxis(Maths::Vector{ Maths::Vector::UNIT_Z }, zrgba);
}
void DebugLines::AddXYAxis(const Color3& xrgba, const Color3& yrgba)
{
   AddAxis(Maths::Vector{ Maths::Vector::UNIT_X }, xrgba);
   AddAxis(Maths::Vector{ Maths::Vector::UNIT_Y }, yrgba);
}
void DebugLines::AddXZAxis(const Color3& xrgba, const Color3& zrgba)
{
   AddAxis(Maths::Vector{ Maths::Vector::UNIT_X }, xrgba);
   AddAxis(Maths::Vector{ Maths::Vector::UNIT_Z }, zrgba);
}
void DebugLines::AddYZAxis(const Color3& yrgba, const Color3& zrgba)
{
   AddAxis(Maths::Vector{ Maths::Vector::UNIT_Y }, yrgba);
   AddAxis(Maths::Vector{ Maths::Vector::UNIT_Z }, zrgba);
}
void DebugLines::AddXYZAxes(void)
{
   constexpr Color3 x(1.0f, 0.0f, 0.0f);
   constexpr Color3 y(0.0f, 1.0f, 0.0f);
   constexpr Color3 z(0.0f, 0.0f, 1.0f);
   AddAxis(Maths::Vector{ Maths::Vector::UNIT_X }, x);
   AddAxis(Maths::Vector{ Maths::Vector::UNIT_Y }, y);
   AddAxis(Maths::Vector{ Maths::Vector::UNIT_Z }, z);
}

void DebugLines::AddGrid(Maths::Point center, Maths::Vector normal, unsigned widthMeters, unsigned heightMeters, float metersPerLine, const Color3& rgba)
{
   if (normal.IsZero())
      return;

   normal.Normalize();
   Maths::Vector comp0, comp1;

   Maths::Vector::BuildOrthonormalBasis(comp0, comp1, normal);

   Maths::Point end0 = center + comp0 * float(widthMeters), end1 = center - comp0 * float(widthMeters);

   AddSegment(Maths::Segment(end0, end1), rgba);

   Maths::Point posEnd0(end0), posEnd1(end1), negEnd0(end0), negEnd1(end1);
   unsigned numHeightLines = Maths::Round(float(heightMeters) / metersPerLine);
   for (unsigned i = 0; i < numHeightLines; ++i)
   {
      posEnd0 += comp1 * metersPerLine;
      posEnd1 += comp1 * metersPerLine;
      AddSegment(Maths::Segment(posEnd0, posEnd1), rgba);
      negEnd0 -= comp1 * metersPerLine;
      negEnd1 -= comp1 * metersPerLine;
      AddSegment(Maths::Segment(negEnd0, negEnd1), rgba);
   }

   end0 = posEnd0 = negEnd0 = center + comp1 * float(heightMeters);
   end1 = posEnd1 = negEnd1 = center - comp1 * float(heightMeters);
   AddSegment(Maths::Segment(end0, end1), rgba);

   unsigned numWidthLines = Maths::Round(float(widthMeters) / metersPerLine);
   for (unsigned i = 0; i < numWidthLines; ++i)
   {
      posEnd0 += comp0 * metersPerLine;
      posEnd1 += comp0 * metersPerLine;
      AddSegment(Maths::Segment(posEnd0, posEnd1), rgba);
      negEnd0 -= comp0 * metersPerLine;
      negEnd1 -= comp0 * metersPerLine;
      AddSegment(Maths::Segment(negEnd0, negEnd1), rgba);
   }

}




/*
/// DEebug shader using the mesh visualizer which uses a geometry shader that allows a way to
// easily change line width
DebugLinesGeo::DebugLinesGeo()
   : _mesh{ GL::MeshPrimitive::Lines },
   _shader{ Shaders::MeshVisualizer::Flag::Wireframe }
{
   _mesh.addVertexBuffer(_buffer, 0,
      Shaders::MeshVisualizer::Position{});
}

void DebugLinesGeo::reset()
{
   _bufferData.clear();
   _buffer.invalidateData();
}

void DebugLinesGeo::SetBuffer(std::vector<Vector3>& lines)
{
   _bufferData = lines;
   _buffer.setData(lines, GL::BufferUsage::StaticDraw);
}

void DebugLinesGeo::draw(const Matrix4& transformationProjectionMatrix)
{
   if (!_bufferData.empty())
   {
      GL::Renderer::disable(GL::Renderer::Feature::DepthTest);
      _buffer.setData(_bufferData, GL::BufferUsage::StreamDraw);
      _mesh.setCount(_bufferData.size());
      _shader.setTransformationProjectionMatrix(transformationProjectionMatrix)
         .setWireframeWidth(2.0f)
         .setColor(0xf0bfff_rgbf)
         .setWireframeColor(0xffffff_rgbf)
         .setViewportSize(Vector2{ GL::defaultFramebuffer.viewport().size() });
      _mesh.draw(_shader);
      GL::Renderer::enable(GL::Renderer::Feature::DepthTest);
   }
}
*/