#include "Draw.h"

#include "Matrix4.h"

#include "Geometry/Point.h"
#include "Geometry/Segment.h"
#include "Geometry/Ray.h"
#include "Geometry/Line.h"
#include "Geometry/Plane.h"
#include "Geometry/AABB.h"
#include "Geometry/OOB.h"
#include "Geometry/Circle.h"
#include "Geometry/Sphere.h"
#include "Geometry/ConvexPolyhedron.h"
#include "Distance\DistancePointToPlane.h"


namespace Maths
{


  void DrawPoint(const Point& p, ColorPtr rgba /*= nullptr*/)
  {
    //if (rgba)
    //	glColor4f(rgba->r, rgba->g, rgba->b, rgba->a);
    //
    //glBegin(GL_POINTS);
    //glVertex4f(p.x, p.y, p.z, p.w);
    //glEnd();
  }

  void DrawSegment(const Segment& s, ColorPtr rgba /*= nullptr*/)
  {
    //if (rgba)
    //	glColor4f(rgba->r, rgba->g, rgba->b, rgba->a);
    //
    //glBegin(GL_LINE_LOOP);
    //glVertex3f(s.point0.x, s.point0.y, s.point0.z);
    //glVertex3f(s.point1.x, s.point1.y, s.point1.z);
    //glEnd();
  }


  void DrawRay(const class Ray& r, ColorPtr rgba /*= nullptr*/)
  {
    //if (rgba)
    //	glColor4f(rgba->r, rgba->g, rgba->b, rgba->a);
    //
    //DrawSegment(Segment(r.m_origin, r.m_origin + r.m_dir * 50000.0f));
  }

  void DrawLine(const Line& line, ColorPtr rgba /*= nullptr*/)
  {
    //if (rgba)
    //  glColor4f(rgba->r, rgba->g, rgba->b, rgba->a);
    //
    //glBegin(GL_LINES);
    //glVertex4f(line.point.x, line.point.y, line.point.z, line.point.w);
    //glVertex4f(line.direction.x, line.direction.y, line.direction.z, line.direction.w);
    //glVertex4f(line.point.x, line.point.y, line.point.z, line.point.w);
    //glVertex4f(-line.direction.x, -line.direction.y, -line.direction.z, line.direction.w);
    //glEnd();
  }


  void DrawPolyhedron(const Polyhedron& p, ColorPtr rgba /*= nullptr*/)
  {
    //if (rgba)
    //  glColor4f(rgba->r, rgba->g, rgba->b, rgba->a);

    //const std::vector<Point> verts = p.GetVertices();
    //const std::vector<unsigned> indices = p.GetIndices();
    //
    //for (unsigned i = 0; i < indices.size(); i += 2)
    //{
    //  unsigned i1 = indices[i] - 1;
    //  unsigned i2 = indices[i+1] - 1;
    //  DrawSegment(Segment(verts[i1], verts[i2]));
    //}

    const std::vector<Point> verts = p.GetVertices();
    unsigned count = p.GetVertexCount();
    for (unsigned i = 0; i < count; ++i)
    {
      unsigned inext = ((i + 1) < count ? (i + 1) : 0);
      DrawSegment(Segment(verts[i], verts[inext]));
    }
  }

  void DrawConvexPolyhedron(const ConvexPolyhedron& c, ColorPtr rgba /*= nullptr*/)
  {
    //if (rgba)
    //  glColor4f(rgba->r, rgba->g, rgba->b, rgba->a);

    const std::vector<Point> verts = c.GetVertices();
    unsigned count = c.GetVertexCount();
    for (unsigned i = 0; i < count; ++i)
    {
      unsigned inext = ((i + 1) < count ? (i + 1) : 0);
      DrawSegment(Segment(verts[i], verts[inext]));
    }
  }


  static void DrawAxisTick(Segment& tick, const Vector& step, unsigned count, ColorPtr x = nullptr)
  {
    for (unsigned i = 0; i < count; ++i)
    {
      tick.center += step;
      tick.ComputeEndPoints();
      DrawSegment(tick, x);
    }
  }


  void DrawXAxis(ColorPtr xrgba /*= nullptr*/)
  {
    Vector Axis = Vector::UNIT_X;
    Line xAxis(Point::ORIGIN, Axis);
    DrawLine(xAxis, xrgba);

    Segment tick(Point::ORIGIN, Vector::UNIT_Z, 0.5f);
    DrawAxisTick(tick, Axis, 100);

    tick.center = Point::ORIGIN;
    DrawAxisTick(tick, -Axis, 100);

    tick.extent = 0.25f;
    tick.center = Point::ORIGIN - (Axis * 0.5f);
    DrawAxisTick(tick, Axis, 100);
  }

  void DrawYAxis(ColorPtr yrgba /*= nullptr*/)
  {
    Vector axis = Vector::UNIT_Y;
    Line yAxis(Point::ORIGIN, Vector::UNIT_Y);
    DrawLine(yAxis, yrgba);

    Segment tick(Point::ORIGIN, Vector::UNIT_X, 0.5f);
    DrawAxisTick(tick, axis, 100);

    tick.center = Point::ORIGIN;
    DrawAxisTick(tick, -axis, 100);

    tick.extent = 0.25f;
    tick.center = Point::ORIGIN - (axis * 0.5f);
    DrawAxisTick(tick, axis, 100);
  }

  void DrawZAxis(ColorPtr zrgba /*= nullptr*/)
  {
    Vector axis = Vector::UNIT_Z;
    Line zAxis(Point::ORIGIN, Vector::UNIT_Z);
    DrawLine(zAxis, zrgba);

    Segment tick(Point::ORIGIN, Vector::UNIT_X, 0.5f);
    DrawAxisTick(tick, axis, 100);

    tick.center = Point::ORIGIN;
    DrawAxisTick(tick, -axis, 100);

    tick.extent = 0.25f;
    tick.center = Point::ORIGIN - (axis * 0.5f);
    DrawAxisTick(tick, axis, 100);
  }

  void DrawXYAxis(ColorPtr xrgba /*= nullptr*/, ColorPtr yrgba /*= nullptr*/)
  {
    DrawXAxis(xrgba);
    DrawYAxis(yrgba);
  }

  void DrawXZAxis(ColorPtr xrgba  /*= nullptr*/, ColorPtr zrgba /*= nullptr*/)
  {
    DrawXAxis(xrgba);
    DrawZAxis(zrgba);
  }

  void DrawYZAxis(ColorPtr yrgba  /*= nullptr*/, ColorPtr zrgba /*= nullptr*/)
  {
    DrawXAxis(yrgba);
    DrawZAxis(zrgba);
  }

  void DrawXYZAxes(void)
  {
    Color x1(1.0f, 0.0f, 0.0f);
    Color y1(0.0f, 1.0f, 0.0f);
    Color z1(0.0f, 0.0f, 1.0f);
    DrawXAxis(&x1);
    DrawYAxis(&y1);
    DrawZAxis(&z1);
  }

  void DrawOriginCircles(ColorPtr rgba /*= nullptr*/)
  {

  }

  void DrawGrid(Point center, Vector normal, unsigned widthMeters, unsigned heightMeters, float metersPerLine, ColorPtr rgba /*= nullptr*/)
  {
    if (normal.IsZero())
      return;

    normal.Normalize();
    Vector comp0, comp1;

    //glLineWidth(3.0);

    Vector::BuildOrthonormalBasis(comp0, comp1, normal);

    Point end0 = center + comp0 * float(widthMeters), end1 = center - comp0 * float(widthMeters);

    DrawSegment(Segment(end0, end1), rgba);

    Point posEnd0(end0), posEnd1(end1), negEnd0(end0), negEnd1(end1);
    unsigned numHeightLines = Round(float(heightMeters) / metersPerLine);
    for (unsigned i = 0; i < numHeightLines; ++i)
    {
      posEnd0 += comp1 * metersPerLine;
      posEnd1 += comp1 * metersPerLine;
      DrawSegment(Segment(posEnd0, posEnd1), rgba);
      negEnd0 -= comp1 * metersPerLine;
      negEnd1 -= comp1 * metersPerLine;
      DrawSegment(Segment(negEnd0, negEnd1), rgba);
    }

    end0 = posEnd0 = negEnd0 = center + comp1 * float(heightMeters);
    end1 = posEnd1 = negEnd1 = center - comp1 * float(heightMeters);
    DrawSegment(Segment(end0, end1), rgba);

    unsigned numWidthLines = Round(float(widthMeters) / metersPerLine);
    for (unsigned i = 0; i < numWidthLines; ++i)
    {
      posEnd0 += comp0 * metersPerLine;
      posEnd1 += comp0 * metersPerLine;
      DrawSegment(Segment(posEnd0, posEnd1), rgba);
      negEnd0 -= comp0 * metersPerLine;
      negEnd1 -= comp0 * metersPerLine;
      DrawSegment(Segment(negEnd0, negEnd1), rgba);
    }
  }

  static void DrawPlaneStatic(const Plane& p, const Point& c, unsigned units)
  {
    DistancePointToPlane pointToPlane(c, p);
    pointToPlane.GetDistance();

    DrawGrid(pointToPlane.GetClosestPoint1(), p.GetNormal(), units, units, 1.0f);
  }

  void DrawPlane(const Plane& p, const Point* center /*= nullptr*/, ColorPtr rgba /*= nullptr*/, unsigned units /*= 1000*/)
  {
    //if (rgba)
    //  glColor4f(rgba->r, rgba->g, rgba->b, rgba->a);

    if (center == nullptr)
      DrawPlaneStatic(p, Point::ORIGIN, units);
    else
      DrawPlaneStatic(p, *center, units);
  }

  void DrawAAB(const class AABB& aab, ColorPtr rgba /*= nullptr*/)
  {
    //if (rgba)
    //  glColor4f(rgba->r, rgba->g, rgba->b, rgba->a);

    Point v[8];

    v[0].x = v[3].x = v[4].x = v[7].x = aab.m_min.x;
    v[0].y = v[1].y = v[2].y = v[3].y = aab.m_min.y;
    v[0].z = v[1].z = v[4].z = v[5].z = aab.m_min.z;

    v[1].x = v[2].x = v[5].x = v[6].x = aab.m_max.x;
    v[4].y = v[5].y = v[6].y = v[7].y = aab.m_max.y;
    v[2].z = v[3].z = v[6].z = v[7].z = aab.m_max.z;

    DrawSegment(Segment(v[0], v[1]));
    DrawSegment(Segment(v[1], v[2]));
    DrawSegment(Segment(v[2], v[3]));
    DrawSegment(Segment(v[3], v[0]));
    DrawSegment(Segment(v[0], v[4]));
    DrawSegment(Segment(v[1], v[5]));
    DrawSegment(Segment(v[2], v[6]));
    DrawSegment(Segment(v[3], v[7]));
    DrawSegment(Segment(v[4], v[5]));
    DrawSegment(Segment(v[5], v[6]));
    DrawSegment(Segment(v[6], v[7]));
    DrawSegment(Segment(v[7], v[4]));
  }

  void DrawOOB(const class OOB& oob, ColorPtr rgba /*= nullptr*/)
  {
    //if (rgba)
    //  glColor4f(rgba->r, rgba->g, rgba->b, rgba->a);

    Point v[8];
    oob.ComputeVertices(v);

    DrawSegment(Segment(v[0], v[1]));
    DrawSegment(Segment(v[0], v[3]));
    DrawSegment(Segment(v[0], v[4]));
    DrawSegment(Segment(v[1], v[2]));
    DrawSegment(Segment(v[1], v[5]));
    DrawSegment(Segment(v[2], v[3]));
    DrawSegment(Segment(v[2], v[6]));
    DrawSegment(Segment(v[3], v[7]));
    DrawSegment(Segment(v[4], v[5]));
    DrawSegment(Segment(v[4], v[7]));
    DrawSegment(Segment(v[5], v[6]));
    DrawSegment(Segment(v[6], v[7]));
  }


  void DrawCircle(const class Circle& c, ColorPtr rgba /*= nullptr*/)
  {
    //if (rgba)
    //  glColor4f(rgba->r, rgba->g, rgba->b, rgba->a);

    //unsigned segCount = Round(PI * 7.0f * c.m_radius);
    //float angle = 0.0f;
    //float delta = TWO_PI / float(segCount);
    //float sina, cosa;
    //
    //glBegin(GL_LINE_LOOP);
    //for (unsigned i = 0; i <= segCount; ++i)
    //{
    //  angle += delta;
    //  GetSinCos(angle, sina, cosa);
    //
    //  Point p = c.m_center + (c.m_dir1 * c.m_radius * cosa + c.m_dir0 * c.m_radius * sina);
    //  glVertex3f(p.x, p.y, p.z);
    //}
    //glEnd();
  }


  void DrawSphere(const class Sphere& s, ColorPtr rgba /*= nullptr*/)
  {
    //if (rgba)
    //  glColor4f(rgba->r, rgba->g, rgba->b, rgba->a);

    Circle xNorm(s.m_center, Vector::UNIT_X, s.m_radius);
    Circle yNorm(s.m_center, Vector::UNIT_Y, s.m_radius);
    Circle zNorm(s.m_center, Vector::UNIT_Z, s.m_radius);

    unsigned numCircles = Round(TWO_PI * s.m_radius);
    float angleDelta = PI / float(numCircles);

    Matrix4 rotAboutX;
    Matrix4 rotAboutY;
    Matrix4 rotAboutZ;

    rotAboutX.FromAxisAngle(Vector::UNIT_X, angleDelta);
    rotAboutY.FromAxisAngle(Vector::UNIT_Y, angleDelta);
    rotAboutZ.FromAxisAngle(Vector::UNIT_Z, angleDelta);

    for (unsigned i = 0; i < numCircles; ++i)
    {
      xNorm.m_normal = rotAboutZ * xNorm.m_normal;
      xNorm.m_dir0 = rotAboutZ * xNorm.m_dir0;
      xNorm.m_dir1 = rotAboutZ * xNorm.m_dir1;
      DrawCircle(xNorm);

      yNorm.m_normal = rotAboutX * yNorm.m_normal;
      yNorm.m_dir0 = rotAboutX * yNorm.m_dir0;
      yNorm.m_dir1 = rotAboutX * yNorm.m_dir1;
      DrawCircle(yNorm);

      zNorm.m_normal = rotAboutY * zNorm.m_normal;
      zNorm.m_dir0 = rotAboutY * zNorm.m_dir0;
      zNorm.m_dir1 = rotAboutY * zNorm.m_dir1;
      DrawCircle(zNorm);
    }
  }


}// namespace Maths
