#include "MathsDemo.h"

#include <memory> // make_unique

#include "Geometry/Polyhedron.h"
#include "Geometry/Plane.h"
#include "Geometry/Segment.h"
#include "Geometry/AABB.h"
#include "Geometry/OOB.h"
#include "Geometry/Sphere.h"
#include "Geometry/Line.h"


#include <Magnum\GL\PixelFormat.h>

#include <Magnum/Shaders/Phong.h>

#include <Magnum/MeshTools/Interleave.h>
#include <Magnum/MeshTools/CompressIndices.h>

#include <Magnum\Primitives\Line.h>

#include "Integration.h"

using namespace Magnum;

void MathsDemo::InitModels()
{
   /* Set up meshes */
   //addModel(Primitives::cubeWireframe());
   //addModel(Primitives::uvSphereSolid(16, 32));
   ////addModel(Primitives::planeSolid());// plane solid is not indexed so can't use addModel
   m_models.emplace_back();
   m_models.back().mesh = MeshTools::compile(Primitives::line3D());
   m_models.emplace_back();
   m_models.back().mesh = MeshTools::compile(Primitives::cubeWireframe());
   m_models.emplace_back();
   m_models.back().mesh = MeshTools::compile(Primitives::uvSphereWireframe(16, 32));
   m_models.emplace_back();
   m_models.back().mesh = MeshTools::compile(Primitives::planeWireframe());

   //Plane* plane = new Plane(0, 1, 0, 0);
   //Segment* s1 = new Segment(Point(-2.0f, 0.0f, -5.0f), Point(2.0f, 0.0f, -5.0f));
   //Segment* s2 = new Segment(Point(-2.0f, 1.0f, -5.0f), Point(2.0f, -1.0f, -5.0f));
   //Line* l1 = new Line(Point::ORIGIN, Vector::UNIT_Y);
   //AABB* aab = new AABB(Point(2.0f, 0.0f, 2.0f), Point(5.0f, 3.0f, 5.0f));
   //Sphere* sphere = new Sphere(Point(-2.0f, 1.0f, 2.0f), 1.0f);
   //
   //Vector axes[3] = { Vector(1.0f, 0, 0), Vector::BuildRandomUnitYZ(), Vector::BuildRandomUnitYZ() };
   //float ext[3] = { 2.0f, 2.0f, 2.0f };
   //OOB* oob = new OOB(Point(3.5f, 1.0f, -3.5f), axes, ext);
   //
   //m_shapes.push_back(ShapeInfo(plane, Color(0.9f, 0.1f, 0.1f, 0.1f), 1.0f));
   //m_shapes.push_back(ShapeInfo(s1, Color::BuildRandomBlue(), 1.0f));
   //m_shapes.push_back(ShapeInfo(s2, Color::BuildRandomBlue(), 1.0f));
   //m_shapes.push_back(ShapeInfo(l1, Color::BuildRandomGreen(), 1.0f));
   //m_shapes.push_back(ShapeInfo(aab, Color::BuildRandomBlue(), 1.0f));
   //m_shapes.push_back(ShapeInfo(oob, Color::BuildRandomBlue(), 1.0f));
}

void MathsDemo::InitScene()
{
   /* Set up objects */
   //m_objects.emplace_back(new PickableObject{ 1, _shader, _shaderWireframe, 0x2f83ccaa_rgbaf, m_models[3].mesh, _scene, _drawables });
   //m_objects.emplace_back(new PickableObject{ 2, _shader, _shaderWireframe, 0x3bd267aa_rgbaf, m_models[0].mesh, _scene, _drawables });
   //m_objects.emplace_back(new PickableObject{ 3, _shader, _shaderWireframe, 0xdcdcdcaa_rgbaf, m_models[0].mesh, _scene, _drawables });
   //m_objects.emplace_back(new PickableObject{ 4, _shader, _shaderWireframe, 0xc7cf2faa_rgbaf, m_models[0].mesh, _scene, _drawables });
   //m_objects.emplace_back(new PickableObject{ 5, _shader, _shaderWireframe, 0xcd3431aa_rgbaf, m_models[1].mesh, _scene, _drawables });
   //m_objects.emplace_back(new PickableObject{ 6, _shader, _shaderWireframe, 0xa5c9eaaa_rgbaf, m_models[1].mesh, _scene, _drawables });
   //
   //// set location of objects we just made
   //m_objects[1].get()->translate(Vector3(-5.0f, 2.0f, 1.0f)).rotateZ(Rad(-1.0f));
   //m_objects[2].get()->rotate(278.0_degf, Vector3(1.0f).normalized());
   //m_objects[3].get()->translate({ -0.2f, -1.7f, -2.7f });
   //// AABB
   //m_objects[4].get()->translate({ 0.7f, 0.6f, 2.2f })
   //  .scale(Vector3(0.75f));
   //// OOB
   //m_objects[5].get()->rotate(-92.0_degf, Vector3(1.0f).normalized())
   //  .scale(Vector3(2.0f))
   //  .translate({ -0.5f, -0.3f, 1.8f });

   using namespace Maths;
   //std::vector<Vector3> verts;
   //verts.push_back(Vector3{ 0, 0, 0 });
   //verts.push_back(Vector3{ 13.0833f, 0.0f, 0 });
   //verts.push_back(Vector3{ 13.0833f, 0.0f, 3.0833f });
   //verts.push_back(Vector3{ 12.1666f, 0.0f, 3.0833f });
   //verts.push_back(Vector3{ 7.09295f, 0.0f, 11.2134f });
   //verts.push_back(Vector3{ 0, 0, 7.0f });
   //
   //
   //unsigned count = verts.size() - 1;
   //for (unsigned i = 0; i < count; ++i)
   //{
   //   _debugLines.AddLine(verts[i], verts[i + 1], 0xffffff_rgbf);
   //}
   //// connect the first to the last
   //_debugLines.AddLine(verts.back(), verts[0], 0xffffff_rgbf);
   //
   //Circle c({ {0, 0, 0},
   //           {1.0, 0, 0}, 2.0f });
   //_debugLines.AddCircle(c, 0xfbbfbf_rgbf);

   Sphere s{ {0, 0.0f, 0}, 1.5 };
   _debugLines.AddSphere(s, 0xfbbfbf_rgbf);



   //Point out;
   //Point  line1(0.0f, 1.0f, 0.0f);
   //Point  line2(0.0f, 0.0f, 0.0f);
   //Vector hint(0.0f, 0.0f, 0.0f);
   //float len1 = 2.0f;
   //float len2 = 1.0f;
   //
   //if (intersect_line_segments(line1, len1, line2, len2, hint, &out))
   //{
   //   Circle c1(line1, Vector::UNIT_Z, len1);
   //   Circle c2(line2, Vector::UNIT_Z, len2);
   //   c1.Draw(&Color::BROWN);
   //   c2.Draw(&Color::GRAY);
   //   Segment s1(line1, out);
   //   Segment s2(line2, out);
   //   s1.Draw(&Color::YELLOW);
   //   s2.Draw(&Color::BLUE);
   //   line1.Draw(&Color::ORANGE);
   //   line2.Draw(&Color::ORANGE);
   //   out.Draw(&Color::GREEN);
   //}
   //Sphere sphere1(line1, len1);
   //Sphere sphere2(line2, len2);
   //sphere1.Draw(&Color::BROWN);
   //sphere2.Draw(&Color::GRAY);


   //DrawPoint(m_camera.GetFocusPos(), &Maths::Color::RED);
   //
   //DistancePointToSegment(m_camera.GetFocusPos(), *static_cast<Maths::Segment*>(m_shapes[1].shape))
   //  .Draw(true, &Color::BuildRandomColor(), &Color::BuildRandomColor(), &Color::BuildRandomColor());
   //glPopMatrix();
   //
   //DistancePointToOOB(m_camera.GetFocusPos(), *static_cast<Maths::OOB*>(m_shapes[5].shape))
   //  .Draw(true, &Color::BuildRandomColor(), &Color::BuildRandomColor(), &Color::BuildRandomColor());
}


MathsDemo::MathsDemo(const Arguments& arguments)
   : Platform::Application{ arguments, Configuration{}.setTitle("Maths Demo") },
   _framebuffer{ GL::defaultFramebuffer.viewport() }, _debugLines{ 100 }//, _debugLinesGeo{}
{
   MAGNUM_ASSERT_GL_VERSION_SUPPORTED(GL::Version::GL330);

   _color.setStorage(GL::RenderbufferFormat::RGBA8, GL::defaultFramebuffer.viewport().size());
   _objectId.setStorage(GL::RenderbufferFormat::R8UI, GL::defaultFramebuffer.viewport().size());
   _depth.setStorage(GL::RenderbufferFormat::DepthComponent24, GL::defaultFramebuffer.viewport().size());
   _framebuffer.attachRenderbuffer(GL::Framebuffer::ColorAttachment{ 0 }, _color)
      .attachRenderbuffer(GL::Framebuffer::ColorAttachment{ 1 }, _objectId)
      .attachRenderbuffer(GL::Framebuffer::BufferAttachment::Depth, _depth)
      .mapForDraw({ {Shaders::Flat3D::ColorOutput, GL::Framebuffer::ColorAttachment{0}},
                    {Shaders::Flat3D::ObjectIdOutput, GL::Framebuffer::ColorAttachment{1}} })
      .clearColor(0, 0x1f1f1fff_rgbaf)
      .clearColor(1, Vector4ui{ 0 })
      .bind();
   CORRADE_INTERNAL_ASSERT(_framebuffer.checkStatus(GL::FramebufferTarget::Draw) == GL::Framebuffer::Status::Complete);

   _shader = Shaders::Phong{ Shaders::Phong::Flag::ObjectId };
   _shaderWireframe = Shaders::Flat3D{ Shaders::Flat3D::Flag::ObjectId };


   InitModels();

   InitScene();

   /* Configure camera */
   _cameraObject = new Object3D{ &_scene };
   _cameraObject->translate(Vector3::zAxis(8.0f));
   _camera = new SceneGraph::Camera3D{ *_cameraObject };
   _camera->setAspectRatioPolicy(SceneGraph::AspectRatioPolicy::Extend)
      .setProjectionMatrix(Matrix4::perspectiveProjection(35.0_degf, 4.0f / 3.0f, 0.001f, 100.0f))
      .setViewport(GL::defaultFramebuffer.viewport().size());

   /* Initialize initial depth to the value at scene center */
   _lastDepth = ((_camera->projectionMatrix() * _camera->cameraMatrix()).transformPoint({}).z() + 1.0f) * 0.5f;
}

void MathsDemo::addModel(const Trade::MeshData3D& meshData3D)
{
   m_models.emplace_back();
   Model& model = m_models.back();

   model.vertexBuffer.setData(MeshTools::interleave(meshData3D.positions(0)), GL::BufferUsage::StaticDraw);

   Float maxMagnitudeSquared = 0.0f;
   for (Vector3 position : meshData3D.positions(0))
   {
      Float magnitudeSquared = position.dot();

      if (magnitudeSquared > maxMagnitudeSquared)
      {
         maxMagnitudeSquared = magnitudeSquared;
      }
   }
   model.radius = std::sqrt(maxMagnitudeSquared);

   Containers::Array<char> indexData;
   MeshIndexType indexType;
   UnsignedInt indexStart, indexEnd;
   std::tie(indexData, indexType, indexStart, indexEnd) = MeshTools::compressIndices(meshData3D.indices());
   model.indexBuffer.setData(indexData, GL::BufferUsage::StaticDraw);

   model.mesh.setPrimitive(meshData3D.primitive())
      .setCount(meshData3D.indices().size())
      .addVertexBuffer(model.vertexBuffer, 0, Shaders::Flat3D::Position{})
      .setIndexBuffer(model.indexBuffer, 0, indexType, indexStart, indexEnd);
}

void MathsDemo::drawEvent()
{
   /* Draw to custom framebuffer */
   _framebuffer
      .clearColor(0, Color4{ 0.125f })
      .clearColor(1, Vector4ui{})
      .clearDepth(1.0f)
      .bind();

   if (_drawObjects)
      _camera->draw(_drawables);

   if (_drawDebug)
   {
      // change this to avoid flickering
      if (_drawObjects)
         GL::Renderer::setDepthFunction(GL::Renderer::DepthFunction::LessOrEqual);

      //_debugDraw.setTransformationProjectionMatrix(
      //  _camera->projectionMatrix() * _camera->cameraMatrix());

      if (_drawObjects)
         GL::Renderer::setDepthFunction(GL::Renderer::DepthFunction::Less);
   }

   /* Bind the main buffer back */
   GL::defaultFramebuffer.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth)
      .bind();

   /* Blit color to window framebuffer */
   _framebuffer.mapForRead(GL::Framebuffer::ColorAttachment{ 0 });
   GL::AbstractFramebuffer::blit(_framebuffer, GL::defaultFramebuffer,
      { {}, _framebuffer.viewport().size() }, GL::FramebufferBlit::Color);

   renderDebugLines();

   swapBuffers();

}

void MathsDemo::mousePressEvent(MouseEvent& event)
{
   if (event.button() != MouseEvent::Button::Left)
      return;

   _previousMousePosition = _mousePressPosition = event.position();

   const Float currentDepth = depthAt(event.position());
   const Float depth = currentDepth == 1.0f ? _lastDepth : currentDepth;
   _translationPoint = unproject(event.position(), depth);
   /* Update the rotation point only if we're not zooming against infinite
      depth or if the original rotation point is not yet initialized */
   if (currentDepth != 1.0f || _rotationPoint.isZero())
   {
      _rotationPoint = _translationPoint;
      _lastDepth = depth;
   }
   event.setAccepted();
}

void MathsDemo::mouseMoveEvent(MouseMoveEvent& event)
{
   if (_lastPosition == Vector2i{ -1 })
      _lastPosition = event.position();
   const Vector2i delta = event.position() - _lastPosition;
   _lastPosition = event.position();

   if (!(event.buttons() & MouseMoveEvent::Button::Left))
      return;

   //const Vector2 delta = 3.0f *
   //  Vector2{ event.position() - _previousMousePosition } /
   //  Vector2{ GL::defaultFramebuffer.viewport().size() };

   /* Translate */
   if (event.modifiers() & MouseMoveEvent::Modifier::Shift)
   {
      const Vector3 p = unproject(event.position(), _lastDepth);
      _cameraObject->translateLocal(_translationPoint - p); /* is Z always 0? */
      _translationPoint = p;
   }
   else
   {
      _cameraObject->transformLocal(
         Matrix4::translation(_rotationPoint) *
         Matrix4::rotationX(-0.01_radf * static_cast<Float>(delta.y())) *
         Matrix4::rotationY(-0.01_radf * static_cast<Float>(delta.x())) *
         Matrix4::translation(-_rotationPoint));

   }

   _previousMousePosition = event.position();
   event.setAccepted();
   redraw();
}

void MathsDemo::mouseReleaseEvent(MouseEvent& event)
{
   if (event.button() != MouseEvent::Button::Left || _mousePressPosition != event.position())
      return;

   /* Read object ID at given click position (framebuffer has Y up while windowing system Y down) */
   _framebuffer.mapForRead(GL::Framebuffer::ColorAttachment{ 1 });
   Image2D data = _framebuffer.read(Range2Di::fromSize({ event.position().x(), _framebuffer.viewport().sizeY() - event.position().y() - 1 },
      { 1, 1 }),
      { PixelFormat::R8UI });

   /* Highlight object under mouse and deselect all other */
   for (auto& o : m_objects)
      o->setSelected(false);
   UnsignedByte id = Containers::arrayCast<UnsignedByte>(data.data())[0];
   if (id > 0 && id < m_objects.size() + 1)
      m_objects[id - 1]->setSelected(true);

   event.setAccepted();
   redraw();
}


Float MathsDemo::depthAt(const Vector2i& windowPosition)
{
   /* First scale the position from being relative to window size to being
        relative to framebuffer size as those two can be different on HiDPI
        systems */
   const Vector2i position = windowPosition * Vector2{ framebufferSize() } / Vector2{ windowSize() };
   const Vector2i fbPosition{ position.x(), GL::defaultFramebuffer.viewport().sizeY() - position.y() - 1 };

   GL::defaultFramebuffer.mapForRead(GL::DefaultFramebuffer::ReadAttachment::Front);
   Image2D data = GL::defaultFramebuffer.read(
      Range2Di::fromSize(fbPosition, Vector2i{ 1 }).padded(Vector2i{ 2 }),
      { GL::PixelFormat::DepthComponent, GL::PixelType::Float });

   return Math::min<Float>(Containers::arrayCast<const Float>(data.data()));
}

Vector3 MathsDemo::unproject(const Vector2i& windowPosition, Float depth) const
{
   /* We have to take window size, not framebuffer size, since the position is
        in window coordinates and the two can be different on HiDPI systems */
   const Vector2i viewSize = windowSize();
   const Vector2i viewPosition{ windowPosition.x(), viewSize.y() - windowPosition.y() - 1 };
   const Vector3 in{ 2 * Vector2{viewPosition} / Vector2{viewSize} -Vector2{1.0f}, depth * 2.0f - 1.0f };

   /*
       Use the following to get global coordinates instead of camera-relative:

       (_cameraObject->absoluteTransformationMatrix()*_camera->projectionMatrix().inverted()).transformPoint(in)
   */
   return _camera->projectionMatrix().inverted().transformPoint(in);
}

void MathsDemo::renderDebugLines()
{
   _debugLines.setTransformationProjectionMatrix(_camera->projectionMatrix() * _camera->cameraMatrix());
   _debugLines.FlushLines();
   _debugLines.draw();
}

void MathsDemo::keyPressEvent(KeyEvent& event)
{
   if (event.key() == KeyEvent::Key::NumSix)
   {
      m_objects[0].get()->setColor(Color4{ 0.5f, 0.3f, 0.05f, 0 });
      redraw();
      return;
   }

   if (event.key() == KeyEvent::Key::NumFive)
   {
      _drawWireframe = true;
      redraw();
      return;
   }
   else if (event.key() == KeyEvent::Key::NumEight)
   {
      _drawWireframe = false;
      redraw();
      return;
   }

   /* Reset the transformation to the original view */
   if (event.key() == KeyEvent::Key::NumZero)
   {
      (*_cameraObject)
         .resetTransformation()
         .translate(Vector3::zAxis(5.0f))
         .rotateX(-15.0_degf)
         .rotateY(30.0_degf);
      redraw();
      return;

      /* Axis-aligned view */
   }
   else if (event.key() == KeyEvent::Key::NumOne ||
      event.key() == KeyEvent::Key::NumThree ||
      event.key() == KeyEvent::Key::NumSeven)
   {
      /* Start with current camera translation with the rotation inverted */
      const Vector3 viewTranslation = _cameraObject->transformation().rotationScaling().inverted() * _cameraObject->transformation().translation();

      /* Front/back */
      const Float multiplier = event.modifiers() & KeyEvent::Modifier::Ctrl ? -1.0f : 1.0f;

      Matrix4 transformation;
      if (event.key() == KeyEvent::Key::NumSeven) /* Top/bottom */
         transformation = Matrix4::rotationX(-90.0_degf * multiplier);
      else if (event.key() == KeyEvent::Key::NumOne) /* Front/back */
         transformation = Matrix4::rotationY(90.0_degf - 90.0_degf * multiplier);
      else if (event.key() == KeyEvent::Key::NumThree) /* Right/left */
         transformation = Matrix4::rotationY(90.0_degf * multiplier);
      else CORRADE_ASSERT_UNREACHABLE("n/a", -1);

      _cameraObject->setTransformation(transformation * Matrix4::translation(viewTranslation));
      redraw();
   }
}

void MathsDemo::mouseScrollEvent(MouseScrollEvent& event)
{
   const Float currentDepth = depthAt(event.position());
   const Float depth = currentDepth == 1.0f ? _lastDepth : currentDepth;
   const Vector3 p = unproject(event.position(), depth);
   /* Update the rotation point only if we're not zooming against infinite
      depth or if the original rotation point is not yet initialized */
   if (currentDepth != 1.0f || _rotationPoint.isZero())
   {
      _rotationPoint = p;
      _lastDepth = depth;
   }

   const Float direction = event.offset().y();
   if (!direction)
      return;

   /* Move towards/backwards the rotation point in cam coords */
   _cameraObject->translateLocal(_rotationPoint * direction * 0.1f);

   event.setAccepted();
   redraw();
}



MAGNUM_APPLICATION_MAIN(MathsDemo)

/*
      void Scene::InitScene()
      {
        using namespace Maths;
        Plane* plane = new Plane(0, 1, 0, 0);
        Segment* s1 = new Segment(Point(-2.0f, 0.0f, -5.0f), Point(2.0f, 0.0f, -5.0f));
        Segment* s2 = new Segment(Point(-2.0f, 1.0f, -5.0f), Point(2.0f, -1.0f, -5.0f));
        Line* l1 = new Line(Point::ORIGIN, Vector::UNIT_Y);
        AABB* aab = new AABB(Point(2.0f, 0.0f, 2.0f), Point(5.0f, 3.0f, 5.0f));
        Sphere* sphere = new Sphere(Point(-2.0f, 1.0f, 2.0f), 1.0f);

        Vector axes[3] = { Vector(1.0f, 0, 0), Vector::BuildRandomUnitYZ(), Vector::BuildRandomUnitYZ() };
        float ext[3] = { 2.0f, 2.0f, 2.0f };
        OOB* oob = new OOB(Point(3.5f, 1.0f, -3.5f), axes, ext);

        m_shapes.push_back(ShapeInfo(plane, Color(0.9f, 0.1f, 0.1f, 0.1f), 1.0f));
        m_shapes.push_back(ShapeInfo(s1, Color::BuildRandomBlue(), 1.0f));
        m_shapes.push_back(ShapeInfo(s2, Color::BuildRandomBlue(), 1.0f));
        m_shapes.push_back(ShapeInfo(l1, Color::BuildRandomGreen(), 1.0f));
        m_shapes.push_back(ShapeInfo(aab, Color::BuildRandomBlue(), 1.0f));
        m_shapes.push_back(ShapeInfo(oob, Color::BuildRandomBlue(), 1.0f));

        //std::vector<Point> verts;
        //std::vector<unsigned> indices;
        //unsigned numTris = 0;
        //if (GetBasicObj("Objects/dodecahedron.obj", verts, indices))
        //{
        //  numTris = indices.size() / 3;
        //  Polyhedron* p1 = new Polyhedron(verts, numTris, indices);
        //  m_shapes.push_back(ShapeInfo(p1, Color::BuildRandomGreen(), 1.0f));
        //}

        m_shapes.push_back(ShapeInfo(sphere, Color::BuildRandomGreen(), 1.0f));

        //MakeRoom();
      }

      void MakeRoom()
      {
        using namespace Maths;
        std::vector<Point> verts;
        std::vector<unsigned> indices;
        indices.push_back(0);
        indices.push_back(1);
        indices.push_back(3);

        indices.push_back(1);
        indices.push_back(2);
        indices.push_back(3);

        indices.push_back(0);
        indices.push_back(3);
        indices.push_back(4);

        indices.push_back(0);
        indices.push_back(4);
        indices.push_back(5);

        verts.push_back(Point(0, 0, 0));
        verts.push_back(Point(13.0833f, 0, 0));
        verts.push_back(Point(13.0833f, 0, 3.0833f));
        verts.push_back(Point(12.1666f, 0, 3.0833f));
        verts.push_back(Point(7.09295f, 0, 11.2134f));
        verts.push_back(Point(0, 0, 7.0f));
        Polyhedron* room = new Polyhedron(verts, 4, indices);
        float sa = room->ComputeSurfaceArea();
        m_shapes.push_back(ShapeInfo(room, Color::BuildRandomBlue(), 1.0f));

        verts.clear();
        verts.push_back(Point(0, 0, 0));
        verts.push_back(Point(13.0833f, 0, 0));
        verts.push_back(Point(13.0833f, 0, 3.0833f));
        verts.push_back(Point(12.1666f, 0, 3.0833f));
        indices.clear();
        indices.push_back(0);
        indices.push_back(1);
        indices.push_back(2);

        indices.push_back(0);
        indices.push_back(2);
        indices.push_back(3);
        Polyhedron* closet = new Polyhedron(verts, 4, indices);
        float csa = closet->ComputeSurfaceArea();
        m_shapes.push_back(ShapeInfo(closet, Color::BuildRandomBlue(), 1.0f));
        float closetWallStart = (84.0f / 12.0f);

        float unknownX = 87.6810379f / 12.0f;// these were found using circle circle intersection based on wall points
        float unknownZ = 136.115585f / 12.0f;// these were found using circle circle intersection based on wall points

        float closetWallLength = 102.0f / 12.0f;
        float closestStartOnWallLength = 17.0f / 12.0f;
        float closetEndOnWallLength = 91.0f / 12.0f;
        float closetDoorDepth = 3.0f / 12.0f;
        float closetInsideLeftWallLength = 14.0f / 12.0f;
        float closetInsideRightWallLength = 11.0f / 12.0f;
        float closetLeftWallLength = (28.0f / 12.0f);
        float closetRightWallLength = (25.0f / 12.0f);
        Segment* closetWall = new Segment(Point(0, 0, closetWallStart), Point(unknownX, 0, unknownZ));

        float wallLength = closetWall->extent * 2.0f;
        Vector wallVector = closetWall->direction;
        wallVector.Normalize();
        Point wallEnd = closetWall->point0 + wallVector * closetWallLength;
        Point closetBegin = closetWall->point0 + wallVector * closestStartOnWallLength;
        Point closetEnd = closetWall->point0 + wallVector * closetEndOnWallLength;

        Segment* closetSeg = new Segment(closetBegin, closetEnd);

        float closetDoor = closetSeg->extent * 2.0f;

        Vector closetVector = closetEnd - closetBegin;
        closetVector.Normalize();
        Vector closetNormal(-closetVector.z, 0, closetVector.x);

        Point closetInsideBegin = closetBegin + closetNormal * closetDoorDepth;
        Point closetInsideEnd = closetEnd + closetNormal * closetDoorDepth;


        Segment* closetLeft = new Segment(closetBegin, closetInsideBegin);
        Segment* closetRight = new Segment(closetEnd, closetInsideEnd);


        m_shapes.push_back(ShapeInfo(closetLeft, Color::BuildRandomBlue(), 1.0f));
        m_shapes.push_back(ShapeInfo(closetRight, Color::BuildRandomBlue(), 1.0f));

        Vector closetInsideVector = closetEnd - closetInsideEnd;
        Vector closetInsideNormal(-closetInsideVector.z, 0, closetInsideVector.x);
        closetInsideNormal.Normalize();

        Segment* closetInsideWallLeft = new Segment(closetInsideBegin, closetInsideBegin + closetInsideNormal * -closetInsideLeftWallLength);
        Segment* closetInsideWallRight = new Segment(closetInsideEnd, closetInsideEnd + closetInsideNormal * closetInsideRightWallLength);

        Segment* closetInside = new Segment(closetInsideWallLeft->point1, closetInsideWallRight->point1);

        float closetInsideLength = closetInside->extent * 2.0f;


        m_shapes.push_back(ShapeInfo(closetInsideWallLeft, Color::BuildRandomBlue(), 1.0f));
        m_shapes.push_back(ShapeInfo(closetInsideWallRight, Color::BuildRandomBlue(), 1.0f));


        // rotate the left and right closetInsideNormal by degrees
        float radLeft = -62.797f * DEG_TO_RAD; //-63.199f * DEG_TO_RAD;
        float newX = closetInsideNormal.x * cos(radLeft) + closetInsideNormal.z * sin(radLeft);
        float newZ = -closetInsideNormal.x * sin(radLeft) + closetInsideNormal.z * cos(radLeft);
        Vector closetLeftAngleVector(newX, 0, newZ);
        // rotate the left and right closetInsideNormal by degrees
        float radRight = (91.433f - 180.0f) * DEG_TO_RAD; //(84.951f - 180.0f) * DEG_TO_RAD;
        newX = closetInsideNormal.x * cos(radRight) + closetInsideNormal.z * sin(radRight);
        newZ = -closetInsideNormal.x * sin(radRight) + closetInsideNormal.z * cos(radRight);
        Vector closetRightAngleVector(newX, 0, newZ);

        closetLeftAngleVector.Normalize();
        closetRightAngleVector.Normalize();


        Segment* closetLeftWall = new Segment(closetInsideWallLeft->point1, closetInsideWallLeft->point1 + closetLeftAngleVector * closetLeftWallLength);
        Segment* closetRightWall = new Segment(closetInsideWallRight->point1, closetInsideWallRight->point1 + closetRightAngleVector * closetRightWallLength);

        m_shapes.push_back(ShapeInfo(closetLeftWall, Color::BuildRandomGreen(), 1.0f));
        m_shapes.push_back(ShapeInfo(closetRightWall, Color::BuildRandomRed(), 1.0f));


        Segment* closetBackWall = new Segment(closetLeftWall->point1, closetRightWall->point1);

        float closetLeftLength = closetLeftWall->extent * 2.0f;
        float closetRightLength = closetRightWall->extent * 2.0f;
        float closetBackLength = closetBackWall->extent * 2.0f;
        m_shapes.push_back(ShapeInfo(closetBackWall, Color::BuildRandomBlue(), 1.0f));
      }


      void Scene::HandleDisplay(void)
      {
        using namespace Maths;
        std::stringstream ss;
        ss << "Cam Pos: ";
        ss << m_camera.GetPos().x << ", " << m_camera.GetPos().y << ", " << m_camera.GetPos().z;
        m_camPosString = ss.str();

        Point focus;
        //find focal point
        m_camera.GetLookDir().Normalize();
        focus = m_camera.GetPos() + (m_camera.GetLookDir() * m_camera.GetFocalLength());
        Vector right = m_camera.GetLookDir().Cross(m_camera.GetUpDir());
        right.Normalize();
        //right *= m_camera.GetEyeSeperation() / 2.0f;


        MakeWorld();
      }


      void Scene::MakeWorld()
      {
        //static float rotateangle = 0.0f;
        //glRotatef(rotateangle, 0.0, 1.0, 0.0);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        using namespace Maths;
        for (unsigned i = 0; i < m_shapes.size(); ++i)
        {
          glLineWidth(m_shapes[i].thickness);
          m_shapes[i].shape->Draw(&m_shapes[i].color);
        }
        glLineWidth(1.0f);


        //Point out;
        //Point  line1(0.0f, 1.0f, 0.0f);
        //Point  line2(0.0f, 0.0f, 0.0f);
        //Vector hint(0.0f, 0.0f, 0.0f);
        //float len1 = 2.0f;
        //float len2 = 1.0f;
        //
        //if (intersect_line_segments(line1, len1, line2, len2, hint, &out))
        //{
        //  Circle c1(line1, Vector::UNIT_Z, len1);
        //  Circle c2(line2, Vector::UNIT_Z, len2);
        //  c1.Draw(&Color::BROWN);
        //  c2.Draw(&Color::GRAY);
        //  Segment s1(line1, out);
        //  Segment s2(line2, out);
        //  s1.Draw(&Color::YELLOW);
        //  s2.Draw(&Color::BLUE);
        //  line1.Draw(&Color::ORANGE);
        //  line2.Draw(&Color::ORANGE);
        //  out.Draw(&Color::GREEN);
        //}
        //Sphere sphere1(line1, len1);
        //Sphere sphere2(line2, len2);
        //sphere1.Draw(&Color::BROWN);
        //sphere2.Draw(&Color::GRAY);


        DrawPoint(m_camera.GetFocusPos(), &Maths::Color::RED);

        DistancePointToSegment(m_camera.GetFocusPos(), *static_cast<Maths::Segment*>(m_shapes[1].shape))
          .Draw(true, &Color::BuildRandomColor(), &Color::BuildRandomColor(), &Color::BuildRandomColor());
        glPopMatrix();

        DistancePointToOOB(m_camera.GetFocusPos(), *static_cast<Maths::OOB*>(m_shapes[5].shape))
          .Draw(true, &Color::BuildRandomColor(), &Color::BuildRandomColor(), &Color::BuildRandomColor());

        m_viewToPlane.Draw(&Color::BuildRandomBlue());
        //rotateangle += rotatespeed;
      }

      bool Scene::GetBasicObj(const std::string file, std::vector<Maths::Point>& verts, std::vector<unsigned>& indices)
      {
        std::ifstream obj(file);
        std::string line;
        if (obj.is_open())
        {
          verts.clear();
          indices.clear();

          while (getline(obj, line))
          {
            std::istringstream lineSS(line);
            std::string lineType;
            lineSS >> lineType;

            if (lineType == "v")
            {
              float x = 0, y = 0, z = 0, w = 1.0f;
              lineSS >> x >> y >> z >> w;
              verts.push_back(Maths::Point(x, y, z, w));
            }
            else if (lineType == "f")
            {
              // only assumes there are vertices associated with faces
              unsigned i1 = 0, i2 = 0, i3 = 0;
              lineSS >> i1 >> i2 >> i3;
              indices.push_back(i1); indices.push_back(i2);
              indices.push_back(i1); indices.push_back(i3);
              indices.push_back(i2); indices.push_back(i3);
            }
          }
          return true;
        }
        return false;
      }

      void Scene::CastRay(int rayStartX, int rayStartY)
      {
        m_camera.Update(0);

        Maths::Point rayStartWorld = m_camera.WorldFromScreen(m_windowWidth, m_windowHeight, rayStartX, rayStartY);
        Maths::Vector dir = rayStartWorld - m_camera.GetPos();
        dir.Normalize();
        Maths::IntersectionRayToPlane iRayPlane(Maths::Ray(rayStartWorld, dir), *static_cast<Maths::Plane*>(m_shapes[0].shape));

        iRayPlane.Draw(true, &Maths::Color::BuildRandomColor());
        Maths::Ray r = iRayPlane.GetRay();
        Maths::Point p = r.GetPosition();
        m_viewToPlane = Maths::Segment(p, iRayPlane.GetIntersectionPoint());


        std::stringstream ss;
        ss << "Ray pos: ";
        ss << p.x << ", " << p.y << ", " << p.z;
        m_rayStartPosString = ss.str();

        std::stringstream ss1;
        ss1 << "Int pos: ";
        ss1 << m_viewToPlane.point1.x << ", " << m_viewToPlane.point1.y << ", " << m_viewToPlane.point1.z;
        m_intersectPosString = ss1.str();

        //m_shapes.back().shape->SetPosition(iRayPlane.GetIntersectionPoint());
      }


    }// namespace Demo

    */
    //}// namespace Maths
