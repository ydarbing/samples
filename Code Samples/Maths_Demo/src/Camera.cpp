#include "Camera.h"
#include "Utils.h"

namespace Maths
{

	float dtheta = 1.0f;         // Camera rotation angle increment

	Camera::Camera()
		: m_spin(-150.0f), m_tilt(-60.0f), m_zoom(1.0f), m_fov(45.0f), m_eyeSeparation(0),
		m_aperture(0), m_focalLength(1.0f), m_far(1000.0f), m_near(0.1f), m_aspectRatio(16.0f / 9.0f),
		m_pos(), m_focusPos(), m_upDir(), m_lookDir(),
		m_matrixProjection(), m_matrixView(),
		m_cameraFromWorld(), m_projectionFromCamera()
	{
	}

	void Camera::Rotate(float yaw, float pitch, float roll)
	{
		Point pos;
		Vector right, up, view;
		Vector newPos, newRight;
		float radius, dd, radians;

		up = m_upDir;
		pos = m_pos;
		view = m_lookDir;
		up.Normalize();
		view.Normalize();
		right = view.Cross(up);
		right.Normalize();
		radians = dtheta * PI / 180.0f;

		// Handle the roll
		if (roll != 0)
		{
			m_upDir += (right * roll) * radians;
			m_upDir.Normalize();
			return;
		}

		// Distance from rotate point 
		Vector d = pos - m_focusPos;
		radius = d.Length();

		// Determine new view point
		dd = radius * radians;
		newPos = m_pos + right * yaw * dd + up * pitch * dd - m_focusPos;
		newPos.Normalize();
		m_pos = m_focusPos + newPos * radius;

		// Determine new right vector 
		newRight = m_pos + right - m_focusPos;
		newRight.Normalize();
		newRight = m_focusPos + newRight * radius - m_pos;

		m_lookDir = m_focusPos - m_pos;
		m_lookDir.Normalize();

		// Determine new up vector
		m_upDir = newRight.Cross(m_lookDir);
		m_upDir.Normalize();
	}

	void Camera::Translate(float x, float y)
	{
		Point pos;
		Vector right, up, view;
		float radians, delta;

		up = m_upDir;
		pos = m_pos;
		view = m_lookDir;

		up.Normalize();
		view.Normalize();
		right = view.Cross(up);
		right.Normalize();
		radians = dtheta * PI / 180.0f;
		delta = dtheta * m_focalLength / 90.0f;

		m_pos += right * x * delta;
		m_focusPos += right * x * delta;

		m_pos += up * y * delta;
		m_focusPos += up * y * delta;
	}

	Point& Camera::GetFocusPos()
	{
		return m_focusPos;
	}


	Point& Camera::GetPos()
	{
		return m_pos;
	}
	Vector& Camera::GetLookDir()
	{
		return m_lookDir;
	}
	Vector& Camera::GetUpDir()
	{
		return m_upDir;
	}


	float Camera::GetFocalLength()const
	{
		return m_focalLength;
	}
	float Camera::GetAperture()const
	{
		return m_aperture;
	}
	float Camera::GetEyeSeparation()const
	{
		return m_eyeSeparation;
	}


	void Camera::SetSpin(float spin)
	{
		m_spin = spin;
	}

	void Camera::SetTilt(float tilt)
	{
		m_tilt = tilt;
	}


	void Camera::AddSpin(float spin)
	{
		m_spin += spin;
	}

	void Camera::AddTilt(float tilt)
	{
		m_tilt += tilt;
	}

	void Camera::SetAperture(float ap)
	{
		m_aperture = ap;
	}

	void Camera::SetFocusPos(Point fp)
	{
		m_focusPos = fp;
	}

	void Camera::SetLookDir(Vector ld)
	{
		m_lookDir = ld;
	}


	void Camera::SetUpDir(Vector ud)
	{
		m_upDir = ud;
	}
	void Camera::SetPos(Point pos)
	{
		m_pos = pos;
	}

	void Camera::SetFocalLength(float fl)
	{
		m_focalLength = fl;
	}

	void Camera::SetEyeSeparation(float es)
	{
		m_eyeSeparation = es;
	}

	void Camera::SetZoom(double zoom)
	{
		m_zoom = zoom;
	}

	void Camera::AddZoom(float amount)
	{
		m_zoom += amount;
	}

	double Camera::GetZoom() const
	{
		return m_zoom;
	}

	Point Camera::WorldFromScreen(int screenWidth, int screenHeight, int screenX, int screenY)
	{
		Point pos;
		pos.x = screenX / (float)screenWidth;
		pos.y = screenY / (float)screenHeight;

		pos.x *= 2.0f;
		pos.y *= 2.0f;

		pos.y = 1.0f - pos.y;
		pos.x = pos.x - 1.0f;

		pos.z = -1.0f;

		Matrix4 worldFromProjection = m_projectionFromCamera.TransposeTimesTranspose(m_cameraFromWorld).Inverse();
		pos = worldFromProjection * pos;

		if (pos.w != 0.0f)
			pos /= pos.w;

		return pos;
	}

	void Camera::Update(float dt)
	{
		UpdateMatrixProjection();
		UpdateMatrixView();
		UpdateProjectionFromCamera();
		UpdateCameraFromWorld();
	}

	void Camera::UpdateProjectionFromCamera(void)
	{
		m_projectionFromCamera = m_matrixProjection;
		//glMatrixMode(GL_PROJECTION);
		//glLoadIdentity();
		//glLoadMatrixf(&m_projectionFromCamera.m[0][0]);
	}

	void Camera::UpdateCameraFromWorld(void)
	{
		m_cameraFromWorld = m_matrixView;
		//glMatrixMode(GL_MODELVIEW);
		//glLoadIdentity();
		//glLoadMatrixf(&m_cameraFromWorld.m[0][0]);
	}

	void Camera::UpdateMatrixProjection(void)
	{
		m_matrixProjection.MakePerspectiveProjection(/*(float)(m_aperture * m_zoom)*/
			m_fov, m_aspectRatio, m_near, m_far);
	}

	void Camera::UpdateMatrixView(void)
	{
		m_matrixView.MakeLookAt(m_pos, m_focusPos, m_upDir);
	}

	void Camera::SetAspect(float ratio)
	{
		m_aspectRatio = ratio;
	}

	void Camera::SetAspect(float width, float height)
	{
		m_aspectRatio = width / height;
	}

	void Camera::SetNear(float near)
	{
		m_near = near;
	}
	void Camera::SetFar(float far)
	{
		m_far = far;
	}

	float Camera::GetAspectRatio() const
	{
		return m_aspectRatio;
	}
	float Camera::GetNear() const
	{
		return m_near;
	}
	float Camera::GetFar() const
	{
		return m_far;
	}

	float Camera::GetFOV() const
	{
		return m_fov;
	}

	void Camera::SetFOV(float fov)
	{
		m_fov = fov;
	}


}// namespace Maths
