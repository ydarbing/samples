#include "RPN.h"

#include <cassert>

int main(int argc, char ** argv)
{
  RPN<int> rpn("4 13 5 / +"); // (4 + (13 / 5)) = 6

  int answer = rpn.Solve();
  assert(answer == 6);
  rpn.SetRPN("2 1 + 3 *"); // ((2+1) * 3) = 9
  answer = rpn.Solve();
  assert(answer == 9);

  return 0;
}
