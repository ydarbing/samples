#pragma once
#include <string>
#include <stack>

template <typename T>
class RPN
{
public:
  RPN();
	RPN(const std::string inString);
	~RPN();

  T Solve();
  void SetRPN(const std::string rpn);

private:
  void GetTwoStackValues(std::stack<T>& st, T& lVal, T& rVal);

  std::string m_rpn;
};

#include "RPN.hpp"
