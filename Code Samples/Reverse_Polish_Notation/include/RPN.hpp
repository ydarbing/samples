
#include <sstream>
#include <iostream> //cerr

template <typename T>
RPN<T>::RPN(const std::string inString)
  : m_rpn(inString)
{
}

template <typename T>
RPN<T>::RPN()
{
}

template <typename T>
RPN<T>::~RPN()
{
}

template <typename T>
void RPN<T>::SetRPN(const std::string rpn)
{
  m_rpn = rpn;
}

template <typename T>
void RPN<T>::GetTwoStackValues(std::stack<T>& st, T& lVal, T& rVal)
{
  rVal = st.top();
  st.pop();
  lVal = st.top();
  st.pop();
}


template <typename T>
T RPN<T>::Solve()
{
  if (m_rpn.empty())
    return 0;

  std::stack<T> st;
  std::istringstream iss(m_rpn);

  std::string token;
  T value;

  while (iss >> token)
  {
    if (std::istringstream(token) >> value)
    {
      st.push(value);
    }
    else // non-number
    {
      T lVal = 0;
      T rVal = 0;
      GetTwoStackValues(st, lVal, rVal);

      switch (token[0])
      {
      case '+': st.push(lVal + rVal); break;
      case '-': st.push(lVal - rVal); break;
      case '*': st.push(lVal * rVal); break;
      case '/': st.push(lVal / rVal); break;
      default:
        std::cerr << "error" << std::endl;
        std::exit(1);
      }
    }

  }
  if (st.size() == 1)
    return st.top();
  else // input was an incomplete rpn or not valid
  {
    std::cerr << "error" << std::endl;
    std::exit(1);
  }
}
