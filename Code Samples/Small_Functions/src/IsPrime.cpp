


bool IsPrime(int n)
{
  int divisor;

  if (n <= 1)
    return false;
  
  for (divisor = 2; divisor * divisor <= n; ++divisor)
  {
    if (n % divisor == 0)
    {
      return false;
    }
  }
  return true;
}
