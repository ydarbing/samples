#include <iostream>
#include <cstdio> // printf
/*
Function:
Will always print something
Iterates a given 'x' number of times
The current iteration number is 'i'
'i' will iterate x-1 number of times
If 'i' is a multiple of 3 it will print fizz
If 'i' is a multiple of 5 it will print buzz
If 'i' is a multiple of 15 it will print out fizz buzz on the same line
And if 'i' currently does not meet any of those parameters it will print out 'i'

Flaws:
Sending any negative number will cause infinite loop
*/
void FizzBuzz(int x)
{
  bool b;
  for (int i = 0; i < x; ++i)
  {
    b = false;
    if (i % 3 == 0)
    {
      printf("fizz ");
      b = true;
    }
    if (i % 5 == 0)
    {
      printf("buzz ");
      b = true;
    }
    if (!b)
    {
      printf("%d", i);
    }
    printf("\n");
  }
}