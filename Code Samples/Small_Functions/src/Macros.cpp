#include "stdio.h"

#define PRINT_INT(n) printf(#n " = %d\n", n)

#define MAKE_STR(str) #str

#define TEST(condition, ...) ((condition) ? ) \
  printf("Passed Test: %s\n", #condition) :   \
  printf(__VA_ARGS__))

// ex:  TEST(voltage <= maxVoltage, "Voltage %d exceeds %d\n", voltage, maxVoltage);

#define FUNCTION_CALLED printf("%s called\n", __func__);


#define GENERIC_MAX(type)       \
type type##_max(type x, type y) \
{                               \
  return x > y ? x : y;         \
}

GENERIC_MAX(float)
GENERIC_MAX(int)




#define ECHO(str)    \
        do {         \
          gets(str); \
          puts(str); \
        } while (0)




