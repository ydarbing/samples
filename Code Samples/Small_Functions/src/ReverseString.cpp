// many ways to reverse a string
#include <string>
#include <algorithm> // std::reverse

void ReverseString_InPlace_NoTemp(char* p)
{
	if (!p)
		return;
	char* q = p;
	while (q && *q)// get length of str
		++q;
	for (--q; p < q; ++p, --q)
	{
		*p = *p ^ *q,
			* q = *p ^ *q,
			* p = *p ^ *q;
	}
}


void ReverseString_InPlace_Temp(char str[])
{
	size_t length = strlen(str);
	if (length <= 1)
		return;
	char c;
	size_t i = 0;
	size_t j = length - 1;
	while (i < j)
	{
		c = str[i];
		str[i] = str[j];
		str[j] = c;
		++i;
		--j;
	}
}

void ReverseString_Simple_InPlace(char* str)
{
	if (!str)
		return;
	size_t len = strlen(str);
	std::reverse(str, str + len);
}

void ReverseString_Simple_InPlace_String(std::string& str)
{
	std::reverse(str.begin(), str.end());
}



int Test(int argc, char** argv)
{
	printf("Input: %s ", argv[1]);
	ReverseString_InPlace_NoTemp(argv[1]);
	printf("Reversed: %s\n", argv[1]);

	printf("Input: %s ", argv[1]);
	ReverseString_InPlace_Temp(argv[1]);
	printf("Reversed: %s\n", argv[1]);

	printf("Input: %s ", argv[1]);
	ReverseString_Simple_InPlace(argv[1]);
	printf("Reversed: %s\n", argv[1]);

	std::string s = argv[1];
	printf("Input: %s ", s.c_str());
	ReverseString_Simple_InPlace_String(s);
	printf("Reversed: %s\n", s.c_str());
	return 0;
}