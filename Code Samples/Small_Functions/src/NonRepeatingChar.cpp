
char FirstNonRepeatingChar(const char* str)
{
  if (str == nullptr)
    return 0;
  char prevChar = *str;
  ++str;        // put this in after completing the rest of function
  if (*str == 0 || prevChar != *str)
    return prevChar;

  while (*str != 0)
  {
    if (prevChar != *str)
    {
      // check to see if the next char is repeated
      char nextChar = *(str + 1);
      if (*str != nextChar)
      {
        return *str;
      }
    }

    prevChar = *str;
    ++str;
  }

  return 0;
}

/*
int main()
{
  char* str = "aabccddeef";

  str = nullptr;
  char has = FirstNonRepeatingChar(str);


  return 0;
}
*/
