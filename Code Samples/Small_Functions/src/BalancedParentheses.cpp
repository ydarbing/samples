#include <string>
#include <stack>

bool BalancedExpressionWithStack(const std::string& str)
{
  std::stack<char> check;
  for (unsigned i = 0; i < str.size(); ++i)
  {
    if (str[i] == '(' || str[i] == '[' || str[i] == '{')
    {
      check.push(str[i]);
    }
    else if (str[i] == ']')
    {
      if (check.empty() || check.top() != '[')
        return false;
      else
        check.pop();
    }
    else if (str[i] == ')')
    {
      if (check.empty() || check.top() != '(')
        return false;
      else
        check.pop();
    }
    else if (str[i] == '}')
    {
      if (check.empty() || check.top() != '{')
        return false;
      else
        check.pop();
    }
  }

  // if check is empty then we know string is balanced
  return check.empty();
}


bool CheckPair(char open, char close)
{
  if (open == '(' && close == ')')
    return true;
  else if (open == '[' && close == ']')
    return true;
  else if (open == '{' && close == '}')
    return true;
  else
    return false;
}
bool BalancedExpressionWithStack2(const std::string& str)
{
  std::stack<char> check;
  for (unsigned i = 0; i < str.size(); ++i)
  {
    switch (str[i])
    {
    case '(':
    case '[':
    case '{':
      check.push(str[i]);
      break;
    case ')':
    case ']':
    case '}':
      if (check.empty() || CheckPair(check.top(), str[i]) == false)
        return false;
      else
        check.pop();
      break;
    }
  }

  // if check is empty then we know string is balanced
  return check.empty();
}


bool BalancedParentheses(const std::string& str)
{
  int count = 0;
  for (unsigned i = 0; i < str.size(); ++i)
  {
    if (str[i] == '(')
    {
      ++count;
    }
    else if (str[i] == ')')
    {
      if (count == 0)
        return false;
      else
        --count;
    }
  }

  return count == 0;
}


int main(void)
{
  std::string a = "([)]";
  //std::string a = "{}()[]";

  bool test = BalancedExpressionWithStack(a);
  test = BalancedExpressionWithStack2(a);

  test = BalancedParentheses(a);

  return 0;
}