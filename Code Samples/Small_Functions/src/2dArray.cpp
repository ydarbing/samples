
#define NUM_COLS 5
#define NUM_ROWS 5

void ClearRow()
{
  int a[NUM_ROWS][NUM_COLS];
  int* p;
  int i = 0;

  for (p = a[i]; p < a[i] + NUM_COLS; ++p)
  {
    *p = 0;
  }
}

void ClearColumn()
{
  int a[NUM_ROWS][NUM_COLS];

  int (*p)[NUM_COLS];
  int i = 0;

  for (p = &a[0]; p < &a[NUM_ROWS]; ++p)
  {
    (*p)[i] = 0;
  }
}
void ClearColumn2()
{
  int a[NUM_ROWS][NUM_COLS];

  int(*p)[NUM_COLS];
  int i = 0;

  for (p = a; p < a + NUM_ROWS; ++p)
  {
    (*p)[i] = 0;
  }
}


int* FindLargest(int* arr, int size)
{
  int* ret = &arr[0];
  
  for (int i = 1; i < size; ++i)
    if (arr[i] > *ret)
      *ret = arr[i];
    

  return ret;
}

void CallMultidimensionalArrayFunction()
{
  int a[NUM_ROWS][NUM_COLS];

  int* largest = FindLargest(a[0], NUM_ROWS * NUM_COLS);
}
