

#include <string>

// if not taking strings, would do roughly the same thing but with char*
bool HasSubstring(const std::string& str, const std::string& substr)
{
  // can't be a substring if str is shorter than substr
  if (substr.size() > str.size())
  {
    return false;
  }
  
  for (unsigned i = 0; i < str.size(); ++i)
  {
    unsigned j = 0;
    // start checking if substring is in string
    while (i < str.size() && str[i] == substr[j])
    {
      ++i;
      ++j;
    }
    if (j == substr.size())
      return true;
  }

  return false;
}

int getSize(const char* a)
{
  int ret = 0;
  while (*a++ != 0)
  {
    ++ret;
  }
  return ret;
}

bool HasSubstring(const char* str, const char* substr)
{
  int strSize = getSize(str);
  int substrSize = getSize(substr);
  // can't be a substring if str is shorter than substr
  if (substrSize > strSize)
  {
    return false;
  }

  while(*str != 0)
  {
    const char* subTemp = substr;
    // start checking if substring is in string
    while (*subTemp != 0 && *str == *subTemp)
    {
      ++str;
      ++subTemp;
    }
    if (*subTemp == 0) // matched all of the substring
      return true;

    ++str;
  }

  return false;
}

int main4()
{
  //std::string str = "abate";
  //std::string sub = "bat";

  char* str = "abae";
  char* sub = "bat";

  bool has = HasSubstring(str, sub);


  return 0;
}