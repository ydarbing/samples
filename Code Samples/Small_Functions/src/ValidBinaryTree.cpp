#include <climits>

struct Node{
  int data;
  Node* left;
  Node* right;
};
Node* NewNode(int data)
{
  Node* node = new Node;
  node->data = data;
  node->left = nullptr;
  node->right = nullptr;

  return(node);
}


static bool IsBST(Node* node, int minVal, int maxVal)
{
  if (node == nullptr)
    return true;

  if (node->data > minVal && node->data < maxVal
    && IsBST(node->left, minVal, node->data)
    && IsBST(node->right, node->data, maxVal))
    return true;
  else
    return false;
}

bool IsBST(Node* root)
{
  return IsBST(root, INT_MIN, INT_MAX);
}

// or use in-order traversal
// previous node should always be less than current or else not BST
bool IsBSTInOrderTraversal(Node* root)
{
  static Node* prev = nullptr;

  if (root)
  {
    if (!IsBSTInOrderTraversal(root->left))
      return false;

    // only distinct valued nodes
    if (prev != nullptr && root->data <= prev->data)
      return false;

    prev = root;

    return IsBSTInOrderTraversal(root->right);
  }
  return true;
}



int main3()
{
  Node* root = NewNode(4);
  root->left = NewNode(2);
  root->right = NewNode(5);
  root->left->left = NewNode(1);
  root->left->right = NewNode(3);

  bool test = IsBSTInOrderTraversal(root);

  return 0;
}