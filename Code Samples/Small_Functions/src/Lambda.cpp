#include <iostream>
#include <functional> // std::function

int Lambda1()
{
  []() {
    std::cout << "Lambda calling this cout" << std::endl;
  }(); // Call this lambda in place


  [](int val) {
    std::cout << "The value passed in this function is: " << val << std::endl;
  }(100); // Passing the parameter while calling in place



  auto lFunct = [](){
    std::cout << "This lambda is called using function name: " << __FUNCTION__ << std::endl;
  };
  lFunct();


  std::function<void(int)> lfnfunc = [](int val) {
    std::cout << "Value passed in this function is: " << val << std::endl;
  };
  // calling lambda function with explicit std::function handler (replacement of auto)
  lfnfunc(200);


  // using plain c style function pointers
  void(*cstylefp)(int) = [](int val) {
    std::cout << "Value passed in this function is: " << val << std::endl;
  };
  // calling the function pointers
  cstylefp(300);



  // lambda returning int
  int retval = []() -> int { return (int)1; }();





  // Named lambda with return
  auto lfnParam = [](int val) ->int {
    std::cout << "lambda takes int int =>" <<
      val << "<= and returns an int" << std::endl;
    return val * 100;
  };

  // calling lambda function with parameter and collecting the return value
  int retval1 = lfnParam(100);






  int x = 10, y = 20;
  // Passing x & y in lambda by specifying the same in capture list
  auto retVal = [x, y]() -> int
  {
    return x + y;
  }();

  std::cout << "retVal => " << retVal << "  x => " << x << "  y => " << y << std::endl;




  int x1 = 10, y1 = 20, z1 = 30;
  // passing all locals to lambda by value

  auto retval2 = [=]() -> int { return x1 + y1 + z1; }();
  std::cout << "retVal2 =>" << retval2 << std::endl;





  int x2 = 10, y2 = 20, z2 = 30;
  // passing all locals to lambda by reference
  auto retval3 = [&]() -> int
  {    x2++; y2++; z2++;
  return x2 + y2 + z2;
  }();
  std::cout << "retVal3 =>" << retval3 
    << "  x2 => " << x2 << "  y2 => " << y2 << " z2 => " << z2 << std::endl;





  int x3 = 10, y3 = 20;
  // passing locals in mix and match way ('x' by value and 'y' by reference)
  auto retval4 = [x3, &y3]() -> int
  { 
    y3++;
    return x3 + y3;
  }();
  std::cout << "retVal4 =>" << retval4 << "  x3 => " << x3 << "  y3 => " << y3 << std::endl;



  return 0;
}