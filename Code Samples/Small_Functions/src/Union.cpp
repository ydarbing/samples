/* 
  if we know the catalog will only have 3 items 
  we can save space by using unions
*/
#include "stdio.h" // printf
#include <cstring> //strcpy

#ifdef _MSC_VER
#pragma warning(disable: 4996)  // _s functions
#endif

struct catalog_item{
  int stock_number;
  double price;
  int item_type;

  union{
    struct {
      char title[12];
      char author[15];
      int num_pages;
    } book;
    struct {
      char design[24];
    } mug;
    struct {
      char design[24];
      int colors;
      int sizes;
    } shirt;
  } item;
};


void Test()
{
  catalog_item catalogItem;
  strcpy(catalogItem.item.mug.design, "Cats");
  printf("%s", catalogItem.item.mug.design); // prints cats
}
